﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Master_ClaimstatusMaster : System.Web.UI.Page
{
    clsClaimStatusMaster clsCSM = new clsClaimStatusMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptPos.DataSource = clsCSM.GetAllRecord();
        rptPos.DataBind();
    }
    protected void cmdDlt_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = (LinkButton)sender;
        Boolean blndltrslt = clsCSM.DeleteByPKID(Convert.ToInt64(lnkbtn.CommandArgument));
        if (blndltrslt)
        {
            MySession.Current.MESSAGE = "Record Deleted Successfully";
            Response.Redirect("~/Master/ClaimstatusMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsCSM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        if (txtsts.Text == string.Empty)
        {
            lblmessage.Text = "Status is required";
            msgdiv.Visible = true;
            return;
        }

        clsCSM.clmsts_status = txtsts.Text;

        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "EDIT")
        {
            blnRslt = clsCSM.Update(MySession.Current.EditID);
            MySession.Current.AddEditFlag = "ADD";
        }
        else
        {
            blnRslt = clsCSM.Save();
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/ClaimstatusMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsCSM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        LinkButton lnkedit = (LinkButton)sender;
        Boolean blnRslt = clsCSM.GetRecordByIDInProperties(Convert.ToInt64(lnkedit.CommandArgument));
        if(blnRslt)
        {
            MySession.Current.AddEditFlag  = "EDIT";
            MySession.Current.EditID = clsCSM.clmsts_id;
            txtsts.Text = clsCSM.clmsts_status;
        }
    }
}