﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="HealthPlanAddEdit.aspx.cs" Inherits="Master_HealthPlanAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkmaster" runat="server" NavigateUrl="~/Master/HealthPlanMaster.aspx"><i class="icon-retweet"></i>&nbsp;Health Plan Master</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </div>
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Plan Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <%-- <% if (User.IsInRole("administrator"))
                           { %>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Creater</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectMember" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                <span class="help-block">Select Creater for whom Workbook Is created</span>
                            </div>
                        </div>
                        <%} %>--%>
                        <div class="control-group">
                            <label for="txtcode" class="control-label">Payer Id</label>
                            <div class="controls">
                                <asp:TextBox ID="txtpyrid" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtdescription" class="control-label">Payer Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtpyrname" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtmodb" class="control-label">ENR</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlEnr" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="txtmodb" class="control-label">Type</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlType" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="C/P">COMMERCIAL/PAR</asp:ListItem>
                                    <asp:ListItem Value="G/NP">GOVERNMENT/NON-PAR</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="txtpos" class="control-label">State</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlState" runat="server" CssClass="select2-me input-large">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="txtmodb" class="control-label">LOB</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddllob" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="M">MEDICAL / PROFESSIONAL</asp:ListItem>
                                    <asp:ListItem Value="M/H">HOSPITAL / INSTITUTIONA</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="txtmodb" class="control-label">RTE1</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlRte1" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="txtmodb" class="control-label">RTE 2</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlRte2" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="txtmodb" class="control-label">RTS</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlRts" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="txtmodb" class="control-label">ERA</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlEra" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="txtmodb" class="control-label">SEC</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSec" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtdescription" class="control-label">Note</label>
                            <div class="controls">
                                <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" runat="server" CssClass="btn btn-primary" OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                            <asp:LinkButton ID="cmdCancel" runat="server" CssClass="btn" OnClick="cmdCancel_Click"><i class="icon-remove"></i> Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
