﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using clsDAL;
using System.Data;
using System.Web.Security;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;

public partial class AaaaadUser : System.Web.UI.Page
{
    clsProfileMaster clsProfile = new clsProfileMaster();
    clsUsers clsUSR = new clsUsers();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        StreamReader reader = new StreamReader(File.OpenRead(@"F:\import.csv"));
        while (!reader.EndOfStream)
        {
            string line = reader.ReadLine();
            if (!String.IsNullOrWhiteSpace(line))
            {
                string[] values = line.Split(',');
                MembershipCreateStatus memstatus;
                MembershipUser memuser = Membership.CreateUser(values[3].Trim().ToString(), values[4].Trim().ToString(), values[3].Trim().ToString(), "admin@sizzpro", "nexus@admin", true, out memstatus);
                String[] strRoles = new String[1];
                strRoles[0] = "Client";
                Roles.AddUserToRoles(values[3].Trim().ToString(), strRoles);
                MembershipUser mu = Membership.GetUser(values[3].Trim().ToString());
                Guid userguid = (Guid)mu.ProviderUserKey;
                clsUsers clsUsr = new clsUsers();
                Boolean blnUsr = clsUsr.GetRecordByIDInProperties(userguid);
                if (blnUsr == true)
                {
                    clsProfile.prm_firstname = values[2].Trim().ToString();
                    clsProfile.prm_middlename = values[1].Trim().ToString();
                    clsProfile.prm_lastname = values[0].Trim().ToString();
                    clsProfile.prm_user_Id = clsUsr.User_Id;
                    clsProfile.prm_userId = userguid.ToString();
                    clsProfile.prm_emailid = values[3].Trim().ToString();//username txtEmail.Text.Trim();
                    clsProfile.prm_passwordreset = true;
                    clsProfile.prm_userId = userguid.ToString();
                    clsProfile.prm_emailverify = false;
                    clsProfile.prm_passwordreset = true;
                    clsProfile.prm_userrole = "Client";
                    clsProfile.prm_tokenid = userguid.ToString();
                    clsProfile.prm_tokendatetime = DateTime.Now;
                    Boolean blnProfile = clsProfile.Save();
                    if (blnProfile == false)
                    {
                        Response.Write("Error in user profile creation.");
                    }
                }
                else
                {
                    Response.Write("Error in getting user primary key.");
                }
            }
        }
    }


    protected void createpdf_Click(object sender, EventArgs e)
    {

    //    Guid id = new Guid("6B92BD0C-D496-40A0-B252-8F99772F5035");

    //    DataTable dt = cis.getDatabyUserId(id);

    //    //Guid id = new Guid(Membership.GetUser().ProviderUserKey.ToString());

    //    //DataTable dt = clsInfCsnt.getDatabyUserId(id);

    //    string ICfile = (Request.PhysicalApplicationPath + "\\INTAKE_FORM_ICNewEditable.pdf").ToString();
    //    //@"F:\Anil Ravrani\shp\SizzPro\ltcweb\ICform.pdf";

    //    string newFile = (Request.PhysicalApplicationPath + "\\Clients_informedConsentFiles\\" + id + ".pdf").ToString();
    //    //@"F:\Anil Ravrani\shp\SizzPro\ltcweb\Clients_informedConsentFiles\" + id + ".pdf";

    //    PdfReader pdfReader = new PdfReader(ICfile);

    //    PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));

    //    AcroFields pdfFormFields = pdfStamper.AcroFields;


    //    foreach (DataRow row in dt.Rows)
    //    {
    //        string newfiled = "";
    //        string oldfiled = row["ini_valueflag"].ToString();
    //        if (oldfiled.Contains("date"))
    //        {
    //            newfiled = Convert.ToDateTime(row["ini_valuedate"]).ToString("dd-MM-yyyy");
    //            pdfFormFields.SetField(oldfiled, newfiled);
    //        }
    //        else if (oldfiled.Contains("cpMainContent_rdbpaymentdivided"))
    //        {
    //            int cnt = Convert.ToInt32(row["ini_valueint"].ToString());
    //            switch (cnt)
    //            {
    //                case 1:
    //                    pdfFormFields.SetField("cpMainContent_rdbpaymentdivided1", "Yes");
    //                    break;
    //                case 2:
    //                    pdfFormFields.SetField("cpMainContent_rdbpaymentdivided2", "Yes");
    //                    break;
    //                case 3:
    //                    pdfFormFields.SetField("cpMainContent_rdbpaymentdivided3", "Yes");
    //                    break;
    //                case 4:
    //                    pdfFormFields.SetField("cpMainContent_rdbpaymentdivided4", "Yes");
    //                    break;
    //            }

    //        }
    //        else if (oldfiled.Contains("cpMainContent_rdbCardType"))
    //        {
    //            int cnt = Convert.ToInt32(row["ini_valueint"].ToString());
    //            switch (cnt)
    //            {
    //                case 1:
    //                    pdfFormFields.SetField("cpMainContent_rdbCardType1", "Yes");
    //                    break;
    //                case 2:
    //                    pdfFormFields.SetField("cpMainContent_rdbCardType2", "Yes");
    //                    break;
    //                case 3:
    //                    pdfFormFields.SetField("cpMainContent_rdbCardType3", "Yes");
    //                    break;
    //                case 4:
    //                    pdfFormFields.SetField("cpMainContent_rdbCardType4", "Yes");
    //                    break;
    //            }
    //        }
    //        else if (oldfiled.Contains("cpMainContent_notityChk"))
    //        {
    //            int cnt = Convert.ToInt32(row["ini_valueint"].ToString());
    //            switch (cnt)
    //            {
    //                case 1:
    //                    pdfFormFields.SetField("cpMainContent_notityChk1", "Yes");
    //                    break;
    //                case 2:
    //                    pdfFormFields.SetField("cpMainContent_notityChk2", "Yes");
    //                    break;
    //                case 3:
    //                    pdfFormFields.SetField("cpMainContent_notityChk3", "Yes");
    //                    break;
    //                case 4:
    //                    pdfFormFields.SetField("cpMainContent_notityChk4", "Yes");
    //                    break;
    //            }
    //        }
    //        else if (oldfiled.Contains("cpMainContent_FeeScalechk"))
    //        {
    //            string[] feeScale = oldfiled.Split('_');
    //            int cnt = Convert.ToInt32(feeScale[2].ToString());
    //            //bool rslt = Convert.ToBoolean(row["ini_valuebit"].ToString());
    //            switch (cnt)
    //            {
    //                case 1:
    //                    pdfFormFields.SetField("cpMainContent_FeeScalechk_1", "Yes");
    //                    break;
    //                case 2:
    //                    pdfFormFields.SetField("cpMainContent_FeeScalechk_2", "Yes");
    //                    break;
    //                case 3:
    //                    pdfFormFields.SetField("cpMainContent_FeeScalechk_3", "Yes");
    //                    break;
    //                case 4:
    //                    pdfFormFields.SetField("cpMainContent_FeeScalechk_4", "Yes");
    //                    break;
    //                case 5:
    //                    pdfFormFields.SetField("cpMainContent_FeeScalechk_5", "Yes");
    //                    break;
    //                case 6:
    //                    pdfFormFields.SetField("cpMainContent_FeeScalechk_6", "Yes");
    //                    break;
    //            }
    //        }

    //        else if (oldfiled.Contains("cpMainContent_pRate"))
    //        {
    //            string[] feeScale = oldfiled.Split('_');
    //            int cnt = Convert.ToInt32(feeScale[2].ToString());
    //            //bool rslt = Convert.ToBoolean(row["ini_valuebit"].ToString());
    //            switch (cnt)
    //            {
    //                case 1:
    //                    pdfFormFields.SetField("cpMainContent_pRate_1", "Yes");
    //                    break;
    //                case 2:
    //                    pdfFormFields.SetField("cpMainContent_pRate_2", "Yes");
    //                    break;
    //                case 3:
    //                    pdfFormFields.SetField("cpMainContent_pRate_3", "Yes");
    //                    break;
    //                case 4:
    //                    pdfFormFields.SetField("cpMainContent_pRate_4", "Yes");
    //                    break;
    //                case 5:
    //                    pdfFormFields.SetField("cpMainContent_pRate_5", "Yes");
    //                    break;
    //            }
    //        }
    //        else
    //        {
    //            newfiled = row["ini_valuetext"].ToString();
    //            pdfFormFields.SetField(oldfiled, newfiled);
    //        }
    //    }

    //    pdfStamper.FormFlattening = true;
    //    // close the pdf
    //    pdfStamper.Dispose();
    //    pdfStamper.Close();
    }
}
