﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MySession
/// </summary>
public class MySession
{
    // private constructor
    private MySession()
    {
        UserLoginName = string.Empty;
        UserLoginID = 0;
        UserGuID = Guid.NewGuid();
        UserRole = "";

        SESSIONID = new Guid();

        EditID = 0;
        AddEditFlag = "null";

        MemberAlias = "Member";
        UserAlias = "User";
        ClientAlias = "Client";

        MemberID = new Guid();

        UserFullName = string.Empty;
        UserLoginFullname = string.Empty;

        MSGCODE = string.Empty;
        MESSAGE = string.Empty;

        ObjOwnerID = new Guid();
        AssignmentClientId = new Guid();

        CodeCharge = 0;


    }

    // Gets the current session.
    public static MySession Current
    {
        get
        {
            MySession session =
              (MySession)HttpContext.Current.Session["__MySession__"];
            if (session == null)
            {
                session = new MySession();
                HttpContext.Current.Session["__MySession__"] = session;
            }
            return session;
        }
    }

    // **** add your session properties here, e.g like this:
    public string UserLoginName { get; set; }
    public Int64 UserLoginID { get; set; }
    public Guid UserGuID { get; set; }
    public string UserRole { get; set; }

    public Int64 EditID { get; set; }
    public string AddEditFlag { get; set; }

    public Guid MemberID { get; set; }

    public Guid AssignmentClientId { get; set; }

    public Guid ObjOwnerID { get; set; }

    public Guid SESSIONID { get; set; }

    public string MemberAlias { get; set; }
    public string UserAlias { get; set; }
    public string ClientAlias { get; set; }

    public string UserFullName { get; set; }

    public string UserLoginFullname { get; set; }

    public int noOfAdmin { get; set; }
    public int noOfmember { get; set; }
    public int noOfuser { get; set; }
    public int noOfclient { get; set; }
    public int noOfserv{ get; set; }

    public string MSGCODE { get; set; }
    public string MESSAGE { get; set; }

    public Int64 CodeCharge { get; set; }
}