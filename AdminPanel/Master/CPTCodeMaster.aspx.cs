﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_CodeMaster : System.Web.UI.Page
{
    clsCPTCodeMaster clsCCM = new clsCPTCodeMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptCode.DataSource = clsCCM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        rptCode.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Master/CPTCodeAddEdit.aspx");
    }
    protected void rptCode_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/CPTCodeAddEdit.aspx");
        }
        if(e.CommandName == "DELETE")
        {
            Boolean blnRslt= clsCCM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (blnRslt)
            {
                MySession.Current.MESSAGE = "Record Deleted Successfully";
                Response.Redirect("~/Master/CPTCodeMaster.aspx");
            }
            else
            {
                lblmessage.Text = clsCCM.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
}