﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using clsDAL.Conn;
using System.Web.Security;
using System.Drawing;
using clsDAL;
using System.Web.UI.HtmlControls;
public partial class Users_MessageCenter : System.Web.UI.Page
{
    clsProfileMaster clsPro = new clsProfileMaster();
    DataTable dt = new DataTable();
    clsmsgMaster msgMas = new clsmsgMaster();
    string memKey = Membership.GetUser().ProviderUserKey.ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindContact();
            bindSentMsg();
        }
    }

    public void bindContact()
    {
        dt = clsPro.GetUserList(new Guid(memKey));
        rptContacts.DataSource = dt;
        rptContacts.DataBind();
    }
    public void bindSentMsg()
    {
        dt.Clear();
        dt = msgMas.GetSentByGUID(new Guid(memKey));
        rptSentMesg.DataSource = dt;
        rptSentMesg.DataBind();
    }
    protected void cmdSend_Click(object sender, EventArgs e)
    {
        bool reslt = false;
        List<Guid> reciverId = new List<Guid>();

        foreach (RepeaterItem aItem in rptContacts.Items)
        {
            HtmlInputCheckBox chkDisplayTitle = (HtmlInputCheckBox)aItem.FindControl("chkDisplayTitle");
            if (chkDisplayTitle.Checked)
            {
                reciverId.Add(new Guid(chkDisplayTitle.Value.ToString()));
            }
        }
        if (reciverId.Count == 0)
        {
            lblMsg.Text = "Please select at least one recipent!!!";
            lblMsg.ForeColor = Color.Red;
            lblMsg.Visible = true;
            return;
        }
        msgMas.Msender = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        msgMas.Mtext = txtMsg.Text.ToString();
        msgMas.ReciverIds = reciverId;
        msgMas.Mdatetime = DateTime.Now;
        reslt = msgMas.Save();

        if (reslt == true)
        {
            lblMsg.Text = "Message Sent Successfully.";
            lblMsg.ForeColor = Color.Green;
            lblMsg.Visible = true;
            txtMsg.Text = string.Empty;
            foreach (RepeaterItem aItem in rptContacts.Items)
            {
                HtmlInputCheckBox chkDisplayTitle = (HtmlInputCheckBox)aItem.FindControl("chkDisplayTitle");
                if (chkDisplayTitle.Checked)
                {
                    chkDisplayTitle.Checked = false;
                }
            }
            bindSentMsg();
        }
    }
}