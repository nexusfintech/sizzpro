﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
using System.Data;

public partial class Admin_Course_CourseAddEdit : System.Web.UI.Page
{
    clsAppointmentCourse clsCRS = new clsAppointmentCourse();
    clsClientParentMapping clsCPM = new clsClientParentMapping();

    private void EditEntry()
    {
        Boolean blnResult = clsCRS.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnResult == true)
        {
            txtDescription.Text = clsCRS.crs_coursedescription;
            txtCourseName.Text = clsCRS.crs_coursename;
            txtGroupName.Text = clsCRS.crs_groupname;
            //txtMinutes.Text = clsCRS.crs_minutes.ToString();
            long interval = clsCRS.crs_interval;
            long h = 0;
            long m = 0;
            if(interval >= 60)
            {
                h = interval % 60;
                m = interval - (h * 60);
            }
            ListItem member = ddlSelectMember.Items.FindByValue(clsCRS.crs_userid.ToString());
            if (member != null)
            {
                ddlSelectMember.SelectedIndex = ddlSelectMember.Items.IndexOf(member);
            }
        }
        else { lblmessage.Text = "Error in edit operation please try again " + clsCRS.GetSetErrorMessage; }

    }

    private void BindCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "UserId";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRole("Member");
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCombo();
            if (MySession.Current.AddEditFlag == "null")
            {
                Response.Redirect("~/Admin/Course/CourseMaster.aspx");
            }
            if (MySession.Current.AddEditFlag == "EDIT")
            { 
                EditEntry(); 
            }
        }
    }

    private string PageValidation()
    {
        string strResult = "";
        if (txtCourseName.Text.Trim() == "")
        { strResult += "Please Input Name <br/>"; }
        if (txtDescription.Text.Trim() == "")
        { strResult += "Please Input Description <br/>"; }
        if (txtMinutes.Text.Trim() == "")
        { strResult += "Please Input Minutes <br/>"; }
        return strResult;
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Boolean blnSave = false;
        string strResult = PageValidation();
        if (strResult != "")
        { lblmessage.Text = strResult; return; }
        clsCRS.crs_coursedescription = txtDescription.Text.Trim();
        clsCRS.crs_coursename = txtCourseName.Text.Trim();
        clsCRS.crs_groupname = txtGroupName.Text.Trim();
        //clsCRS.crs_minutes = Convert.ToInt64(txtMinutes.Text.Trim());
        if (User.IsInRole("administrator"))
        {
            clsCRS.crs_userid = new Guid(ddlSelectMember.SelectedItem.Value);
        }
        else
        {
            clsCRS.crs_userid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        }
        clsCRS.crs_interval = (Convert.ToInt64(txtHour.Text) * 60) + (Convert.ToInt64(txtMinutes.Text));
        if (MySession.Current.AddEditFlag == "ADD")
        { blnSave = clsCRS.Save(); }
        else
        { blnSave = clsCRS.Update(MySession.Current.EditID); }
        if (blnSave == true)
        { lblmessage.Text = "Saved Successfully."; clear(); return; }
        else { lblmessage.Text = "Error in save ..." + clsCRS.GetSetErrorMessage; }
    }

    public void clear()
    {
        //ddlInterval.SelectedIndex = -1;
        ddlSelectMember.SelectedIndex = -1;
        txtCourseName.Text = string.Empty;
        txtDescription.Text = string.Empty;
        txtGroupName.Text = string.Empty;
        txtMinutes.Text = string.Empty;
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/Course/CourseMaster.aspx");
    }
}