﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using clsDAL;

/// <summary>
/// Summary description for clsGetTemplate
/// </summary>
public class clsGetTemplate
{
    static clsEmailAccounts clsEmailAct = new clsEmailAccounts();
    static clsEmailTemplate clsEmpTemplate = new clsEmailTemplate();
    static clsSendMail clsMailSend = new clsSendMail();

    private static string strTemplate;

    public static string HtmlTemplate
    {
        get { return strTemplate; }
        set { strTemplate = value; }
    }

    public clsGetTemplate()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static Boolean GetTemplate(string strTemplateName)
    {
        Boolean blnResult = false;
        strTemplate = "";
        Boolean blnTemplate = clsEmpTemplate.GetRecordByNameInProperties(strTemplateName);
        if (blnTemplate == true)
        {
            Boolean blnMailAct = clsEmailAct.GetRecordByIDInProperties(clsEmpTemplate.emt_mailaccountid);
            if (blnMailAct == true)
            {
                Boolean blnBindAct = clsMailSend.BindEmailAccount(clsEmailAct.ema_emailaddress);
                if (blnBindAct == true)
                {
                    strTemplate = clsEmpTemplate.emt_templatebody.Trim();
                    blnResult = true;
                }
                else { strTemplate = "Error in email account binding to mail send engine.."; blnResult = false; }
            }
            else { strTemplate = "Error in template mail account information"; blnResult = false; }
        }
        else { strTemplate = "Error in mail template"; blnResult = false; }
        return blnResult;
    }

    public static Boolean SendMail(string strToEmail)
    {
        Boolean blnResult = false;
        clsMailSend.Subject = clsEmpTemplate.emt_templatesubject;
        clsMailSend.ToMailAddress = strToEmail;
        if (clsEmpTemplate.emt_ccmail.Trim() != "") { clsMailSend.CCMailAddress = clsEmpTemplate.emt_ccmail.Trim(); }
        if (clsEmpTemplate.emt_bccmail.Trim() != "") { clsMailSend.BCCMailAddress = clsEmpTemplate.emt_bccmail.Trim(); }
        clsMailSend.HtmlBody = strTemplate;
        clsMailSend.Priority = System.Net.Mail.MailPriority.Normal;
        Boolean blnSendMail = clsMailSend.SendMail();
        if (blnSendMail == true)
        { blnResult = true; }
        else { strTemplate = "Error in email sending."; blnResult = false; }
        return blnResult;
    }

}