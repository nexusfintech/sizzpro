﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsFamilyofOrigin
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsFamilyofOrigin()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsFamilyofOrigin()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _foo_id; }
            set
            {
                _foo_id = value;
                if (clsCMD.Parameters.Contains("@foo_id"))
                { clsCMD.Parameters["@foo_id"].Value = value; }
                else { clsCMD.Parameters.Add("@foo_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _foo_id;
        public Int64 foo_id
        {
            get { return _foo_id; }
            set
            {
                _foo_id = value;
                if (clsCMD.Parameters.Contains("@foo_id"))
                { clsCMD.Parameters["@foo_id"].Value = value; }
                else { clsCMD.Parameters.Add("@foo_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _foo_userid;
        public Guid foo_userid
        {
            get { return _foo_userid; }
            set
            {
                _foo_userid = value;
                if (clsCMD.Parameters.Contains("@foo_userid"))
                { clsCMD.Parameters["@foo_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@foo_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _foo_age;
        public String foo_age
        {
            get { return _foo_age; }
            set
            {
                _foo_age = value;
                if (clsCMD.Parameters.Contains("@foo_age"))
                { clsCMD.Parameters["@foo_age"].Value = value; }
                else { clsCMD.Parameters.Add("@foo_age", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _foo_firstname;
        public String foo_firstname
        {
            get { return _foo_firstname; }
            set
            {
                _foo_firstname = value;
                if (clsCMD.Parameters.Contains("@foo_firstname"))
                { clsCMD.Parameters["@foo_firstname"].Value = value; }
                else { clsCMD.Parameters.Add("@foo_firstname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _foo_middlename;
        public String foo_middlename
        {
            get { return _foo_middlename; }
            set
            {
                _foo_middlename = value;
                if (clsCMD.Parameters.Contains("@foo_middlename"))
                { clsCMD.Parameters["@foo_middlename"].Value = value; }
                else { clsCMD.Parameters.Add("@foo_middlename", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _foo_lastname;
        public String foo_lastname
        {
            get { return _foo_lastname; }
            set
            {
                _foo_lastname = value;
                if (clsCMD.Parameters.Contains("@foo_lastname"))
                { clsCMD.Parameters["@foo_lastname"].Value = value; }
                else { clsCMD.Parameters.Add("@foo_lastname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _foo_description;
        public String foo_description
        {
            get { return _foo_description; }
            set
            {
                _foo_description = value;
                if (clsCMD.Parameters.Contains("@foo_description"))
                { clsCMD.Parameters["@foo_description"].Size = value.Length; clsCMD.Parameters["@foo_description"].Value = value; }
                else { clsCMD.Parameters.Add("@foo_description", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Boolean _foo_livewithclient;
        public Boolean foo_livewithclient
        {
            get { return _foo_livewithclient; }
            set
            {
                _foo_livewithclient = value;
                if (clsCMD.Parameters.Contains("@foo_livewithclient"))
                { clsCMD.Parameters["@foo_livewithclient"].Value = value; }
                else { clsCMD.Parameters.Add("@foo_livewithclient", SqlDbType.Bit).Value = value; }
            }
        }
        private String _foo_relationflag;
        public String foo_relationflag
        {
            get { return _foo_relationflag; }
            set
            {
                _foo_relationflag = value;
                if (clsCMD.Parameters.Contains("@foo_relationflag"))
                { clsCMD.Parameters["@foo_relationflag"].Value = value; }
                else { clsCMD.Parameters.Add("@foo_relationflag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                foo_id = Convert.ToInt64(dr["foo_id"]);
                foo_userid = new Guid(dr["foo_userid"].ToString());
                foo_age = Convert.ToString(dr["foo_age"]);
                foo_firstname = Convert.ToString(dr["foo_firstname"]);
                foo_middlename = Convert.ToString(dr["foo_middlename"]);
                foo_lastname = Convert.ToString(dr["foo_lastname"]);
                foo_description = Convert.ToString(dr["foo_description"]);
                foo_livewithclient = Convert.ToBoolean(dr["foo_livewithclient"]);
                foo_relationflag = Convert.ToString(dr["foo_relationflag"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            foo_id = Convert.ToInt64("0");
            foo_userid = new Guid();
            foo_age = Convert.ToString("NA");
            foo_firstname = Convert.ToString("NA");
            foo_middlename = Convert.ToString("NA");
            foo_lastname = Convert.ToString("NA");
            foo_description = Convert.ToString("NA");
            foo_livewithclient = Convert.ToBoolean(false);
            foo_relationflag = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.foo_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_familyoforiginAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 foo_id)
        {
            Boolean blnResult = false;
            this.foo_id = foo_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_familyoforiginAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 foo_id)
        {
            Boolean blnResult = false;
            this.foo_id = foo_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_familyoforiginAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_familyoforiginGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsFamilyofOrigin_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsFamilyofOrigin_PropertiesList> lstResult = new List<clsFamilyofOrigin_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].foo_id = Convert.ToInt64(dr["foo_id"]);
                lstResult[i].foo_userid = new Guid(dr["foo_userid"].ToString());
                lstResult[i].foo_age = Convert.ToString(dr["foo_age"]);
                lstResult[i].foo_firstname = Convert.ToString(dr["foo_firstname"]);
                lstResult[i].foo_middlename = Convert.ToString(dr["foo_middlename"]);
                lstResult[i].foo_lastname = Convert.ToString(dr["foo_lastname"]);
                lstResult[i].foo_description = Convert.ToString(dr["foo_description"]);
                lstResult[i].foo_livewithclient = Convert.ToBoolean(dr["foo_livewithclient"]);
                lstResult[i].foo_relationflag = Convert.ToString(dr["foo_relationflag"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 foo_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.foo_id = foo_id;
                clsCMD.CommandText = "sp_familyoforiginGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByIDInProperties(Guid Userid,string strRelFlag)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.foo_userid = Userid;
                this.foo_relationflag = strRelFlag;
                clsCMD.CommandText = "sp_familyoforiginGetData";
                SetGetSPFlag = "BYFLAG";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 foo_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.foo_id = foo_id;
                clsCMD.CommandText = "sp_familyoforiginGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }

    public class clsFamilyofOrigin_PropertiesList
    {
        public Int64 foo_id { get; set; }
        public Guid foo_userid { get; set; }
        public String foo_age { get; set; }
        public String foo_firstname { get; set; }
        public String foo_middlename { get; set; }
        public String foo_lastname { get; set; }
        public String foo_description { get; set; }
        public Boolean foo_livewithclient { get; set; }
        public String foo_relationflag { get; set; }
    }
}