﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="ViewClientSession.aspx.cs" Inherits="Admin_SessionManagement_ViewClientSession" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Action</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a><i class="glyphicon-notes"></i>
                    <asp:Button ID="cmdClntWise" runat="server" Text="Client Wise Sessions" CssClass="btn" OnClick="cmdClntWise_Click" /></a>
            </li>
            <li>
                <a><i class="glyphicon-notes"></i>
                    <asp:Button ID="cmdDateWise" runat="server" Text="Date Wise Sessions" CssClass="btn" OnClick="cmdDateWise_Click" /></a>
            </li>
            <li>
                <a><i class="glyphicon-notes"></i>
                    <asp:Button ID="cmdItemWise" runat="server" Text="Item Wise Sessions" CssClass="btn" OnClick="cmdItemWise_Click" /></a>
            </li>
            <li>
                <a><i class="glyphicon-notes"></i>
                    <asp:Button ID="cmdProvWise" runat="server" Text="Provider Wise Sessions" CssClass="btn" OnClick="cmdProvWise_Click" /></a>
            </li>
            <li>
                <a><i class="glyphicon-notes"></i>
                    <asp:Button ID="cmdFaciWise" runat="server" Text="Facilities Wise Sessions" CssClass="btn" OnClick="cmdFaciWise_Click" /></a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="glyphicon-projector"></i>
                Client Session View
            </h3>
        </div>
        <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        </div>
        <div class="box-content nopadding">
            <asp:Panel ID="pnlClientWise" runat="server" Visible="false">
                <div class="box">
                    <div class="box-title">
                        <h5>
                            <i class="glyphicon-filter"></i>
                            Filter By <%= MySession.Current.ClientAlias %> Wise
                        </h5>
                    </div>
                    <div class="box-content nopadding">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="span3">
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">
                                            Select <%= MySession.Current.ClientAlias %></label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlClient" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="span2">
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">
                                            &nbsp;
                                        </label>
                                        <div class="controls controls-row">
                                            <asp:LinkButton ID="cmdClientWisefilter" CssClass="btn btn-primary" OnClick="cmdClientWisefilter_Click" runat="server"><i class="icon-refresh"></i> Display</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="span7">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlDateWise" runat="server" Visible="false">
                <div class="box">
                    <div class="box-title">
                        <h5>
                            <i class="glyphicon-filter"></i>
                            Filter By Date Range Wise
                        </h5>
                    </div>
                    <div class="box-content nopadding">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="span3">
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">
                                            Enter Date Range</label>
                                        <div class="controls controls-row">
                                            <asp:TextBox ID="txtDateRange" runat="server" CssClass="input-large daterangepick"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="span2">
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">
                                            &nbsp;
                                        </label>
                                        <div class="controls controls-row">
                                            <asp:LinkButton ID="cmdDateWiseFilter" CssClass="btn btn-primary" OnClick="cmdDateWiseFilter_Click" runat="server"><i class="icon-refresh"></i> Display</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="span7">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlItemWise" runat="server" Visible="false">
                <div class="box">
                    <div class="box-title">
                        <h5>
                            <i class="glyphicon-filter"></i>
                            Filter By Item Wise
                        </h5>
                    </div>
                    <div class="box-content nopadding">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="span3">
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">
                                            Select Item</label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlItem" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="span2">
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">
                                            &nbsp;
                                        </label>
                                        <div class="controls controls-row">
                                            <asp:LinkButton ID="cmdItemWiseFilter" CssClass="btn btn-primary" OnClick="cmdItemWiseFilter_Click" runat="server"><i class="icon-refresh"></i> Display</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="span7">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlProvWise" runat="server" Visible="false">
                <div class="box">
                    <div class="box-title">
                        <h5>
                            <i class="glyphicon-filter"></i>
                            Filter By Provider Wise
                        </h5>
                    </div>
                    <div class="box-content nopadding">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="span3">
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">
                                            Select Provider</label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlProvider" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="span2">
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">
                                            &nbsp;
                                        </label>
                                        <div class="controls controls-row">
                                            <asp:LinkButton ID="cmdProvWiseFilter" CssClass="btn btn-primary" OnClick="cmdProvWiseFilter_Click" runat="server"><i class="icon-refresh"></i> Display</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="span7">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlFaciWise" runat="server" Visible="false">
                <div class="box">
                    <div class="box-title">
                        <h5>
                            <i class="glyphicon-filter"></i>
                            Filter By Facilities Wise
                        </h5>
                    </div>
                    <div class="box-content nopadding">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="span3">
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">
                                            Select Facilities</label>
                                        <div class="controls controls-row">
                                            <asp:DropDownList ID="ddlFacilities" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="span2">
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">
                                            &nbsp;
                                        </label>
                                        <div class="controls controls-row">
                                            <asp:LinkButton ID="cmdFaciWiseFilter" CssClass="btn btn-primary" OnClick="cmdFaciWiseFilter_Click" runat="server"><i class="icon-refresh"></i> Display</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="span7">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Repeater ID="dtDisplay" runat="server">
                <HeaderTemplate>
                    <table class="table table-hover table-colored-header table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="width: 10%;">Date</th>
                                <th style="width: 10%;">Client Name</th>
                                <th style="width: 10%;">Session Type</th>
                                <th style="width: 10%;">Location</th>
                                <th style="width: 10%;">Total Charged</th>
                                <th style="width: 10%;"><%= MySession.Current.UserAlias%> </th>
                                <th style="text-align: center; width: 10%;">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("ssn_date") %></td>
                        <td><%# Eval("prm_firstname") %>&nbsp;<%# Eval("prm_lastname") %></td>
                        <td><%# Eval("ssn_sessiontype") %></td>
                        <td><%# Eval("ssn_location") %></td>
                        <td><%# Eval("ssn_totalcharged") %></td>
                        <td><%# Eval("prm_firstname") %></td>
                        <td>
                            <asp:Button ID="cmdEdit" runat="server" Text="Edit" CssClass="btn cancel" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ssn_id") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

