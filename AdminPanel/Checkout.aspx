﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSite.master" AutoEventWireup="true" CodeFile="Checkout.aspx.cs" Inherits="Checkout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid" style="border: 1px solid #0072D6;">
        <div class="span12">
            <%--<asp:Literal ID="ltrPageBody" runat="server"></asp:Literal>--%>
            <h4 style="text-align: center">Get SizzPRO now !</h4>
            <hr />
            <asp:Repeater ID="dtSubModul" runat="server" OnItemDataBound="dtSubModul_ItemDataBound">
                <HeaderTemplate>
                    <table class="table table-nomargin table-bordered">
                        <thead>
                            <tr>
                                <th>Our Service</th>
                                <th>Affordable Price</th>
                                <th>Get It Now</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><strong><%# Eval("srvc_servicename") %></strong></td>
                        <td> $ <%# Eval("srvc_recuringcharge") %> / <%# Eval("srvc_periodinterval") %></td>
                        <td align="center">
                            <asp:LinkButton ID='lnkadd' runat="server" CssClass="btn btn-small btn-primary" CommandName='<%# Eval("srvc_servicename") %>' CommandArgument='<%#Eval("srvc_id") %>' OnClick="LinkButton_Click"><i class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID='lnkrmv' runat="server" CssClass="btn btn-small btn-inverse" CommandName='<%# Eval("srvc_servicename") %>' Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:Label ID="lblMsg" Font-Bold="true" ForeColor="Red" runat="server"></asp:Label>
            <%--<table class="table table-nomargin table-bordered">
                <thead>
                    <tr>
                        <th>Our Service</th>
                        <th>Affordable Price</th>
                        <th>Get It Now</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><strong>eMarketing</strong></td>
                        <td>$79</td>
                        <td align="center">
                            <asp:LinkButton ID="lnkemarketingadd" runat="server" CssClass="btn btn-small btn-primary" CommandArgument="eMarketing#$79" OnClick="LinkButton_Click"><i class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID="lnkemarketingrmv" runat="server" CssClass="btn btn-small btn-inverse" Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Scheduling</strong></td>
                        <td>$39</td>
                        <td align="center">
                            <asp:LinkButton ID="lnkschedulingadd" runat="server" CssClass="btn btn-small btn-primary" CommandArgument="Scheduling#$39" OnClick="LinkButton_Click"><i class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID="lnkschedulingrmv" runat="server" CssClass="btn btn-small btn-inverse" Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Charting</strong></td>
                        <td>$29</td>
                        <td align="center">
                            <asp:LinkButton ID="lnkchartingadd" runat="server" CssClass="btn btn-small btn-primary" CommandArgument="Charting#$29" OnClick="LinkButton_Click"><i class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID="lnkchartingrmv" runat="server" CssClass="btn btn-small btn-inverse" Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Billing</strong></td>
                        <td>$59</td>
                        <td align="center">
                            <asp:LinkButton ID="lnkbillingadd" runat="server" CssClass="btn btn-small btn-primary" CommandArgument="Billing#$59" OnClick="LinkButton_Click"><i class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID="lnkbillingrmv" runat="server" CssClass="btn btn-small btn-inverse" Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Payables</strong></td>
                        <td>$29</td>
                        <td align="center">
                            <asp:LinkButton ID="lnkpayblesadd" runat="server" CssClass="btn btn-small btn-primary" CommandArgument="Payables#$29" OnClick="LinkButton_Click"><i class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID="lnkpayblesrmv" runat="server" CssClass="btn btn-small btn-inverse" Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Workbooks</strong></td>
                        <td>$39</td>
                        <td align="center">
                            <asp:LinkButton ID="lnkwrkbkadd" runat="server" CssClass="btn btn-small btn-primary" CommandArgument="Workbooks#$39" OnClick="LinkButton_Click"><i class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID="lnkwrkbkrmv" runat="server" CssClass="btn btn-small btn-inverse" Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Tests</strong></td>
                        <td>$19</td>
                        <td align="center">
                            <asp:LinkButton ID="lnktestadd" runat="server" CssClass="btn btn-small btn-primary" CommandArgument="Tests#$19" OnClick="LinkButton_Click"><i id="i7" runat="server" class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID="lnktestrmv" runat="server" CssClass="btn btn-small btn-inverse" Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>sMail</strong></td>
                        <td>$19</td>
                        <td align="center">
                            <asp:LinkButton ID="lnksmailadd" runat="server" CssClass="btn btn-small btn-primary" CommandArgument="sMail#$19" OnClick="LinkButton_Click"><i class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID="lnksmailrmv" runat="server" CssClass="btn btn-small btn-inverse" Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Offline Solution</strong></td>
                        <td>$29</td>
                        <td align="center">
                            <asp:LinkButton ID="lnkofflinesolutionadd" runat="server" CssClass="btn btn-small btn-primary" CommandArgument="Offline Solution#$29" OnClick="LinkButton_Click"><i class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID="lnkofflinesolutionrmv" runat="server" CssClass="btn btn-small btn-inverse" Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>SizzPRO Suite</strong></td>
                        <td>$299</td>
                        <td align="center">
                            <asp:LinkButton ID="lnksizzprosuitadd" runat="server" CssClass="btn btn-small btn-primary" CommandArgument="SizzPRO Suite#$299" OnClick="LinkButton_Click"><i class="glyphicon-cart_in"></i>&nbsp;&nbsp;Add to Cart</asp:LinkButton>
                            <asp:LinkButton ID="lnksizzprosuitrmv" runat="server" CssClass="btn btn-small btn-inverse" Visible="false" OnClick="LinkbuttonRemove_Click"><i class="glyphicon-cart_out"></i>&nbsp;&nbsp;Remove from cart</asp:LinkButton>
                        </td>
                    </tr>
                </tbody>
            </table>--%>
            <div class="span12" style="margin: auto; padding: 15px;">
                <div class="span6">
                    <a href="Default.aspx" class="btn btn-block btn-primary">
                        <h6><i class="icon-home"></i>Home</h6>
                    </a>
                </div>
                <div class="span6">
                    <a href="mycart.aspx" class="btn btn-block btn-primary">
                        <h6><i class="glyphicon-shopping_cart"></i>Go To Cart</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

