﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;

public partial class UserControl_GeneralControl_ucAuthorization : System.Web.UI.UserControl
{
    clsauthorizationMaster clsAM = new clsauthorizationMaster();
    private Int64 intId = 0;
    private string strName = string.Empty;

    public Int64 SelectedValue
    {
        get { intId = Convert.ToInt64(ddlauthorization.SelectedValue); return intId; }
        set { intId = value; }
    }

    public string SelectedName
    {
        get { strName = ddlauthorization.SelectedItem.Text; return strName; }
        set { strName = value; }
    }

    public int Width
    {
        set { ddlauthorization.Width = Unit.Pixel(value); }
    }

    public void Bind()
    {
        DataTable dt = clsAM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);

        foreach(DataRow dr in dt.Rows)
        {
            string an = dr["auth_authorizationnumber"].ToString();
            string dtsd = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(dr["auth_startdate"].ToString()));
            string dted = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(dr["auth_enddate"].ToString()));
            ddlauthorization.Items.Add(new ListItem(an+" - "+dtsd+" - "+dted, dr["auth_authorizationnumber"].ToString()));
        }
        //dt.Columns.Add("AuthCode", typeof(string), "auth_authorizationnumber + ' - '  + auth_startdate + ' - ' +auth_enddate");
        //ddlauthorization.DataTextField = "AuthCode";
        //ddlauthorization.DataValueField = "auth_authorizationnumber";
        //ddlauthorization.DataSource = dt;
        //ddlauthorization.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}