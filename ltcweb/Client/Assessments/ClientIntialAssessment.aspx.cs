﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class Clients_ClientIntialAssessment : System.Web.UI.Page
{

    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();
    protected void Page_Load(object sender, EventArgs e)
    {
        Boolean isRcrdFound = clsASR.GetRecordByStepID(Convert.ToInt64(AssesmentStep.Value), (Guid)Membership.GetUser().ProviderUserKey);
        if (!isRcrdFound)
        {
            ucProfile.MemberKey = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        }
        else
        {
            nextstep();
        }
    }

    protected void cmdNext_Click(object sender, EventArgs e)
    {
        clsASR.storcrd_stepid = Convert.ToInt32(AssesmentStep.Value);
        clsASR.stprcrd_userid = (Guid)Membership.GetUser().ProviderUserKey;
        Boolean stprslt = clsASR.Save();
        if (stprslt)
        {
            nextstep();
        }
        else
        {
            Response.Redirect("~/Client/Assessments/ClientIntialAssessment.aspx");
            Label1.Text = "your step is not completed please click here again";
        }
    }

    public void nextstep()
    {
        Response.Redirect("~/Client/Assessments/UserFamilyOrigion.aspx");
    }
}