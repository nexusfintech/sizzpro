﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class Settings_WorkingHours : System.Web.UI.Page
{
    clsSettings clsSTG = new clsSettings();
    clsProfessionMaster clsPRO = new clsProfessionMaster();
    private Int64 intId = 0;

    private void BindCombo(Guid userid)
    {
        ddlProfession.DataTextField = "prf_name";
        ddlProfession.DataValueField = "prf_id";
        ddlProfession.DataSource = clsPRO.GetAllRecord();
        ddlProfession.DataBind();
        Boolean blnResult = clsSTG.GetRecordByFlagUserInProperties("PROFESSION", userid);
        if (blnResult == true)
        {
            ListItem liStatic = ddlProfession.Items.FindByValue(clsSTG.stg_intvalue.ToString());
            if (liStatic != null)
            {
                ddlProfession.SelectedIndex = ddlProfession.Items.IndexOf(liStatic);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Guid guUserid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            BindCombo(guUserid);
            Boolean blnResult = clsSTG.GetRecordByFlagUserInProperties("WHSTARTTIME", guUserid);
            if (blnResult == true)
            {
                txtStartTime.Text = clsSTG.stg_datetimevalue.ToString("hh:mm:ss tt", CultureInfo.InvariantCulture);
            }
            Boolean blnResult1 = clsSTG.GetRecordByFlagUserInProperties("WHENDTIME", guUserid);
            if (blnResult1 == true)
            {
                txtEndTime.Text = clsSTG.stg_datetimevalue.ToString("hh:mm:ss tt", CultureInfo.InvariantCulture);
            }
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Guid guUserid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        clsSTG.stg_settingflag = "WHSTARTTIME";
        DateTime dtStartTime = DateTime.Parse(txtStartTime.Text.ToUpper(), CultureInfo.InvariantCulture, DateTimeStyles.None);
        clsSTG.stg_datetimevalue = dtStartTime;
        clsSTG.Save(guUserid);
        clsSTG.stg_settingflag = "WHENDTIME";
        DateTime dtEndTime = DateTime.Parse(txtEndTime.Text.ToUpper(), CultureInfo.InvariantCulture, DateTimeStyles.None);
        clsSTG.stg_datetimevalue = dtEndTime;
        Boolean blnResult = clsSTG.Save(guUserid);
        clsSTG.stg_settingflag = "PROFESSION";
        clsSTG.stg_intvalue = Convert.ToInt64(ddlProfession.SelectedItem.Value);
        clsSTG.Save(guUserid);
        if (blnResult == true)
        { lblMessage.Text = "Save Successfully."; }
        else { lblMessage.Text = "Error in save settings.. " + clsSTG.GetSetErrorMessage; }
    }
}