﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class UserControl_Assessment_ucPhysicalWellBeing : System.Web.UI.UserControl
{
    clsInitialAssessment clsASS = new clsInitialAssessment();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();
    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    private void SetSelectedIndex(DropDownList ddl, Int64 intValue)
    {
        ListItem li = ddl.Items.FindByValue(intValue.ToString());
        if (li != null)
        {
            ddl.SelectedIndex = ddl.Items.IndexOf(li);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            EditEntry();
        }
    }

    private void EditEntry()
    {
        Guid objKey = new Guid(strMemberkey);
        clsASS.GetRecordUseridFlagInProperties(objKey, ddlaidsHiv.ClientID);
        SetSelectedIndex(ddlaidsHiv, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAlcoholism.ClientID);
        SetSelectedIndex(ddlAlcoholism, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAllergyShots.ClientID);
        SetSelectedIndex(ddlAllergyShots, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAnemia.ClientID);
        SetSelectedIndex(ddlAnemia, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAnorexia.ClientID);
        SetSelectedIndex(ddlAnorexia, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAppendicitis.ClientID);
        SetSelectedIndex(ddlAppendicitis, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlArthritis.ClientID);
        SetSelectedIndex(ddlArthritis, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAsthma.ClientID);
        SetSelectedIndex(ddlAsthma, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlBaddreams.ClientID);
        SetSelectedIndex(ddlBaddreams, clsASS.ini_valueint);
        

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlBleedingDisorders.ClientID);
        SetSelectedIndex(ddlBleedingDisorders, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlBloodSugarLevels.ClientID);
        SetSelectedIndex(ddlBloodSugarLevels, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlBreastLump.ClientID);
        SetSelectedIndex(ddlBreastLump, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlBronchitis.ClientID);
        SetSelectedIndex(ddlBronchitis, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlBulimia.ClientID);
        SetSelectedIndex(ddlBulimia, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlCancer.ClientID);
        SetSelectedIndex(ddlCancer, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlCataracts.ClientID);
        SetSelectedIndex(ddlCataracts, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlChemicalDependency.ClientID);
        SetSelectedIndex(ddlChemicalDependency, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlChickenPox.ClientID);
        SetSelectedIndex(ddlChickenPox, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlCholesterol.ClientID);
        SetSelectedIndex(ddlCholesterol, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlDiabetes.ClientID);
        SetSelectedIndex(ddlDiabetes, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlEmphysema.ClientID);
        SetSelectedIndex(ddlEmphysema, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlEpilepsy.ClientID);
        SetSelectedIndex(ddlEpilepsy, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey,ddlExercise.ClientID);
        SetSelectedIndex(ddlExercise, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlFear.ClientID);
        SetSelectedIndex(ddlFear, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlEpilepsy.ClientID);
        SetSelectedIndex(ddlEpilepsy, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlFractures.ClientID);
        SetSelectedIndex(ddlFractures, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlGlaucoma.ClientID);
        SetSelectedIndex(ddlGlaucoma, clsASS.ini_valueint);
        
        clsASS.GetRecordUseridFlagInProperties(objKey, ddlGoiter.ClientID);
        SetSelectedIndex(ddlGoiter, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlGonorrhea.ClientID);
        SetSelectedIndex(ddlGonorrhea, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlGout.ClientID);
        SetSelectedIndex(ddlGout, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlHeadaches.ClientID);
        SetSelectedIndex(ddlHeadaches, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlHeartDisease.ClientID);
        SetSelectedIndex(ddlHeartDisease, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlHepatitis.ClientID);
        SetSelectedIndex(ddlHepatitis, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlHernia.ClientID);
        SetSelectedIndex(ddlHernia, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey,ddlHerniatedDisk.ClientID);
        SetSelectedIndex(ddlHerniatedDisk, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlHerpes.ClientID);
        SetSelectedIndex(ddlHerpes, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlHighBloodPressures.ClientID);
        SetSelectedIndex(ddlHighBloodPressures, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlHighCholesterol.ClientID);
        SetSelectedIndex(ddlHighCholesterol, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlKidneyDisease.ClientID);
        SetSelectedIndex(ddlKidneyDisease, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlLiverDisease.ClientID);
        SetSelectedIndex(ddlLiverDisease, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlMeasles.ClientID);
        SetSelectedIndex(ddlMeasles, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlMigraineHeadaches.ClientID);
        SetSelectedIndex(ddlMigraineHeadaches, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlMiscarriage.ClientID);
        SetSelectedIndex(ddlMiscarriage, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlMultipleSclerosis.ClientID);
        SetSelectedIndex(ddlMultipleSclerosis, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlOsteoporosis.ClientID);
        SetSelectedIndex(ddlOsteoporosis, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlPacemaker.ClientID);
        SetSelectedIndex(ddlPacemaker, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlParkinsonDisease.ClientID);
        SetSelectedIndex(ddlParkinsonDisease, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlPinchedNerve.ClientID);
        SetSelectedIndex(ddlPinchedNerve, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlPneumonia.ClientID);
        SetSelectedIndex(ddlPneumonia, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlPolio.ClientID);
        SetSelectedIndex(ddlPolio, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlProstateProblem.ClientID);
        SetSelectedIndex(ddlProstateProblem, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlProsthesis.ClientID);
        SetSelectedIndex(ddlProsthesis, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlPsychiatricCare.ClientID);
        SetSelectedIndex(ddlPsychiatricCare, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlRheumaticFever.ClientID);
        SetSelectedIndex(ddlRheumaticFever, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlRheumatoidArthritis.ClientID);
        SetSelectedIndex(ddlRheumatoidArthritis, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlScarletFever.ClientID);
        SetSelectedIndex(ddlScarletFever, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlSmoking.ClientID);
        SetSelectedIndex(ddlSmoking, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlSTD.ClientID);
        SetSelectedIndex(ddlSTD, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlStroke.ClientID);
        SetSelectedIndex(ddlStroke, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlSuicideAttempt.ClientID);
        SetSelectedIndex(ddlSuicideAttempt, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlThyroidProblems.ClientID);
        SetSelectedIndex(ddlThyroidProblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlTonsillitis.ClientID);
        SetSelectedIndex(ddlTonsillitis, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlTuberculosis.ClientID);
        SetSelectedIndex(ddlTuberculosis, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlTumorsGrowths.ClientID);
        SetSelectedIndex(ddlTumorsGrowths, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlTyphoidFever.ClientID);
        SetSelectedIndex(ddlTyphoidFever, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlUlcers.ClientID);
        SetSelectedIndex(ddlUlcers, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlVaginalInfections.ClientID);
        SetSelectedIndex(ddlVaginalInfections, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlWhoopingCough.ClientID);
        SetSelectedIndex(ddlWhoopingCough, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlWorkActivity.ClientID);
        SetSelectedIndex(ddlWorkActivity, clsASS.ini_valueint);
    }

   
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Boolean blnSave = false;
            Guid objKey = new Guid(strMemberkey);

            clsASS.ini_userid = objKey;

            clsASS.ini_valueint = Convert.ToInt64(ddlaidsHiv.SelectedValue);
            clsASS.ini_valueflag = ddlaidsHiv.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAlcoholism.SelectedValue);
            clsASS.ini_valueflag = ddlAlcoholism.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAllergyShots.SelectedValue);
            clsASS.ini_valueflag = ddlAllergyShots.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAnemia.SelectedValue);
            clsASS.ini_valueflag = ddlAnemia.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAnorexia.SelectedValue);
            clsASS.ini_valueflag = ddlAnorexia.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAppendicitis.SelectedValue);
            clsASS.ini_valueflag = ddlAppendicitis.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlArthritis.SelectedValue);
            clsASS.ini_valueflag = ddlArthritis.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAsthma.SelectedValue);
            clsASS.ini_valueflag = ddlAsthma.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlBaddreams.SelectedValue);
            clsASS.ini_valueflag = ddlBaddreams.ClientID;
            blnSave = clsASS.Save();


            clsASS.ini_valueint = Convert.ToInt64(ddlBleedingDisorders.SelectedValue);
            clsASS.ini_valueflag = ddlBleedingDisorders.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlBloodSugarLevels.SelectedValue);
            clsASS.ini_valueflag = ddlBloodSugarLevels.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlBreastLump.SelectedValue);
            clsASS.ini_valueflag = ddlBreastLump.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlBronchitis.SelectedValue);
            clsASS.ini_valueflag = ddlBronchitis.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlBulimia.SelectedValue);
            clsASS.ini_valueflag = ddlBulimia.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlCancer.SelectedValue);
            clsASS.ini_valueflag = ddlCancer.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlCataracts.SelectedValue);
            clsASS.ini_valueflag = ddlCataracts.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlChemicalDependency.SelectedValue);
            clsASS.ini_valueflag = ddlChemicalDependency.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlChickenPox.SelectedValue);
            clsASS.ini_valueflag = ddlChickenPox.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlCholesterol.SelectedValue);
            clsASS.ini_valueflag = ddlCholesterol.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlDiabetes.SelectedValue);
            clsASS.ini_valueflag = ddlDiabetes.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlEmphysema.SelectedValue);
            clsASS.ini_valueflag = ddlEmphysema.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlEpilepsy.SelectedValue);
            clsASS.ini_valueflag = ddlEpilepsy.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlExercise.SelectedValue);
            clsASS.ini_valueflag = ddlExercise.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlEpilepsy.SelectedValue);
            clsASS.ini_valueflag = ddlEpilepsy.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlFractures.SelectedValue);
            clsASS.ini_valueflag = ddlFractures.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlFear.SelectedValue);
            clsASS.ini_valueflag = ddlFear.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlGlaucoma.SelectedValue);
            clsASS.ini_valueflag = ddlGlaucoma.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlGoiter.SelectedValue);
            clsASS.ini_valueflag = ddlGoiter.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlGonorrhea.SelectedValue);
            clsASS.ini_valueflag = ddlGonorrhea.ClientID;
            blnSave = clsASS.Save();


            clsASS.ini_valueint = Convert.ToInt64(ddlGout.SelectedValue);
            clsASS.ini_valueflag = ddlGout.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlHeartDisease.SelectedValue);
            clsASS.ini_valueflag = ddlHeartDisease.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlHepatitis.SelectedValue);
            clsASS.ini_valueflag = ddlHepatitis.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlHernia.SelectedValue);
            clsASS.ini_valueflag = ddlHernia.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlHerniatedDisk.SelectedValue);
            clsASS.ini_valueflag = ddlHerniatedDisk.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlHerpes.SelectedValue);
            clsASS.ini_valueflag = ddlHerpes.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlHighBloodPressures.SelectedValue);
            clsASS.ini_valueflag = ddlHighBloodPressures.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlHighCholesterol.SelectedValue);
            clsASS.ini_valueflag = ddlHighCholesterol.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlKidneyDisease.SelectedValue);
            clsASS.ini_valueflag = ddlKidneyDisease.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlNervousness.SelectedValue);
            clsASS.ini_valueflag = ddlNervousness.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlLiverDisease.SelectedValue);
            clsASS.ini_valueflag = ddlLiverDisease.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlMeasles.SelectedValue);
            clsASS.ini_valueflag = ddlMeasles.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlMigraineHeadaches.SelectedValue);
            clsASS.ini_valueflag = ddlMigraineHeadaches.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlMiscarriage.SelectedValue);
            clsASS.ini_valueflag = ddlMiscarriage.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlMultipleSclerosis.SelectedValue);
            clsASS.ini_valueflag = ddlMultipleSclerosis.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlNervousness.SelectedValue);
            clsASS.ini_valueflag = ddlNervousness.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlOsteoporosis.SelectedValue);
            clsASS.ini_valueflag = ddlOsteoporosis.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlPacemaker.SelectedValue);
            clsASS.ini_valueflag = ddlPacemaker.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlParkinsonDisease.SelectedValue);
            clsASS.ini_valueflag = ddlParkinsonDisease.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlPinchedNerve.SelectedValue);
            clsASS.ini_valueflag = ddlPinchedNerve.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlPneumonia.SelectedValue);
            clsASS.ini_valueflag = ddlPneumonia.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlPolio.SelectedValue);
            clsASS.ini_valueflag = ddlPolio.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlProstateProblem.SelectedValue);
            clsASS.ini_valueflag = ddlProstateProblem.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlProsthesis.SelectedValue);
            clsASS.ini_valueflag = ddlProsthesis.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlPsychiatricCare.SelectedValue);
            clsASS.ini_valueflag = ddlPsychiatricCare.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlRheumaticFever.SelectedValue);
            clsASS.ini_valueflag = ddlRheumaticFever.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlRheumatoidArthritis.SelectedValue);
            clsASS.ini_valueflag = ddlRheumatoidArthritis.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlScarletFever.SelectedValue);
            clsASS.ini_valueflag = ddlScarletFever.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlSTD.SelectedValue);
            clsASS.ini_valueflag = ddlSTD.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlStroke.SelectedValue);
            clsASS.ini_valueflag = ddlStroke.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlSuicideAttempt.SelectedValue);
            clsASS.ini_valueflag = ddlSuicideAttempt.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlThyroidProblems.SelectedValue);
            clsASS.ini_valueflag = ddlThyroidProblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlTonsillitis.SelectedValue);
            clsASS.ini_valueflag = ddlTonsillitis.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlTuberculosis.SelectedValue);
            clsASS.ini_valueflag = ddlTuberculosis.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlTumorsGrowths.SelectedValue);
            clsASS.ini_valueflag = ddlTumorsGrowths.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlTyphoidFever.SelectedValue);
            clsASS.ini_valueflag = ddlTyphoidFever.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlUlcers.SelectedValue);
            clsASS.ini_valueflag = ddlUlcers.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlVaginalInfections.SelectedValue);
            clsASS.ini_valueflag = ddlVaginalInfections.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlWhoopingCough.SelectedValue);
            clsASS.ini_valueflag = ddlWhoopingCough.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlWorkActivity.SelectedValue);
            clsASS.ini_valueflag = ddlWorkActivity.ClientID;
            blnSave = clsASS.Save();

            clsASR.storcrd_stepid = 12;
            clsASR.stprcrd_userid = objKey;
            Boolean stprslt = clsASR.Save();
            if (stprslt == true)
            {
                Response.Redirect("~/Client/Assessments/PhysicalSymptoms.aspx");
            }
            lblMessage.Text = "Save Successfully.";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error in saving " + clsASS.ini_valueflag + " process please try again... " + ex.Message + Environment.NewLine + ex.StackTrace;
        }

    }

}