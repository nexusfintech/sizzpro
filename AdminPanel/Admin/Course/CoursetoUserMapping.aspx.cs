﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;

public partial class Admin_Course_CoursetoUserMapping : System.Web.UI.Page
{
    clsUsers clsUser = new clsUsers();
    clsCourseMapping clsCRM = new clsCourseMapping();
    clsClientParentMapping clsCPM = new clsClientParentMapping();

    private void BindCombo(Guid UserId)
    {
        ddlUSer.Items.Clear();
        ddlUSer.SelectedIndex = -1;
        ddlUSer.SelectedValue = null;
        ddlUSer.ClearSelection();
        ddlUSer.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlUSer.Items.Add(lst);
        ddlUSer.DataValueField = "UserId";
        ddlUSer.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRoleandParent("user", UserId);
        ddlUSer.DataSource = dtResult;
        ddlUSer.DataBind();
    }

    private void BindListView(Guid guUserID,Guid MemId)
    {
        DataTable dtleft = clsCRM.GetAllRecordBYUseridNotMapped(guUserID,MemId);
        DataTable dtright = clsCRM.GetAllRecordBYUserid(guUserID,MemId);

        RptrLeft.DataSource = dtleft;
        RptrLeft.DataBind();

        RptrRight.DataSource = dtright;
        RptrRight.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //BindCombo();
            if (User.IsInRole("administrator"))
            {
                BindMemberCombo();
            }
            else
            {
                BindCombo((Guid)Membership.GetUser().ProviderUserKey);
            }
        }
    }

    protected void cmdSelect_Click(object sender, EventArgs e)
    {
        Session["userid"] = ddlUSer.SelectedValue.ToString();
        Guid guUser_id = new Guid(Session["userid"].ToString());
        hdUserid.Value = ddlUSer.SelectedValue.ToString();
        lblBoxHeader.Text = "( " + ddlUSer.SelectedItem.Text + " ) User Selected";
        pnlMapping.Visible = true;
        if (User.IsInRole("administrator"))
        {
            BindListView(guUser_id,new Guid(ddlSelectMember.SelectedValue.ToString()));
        }
        else
        {
            BindListView(guUser_id,(Guid)Membership.GetUser().ProviderUserKey);
        }
    }

    private void BindMemberCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "UserId";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRole("Member");
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }
    
    protected void ddlSelectMember_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCombo(new Guid(ddlSelectMember.SelectedValue.ToString()));
    }
}