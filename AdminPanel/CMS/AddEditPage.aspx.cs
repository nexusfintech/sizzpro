﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class CMS_AddEditPage : System.Web.UI.Page
{
    clsContents clscntn = new clsContents();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if(MySession.Current.AddEditFlag == "EDIT")
            {
                binddata();
            }
        }
    }

    private String ValidatePage()
    {
        string StrMsg = string.Empty;
        if(txtUrl.Text == string.Empty)
        {
            StrMsg += "Page Url Required </br>";
        }
        if (txtDescription.Text == string.Empty)
        {
            StrMsg += "Page Content Required </br>";
        }
        return StrMsg;
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string pagevalidation = ValidatePage();
        if(pagevalidation != string .Empty)
        {
            lblmessage.Text = pagevalidation;
            return;
        }
        clscntn.cnt_key = txtUrl.Text.ToString();
        clscntn.cnt_html = txtDescription.Text.ToString();
        clscntn.cnt_title = txtTitle.Text.ToString();
        clscntn.cnt_metakeywords = txtMetaKey.Text.ToString();
        clscntn.cnt_metadescription = txtMetaDesc.Text.ToString();
        bool blnResult = false;
        if (MySession.Current.AddEditFlag == "EDIT")
        {
            blnResult = clscntn.Update(MySession.Current.EditID);
        }
        else
        {
            blnResult = clscntn.Save();
        }
        if(blnResult == true)
        {
            Response.Redirect("~/CMS/MyPages.aspx");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/CMS/MyPages.aspx");
    }

    private void binddata()
    {
        Boolean blnReslt = clscntn.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnReslt == true)
        {
            txtDescription.Text = clscntn.cnt_html;
            txtTitle.Text = clscntn.cnt_title;
            txtUrl.Text = clscntn.cnt_key;
            txtMetaDesc.Text = clscntn.cnt_metadescription;
            txtMetaKey.Text = clscntn.cnt_metakeywords;
        }
        else
        {
            Response.Redirect("~/CMS/MyPages.aspx");
        }
    }
}