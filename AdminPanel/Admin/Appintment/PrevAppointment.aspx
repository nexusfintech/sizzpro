﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PrevAppointment.aspx.cs" Inherits="Admin_Appintment_PrevAppointment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a href="MyAppointment.aspx">
                    <i class="icon-calendar"></i>&nbsp; My Appointment
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h3><i class="icon-time"></i>&nbsp; Previous Appointment</h3>
            <h5>
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label></h5>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="glyphicon-calendar"></i>
                        Previous Appointments
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-column form-bordered'>
                        <div id="calendar">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hduserid" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $(function () {
            var calendar = $('#calendar').fullCalendar({
                theme: false,
                header: { left: 'prev,next today', center: 'title', right: 'month,agendaWeek,agendaDay' },
                selectable: true,
                selectHelper: true,
                editable: true,
                events: "../../WebServices/PrevCalendarBind.ashx?userid=<%= hduserid.Value %>",
                eventRender: function (event, element) {
                    //alert(event.title);
                    //element.qtip({
                    //    content: event.title,
                    //    position: { corner: { tooltip: 'bottomLeft', target: 'topRight' } },
                    //    style: {
                    //        border: {
                    //            width: 1,
                    //            radius: 3,
                    //            color: '#2779AA'
                    //        },
                    //        padding: 10,
                    //        textAlign: 'center',
                    //        tip: true, // Give it a speech bubble tip with automatic corner detection
                    //        name: 'cream' // Style it according to the preset 'cream' style
                    //    }

                    //});
                }
            });
        });
    </script>
</asp:Content>

