﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class UserControl_Assessment_ucAssessmentsymptoms : System.Web.UI.UserControl
{
    clsInitialAssessment clsASS = new clsInitialAssessment();
    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    private void SetSelectedIndex(DropDownList ddl,Int64 intValue)
    {
        ListItem li = ddl.Items.FindByValue(intValue.ToString());
        if (li != null)
        {
            ddl.SelectedIndex = ddl.Items.IndexOf(li);
        }
    }

    private void EditEntry()
    {
        Guid objKey = new Guid(strMemberkey);
        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAbortion.ClientID);
        SetSelectedIndex(ddlAbortion, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAccident.ClientID);
        SetSelectedIndex(ddlAccident, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlActingImpulsively.ClientID);
        SetSelectedIndex(ddlActingImpulsively, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAggressivebehavior.ClientID);
        SetSelectedIndex(ddlAggressivebehavior, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAlcoholproblems.ClientID);
        SetSelectedIndex(ddlAlcoholproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAmbition.ClientID);
        SetSelectedIndex(ddlAmbition, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAnger.ClientID);
        SetSelectedIndex(ddlAnger, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAnxiety.ClientID);
        SetSelectedIndex(ddlAnxiety, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlAppetite.ClientID);
        SetSelectedIndex(ddlAppetite, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlBaddreams.ClientID);
        SetSelectedIndex(ddlBaddreams, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlBadIllness.ClientID);
        SetSelectedIndex(ddlBadIllness, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlCantkeepajob.ClientID);
        SetSelectedIndex(ddlCantkeepajob, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlCareerproblems.ClientID);
        SetSelectedIndex(ddlCareerproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlChildren.ClientID);
        SetSelectedIndex(ddlChildren, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlCompulsionorobsession.ClientID);
        SetSelectedIndex(ddlCompulsionorobsession, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlConcentrationdifficulties.ClientID);
        SetSelectedIndex(ddlConcentrationdifficulties, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlddlAbusiverelationship.ClientID);
        SetSelectedIndex(ddlddlAbusiverelationship, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlddlDepression.ClientID);
        SetSelectedIndex(ddlddlDepression, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlddlDepression.ClientID);
        SetSelectedIndex(ddlddlDepression, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlDeployment.ClientID);
        SetSelectedIndex(ddlDeployment, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlDifficultymakingdecisions.ClientID);
        SetSelectedIndex(ddlDifficultymakingdecisions, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlDivorce.ClientID);
        SetSelectedIndex(ddlDivorce, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlDrinkingtoomuch.ClientID);
        SetSelectedIndex(ddlDrinkingtoomuch, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlDrugproblems.ClientID);
        SetSelectedIndex(ddlDrugproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlEatingproblems.ClientID);
        SetSelectedIndex(ddlEatingproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlEpilepsy.ClientID);
        SetSelectedIndex(ddlEpilepsy, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlEvilthoughts.ClientID);
        SetSelectedIndex(ddlEvilthoughts, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlFamilyproblems.ClientID);
        SetSelectedIndex(ddlFamilyproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlFear.ClientID);
        SetSelectedIndex(ddlFear, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlFinancialproblems.ClientID);
        SetSelectedIndex(ddlFinancialproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlFriends.ClientID);
        SetSelectedIndex(ddlFriends, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlGuilt.ClientID);
        SetSelectedIndex(ddlGuilt, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlHeadaches.ClientID);
        SetSelectedIndex(ddlHeadaches, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlHighbloodpressure.ClientID);
        SetSelectedIndex(ddlHighbloodpressure, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlInferiorityfeelings.ClientID);
        SetSelectedIndex(ddlInferiorityfeelings, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlLazy.ClientID);
        SetSelectedIndex(ddlLazy, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlLegalproblems.ClientID);
        SetSelectedIndex(ddlLegalproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlLonely.ClientID);
        SetSelectedIndex(ddlLonely, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlLossofcontrol.ClientID);
        SetSelectedIndex(ddlLossofcontrol, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlMaritaldifficulties.ClientID);
        SetSelectedIndex(ddlMaritaldifficulties, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlMemory.ClientID);
        SetSelectedIndex(ddlMemory, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlMentalillness.ClientID);
        SetSelectedIndex(ddlMentalillness, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlNausea.ClientID);
        SetSelectedIndex(ddlNausea, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlNervousness.ClientID);
        SetSelectedIndex(ddlNervousness, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlNervoustic.ClientID);
        SetSelectedIndex(ddlNervoustic, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlOddbehavior.ClientID);
        SetSelectedIndex(ddlOddbehavior, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlOutburstsoftemper.ClientID);
        SetSelectedIndex(ddlOutburstsoftemper, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlOvereating.ClientID);
        SetSelectedIndex(ddlOvereating, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlParentingproblems.ClientID);
        SetSelectedIndex(ddlParentingproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlPhobicavoidance.ClientID);
        SetSelectedIndex(ddlPhobicavoidance, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlProcrastinating.ClientID);
        SetSelectedIndex(ddlProcrastinating, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlSchoolproblems.ClientID);
        SetSelectedIndex(ddlSchoolproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlSelfesteemconfidence.ClientID);
        SetSelectedIndex(ddlSelfesteemconfidence, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlSexualproblems.ClientID);
        SetSelectedIndex(ddlSexualproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlShyness.ClientID);
        SetSelectedIndex(ddlShyness, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlSleepDisturbance.ClientID);
        SetSelectedIndex(ddlSleepDisturbance, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlSmoking.ClientID);
        SetSelectedIndex(ddlSmoking, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlStomachproblems.ClientID);
        SetSelectedIndex(ddlStomachproblems, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlStrangeorunusualsensations.ClientID);
        SetSelectedIndex(ddlStrangeorunusualsensations, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlStressedout.ClientID);
        SetSelectedIndex(ddlStressedout, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlSuicidethoughtsattempts.ClientID);
        SetSelectedIndex(ddlSuicidethoughtsattempts, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlTakingdrugs.ClientID);
        SetSelectedIndex(ddlTakingdrugs, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlTakingtoomanyrisks.ClientID);
        SetSelectedIndex(ddlTakingtoomanyrisks, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlTirednessLessEnergy.ClientID);
        SetSelectedIndex(ddlTirednessLessEnergy, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlUnhappiness.ClientID);
        SetSelectedIndex(ddlUnhappiness, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlUnusualphysicalsymptoms.ClientID);
        SetSelectedIndex(ddlUnusualphysicalsymptoms, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlVictimofaCrime.ClientID);
        SetSelectedIndex(ddlVictimofaCrime, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlVomiting.ClientID);
        SetSelectedIndex(ddlVomiting, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlWithdrawal.ClientID);
        SetSelectedIndex(ddlWithdrawal, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlWorkdissatisfaction.ClientID);
        SetSelectedIndex(ddlWorkdissatisfaction, clsASS.ini_valueint);

        clsASS.GetRecordUseridFlagInProperties(objKey, ddlWorkingtoohard.ClientID);
        SetSelectedIndex(ddlWorkingtoohard, clsASS.ini_valueint);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            EditEntry();
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Boolean blnSave = false;
            Guid objKey = new Guid(strMemberkey);
            clsASS.ini_userid = objKey;

            clsASS.ini_valueint = Convert.ToInt64(ddlAbortion.SelectedValue);
            clsASS.ini_valueflag = ddlAbortion.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAccident.SelectedValue);
            clsASS.ini_valueflag = ddlAccident.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlActingImpulsively.SelectedValue);
            clsASS.ini_valueflag = ddlActingImpulsively.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAggressivebehavior.SelectedValue);
            clsASS.ini_valueflag = ddlAggressivebehavior.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAlcoholproblems.SelectedValue);
            clsASS.ini_valueflag = ddlAlcoholproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAmbition.SelectedValue);
            clsASS.ini_valueflag = ddlAmbition.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAnger.SelectedValue);
            clsASS.ini_valueflag = ddlAnger.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAnxiety.SelectedValue);
            clsASS.ini_valueflag = ddlAnxiety.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlAppetite.SelectedValue);
            clsASS.ini_valueflag = ddlAppetite.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlBaddreams.SelectedValue);
            clsASS.ini_valueflag = ddlBaddreams.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlBadIllness.SelectedValue);
            clsASS.ini_valueflag = ddlBadIllness.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlCantkeepajob.SelectedValue);
            clsASS.ini_valueflag = ddlCantkeepajob.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlCareerproblems.SelectedValue);
            clsASS.ini_valueflag = ddlCareerproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlChildren.SelectedValue);
            clsASS.ini_valueflag = ddlChildren.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlCompulsionorobsession.SelectedValue);
            clsASS.ini_valueflag = ddlCompulsionorobsession.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlConcentrationdifficulties.SelectedValue);
            clsASS.ini_valueflag = ddlConcentrationdifficulties.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlddlAbusiverelationship.SelectedValue);
            clsASS.ini_valueflag = ddlddlAbusiverelationship.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlddlDepression.SelectedValue);
            clsASS.ini_valueflag = ddlddlDepression.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlDeployment.SelectedValue);
            clsASS.ini_valueflag = ddlDeployment.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlDifficultymakingdecisions.SelectedValue);
            clsASS.ini_valueflag = ddlDifficultymakingdecisions.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlDivorce.SelectedValue);
            clsASS.ini_valueflag = ddlDivorce.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlDrinkingtoomuch.SelectedValue);
            clsASS.ini_valueflag = ddlDrinkingtoomuch.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlDrugproblems.SelectedValue);
            clsASS.ini_valueflag = ddlDrugproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlEatingproblems.SelectedValue);
            clsASS.ini_valueflag = ddlEatingproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlEpilepsy.SelectedValue);
            clsASS.ini_valueflag = ddlEpilepsy.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlEvilthoughts.SelectedValue);
            clsASS.ini_valueflag = ddlEvilthoughts.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlFamilyproblems.SelectedValue);
            clsASS.ini_valueflag = ddlFamilyproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlFear.SelectedValue);
            clsASS.ini_valueflag = ddlFear.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlFinancialproblems.SelectedValue);
            clsASS.ini_valueflag = ddlFinancialproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlFriends.SelectedValue);
            clsASS.ini_valueflag = ddlFriends.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlGuilt.SelectedValue);
            clsASS.ini_valueflag = ddlGuilt.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlHeadaches.SelectedValue);
            clsASS.ini_valueflag = ddlHeadaches.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlHighbloodpressure.SelectedValue);
            clsASS.ini_valueflag = ddlHighbloodpressure.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlInferiorityfeelings.SelectedValue);
            clsASS.ini_valueflag = ddlInferiorityfeelings.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlLazy.SelectedValue);
            clsASS.ini_valueflag = ddlLazy.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlLegalproblems.SelectedValue);
            clsASS.ini_valueflag = ddlLegalproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlLonely.SelectedValue);
            clsASS.ini_valueflag = ddlLonely.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlLossofcontrol.SelectedValue);
            clsASS.ini_valueflag = ddlLossofcontrol.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlMaritaldifficulties.SelectedValue);
            clsASS.ini_valueflag = ddlMaritaldifficulties.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlMemory.SelectedValue);
            clsASS.ini_valueflag = ddlMemory.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlMentalillness.SelectedValue);
            clsASS.ini_valueflag = ddlMentalillness.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlNausea.SelectedValue);
            clsASS.ini_valueflag = ddlNausea.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlNervousness.SelectedValue);
            clsASS.ini_valueflag = ddlNervousness.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlNervoustic.SelectedValue);
            clsASS.ini_valueflag = ddlNervoustic.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlOddbehavior.SelectedValue);
            clsASS.ini_valueflag = ddlOddbehavior.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlOutburstsoftemper.SelectedValue);
            clsASS.ini_valueflag = ddlOutburstsoftemper.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlOvereating.SelectedValue);
            clsASS.ini_valueflag = ddlOvereating.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlParentingproblems.SelectedValue);
            clsASS.ini_valueflag = ddlParentingproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlPhobicavoidance.SelectedValue);
            clsASS.ini_valueflag = ddlPhobicavoidance.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlProcrastinating.SelectedValue);
            clsASS.ini_valueflag = ddlProcrastinating.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlSchoolproblems.SelectedValue);
            clsASS.ini_valueflag = ddlSchoolproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlSelfesteemconfidence.SelectedValue);
            clsASS.ini_valueflag = ddlSelfesteemconfidence.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlSexualproblems.SelectedValue);
            clsASS.ini_valueflag = ddlSexualproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlShyness.SelectedValue);
            clsASS.ini_valueflag = ddlShyness.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlSleepDisturbance.SelectedValue);
            clsASS.ini_valueflag = ddlSleepDisturbance.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlSmoking.SelectedValue);
            clsASS.ini_valueflag = ddlSmoking.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlStomachproblems.SelectedValue);
            clsASS.ini_valueflag = ddlStomachproblems.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlStrangeorunusualsensations.SelectedValue);
            clsASS.ini_valueflag = ddlStrangeorunusualsensations.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlStressedout.SelectedValue);
            clsASS.ini_valueflag = ddlStressedout.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlSuicidethoughtsattempts.SelectedValue);
            clsASS.ini_valueflag = ddlSuicidethoughtsattempts.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlTakingdrugs.SelectedValue);
            clsASS.ini_valueflag = ddlTakingdrugs.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlTakingtoomanyrisks.SelectedValue);
            clsASS.ini_valueflag = ddlTakingtoomanyrisks.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlTirednessLessEnergy.SelectedValue);
            clsASS.ini_valueflag = ddlTirednessLessEnergy.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlUnhappiness.SelectedValue);
            clsASS.ini_valueflag = ddlUnhappiness.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlUnusualphysicalsymptoms.SelectedValue);
            clsASS.ini_valueflag = ddlUnusualphysicalsymptoms.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlVictimofaCrime.SelectedValue);
            clsASS.ini_valueflag = ddlVictimofaCrime.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlVomiting.SelectedValue);
            clsASS.ini_valueflag = ddlVomiting.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlWithdrawal.SelectedValue);
            clsASS.ini_valueflag = ddlWithdrawal.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlWorkdissatisfaction.SelectedValue);
            clsASS.ini_valueflag = ddlWorkdissatisfaction.ClientID;
            blnSave = clsASS.Save();

            clsASS.ini_valueint = Convert.ToInt64(ddlWorkingtoohard.SelectedValue);
            clsASS.ini_valueflag = ddlWorkingtoohard.ClientID;
            blnSave = clsASS.Save();
            lblMessage.Text = "Save Successfully.";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error in saving " + clsASS.ini_valueflag + " process please try again... " + ex.Message; 
        }
    }
}