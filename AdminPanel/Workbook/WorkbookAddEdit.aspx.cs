﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;
using System.Web.Security;

public partial class Workbook_WorkbookAddEdit : System.Web.UI.Page
{
    clsWorkBook clsWB = new clsWorkBook();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsMembership mem = new clsMembership();
    clsobjectsharing clsOBS = new clsobjectsharing();
    clsObjectPricing clsOBP = new clsObjectPricing();

    private void EditEntry()
    {
        Boolean blnResult = clsWB.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnResult == true)
        {
            txtDescription.Text = clsWB.wrb_description;
            txtworkbookname.Text = clsWB.wrb_name;
            txtshort.Text = clsWB.wrb_short;
            ListItem liInterval = ddlSelectMember.Items.FindByValue(clsWB.wrb_UserId.ToString());
            if (liInterval != null)
            {
                ddlSelectMember.SelectedIndex = ddlSelectMember.Items.IndexOf(liInterval);
            }
            wrkcd.Value = clsWB.wrb_code.ToString();
            wrksts.Value = clsWB.wrb_sts.ToString();
            blnResult = clsOBP.GetRecordByObject(clsWB.wrb_id);
            if (blnResult)
            {
                txtunlockedprice.Text = clsOBP.op_unlockedprice.ToString();
                txtlockedprice.Text = clsOBP.op_lockedprice.ToString();
            }
        }
        else { Response.Redirect("~/Workbook/WorkbookMaster.aspx"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (MySession.Current.AddEditFlag == "null")
            {
                Response.Redirect("~/Workbook/WorkbookMaster.aspx");
            }
            if (User.IsInRole("administrator"))
            {
                BindCombo();
            }
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                EditEntry();
            }
        }
    }

    private void BindCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllCombo();
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }

    private string PageValidation()
    {
        string strResult = "";
        if (txtworkbookname.Text.Trim() == "")
        { strResult += "Please Enter Workbook Name <br/>"; return strResult; }
        if (txtshort.Text.Trim() == "")
        { strResult += "Please Enter Workbook Short <br/>"; return strResult; }
        return strResult;
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Boolean blnSave = false;
        string strResult = PageValidation();
        if (strResult != "")
        { lblmessage.Text = strResult; return; }
        clsWB.wrb_description = txtDescription.Text;
        clsWB.wrb_name = txtworkbookname.Text;
        clsWB.wrb_short = txtshort.Text;
        if (User.IsInRole("administrator"))
        {
            clsWB.wrb_UserId = new Guid(ddlSelectMember.SelectedValue.ToString());
        }
        else
        {
            clsWB.wrb_UserId = (Guid)Membership.GetUser().ProviderUserKey;
        }
        if (txtlockedprice.Text != null)
        {
            clsOBP.op_lockedprice = Convert.ToInt64(txtlockedprice.Text);
        }
        if (txtunlockedprice.Text != null)
        {
            clsOBP.op_unlockedprice = Convert.ToInt64(txtunlockedprice.Text);
        }
        clsWB.wrb_OwnerId = mem.SetOwner(clsWB.wrb_UserId);
        if (MySession.Current.AddEditFlag == "ADD")
        {
            clsWB.wrb_code = Guid.NewGuid();
            blnSave = clsWB.Save();
            if (blnSave == true)
            {
                clsWB.GetRecordByCodeInProperties(clsWB.wrb_code);
                clsOBS.shr_sharedobjid = clsWB.wrb_id;
                clsOBS.shr_object = "WORKBOOK";
                clsOBS.shr_ownerid = clsWB.wrb_UserId;
                clsOBS.shr_sharedwithid = clsWB.wrb_UserId;
                blnSave = clsOBS.Save();

                clsOBP.op_object = "WORKBOOK";
                clsOBP.op_objid = clsWB.wrb_id;
                blnSave = clsOBP.Save();
            }
        }
        else
        {
            clsWB.wrb_code = new Guid(wrkcd.Value);
            clsWB.wrb_sts = Convert.ToBoolean(wrksts.Value);
            blnSave = clsWB.Update(MySession.Current.EditID);

            clsOBP.op_object = "WORKBOOK";
            clsOBP.op_objid = clsWB.wrb_id;
            blnSave = clsOBP.UpdateByobjId(clsOBP.op_objid);
        }
        if (blnSave == true)
        {
            Response.Redirect("~/Workbook/WorkbookMaster.aspx");
        }
        else
        {
            lblmessage.Text = "Error in save ..." + clsOBP.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    public void clear()
    {
        txtDescription.Text = string.Empty;
        txtshort.Text = string.Empty;
        txtworkbookname.Text = string.Empty;
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Workbook/WorkbookMaster.aspx");
    }
}