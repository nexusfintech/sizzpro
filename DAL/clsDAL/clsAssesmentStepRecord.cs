﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clsDAL
{
    public class clsAssesmentStepRecord
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsAssesmentStepRecord()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsAssesmentStepRecord()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _stprecord_id; }
            set
            {
                _stprecord_id = value;
                if (clsCMD.Parameters.Contains("@stprecord_id"))
                { clsCMD.Parameters["@stprecord_id"].Value = value; }
                else { clsCMD.Parameters.Add("@stprecord_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _stprecord_id;
        public Int64 stprecord_id
        {
            get { return _stprecord_id; }
            set
            {
                _stprecord_id = value;
                if (clsCMD.Parameters.Contains("@stprecord_id"))
                { clsCMD.Parameters["@stprecord_id"].Value = value; }
                else { clsCMD.Parameters.Add("@stprecord_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _stprcrd_userid;
        public Guid stprcrd_userid
        {
            get { return _stprcrd_userid; }
            set
            {
                _stprcrd_userid = value;
                if (clsCMD.Parameters.Contains("@stprcrd_userid"))
                { clsCMD.Parameters["@stprcrd_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@stprcrd_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _storcrd_stepid;
        public Int64 storcrd_stepid
        {
            get { return _storcrd_stepid; }
            set
            {
                _storcrd_stepid = value;
                if (clsCMD.Parameters.Contains("@storcrd_stepid"))
                { clsCMD.Parameters["@storcrd_stepid"].Value = value; }
                else { clsCMD.Parameters.Add("@storcrd_stepid", SqlDbType.BigInt).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                stprecord_id = Convert.ToInt64(dr["stprecord_id"]);
                stprcrd_userid = new Guid(dr["stprcrd_userid"].ToString());
                storcrd_stepid = Convert.ToInt64(dr["storcrd_stepid"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            stprecord_id = Convert.ToInt64("0");
            stprcrd_userid = new Guid();
            storcrd_stepid = Convert.ToInt64("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.stprecord_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_tbl_AssesmentStepRecordAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 stprecord_id)
        {
            Boolean blnResult = false;
            this.stprecord_id = stprecord_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_tbl_AssesmentStepRecordAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByStepID(Int64 storcrd_stepid,Guid userid)
        {
            Boolean blnResult = false;
            this.storcrd_stepid = storcrd_stepid;
            this.stprcrd_userid = userid;
            this.AddEditDeleteFlag = "DELETEBYID";
            try
            {
                clsCMD.CommandText = "sp_tbl_AssesmentStepRecordAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Assesment Rescheduled Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_tbl_AssesmentStepRecordGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsAssesmentStepRecord_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsAssesmentStepRecord_PropertiesList> lstResult = new List<clsAssesmentStepRecord_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].stprecord_id = Convert.ToInt64(dr["stprecord_id"]);
                lstResult[i].stprcrd_userid = new Guid(dr["stprcrd_userid"].ToString());
                lstResult[i].storcrd_stepid = Convert.ToInt64(dr["storcrd_stepid"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 stprecord_id)
        {
            Boolean blnResult = false;
            try
            {
                this.stprecord_id = stprecord_id;
                clsCMD.CommandText = "sp_tbl_AssesmentStepRecordGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public int GetRecordByUserID(Guid userid)
        {
            int blnResult = 0;
            try
            {
                this.stprcrd_userid = userid;
                clsCMD.CommandText = "sp_tbl_AssesmentStepRecordGetData";
                SetGetSPFlag = "BYUSERID";
                blnResult = Convert.ToInt32(clsCMD.ExecuteScalar());
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = 0;
            }
            return blnResult;
        }

        public bool GetRecordByStepID(long stepid,Guid Userid)
        {
            bool blnResult = false;
            try
            {
                this.storcrd_stepid = stepid;
                this.stprcrd_userid = Userid;
                clsCMD.CommandText = "sp_tbl_AssesmentStepRecordGetData";
                SetGetSPFlag = "BYSTEPID";
                int i = Convert.ToInt32(clsCMD.ExecuteScalar());
                if(i > 0)
                {
                    blnResult = true;
                }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 stprecord_id)
        {
            Boolean blnResult = false;
            try
            {
                this.stprecord_id = stprecord_id;
                clsCMD.CommandText = "sp_tbl_AssesmentStepRecordGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsAssesmentStepRecord_PropertiesList
    {
        public Int64 stprecord_id { get; set; }
        public Guid stprcrd_userid { get; set; }
        public Int64 storcrd_stepid { get; set; }
    }
}
