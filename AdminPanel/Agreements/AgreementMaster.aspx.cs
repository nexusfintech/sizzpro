﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.IO;

public partial class Agreements_AgreementMaster : System.Web.UI.Page
{
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsAgreements clsAgreement = new clsAgreements();
    UserPermision UP = new UserPermision();
    protected void Page_Load(object sender, EventArgs e)
    {
        loaddata();
    }

    private void loaddata()
    {
        DataTable dtRecords = new DataTable();
        if (User.IsInRole("administrator"))
        {
            dtRecords = clsAgreement.GetAllRecord();
        }
        else
        {
            dtRecords = clsAgreement.GetAllRecordByUser((Guid)Membership.GetUser().ProviderUserKey);
        }
        dtAgreements.DataSource = dtRecords;
        dtAgreements.DataBind();
    }

    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Agreements/AddEditAgreement.aspx");
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        LinkButton btnEdit = (LinkButton)sender;
        MySession.Current.AddEditFlag = "EDIT";
        MySession.Current.EditID = Convert.ToInt64(btnEdit.CommandArgument);
        Response.Redirect("~/Agreements/AddEditAgreement.aspx");
    }

    protected void CmdMapping_Click(object sender, EventArgs e)
    {
        LinkButton btnEdit = (LinkButton)sender;
        MySession.Current.EditID = Convert.ToInt64(btnEdit.CommandArgument);

        Response.Redirect("~/Agreements/AgreementMapping.aspx");
    }
}