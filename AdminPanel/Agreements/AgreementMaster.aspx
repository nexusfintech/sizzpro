﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AgreementMaster.aspx.cs" Inherits="Agreements_AgreementMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkServiceMapping" runat="server" NavigateUrl="~/Agreements/AgreementServiceMapping.aspx"><i class="icon-asterisk"></i>&nbsp;&nbsp;Mapping To Service</asp:HyperLink>
            </li>
        </ul>
    </div>
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn btn-primary" Text="Create New Agreement" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-color box-bordered">
        <div class="box-title">
            <h3>
                <i class="glyphicon-projector"></i>
                Agreements
            </h3>
        </div>
        <div class="box-content nopadding">

            <asp:Repeater ID="dtAgreements" runat="server">
                <HeaderTemplate>
                    <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Agreement Name</th>
                                <% if (User.IsInRole("administrator"))
                                   { %>
                                <th>Agrement For</th>
                                <%} %>
                                <th>Manage</th>
                                <th>Mapping</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("agr_name") %></td>
                        <% if (User.IsInRole("administrator"))
                           { %>
                        <td><%# Eval("UserName") %></td>
                        <%} %>
                        <td style="text-align: center">
                            <asp:LinkButton ID="cmdEdit" runat="server" OnClick="cmdEdit_Click" CssClass="btn btn-green" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "agr_id") %>'><i class="icon-cog"></i>&nbsp;Manage It</asp:LinkButton>
                        </td>
                         <td style="text-align: center">
                             <asp:LinkButton ID="CmdMapping" runat="server" OnClick="CmdMapping_Click" CssClass="btn btn-green" CommandName="MAPPING" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "agr_id") %>' >Go To Mapping <i class="icon-double-angle-right"></i></asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
