﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsClientParentMapping
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsClientParentMapping()
        {
            clsCMD.Parameters.Clear();
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsClientParentMapping()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _cpm_id; }
            set
            {
                _cpm_id = value;
                if (clsCMD.Parameters.Contains("@cpm_id"))
                { clsCMD.Parameters["@cpm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cpm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _cpm_id;
        public Int64 cpm_id
        {
            get { return _cpm_id; }
            set
            {
                _cpm_id = value;
                if (clsCMD.Parameters.Contains("@cpm_id"))
                { clsCMD.Parameters["@cpm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cpm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _cpm_userid;
        public Guid cpm_userid
        {
            get { return _cpm_userid; }
            set
            {
                _cpm_userid = value;
                if (clsCMD.Parameters.Contains("@cpm_userid"))
                { clsCMD.Parameters["@cpm_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@cpm_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        private String _cpm_DisplayFlag;
        public String cpm_DisplayFlag
        {
            get { return _cpm_DisplayFlag; }
            set
            {
                _cpm_DisplayFlag = value;
                if (clsCMD.Parameters.Contains("@FlagDisp"))
                { clsCMD.Parameters["@FlagDisp"].Value = value; }
                else { clsCMD.Parameters.Add("@FlagDisp", SqlDbType.NVarChar,50).Value = value; }
            }
        }

        private Guid _cpm_parentid;
        public Guid cpm_parentid
        {
            get { return _cpm_parentid; }
            set
            {
                _cpm_parentid = value;
                if (clsCMD.Parameters.Contains("@cpm_parentid"))
                { clsCMD.Parameters["@cpm_parentid"].Value = value; }
                else { clsCMD.Parameters.Add("@cpm_parentid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        private Boolean _cpm_isactive;
        public Boolean cpm_isactive
        {
            get { return _cpm_isactive; }
            set
            {
                _cpm_isactive = value;
                if (clsCMD.Parameters.Contains("@cpm_isactive"))
                { clsCMD.Parameters["@cpm_isactive"].Value = value; }
                else { clsCMD.Parameters.Add("@cpm_isactive", SqlDbType.Bit).Value = value; }
            }
        }
        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                cpm_id = Convert.ToInt64(dr["cpm_id"]);
                cpm_userid = new Guid(dr["cpm_userid"].ToString());
                cpm_parentid = new Guid(dr["cpm_parentid"].ToString());
                cpm_DisplayFlag = "Flag";
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            cpm_id = 0;
            cpm_userid = new Guid();
            cpm_parentid = new Guid();
            cpm_isactive = true;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            OpenConnection();
            this.cpm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "ClientParentMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 cpm_id)
        {
            Boolean blnResult = false;
            OpenConnection();
            this.cpm_id = cpm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "ClientParentMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 cpm_id)
        {
            Boolean blnResult = false;
            OpenConnection();
            this.cpm_id = cpm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "ClientParentMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean UpdateStatus(Guid userid,Boolean sts)
        {
            Boolean blnResult = false;
            OpenConnection();
            this.cpm_parentid = userid;
            this.cpm_isactive = sts;
            this.cpm_userid = userid;
            this.AddEditDeleteFlag = "EDITSTS";
            try
            {
                clsCMD.CommandText = "ClientParentMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean UpdateUserStatus(Guid userid,Guid ParentId, Boolean sts)
        {
            Boolean blnResult = false;
            OpenConnection();
            this.cpm_parentid = ParentId;
            this.cpm_isactive = sts;
            this.cpm_userid = userid;
            this.AddEditDeleteFlag = "EDITSTS";
            try
            {
                clsCMD.CommandText = "ClientParentMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        public DataTable GetAllCombo()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "GETALLCOMBO";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        public DataTable GetAllComboByRole(string strRole)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                cpm_DisplayFlag = strRole;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "GETALLCOMBOBYROLE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<Guid> GetAllUserByRole(string strRole,Boolean sts)
        {
            List<Guid> result = new List<Guid>();
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                cpm_DisplayFlag = strRole;
                cpm_isactive = sts;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "GETALLCOMBOBYROLE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            if(dtResult.Rows.Count > 0)
            {
                result = UserList(dtResult);
            }
            CloseConnection();
            return result;
        }

        public DataTable GetAllComboByRoleandParent(string strRole, Guid ParentUserID)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                cpm_DisplayFlag = strRole;
                cpm_parentid = ParentUserID;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "GETCOMBOBYROLEANDPARENT";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<Guid> GetAllUserByRoleandParent(string strRole, Guid ParentUserID,Boolean sts)
        {
            List<Guid> result = new List<Guid>();
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                cpm_DisplayFlag = strRole;
                cpm_parentid = ParentUserID;
                cpm_isactive = sts;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "GETCOMBOBYROLEANDPARENT";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();

            if(dtResult.Rows.Count > 0)
            {
                result=UserList(dtResult);
            }
            return result;
        }

        public DataTable GetComboByUserID(string strRole, Guid Userid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "GETCOMBOUSERID";
                cpm_DisplayFlag = strRole;
                cpm_userid = Userid;
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetComboByMemberID(string strRole, Guid Userid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "GETCOMBOMEMBERID";
                cpm_DisplayFlag = strRole;
                cpm_userid = Userid;
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetComboByParentID(string strRole, Guid ParentUserID)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "GETCOMBOPARENTID";
                cpm_DisplayFlag = strRole;
                cpm_parentid = ParentUserID;
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsClientParentMapping_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsClientParentMapping_PropertiesList> lstResult = new List<clsClientParentMapping_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].cpm_id = Convert.ToInt64(dr["cpm_id"]);
                lstResult[i].cpm_userid = new Guid(dr["cpm_userid"].ToString());
                lstResult[i].cpm_parentid = new Guid(dr["cpm_parentid"].ToString());
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 cpm_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.cpm_id = cpm_id;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByParentIDInProperties(Guid ParendID)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.cpm_parentid = ParendID;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "BYPARENTID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 cpm_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.cpm_id = cpm_id;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckOtherParent(Guid userid,Guid parentid)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.cpm_userid = userid;
                this.cpm_parentid = parentid;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "CHECKOTHRPRNT";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Guid GetParent(Guid userid)
        {
            Guid Parent = new Guid();
            OpenConnection();
            //Boolean blnResult = false;
            try
            {
                this.cpm_userid = userid;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "GETPARENT";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SqlDataReader RDR = clsCMD.ExecuteReader();
                if(RDR.Read())
                {
                    Parent = new Guid(RDR["cpm_parentid"].ToString());
                }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                Parent = new Guid();
            }
            CloseConnection();
            return Parent;
        }

        public List<Guid> UserList(DataTable dtTable)
        {
            List<Guid> lstResult = new List<Guid>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                string userid = dr["UserId"].ToString();
                lstResult.Add(new Guid(userid));
                i++;
            }
            return lstResult;
        }

        public bool CheckForMapping(Guid ParentId,Guid ClientId)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.cpm_userid = ClientId;
                this.cpm_parentid = ParentId;
                clsCMD.CommandText = "ClientParentMappingGetData";
                SetGetSPFlag = "CHECKMAPING";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion
    }
    public class clsClientParentMapping_PropertiesList
    {
        public Int64 cpm_id { get; set; }
        public Guid cpm_userid { get; set; }
        public Guid cpm_parentid { get; set; }
    }
}