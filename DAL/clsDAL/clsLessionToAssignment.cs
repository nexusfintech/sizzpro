﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsLessionToAssignment
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsLessionToAssignment()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsLessionToAssignment()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _lta_id; }
            set
            {
                _lta_id = value;
                if (clsCMD.Parameters.Contains("@lta_id"))
                { clsCMD.Parameters["@lta_id"].Value = value; }
                else { clsCMD.Parameters.Add("@lta_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _ByUser = new Guid();
        public Guid ByUser
        {
            get { return _ByUser; }
            set
            {
                _ByUser = value;
                if (clsCMD.Parameters.Contains("@User"))
                { clsCMD.Parameters["@User"].Value = value; }
                else { clsCMD.Parameters.Add("@User", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _lta_id;
        public Int64 lta_id
        {
            get { return _lta_id; }
            set
            {
                _lta_id = value;
                if (clsCMD.Parameters.Contains("@lta_id"))
                { clsCMD.Parameters["@lta_id"].Value = value; }
                else { clsCMD.Parameters.Add("@lta_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _lta_lessionid;
        public Int64 lta_lessionid
        {
            get { return _lta_lessionid; }
            set
            {
                _lta_lessionid = value;
                if (clsCMD.Parameters.Contains("@lta_lessionid"))
                { clsCMD.Parameters["@lta_lessionid"].Value = value; }
                else { clsCMD.Parameters.Add("@lta_lessionid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _lta_assignmentid;
        public Int64 lta_assignmentid
        {
            get { return _lta_assignmentid; }
            set
            {
                _lta_assignmentid = value;
                if (clsCMD.Parameters.Contains("@lta_assignmentid"))
                { clsCMD.Parameters["@lta_assignmentid"].Value = value; }
                else { clsCMD.Parameters.Add("@lta_assignmentid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _lta_position;
        public Int64 lta_position
        {
            get { return _lta_position; }
            set
            {
                _lta_position = value;
                if (clsCMD.Parameters.Contains("@lta_position"))
                { clsCMD.Parameters["@lta_position"].Value = value; }
                else { clsCMD.Parameters.Add("@lta_position", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _lta_userid;
        public Guid lta_userid
        {
            get { return _lta_userid; }
            set
            {
                _lta_userid = value;
                if (clsCMD.Parameters.Contains("@lta_userid"))
                { clsCMD.Parameters["@lta_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@lta_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                lta_id = Convert.ToInt64(dr["lta_id"]);
                lta_lessionid = Convert.ToInt64(dr["lta_lessionid"]);
                lta_assignmentid = Convert.ToInt64(dr["lta_assignmentid"]);
                lta_position = Convert.ToInt64(dr["lta_position"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            lta_id = Convert.ToInt64("0");
            lta_lessionid = Convert.ToInt64("0");
            lta_assignmentid = Convert.ToInt64("0");
            lta_position = Convert.ToInt64("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.lta_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "lessiontoassigmentidAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 lta_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.lta_id = lta_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "lessiontoassigmentidAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 lta_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.lta_id = lta_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "lessiontoassigmentidAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByLessonID(Int64 intLessonID)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.lta_lessionid = intLessonID;
            this.AddEditDeleteFlag = "DELETEBYASNID";
            try
            {
                clsCMD.CommandText = "lessiontoassigmentidAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                clsCMD.CommandText = "lessiontoassigmentidGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetLessonWise(Int64 intLsnID)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                this.lta_lessionid = intLsnID;
                clsCMD.CommandText = "lessiontoassigmentidGetData";
                SetGetSPFlag = "GETLESSIONWISE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllByLessonID(Int64 intLsnID, Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.lta_lessionid = intLsnID;
                this.ByUser = MemberId;
                clsCMD.CommandText = "lessiontoassigmentidGetData";
                SetGetSPFlag = "BYLSNID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<Int64> GetAsnByLessonID(Int64 intLsnID, Guid MemberId)
        {

            OpenConnection();
            List<Int64> chapters = new List<Int64>();
            try
            {
                this.lta_lessionid = intLsnID;
                this.ByUser = MemberId;
                clsCMD.CommandText = "lessiontoassigmentidGetData";
                SetGetSPFlag = "BYLSNID";
                SqlDataReader SDR = clsCMD.ExecuteReader();
                while (SDR.Read())
                {
                    Int64 chp = Convert.ToInt64(SDR["asn_id"]);
                    chapters.Add(chp);
                }
                SDR.Close();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                chapters = null;
            }
            CloseConnection();
            return chapters;
        }

        public DataTable GetAllByLessonIDNotMapped(Int64 intLsnID,Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.lta_lessionid = intLsnID;
                this.ByUser = MemberId;
                clsCMD.CommandText = "lessiontoassigmentidGetData";
                SetGetSPFlag = "BYLSNIDNOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        
        public List<clsLessionToAssignment_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsLessionToAssignment_PropertiesList> lstResult = new List<clsLessionToAssignment_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].lta_id = Convert.ToInt64(dr["lta_id"]);
                lstResult[i].lta_lessionid = Convert.ToInt64(dr["lta_lessionid"]);
                lstResult[i].lta_assignmentid = Convert.ToInt64(dr["lta_assignmentid"]);
                lstResult[i].lta_position = Convert.ToInt64(dr["lta_position"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 lta_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.lta_id = lta_id;
                clsCMD.CommandText = "lessiontoassigmentidGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public DataTable GetLessonAssignmet(Int64 lsnid)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.lta_lessionid = lsnid;
                this.ByUser = new Guid();
                clsCMD.CommandText = "lessiontoassigmentidGetData";
                SetGetSPFlag = "GETLSNANS";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        public Boolean CheckDuplicateRow(Int64 lta_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.lta_id = lta_id;
                clsCMD.CommandText = "lessiontoassigmentidGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }

    public class clsLessionToAssignment_PropertiesList
    {
        public Int64 lta_id { get; set; }
        public Int64 lta_lessionid { get; set; }
        public Int64 lta_assignmentid { get; set; }
        public Int64 lta_position { get; set; }
    }
}