﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="SessionAddEdit.aspx.cs" Inherits="Admin_SessionManagement_SessionAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkCourse" runat="server" NavigateUrl="~/Admin/SessionManagement/SessionManagement.aspx"><i class="glyphicon-projector"></i>&nbsp;Session Management</asp:HyperLink>
            </li>
             <li>
               <a>
                   <i class="icon-check"></i> 
                   <asp:Button ID="cmdclaim" runat="server" CssClass="btn btn-primary" Text="Claim Managment" OnClick="cmdclaim_Click"/>
               </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
    <button class="close" data-dismiss="alert" type="button">×</button>
    <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-edit"></i>Client Session of
                        <asp:Label ID="lblClientname" runat="server"></asp:Label>
                    <small>
                        <asp:Label ID="lblmode" runat="server" Text=" in ADD Mode"></asp:Label>
                    </small>
                </h3>
            </div>
            <div class="box-content">
                <%--<--Start General Box-->--%>
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-edit"></i>General</h3>
                    </div>
                    <div class="box-content nopadding">
                        <div class="form-horizontal form-column form-bordered">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtDate" class="control-label">Date</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtDate" runat="server" CssClass="input-medium datepick"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="ddlProvider" class="control-label">Location</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtLocation" runat="server" CssClass="input-medium"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtTotalPay" class="control-label">Total Hours</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtTotalHrs" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtShow" class="control-label">Show</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtShow" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtSessiontype" class="control-label">Session Type</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtSessiontype" runat="server" CssClass="input-medium"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtIntime" class="control-label">In Time</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtIntime" runat="server" CssClass="input-medium"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtPayer" class="control-label">Payer</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtPayer" runat="server" CssClass="input-medium"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txttodaygoal" class="control-label">Today's Goal</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txttodaygoal" runat="server" CssClass="input-medium" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtDateNoteSubmit" class="control-label">Date Note Submit</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtDateNoteSubmit" runat="server" CssClass="input-medium datepick"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtNextApnt" class="control-label">Next Appointment</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtNextApnt" runat="server" CssClass="input-medium datepick"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtDate" class="control-label">Item</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlITem" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="ddlFacilities" class="control-label">Facilities</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlFacilities" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="ddlProvider" class="control-label">Provider</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlProvider" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtOutTime" class="control-label">Out Time</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtOutTime" runat="server" CssClass="input-medium"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtco-payDadrecd" class="control-label">Co-Pay/Dad Received</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtcopayDadrecd" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtTotalPay" class="control-label"></label>
                                    <div class="controls">
                                        <div class="span12 check-line">
                                            <asp:CheckBox ID="chkNoteentered" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                                            <label class='inline'>Note Entered</label>
                                        </div>
                                        <div class="span12 check-line">
                                            <asp:CheckBox ID="chkPlaceoninvoice" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                                            <label class='inline'>Placed on Invoice</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <%--<--Start Insurance Box-->--%>
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-edit"></i>Insurance Detail</h3>
                    </div>
                    <div class="box-content nopadding">
                        <div class="form-horizontal form-column form-bordered">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtDtdubmittoinsurace" class="control-label">Date Submit to Insurance</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtDtdubmittoinsurace" runat="server" CssClass="input-medium datepick"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtClaimNumber" class="control-label">Claim Number</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtClaimNumber" runat="server" CssClass="input-medium"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtClaimNote" class="control-label">Claim Note</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtClaimNote" runat="server" CssClass="input-medium"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtSubmittoinsurance" class="control-label">Submit to insurance</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtSubmittoinsurance" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtRecdfromInsurance" class="control-label">Received From Insurance</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtRecdfromInsurance" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtTotalReceivedIns" class="control-label">Total Received</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtTotalReceivedIns" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <%--<--Start Billing Box-->--%>
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-edit"></i>Bill Detail</h3>
                    </div>
                    <div class="box-content nopadding">
                        <div class="form-horizontal form-column form-bordered">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtClientPaymentReceived" class="control-label">Client Payment Received</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtClientPaymentReceived" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtSubmittoCollection" class="control-label">Submit to Collection</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtSubmittoCollection" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtAdjustWriteoff" class="control-label">Adjust Write off</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtAdjustWriteoff" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtTotalCharged" class="control-label">Total Charged</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtTotalCharged" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtTotalPaid" class="control-label">Total Paid</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtTotalPaid" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtBilledClient" class="control-label">Submit to insurance</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtBilledClient" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtReceviedFromCollection" class="control-label">Received From Collection</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtReceviedFromCollection" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtcofacilitesFee" class="control-label">Co-Facilities Fee</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtcofacilitesFee" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtBalanceDue" class="control-label">Balance Due</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtBalanceDue" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtTherapist" class="control-label">Therapist 40%</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtTherapist" runat="server" CssClass="spinner input-mini" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <asp:LinkButton ID="cmdSave" CssClass="btn btn-primary" runat="server" OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                    <asp:LinkButton ID="cmdCancel" CssClass="btn" runat="server" OnClick="cmdCancel_Click"><i class="icon-remove"></i> Cancle</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

