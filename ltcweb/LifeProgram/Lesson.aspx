﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Lesson.aspx.cs" Inherits="LifeProgram_Lesson" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <style type="text/css">
        .alternate
        {
            background-color: #b7d0f3;
        }
    </style>
    <div class="row-fluid">
        <div class="span12">
            <h5>You are in, <a href="../Client/Dashboard.aspx">Home</a> &gt;&gt; 
                        <a href="Workbook.aspx">Workbook</a> &gt;&gt;<a href="Chapters.aspx"> Chapters</a> &gt;&gt;<a href="Lesson.aspx"> Lesson</a>
            </h5>
        </div>
    </div>
    <hr />
    <asp:Repeater ID="rptChapter" runat="server">
        <ItemTemplate>
            <div class="row-fluid">
                <div class="span12">
                    <h2><i class="icon-book"></i>&nbsp;<%#Eval("chp_name") %></h2>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <%#Eval("chp_description") %>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <hr />
    <asp:Repeater ID="rptLessons" runat="server">
        <HeaderTemplate>
            <table class="table table-hover table-nomargin table-colored-header table-bordered table-condensed">
                <thead>
                    <th style="width: 5%;">Sr. No.</th>
                    <th style="width: 75%;">Lesson's Name</th>
                    <th style="text-align: center; width: 10%;">Status</th>
                    <th style="text-align: center; width: 10%;">Lessons</th>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>">
                <td style="text-align: center;"><%# Container.ItemIndex +1 %></td>
                <td><%#Eval("lsn_name") %> </td>
                <td style="text-align: center;">
                    <img src="../img/<%# GetImageName(DataBinder.Eval(Container.DataItem, "lsn_status").ToString(),DataBinder.Eval(Container.DataItem, "ctl_position").ToString()) %>" style="height: 30px; width: 30px;" />
                <td style="text-align: center;">
                        <a href="#" id='<%#Eval("lsn_id") %>' name='<%#GetLinkStatus(Eval("lsn_status").ToString(), Eval("ctl_position").ToString()) %>'>
                            <img src="../img/<%# GetLinkImageName(DataBinder.Eval(Container.DataItem, "lsn_status").ToString(),DataBinder.Eval(Container.DataItem, "ctl_position").ToString()) %>" style="height: 30px; width: 30px;" />
                        </a>

                    <%--<a href="#" id='<%#Eval("lsn_id") %>' name="view">
                        <img src="../img/Go.png" style="height: 30px; width: 30px;" />
                    </a>--%>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <%--    <div>
        <asp:Repeater ID="RT" runat="server">
            <HeaderTemplate>
                <table border="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="padding-left: 10px; height: 40px; text-align: left" colspan="4">
                        <h2><i class="icon-book"></i>&nbsp;<%#Eval("Title") %></h2>
                    </td>
                </tr>
                <tr style="width: auto;">
                    <td style="text-align: justify;" colspan="4"><%#Eval("Summary") %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div>
        <asp:Repeater ID="RTtable" runat="server">
            <HeaderTemplate>
                <table style="margin: auto; width: 790px; float: left" border="0">
                    <tr style="text-align: center; font-weight: bold; background-color: #296a9d; color: #FFFFFF;">
                        <td style="width: 50px; height: 30px;">No.
                        </td>
                        <td style="text-align: left; padding-left: 5px; width: 600px;">Lesson Title
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr style="text-align: center;">
                    <td style="height: 30px" class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>"><%# Container.ItemIndex +1 %></td>
                    <td style="text-align: left; padding-left: 5px;" class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>"><%#Eval("Title") %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <asp:Repeater ID="RTLink" runat="server">
            <HeaderTemplate>
                <table style="margin: auto; width: 350px;" border="0">
                    <tr style="text-align: center; font-weight: bold; background-color: #296a9d; color: #FFFFFF;">
                        <td style="width: 80px; height: 30px">Status
                        </td>
                        <td style="width: 75px">View
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr style="text-align: center;">
                    <td class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>">
                        <asp:Image ID="imgsts" runat="server" ImageUrl='<%#Eval("urlsts") %>' Style="height: 30px; width: 30px;" />
                    </td>
                    <td class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>">
                        <a href='<%#Eval("href") %>' id='<%#Eval("ID") %>' name="view">
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%#Eval("urlgo") %>' Style="height: 30px; width: 30px;" />
                        </a>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>--%>
    <input type="hidden" id="hidden" name="hidden" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("[name='view']").click(function () {
                var id = this.id;
                $("#<%= hidden.ClientID %>").val(id);
                $("#form1").submit();
                return false;
            });
        });
    </script>
</asp:Content>

