﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="PlaceofServiceMaster.aspx.cs" Inherits="Master_PlaceofServiceMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
     <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <%--<ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn" Text="Add New Facility" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>--%>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Place Of Services
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptPos" runat="server">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>CODE</th>
                                        <th>PLACE</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("pos_code") %></td>
                                <td><%# Eval("pos_place") %></td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "pos_id") %>' OnClick="cmdEdit_Click"><i class="icon-edit-sign"></i> &nbsp;Edit</asp:LinkButton>
                                    <asp:LinkButton ID="cmdDlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "pos_id") %>' OnClick="cmdDlt_Click" rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i> </asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>POS Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <div class="control-group">
                            <label for="txtCode" class="control-label">Code</label>
                            <div class="controls">
                                <asp:TextBox ID="txtcode" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtCode" class="control-label">Place</label>
                            <div class="controls">
                                <asp:TextBox ID="txtPlace" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" CssClass="btn btn-primary" runat="server" OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
