﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSite.master" AutoEventWireup="true" CodeFile="message.aspx.cs" Inherits="message" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid" style="border: 1px solid #0072D6;">

        <div class="span12">
            <%--<asp:Literal ID="ltrPageBody" runat="server"></asp:Literal>--%>
            <h4 style="text-align: center">message from sizzpro system</h4>
            <hr />
        </div>

        <div class="span12" id="InvalidReq" runat="server" visible="false">
            <h5 style="text-align: center">unExceptional Url Redirection
                <br />
                <a href="~/Default.aspx">Click Here</a> For continue with SizzPro.
            </h5>
        </div>

        <div class="span12" id="newuserregister" runat="server" visible="false">
            <h5 style="text-align: center">Congratulation
                <br />
                <br />
                You are Successfully Registered with Sizzpro .
                <br />
                <br />
                Our automated System have sent you a mail in your registerd mail address .Please activate your account from there for to continue with SizzPro .
                <br />
                <br />
                Thanks ...!
            </h5>
        </div>

        <div class="span12" id="mappingerreo" runat="server" visible="false">
            <h5 style="text-align: center">Error occured to mapping your services to your account.
                <br />
                Please contact administrator.
            </h5>
        </div>

        <div class="span12" id="servicepurchased" runat="server" visible="false">
            <h5 style="text-align: center">Your Service have been added to your account successfully .
                <br />
                <br />
                Click <a href="Default.aspx">Here</a> and login with your credential to enjoy your service .
                <br />
                <br />
                Thanks ...!
            </h5>
        </div>

    </div>
</asp:Content>

