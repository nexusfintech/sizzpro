﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="DignosiCodes.aspx.cs" Inherits="Master_DignosiCodes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn" Text="Add New Code" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>

    <div class="row-fluid" id="adddc" runat="server" visible="false">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Add Dignosis Codes
                    </h3>
                </div>
                <div class="box-content">
                    <div class="span10">
                        <div class="control-group">
                            <div class="controls">
                                <asp:DropDownList ID="ddlCodes" runat="server" CssClass="select2-me input-block-level"></asp:DropDownList>
                                <span class="help-block"><strong>CODE FORMATE</strong> : Dignosiscode - Description - hcc - isbilable </span>
                            </div>
                        </div>
                    </div>
                    <div class="span2">
                        <asp:LinkButton ID="lnkAdd" runat="server" CssClass="btn btn-primary" OnClick="lnkAdd_Click"> <i class="icon-ok-circle"></i> Add Code</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Dignosis Codes
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptdgCode" runat="server" OnItemCommand="rptdgCode_ItemCommand" OnItemDataBound="rptdgCode_ItemDataBound">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                        <th>HCC</th>
                                        <th>Terminate Date</th>
                                        <th>Billable</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("dcm_code") %></td>
                                <td><%# Eval("dcm_description") %></td>
                                <td><%# Eval("dcm_codetype") %></td>
                                <td class="qty" style="text-align: center">
                                    <asp:Label ID="lht" runat="server" Text="true" CssClass="label label-success" Visible="false"></asp:Label>
                                    <asp:Label ID="lhf" runat="server" Text="false" CssClass="label label-warning" Visible="false"></asp:Label>
                                </td>
                                <td style="text-align: center"><%# Eval("dcm_terminateddate","{0:dd/MM/yyyy}") %></td>

                                <td class="qty" style="text-align: center">
                                    <asp:Label ID="lpt" runat="server" Text="yes" CssClass="label label-success" Visible="false"></asp:Label>
                                    <asp:Label ID="lpf" runat="server" Text="no" CssClass="label label-warning" Visible="false"></asp:Label>
                                </td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdDlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ucm_id") %>' rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i> </asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
