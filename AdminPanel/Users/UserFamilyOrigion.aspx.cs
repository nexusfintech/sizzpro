﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Users_UserFamilyOrigion : System.Web.UI.Page
{
    string strMemberkey = "";
    Int64 intUserid;

    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["form_memberkey"] != null)
            {
                strMemberkey = Session["form_memberkey"].ToString();
                intUserid = Convert.ToInt64(Session["form_userid"]);
                ucFather.Memberkey = strMemberkey;
                ucMother.Memberkey = strMemberkey;
                ucMatFather.Memberkey = strMemberkey;
                ucMatMother.Memberkey = strMemberkey;
                ucPatFather.Memberkey = strMemberkey;
                ucPatMother.Memberkey = strMemberkey;
            }
            else
            {
                Response.Redirect("~/Users/UserClientManage.aspx");
            }
            Boolean isRcrdFound = clsASR.GetRecordByStepID(5, new Guid(strMemberkey));
            if (isRcrdFound)
            {
                cmdReschedule.Visible = true;
            }
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Boolean blnResult = false;
        blnResult = ucFather.Save();
        blnResult = ucMother.Save();
        blnResult = ucMatFather.Save();
        blnResult = ucMatMother.Save();
        blnResult = ucPatFather.Save();
        blnResult = ucPatMother.Save();
        if (blnResult == true)
        {
            clsASR.storcrd_stepid = 5;
            clsASR.stprcrd_userid = new Guid(strMemberkey);
            Boolean stprslt = clsASR.Save();
            if (stprslt == true)
            {
                lblMessage.Text = clsASR.GetSetErrorMessage;
            }
            else
            {
                lblMessage.Text = "Error in Save..." + clsASR.GetSetErrorMessage;
            }
        }
        else
        {
            lblMessage.Text = "Error in Save User Data";
        }
    }

    protected void cmdReschedule_Click(object sender, EventArgs e)
    {
        Boolean blnresult = clsASR.DeleteByStepID(5, new Guid(strMemberkey));
        if (blnresult == true)
        {
            lblMessage.Text = clsASR.GetSetErrorMessage;
        }
        else
        {
            lblMessage.Text = "Error in Operation..." + clsASR.GetSetErrorMessage;
        }
    }
}