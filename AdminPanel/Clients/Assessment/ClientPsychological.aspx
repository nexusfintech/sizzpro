﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientPsychological.aspx.cs" Inherits="Clients_Assessment_ClientPsychological" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <UC:AssessmentStepLinks ID="ucasslinks" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-heart"></i>
                Client's Psychological Information
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:Psychological ID="ucpsychological" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $("textarea").keyup(function (e) {
                while ($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                    $(this).height($(this).height() + 1);
                };
            });
        });
    </script>
</asp:Content>

