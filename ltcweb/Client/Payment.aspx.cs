﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Payment : System.Web.UI.Page
{
    public string success = string.Empty;
    public string cancle = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        success = Request.PhysicalPath + "/client/Payment.aspx?f=success";
        cancle = Request.PhysicalPath + "/client/Payment.aspx?f=cancle";
        clsAppointmentMaster clsAPT = new clsAppointmentMaster();
        clsClientParentMapping clsCPM = new clsClientParentMapping();
        if (Session["appoinmentId"] != null)
        {
            if (Request.QueryString["f"] != null)
            {
                string flag = Request.QueryString["f"].ToString();
                if (flag.ToLower() == "success")
                {
                    Boolean blnrslt = false;
                    bool BlnApntReslt = clsAPT.GetRecordByIDInProperties(Session["appoinmentId"].ToString());
                    if (BlnApntReslt)
                    {
                        clsAPT.apt_iscompleted = true;
                        blnrslt = clsAPT.Update(Session["appoinmentId"].ToString());
                        Boolean blnIsMaped = clsCPM.CheckForMapping(clsAPT.apt_userid, clsAPT.apt_clientid);
                        if (!blnIsMaped)
                        {
                            clsCPM.cpm_parentid = clsAPT.apt_userid;
                            clsCPM.cpm_userid = clsAPT.apt_clientid;
                            blnrslt = clsCPM.Save();
                        }
                    }
                    if (blnrslt == true)
                    {
                        Session["msg_boxheader"] = "Success";
                        Session["msg_message"] = "Hello, " + Membership.GetUser().UserName + "<br/>Your appointment saved successfully. Please manage your appointment details on Dashboard.";
                        Response.Redirect("~/Messages.aspx");
                    }
                    else
                    {
                        Session["msg_boxheader"] = "Error";
                        Session["msg_message"] = "Hello, " + Membership.GetUser().UserName + " <br/>Soory because of internal error your appoinment is not fixed.please contact your provider";
                        Response.Redirect("~/Messages.aspx");
                    }
                }
                else if (flag.ToLower() == "cancle")
                {
                    bool blnrslt = clsAPT.DeleteByAPTCode(Session["appoinmentId"].ToString());
                    if (blnrslt == true)
                    {
                        Session["msg_boxheader"] = "Error";
                        Session["msg_message"] = "Hello, " + Membership.GetUser().UserName + " <br/>You have cancled you intial fees for appoinment so your appoinment is canclled";
                        Response.Redirect("~/Messages.aspx");
                    }
                }
            }
        }
        else
        {
            Response.Redirect("~/Client/Appointment.aspx");
        }
    }
}