﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_AuthorizationMaster : System.Web.UI.Page
{
    clsauthorizationMaster clsAM = new clsauthorizationMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptAutho.DataSource = clsAM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        rptAutho.DataBind();
    }
    protected void rptAutho_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if(e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/AuthorizationAddEdit.aspx");
        }
        if(e.CommandName == "DELETE")
        {
            Boolean blnRslt = clsAM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if(blnRslt)
            {
                MySession.Current.MESSAGE = "Record Deleted Successfully";
                Response.Redirect("~/Master/AuthorizationMaster.aspx");
            }
            else
            {
                lblmessage.Text = clsAM.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Master/AuthorizationAddEdit.aspx");
    }
}