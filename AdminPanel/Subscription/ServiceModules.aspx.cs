﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;

public partial class Subscription_ServiceModules : System.Web.UI.Page
{
    clssubscriptionservicemodulemapping clsSSMM = new clssubscriptionservicemodulemapping();

    public long serviceid { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["serviceid"] != null)
        {
            serviceid = Convert.ToInt64(Session["serviceid"]);
        }
        else
        {
            Response.Redirect("~/Subscription/ServicesMaster.aspx");
        }
        if (!IsPostBack)
        {
            binddata();
        }
    }

    private void binddata()
    {
        dtServModules.DataSource = clsSSMM.GetAllRecordByServiceId(serviceid);
        dtServModules.DataBind();

        DataTable dtResult = clsSSMM.GetNotMapedModule(serviceid);
        if (dtResult.Rows.Count != 0)
        {
            ddlservicemodules.Items.Clear();
            ddlservicemodules.SelectedIndex = -1;
            ddlservicemodules.SelectedValue = null;
            ddlservicemodules.ClearSelection();
            ddlservicemodules.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddlservicemodules.Items.Add(lst);
            ddlservicemodules.DataValueField = "mdl_id";
            ddlservicemodules.DataTextField = "mdl_name";
            ddlservicemodules.DataSource = dtResult;
            ddlservicemodules.DataBind();
        }
        else
        {
            ddlservicemodules.Items.Clear();
            ddlservicemodules.SelectedIndex = -1;
            ddlservicemodules.SelectedValue = null;
            ddlservicemodules.ClearSelection();
            ddlservicemodules.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<- No More Module Available ->>";
        }
    }
    protected void cmdRemove_Click(object sender, EventArgs e)
    {
        LinkButton btnlnk = (LinkButton)sender;
        Boolean blnDltRslt = clsSSMM.DeleteByPKID(Convert.ToInt64(btnlnk.CommandArgument));
        binddata();
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        clsSSMM.smm_moduleid = Convert.ToInt64(ddlservicemodules.SelectedValue);
        clsSSMM.smm_serviceid = serviceid;
        Boolean blnResult = clsSSMM.Save();
        if (blnResult == true)
        {
            lblmessage.Text = "Record Saved Successfully";
            binddata();
        }
        else
        {
            lblmessage.Text = clsSSMM.GetSetErrorMessage;
        }
    }
}