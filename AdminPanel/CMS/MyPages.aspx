﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MyPages.aspx.cs" Inherits="CMS_MyPages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="glyphicon-book_open"></i>
                    <asp:Button ID="cmdCreateNew" Enabled="true" runat="server" CssClass="btn btn-primary" Text="Create New Page" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-color box-bordered">
        <div class="box-title">
            <h3>
                <i class="glyphicon-projector"></i>
                My Pages
            </h3>
        </div>
        <div class="box-content nopadding">
            <asp:Repeater ID="dtPages" runat="server">
                <HeaderTemplate>
                    <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Page Name</th>
                                <th>Url</th>
                                <th style="text-align: center; width: 5%;"></th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%#Eval("cnt_key") %></td>
                        <td><%#Eval("cnt_key") %></td>
                        <td style="text-align:center">
                            <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-green" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "cnt_id") %>' OnClick="cmdEdit_Click" ><i class="icon-cog"></i> MANAGE
                            </asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
