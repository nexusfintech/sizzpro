﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.IO;
using System.Net.Mime;
using SendGridMail;
using SendGridMail.Transport;

namespace clsDAL
{
    public class clsSendMail
    {
        clsEmailAccounts clsMailAccount = new clsEmailAccounts();

        private String strToMailAddress = "";
        private String strCCMailAddress = "";
        private String strBCCMailAddress = "";
        private String strSubject = "";
        private String strHtmlBody = "";
        private MailPriority enuMailPriority = MailPriority.Normal;

        public String strError;

        public String ToMailAddress
        {
            get { return strToMailAddress; }
            set { strToMailAddress = value; }
        }

        public String CCMailAddress
        {
            get { return strCCMailAddress; }
            set { strCCMailAddress = value; }
        }

        public String BCCMailAddress
        {
            get { return strBCCMailAddress; }
            set { strBCCMailAddress = value; }
        }

        public String Subject
        {
            get { return strSubject; }
            set { strSubject = value; }
        }

        public String HtmlBody
        {
            get { return strHtmlBody; }
            set { strHtmlBody = value; }
        }

        public MailPriority Priority
        {
            get { return enuMailPriority; }
            set { enuMailPriority = value; }
        }

        private string strDisplayName;
        private string strFromEmail;
        private string strSMTPServer;
        private string strFromPassword;
        private Boolean blnisHtml;
        private Boolean blnEnableSSL;
        private Int32 intSMTPPort;
        private Boolean blnEmailAccountBindStatus = false;

        public Boolean BindEmailAccount(String strEmailAccount)
        {
            Boolean blnResult = false;
            Boolean blnGet = clsMailAccount.GetRecordByAccountInProperties(strEmailAccount.Trim());
            if (blnGet == true)
            {
                strDisplayName = clsMailAccount.ema_accountname;
                strFromEmail = clsMailAccount.ema_emailaddress;
                strSMTPServer = clsMailAccount.ema_smtpserver;
                strFromPassword = clsMailAccount.ema_emailpassword;
                blnEnableSSL = clsMailAccount.ema_enablessl;
                blnisHtml = clsMailAccount.ema_isbodyhtml;
                intSMTPPort = Convert.ToInt32(clsMailAccount.ema_smtpport);
                blnEmailAccountBindStatus = true;
                blnResult = true;
            }
            else { blnResult = false; blnEmailAccountBindStatus = false; }
            return blnResult;
        }

        public Boolean SendMail()
        {
            if (blnEmailAccountBindStatus == false)
            {
                strError = "Email Account not Binded..!!";
                return false;
            }

            Boolean blnResult = false;
            #region Send Mail
            string from = strFromEmail; //Replace this with your own correct Gmail Address
            string to = ToMailAddress; //Replace this with the Email Address to whom you want to send the mail
            string cc = CCMailAddress;
            string bcc = BCCMailAddress;
            string body = HtmlBody;

            var tt = SendGrid.GetInstance();
            SMTP smtpInstance = SMTP.GetInstance(new NetworkCredential("azure_3b588217fd94a16e4729eeb6efd200b4@azure.com", "ucs4dwd3"));
            var newMessage = SendGrid.GetInstance();

            if (strToMailAddress.Trim().Length > 0)
            {
                newMessage.AddTo(to);
            }
            if (strCCMailAddress.Trim().Length > 0)
            {
                newMessage.AddCc(cc);
            }
            if (strBCCMailAddress.Trim().Length > 0)
            {
                newMessage.AddBcc(bcc);
            }
            newMessage.From = new MailAddress(from,strDisplayName, System.Text.Encoding.UTF8);
            newMessage.Html = body;
            newMessage.Subject = Subject;

            try
            {
                smtpInstance.Deliver(newMessage);
                blnResult = true;
                strError = "Mail Send Successfully.";
            }
            catch (Exception ex)
            {
                blnResult = false;
                strError = ex.Message;
            }
            #endregion
            return blnResult;
        }
        //public string GenerateHtmlSource(string strLink)
        //{
        //    string strResult = "";
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new clsGetSavedValue().GetMasterDomainName() + strLink);
        //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //    if (response.StatusCode == HttpStatusCode.OK)
        //    {
        //        Stream receiveStream = response.GetResponseStream();
        //        StreamReader readStream = null;
        //        if (response.CharacterSet == null)
        //            readStream = new StreamReader(receiveStream);
        //        else
        //            readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
        //        strResult = readStream.ReadToEnd();
        //        response.Close();
        //        readStream.Close();
        //    }
        //    return strResult;
        //}
    }
}
