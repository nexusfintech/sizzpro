﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="InsurancePlanMaster.aspx.cs" Inherits="Master_InsurancePlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn" Text="Add New Plan" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Insurance Plans
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptInsPlan" runat="server" OnItemCommand="rptInsPlan_ItemCommand" OnItemDataBound="rptInsPlan_ItemDataBound">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Group No.</th>
                                        <th>Deductible</th>
                                        <th>CoPay</th>
                                        <th>CoInsurance</th>
                                        <th>MHO</th>
                                        <th>Dietition</th>
                                        <th>Chiropractor</th>
                                        <th>CPT codes</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("insp_planname") %></td>
                                <td><%# Eval("insp_groupnumber") %></td>
                                <td><%# String.Format("{0:C}",Eval("insp_deductible")) %></td>
                                <td><%# String.Format("{0:C}",Eval("insp_copay")) %></td>
                                <td><%# String.Format("{0:C}",Eval("insp_coinsurance")) %></td>
                                <td class="qty" style="text-align: center">
                                    <asp:Label ID="lblmho" runat="server" CssClass="label label-blue"></asp:Label>
                                </td>
                                <td class="qty" style="text-align: center">
                                    <asp:Label ID="lbldit" runat="server" CssClass="label label-blue"></asp:Label>
                                </td>
                                <td class="qty" style="text-align: center">
                                    <asp:Label ID="lblchi" runat="server" CssClass="label label-blue"></asp:Label>
                                </td>
                                <td class="qty" style="text-align:center">
                                    <a href="javascript:void(0);" class="btn btn-primary" rel="popover" title="ALLOWED CPT CODES" data-placement="top" data-content="<%# Eval("insp_cptcodes") %>">#</a>
                                </td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "insp_id") %>'><i class="icon-edit-sign"></i> &nbsp;Edit</asp:LinkButton>
                                    <asp:LinkButton ID="cmdDlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "insp_id") %>' rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i> </asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

