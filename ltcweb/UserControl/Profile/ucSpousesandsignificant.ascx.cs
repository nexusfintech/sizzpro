﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
using System.Globalization;

public partial class UserControl_ClientProfile_ucSpousesandsignificant : System.Web.UI.UserControl
{
    clsSpousesSignificant clsSpouse = new clsSpousesSignificant();
    clsUsers clsUsr = new clsUsers();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();
    Int64 intUserID;

    private static string strAddEditFlag;
    private static Int64 intEditID;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void EmptyControl()
    {
        txtDOB.Text = DateTime.Now.ToString("MM/dd/yyyy",CultureInfo.InvariantCulture);
        txtEndDate.Text = "";
        txtFirstName.Text = "";
        txtlastname.Text = "";
        txtMiddlename.Text = "";
        txtStartDate.Text = "";
    }

    private void BindGrid()
    {
        Guid objKey = new Guid(Memberkey);
        dtSpouses.DataSource = clsSpouse.GetAllByUserID(objKey);
        dtSpouses.DataBind();
    }

    private String ValidateForm()
    {
        string strResult = "";
        if (txtFirstName.Text.Trim() == "")
        { strResult += "Please Enter Firstname. <br/>"; }
        if (txtMiddlename.Text.Trim() == "")
        { strResult += "Please Enter Middlename. <br/>"; }
        if (txtlastname.Text.Trim() == "")
        { strResult += "Please Enter Lastname. <br/>"; }
        if (!clsFormat.ValidateDate(txtDOB.Text))
        { strResult += "Enter Valid Birthdate. <br/>"; }
        if (txtStartDate.Text.Trim() != "")
        {
            if (!clsFormat.ValidateDate(txtStartDate.Text))
            {
                strResult += "Invalid Start Date <br/>";
            }
        }
        if (txtEndDate.Text.Trim() != "")
        {
            if (!clsFormat.ValidateDate(txtEndDate.Text))
            {
                strResult += "Invalid Start Date <br/>";
            }
        }
        return strResult;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            strAddEditFlag = "ADD";
            txtDOB.Text = DateTime.Now.ToString("MM/dd/yyyy",CultureInfo.InvariantCulture);
            txtStartDate.Text = DateTime.Now.ToString("MM/dd/yyyy",CultureInfo.InvariantCulture);
            txtEndDate.Text = DateTime.Now.ToString("MM/dd/yyyy",CultureInfo.InvariantCulture);
            BindGrid();
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        String strResult = ValidateForm();
        if (strResult != "")
        {
            lblMessage.Text = strResult;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        DateTime dtBirthDate;
        if (!(DateTime.TryParseExact(txtDOB.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtBirthDate)))
        {
            lblMessage.Text = "Invalid Birth Date format please enter in mm/dd/yyyy"; return;
        }
        clsSpouse.sas_birthdate = dtBirthDate;

        DateTime dtStartDate;
        if (!(DateTime.TryParseExact(txtStartDate.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtStartDate)))
        {
            lblMessage.Text = "Invalid Start Date format please enter in mm/dd/yyyy"; return;
        }
        clsSpouse.sas_startdate = dtStartDate;

        if (txtEndDate.Text.Trim() == "")
        {
            clsSpouse.sas_enddate = null;
        }
        else
        {
            DateTime dtEndDate;
            if (!(DateTime.TryParseExact(txtEndDate.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtEndDate)))
            {
                lblMessage.Text = "Invalid Start Date format please enter in mm/dd/yyyy"; return;
            }
            clsSpouse.sas_enddate = dtEndDate;
        }

        clsSpouse.sas_description = ddlRelationship.SelectedItem.Text;
        clsSpouse.sas_firstname = txtFirstName.Text;
        clsSpouse.sas_gender = ddlGender.SelectedItem.Text;
        clsSpouse.sas_lastname = txtlastname.Text;
        clsSpouse.sas_livewithclient = chkLive.Checked;
        clsSpouse.sas_middlename = txtMiddlename.Text;
        clsSpouse.sas_relationflag = "SPOUSES";
        clsSpouse.sas_relationship = "";

        Guid objKey = new Guid(Memberkey);
        clsSpouse.sas_userid = objKey;
        Boolean blnSave = false;
        if (strAddEditFlag == "ADD")
        {
            blnSave = clsSpouse.Save();
        }
        else { blnSave = clsSpouse.Update(intEditID); }
        if (blnSave == true)
        {
            lblMessage.Text = "Saved Successfully.";
            clsASR.storcrd_stepid =6;
            clsASR.stprcrd_userid =objKey;
            Boolean stprslt = clsASR.Save();
            BindGrid();
            strAddEditFlag = "ADD";
        }
        else { lblMessage.Text = "Error in Save.." + clsSpouse.GetSetErrorMessage; }
    }

    private void EditEntry()
    {
        strAddEditFlag = "EDIT";
        txtDOB.Text = clsSpouse.sas_birthdate.ToString("MM/dd/yyyy",CultureInfo.InvariantCulture);
        txtStartDate.Text = clsSpouse.sas_startdate.ToString();
        ddlRelationship.Text = clsSpouse.sas_description;
        txtEndDate.Text = clsSpouse.sas_enddate.ToString();
        txtFirstName.Text = clsSpouse.sas_firstname;
        ddlGender.Text = clsSpouse.sas_gender;
        txtlastname.Text = clsSpouse.sas_lastname;
        chkLive.Checked = clsSpouse.sas_livewithclient;
        txtMiddlename.Text = clsSpouse.sas_middlename;
        
    }

    protected void dtSpouses_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            intEditID = Convert.ToInt64(e.CommandArgument);
            Boolean blnEdit = clsSpouse.GetRecordByIDInProperties(intEditID);
            if (blnEdit == true)
            { EditEntry(); }
            else { lblMessage.Text = "Error... Edit Process not proper complete please try again."; }
        }
        if (e.CommandName == "delete")
        {
            intEditID = Convert.ToInt64(e.CommandArgument);
            Boolean blnEdit = clsSpouse.DeleteByPKID(intEditID);
            if (blnEdit == true)
            { BindGrid(); lblMessage.Text = "Delete Successfully."; }
            else { lblMessage.Text = "Error... Edit Process not proper complete please try again."; }
        }
    }
}