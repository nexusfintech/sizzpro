﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class LifeProgram_Assignment : System.Web.UI.Page
{
    clsLessions clsLSN = new clsLessions();
    clsLessionToAssignment clsLTA = new clsLessionToAssignment();
    clsWorkbookAnswer clsAnswer = new clsWorkbookAnswer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            _GridFill();
        }
    }

    void _GridFill()
    {
        string strusername = User.Identity.Name;
        int intasid = Convert.ToInt32(Session["assignmentid"]);
        if (intasid == 0)
        {
            Response.Redirect("~/Client/DashBoard.aspx");
        }

        rptLesson.DataSource = clsLSN.GetAllRecordPKWise(intasid);
        rptLesson.DataBind();
        Guid ltaGuiud = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        clsLTA.lta_userid = ltaGuiud;
        rptAssignment.DataSource = clsLTA.GetLessonWise(intasid);
        rptAssignment.DataBind();

        //RTAssignment.DataSource = clsworkbook.AssignementDetails(intasid);
        //RTAssignment.DataBind();
        //answer = clsworkbook.AssignementAnswer(intasid, strusername);
        //txtanswer.InnerText = answer;
    }

    protected void btn_update_ans_Click(object sender, EventArgs e)
    {
        int intasid = Convert.ToInt32(Session["assignmentid"]);
        Guid usrGUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        foreach (RepeaterItem item in rptAssignment.Items)
        {
            if (item.ItemType == ListItemType.Item)
            {
                TextBox txtAnswer = (TextBox)item.FindControl("txtAnswer");
                HiddenField hdid = (HiddenField)item.FindControl("asnid");
                clsAnswer.wns_answer = txtAnswer.Text;
                clsAnswer.wns_answerdatetime = DateTime.Now;
                clsAnswer.wns_assignmentid = Convert.ToInt64(hdid.Value);
                clsAnswer.wns_userid = usrGUID;
                clsAnswer.Save();
            }
        }
        Response.Redirect("~/LifeProgram/Lesson.aspx");

        //string ans = txtanswer.InnerText.ToString();
        //string strusername = User.Identity.Name;
        //int intasid = Convert.ToInt32(Session["assignmentid"]);
        //bool sts = clsworkbook.UpdateAssignmentAnswer(ans, intasid, strusername);

        ////Here Code for to change status of new lesson
        //if (sts == true)
        //{
        //    Response.Redirect("~/LifeProgram/Lesson.aspx");
        //}
    }
}