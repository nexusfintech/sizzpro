﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsDiagnosisCodeMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsDiagnosisCodeMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsDiagnosisCodeMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _dcm_id; }
            set
            {
                _dcm_id = value;
                if (clsCMD.Parameters.Contains("@dcm_id"))
                { clsCMD.Parameters["@dcm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@dcm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _dcm_id;
        public Int64 dcm_id
        {
            get { return _dcm_id; }
            set
            {
                _dcm_id = value;
                if (clsCMD.Parameters.Contains("@dcm_id"))
                { clsCMD.Parameters["@dcm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@dcm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _dcm_code;
        public String dcm_code
        {
            get { return _dcm_code; }
            set
            {
                _dcm_code = value;
                if (clsCMD.Parameters.Contains("@dcm_code"))
                { clsCMD.Parameters["@dcm_code"].Value = value; }
                else { clsCMD.Parameters.Add("@dcm_code", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _dcm_codetype;
        public String dcm_codetype
        {
            get { return _dcm_codetype; }
            set
            {
                _dcm_codetype = value;
                if (clsCMD.Parameters.Contains("@dcm_codetype"))
                { clsCMD.Parameters["@dcm_codetype"].Value = value; }
                else { clsCMD.Parameters.Add("@dcm_codetype", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _dcm_description;
        public String dcm_description
        {
            get { return _dcm_description; }
            set
            {
                _dcm_description = value;
                if (clsCMD.Parameters.Contains("@dcm_description"))
                { clsCMD.Parameters["@dcm_description"].Value = value; }
                else { clsCMD.Parameters.Add("@dcm_description",SqlDbType.NVarChar).Value = value; }
            }
        }
        private Boolean _dcm_hcc;
        public Boolean dcm_hcc
        {
            get { return _dcm_hcc; }
            set
            {
                _dcm_hcc = value;
                if (clsCMD.Parameters.Contains("@dcm_hcc"))
                { clsCMD.Parameters["@dcm_hcc"].Value = value; }
                else { clsCMD.Parameters.Add("@dcm_hcc", SqlDbType.Bit).Value = value; }
            }
        }
        private DateTime _dcm_terminateddate;
        public DateTime dcm_terminateddate
        {
            get { return _dcm_terminateddate; }
            set
            {
                _dcm_terminateddate = value;
                if (clsCMD.Parameters.Contains("@dcm_terminateddate"))
                { clsCMD.Parameters["@dcm_terminateddate"].Value = value; }
                else { clsCMD.Parameters.Add("@dcm_terminateddate", SqlDbType.DateTime).Value = value; }
            }
        }
        private Boolean _dcm_ispayable;
        public Boolean dcm_ispayable
        {
            get { return _dcm_ispayable; }
            set
            {
                _dcm_ispayable = value;
                if (clsCMD.Parameters.Contains("@dcm_ispayable"))
                { clsCMD.Parameters["@dcm_ispayable"].Value = value; }
                else { clsCMD.Parameters.Add("@dcm_ispayable", SqlDbType.Bit).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                dcm_id = Convert.ToInt64(dr["dcm_id"]);
                dcm_code = Convert.ToString(dr["dcm_code"]);
                dcm_codetype = dr["dcm_codetype"].ToString();
                dcm_description = dr["dcm_description"].ToString();
                dcm_hcc = Convert.ToBoolean(dr["dcm_hcc"]);
                dcm_terminateddate = Convert.ToDateTime(dr["dcm_terminateddate"]);
                dcm_ispayable = Convert.ToBoolean(dr["dcm_ispayable"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            dcm_id = Convert.ToInt64("0");
            dcm_code = Convert.ToString("NA");
            dcm_codetype = "";
            dcm_description = "";
            dcm_hcc = Convert.ToBoolean(false);
            dcm_terminateddate = DateTime.Now;
            dcm_ispayable = Convert.ToBoolean(false);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.dcm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_DiagnosisCodeMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 dcm_id)
        {
            Boolean blnResult = false;
            this.dcm_id = dcm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_DiagnosisCodeMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 dcm_id)
        {
            Boolean blnResult = false;
            this.dcm_id = dcm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_DiagnosisCodeMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_DiagnosisCodeMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsDiagnosisCodeMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsDiagnosisCodeMaster_PropertiesList> lstResult = new List<clsDiagnosisCodeMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].dcm_id = Convert.ToInt64(dr["dcm_id"]);
                lstResult[i].dcm_code = Convert.ToString(dr["dcm_code"]);
                lstResult[i].dcm_codetype = dr["dcm_codetype"].ToString();
                lstResult[i].dcm_description = dr["dcm_description"].ToString();
                lstResult[i].dcm_hcc = Convert.ToBoolean(dr["dcm_hcc"]);
                lstResult[i].dcm_terminateddate = Convert.ToDateTime(dr["dcm_terminateddate"]);
                lstResult[i].dcm_ispayable = Convert.ToBoolean(dr["dcm_ispayable"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 dcm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.dcm_id = dcm_id;
                clsCMD.CommandText = "sp_DiagnosisCodeMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 dcm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.dcm_id = dcm_id;
                clsCMD.CommandText = "sp_DiagnosisCodeMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion
    }
    public class clsDiagnosisCodeMaster_PropertiesList
    {
        public Int64 dcm_id { get; set; }
        public String dcm_code { get; set; }
        public String dcm_codetype { get; set; }
        public String dcm_description { get; set; }
        public Boolean dcm_hcc { get; set; }
        public DateTime dcm_terminateddate { get; set; }
        public Boolean dcm_ispayable { get; set; }
    }
}