﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsUsers
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsUsers()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsUsers()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Guid PrimeryKey
        {
            get { return _UserId; }
            set
            {
                _UserId = value;
                if (clsCMD.Parameters.Contains("@UserId"))
                { clsCMD.Parameters["@UserId"].Value = value; }
                else { clsCMD.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Guid _ApplicationId;
        public Guid ApplicationId
        {
            get { return _ApplicationId; }
            set
            {
                _ApplicationId = value;
                if (clsCMD.Parameters.Contains("@ApplicationId"))
                { clsCMD.Parameters["@ApplicationId"].Value = value; }
                else { clsCMD.Parameters.Add("@ApplicationId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _UserId;
        public Guid UserId
        {
            get { return _UserId; }
            set
            {
                _UserId = value;
                if (clsCMD.Parameters.Contains("@UserId"))
                { clsCMD.Parameters["@UserId"].Value = value; }
                else { clsCMD.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _User_Id;
        public Int64 User_Id
        {
            get { return _User_Id; }
            set
            {
                _User_Id = value;
                if (clsCMD.Parameters.Contains("@User_Id"))
                { clsCMD.Parameters["@User_Id"].Value = value; }
                else { clsCMD.Parameters.Add("@User_Id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _UserName;
        public String UserName
        {
            get { return _UserName; }
            set
            {
                _UserName = value;
                if (clsCMD.Parameters.Contains("@UserName"))
                { clsCMD.Parameters["@UserName"].Value = value; }
                else { clsCMD.Parameters.Add("@UserName", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Boolean _IsAnonymous;
        public Boolean IsAnonymous
        {
            get { return _IsAnonymous; }
            set
            {
                _IsAnonymous = value;
                if (clsCMD.Parameters.Contains("@IsAnonymous"))
                { clsCMD.Parameters["@IsAnonymous"].Value = value; }
                else { clsCMD.Parameters.Add("@IsAnonymous", SqlDbType.Bit).Value = value; }
            }
        }
        private DateTime _LastActivityDate;
        public DateTime LastActivityDate
        {
            get { return _LastActivityDate; }
            set
            {
                _LastActivityDate = value;
                if (clsCMD.Parameters.Contains("@LastActivityDate"))
                { clsCMD.Parameters["@LastActivityDate"].Value = value; }
                else { clsCMD.Parameters.Add("@LastActivityDate", SqlDbType.DateTime).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                string straid = dr["ApplicationId"].ToString();
                Guid aid = new Guid(straid);
                ApplicationId = aid;
                string struserid =dr["UserId"].ToString();
                Guid guuserid = new Guid(struserid);
                UserId = guuserid;
                User_Id = Convert.ToInt64(dr["User_Id"]);
                UserName = Convert.ToString(dr["UserName"]);
                IsAnonymous = Convert.ToBoolean(dr["IsAnonymous"]);
                LastActivityDate = Convert.ToDateTime(dr["LastActivityDate"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            ApplicationId = new Guid();
            UserId = new Guid();
            User_Id = Convert.ToInt64("0");
            UserName = Convert.ToString("NA");
            IsAnonymous = Convert.ToBoolean(false);
            LastActivityDate = Convert.ToDateTime(DateTime.Now);
        }
        #endregion

        #region Declare Add/Edit/Delete Function
        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }


        public Boolean Save()
        {
            Boolean blnResult = false;
            this.UserId = new Guid();
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_UsersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Guid UserId)
        {
            Boolean blnResult = false;
            this.UserId = UserId;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_UsersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Guid UserId)
        {
            Boolean blnResult = false;
            this.UserId = UserId;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_UsersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_UsersGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<string> GetAllRecordByRole(string role)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_UsersGetData";
                SetGetSPFlag = "ALLUSERFORMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            List<string> userlist = UserList(dtResult);
            return userlist;
        }

        public List<string> GetAllRecordByRoleForMember(string role,Guid MemberId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_UsersGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            List<string> userlist = UserList(dtResult);
            return userlist;
        }

        public List<clsUsers_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsUsers_PropertiesList> lstResult = new List<clsUsers_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                string straid=dr["ApplicationId"].ToString();
                Guid aid = new Guid(straid);
                lstResult[i].ApplicationId = aid;
                string struserid = dr["UserId"].ToString();
                Guid userguid = new Guid(struserid);
                lstResult[i].UserId = userguid;
                lstResult[i].User_Id = Convert.ToInt64(dr["User_Id"]);
                lstResult[i].UserName = Convert.ToString(dr["UserName"]);
                lstResult[i].IsAnonymous = Convert.ToBoolean(dr["IsAnonymous"]);
                lstResult[i].LastActivityDate = Convert.ToDateTime(dr["LastActivityDate"]);

                i++;
            }
            return lstResult;
        }

        public List<string> UserList(DataTable dtTable)
        {
            List<string> lstResult = new List<string>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                string username = dr["UserName"].ToString();
                lstResult.Add(username);
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Guid UserId)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.UserId = UserId;
                clsCMD.CommandText = "sp_UsersGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(String UserName)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.UserName = UserName;
                clsCMD.CommandText = "sp_UsersGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }
    public class clsUsers_PropertiesList
    {
        public Guid ApplicationId { get; set; }
        public Guid UserId { get; set; }
        public Int64 User_Id { get; set; }
        public String UserName { get; set; }
        public Boolean IsAnonymous { get; set; }
        public DateTime LastActivityDate { get; set; }
    }
}