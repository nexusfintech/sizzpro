﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_PlaceofServiceMaster : System.Web.UI.Page
{
    clsPlaceofServicesMaster clsPSM = new clsPlaceofServicesMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            loaddata();
        }
        if(MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptPos.DataSource = clsPSM.GetAllRecord();
        rptPos.DataBind();
    }
    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        msgdiv.Visible = false;
        LinkButton lnkbtn = (LinkButton)sender;
        MySession.Current.AddEditFlag = "EDIT";
        MySession.Current.EditID = Convert.ToInt64(lnkbtn.CommandArgument);
        
        Boolean blnrslt = clsPSM.GetRecordByIDInProperties(MySession.Current.EditID);
        if(blnrslt)
        {
            txtcode.Text = clsPSM.pos_code.ToString();
            txtPlace.Text = clsPSM.pos_place;
        }
    }
    private string validatepage()
    {
        string vldtresult = string.Empty;
        if(txtcode.Text == string.Empty)
        {
            return vldtresult="POS code Required";
        }
        else if (txtPlace.Text == string.Empty)
        {
            return vldtresult = "POS Place Required";
        }
        else
        {
            return vldtresult;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string validationresult = validatepage();
        if(validationresult != string.Empty)
        {
            lblmessage.Text = validationresult;
            msgdiv.Visible = true;
            return;
        }
        clsPSM.pos_code = Convert.ToInt64(txtcode.Text);
        clsPSM.pos_place = txtPlace.Text.ToString();

        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "EDIT")
        {
            blnRslt = clsPSM.Update(MySession.Current.EditID);
            MySession.Current.AddEditFlag = "ADD";
        }
        else
        {
            blnRslt = clsPSM.Save();
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/PlaceofServiceMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsPSM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdDlt_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = (LinkButton)sender;
        Boolean blndltrslt = clsPSM.DeleteByPKID(Convert.ToInt64(lnkbtn.CommandArgument));
        if (blndltrslt)
        {
            MySession.Current.MESSAGE = "Record Deleted Successfully";
            Response.Redirect("~/Master/PlaceofServiceMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsPSM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
}