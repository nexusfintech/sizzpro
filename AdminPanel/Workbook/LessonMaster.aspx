﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LessonMaster.aspx.cs" Inherits="Workbook_LessonMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkMappingtoWB" runat="server" NavigateUrl="~/Workbook/LessonAssignmentMapping.aspx"><i class="glyphicon-book_open"></i>&nbsp;Mapping to Assignment</asp:HyperLink>
            </li>
        </ul>
    </div>
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn" Text="Create New Lesson" OnClick="cmdCreateNew_Click" /></a>
            </li>
            <% if (HttpContext.Current.User.IsInRole("member"))
               { %>
            <li>
                <a>
                    <i class="glyphicon-table"></i>
                    <asp:Button ID="cmdMyRec" runat="server" CssClass="btn" Text="Show My Records" OnClick="cmdMyRec_Click" />
                </a>
            </li>
            <%} %>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="box box-color box-bordered">
        <div class="box-title">
            <h3>
                <i class="glyphicon-projector"></i>
                Lesson
            </h3>
        </div>
        <div class="box-content nopadding">
            <asp:Repeater ID="dtLesson" runat="server" OnItemCommand="dtLesson_ItemCommand" OnItemDataBound="dtLesson_ItemDataBound">
                <HeaderTemplate>
                    <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Short Name</th>
                                <th>Lesson Name</th>
                                <th>Version</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("lsn_short") %></td>
                        <td><%# Eval("lsn_name") %></td>
                        <td style="text-align: center"><%# Eval("lsn_version") %></td>
                        <td style="text-align: center">
                            <div id="action" runat="server" visible="false">
                                <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "lsn_id") %>'><i class="icon-edit"></i> Edit</asp:LinkButton>
                                <asp:LinkButton ID="cmdFreez" runat="server" CssClass="btn btn-primary" CommandName="FREEZ" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "lsn_id") %>'><i class="icon-ban-circle"></i> Freez </asp:LinkButton>
                                <%--&nbsp;
                                <asp:LinkButton ID="cmdShare" runat="server" CssClass="btn btn-primary" CommandName="SHARE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "lsn_id") %>'><i class="icon-share"></i> Share</asp:LinkButton>--%>
                            </div>
                            <div id="share" runat="server" visible="false">
                                <asp:Label ID="Label1" runat="server" CssClass="label label-info"> SHARED </asp:Label>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

