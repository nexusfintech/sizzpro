﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using clsDAL;

public partial class shpweb_LifeProgram_MyReport : System.Web.UI.Page
{
    clsWorkBook clsworkbook = new clsWorkBook();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (hidden.Value != "")
        {
            Session["workbookid"] = hidden.Value;
            Response.Redirect("~/LifeProgram/RChapters.aspx");
            return;
        }
        if (!IsPostBack)
        {
            _GridFill();
        }
    }
    void _GridFill()
    {
        string strusername = User.Identity.Name;
        DL.DataSource = clsworkbook.Read(strusername);
        DL.DataBind();
    }
}