﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MyReport.aspx.cs" Inherits="shpweb_LifeProgram_MyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="glyphicon-dashboard"></i>
                        Select Workbook for Report:
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-column form-bordered'>
                        <asp:Repeater ID="DL" runat="server">
                            <ItemTemplate>
                                <div style="text-align: center; vertical-align: middle; margin: 5px; float: left;">
                                    <img src="../img/workbook.png" style="width: 150px; height: 150px;">
                                    <br />
                                    <a href="RChapters.aspx" id='<%#Eval("step_ID")%>' name="view">
                                        <%#Eval("Name") %>
                                    </a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hidden" name="hidden" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("[name='view']").click(function () {
                var id = this.id;
                $("#<%= hidden.ClientID %>").val(id);
                $("#form1").submit();
                return false;
            });
        });
    </script>
</asp:Content>

