﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsClientInformconcent
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsClientInformconcent()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsClientInformconcent()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int32 PrimeryKey
        {
            get { return _cic_id; }
            set
            {
                _cic_id = value;
                if (clsCMD.Parameters.Contains("@cic_id"))
                { clsCMD.Parameters["@cic_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cic_id", SqlDbType.Int).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int32 _cic_id;
        public Int32 cic_id
        {
            get { return _cic_id; }
            set
            {
                _cic_id = value;
                if (clsCMD.Parameters.Contains("@cic_id"))
                { clsCMD.Parameters["@cic_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cic_id", SqlDbType.Int).Value = value; }
            }
        }
        private Guid _cic_clientid;
        public Guid cic_clientid
        {
            get { return _cic_clientid; }
            set
            {
                _cic_clientid = value;
                if (clsCMD.Parameters.Contains("@cic_clientid"))
                { clsCMD.Parameters["@cic_clientid"].Value = value; }
                else { clsCMD.Parameters.Add("@cic_clientid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _cic_valueflag;
        public String cic_valueflag
        {
            get { return _cic_valueflag; }
            set
            {
                _cic_valueflag = value;
                if (clsCMD.Parameters.Contains("@cic_valueflag"))
                { clsCMD.Parameters["@cic_valueflag"].Value = value; }
                else { clsCMD.Parameters.Add("@cic_valueflag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _cic_value;
        public String cic_value
        {
            get { return _cic_value; }
            set
            {
                _cic_value = value;
                if (clsCMD.Parameters.Contains("@cic_value"))
                { clsCMD.Parameters["@cic_value"].Value = value; }
                else { clsCMD.Parameters.Add("@cic_value", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                cic_id = Convert.ToInt32(dr["cic_id"]);
                cic_clientid = new Guid(dr["cic_clientid"].ToString());
                cic_valueflag = Convert.ToString(dr["cic_valueflag"]);
                cic_value = Convert.ToString(dr["cic_value"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            cic_id = Convert.ToInt32("0");
            cic_clientid = new Guid();
            cic_valueflag = string.Empty;
            cic_value = string.Empty;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.cic_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_ClientInformconcentAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int32 cic_id)
        {
            Boolean blnResult = false;
            this.cic_id = cic_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_ClientInformconcentAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int32 cic_id)
        {
            Boolean blnResult = false;
            this.cic_id = cic_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_ClientInformconcentAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_ClientInformconcentGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsClientInformconcent_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsClientInformconcent_PropertiesList> lstResult = new List<clsClientInformconcent_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].cic_id = Convert.ToInt32(dr["cic_id"]);
                lstResult[i].cic_clientid = new Guid(dr["cic_clientid"].ToString());
                lstResult[i].cic_valueflag = Convert.ToString(dr["cic_valueflag"]);
                lstResult[i].cic_value = Convert.ToString(dr["cic_value"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Guid ClntKey,string vluflg)
        {
            Boolean blnResult = false;
            try
            {
                this.cic_clientid = ClntKey;
                this.cic_valueflag = vluflg;
                clsCMD.CommandText = "sp_ClientInformconcentGetData";
                SetGetSPFlag = "BYCLIENTANDFLAG";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { 
                    blnResult = SetValueToProperties(dtResult.Rows[0]); 
                }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public DataTable GetRecordByClient(Guid ClientId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.cic_clientid = ClientId;
                clsCMD.CommandText = "sp_ClientInformconcentGetData";
                SetGetSPFlag = "BYCLIENTID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public Boolean GetRecordByIDInPropertiesFlag(Guid userId, String strFlag)
        {
            Boolean blnResult = false;
            try
            {
                this.cic_clientid = userId;
                this.cic_valueflag = strFlag;
                clsCMD.CommandText = "sp_ClientInformconcentGetData";
                SetGetSPFlag = "BYCLIENTID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int32 cic_id)
        {
            Boolean blnResult = false;
            try
            {
                this.cic_id = cic_id;
                clsCMD.CommandText = "sp_ClientInformconcentGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsClientInformconcent_PropertiesList
    {
        public Int32 cic_id { get; set; }
        public Guid cic_clientid { get; set; }
        public String cic_valueflag { get; set; }
        public String cic_value { get; set; }
    }
}