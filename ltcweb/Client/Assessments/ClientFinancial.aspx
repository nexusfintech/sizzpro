﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientFinancial.aspx.cs" Inherits="Clients_Assessment_ClientInsurance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cpMainContent" runat="Server">
    <asp:HiddenField ID="AssesmentStep" runat="server" Value="10" />
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-ambulance"></i>
                Client's Insurance Information
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:ClientFinancial ID="ucFin" runat="server" />
        </div>
    </div>
    <div class="row-fluid">
        <hr />
        <div class="span12">
            <asp:Button ID="cmdNext" runat="server" Text="Next Personal History >>" CssClass="btn btn-primary" OnClick="cmdNext_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

