﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class shpweb_LifeProgram_Chapters : System.Web.UI.Page
{
    clsDAL.clsWorkBook clsWB = new clsDAL.clsWorkBook();
    clsWorkbookToChapters clsWTC = new clsWorkbookToChapters();

    public string GetImageName(string strStatus,string strPOSID)
    {
        string strResult = "";
        if (strStatus == "0")
        { 
            Int64 intWBID = Convert.ToInt64(Session["chapterid"]);
            Guid usrGUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            Int64 intPrvStatus = clsWTC.GetPreviousChapterStatus(Convert.ToInt64(strPOSID), intWBID, usrGUID);
            if (intPrvStatus == 2)
            {
                strResult = "unlock.png";
            }
            else {
                strResult = "lock.png";
                strStatus = intPrvStatus.ToString();
            }
        }
        if (strStatus == "1")
        { strResult = "unlock.png"; }
        if (strStatus == "2")
        { strResult = "status.png"; }
        return strResult;
    }

    public string GetLinkImageName(string strStatus, string strPOSID)
    {
        string strResult = "";
        if (strStatus == "0")
        {
            Int64 intWBID = Convert.ToInt64(Session["chapterid"]);
            Guid usrGUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            Int64 intPrvStatus = clsWTC.GetPreviousChapterStatus(Convert.ToInt64(strPOSID), intWBID, usrGUID);
            if (intPrvStatus == 2)
            {
                strResult = "Go.png";
            }
            else
            {
                strResult = "Go_Black.png"; 
                strStatus = intPrvStatus.ToString();
            }
        }
        if (strStatus == "1")
        { strResult = "Go.png"; }
        if (strStatus == "2")
        { strResult = "Go.png"; }
        return strResult;
    }

    public string GetLinkStatus(string strStatus, string strPOSID)
    {
        string strResult = "";
        if (strStatus == "0")
        {
            Int64 intWBID = Convert.ToInt64(Session["chapterid"]);
            Guid usrGUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            Int64 intPrvStatus = clsWTC.GetPreviousChapterStatus(Convert.ToInt64(strPOSID), intWBID, usrGUID);
            if (intPrvStatus == 2)
            {
                strResult = "view";
            }
            else
            {
                strResult = "";
                strStatus = intPrvStatus.ToString();
            }
        }
        if (strStatus == "1")
        { strResult = "view"; }
        if (strStatus == "2")
        { strResult = "view"; }
        return strResult;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (hidden.Value != "")
        {
            Session["lessonid"] = hidden.Value;
            Response.Redirect("~/LifeProgram/Lesson.aspx");
            return;
        }
        if (!IsPostBack)
        {
            _GridFill();
        }
    }

    void _GridFill()
    {
        string username=User.Identity.Name;
        string strusername = User.Identity.Name;
        int intcid = Convert.ToInt32(Session["chapterid"]);
        if (intcid == 0)
        {
            Response.Redirect("~/");
        }
        rptWorkbook.DataSource = clsWB.GetAllRecordByPKID(intcid);
        rptWorkbook.DataBind();

        Guid usrGUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        clsWTC.wtc_userid = usrGUID;
        rptChapters.DataSource = clsWTC.GetChapterWorkbookWise(intcid);
        rptChapters.DataBind();
    }
}