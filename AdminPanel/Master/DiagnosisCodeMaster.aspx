﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DiagnosisCodeMaster.aspx.cs" Inherits="Master_DiagnosisCodeMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn" Text="Add New Code" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Dignosis Codes
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptdgCode" runat="server" OnItemCommand="rptdgCode_ItemCommand" OnItemDataBound="rptdgCode_ItemDataBound">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                        <th>HCC</th>
                                        <th>Terminate Date</th>
                                        <th>Billable</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("dcm_code") %></td>
                                <td><%# Eval("dcm_description") %></td>
                                <td><%# Eval("dcm_codetype") %></td>
                                <td class="qty" style="text-align:center">
                                    <asp:Label ID="lht" runat="server" Text="true" CssClass="label label-success" Visible="false"></asp:Label>
                                    <asp:Label ID="lhf" runat="server" Text="false" CssClass="label label-warning" Visible="false"></asp:Label>
                                </td>
                                <td style="text-align:center"><%# Eval("dcm_terminateddate","{0:dd/MM/yyyy}") %></td>

                                <td class="qty" style="text-align:center">
                                    <asp:Label ID="lpt" runat="server" Text="yes" CssClass="label label-success" Visible="false"></asp:Label>
                                    <asp:Label ID="lpf" runat="server" Text="no" CssClass="label label-warning" Visible="false"></asp:Label>
                                </td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "dcm_id") %>'><i class="icon-edit-sign"></i> &nbsp;Edit</asp:LinkButton>
                                    <asp:LinkButton ID="cmdDlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "dcm_id") %>' rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i> </asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>