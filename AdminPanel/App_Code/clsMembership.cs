﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using clsDAL;
/// <summary>
/// Summary description for clsMembership
/// </summary>
public class clsMembership
{
    clsClientParentMapping clsCPM = new clsClientParentMapping();
	public clsMembership()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string[] GetUserRoles(string strUserName)
    {
        string[] usersInRole = Roles.GetRolesForUser(strUserName.Trim());
        return usersInRole;
    }

    public Guid SetOwner(Guid UserId)
    {
        Guid OwnerId = new Guid();
        MembershipUser mu = Membership.GetUser(UserId);
        if (Roles.IsUserInRole(mu.UserName, "member"))
        {
            OwnerId = (Guid)mu.ProviderUserKey;
        }
        else
        {
            OwnerId = clsCPM.GetParent((Guid)mu.ProviderUserKey);
        }
        return OwnerId;
    }
}