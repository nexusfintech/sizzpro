﻿<%@ Page Title="PhysicalWellBeing" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PhysicalWellBeing.aspx.cs" Inherits="Client_Assessments_PhysicalWellBeing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <asp:HiddenField ID="AssesmentStep" runat="server" Value="12" />
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-github-alt"></i>
                Physical WellBeing Information
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:PhysicalWellBeing ID="physicalwellbeing" runat="server" />
        </div>
    </div>
    <div class="row-fluid">
        <hr />
        <div class="span12">
            <asp:Button ID="cmdNext" runat="server" Text="Next PhysicalSymptoms >>" CssClass="btn btn-primary" OnClick="cmdNext_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
