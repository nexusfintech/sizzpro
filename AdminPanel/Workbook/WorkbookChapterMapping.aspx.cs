﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;

public partial class Workbook_WorkbookChapterMapping : System.Web.UI.Page
{
    clsWorkBook clsWB = new clsWorkBook();
    clsWorkbookToChapters clsWTC = new clsWorkbookToChapters();
    clsClientParentMapping clsCPM = new clsClientParentMapping();

    private void BindCombo(Guid UserId)
    {
        DataTable dtRslt = clsWB.GetAllRecordByMember(UserId);
        ddlWorkbook.Items.Clear();
        ddlWorkbook.SelectedIndex = -1;
        ddlWorkbook.SelectedValue = null;
        ddlWorkbook.ClearSelection();
        ddlWorkbook.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlWorkbook.Items.Add(lst);
        dtRslt.Columns.Add("WorkBook", typeof(string), "wrb_version + ' - '  + wrb_name");
        ddlWorkbook.DataValueField = "wrb_id";
        ddlWorkbook.DataTextField = "WorkBook";
        ddlWorkbook.DataSource = dtRslt;
        ddlWorkbook.DataBind();
    }

    private void BindListView(Int64 intProgID,Guid Userid)
    {
        DataTable dtleft = clsWTC.GetAllByWorkbookIDNotMapped(intProgID, Userid);
        DataTable dtright = clsWTC.GetAllByWorkbookID(intProgID, Userid);

        RptrLeft.DataSource = dtleft;
        RptrLeft.DataBind();

        RptrRight.DataSource = dtright;
        RptrRight.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (User.IsInRole("administrator"))
            {
                BindMemberCombo();
            }
            else
            {
                BindCombo((Guid)Membership.GetUser().ProviderUserKey);
            }
        }
    }

    private void BindMemberCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRole("Member");
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }

    protected void cmdSelect_Click(object sender, EventArgs e)
    {
        Session["wrbid"] = ddlWorkbook.SelectedValue.ToString();
        Int64 intwrbid = Convert.ToInt64(Session["wrbid"]);
        hdWorkbook.Value = ddlWorkbook.SelectedValue.ToString();
        lblBoxHeader.Text = "( " + ddlWorkbook.SelectedItem.Text + " ) Workbook Selected";
        pnlMapping.Visible = true;
        if (User.IsInRole("administrator"))
        {
            BindListView(intwrbid, new Guid(ddlSelectMember.SelectedValue.ToString()));
        }
        else
        {
            BindListView(intwrbid, (Guid)Membership.GetUser().ProviderUserKey);
        }
    }

    protected void ddlSelectMember_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCombo(new Guid(ddlSelectMember.SelectedValue.ToString()));
    }
}