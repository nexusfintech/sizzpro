﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsWorkBook
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsWorkBook()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsWorkBook()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _wrb_id; }
            set
            {
                _wrb_id = value;
                if (clsCMD.Parameters.Contains("@wrb_id"))
                { clsCMD.Parameters["@wrb_id"].Value = value; }
                else { clsCMD.Parameters.Add("@wrb_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _wrb_id;
        public Int64 wrb_id
        {
            get { return _wrb_id; }
            set
            {
                _wrb_id = value;
                if (clsCMD.Parameters.Contains("@wrb_id"))
                { clsCMD.Parameters["@wrb_id"].Value = value; }
                else { clsCMD.Parameters.Add("@wrb_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _wrb_code;
        public Guid wrb_code
        {
            get { return _wrb_code; }
            set
            {
                _wrb_code = value;
                if (clsCMD.Parameters.Contains("@wrb_code"))
                { clsCMD.Parameters["@wrb_code"].Value = value; }
                else { clsCMD.Parameters.Add("@wrb_code", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _wrb_name;
        public String wrb_name
        {
            get { return _wrb_name; }
            set
            {
                _wrb_name = value;
                if (clsCMD.Parameters.Contains("@wrb_name"))
                { clsCMD.Parameters["@wrb_name"].Value = value; }
                else { clsCMD.Parameters.Add("@wrb_name", SqlDbType.NVarChar, 1024).Value = value; }
            }
        }
        private String _wrb_short;
        public String wrb_short
        {
            get { return _wrb_short; }
            set
            {
                _wrb_short = value;
                if (clsCMD.Parameters.Contains("@wrb_short"))
                { clsCMD.Parameters["@wrb_short"].Value = value; }
                else { clsCMD.Parameters.Add("@wrb_short", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _wrb_description;
        public String wrb_description
        {
            get { return _wrb_description; }
            set
            {
                _wrb_description = value;
                if (clsCMD.Parameters.Contains("@wrb_description"))
                { clsCMD.Parameters["@wrb_description"].Size = value.Length; clsCMD.Parameters["@wrb_description"].Value = value; }
                else { clsCMD.Parameters.Add("@wrb_description", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Guid _wrb_UserId;
        public Guid wrb_UserId
        {
            get { return _wrb_UserId; }
            set
            {
                _wrb_UserId = value;
                if (clsCMD.Parameters.Contains("@wrb_UserId"))
                { clsCMD.Parameters["@wrb_UserId"].Value = value; }
                else { clsCMD.Parameters.Add("@wrb_UserId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _wrb_OwnerId;
        public Guid wrb_OwnerId
        {
            get { return _wrb_OwnerId; }
            set
            {
                _wrb_OwnerId = value;
                if (clsCMD.Parameters.Contains("@wrb_OwnerId"))
                { clsCMD.Parameters["@wrb_OwnerId"].Value = value; }
                else { clsCMD.Parameters.Add("@wrb_OwnerId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Boolean _wrb_sts;
        public Boolean wrb_sts
        {
            get { return _wrb_sts; }
            set
            {
                _wrb_sts = value;
                if (clsCMD.Parameters.Contains("@wrb_sts"))
                { clsCMD.Parameters["@wrb_sts"].Value = value; }
                else { clsCMD.Parameters.Add("@wrb_sts", SqlDbType.Bit).Value = value; }
            }
        }

        //wrb_version
        private Int64 _wrb_version;
        public Int64 wrb_version
        {
            get { return _wrb_version; }
            set
            {
                _wrb_version = value;
                if (clsCMD.Parameters.Contains("@wrb_version"))
                { clsCMD.Parameters["@wrb_version"].Value = value; }
                else { clsCMD.Parameters.Add("@wrb_version", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                wrb_id = Convert.ToInt64(dr["wrb_id"]);
                wrb_code = new Guid(dr["wrb_code"].ToString());
                wrb_name = Convert.ToString(dr["wrb_name"]);
                wrb_short = Convert.ToString(dr["wrb_short"]);
                wrb_description = Convert.ToString(dr["wrb_description"]);
                wrb_UserId = new Guid(dr["wrb_UserId"].ToString());
                wrb_OwnerId = new Guid(dr["wrb_OwnerId"].ToString());
                wrb_sts = Convert.ToBoolean(dr["wrb_sts"].ToString());
                wrb_version = Convert.ToInt64(dr["wrb_version"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            wrb_id = Convert.ToInt64("0");
            wrb_code = new Guid();
            wrb_name = Convert.ToString("NA");
            wrb_short = Convert.ToString("NA");
            wrb_description = Convert.ToString("NA");
            wrb_UserId = new Guid();
            wrb_OwnerId = new Guid();
            wrb_version = 0;
            wrb_sts = false;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wrb_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "workbooksAddEditDelete";
                int intRow = Convert.ToInt32(clsCMD.ExecuteNonQuery());
                if (intRow > 0)
                {
                    blnResult = true; 
                    GetSetErrorMessage =intRow.ToString();
                }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 wrb_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wrb_id = wrb_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "workbooksAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean ChangeStatus(Int64 wrb_id,Boolean status,Int64 ver)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wrb_id = wrb_id;
            this.wrb_sts = status;
            this.wrb_version = ver;
            this.AddEditDeleteFlag = "CHANGESTS";
            try
            {
                clsCMD.CommandText = "workbooksAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 wrb_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wrb_id = wrb_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "workbooksAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "workbooksGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByMember(Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "workbooksGetData";
                this.wrb_UserId = MemberId;
                SetGetSPFlag = "ALLBYMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByUser(Guid UserId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "workbooksGetData";
                this.wrb_UserId = UserId;
                SetGetSPFlag = "ALLBYUSER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllNotMapped()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "workbooksGetData";
                SetGetSPFlag = "NOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        public DataTable GetAllNotMappedByMember(Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "workbooksGetData";
                this.wrb_UserId = MemberId;
                SetGetSPFlag = "NOTMAPPEDBYMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByPKID(Int64 wrb_id)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.wrb_id = wrb_id;
                clsCMD.CommandText = "workbooksGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsWorkBook_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsWorkBook_PropertiesList> lstResult = new List<clsWorkBook_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].wrb_code = new Guid(dr["wrb_code"].ToString());
                lstResult[i].wrb_id = Convert.ToInt64(dr["wrb_id"]);
                lstResult[i].wrb_name = Convert.ToString(dr["wrb_name"]);
                lstResult[i].wrb_short = Convert.ToString(dr["wrb_short"]);
                lstResult[i].wrb_description = Convert.ToString(dr["wrb_description"]);
                lstResult[i].wrb_UserId = new Guid(dr["wrb_UserId"].ToString());
                lstResult[i].wrb_OwnerId = new Guid(dr["wrb_OwnerId"].ToString());
                lstResult[i].wrb_sts = Convert.ToBoolean(dr["wrb_sts"].ToString());
                lstResult[i].wrb_version = Convert.ToInt64(dr["wrb_version"]);
                i++;
            }
            return lstResult;
        }
        public Boolean GetRecordByIDInProperties(Int64 wrb_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.wrb_id = wrb_id;
                clsCMD.CommandText = "workbooksGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByCodeInProperties(Guid WrkCode)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.wrb_code =WrkCode ;
                clsCMD.CommandText = "workbooksGetData";
                SetGetSPFlag = "BYCODE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Int64 GetWorkBookVersion(Guid WrkCode)
        {
            OpenConnection();
            Int64 Version=0;
            try
            {
                this.wrb_code = WrkCode;
                clsCMD.CommandText = "workbooksGetData";
                SetGetSPFlag = "FINDVER";
                Version = Convert.ToInt64(clsCMD.ExecuteScalar());
                
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                Version = -1;
            }
            CloseConnection();
            return Version;
        }

        public Boolean CheckDuplicateRow(Guid wrb_code)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.wrb_code = wrb_code;
                clsCMD.CommandText = "workbooksGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }

    public class clsWorkBook_PropertiesList
    {
        public Int64 wrb_id { get; set; }
        public Guid wrb_code { get; set; }
        public String wrb_name { get; set; }
        public String wrb_short { get; set; }
        public String wrb_description { get; set; }
        public Guid wrb_UserId { get; set; }
        public Guid wrb_OwnerId { get; set; }
        public Boolean wrb_sts { get; set; }
        public Int64 wrb_version { get; set; }
    }
}