﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Workbook_ChapterSharing : System.Web.UI.Page
{
    clsUserspermission clsUPM = new clsUserspermission();
    clsChapters clschp = new clsChapters();
    clsobjectsharing clsOBS = new clsobjectsharing();
    private Guid UserId { get; set; }
    private Boolean blnGetResult { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (MySession.Current.EditID != 0)
        {
            blnGetResult = clschp.GetRecordByIDInProperties(MySession.Current.EditID);
            if (!IsPostBack)
            {
                if (blnGetResult)
                {
                    this.UserId = clschp.chp_OwnerId;
                    BindAdmin(MySession.Current.EditID);
                    BindData();
                }
                else
                {
                    Response.Redirect("~/Workbook/ChapterMaster.aspx");
                }
            }
        }
        else
        {
            Response.Redirect("~/Workbook/ChapterMaster.aspx");
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        if (ddladmins.SelectedIndex != 0)
        {
            clsOBS.shr_sharedwithid = new Guid(ddladmins.SelectedValue.ToString());
            if (DropDownList1.SelectedIndex == 0)
            {
                sharechapter(MySession.Current.EditID);
            }
            else
            { 
                // code for paid sharing
            }
        }
        else
        {
            lblmessage.Text = "Please Select Any User";
        }
    }
    public void BindAdmin(Int64 WrkId)
    {
        clsUPM.upm_userid = UserId;
        clsUPM.SetGetSPFlag = "USERLISTFORCH";
        DataTable dtSrc = clsUPM.GetAdminList(WrkId);
        if (dtSrc.Rows.Count > 0)
        {
            ddladmins.Items.Clear();
            ddladmins.SelectedIndex = -1;
            ddladmins.SelectedValue = null;
            ddladmins.ClearSelection();
            ddladmins.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddladmins.Items.Add(lst);
            ddladmins.DataValueField = "Userid";
            ddladmins.DataTextField = "UserName";
            ddladmins.DataSource = dtSrc;
            ddladmins.DataBind();
        }
        else
        {
            ddladmins.Items.Clear();
            ddladmins.SelectedIndex = -1;
            ddladmins.SelectedValue = null;
            ddladmins.ClearSelection();
            ddladmins.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<- No Record Found ->>";
            lst.Selected = true;
            ddladmins.Items.Add(lst);
        }
    }
    public void BindData()
    {
        clsOBS.shr_ownerid = UserId;
        clsOBS.shr_sharedobjid = MySession.Current.EditID;
        clsOBS.SetGetSPFlag = "CHAPTER";
        DataTable dtRslt = clsOBS.GetRecords();
        dtSharing.DataSource = dtRslt;
        dtSharing.DataBind();
    }

    public void sharechapter(Int64 ChpId)
    {
        Boolean blnCRslt = blnGetResult;
        if (blnCRslt)
        {
            List<Int64> LsnList = new clsChapterToLession().GetLsnByChapterID(ChpId, clschp.chp_Userid);
            foreach (Int64 lsn in LsnList)
            {
                List<Int64> AsnList = new clsLessionToAssignment().GetAsnByLessonID(lsn, clschp.chp_Userid);
                foreach (Int64 asn in AsnList)
                {
                    clsOBS.shr_object = "ASSIGNMENT";
                    clsOBS.shr_sharedobjid = asn;
                    clsOBS.shr_ownerid = clschp.chp_Userid;
                    clsOBS.Save();
                }
                clsOBS.shr_object = "LESSON";
                clsOBS.shr_sharedobjid = lsn;
                clsOBS.shr_ownerid = clschp.chp_Userid;
                clsOBS.Save();
            }
            clsOBS.shr_object = "CHAPTER";
            clsOBS.shr_sharedobjid = ChpId;
            clsOBS.shr_ownerid = clschp.chp_Userid;
            clsOBS.Save();
            Response.Redirect("~/Workbook/ChapterSharing.aspx");
        }
        else
        {
            Response.Redirect("~/Workbook/ChapterMaster.aspx");
        }
    }
}