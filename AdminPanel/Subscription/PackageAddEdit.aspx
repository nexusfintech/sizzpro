﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PackageAddEdit.aspx.cs" Inherits="Subscription_ServiceAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkCreateNew" runat="server" NavigateUrl="~/Subscription/PackageMaster.aspx"><i class="icon-retweet"></i>&nbsp;Packages</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="box box-bordered box-color">
        <div class="box-title">
            <h3><i class="icon-edit"></i>Package Add/Edit</h3>
        </div>
        <div class="box-content nopadding">
            <div class="form-horizontal form-bordered">
                <div class="control-group">
                    <label for="txtCourseName" class="control-label">Package Name</label>
                    <div class="controls">
                        <asp:TextBox ID="txtServiceName" runat="server" CssClass="input-xlarge"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtDescription" class="control-label">Package - Duration</label>
                    <div class="controls">
                        <asp:TextBox ID="txtDuration" runat="server" CssClass="input-small"></asp:TextBox>
                        <asp:DropDownList ID="ddlInterval" runat="server" CssClass="select2-me input-medium">
                            <asp:ListItem Value="month">Month</asp:ListItem>
                            <asp:ListItem Value="year">Year</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtDescription" class="control-label">Fix-Charge</label>
                    <div class="controls">
                        <div class="input-append input-prepend">
                            <span class="add-on">$</span>
                            <asp:TextBox ID="txtFixcharge" runat="server" CssClass="input-small"></asp:TextBox>
                        </div>
                        <span class="help-block">Leave it blank if you not want to set it</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtGroupName" class="control-label">Recuring Charge</label>
                    <div class="controls">
                        <div class="input-append input-prepend">
                            <span class="add-on">$</span>
                            <asp:TextBox ID="txtRecurringCharge" runat="server" CssClass="input-small"></asp:TextBox>
                        </div>
                        <span class="help-block">Leave it blank if you not want to set it .</span>
                    </div>
                </div>
                <div class="form-actions">
                    <asp:LinkButton ID="cmdSave" CssClass="btn btn-primary" OnClick="cmdSave_Click" runat="server"><i class="icon-ok"></i> Save</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

