﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PackageSubscriptionAddEdit.aspx.cs" Inherits="Subscription_PackageSubscriptionAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Subscription/PackageSubscriptionMaster.aspx"><i class="icon-retweet"></i>&nbsp;Subscriptions</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </div>
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>New Subscribtion</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <div class="control-group">
                            <label for="textfield" class="control-label">Select Admin</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectMember" runat="server" AutoPostBack="true" CssClass="select2-me input-xlarge" OnSelectedIndexChanged="ddlSelectMember_SelectedIndexChanged"></asp:DropDownList>
                                <span class="help-block">Select Admin For Whom Want to Add New Subscribtion</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Select Package</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectPackage" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                <span class="help-block">
                                    <asp:Label ID="lblmsg" runat="server" Font-Bold="true" ForeColor="Green"></asp:Label>
                                </span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" runat="server" CssClass="btn btn-primary" OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                            <asp:LinkButton ID="cmdCancel" runat="server" CssClass="btn" OnClick="cmdCancel_Click"><i class="icon-remove"></i> Cancle</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
