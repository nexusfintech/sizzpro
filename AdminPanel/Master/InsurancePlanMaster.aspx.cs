﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_InsurancePlan : System.Web.UI.Page
{
    clsInsuranceplanMaster clsIPM = new clsInsuranceplanMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptInsPlan.DataSource = clsIPM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        rptInsPlan.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Master/InsurancePlanAddEdit.aspx");
    }
    protected void rptInsPlan_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if(e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/InsurancePlanAddEdit.aspx");
        }
        if(e.CommandName == "DELETE")
        {
            Boolean blnRslt = clsIPM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (blnRslt)
            {
                MySession.Current.MESSAGE = "Record Deleted Successfully";
                Response.Redirect("~/Master/InsurancePlanMaster.aspx");
            }
            else
            {
                lblmessage.Text = clsIPM.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
    protected void rptInsPlan_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;

            Boolean MHO = Convert.ToBoolean(DataBinder.Eval(DI, "insp_covermhpo"));
            Boolean DIT = Convert.ToBoolean(DataBinder.Eval(DI, "insp_coverdietition"));
            Boolean CHI = Convert.ToBoolean(DataBinder.Eval(DI, "insp_coverchiropractor"));

            if (MHO)
            {
                Label lbl = (Label)e.Item.FindControl("lblmho");
                lbl.Text = "Y";
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lblmho");
                lbl.Text = "N";
            }
            if (DIT)
            {
                Label lbl = (Label)e.Item.FindControl("lbldit");
                lbl.Text = "Y";
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lbldit");
                lbl.Text = "N";
            }
            if (CHI)
            {
                Label lbl = (Label)e.Item.FindControl("lblchi");
                lbl.Text = "Y";
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lblchi");
                lbl.Text = "N";
            }
        }
    }
}