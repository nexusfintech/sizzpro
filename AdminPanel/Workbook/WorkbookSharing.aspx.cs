﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using clsDAL;

public partial class Workbook_WorkbookSharing : System.Web.UI.Page
{
    clsUserspermission clsUPM = new clsUserspermission();
    clsWorkBook clswbk = new clsWorkBook();
    clsobjectsharing clsOBS = new clsobjectsharing();
    clsObjectPricing clsOBP = new clsObjectPricing();

    private Guid UserId { get; set; }
    private Boolean blnGetResult { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (MySession.Current.EditID != 0)
        {
            blnGetResult = clswbk.GetRecordByIDInProperties(MySession.Current.EditID);
            if (!IsPostBack)
            {
                if (blnGetResult)
                {
                    this.UserId = clswbk.wrb_OwnerId;
                    BindAdmin(MySession.Current.EditID);
                    // BindData();
                }
                else
                {
                    Response.Redirect("~/Workbook/WorkbookMaster.aspx");
                }
            }
        }
        else
        {
            Response.Redirect("~/Workbook/WorkbookMaster.aspx");
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        if (ddladmins.SelectedIndex != 0)
        {
            clsOBS.shr_sharedwithid = new Guid(ddladmins.SelectedValue.ToString());
            if (DropDownList1.SelectedIndex == 0)
            {
                if (DropDownList2.SelectedIndex == 0)
                {
                    // free lockked
                    clsOBS.shr_object = "WORKBOOK";
                    clsOBS.shr_sharedobjid = clswbk.wrb_id;
                    clsOBS.shr_ownerid = clswbk.wrb_UserId;
                    clsOBS.Save();
                    MySession.Current.MESSAGE = "Workbook Shared Successfully";
                    Response.Redirect("~/Workbook/WorkbookMaster.aspx");
                }
                else
                {
                    // free unlockked
                    clswbk.wrb_OwnerId = clsOBS.shr_sharedwithid;
                    clswbk.wrb_sts = false;
                    Boolean svrslt = clswbk.Save();
                    if (svrslt)
                    {
                        clswbk.GetRecordByCodeInProperties(clswbk.wrb_code);
                        clsOBS.shr_object = "WORKBOOK";
                        clsOBS.shr_sharedobjid = clswbk.wrb_id;
                        clsOBS.shr_ownerid = clswbk.wrb_UserId;
                        clsOBS.Save();

                        Boolean blnResult = clsOBP.GetRecordByObject(MySession.Current.EditID);
                        clsOBP.op_objid = clswbk.wrb_id;
                        svrslt = clsOBP.Save();

                        MySession.Current.MESSAGE = "Workbook Shared Successfully";
                        Response.Redirect("~/Workbook/WorkbookMaster.aspx");
                    }
                }
            }
            else
            {
                if (DropDownList2.SelectedIndex == 0)
                {
                    // paid lockked
                    clsOBS.shr_object = "WORKBOOK";
                    clsOBS.shr_sharedobjid = clswbk.wrb_id;
                    clsOBS.shr_ownerid = clswbk.wrb_UserId;
                    clsOBS.Save();
                    // make payment process here 
                    MySession.Current.MESSAGE = "Workbook Shared Successfully";
                    Response.Redirect("~/Workbook/WorkbookMaster.aspx");
                }
                else
                {
                    // paid unlockked
                    clswbk.wrb_OwnerId = clsOBS.shr_sharedwithid;
                    clswbk.wrb_sts = false;
                    Boolean svrslt = clswbk.Save();
                    if (svrslt)
                    {
                        clswbk.GetRecordByCodeInProperties(clswbk.wrb_code);
                        clsOBS.shr_object = "WORKBOOK";
                        clsOBS.shr_sharedobjid = clswbk.wrb_id;
                        clsOBS.shr_ownerid = clswbk.wrb_UserId;
                        clsOBS.Save();
                        Response.Redirect("~/Workbook/WorkbookMaster.aspx");
                        // make payment process here
                        MySession.Current.MESSAGE = "Workbook Shared Successfully";
                        Response.Redirect("~/Workbook/WorkbookMaster.aspx");
                    }
                }
            }
        }
        else
        {
            lblmessage.Text = "Please Select Any User";
        }
    }
    public void BindAdmin(Int64 WrkId)
    {
        clsUPM.upm_userid = UserId;
        clsUPM.SetGetSPFlag = "USERLISTFORWB";
        DataTable dtSrc = clsUPM.GetAdminList(WrkId);
        if (dtSrc.Rows.Count > 0)
        {
            ddladmins.Items.Clear();
            ddladmins.SelectedIndex = -1;
            ddladmins.SelectedValue = null;
            ddladmins.ClearSelection();
            ddladmins.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddladmins.Items.Add(lst);
            ddladmins.DataValueField = "Userid";
            ddladmins.DataTextField = "UserName";
            ddladmins.DataSource = dtSrc;
            ddladmins.DataBind();
        }
        else
        {
            ddladmins.Items.Clear();
            ddladmins.SelectedIndex = -1;
            ddladmins.SelectedValue = null;
            ddladmins.ClearSelection();
            ddladmins.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<- No Record Found ->>";
            lst.Selected = true;
            ddladmins.Items.Add(lst);
        }
    }
    public void BindData()
    {
        clsOBS.shr_ownerid = UserId;
        clsOBS.shr_sharedobjid = MySession.Current.EditID;
        clsOBS.SetGetSPFlag = "WORKBOOK";
        //DataTable dtRslt = clsOBS.GetRecords();
        //dtSharing.DataSource = dtRslt;
        //dtSharing.DataBind();
    }
    protected void cmdRemove_Click(object sender, EventArgs e)
    {

    }

    //public void shareworkbook(Int64 WrkbkId)
    //{
    //    Boolean blnWRslt = blnGetResult;
    //    if (blnWRslt)
    //    {
    //        List<Int64> ChpList = new clsWorkbookToChapters().GetChpByWorkbookID(WrkbkId, clswbk.wrb_UserId);
    //        foreach (Int64 chp in ChpList)
    //        {
    //            List<Int64> LsnList = new clsChapterToLession().GetLsnByChapterID(chp, clswbk.wrb_UserId);
    //            foreach (Int64 lsn in LsnList)
    //            {
    //                List<Int64> AsnList = new clsLessionToAssignment().GetAsnByLessonID(lsn, clswbk.wrb_UserId);
    //                foreach (Int64 asn in AsnList)
    //                {
    //                    clsOBS.shr_object = "ASSIGNMENT";
    //                    clsOBS.shr_sharedobjid = asn;
    //                    clsOBS.shr_ownerid = clswbk.wrb_UserId;
    //                    clsOBS.Save();
    //                }
    //                clsOBS.shr_object = "LESSON";
    //                clsOBS.shr_sharedobjid = lsn;
    //                clsOBS.shr_ownerid = clswbk.wrb_UserId;
    //                clsOBS.Save();
    //            }
    //            clsOBS.shr_object = "CHAPTER";
    //            clsOBS.shr_sharedobjid = chp;
    //            clsOBS.shr_ownerid = clswbk.wrb_UserId;
    //            clsOBS.Save();
    //        }
    //        clsOBS.shr_object = "WORKBOOK";
    //        clsOBS.shr_sharedobjid = clswbk.wrb_id;
    //        clsOBS.shr_ownerid = clswbk.wrb_UserId;
    //        clsOBS.Save();
    //    }
    //    else
    //    {
    //        Response.Redirect("~/Workbook/WorkbookMaster.aspx");
    //    }
    //}
}