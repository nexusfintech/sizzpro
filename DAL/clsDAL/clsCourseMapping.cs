﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsCourseMapping
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }
        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsCourseMapping()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsCourseMapping()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _crm_id; }
            set
            {
                _crm_id = value;
                if (clsCMD.Parameters.Contains("@crm_id"))
                { clsCMD.Parameters["@crm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@crm_id", SqlDbType.BigInt).Value = value; }
            }
        }

        private Guid _ByUser;
        public Guid ByUser
        {
            get { return _ByUser; }
            set
            {
                _ByUser = value;
                if (clsCMD.Parameters.Contains("@User"))
                { clsCMD.Parameters["@User"].Value = value; }
                else { clsCMD.Parameters.Add("@User", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _crm_id;
        public Int64 crm_id
        {
            get { return _crm_id; }
            set
            {
                _crm_id = value;
                if (clsCMD.Parameters.Contains("@crm_id"))
                { clsCMD.Parameters["@crm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@crm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _crm_userid;
        public Guid crm_userid
        {
            get { return _crm_userid; }
            set
            {
                _crm_userid = value;
                if (clsCMD.Parameters.Contains("@crm_userid"))
                { clsCMD.Parameters["@crm_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@crm_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _crm_courseid;
        public Int64 crm_courseid
        {
            get { return _crm_courseid; }
            set
            {
                _crm_courseid = value;
                if (clsCMD.Parameters.Contains("@crm_courseid"))
                { clsCMD.Parameters["@crm_courseid"].Value = value; }
                else { clsCMD.Parameters.Add("@crm_courseid", SqlDbType.BigInt).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                crm_id = Convert.ToInt64(dr["crm_id"]);
                crm_userid = new Guid(dr["crm_userid"].ToString());
                crm_courseid = Convert.ToInt64(dr["crm_courseid"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            crm_id = Convert.ToInt64("0");
            crm_userid = new Guid();
            crm_courseid = Convert.ToInt64("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.crm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "CourseUserMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 crm_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.crm_id = crm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "CourseUserMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 crm_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.crm_id = crm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "CourseUserMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByUserID(Guid userid)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.crm_userid = userid;
            this.AddEditDeleteFlag = "DELETEBYUSERID";
            try
            {
                clsCMD.CommandText = "CourseUserMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            this.ByUser = new Guid();
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "CourseUserMappingGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetUserWiseCombo(Guid guUserid)
        {
            OpenConnection();
            this.crm_userid = guUserid;
            this.ByUser = new Guid();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "CourseUserMappingGetData";
                SetGetSPFlag = "FORCOMBO";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordCombo()
        {
            OpenConnection();
            this.ByUser = new Guid();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "CourseUserMappingGetData";
                SetGetSPFlag = "FORADMINCOMBO";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordBYUserid(Guid guUserID, Guid MemId)
        {
            OpenConnection();
            this.ByUser = MemId;
            DataTable dtResult = new DataTable();
            try
            {
                this.crm_userid = guUserID;
                clsCMD.CommandText = "CourseUserMappingGetData";
                SetGetSPFlag = "BYUSERID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordBYUseridNotMapped(Guid guUserID,Guid MemId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.crm_userid = guUserID;
                this.ByUser = MemId;
                clsCMD.CommandText = "CourseUserMappingGetData";
                SetGetSPFlag = "BYUSERIDNOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsCourseMapping_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsCourseMapping_PropertiesList> lstResult = new List<clsCourseMapping_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].crm_id = Convert.ToInt64(dr["crm_id"]);
                lstResult[i].crm_userid = new Guid(dr["crm_userid"].ToString());
                lstResult[i].crm_courseid = Convert.ToInt64(dr["crm_courseid"]);
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 crm_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.crm_id = crm_id;
                clsCMD.CommandText = "CourseUserMappingGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 crm_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.crm_id = crm_id;
                clsCMD.CommandText = "CourseUserMappingGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }

    public class clsCourseMapping_PropertiesList
    {
        public Int64 crm_id { get; set; }
        public Guid crm_userid { get; set; }
        public Int64 crm_courseid { get; set; }
    }
}