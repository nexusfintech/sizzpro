﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Messages.aspx.cs" Inherits="Messages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="span12">
                <div class="box">
                    <div class="box-title">
                        <h3>
                            <i class="icon-user"></i>
                            <asp:Label ID="lblboxheader" runat="server" Text='<%# Session["msg_boxheader"].ToString() %>'></asp:Label>
								</h3>
                    </div>
                    <div class="box-content nopadding">
                        <br />
                        <h5 style="text-align:center">
                            <asp:Literal ID="ltrmessage" runat="server" Text='<%# Session["msg_message"].ToString() %>' >
                            </asp:Literal>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

