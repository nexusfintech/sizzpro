﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsSibling
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsSibling()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsSibling()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _csb_id; }
            set
            {
                _csb_id = value;
                if (clsCMD.Parameters.Contains("@csb_id"))
                { clsCMD.Parameters["@csb_id"].Value = value; }
                else { clsCMD.Parameters.Add("@csb_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _csb_id;
        public Int64 csb_id
        {
            get { return _csb_id; }
            set
            {
                _csb_id = value;
                if (clsCMD.Parameters.Contains("@csb_id"))
                { clsCMD.Parameters["@csb_id"].Value = value; }
                else { clsCMD.Parameters.Add("@csb_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _csb_userid;
        public Guid csb_userid
        {
            get { return _csb_userid; }
            set
            {
                _csb_userid = value;
                if (clsCMD.Parameters.Contains("@csb_userid"))
                { clsCMD.Parameters["@csb_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@csb_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _csb_firstname;
        public String csb_firstname
        {
            get { return _csb_firstname; }
            set
            {
                _csb_firstname = value;
                if (clsCMD.Parameters.Contains("@csb_firstname"))
                { clsCMD.Parameters["@csb_firstname"].Value = value; }
                else { clsCMD.Parameters.Add("@csb_firstname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _csb_middlename;
        public String csb_middlename
        {
            get { return _csb_middlename; }
            set
            {
                _csb_middlename = value;
                if (clsCMD.Parameters.Contains("@csb_middlename"))
                { clsCMD.Parameters["@csb_middlename"].Value = value; }
                else { clsCMD.Parameters.Add("@csb_middlename", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _csb_lastname;
        public String csb_lastname
        {
            get { return _csb_lastname; }
            set
            {
                _csb_lastname = value;
                if (clsCMD.Parameters.Contains("@csb_lastname"))
                { clsCMD.Parameters["@csb_lastname"].Value = value; }
                else { clsCMD.Parameters.Add("@csb_lastname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _csb_age;
        public String csb_age
        {
            get { return _csb_age; }
            set
            {
                _csb_age = value;
                if (clsCMD.Parameters.Contains("@csb_age"))
                { clsCMD.Parameters["@csb_age"].Value = value; }
                else { clsCMD.Parameters.Add("@csb_age", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _csb_gender;
        public String csb_gender
        {
            get { return _csb_gender; }
            set
            {
                _csb_gender = value;
                if (clsCMD.Parameters.Contains("@csb_gender"))
                { clsCMD.Parameters["@csb_gender"].Value = value; }
                else { clsCMD.Parameters.Add("@csb_gender", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _csb_description;
        public String csb_description
        {
            get { return _csb_description; }
            set
            {
                _csb_description = value;
                if (clsCMD.Parameters.Contains("@csb_description"))
                { clsCMD.Parameters["@csb_description"].Size = value.Length; clsCMD.Parameters["@csb_description"].Value = value; }
                else { clsCMD.Parameters.Add("@csb_description", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Boolean _csb_livewithclient;
        public Boolean csb_livewithclient
        {
            get { return _csb_livewithclient; }
            set
            {
                _csb_livewithclient = value;
                if (clsCMD.Parameters.Contains("@csb_livewithclient"))
                { clsCMD.Parameters["@csb_livewithclient"].Value = value; }
                else { clsCMD.Parameters.Add("@csb_livewithclient", SqlDbType.Bit).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                csb_id = Convert.ToInt64(dr["csb_id"]);
                csb_userid = new Guid(dr["csb_userid"].ToString());
                csb_firstname = Convert.ToString(dr["csb_firstname"]);
                csb_middlename = Convert.ToString(dr["csb_middlename"]);
                csb_lastname = Convert.ToString(dr["csb_lastname"]);
                csb_age = Convert.ToString(dr["csb_age"]);
                csb_gender = Convert.ToString(dr["csb_gender"]);
                csb_description = Convert.ToString(dr["csb_description"]);
                csb_livewithclient = Convert.ToBoolean(dr["csb_livewithclient"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            csb_id = Convert.ToInt64("0");
            csb_userid = new Guid();
            csb_firstname = Convert.ToString("NA");
            csb_middlename = Convert.ToString("NA");
            csb_lastname = Convert.ToString("NA");
            csb_age = Convert.ToString("NA");
            csb_gender = Convert.ToString("NA");
            csb_description = Convert.ToString("NA");
            csb_livewithclient = Convert.ToBoolean(false);
        }
        #endregion

        #region Declare Add/Edit/Delete Function
        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.csb_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_clientsiblingsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 csb_id)
        {
            Boolean blnResult = false;
            this.csb_id = csb_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_clientsiblingsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 csb_id)
        {
            Boolean blnResult = false;
            this.csb_id = csb_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_clientsiblingsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_clientsiblingsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllBYUserId(Guid Userid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.csb_userid = Userid;
                clsCMD.CommandText = "sp_clientsiblingsGetData";
                SetGetSPFlag = "BYUSERID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsSibling_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsSibling_PropertiesList> lstResult = new List<clsSibling_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].csb_id = Convert.ToInt64(dr["csb_id"]);
                lstResult[i].csb_userid = new Guid(dr["csb_userid"].ToString());
                lstResult[i].csb_firstname = Convert.ToString(dr["csb_firstname"]);
                lstResult[i].csb_middlename = Convert.ToString(dr["csb_middlename"]);
                lstResult[i].csb_lastname = Convert.ToString(dr["csb_lastname"]);
                lstResult[i].csb_age = Convert.ToString(dr["csb_age"]);
                lstResult[i].csb_gender = Convert.ToString(dr["csb_gender"]);
                lstResult[i].csb_description = Convert.ToString(dr["csb_description"]);
                lstResult[i].csb_livewithclient = Convert.ToBoolean(dr["csb_livewithclient"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 csb_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.csb_id = csb_id;
                clsCMD.CommandText = "sp_clientsiblingsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 csb_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.csb_id = csb_id;
                clsCMD.CommandText = "sp_clientsiblingsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }

    public class clsSibling_PropertiesList
    {
        public Int64 csb_id { get; set; }
        public Guid csb_userid { get; set; }
        public String csb_firstname { get; set; }
        public String csb_middlename { get; set; }
        public String csb_lastname { get; set; }
        public String csb_age { get; set; }
        public String csb_gender { get; set; }
        public String csb_description { get; set; }
        public Boolean csb_livewithclient { get; set; }
    }
}