﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.IO;

public partial class Agreements_AddEditAgreementaspx : System.Web.UI.Page
{
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsAgreements clsAgreement = new clsAgreements();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (User.IsInRole("administrator"))
            {
                BindCombo();
            }
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                binddata();
            }
        }
    }

    private String ValidatePage()
    {
        string message = string.Empty;
        if (User.IsInRole("administrator"))
        {
            if (ddlSelectMember.SelectedIndex == 0)
            {
                message += "Please Select User <br/>";
            }
        }
        if (txtName.Text == string.Empty)
        {
            message += "Please Input Name <br/>";
        }
        if (txtDescription.Text == string.Empty)
        {
            message += "Please Input Description <br/>";
        }
        return message;
    }

    private void BindCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRole("Member");
        DataTable dtusr = clsCPM.GetAllComboByRole("User");
        dtResult.Merge(dtusr);
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }

    private void binddata()
    {
        Boolean isRec = clsAgreement.GetRecordByIDInProperties(MySession.Current.EditID);
        if (isRec == true)
        {
            txtName.Text = clsAgreement.agr_name;
            txtDescription.Text = clsAgreement.agr_content;
            ListItem lst = ddlSelectMember.Items.FindByValue(clsAgreement.agr_createdby.ToString());
            if (lst != null)
            {
                ddlSelectMember.SelectedIndex = ddlSelectMember.Items.IndexOf(lst);
            }
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string StrResult = ValidatePage();
        if (StrResult != string.Empty)
        {
            lblmessage.Text = StrResult;
            return;
        }
        if (User.IsInRole("administrator"))
        {
            clsAgreement.agr_createdby = new Guid(ddlSelectMember.SelectedValue.ToString());
        }
        else
        {
            clsAgreement.agr_createdby = (Guid)Membership.GetUser().ProviderUserKey;
        }
        clsAgreement.agr_content = txtDescription.Text.ToString();
        clsAgreement.agr_name = txtName.Text.ToString();
        Boolean BlnSv = false;
        if (MySession.Current.AddEditFlag == "EDIT")
        {

            BlnSv = clsAgreement.Update(MySession.Current.EditID);
        }
        else
        {
            BlnSv = clsAgreement.Save();
        }
        if (BlnSv == true)
        {
            Response.Redirect("~/Agreements/AgreementMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsAgreement.GetSetErrorMessage;
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Agreements/AgreementMaster.aspx");
    }
}