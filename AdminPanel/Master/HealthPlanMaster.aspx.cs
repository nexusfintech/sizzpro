﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Master_HealthPlanMaster : System.Web.UI.Page
{
    clsPayerMaster clsPM = new clsPayerMaster(); 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptPayer.DataSource = clsPM.GetAllRecord();
        rptPayer.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Master/HealthPlanAddEdit.aspx");
    }
    protected void rptPayer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if(e.CommandName=="EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/HealthPlanAddEdit.aspx");
        }
        if(e.CommandName == "DELETE")
        {
            Boolean blnDltrslt = clsPM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (blnDltrslt)
            {
                MySession.Current.MESSAGE = "Record Deleted Successfully";
                Response.Redirect("~/Master/HealthPlanMaster.aspx");
            }
            else
            {
                lblmessage.Text =clsPM.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
    protected void rptPayer_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;

            Boolean ENR = Convert.ToBoolean(DataBinder.Eval(DI, "pm_ENR"));
            Boolean RTE1 = Convert.ToBoolean(DataBinder.Eval(DI, "pm_RTE1"));
            Boolean RTE2 = Convert.ToBoolean(DataBinder.Eval(DI, "pm_RTE2"));
            Boolean RTS = Convert.ToBoolean(DataBinder.Eval(DI, "pm_RTS"));
            Boolean ERA = Convert.ToBoolean(DataBinder.Eval(DI, "pm_ERA"));
            Boolean SEC = Convert.ToBoolean(DataBinder.Eval(DI, "pm_SEC"));

            if (ENR)
            {
                Label lbl = (Label)e.Item.FindControl("lblenr");
                lbl.Text = "Y";
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lblenr");
                lbl.Text = "N";
            }

            if (RTE1)
            {
                Label lbl = (Label)e.Item.FindControl("lblrte1");
                lbl.Text = "Y";
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lblrte1");
                lbl.Text = "N";
            }
            if (RTE2)
            {
                Label lbl = (Label)e.Item.FindControl("lblrte2");
                lbl.Text = "Y";
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lblrte2");
                lbl.Text = "N";
            }
            if (RTS)
            {
                Label lbl = (Label)e.Item.FindControl("lblrts");
                lbl.Text = "Y";
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lblrts");
                lbl.Text = "N";
            }
            if (ERA)
            {
                Label lbl = (Label)e.Item.FindControl("lblera");
                lbl.Text = "Y";
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lblera");
                lbl.Text = "N";
            }
            if (SEC)
            {
                Label lbl = (Label)e.Item.FindControl("lblsec");
                lbl.Text = "Y";
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lblsec");
                lbl.Text = "N";
            }
        }
    }
}