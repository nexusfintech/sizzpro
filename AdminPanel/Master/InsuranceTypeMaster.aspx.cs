﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Master_InsuranceType : System.Web.UI.Page
{
    clsInsuranceTypeMaster clsITM = new clsInsuranceTypeMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptInsTyp.DataSource = clsITM.GetAllRecord();
        rptInsTyp.DataBind();
    }
    protected void cmdDlt_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = (LinkButton)sender;
        Boolean blndltrslt = clsITM.DeleteByPKID(Convert.ToInt64(lnkbtn.CommandArgument));
        if (blndltrslt)
        {
            MySession.Current.MESSAGE = "Record Deleted Successfully";
            Response.Redirect("~/Master/InsuranceTypeMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsITM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    private string validatepage()
    {
        string vldtresult = string.Empty;
        if (txtInsType.Text == string.Empty)
        {
            return vldtresult = "Insurance Type is Required";
        }
        else
        {
            return vldtresult;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string validationresult = validatepage();
        if (validationresult != string.Empty)
        {
            lblmessage.Text = validationresult;
            msgdiv.Visible = true;
            return;
        }

        clsITM.itm_insurance = txtInsType.Text;
        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "EDIT")
        {
            blnRslt = clsITM.Update(MySession.Current.EditID);
            MySession.Current.AddEditFlag = "ADD";
        }
        else
        {
            blnRslt = clsITM.Save();
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/InsuranceTypeMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsITM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
}