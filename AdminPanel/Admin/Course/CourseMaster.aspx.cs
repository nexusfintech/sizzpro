﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Admin_Course_CourseMaster : System.Web.UI.Page
{
    clsAppointmentCourse clsCRS = new clsAppointmentCourse();

    private void BindGrid()
    {
        //dtAssignment.DataSource = clsCRS.GetAllRecord();
        if (User.IsInRole("administrator"))
        {
            dtAssignment.DataSource = clsCRS.GetAllRecord();
        }
        else
        {
            dtAssignment.DataSource = clsCRS.GetAllRecordByMemberKey((Guid)Membership.GetUser().ProviderUserKey);
        }
        dtAssignment.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGrid();
        }
    }

    protected void dtAssignment_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = e.CommandName;
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Admin/Course/CourseAddEdit.aspx");
        }
    }

    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Admin/Course/CourseAddEdit.aspx");
    }
}