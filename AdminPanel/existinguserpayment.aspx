﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSite.master" AutoEventWireup="true" CodeFile="existinguserpayment.aspx.cs" Inherits="existinguserpayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid" style="border: 1px solid #0072D6;">
        <div class="span12">
            <%--<asp:Literal ID="ltrPageBody" runat="server"></asp:Literal>--%>
            <h4 style="text-align: center">Payment</h4>
            <hr />
            <div class="form-horizontal form-bordered">
                <div class="control-group">
                    <label for="txtusername" class="control-label">UserName</label>
                    <div class="controls">
                        <asp:TextBox ID="txtusername" runat="server" CssClass="input-large" placeholder="sizzpro username"></asp:TextBox>
                        <span class="help-block">enter your username of sizzpro account</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtpassword" class="control-label">Password </label>
                    <div class="controls">
                        <asp:TextBox ID="txtpassword" runat="server" CssClass="input-large" TextMode="Password" placeholder="sizzpro password"></asp:TextBox>
                        <span class="help-block">your password of sizzpro account</span>
                    </div>
                </div>
                <div class="form-actions">
                    <asp:Button ID="cmdLogin" runat="server" CssClass="btn btn-primary" Text="Log In Now" OnClick="cmdLogin_Click" />
                    <%--<asp:Button ID="cmdCancel" runat="server" CssClass="btn" Text="Return To Cart" OnClick="cmdCancel_Click" />--%>
                    <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
