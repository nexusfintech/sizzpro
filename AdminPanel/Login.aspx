﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSite.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
       <div style="margin: 0 auto; margin-top: 5px; width: 1160px; min-height: 400px; background-color: white;">
                <div class="row-fluid">
                    <%--<div class="span8">
                        <div class="fadein">
                            <img src="img/banner/pic1.jpg" style="width: 770px; height: 340px; border: 1px solid #0072D6" />
                        </div>
                    </div>--%>
                    <div class="span12">
                        <div class="spanlogin">
                            <div class="wrapper">
                                <div class="login-body">
                                    <h2>SIGN IN</h2>
                                    <div>
                                        <div class="control-group">
                                            <div class="email controls">
                                                <asp:TextBox ID="txtusername" runat="server" placeholder="Username or email" class='input-block-level' data-rule-required="true" data-rule-email="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="email controls">
                                                <asp:TextBox ID="txtpassword" runat="server" placeholder="Password" class='input-block-level' TextMode="Password" data-rule-required="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="submit">
                                            <asp:Button ID="cmdlogin" runat="server" Text="Sign in" class="btn btn-primary" OnClick="cmdlogin_Click" />
                                        </div>
                                        <div class="msg">
                                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="forget">
                                        <a href="Users/UserPasswordForgot.aspx?action=reset"><span>Forgot password?</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</asp:Content>