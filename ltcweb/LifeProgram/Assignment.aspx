﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Assignment.aspx.cs" Inherits="LifeProgram_Assignment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h5>You are in, <a href="../Client/Dashboard.aspx">Home</a>  &gt;&gt; 
                        <a href="Workbook.aspx">Workbook</a> &gt;&gt;<a href="Chapters.aspx"> Chapters</a>&gt;&gt;<a href="Lesson.aspx"> Lessons</a>&gt;&gt;<a href="Assignment.aspx"> Assignment</a>
            </h5>
            <hr />
            <asp:Repeater ID="rptLesson" runat="server">
                <ItemTemplate>
                    <h2><i class="icon-pencil"></i> <%# Eval("lsn_name") %> </h2>
                    <div>
                        <%# Eval("lsn_description") %>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <hr />
            <asp:Repeater ID="rptAssignment" runat="server">
                <ItemTemplate>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="box box-bordered box-color">
                                <div class="box-title">
                                    <h3><i class="icon-pencil"></i>
                                       Assignment : <%# Eval("asn_name") %>
                                    </h3>
                                </div>
                                <div class="box-content">
                                    <div class='form-horizontal form-column form-bordered'>
                                        <div>
                                            <%# Eval("asn_description") %>
                                        </div>
                                        <div>
                                            <i><b>Answer:</b></i>
                                            <asp:TextBox ID="txtAnswer" runat="server" TextMode="MultiLine" Width="99%" Text='<%# Eval("wns_answer") %>'>
                                            </asp:TextBox>
                                            <asp:HiddenField ID="asnid" runat="server" Value='<%# Eval("asn_id") %>' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                </ItemTemplate>
            </asp:Repeater>

            <%--<asp:Repeater ID="RTAssignment" runat="server">
                <HeaderTemplate>
                    <table>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <h2><i class="icon-book"></i>&nbsp;<%#Eval("Title") %></h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3 style="text-decoration: underline;">Introduction</h3>
                            <p><%#Eval("Introduction") %></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3 style="text-decoration: underline;">Summary</h3>
                            <p><%#Eval("Summary") %></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3 style="text-decoration: underline;">Section</h3>
                            <p><%#Eval("section") %></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3 style="text-decoration: underline;">Assignment</h3>
                            <p><%#Eval("Description") %></p>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>--%>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <asp:Button ID="btn_update_ans" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="btn_update_ans_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
    <%--<script type="text/javascript" charset='UTF-8'>
        $(document).ready(function () {
            CKEDITOR.replace('<%= txtanswer.ClientID %>',
                {
                    uiColor: '#B7B0B7',
                    height: '250px',
                    htmlEncodeOutput: false
                });
        });
    </script>--%>
</asp:Content>

