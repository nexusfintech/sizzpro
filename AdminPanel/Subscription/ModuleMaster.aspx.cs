﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;

public partial class Subscription_ModuleMaster : System.Web.UI.Page
{
    clssubscriptionmodulemaster clsSMM = new clssubscriptionmodulemaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            loaddata();
        }
    }

    private void loaddata()
    {
        MySession.Current.AddEditFlag = string.Empty;
        txtModuleName.Text = string.Empty;
        DataTable dtResult = clsSMM.GetAllRecord();
        dtSubModul.DataSource = dtResult;
        dtSubModul.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Subscription/ModuleAddEdit.aspx");
    }
    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        LinkButton btnlnk = (LinkButton)sender;
        if (btnlnk.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID =Convert.ToInt64(btnlnk.CommandArgument);
            fillform();
        }
        else
        {
            Boolean blnResult = clsSMM.DeleteByPKID(Convert.ToInt64(btnlnk.CommandArgument));
            if(blnResult)
            {
                loaddata();
            }
        }
    }

    private void fillform()
    {
        Boolean blnResult = clsSMM.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnResult == true)
        {
            txtModuleName.Text = clsSMM.mdl_name;
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        if (txtModuleName.Text.ToString() == string.Empty)
        {
            lblmessage.Text = "Please propvide proper module name";
            return;
        }
        Boolean blnResult;
        if (MySession.Current.AddEditFlag.ToString() == "EDIT")
        {
            clsSMM.mdl_name = txtModuleName.Text.ToString();
            blnResult = clsSMM.Update(MySession.Current.EditID);
        }
        else
        {
            clsSMM.mdl_name = txtModuleName.Text.ToString();
            blnResult = clsSMM.Save();
        }
        if (blnResult == true)
        {
            lblmessage.Text = "Record Saved Successfully";
            loaddata();
        }
        else
        {
            lblmessage.Text = clsSMM.GetSetErrorMessage;
        }
    }
}