﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsAppointmentCourse
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsAppointmentCourse()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsAppointmentCourse()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _crs_id; }
            set
            {
                _crs_id = value;
                if (clsCMD.Parameters.Contains("@crs_id"))
                { clsCMD.Parameters["@crs_id"].Value = value; }
                else { clsCMD.Parameters.Add("@crs_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _crs_id;
        public Int64 crs_id
        {
            get { return _crs_id; }
            set
            {
                _crs_id = value;
                if (clsCMD.Parameters.Contains("@crs_id"))
                { clsCMD.Parameters["@crs_id"].Value = value; }
                else { clsCMD.Parameters.Add("@crs_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _crs_userid;
        public Guid crs_userid
        {
            get { return _crs_userid; }
            set
            {
                _crs_userid = value;
                if (clsCMD.Parameters.Contains("@crs_userid"))
                { clsCMD.Parameters["@crs_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@crs_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _crs_coursename;
        public String crs_coursename
        {
            get { return _crs_coursename; }
            set
            {
                _crs_coursename = value;
                if (clsCMD.Parameters.Contains("@crs_coursename"))
                { clsCMD.Parameters["@crs_coursename"].Value = value; }
                else { clsCMD.Parameters.Add("@crs_coursename", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _crs_coursedescription;
        public String crs_coursedescription
        {
            get { return _crs_coursedescription; }
            set
            {
                _crs_coursedescription = value;
                if (clsCMD.Parameters.Contains("@crs_coursedescription"))
                { clsCMD.Parameters["@crs_coursedescription"].Size = value.Length; clsCMD.Parameters["@crs_coursedescription"].Value = value; }
                else { clsCMD.Parameters.Add("@crs_coursedescription", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private String _crs_groupname;
        public String crs_groupname
        {
            get { return _crs_groupname; }
            set
            {
                _crs_groupname = value;
                if (clsCMD.Parameters.Contains("@crs_groupname"))
                { clsCMD.Parameters["@crs_groupname"].Value = value; }
                else { clsCMD.Parameters.Add("@crs_groupname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _crs_interval;
        public Int64 crs_interval
        {
            get { return _crs_interval; }
            set
            {
                _crs_interval = value;
                if (clsCMD.Parameters.Contains("@crs_interval"))
                { clsCMD.Parameters["@crs_interval"].Value = value; }
                else { clsCMD.Parameters.Add("@crs_interval", SqlDbType.BigInt).Value = value; }
            }
        }
        private Boolean _crs_status;
        public Boolean crs_status
        {
            get { return _crs_status; }
            set
            {
                _crs_status = value;
                if (clsCMD.Parameters.Contains("@crs_status"))
                { clsCMD.Parameters["@crs_status"].Value = value; }
                else { clsCMD.Parameters.Add("@crs_status", SqlDbType.Bit).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                crs_id = Convert.ToInt64(dr["crs_id"]);
                if (dr["crs_userid"] != DBNull.Value || dr["crs_userid"].ToString().Trim() != "")
                {
                    crs_userid = new Guid(dr["crs_userid"].ToString());
                }
                crs_coursename = Convert.ToString(dr["crs_coursename"]);
                crs_coursedescription = Convert.ToString(dr["crs_coursedescription"]);
                crs_groupname = Convert.ToString(dr["crs_groupname"]);
                crs_interval = Convert.ToInt64(dr["crs_interval"]);
                crs_status = Convert.ToBoolean(dr["crs_status"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            crs_id = Convert.ToInt64("0");
            crs_userid = new Guid();
            crs_coursename = Convert.ToString("NA");
            crs_coursedescription = Convert.ToString("NA");
            crs_groupname = Convert.ToString("NA");
            crs_interval = Convert.ToInt64("0");
            crs_status = Convert.ToBoolean(false);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.crs_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "AppointmentCourseAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 crs_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.crs_id = crs_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "AppointmentCourseAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 crs_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.crs_id = crs_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "AppointmentCourseAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "AppointmentCourseGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByMemberKey(Guid crs_userid)
        {
            OpenConnection();
            this.crs_userid = crs_userid;
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "AppointmentCourseGetData";
                SetGetSPFlag = "BYMEMBERKEY";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByStaff(Guid crs_userid)
        {
            OpenConnection();
            this.crs_userid = crs_userid;
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "AppointmentCourseGetData";
                SetGetSPFlag = "BYUSERKEY";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordForCombo(Guid guUserID)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "AppointmentCourseGetData";
                SetGetSPFlag = "FORCOMBO";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsAppointmentCourse_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsAppointmentCourse_PropertiesList> lstResult = new List<clsAppointmentCourse_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].crs_id = Convert.ToInt64(dr["crs_id"]);
                lstResult[i].crs_userid = new Guid(dr["crs_userid"].ToString());
                lstResult[i].crs_coursename = Convert.ToString(dr["crs_coursename"]);
                lstResult[i].crs_coursedescription = Convert.ToString(dr["crs_coursedescription"]);
                lstResult[i].crs_groupname = Convert.ToString(dr["crs_groupname"]);
                lstResult[i].crs_minutes = Convert.ToInt64(dr["crs_minutes"]);
                lstResult[i].crs_interval = Convert.ToInt64(dr["crs_interval"]);
                lstResult[i].crs_status = Convert.ToBoolean(dr["crs_status"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 crs_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.crs_id = crs_id;
                clsCMD.CommandText = "AppointmentCourseGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 crs_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.crs_id = crs_id;
                clsCMD.CommandText = "AppointmentCourseGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion
    }

    public class clsAppointmentCourse_PropertiesList
    {
        public Int64 crs_id { get; set; }
        public Guid crs_userid { get; set; }
        public String crs_coursename { get; set; }
        public String crs_coursedescription { get; set; }
        public String crs_groupname { get; set; }
        public Int64 crs_minutes { get; set; }
        public Int64 crs_interval { get; set; }
        public Boolean crs_status { get; set; }
    }
}