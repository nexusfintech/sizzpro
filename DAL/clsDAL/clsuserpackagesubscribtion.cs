﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsuserpackagesubscribtion
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsuserpackagesubscribtion()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsuserpackagesubscribtion()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _us_id; }
            set
            {
                _us_id = value;
                if (clsCMD.Parameters.Contains("@us_id"))
                { clsCMD.Parameters["@us_id"].Value = value; }
                else { clsCMD.Parameters.Add("@us_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _us_id;
        public Int64 us_id
        {
            get { return _us_id; }
            set
            {
                _us_id = value;
                if (clsCMD.Parameters.Contains("@us_id"))
                { clsCMD.Parameters["@us_id"].Value = value; }
                else { clsCMD.Parameters.Add("@us_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _us_userid;
        public Guid us_userid
        {
            get { return _us_userid; }
            set
            {
                _us_userid = value;
                if (clsCMD.Parameters.Contains("@us_userid"))
                { clsCMD.Parameters["@us_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@us_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _us_packageid;
        public Int64 us_packageid
        {
            get { return _us_packageid; }
            set
            {
                _us_packageid = value;
                if (clsCMD.Parameters.Contains("@us_packageid"))
                { clsCMD.Parameters["@us_packageid"].Value = value; }
                else { clsCMD.Parameters.Add("@us_packageid", SqlDbType.BigInt).Value = value; }
            }
        }
        private DateTime _us_subscribeddate;
        public DateTime us_subscribeddate
        {
            get { return _us_subscribeddate; }
            set
            {
                _us_subscribeddate = value;
                if (clsCMD.Parameters.Contains("@us_subscribeddate"))
                { clsCMD.Parameters["@us_subscribeddate"].Value = value; }
                else { clsCMD.Parameters.Add("@us_subscribeddate", SqlDbType.DateTime).Value = value; }
            }
        }
        private Boolean _us_status;
        public Boolean us_status
        {
            get { return _us_status; }
            set
            {
                _us_status = value;
                if (clsCMD.Parameters.Contains("@us_status"))
                { clsCMD.Parameters["@us_status"].Value = value; }
                else { clsCMD.Parameters.Add("@us_status", SqlDbType.Bit).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                us_id = Convert.ToInt64(dr["us_id"]);
                us_userid = new Guid(dr["us_userid"].ToString());
                us_packageid = Convert.ToInt64(dr["us_packageid"]);
                us_subscribeddate = Convert.ToDateTime(dr["us_subscribeddate"]);
                us_status = Convert.ToBoolean(dr["us_status"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            us_id = Convert.ToInt64("0");
            us_userid = new Guid();
            us_packageid = Convert.ToInt64("0");
            us_subscribeddate = Convert.ToDateTime(DateTime.Now);
            us_status = Convert.ToBoolean(false);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.us_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_userpackagesubscribtionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 us_id)
        {
            Boolean blnResult = false;
            this.us_id = us_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_userpackagesubscribtionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 us_id)
        {
            Boolean blnResult = false;
            this.us_id = us_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_userpackagesubscribtionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_userpackagesubscribtionGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsuserpackagesubscribtion_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsuserpackagesubscribtion_PropertiesList> lstResult = new List<clsuserpackagesubscribtion_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].us_id = Convert.ToInt64(dr["us_id"]);
                lstResult[i].us_userid = new Guid(dr["us_userid"].ToString());
                lstResult[i].us_packageid = Convert.ToInt64(dr["us_packageid"]);
                lstResult[i].us_subscribeddate = Convert.ToDateTime(dr["us_subscribeddate"]);
                lstResult[i].us_status = Convert.ToBoolean(dr["us_status"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 us_id)
        {
            Boolean blnResult = false;
            try
            {
                this.us_id = us_id;
                clsCMD.CommandText = "sp_userpackagesubscribtionGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 us_id)
        {
            Boolean blnResult = false;
            try
            {
                this.us_id = us_id;
                clsCMD.CommandText = "sp_userpackagesubscribtionGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }



        #endregion

    }
    public class clsuserpackagesubscribtion_PropertiesList
    {
        public Int64 us_id { get; set; }
        public Guid us_userid { get; set; }
        public Int64 us_packageid { get; set; }
        public DateTime us_subscribeddate { get; set; }
        public Boolean us_status { get; set; }
    }
}