﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="UserManage.aspx.cs" Inherits="Users_UserManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkProfile" runat="server" Text="Profile"></asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-reorder"></i>
                User's Utility Management
            </h3>
        </div>
        <div class="box-content">
            <div class="row-fluid">
                <div class="span12">
                    <div class="box box-bordered box-color">
                        <div class="box-title">
                            <h3><i class="icon-key"></i>Change Password</h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class='form-horizontal form-bordered'>
                                <div class="control-group">
                                    <label for="password" class="control-label">Old Password</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtOldPassword" runat="server" placeholder="Old Password " class="complexify-me input-xlarge" TextMode="Password"></asp:TextBox>
                                        <span class="help-block">Password Strength
                                            <div class="progress progress-info">
                                                <div class="bar bar-red" style="width: 0%"></div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="password" class="control-label">New Password</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtNewPassword" runat="server" placeholder="New Password" class="complexify-me input-xlarge" TextMode="Password"></asp:TextBox>
                                        <span class="help-block">Password Strength
                                            <div class="progress progress-info">
                                                <div class="bar bar-red" style="width: 0%"></div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="password" class="control-label">Conform Password</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtConformPAssword" runat="server" placeholder="Confirm New Password" class="complexify-me input-xlarge" TextMode="Password"></asp:TextBox>
                                        <span class="help-block">Password Strength
                                            <div class="progress progress-info">
                                                <div class="bar bar-red" style="width: 0%"></div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <asp:Button ID="cmdChnagePass" runat="server" CssClass="btn btn-primary" Text="Change Password" OnClick="cmdChnagePass_Click" />
                                </div>
                                <div class="control-group">
                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <div class="box box-bordered box-color">
                        <div class="box-title">
                            <h3><i class="icon-key"></i>Change Email</h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class='form-horizontal form-bordered'>
                                <div class="control-group">
                                    <label for="textfield" class="control-label">Email Address</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Email input" class="complexify-me input-xlarge"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <asp:Button ID="cmdChangeEmail" runat="server" CssClass="btn btn-primary" Text="Change Email" OnClick="cmdChangeEmail_Click" />
                                </div>
                                <div class="control-group">
                                    <asp:Label ID="lblEmailChange" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="span6">
                    <div class="box box-bordered box-color">
                        <div class="box-title">
                            <h3><i class="icon-key"></i>Change Username</h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class='form-horizontal form-bordered'>
                                <div class="control-group">
                                    <label for="textfield" class="control-label">Username</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtUserName" runat="server" placeholder="New Username" class="complexify-me input-xlarge"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <asp:Button ID="btnChangUserName" runat="server" CssClass="btn btn-primary" Text="Change UserName" OnClick="btnChangUserName_Click" />
                                </div>
                                <div class="control-group">
                                    <asp:Label ID="lblUserName" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

