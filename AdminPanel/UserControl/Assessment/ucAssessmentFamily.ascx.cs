﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class UserControl_Assessment_AssessmentFamily : System.Web.UI.UserControl
{
    clsInitialAssessment clsASS = new clsInitialAssessment();
    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    private void EditEntry()
    {
        Boolean blnEdit = false;
        Guid objKey = new Guid(strMemberkey);
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, rdostepperents.ClientID);
        if (blnEdit == true)
        {
            ListItem li = rdostepperents.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                rdostepperents.SelectedIndex = rdostepperents.Items.IndexOf(li);
            }
        }

        #region Save textbox value
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtBirthOrderBio.ClientID);
        if (blnEdit == true)
        { txtBirthOrderBio.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtbrifdescfamorigion.ClientID);
        if (blnEdit == true)
        {txtbrifdescfamorigion.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtbrifdescromrelhistory.ClientID);
        if (blnEdit == true)
        {txtbrifdescromrelhistory.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtchildrenotherexplain.ClientID);
        if (blnEdit == true)
        {txtchildrenotherexplain.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtdecrelwithparent.ClientID);
        if (blnEdit == true)
        {txtdecrelwithparent.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtdescbiologiparentrel.ClientID);
        if (blnEdit == true)
        {txtdescbiologiparentrel.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtDescfrindsupport.ClientID);
        if (blnEdit == true)
        {txtDescfrindsupport.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtdescstscommunity.ClientID);
        if (blnEdit == true)
        {txtdescstscommunity.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtfamilybrother.ClientID);
        if (blnEdit == true)
        {txtfamilybrother.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtfamilySisters.ClientID);
        if (blnEdit == true)
        {txtfamilySisters.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtsigfamperunderbetter.ClientID);
        if (blnEdit == true)
        {txtsigfamperunderbetter.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtStepSiblings.ClientID);
        if (blnEdit == true)
        { txtStepSiblings.Text = clsASS.ini_valuetext; }

        #endregion

        #region Save checkbox value
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkAcademicschoolproblems.ClientID);
        if (blnEdit == true)
        { chkAcademicschoolproblems.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkAlcoholabuse.ClientID);
        if (blnEdit == true)
        { chkAlcoholabuse.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkBehaviorproblems.ClientID);
        if (blnEdit == true)
        { chkBehaviorproblems.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkDrugabuse.ClientID);
        if (blnEdit == true)
        { chkDrugabuse.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkEatingdisorder.ClientID);
        if (blnEdit == true)
        { chkEatingdisorder.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkEmotionalproblems.ClientID);
        if (blnEdit == true)
        { chkEmotionalproblems.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkfamilyboth.ClientID);
        if (blnEdit == true)
        { chkfamilyboth.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkfamilyFather.ClientID);
        if (blnEdit == true)
        { chkfamilyFather.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkfamilymother.ClientID);
        if (blnEdit == true)
        { chkfamilymother.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkFamilyproblems.ClientID);
        if (blnEdit == true)
        { chkFamilyproblems.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkFinancialproblems.ClientID);
        if (blnEdit == true)
        { chkFinancialproblems.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkHappychildhood.ClientID);
        if (blnEdit == true)
        { chkHappychildhood.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkLegaltrouble.ClientID);
        if (blnEdit == true)
        { chkLegaltrouble.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkOtherPleaseexplain.ClientID);
        if (blnEdit == true)
        { chkOtherPleaseexplain.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkPhysicallyabused.ClientID);
        if (blnEdit == true)
        { chkPhysicallyabused.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkSexuallyabused.ClientID);
        if (blnEdit == true)
        { chkSexuallyabused.Checked = clsASS.ini_valuebit; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkUnhappychildhood.ClientID);
        if (blnEdit == true)
        { chkUnhappychildhood.Checked = clsASS.ini_valuebit; }
        #endregion

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            EditEntry();
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Boolean blnSave = false;
        try
        {
            Guid objKey = new Guid(strMemberkey);
            clsASS.ini_userid = objKey;

            clsASS.ini_valueflag = chkAcademicschoolproblems.ClientID;
            clsASS.ini_valuebit = chkAcademicschoolproblems.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkAlcoholabuse.ClientID;
            clsASS.ini_valuebit = chkAlcoholabuse.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkBehaviorproblems.ClientID;
            clsASS.ini_valuebit = chkBehaviorproblems.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkDrugabuse.ClientID;
            clsASS.ini_valuebit = chkDrugabuse.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkEatingdisorder.ClientID;
            clsASS.ini_valuebit = chkEatingdisorder.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkEmotionalproblems.ClientID;
            clsASS.ini_valuebit = chkEmotionalproblems.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkfamilyboth.ClientID;
            clsASS.ini_valuebit = chkfamilyboth.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkfamilyFather.ClientID;
            clsASS.ini_valuebit = chkfamilyFather.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkfamilymother.ClientID;
            clsASS.ini_valuebit = chkfamilymother.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkFamilyproblems.ClientID;
            clsASS.ini_valuebit = chkFamilyproblems.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkFinancialproblems.ClientID;
            clsASS.ini_valuebit = chkFinancialproblems.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkHappychildhood.ClientID;
            clsASS.ini_valuebit = chkHappychildhood.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkLegaltrouble.ClientID;
            clsASS.ini_valuebit = chkLegaltrouble.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkOtherPleaseexplain.ClientID;
            clsASS.ini_valuebit = chkOtherPleaseexplain.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkPhysicallyabused.ClientID;
            clsASS.ini_valuebit = chkPhysicallyabused.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkSexuallyabused.ClientID;
            clsASS.ini_valuebit = chkSexuallyabused.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkUnhappychildhood.ClientID;
            clsASS.ini_valuebit = chkUnhappychildhood.Checked;
            blnSave = clsASS.Save();

            ListItem li = rdostepperents.SelectedItem;
            if (li != null)
            {
                clsASS.ini_valueflag = rdostepperents.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(rdostepperents.SelectedValue);
                blnSave = clsASS.Save();
            }

            clsASS.ini_valueflag = txtBirthOrderBio.ClientID;
            clsASS.ini_valuetext = txtBirthOrderBio.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtbrifdescfamorigion.ClientID;
            clsASS.ini_valuetext = txtbrifdescfamorigion.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtbrifdescromrelhistory.ClientID;
            clsASS.ini_valuetext = txtbrifdescromrelhistory.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtchildrenotherexplain.ClientID;
            clsASS.ini_valuetext = txtchildrenotherexplain.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtdecrelwithparent.ClientID;
            clsASS.ini_valuetext = txtdecrelwithparent.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtdescbiologiparentrel.ClientID;
            clsASS.ini_valuetext = txtdescbiologiparentrel.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtDescfrindsupport.ClientID;
            clsASS.ini_valuetext = txtDescfrindsupport.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtdescstscommunity.ClientID;
            clsASS.ini_valuetext = txtdescstscommunity.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtfamilybrother.ClientID;
            clsASS.ini_valuetext = txtfamilybrother.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtfamilySisters.ClientID;
            clsASS.ini_valuetext = txtfamilySisters.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtsigfamperunderbetter.ClientID;
            clsASS.ini_valuetext = txtsigfamperunderbetter.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtStepSiblings.ClientID;
            clsASS.ini_valuetext = txtStepSiblings.Text;
            blnSave = clsASS.Save();

            lblMessage.Text = "Save Successfully.";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error.. in " + clsASS.ini_valueflag + " " + ex.Message;
        }
    }
}