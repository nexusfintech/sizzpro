﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsobjectsharing
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsobjectsharing()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsobjectsharing()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _shr_id; }
            set
            {
                _shr_id = value;
                if (clsCMD.Parameters.Contains("@shr_id"))
                { clsCMD.Parameters["@shr_id"].Value = value; }
                else { clsCMD.Parameters.Add("@shr_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _shr_id;
        public Int64 shr_id
        {
            get { return _shr_id; }
            set
            {
                _shr_id = value;
                if (clsCMD.Parameters.Contains("@shr_id"))
                { clsCMD.Parameters["@shr_id"].Value = value; }
                else { clsCMD.Parameters.Add("@shr_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _shr_object;
        public String shr_object
        {
            get { return _shr_object; }
            set
            {
                _shr_object = value;
                if (clsCMD.Parameters.Contains("@shr_object"))
                { clsCMD.Parameters["@shr_object"].Value = value; }
                else { clsCMD.Parameters.Add("@shr_object", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Guid _shr_ownerid;
        public Guid shr_ownerid
        {
            get { return _shr_ownerid; }
            set
            {
                _shr_ownerid = value;
                if (clsCMD.Parameters.Contains("@shr_ownerid"))
                { clsCMD.Parameters["@shr_ownerid"].Value = value; }
                else { clsCMD.Parameters.Add("@shr_ownerid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _shr_sharedwithid;
        public Guid shr_sharedwithid
        {
            get { return _shr_sharedwithid; }
            set
            {
                _shr_sharedwithid = value;
                if (clsCMD.Parameters.Contains("@shr_sharedwithid"))
                { clsCMD.Parameters["@shr_sharedwithid"].Value = value; }
                else { clsCMD.Parameters.Add("@shr_sharedwithid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _shr_sharedobjid;
        public Int64 shr_sharedobjid
        {
            get { return _shr_sharedobjid; }
            set
            {
                _shr_sharedobjid = value;
                if (clsCMD.Parameters.Contains("@shr_sharedobjid"))
                { clsCMD.Parameters["@shr_sharedobjid"].Value = value; }
                else { clsCMD.Parameters.Add("@shr_sharedobjid", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                shr_id = Convert.ToInt64(dr["shr_id"]);
                shr_object = Convert.ToString(dr["shr_object"]);
                shr_ownerid = new Guid(dr["shr_ownerid"].ToString());
                shr_sharedwithid = new Guid(dr["shr_sharedwithid"].ToString());
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            shr_id = Convert.ToInt64("0");
            shr_object = Convert.ToString("NA");
            shr_ownerid = new Guid();
            shr_sharedwithid = new Guid();
            shr_sharedobjid = Convert.ToInt64("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.shr_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_objectsharingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 shr_id)
        {
            Boolean blnResult = false;
            this.shr_id = shr_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_objectsharingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 shr_id)
        {
            Boolean blnResult = false;
            this.shr_id = shr_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_objectsharingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_objectsharingGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetRecords()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_objectsharingGetData";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsobjectsharing_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsobjectsharing_PropertiesList> lstResult = new List<clsobjectsharing_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].shr_id = Convert.ToInt64(dr["shr_id"]);
                lstResult[i].shr_object = Convert.ToString(dr["shr_object"]);
                lstResult[i].shr_ownerid = new Guid(dr["shr_ownerid"].ToString());
                lstResult[i].shr_sharedwithid = new Guid(dr["shr_sharedwithid"].ToString());
                lstResult[i].shr_sharedobjid = Convert.ToInt64(dr["shr_sharedobjid"]);
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 shr_id)
        {
            Boolean blnResult = false;
            try
            {
                this.shr_id = shr_id;
                clsCMD.CommandText = "sp_objectsharingGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 shr_id)
        {
            Boolean blnResult = false;
            try
            {
                this.shr_id = shr_id;
                clsCMD.CommandText = "sp_objectsharingGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }
        #endregion

    }
    public class clsobjectsharing_PropertiesList
    {
        public Int64 shr_id { get; set; }
        public String shr_object { get; set; }
        public Guid shr_ownerid { get; set; }
        public Guid shr_sharedwithid { get; set; }
        public Int64 shr_sharedobjid { get; set; }
    }
}