﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class UserControl_ClientProfile_ucFamilyofOrigin : System.Web.UI.UserControl
{
    clsFamilyofOrigin clsFOM = new clsFamilyofOrigin();
    clsProfileMaster clsPRO = new clsProfileMaster();
    clsUsers clsUsr = new clsUsers();
    Int64 intUserid;
    string strHeaderName;
    string strRelationFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value;}
    }

    public String HeaderName
    {
        get { return strHeaderName; }
        set
        {
            strHeaderName = value; 
        }
    }

    public String RelationFlag
    {
        get { return strRelationFlag; }
        set { strRelationFlag = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        //MembershipUser mu = Membership.GetUser(objKey);
        clsPRO.GetRecordByMemberKeyInProperties(objKey.ToString());
        lblBoxheader.Text = clsPRO.prm_firstname + " " + clsPRO.prm_lastname + " 's " + strHeaderName;
    }

    private String ValidationForm()
    {
        string strError = "";
        if (txtFirstName.Text.Trim() == "")
        { strError += "Please Enter Firstname <br/.>"; }
        if (txtMiddlename.Text.Trim() == "")
        { strError += "Please Enter Middlename <br/.>"; }
        if (txtlastname.Text.Trim() == "")
        { strError += "Please Enter Lastname <br/.>"; }
        if (txtAge.Text.Trim() == "")
        { strError += "Please Enter Age <br/.>"; }
        return strError;
    }

    public Boolean Save()
    {
        String strResult = ValidationForm();
        if (strResult != "")
        {
            lblMessage.Text = strResult;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return false;
        }
        Guid objKey = new Guid(Memberkey);
        Boolean blnResult = false;
        clsFOM.foo_age = txtAge.Text.Trim();
        clsFOM.foo_description = txtDescription.Text.Trim();
        clsFOM.foo_firstname = txtFirstName.Text.Trim();
        clsFOM.foo_lastname = txtlastname.Text.Trim();
        clsFOM.foo_livewithclient = chklevewithclient.Checked;
        clsFOM.foo_middlename = txtMiddlename.Text.Trim();
        clsFOM.foo_relationflag = strRelationFlag;
        clsFOM.foo_userid = objKey;
        blnResult = clsFOM.Save();
        return blnResult;
    }

    public void EditEntry()
    {
        Guid objKey = new Guid(strMemberkey);
        Boolean blnResult = clsFOM.GetRecordByIDInProperties(objKey, strRelationFlag);
        if (blnResult == true)
        {
            txtAge.Text = clsFOM.foo_age;
            txtDescription.Text = clsFOM.foo_description;
            txtFirstName.Text = clsFOM.foo_firstname;
            txtlastname.Text = clsFOM.foo_lastname;
            txtMiddlename.Text = clsFOM.foo_middlename;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            SetValue();
            EditEntry();
        }
    }
}