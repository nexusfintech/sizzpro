﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
public partial class Master_referingProvider : System.Web.UI.Page
{
    clsReferringprovidersMaster clsRPM = new clsReferringprovidersMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        if (User.IsInRole("administrator"))
        {
            rptRfrPro.DataSource = clsRPM.GetAllRecordForSA();
        }
        else
        {
            rptRfrPro.DataSource = clsRPM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        }
        rptRfrPro.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Master/ReferingProviderAddEdit.aspx");
    }
    protected void rptRfrPro_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if(e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/ReferingProviderAddEdit.aspx");
        }
        if(e.CommandName == "DELETE")
        {
            Boolean blnRslt = clsRPM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (blnRslt)
            {
                MySession.Current.MESSAGE = "Record Deleted  Successfully";
                Response.Redirect("~/Master/ReferingProviderMaster.aspx");
            }
            else
            {
                lblmessage.Text = clsRPM.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
}