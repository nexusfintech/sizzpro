﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsCPTCodeMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsCPTCodeMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsCPTCodeMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _cd_id; }
            set
            {
                _cd_id = value;
                if (clsCMD.Parameters.Contains("@cd_id"))
                { clsCMD.Parameters["@cd_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cd_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _cd_id;
        public Int64 cd_id
        {
            get { return _cd_id; }
            set
            {
                _cd_id = value;
                if (clsCMD.Parameters.Contains("@cd_id"))
                { clsCMD.Parameters["@cd_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cd_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _cd_code;
        public String cd_code
        {
            get { return _cd_code; }
            set
            {
                _cd_code = value;
                if (clsCMD.Parameters.Contains("@cd_code"))
                { clsCMD.Parameters["@cd_code"].Value = value; }
                else { clsCMD.Parameters.Add("@cd_code", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _cd_description;
        public String cd_description
        {
            get { return _cd_description; }
            set
            {
                _cd_description = value;
                if (clsCMD.Parameters.Contains("@cd_description"))
                { clsCMD.Parameters["@cd_description"].Value = value; }
                else { clsCMD.Parameters.Add("@cd_description", SqlDbType.NVarChar).Value = value; }
            }
        }
        private Double _cd_charge;
        public Double cd_charge
        {
            get { return _cd_charge; }
            set
            {
                _cd_charge = value;
                if (clsCMD.Parameters.Contains("@cd_charge"))
                { clsCMD.Parameters["@cd_charge"].Value = value; }
                else { clsCMD.Parameters.Add("@cd_charge", SqlDbType.Money).Value = value; }
            }
        }
        private Int64 _cd_pos;
        public Int64 cd_pos
        {
            get { return _cd_pos; }
            set
            {
                _cd_pos = value;
                if (clsCMD.Parameters.Contains("@cd_pos"))
                { clsCMD.Parameters["@cd_pos"].Value = value; }
                else { clsCMD.Parameters.Add("@cd_pos", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _cd_userid;
        public Guid cd_userid
        {
            get { return _cd_userid; }
            set
            {
                _cd_userid = value;
                if (clsCMD.Parameters.Contains("@cd_userid"))
                { clsCMD.Parameters["@cd_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@cd_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                cd_id = Convert.ToInt64(dr["cd_id"]);
                cd_code = Convert.ToString(dr["cd_code"]);
                cd_description = Convert.ToString(dr["cd_description"]);
                cd_charge = Convert.ToDouble(dr["cd_charge"]);
                cd_pos = Convert.ToInt64(dr["cd_pos"]);
                cd_userid = new Guid(dr["cd_userid"].ToString());
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            cd_id = Convert.ToInt64("0");
            cd_code = Convert.ToString("NA");
            cd_description = Convert.ToString("NA");
            cd_charge = Convert.ToDouble("0");
            cd_pos = Convert.ToInt64("0");
            cd_userid = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.cd_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_CPTCodeMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 cd_id)
        {
            Boolean blnResult = false;
            this.cd_id = cd_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_CPTCodeMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 cd_id)
        {
            Boolean blnResult = false;
            this.cd_id = cd_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_CPTCodeMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_CPTCodeMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsCPTCodeMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsCPTCodeMaster_PropertiesList> lstResult = new List<clsCPTCodeMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].cd_id = Convert.ToInt64(dr["cd_id"]);
                lstResult[i].cd_code = Convert.ToString(dr["cd_code"]);
                lstResult[i].cd_description = Convert.ToString(dr["cd_description"]);
                lstResult[i].cd_charge = Convert.ToDouble(dr["cd_charge"]);
                lstResult[i].cd_pos = Convert.ToInt64(dr["cd_pos"]);
                lstResult[i].cd_userid = new Guid(dr["cd_userid"].ToString());

                i++;
            }
            return lstResult;
        }

        public DataTable GetAllRecordByAdmin(Guid adminid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.cd_userid = adminid;
                clsCMD.CommandText = "sp_CPTCodeMasterGetData";
                SetGetSPFlag = "ALLBYADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 cd_id)
        {
            Boolean blnResult = false;
            try
            {
                this.cd_id = cd_id;
                clsCMD.CommandText = "sp_CPTCodeMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean GetRecordByCptCode(string code)
        {
            Boolean blnResult = false;
            try
            {
                this.cd_code = code;
                clsCMD.CommandText = "sp_CPTCodeMasterGetData";
                SetGetSPFlag = "BYCPTCODE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 cd_id)
        {
            Boolean blnResult = false;
            try
            {
                this.cd_id = cd_id;
                clsCMD.CommandText = "sp_CPTCodeMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsCPTCodeMaster_PropertiesList
    {
        public Int64 cd_id { get; set; }
        public String cd_code { get; set; }
        public String cd_description { get; set; }
        public Double cd_charge { get; set; }
        public Int64 cd_pos { get; set; }
        public Guid cd_userid { get; set; }
    }
}