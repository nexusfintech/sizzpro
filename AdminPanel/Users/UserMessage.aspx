﻿<%@ Page Language="C#" MasterPageFile="~/MasterPageOuter.master" AutoEventWireup="true" CodeFile="UserMessage.aspx.cs" Inherits="Users_UserMessage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3>
                        <i class="icon-user"></i>
                        User Registration Message
								</h3>
                </div>
                <div class="box-content">
                    <asp:Label ID="lblpagemessage" runat="server" Text="" Font-Size="X-Large"></asp:Label>
                    <div id="content_successfull" class="span12" runat="server" visible="false">
                        <h2 style="color: green;">User Registration Successfull.</h2>
                        <div class="box box-bordered box-color">
                            <div class="box-title">
                                <h3><i class="icon-user"></i>User Registration Complete</h3>
                            </div>
                            <div class="box-content nopadding">
                                <p>
                                    Welcome to our portal, &nbsp;
                                        <asp:Label ID="lblusername" runat="server" Text="{username}"></asp:Label>
                                </p>
                                <br />
                                <p>
                                    Your registration process is completed successfully. Your account details have been sent to your email account.
                                        Please activate your account by visiting activaion link from your email.
                                </p>
                                <h3>Registred Email Account :  
                                        <small>
                                            <asp:Label ID="lblEmailAccount" runat="server" Text="{registredmailaccount}"></asp:Label>
                                        </small>
                                </h3>
                                <h3>User Name :
                                        <small>
                                            <asp:Label ID="lblusernamedisp" runat="server" Text="{username}"></asp:Label>
                                        </small>
                                </h3>

                            </div>
                        </div>
                    </div>
                    <div id="content_activation" class="span12" runat="server" visible="false">
                        <h1 style="color: green;">Email Account Verification Successfull.</h1>
                        <div class="box box-bordered box-color">
                            <div class="box-title">
                                <h3><i class="icon-smile"></i>Email Verification</h3>
                            </div>
                            <div class="box-content nopadding">
                                <p>
                                    Congratulations , &nbsp;
                                        <asp:Label ID="lblusernameactivation" runat="server" Text="{username}"></asp:Label>
                                </p>
                                <br />
                                <p>
                                    User Email address Verification process is completed. Now you can login with your credentials.
                                </p>
                                <h3>Registered Email Account :  
                                        <small>
                                            <asp:Label ID="lblemailactivation" runat="server" Text="{registredmailaccount}"></asp:Label>
                                        </small>
                                </h3>
                                <h3>User Name :
                                        <small>
                                            <asp:Label ID="lblusernameactivation1" runat="server" Text="{username}"></asp:Label>
                                        </small>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div id="content_activationfail" class="span12" runat="server" visible="false">
                        <h2 style="color: red;">Email Account Verification is Incomplete.</h2>
                        <div class="box box-bordered box-color">
                            <div class="box-title">
                                <h3><i class="icon-search"></i>Email Varification Incomplete</h3>
                            </div>
                            <div class="box-content">
                                <p>
                                    Your email account varification is not complete, please check email with activation link and visit that or you can copy and paste in browser. After completing verification process you can login in our site.
                                </p>
                                <h3>Registered Email Account :  
                                        <small>
                                            <asp:Label ID="lblemailinactive" runat="server" Text="{registredmailaccount}"></asp:Label>
                                        </small>
                                </h3>
                                <h3>User Name :
                                        <small>
                                            <asp:Label ID="lblusernameinactive" runat="server" Text="{username}"></asp:Label>
                                        </small>
                                </h3>
                            </div>
                            <div>
                                <h2 style="color: red;">Resend Verification Mail.</h2>
                                <div class="box box-bordered box-color">
                                    <div class="box-title">
                                        <h3><i class="icon-mail-forward"></i>Resend Email</h3>
                                    </div>
                                    <div class="box-content">
                                        <h4>
                                            <asp:Label ID="lbllinkgen" runat="server" Text=""></asp:Label>
                                        </h4>
                                        <p>
                                            If you need us to resend your verification email, please select “Resend” below.
                                        If you do not receive the verification email, make sure to check your SPAM folder!
                                        </p>
                                        <asp:Button ID="cmdresend" runat="server" CssClass="btn" Text="Resend Email" OnClick="cmdresend_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="content_linkexpired" class="span12" runat="server" visible="false">
                        <h2 style="color: red;">Activation Link is Expired</h2>
                        <div class="box box-bordered box-color">
                            <div class="box-title">
                                <h3><i class="icon-mail-forward"></i>Resend Email</h3>
                            </div>
                            <div class="box-content">
                                <h4>
                                    <asp:Label ID="lbllinkgen1" runat="server" Text=""></asp:Label>
                                </h4>
                                <p>
                                    If you need us to resend your verification email, please select “Resend” below.
                                        If you do not receive the verification email, make sure to check your SPAM folder!
                                </p>
                                <asp:Button ID="cmdExpiredResendLink" runat="server" CssClass="btn" Text="Resend Email" OnClick="cmdExpiredResendLink_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
