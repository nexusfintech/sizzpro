﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucPsychological.ascx.cs" Inherits="UserControl_Assessment_ucPsychological" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-heart"></i>
                    Psychological
                </h3>
            </div>
            <div class="box-content ">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="span12">
                        <div class="span6">
                            <label>Have you ever suffered from, been been diagnosed or treated for psychological, emotional or social problems? </label>
                        </div>
                        <div class="span6">
                            <asp:RadioButtonList ID="rdosufferdpsychological" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="2" Text="No"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="span12">
                        <label>Describe?</label>
                        <asp:TextBox ID="txtsuffredpsychological" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <div class="span6">
                            <label>Have you recently thought of hurting yourself (suicide)?</label>
                        </div>
                        <div class="span6">
                            <asp:RadioButtonList ID="rdosuicide" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="2" Text="No"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="span12">
                        <label>Describe any suicide attempts and/or recent thoughts?</label>
                        <asp:TextBox ID="txtsucideappempts" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Describe any recent thoughts of hurting someone else?</label>
                        <asp:TextBox ID="txthurtingsomeone" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Describe any voices you have been hearing or things you have seen that other people weren’t seeing?</label>
                        <asp:TextBox ID="txtvoicedescribe" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>If you have ever felt that your mind wasn’t working right share what you mean by that?</label>
                        <asp:TextBox ID="txtmindworking" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
