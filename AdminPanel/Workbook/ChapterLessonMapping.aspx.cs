﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;

public partial class Workbook_ChapterLessonMapping : System.Web.UI.Page
{
    clsChapters clsCHP = new clsChapters();
    clsChapterToLession clsCTL = new clsChapterToLession();
    clsClientParentMapping clsCPM = new clsClientParentMapping();

    private void BindCombo(Guid UserId)
    {
        DataTable dtRslt = clsCHP.GetAllRecordByMember(UserId);
        ddlChapter.Items.Clear();
        ddlChapter.SelectedIndex = -1;
        ddlChapter.SelectedValue = null;
        ddlChapter.ClearSelection();
        ddlChapter.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlChapter.Items.Add(lst);
        dtRslt.Columns.Add("Chapter", typeof(string), "chp_version + ' - '  + chp_name");
        ddlChapter.DataValueField = "chp_id";
        ddlChapter.DataTextField = "Chapter";
        ddlChapter.DataSource = dtRslt;
        ddlChapter.DataBind();
    }

    private void BindListView(Int64 intchpid, Guid Userid)
    {
        DataTable dtleft = clsCTL.GetAllByChapterIDNotMapped(intchpid, Userid);
        DataTable dtright = clsCTL.GetAllByChapterID(intchpid, Userid);

        RptrLeft.DataSource = dtleft;
        RptrLeft.DataBind();

        RptrRight.DataSource = dtright;
        RptrRight.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (User.IsInRole("administrator"))
            {
                BindMemberCombo();
            }
            else
            {
                BindCombo((Guid)Membership.GetUser().ProviderUserKey);
            }
        }
    }
    protected void cmdSelect_Click(object sender, EventArgs e)
    {
        Session["chpid"] = ddlChapter.SelectedValue.ToString();
        Int64 intchpid = Convert.ToInt64(Session["chpid"]);
        hdChapterID.Value = ddlChapter.SelectedValue.ToString();
        lblBoxHeader.Text = "( " + ddlChapter.SelectedItem.Text + " ) Chapter Selected";
        pnlMapping.Visible = true;
        if (User.IsInRole("administrator"))
        {
            BindListView(intchpid, new Guid(ddlSelectMember.SelectedValue.ToString()));
        }
        else
        {
            BindListView(intchpid, (Guid)Membership.GetUser().ProviderUserKey);
        }
    }

    private void BindMemberCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRole("Member");
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }

    protected void ddlSelectMember_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCombo(new Guid(ddlSelectMember.SelectedValue.ToString()));
    }
}