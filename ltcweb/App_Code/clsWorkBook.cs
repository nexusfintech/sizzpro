﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

public class clsWorkBook
{
    clsConnectionlifeprogram clsSQLCon = new clsConnectionlifeprogram();

    public List<workbookList> Read(string username)
    {
        SqlConnection sqlcon = new SqlConnection();
        List<workbookList> lst = new List<workbookList>();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("getassignworkbooks", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@email", username);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lst.Add(new workbookList
                            {
                                step_ID = dr["stepsID"].ToString(),
                                Name = dr["stepsName"].ToString()
                            });
                        }
                        dr.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (sqlcon.State == ConnectionState.Open)
        { sqlcon.Close(); }
        return lst;
    }

    public List<workbookList> WorkbookRead(int id)
    {
        List<workbookList> lst = new List<workbookList>();
        SqlConnection sqlcon = new SqlConnection();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("cp_getstepsdetails", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lst.Add(new workbookList
                            {
                                ID = (int)dr["stepID"],
                                Name = dr["stepName"].ToString(),
                                Introduction = dr["stepIntroduction"].ToString(),
                                Description = dr["stepDescription"].ToString(),
                                Summary = dr["Summary"].ToString()
                            });
                        }
                        dr.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (sqlcon.State == ConnectionState.Open)
        { sqlcon.Close(); }
        return lst;
    }

    public List<workbookList> ChapterTitle(int id)
    {
        List<workbookList> lst = new List<workbookList>();
        SqlConnection sqlcon = new SqlConnection();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("cp_wbchassc", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@stepsID", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lst.Add(new workbookList
                            {
                                ID = (int)dr["chapterID"],
                                Title = dr["title"].ToString()
                            });
                        }
                        dr.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (sqlcon.State == ConnectionState.Open)
        { sqlcon.Close(); }
        return lst;
    }

    public List<workbookList> ChapterStatus(int id, string user)
    {
        List<workbookList> lst = new List<workbookList>();
        SqlConnection sqlcon = new SqlConnection();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("cp_guc", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@stepsID", id);
                    cmd.Parameters.AddWithValue("@email", user);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            workbookList wb = new workbookList();
                            string sts = dr["status"].ToString();
                            if (sts.Trim() == "1")
                            {
                                wb.ID = Convert.ToInt32(dr["stepsChapters"]);
                                wb.urlsts = "~/img/Unlock.png";
                                wb.urlgo = "~/img/Go.png";
                                wb.href = "Lesson.aspx";
                            }
                            else if (sts.Trim() == "2")
                            {
                                wb.ID = Convert.ToInt32(dr["stepsChapters"]);
                                wb.urlsts = "~/img/Status.png";
                                wb.urlgo = "~/img/Go.png";
                                wb.href = "Lesson.aspx";
                            }
                            else
                            {
                                wb.urlsts = "~/img/Lock.png";
                                wb.urlgo = "~/img/Go_Black.png";
                                wb.href = "Chapters.aspx";
                            }
                            lst.Add(wb);
                        }
                        dr.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (sqlcon.State == ConnectionState.Open)
        { sqlcon.Close(); }
        return lst;
    }

    public List<workbookList> lessonDetails(int id)
    {
        List<workbookList> lst = new List<workbookList>();
        SqlConnection sqlcon = new SqlConnection();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("cp_getchaptersdetails", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@chapterID", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lst.Add(new workbookList
                            {
                                ID = (int)dr["chapterID"],
                                Title = dr["Title"].ToString(),
                                Summary = dr["Summary"].ToString()
                            });
                        }
                        dr.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (sqlcon.State == ConnectionState.Open)
        { sqlcon.Close(); }
        return lst;
    }

    public List<workbookList> lessonstatus(int id, string user)
    {
        List<workbookList> lst = new List<workbookList>();
        SqlConnection sqlcon = new SqlConnection();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("cp_guc2", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@stepsChapters", id);
                    cmd.Parameters.AddWithValue("@email", user);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            workbookList wb = new workbookList();
                            string sts = dr["status"].ToString();
                            if (sts.Trim() == "1")
                            {
                                wb.ID = Convert.ToInt32(dr["stepsLessons"]);
                                wb.urlsts = "~/img/Unlock.png";
                                wb.urlgo = "~/img/Go.png";
                                wb.href = "Assignment.aspx";
                            }
                            else if (sts.Trim() == "2")
                            {
                                wb.ID = Convert.ToInt32(dr["stepsLessons"]);
                                wb.urlsts = "~/img/Status.png";
                                wb.urlgo = "~/img/Go.png";
                                wb.href = "Assignment.aspx";
                            }
                            else
                            {
                                wb.urlsts = "~/img/Lock.png";
                                wb.urlgo = "~/img/Go_Black.png";
                                wb.href = "#";
                            }
                            lst.Add(wb);
                        }
                        dr.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (sqlcon.State == ConnectionState.Open)
        { sqlcon.Close(); }
        return lst;
    }

    public List<workbookList> lessonTitle(int id)
    {
        List<workbookList> lst = new List<workbookList>();
        SqlConnection sqlcon = new SqlConnection();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("cp_chasassc", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@chapterID", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lst.Add(new workbookList
                            {
                                ID = (int)dr["AssignmentID"],
                                Title = dr["Name"].ToString()
                            });
                        }
                        dr.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (sqlcon.State == ConnectionState.Open)
        { sqlcon.Close(); }
        return lst;
    }
    
    public List<workbookList> AssignementDetails(int id)
    {
        List<workbookList> lst = new List<workbookList>();
        SqlConnection sqlcon = new SqlConnection();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("cp_getassignmentdetails", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@assignmentID", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lst.Add(new workbookList
                            {
                                ID = (int)dr["AssignmentID"],
                                Title = dr["Name"].ToString(),
                                Summary = dr["Summary"].ToString(),
                                Introduction = dr["Introduction"].ToString(),
                                section = dr["Section1"].ToString(),
                                Description = dr["Assignment1"].ToString()
                            });
                        }
                        dr.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (sqlcon.State == ConnectionState.Open)
        { sqlcon.Close(); }
        return lst;
    }
    
    public string AssignementAnswer(int id, string user)
    {
        string lst = string.Empty;
        SqlConnection sqlcon = new SqlConnection();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("checklessonanswer", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@lessons", id);
                    cmd.Parameters.AddWithValue("@email", user);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            lst = dr["answer"].ToString();
                        }
                        dr.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (sqlcon.State == ConnectionState.Open)
        { sqlcon.Close(); }
        return lst;
    }

    public bool UpdateAssignmentAnswer(string answer, int id, string user)
    {
        bool sts = false;
        SqlConnection sqlcon = new SqlConnection();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("checkinupdateanswer", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@answer", answer);
                    cmd.Parameters.AddWithValue("@stepsLessons", id);
                    cmd.Parameters.AddWithValue("@email", user);
                    cmd.Parameters.AddWithValue("@dated", DateTime.Now);
                    cmd.ExecuteNonQuery();
                }
            }
            sts = true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (sqlcon.State == ConnectionState.Open)
        { sqlcon.Close(); }
        return sts;
    }
}

public class workbookList
{
    public string step_ID { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string Introduction { get; set; }
    public string section { get; set; }
    public string Summary { get; set; }
    public int ID { get; set; }
    public string Title { get; set; }
    public string status { get; set; }
    public string urlsts { get; set; }
    public string urlgo { get; set; }
    public string href { get; set; }
}
