﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Sizzling Hot Practis Admin Home</title>
    <noscript>
        <div class="alert alert-block span10">
            <h4 class="alert-heading">
                Warning!</h4>
            <p>
                You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                enabled to use this site.</p>
        </div>
    </noscript>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- Apple devices fullscreen -->
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Bootstrap responsive -->
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
    <!-- jQuery UI -->
    <link rel="stylesheet" href="css/plugins/jquery-ui/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="css/plugins/jquery-ui/smoothness/jquery.ui.theme.css" />
    <!-- PageGuide -->
    <link rel="stylesheet" href="css/plugins/pageguide/pageguide.css" />
    <!-- chosen -->
    <link rel="stylesheet" href="css/plugins/chosen/chosen.css" />
    <!-- select2 -->
    <link rel="stylesheet" href="css/plugins/select2/select2.css" />


    <!-- Fullcalendar -->
    <link rel="stylesheet" href="css/plugins/fullcalendar/fullcalendar.css" />
    <link rel="stylesheet" href="css/plugins/fullcalendar/fullcalendar.print.css" media="print" />
    <!-- Tagsinput -->
    <link rel="stylesheet" href="css/plugins/tagsinput/jquery.tagsinput.css" />
    <!-- multi select -->
    <link rel="stylesheet" href="css/plugins/multiselect/multi-select.css" />
    <!-- timepicker -->
    <link rel="stylesheet" href="css/plugins/timepicker/bootstrap-timepicker.min.css" />
    <!-- colorpicker -->
    <link rel="stylesheet" href="css/plugins/colorpicker/colorpicker.css" />
    <!-- Datepicker -->
    <link rel="stylesheet" href="css/plugins/datepicker/datepicker.css" />
    <!-- Daterangepicker -->
    <link rel="stylesheet" href="css/plugins/daterangepicker/daterangepicker.css" />
    <!-- Plupload -->
    <link rel="stylesheet" href="css/plugins/plupload/jquery.plupload.queue.css" />

    <!-- icheck -->
    <link rel="stylesheet" href="css/plugins/icheck/all.css" />
    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Color CSS -->
    <link rel="stylesheet" href="css/themes.css" />
    <link href="css/mystyle.css" rel="stylesheet" />

    <style type="text/css">
        .fadein {
            position: relative;
            height: 350px;
            margin: 0 auto;
        }

            .fadein img {
                position: absolute;
                left: 0;
                top: 0;
            }
    </style>
    <script type="text/javascript">
        $(function () {
            $('.fadein img:gt(0)').hide();
            setInterval(function () { $('.fadein :first-child').fadeOut().next('img').fadeIn().end().appendTo('.fadein'); }, 1000);
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="dvHeader" style="width: 100%;">
            <div style="margin: 0 auto; width: 1160px; min-height: 105px; background-color: #368EE0;">
                <div style="padding: 10px; color: white;">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="span7">
                                <%--<img src='<%= Page.ResolveClientUrl("~/img/logo-Lifestyle-ChiroCare-HYFB.png") %>' width="285px">--%>
                                <img src='<%= Page.ResolveClientUrl("~/img/sizzlogo.PNG") %>' width="200px">
                                <h4>Your Practice Building Engine</h4>
                            </div>
                            <div class="span5">
                                <%--<div style="text-align: right">
                                    <a href='<%= page.resolveclienturl("~/default.aspx") %>' style="color: white;"><i class="icon-home"></i>&nbsp;home</a> &nbsp;&nbsp;
                                    <a href='<%= page.resolveclienturl("~/sitemap.aspx") %>' style="color: white;"><i class="icon-sitemap"></i>&nbsp;sitemap</a>&nbsp;&nbsp;
                                    <a href='<%= page.resolveclienturl("~/contactus.aspx") %>' style="color: white;"><i class="icon-phone-sign"></i>&nbsp;contact us</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="https://www.facebook.com/pages/lifestyle-therapy-and-coaching/176976352335408" target="_blank" style="color: white;"><i class="icon-facebook"></i>&nbsp;facebook</a>&nbsp;&nbsp;
                                    <a href="https://twitter.com/myfreedomcoach" target="_blank" style="color: white;"><i class="icon-twitter"></i>&nbsp;tweeter</a>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="dvHeaderMenu">
            <div id="navigation" style="background-color: #0072D6; width: 1160px; margin: 0 auto;">
                <div style="margin: 0 auto; width: 1160px;">
                    <div class="container-fluid">
                        <ul class='main-nav'>
                            <li>
                                <a href='<%= Page.ResolveClientUrl("~/Default.aspx") %>'>
                                    <span>Home</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="dvMainContent">
            <%--class="shadow_left_right"--%>
            <div style="margin: 0 auto; margin-top: 5px; width: 1160px; min-height: 400px; background-color: white;">
                <div class="row-fluid">
                    <div class="span8">
                        <div class="fadein">
                            <img src="img/banner/pic1.jpg" style="width: 770px; height: 340px; border: 1px solid #0072D6" />
                        </div>
                    </div>
                    <div class="span4">
                        <div class="spanlogin">
                            <div class="wrapper">
                                <div class="login-body">
                                    <h2>SIGN IN</h2>
                                    <div>
                                        <div class="control-group">
                                            <div class="email controls">
                                                <asp:TextBox ID="txtusername" runat="server" placeholder="Username or email" class='input-block-level' data-rule-required="true" data-rule-email="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="email controls">
                                                <asp:TextBox ID="txtpassword" runat="server" placeholder="Password" class='input-block-level' TextMode="Password" data-rule-required="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="submit">
                                            <asp:Button ID="cmdlogin" runat="server" Text="Sign in" class="btn btn-primary" OnClick="cmdlogin_Click" />
                                        </div>
                                        <div class="msg">
                                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="forget">
                                        <a href="Users/UserPasswordForgot.aspx?action=reset"><span>Forgot password?</span></a>
                                        <br />
                                        <a href="registerbussiness.aspx"><span>New to SizzPro ? Register Here</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid" style="border: 1px solid #0072D6;">
                    <div class="span12" style="padding: 15px;">
                        <asp:Literal ID="ltrPageBody" runat="server"></asp:Literal>
                    </div>
                    <div class="span12" style="padding: 15px; margin: auto">
                        <div class="span6">
                            <a href="Polices.aspx" class="btn btn-block btn-primary">
                                <h6>WebSite Policies</h6>
                            </a>
                        </div>
                        <div class="span6">
                            <a href="Checkout.aspx" class="btn btn-block btn-primary">
                                <h6>Our Secure Checkout</h6>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="dvFooter" class="dvbottombackgound" style="min-height: 25px; width: 100%;">
        <div style="margin: 0 10px;">
            <div class="row-fluid">
                <div class="span12">
                    <div class="span4">
                        <h6 style="color: white;">© 2010-2011 JC Enterprise Group, Inc. All Rights Reserved.</h6>
                    </div>
                    <div class="span8">
                        <h6 style="color: white;">SizzPRO Corp 1101 McMurtrie Drive NW Suite C4 Huntsville, AL 35806 256-850-4426 contact: admin@sizzpro.com</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-header">
            <h3 id="myModalLabel">WelCome To SizzPro </h3>
        </div>
        <div class="modal-body">
            <p style="text-align: center">
                <strong>
                    This site is under construction , so demo of this site is available here.
                    <br /><br />
                    If you want to continue with demo then please Press "CONTINUE TO SIZZPRO" button.
                </strong>
            </p>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal">CONTINUE TO SIZZPRO <i class="icon-signin"></i></button>
        </div>
    </div>
    <!-- jQuery -->
    <script src='<%= Page.ResolveClientUrl("~/js/jquery.min.js") %>'></script>
    <!-- Nice Scroll -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/nicescroll/jquery.nicescroll.min.js") %>'></script>
    <!-- jQuery UI -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/jquery-ui/jquery.ui.core.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/jquery-ui/jquery.ui.widget.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/jquery-ui/jquery.ui.mouse.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/jquery-ui/jquery.ui.draggable.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/jquery-ui/jquery.ui.resizable.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/jquery-ui/jquery.ui.sortable.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/jquery-ui/jquery.ui.spinner.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/jquery-ui/jquery.ui.slider.js") %>'></script>
    <!-- Touch enable for jquery UI -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/touch-punch/jquery.touch-punch.min.js") %>'></script>
    <!-- slimScroll -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/slimscroll/jquery.slimscroll.min.js") %>'></script>
    <!-- Bootstrap -->
    <script src='<%= Page.ResolveClientUrl("~/js/bootstrap.min.js") %>'></script>
    <!-- vmap -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/vmap/jquery.vmap.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/vmap/jquery.vmap.world.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/vmap/jquery.vmap.sampledata.js") %>'></script>
    <!-- Bootbox -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/bootbox/jquery.bootbox.js") %>'></script>
    <!-- Flot -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/flot/jquery.flot.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/flot/jquery.flot.bar.order.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/flot/jquery.flot.pie.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/flot/jquery.flot.resize.min.js") %>'></script>
    <!-- imagesLoaded -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/imagesLoaded/jquery.imagesloaded.min.js") %>'></script>
    <!-- PageGuide -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/pageguide/jquery.pageguide.js") %>'></script>
    <!-- FullCalendar -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/fullcalendar/fullcalendar.min.js") %>'></script>
    <!-- Chosen -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/chosen/chosen.jquery.min.js") %>'></script>
    <!-- select2 -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/select2/select2.min.js") %>'></script>
    <!-- icheck -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/icheck/jquery.icheck.min.js") %>'></script>
    <!-- dataTables -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/jquery.dataTables.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/TableTools.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/ColReorderWithResize.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/ColVis.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/jquery.dataTables.columnFilter.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/jquery.dataTables.grouping.js") %>'></script>
    <!-- Theme framework -->
    <script src='<%= Page.ResolveClientUrl("~/js/eakroko.min.js") %>'></script>
    <!-- Theme scripts -->
    <script src='<%= Page.ResolveClientUrl("~/js/application.min.js") %>'></script>

    <!-- Masked inputs -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/maskedinput/jquery.maskedinput.min.js") %>'></script>
    <!-- TagsInput -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/tagsinput/jquery.tagsinput.min.js") %>'></script>
    <!-- FullCalendar -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/fullcalendar/fullcalendar.min.js") %>'></script>
    <!-- Datepicker -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datepicker/bootstrap-datepicker.js") %>'></script>
    <!-- Daterangepicker -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/daterangepicker/daterangepicker.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/daterangepicker/moment.min.js") %>'></script>
    <!-- Timepicker -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/timepicker/bootstrap-timepicker.min.js") %>'></script>
    <!-- Colorpicker -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/colorpicker/bootstrap-colorpicker.js") %>'></script>
    <!-- MultiSelect -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/multiselect/jquery.multi-select.js") %>'></script>

    <!-- CKEditor -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/ckeditor/ckeditor.js") %>'></script>
    <!-- PLUpload -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/plupload/plupload.full.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/plupload/jquery.plupload.queue.js") %>'></script>
    <!-- Custom file upload -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/fileupload/bootstrap-fileupload.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/mockjax/jquery.mockjax.js") %>'></script>
    <!-- complexify -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/complexify/jquery.complexify-banlist.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/complexify/jquery.complexify.min.js") %>'></script>
    <!-- Mockjax -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/mockjax/jquery.mockjax.js") %>'></script>
    <!-- Validation -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/validation/jquery.validate.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/validation/additional-methods.min.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            $("#modal-2").modal('show');
            // Stick the #nav to the top of the window
            var nav = $('#dvHeaderMenu');
            var navHomeY = nav.offset().top;
            var isFixed = false;
            var $w = $(window);
            $w.scroll(function () {
                var scrollTop = $w.scrollTop();
                var shouldBeFixed = scrollTop > navHomeY;
                if (shouldBeFixed && !isFixed) {
                    nav.css({
                        position: 'fixed',
                        top: 0,
                        left: nav.offset().left,
                        width: nav.width()
                    });
                    isFixed = true;
                }
                else if (!shouldBeFixed && isFixed) {
                    nav.css({
                        position: 'static'
                    });
                    isFixed = false;
                }
            });
        });
    </script>
</body>
</html>

