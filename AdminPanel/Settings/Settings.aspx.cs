﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Settings_Settings : System.Web.UI.Page
{
    clsSettings clsSTG = new clsSettings();

    private void EditValue()
    {
        Boolean blnResult = clsSTG.GetRecordByFLAGInProperties("WEBSESSION");
        if (blnResult == true)
        {
            txtWebSesion.Text = clsSTG.stg_intvalue.ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        { EditValue(); }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        clsSTG.stg_settingflag = "WEBSESSION";
        clsSTG.stg_intvalue = Convert.ToInt64(txtWebSesion.Text.Trim());
        Boolean blnResult = clsSTG.Save();
        if (blnResult == true)
        { lblMessage.Text = "Saved Successfully."; }
        else { lblMessage.Text = "Error in save settings.. " + clsSTG.GetSetErrorMessage; }

    }
}