﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAssessmentReadiness.ascx.cs" Inherits="UserControl_Assessment_ucAssessmentReadiness" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="glyphicon-eye_open"></i>
                    Readiness
                </h3>
            </div>
            <div class="box-content">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="span12">
                        <label>Frequently we find it easier to look at others to be the cause and solution for our problems. As you enter into this counseling experience you need to begin focusing on what you actually have power to change - yourself. As you consider the changes you would like to experience in your life (and/or relationships) what do you do that causes you the most problems related to why you are coming to therapy?</label>
                        <asp:TextBox ID="txtreadinessfrequntly" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Answer the following question concerning the behavior you mentioned above that you need to change.</label>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness1" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>1. 	As far as I'm concerned, I don't have any problems that need changing.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness2" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>2. 	I think I might be ready to make some changes in this problem behavior..</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness3" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>3. 	I am doing something about the problems that had been bothering me.</label>

                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness4" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>4. 	It might be worthwhile to work on my problem.</label>

                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness5" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>5. 	I'm not the problem one. It doesn't make much sense for me to be here.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness6" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>6. 	It worries me that I might slip back on a problem I have already changed, so I am here to seek help.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness7" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>7. 	I am finally doing some work on my problem.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness8" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>8. 	I've been thinking that I might want to change something about myself.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness9" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>9. 	I have been successful in working on my problem but I'm not sure I can keep up the effort on my own.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness10" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>10. 	At times my problem is difficult, but I'm working on it.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness11" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>11. 	Being here is pretty much a waste of time for me because the problem doesn't have to do with me.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness12" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>12. 	I'm hoping this place will help me to better understand myself.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness13" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>13. 	I guess I have faults, but there's nothing that I really need to change.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness14" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>14. 	I am really working hard to change.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness15" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>15. 	I have a problem and I really think I should work at it.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness16" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>16. 	I'm not following through with what I had already changed as well as I had hoped, and I'm here to prevent a relapse of the problem.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness17" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>17. 	Even though I'm not always successful in changing, I am at least working on my problem.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness18" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>18. 	I thought once I had resolved my problem I would be free of it, but sometimes I still find myself struggling with it.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness19" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>19. 	I wish I had more ideas on how to solve the problem.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness20" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>20. 	I have started working on my problems but I would like help.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness21" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>21. 	Maybe this place will be able to help me.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness22" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>22. 	I may need a boost right now to help me maintain the changes I've already made.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness23" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>23. 	I may be part of the problem, but I don't really think I am.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness24" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>24. 	I hope that someone here will have some good advice for me.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness25" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>25. 	Anyone can talk about changing; I'm actually doing something about it.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness26" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>26. 	All this talk about psychology is boring. Why can't people just forget about their problems?</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness27" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>27. 	I'm here to prevent myself from having a relapse of my problem.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness28" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>28. 	It is frustrating, but I feel I might be having a recurrence of a problem I thought I had resolved.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness29" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>29. 	I have worries but so does the next guy. Why spend time thinking about them?</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness30" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>30. 	I am actively working on my problem.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness31" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>31. 	I would rather cope with my faults than try to change them.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 check-line">
                            <asp:CheckBox ID="chkreadiness32" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                            <label class='inline'>32. 	After all I had done to try to change my problem, every now and again it comes back to haunt me.</label>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
