﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientIntialAssessment.aspx.cs" Inherits="Clients_ClientIntialAssessment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#lnkYou").click(function () {
                alert("Handler for .click() called.");
                return false;
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpMainContent" runat="Server">
    <asp:HiddenField ID="AssesmentStep" runat="server" Value="2" />
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-group"></i>
                Initial Assessment
            </h3>
        </div>
        <asp:Label ID="lblMessage" runat="server" Text="Label" Visible="false"></asp:Label>
        <div class="box-content nopadding">
            <UC:UserProfile ID="ucProfile" runat="server" />
        </div>
    </div>
  <div class="row-fluid">
        <hr />
        <div class="span12">
            <asp:Button ID="cmdNext" runat="server" Text="Next Family & Origion >>" CssClass="btn btn-primary" OnClick="cmdNext_Click" />
            <asp:Label ID="Label1" runat="server" ForeColor="red">Please click on "Next" after completing profile</asp:Label>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

