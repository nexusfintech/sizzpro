﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class UserControl_ClientProfile_ucSibling : System.Web.UI.UserControl
{
    clsUsers clsUsr = new clsUsers();
    clsSibling clsSib = new clsSibling();

    private static string strAddEditFlag;
    private static Int64 intEditID;
    string strMemberkey;
    
    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void EmptyControls()
    {
        txtAge.Text = "";
        txtDescription.Text = "";
        txtFirstName.Text = "";
        txtlastname.Text = "";
        txtMiddlename.Text = "";
    }

    private void GridDisplay()
    {
        Guid objKey = new Guid(Memberkey);
        dtSiblingDisp.DataSource = clsSib.GetAllBYUserId(objKey);
        dtSiblingDisp.DataBind();
    }

    private String ValidationForm()
    {
        String strError = "";
        if (txtFirstName.Text.Trim() == "")
        { strError += "Please Input Firstname <br/>"; }
        if (txtMiddlename.Text.Trim() == "")
        { strError += "Please Input Middlename <br/>"; }
        if (txtlastname.Text.Trim() == "")
        { strError += "Please Input Lastname <br/>"; }
        if (txtAge.Text.Trim() == "")
        { strError += "Please Input Age <br/>"; }
        return strError;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GridDisplay();
            strAddEditFlag = "ADD";
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        String strResult = ValidationForm();
        if (strResult != "")
        {
            lblMessage.Text = strResult;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        Guid objKey = new Guid(strMemberkey);
        clsSib.csb_age = txtAge.Text;
        clsSib.csb_description = txtDescription.Text;
        clsSib.csb_firstname = txtFirstName.Text;
        clsSib.csb_gender = ddlGender.SelectedItem.Text;
        clsSib.csb_lastname = txtlastname.Text;
        clsSib.csb_livewithclient = chkLive.Checked;
        clsSib.csb_middlename = txtMiddlename.Text;
        clsSib.csb_userid = objKey;
        Boolean blnSave = false;
        if (strAddEditFlag == "ADD")
        { blnSave = clsSib.Save(); strAddEditFlag = "ADD"; }
        else { blnSave = clsSib.Update(intEditID); }
        if (blnSave == true)
        {
            EmptyControls();
            GridDisplay();
        }
    }

    protected void dtSiblingDisp_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            intEditID = Convert.ToInt64(e.CommandArgument);
            Boolean blnEdit = clsSib.GetRecordByIDInProperties(intEditID);
            if (blnEdit == true)
            { EditEntry(); }
            else { lblMessage.Text = "Error... Edit Process not proper complete please try again."; }
        }
        if (e.CommandName == "delete")
        {
            intEditID = Convert.ToInt64(e.CommandArgument);
            Boolean blnEdit = clsSib.DeleteByPKID(intEditID);
            if (blnEdit == true)
            { GridDisplay(); lblMessage.Text = "Delete Successfully."; }
            else { lblMessage.Text = "Error... Edit Process not proper complete please try again."; }
        }
    }

    private void EditEntry()
    {
        strAddEditFlag = "EDIT";
        txtAge.Text = clsSib.csb_age;
        txtDescription.Text = clsSib.csb_description;
        txtFirstName.Text = clsSib.csb_firstname;
        ddlGender.Text = clsSib.csb_gender;
        txtlastname.Text = clsSib.csb_lastname;
        chkLive.Checked = clsSib.csb_livewithclient;
        txtMiddlename.Text = clsSib.csb_middlename;
    }
}