﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="FollowupSession.aspx.cs" Inherits="Admin_SessionManagement_FollowupSession" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn btn-primary" Text="Add New Session" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
            <li>
                <a>
                    <i class="icon-check"></i>
                    <asp:Button ID="cmdclaim" runat="server" CssClass="btn btn-primary" Text="Claim Managment" OnClick="cmdclaim_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Session Master
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptflwsession" runat="server" OnItemCommand="rptflwsession_ItemCommand">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Date</th>
                                        <th>Client Name</th>
                                        <th>Location</th>
                                        <th>Total Charged</th>
                                        <th>Provider</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align:center">
                                    <asp:LinkButton ID="cmdView" runat="server" CssClass="btn btn-primary" CommandName="VIEW" rel="tooltip" data-placement="top" title="VIEW SESSION" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "cfs_sessionid") %>'><i class="glyphicon-hand_right"></i></asp:LinkButton>
                                </td>
                                <td style="text-align: center">
                                    <%# Eval("cfs_sessiondate","{0:MM/dd/yyyy}")%>
                                </td>
                                <td style="text-align: center"><%# Eval("Clientname") %></td>
                                <td style="text-align: center"><%# Eval("pos_place") %></td>
                                <td style="text-align: center">$ <%# Eval("fsb_totalcharged","{0:f2}") %></td>
                                <td style="text-align: center"><%# Eval("Provider")%></td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "cfs_id") %>' rel="tooltip" data-placement="top" title="EDIT"><i class="icon-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="cmdClmSbmt" runat="server" CssClass="btn btn-green" CommandName="CLAIMCREATE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "cfs_sessionid") %>' rel="tooltip" data-placement="top" title="CREATE CLAIM"><i class="icon-plus-sign"></i> create claim</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
