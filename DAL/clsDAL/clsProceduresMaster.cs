﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsProceduresMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsProceduresMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsProceduresMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _p_id; }
            set
            {
                _p_id = value;
                if (clsCMD.Parameters.Contains("@p_id"))
                { clsCMD.Parameters["@p_id"].Value = value; }
                else { clsCMD.Parameters.Add("@p_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _p_id;
        public Int64 p_id
        {
            get { return _p_id; }
            set
            {
                _p_id = value;
                if (clsCMD.Parameters.Contains("@p_id"))
                { clsCMD.Parameters["@p_id"].Value = value; }
                else { clsCMD.Parameters.Add("@p_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _p_code;
        public String p_code
        {
            get { return _p_code; }
            set
            {
                _p_code = value;
                if (clsCMD.Parameters.Contains("@p_code"))
                { clsCMD.Parameters["@p_code"].Value = value; }
                else { clsCMD.Parameters.Add("@p_code", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _p_procedure;
        public String p_procedure
        {
            get { return _p_procedure; }
            set
            {
                _p_procedure = value;
                if (clsCMD.Parameters.Contains("@p_procedure"))
                { clsCMD.Parameters["@p_procedure"].Value = value; }
                else { clsCMD.Parameters.Add("@p_procedure", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _p_description;
        public String p_description
        {
            get { return _p_description; }
            set
            {
                _p_description = value;
                if (clsCMD.Parameters.Contains("@p_description"))
                { clsCMD.Parameters["@p_description"].Value = value; }
                else { clsCMD.Parameters.Add("@p_description", SqlDbType.NVarChar).Value = value; }
            }
        }
        private Guid _p_userid;
        public Guid p_userid
        {
            get { return _p_userid; }
            set
            {
                _p_userid = value;
                if (clsCMD.Parameters.Contains("@p_userid"))
                { clsCMD.Parameters["@p_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@p_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                p_id = Convert.ToInt64(dr["p_id"]);
                p_code = Convert.ToString(dr["p_code"]);
                p_procedure = Convert.ToString(dr["p_procedure"]);
                p_description = Convert.ToString(dr["p_description"]);
                p_userid = new Guid(dr["p_userid"].ToString());
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            p_id = Convert.ToInt64("0");
            p_code = Convert.ToString("NA");
            p_procedure = Convert.ToString("NA");
            p_description = Convert.ToString("NA");
            p_userid = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.p_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_ProceduresMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 p_id)
        {
            Boolean blnResult = false;
            this.p_id = p_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_ProceduresMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 p_id)
        {
            Boolean blnResult = false;
            this.p_id = p_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_ProceduresMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_ProceduresMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByAdmin(Guid adminid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.p_userid = adminid;
                clsCMD.CommandText = "sp_ProceduresMasterGetData";
                SetGetSPFlag = "ALLBYADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsProceduresMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsProceduresMaster_PropertiesList> lstResult = new List<clsProceduresMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].p_id = Convert.ToInt64(dr["p_id"]);
                lstResult[i].p_code = Convert.ToString(dr["p_code"]);
                lstResult[i].p_procedure = Convert.ToString(dr["p_procedure"]);
                lstResult[i].p_description = Convert.ToString(dr["p_description"]);
                lstResult[i].p_userid = new Guid(dr["p_userid"].ToString());

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 p_id)
        {
            Boolean blnResult = false;
            try
            {
                this.p_id = p_id;
                clsCMD.CommandText = "sp_ProceduresMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 p_id)
        {
            Boolean blnResult = false;
            try
            {
                this.p_id = p_id;
                clsCMD.CommandText = "sp_ProceduresMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsProceduresMaster_PropertiesList
    {
        public Int64 p_id { get; set; }
        public String p_code { get; set; }
        public String p_procedure { get; set; }
        public String p_description { get; set; }
        public Guid p_userid { get; set; }
    }
}