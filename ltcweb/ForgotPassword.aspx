﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <asp:Label ID="lblpagemessage" runat="server" Text=""></asp:Label>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3>
                        <i class="icon-lock"></i>
                        Retrival Forgoted Password
								</h3>
                </div>
                <div id="dvPasswordReset" class="box-content" runat="server" visible="false">
                    <div class="box box-bordered box-color">
                        <div class="box-title">
                            <h3><i class="icon-user"></i>Forgot Password</h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class='form-horizontal form-bordered'>
                                <div class="control-group">
                                    <label for="textfield" class="control-label">Username</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtUsername" runat="server" placeholder="Username" class="complexify-me input-xlarge"></asp:TextBox>
                                        <span class="help-block">Enter your username system will send mail on registred email for change password</span>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <asp:Button ID="cmdGetUsername" runat="server" CssClass="btn btn-primary" Text="Reset Password" OnClick="cmdGetUsername_Click" />
                                    <asp:Label ID="lblGetPassword" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="dvPasswordChange" class="box-content" runat="server" visible="false">
                    <div class="box box-bordered box-color">
                        <div class="box-title">
                            <h3><i class="icon-user"></i>Change Password</h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class='form-horizontal form-bordered'>
                                <div class="control-group">
                                    <label for="txtNewPassword" class="control-label">New Password</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtNewPassword" runat="server" placeholder="New Password" TextMode="Password" class="complexify-me input-xlarge"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="txtConformPassword" class="control-label">Conform Password</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtConformPassword" runat="server" placeholder="Conform Password" TextMode="Password" class="complexify-me input-xlarge"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <asp:Button ID="cmdChangePassword" runat="server" CssClass="btn btn-primary" Text="Change Password" OnClick="cmdChangePassword_Click" />
                                    <asp:Label ID="lblChangepass" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

