﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientAddiction.aspx.cs" Inherits="Clients_Assessment_ClientAddiction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
    <UC:AssessmentStepLinks ID="ucasslinks" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="box">
        <div class="box-title">
            <h3>
                <i class="glyphicon-smoking"></i>
                Client's Addiction Information
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:Addiction ID="ucAddiction" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
     <script type="text/javascript">
         $(document).ready(function () {
             $("textarea").keyup(function (e) {
                 while ($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                     $(this).height($(this).height() + 1);
                 };
             });
         });
    </script>
</asp:Content>

