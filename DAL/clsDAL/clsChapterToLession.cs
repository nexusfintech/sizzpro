﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsChapterToLession
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsChapterToLession()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsChapterToLession()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _ctl_id; }
            set
            {
                _ctl_id = value;
                if (clsCMD.Parameters.Contains("@ctl_id"))
                { clsCMD.Parameters["@ctl_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ctl_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _ByUser = new Guid();
        public Guid ByUser
        {
            get { return _ByUser; }
            set
            {
                _ByUser = value;
                if (clsCMD.Parameters.Contains("@User"))
                { clsCMD.Parameters["@User"].Value = value; }
                else { clsCMD.Parameters.Add("@User", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _ctl_id;
        public Int64 ctl_id
        {
            get { return _ctl_id; }
            set
            {
                _ctl_id = value;
                if (clsCMD.Parameters.Contains("@ctl_id"))
                { clsCMD.Parameters["@ctl_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ctl_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _ctl_chapterid;
        public Int64 ctl_chapterid
        {
            get { return _ctl_chapterid; }
            set
            {
                _ctl_chapterid = value;
                if (clsCMD.Parameters.Contains("@ctl_chapterid"))
                { clsCMD.Parameters["@ctl_chapterid"].Value = value; }
                else { clsCMD.Parameters.Add("@ctl_chapterid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _ctl_lessionid;
        public Int64 ctl_lessionid
        {
            get { return _ctl_lessionid; }
            set
            {
                _ctl_lessionid = value;
                if (clsCMD.Parameters.Contains("@ctl_lessionid"))
                { clsCMD.Parameters["@ctl_lessionid"].Value = value; }
                else { clsCMD.Parameters.Add("@ctl_lessionid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _ctl_position;
        public Int64 ctl_position
        {
            get { return _ctl_position; }
            set
            {
                _ctl_position = value;
                if (clsCMD.Parameters.Contains("@ctl_position"))
                { clsCMD.Parameters["@ctl_position"].Value = value; }
                else { clsCMD.Parameters.Add("@ctl_position", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _ctl_userid;
        public Guid ctl_userid
        {
            get { return _ctl_userid; }
            set
            {
                _ctl_userid = value;
                if (clsCMD.Parameters.Contains("@ctl_userid"))
                { clsCMD.Parameters["@ctl_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@ctl_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                ctl_id = Convert.ToInt64(dr["ctl_id"]);
                ctl_chapterid = Convert.ToInt64(dr["ctl_chapterid"]);
                ctl_lessionid = Convert.ToInt64(dr["ctl_lessionid"]);
                ctl_position = Convert.ToInt64(dr["ctl_position"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            ctl_id = Convert.ToInt64("0");
            ctl_chapterid = Convert.ToInt64("0");
            ctl_lessionid = Convert.ToInt64("0");
            ctl_position = Convert.ToInt64("0");
            ctl_userid = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ctl_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "chapterstolessionsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 ctl_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ctl_id = ctl_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "chapterstolessionsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 ctl_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ctl_id = ctl_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "chapterstolessionsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByChpid(Int64 intchpid)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ctl_chapterid = intchpid;
            this.AddEditDeleteFlag = "DELBYCHPID";
            try
            {
                clsCMD.CommandText = "chapterstolessionsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                clsCMD.CommandText = "chapterstolessionsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetLessionChapterWise(Int64 intChpID)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                this.ctl_chapterid = intChpID;
                clsCMD.CommandText = "chapterstolessionsGetData";
                SetGetSPFlag = "GETLESSIONCHPWISE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public Int64 GetPreviousLessonStatus(Int64 intCURPosID, Int64 intCHPID, Guid usrUserID)
        {
            OpenConnection();
            Int64 intResult = 0;
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                this.ctl_position = intCURPosID;
                this.ctl_chapterid = intCHPID;
                this.ctl_userid = usrUserID;
                clsCMD.CommandText = "chapterstolessionsGetData";
                SetGetSPFlag = "GETPRVLSNSTS";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                {
                    DataRow dr = dtResult.Rows[0];
                    intResult = Convert.ToInt64(dr["lsn_status"]);
                }
                else { intResult = 0; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return intResult;
        }

        public DataTable GetAllByChapterID(Int64 intChpID, Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ctl_chapterid = intChpID;
                this.ByUser = MemberId;
                clsCMD.CommandText = "chapterstolessionsGetData";
                SetGetSPFlag = "BYCHPID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<Int64> GetLsnByChapterID(Int64 intChpId, Guid MemberId)
        {

            OpenConnection();
            List<Int64> chapters = new List<Int64>();
            try
            {
                this.ctl_chapterid = intChpId;
                this.ByUser = MemberId;
                clsCMD.CommandText = "chapterstolessionsGetData";
                SetGetSPFlag = "BYCHPID";
                SqlDataReader SDR = clsCMD.ExecuteReader();
                while (SDR.Read())
                {
                    Int64 chp = Convert.ToInt64(SDR["lsn_id"]);
                    chapters.Add(chp);
                }
                SDR.Close();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                chapters = null;
            }
            CloseConnection();
            return chapters;
        }

        public DataTable GetAllByChapterIDNotMapped(Int64 intChpID,Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                this.ctl_chapterid = intChpID;
                this.ByUser = MemberId;
                clsCMD.CommandText = "chapterstolessionsGetData";
                SetGetSPFlag = "BYCHPIDNOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsChapterToLession_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsChapterToLession_PropertiesList> lstResult = new List<clsChapterToLession_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].ctl_id = Convert.ToInt64(dr["ctl_id"]);
                lstResult[i].ctl_chapterid = Convert.ToInt64(dr["ctl_chapterid"]);
                lstResult[i].ctl_lessionid = Convert.ToInt64(dr["ctl_lessionid"]);
                lstResult[i].ctl_position = Convert.ToInt64(dr["ctl_position"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 ctl_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.ctl_id = ctl_id;
                clsCMD.CommandText = "chapterstolessionsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 ctl_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.ctl_id = ctl_id;
                clsCMD.CommandText = "chapterstolessionsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public DataTable GetChapterLesson(Int64 chpid)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ctl_chapterid = chpid;
                this.ByUser = new Guid();
                clsCMD.CommandText = "chapterstolessionsGetData";
                SetGetSPFlag = "CHAPTLESSON";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        #endregion

    }

    public class clsChapterToLession_PropertiesList
    {
        public Int64 ctl_id { get; set; }
        public Int64 ctl_chapterid { get; set; }
        public Int64 ctl_lessionid { get; set; }
        public Int64 ctl_position { get; set; }
    }
}