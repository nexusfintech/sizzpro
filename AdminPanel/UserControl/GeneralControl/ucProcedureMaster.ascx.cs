﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;
public partial class UserControl_GeneralControl_ucProcedureMaster : System.Web.UI.UserControl
{
    clsProceduresMaster clsPM = new clsProceduresMaster();
    private Int64 intId = 0;
    private string strName = string.Empty;

    public Int64 SelectedValue
    {
        get { intId = Convert.ToInt64(ddlProcedure.SelectedValue); return intId; }
        set { 
            intId = value;
            ListItem itm = ddlProcedure.Items.FindByValue(intId.ToString());
            if (itm != null)
            {
                ddlProcedure.SelectedIndex = ddlProcedure.Items.IndexOf(itm);
            }
        }
    }

    public string SelectedName
    {
        get { strName = ddlProcedure.SelectedItem.Text; return strName; }
        set { strName = value; }
    }

    public int Width
    {
        set { ddlProcedure.Width = Unit.Pixel(value); }
    }

    public void Bind()
    {
        DataTable dt = clsPM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        dt.Columns.Add("Procedure", typeof(string), "p_code + ' - '  + p_procedure");
        ddlProcedure.DataTextField = "Procedure";
        ddlProcedure.DataValueField = "p_code";
        ddlProcedure.DataSource = dt;
        ddlProcedure.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}