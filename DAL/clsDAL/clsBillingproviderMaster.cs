﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsBillingproviderMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsBillingproviderMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsBillingproviderMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _bpm_id; }
            set
            {
                _bpm_id = value;
                if (clsCMD.Parameters.Contains("@bpm_id"))
                { clsCMD.Parameters["@bpm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _bpm_id;
        public Int64 bpm_id
        {
            get { return _bpm_id; }
            set
            {
                _bpm_id = value;
                if (clsCMD.Parameters.Contains("@bpm_id"))
                { clsCMD.Parameters["@bpm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _bpm_name;
        public String bpm_name
        {
            get { return _bpm_name; }
            set
            {
                _bpm_name = value;
                if (clsCMD.Parameters.Contains("@bpm_name"))
                { clsCMD.Parameters["@bpm_name"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_name", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _bpm_address;
        public String bpm_address
        {
            get { return _bpm_address; }
            set
            {
                _bpm_address = value;
                if (clsCMD.Parameters.Contains("@bpm_address"))
                { clsCMD.Parameters["@bpm_address"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_address", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _bpm_city;
        public String bpm_city
        {
            get { return _bpm_city; }
            set
            {
                _bpm_city = value;
                if (clsCMD.Parameters.Contains("@bpm_city"))
                { clsCMD.Parameters["@bpm_city"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_city", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _bpm_state;
        public Int64 bpm_state
        {
            get { return _bpm_state; }
            set
            {
                _bpm_state = value;
                if (clsCMD.Parameters.Contains("@bpm_state"))
                { clsCMD.Parameters["@bpm_state"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_state", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _bpm_zip;
        public String bpm_zip
        {
            get { return _bpm_zip; }
            set
            {
                _bpm_zip = value;
                if (clsCMD.Parameters.Contains("@bpm_zip"))
                { clsCMD.Parameters["@bpm_zip"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_zip", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _bpm_phone;
        public String bpm_phone
        {
            get { return _bpm_phone; }
            set
            {
                _bpm_phone = value;
                if (clsCMD.Parameters.Contains("@bpm_phone"))
                { clsCMD.Parameters["@bpm_phone"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_phone", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _bpm_fax;
        public String bpm_fax
        {
            get { return _bpm_fax; }
            set
            {
                _bpm_fax = value;
                if (clsCMD.Parameters.Contains("@bpm_fax"))
                { clsCMD.Parameters["@bpm_fax"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_fax", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Guid _bpm_userid;
        public Guid bpm_userid
        {
            get { return _bpm_userid; }
            set
            {
                _bpm_userid = value;
                if (clsCMD.Parameters.Contains("@bpm_userid"))
                { clsCMD.Parameters["@bpm_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _bpm_taxid;
        public String bpm_taxid
        {
            get { return _bpm_taxid; }
            set
            {
                _bpm_taxid = value;
                if (clsCMD.Parameters.Contains("@bpm_taxid"))
                { clsCMD.Parameters["@bpm_taxid"].Value = value; }
                else { clsCMD.Parameters.Add("@bpm_taxid", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                bpm_id = Convert.ToInt64(dr["bpm_id"]);
                bpm_name = Convert.ToString(dr["bpm_name"]);
                bpm_address = Convert.ToString(dr["bpm_address"]);
                bpm_city = Convert.ToString(dr["bpm_city"]);
                bpm_state = Convert.ToInt64(dr["bpm_state"]);
                bpm_zip = Convert.ToString(dr["bpm_zip"]);
                bpm_phone = Convert.ToString(dr["bpm_phone"]);
                bpm_fax = Convert.ToString(dr["bpm_fax"]);
                bpm_userid = new Guid(dr["bpm_userid"].ToString());
                bpm_taxid = dr["bpm_taxid"].ToString();
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            bpm_id = Convert.ToInt64("0");
            bpm_name = Convert.ToString("NA");
            bpm_address = Convert.ToString("NA");
            bpm_city = Convert.ToString("NA");
            bpm_state = Convert.ToInt64("0");
            bpm_zip = Convert.ToString("NA");
            bpm_phone = Convert.ToString("NA");
            bpm_fax = Convert.ToString("NA");
            bpm_taxid = string.Empty;
            bpm_userid = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.bpm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_BillingproviderMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 bpm_id)
        {
            Boolean blnResult = false;
            this.bpm_id = bpm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_BillingproviderMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 bpm_id)
        {
            Boolean blnResult = false;
            this.bpm_id = bpm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_BillingproviderMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_BillingproviderMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByAdmin(Guid adminid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.bpm_userid = adminid;
                clsCMD.CommandText = "sp_BillingproviderMasterGetData";
                SetGetSPFlag = "ALLBYADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsBillingproviderMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsBillingproviderMaster_PropertiesList> lstResult = new List<clsBillingproviderMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].bpm_id = Convert.ToInt64(dr["bpm_id"]);
                lstResult[i].bpm_name = Convert.ToString(dr["bpm_name"]);
                lstResult[i].bpm_address = Convert.ToString(dr["bpm_address"]);
                lstResult[i].bpm_city = Convert.ToString(dr["bpm_city"]);
                lstResult[i].bpm_state = Convert.ToInt64(dr["bpm_state"]);
                lstResult[i].bpm_zip = Convert.ToString(dr["bpm_zip"]);
                lstResult[i].bpm_phone = Convert.ToString(dr["bpm_phone"]);
                lstResult[i].bpm_fax = Convert.ToString(dr["bpm_fax"]);
                lstResult[i].bpm_userid = new Guid(dr["bpm_userid"].ToString());
                lstResult[i].bpm_taxid = Convert.ToString(dr["bpm_taxid"]);
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 bpm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.bpm_id = bpm_id;
                clsCMD.CommandText = "sp_BillingproviderMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 bpm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.bpm_id = bpm_id;
                clsCMD.CommandText = "sp_BillingproviderMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsBillingproviderMaster_PropertiesList
    {
        public Int64 bpm_id { get; set; }
        public String bpm_name { get; set; }
        public String bpm_address { get; set; }
        public String bpm_city { get; set; }
        public Int64 bpm_state { get; set; }
        public String bpm_zip { get; set; }
        public String bpm_phone { get; set; }
        public String bpm_fax { get; set; }
        public String bpm_taxid { get; set; }
        public Guid bpm_userid { get; set; }
    }
}