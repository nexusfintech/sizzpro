﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.IO;
using System.Web.Security;

public partial class Users_UserClientManage : System.Web.UI.Page
{
    clsProfileMaster clsPro = new clsProfileMaster();
    clsICdetails path = new clsICdetails();
    clsClientParentMapping clsCPM = new clsClientParentMapping();

    private void BindGrid()
    {
        if (Page.User.IsInRole("user"))
        {
            dtClientDisp.DataSource = clsPro.GetClientListByParent(clsCPM.GetParent((Guid)Membership.GetUser().ProviderUserKey));
        }
        else if (Page.User.IsInRole("member"))
        {
            dtClientDisp.DataSource = clsPro.GetClientListByParent((Guid)Membership.GetUser().ProviderUserKey);
        }
        else
        {
            dtClientDisp.DataSource = clsPro.GetClientList();
        }
        dtClientDisp.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        lnkCreateNew.NavigateUrl = "~/Users/ClientRegistration.aspx";
        if (!Page.IsPostBack)
        {
            BindGrid();
        }
    }

    protected void dtClientDisp_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "SESSION")
        {
            Session["sessionuserid"] = e.CommandArgument.ToString();
            //Response.Redirect("~/Admin/SessionManagement/SessionManagement.aspx");
            Response.Redirect("~/Admin/SessionManagement/FollowupSession.aspx");
        }
        if (e.CommandName == "EDIT")
        {
            string[] commandArgs = e.CommandArgument.ToString().Split('@');
            Session["form_memberkey"] = commandArgs[0];
            Session["form_userid"] = commandArgs[1];
            Response.Redirect("~/Clients/Assessment/AssessmentIntroManage.aspx");
        }
        //if (e.CommandName == "VIEW")
        //{
        //    string[] commandArgs = e.CommandArgument.ToString().Split('@');
        //    string filepath = path.getPathByGuid(new Guid(commandArgs[0]));
        //    if (File.Exists(filepath))
        //    {
        //        Response.ContentType = "application/pdf";
        //        Response.AppendHeader("Content-Disposition", "attachment;filename=" + "IC_" + commandArgs[0].ToString() + ".pdf");
        //        Response.TransmitFile(filepath);
        //        HttpContext.Current.ApplicationInstance.CompleteRequest();
        //    }
        //}
    }
}