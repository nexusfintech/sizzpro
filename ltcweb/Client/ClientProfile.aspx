﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientProfile.aspx.cs" Inherits="Client_ClientProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" Runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#lnkYou").click(function () {
                alert("Handler for .click() called.");
                return false;
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
        <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-group"></i>
                Client Profile
            </h3>
        </div>
        <asp:Label ID="lblMessage" runat="server" Text="Label" Visible="false"></asp:Label>
        <div class="box-content nopadding">
            <UC:UserProfile ID="ucProfile" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>

