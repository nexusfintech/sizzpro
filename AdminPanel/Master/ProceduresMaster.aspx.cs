﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_ProceduresMaster : System.Web.UI.Page
{
    clsProceduresMaster clsPM = new clsProceduresMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptPrcd.DataSource = clsPM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        rptPrcd.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
         MySession.Current.AddEditFlag = "ADD";
         Response.Redirect("~/Master/ProceduresAddEdit.aspx");
    }
    protected void rptPrcd_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = e.CommandName;
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/ProceduresAddEdit.aspx");
        }
        if (e.CommandName == "DELETE")
        {
            Boolean blnRslt = clsPM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (blnRslt)
            {
                MySession.Current.MESSAGE = "Record Deleted Successfully";
                Response.Redirect("~/Master/ProceduresMaster.aspx");
            }
            else
            {
                lblmessage.Text = clsPM.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
}