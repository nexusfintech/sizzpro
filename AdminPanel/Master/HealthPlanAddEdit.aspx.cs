﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Master_HealthPlanAddEdit : System.Web.UI.Page
{
    clsZipMaster clszip = new clsZipMaster();
    clsPayerMaster clsPM = new clsPayerMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindmasters();
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                loaddata();
            }
        }
    }
    private void bindmasters()
    {
        ddlState.Items.Clear();
        ddlState.SelectedIndex = -1;
        ddlState.SelectedValue = null;
        ddlState.ClearSelection();
        ddlState.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<< -- Pleasse Select -- >>";
        lst.Selected = true;
        //ddlState.Items.Add(lst);
        ddlState.DataSource = clszip.GetAllByMasterFlag("STATE");
        ddlState.DataTextField = "zip_name";
        ddlState.DataValueField = "zip_id";
        ddlState.DataBind();
    }

    private void loaddata()
    {
        Boolean blnRslt = clsPM.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {
            txtNote.Text = clsPM.pm_note;
            txtpyrid.Text = clsPM.pm_Payerid;
            txtpyrname.Text = clsPM.pm_PayerName;
            if (!clsPM.pm_ENR)
            {
                ListItem li = ddlEnr.Items.FindByValue("0");
                if (li != null)
                {
                    ddlEnr.SelectedIndex = ddlEnr.Items.IndexOf(li);
                }
            }
            if (!clsPM.pm_RTE1)
            {
                ListItem li = ddlRte1.Items.FindByValue("0");
                if (li != null)
                {
                    ddlRte1.SelectedIndex = ddlRte1.Items.IndexOf(li);
                }
            }
            if (!clsPM.pm_RTE2)
            {
                ListItem li = ddlRte2.Items.FindByValue("0");
                if (li != null)
                {
                    ddlRte2.SelectedIndex = ddlRte2.Items.IndexOf(li);
                }
            }
            if (!clsPM.pm_RTS)
            {
                ListItem li = ddlRts.Items.FindByValue("0");
                if (li != null)
                {
                    ddlRts.SelectedIndex = ddlRts.Items.IndexOf(li);
                }
            }
            if (!clsPM.pm_ERA)
            {
                ListItem li = ddlEra.Items.FindByValue("0");
                if (li != null)
                {
                    ddlEra.SelectedIndex = ddlEra.Items.IndexOf(li);
                }
            }
            if (!clsPM.pm_SEC)
            {
                ListItem li = ddlSec.Items.FindByValue("0");
                if (li != null)
                {
                    ddlSec.SelectedIndex = ddlSec.Items.IndexOf(li);
                }
            }
        }
        else
        {
            MySession.Current.MESSAGE = clsPM.GetSetErrorMessage;
            Response.Redirect("~/Master/HealthPlanMaster.aspx");
        }
    }

    private string validatepage()
    {
        string result = string.Empty;
        if (txtpyrid.Text == string.Empty)
        {
            return result = "Payer Id Required";
        }
        else if (txtpyrname.Text == string.Empty)
        {
            return result = "Payer Name Required";
        }
        else
        {
            return result;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string vldtrslt = validatepage();
        if (vldtrslt != string.Empty)
        {
            lblmessage.Text = vldtrslt;
            msgdiv.Visible = true;
            return;
        }
        clsPM.pm_Payerid = txtpyrid.Text;
        clsPM.pm_PayerName = txtpyrname.Text;
        clsPM.pm_state = Convert.ToInt64(ddlState.SelectedValue);
        clsPM.pm_TYP = ddlType.SelectedValue;
        clsPM.pm_LOB = ddllob.SelectedValue;
        clsPM.pm_note = txtNote.Text;
        if (Convert.ToInt32(ddlEnr.SelectedValue) == 1)
        {
            clsPM.pm_ENR = true;
        }
        if (Convert.ToInt32(ddlRte1.SelectedValue) == 1)
        {
            clsPM.pm_RTE1 = true;
        }
        if (Convert.ToInt32(ddlRte2.SelectedValue) == 1)
        {
            clsPM.pm_RTE2 = true;
        }
        if (Convert.ToInt32(ddlRts.SelectedValue) == 1)
        {
            clsPM.pm_RTS = true;
        }
        if (Convert.ToInt32(ddlEra.SelectedValue) == 1)
        {
            clsPM.pm_ERA = true;
        }
        if (Convert.ToInt32(ddlSec.SelectedValue) == 1)
        {
            clsPM.pm_SEC = true;
        }
        if (Convert.ToInt32(ddlEnr.SelectedValue) == 1)
        {
            clsPM.pm_ENR = true;
        }
        Boolean blnSvRslt;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnSvRslt = clsPM.Save();
        }
        else
        {
            blnSvRslt = clsPM.Update(MySession.Current.EditID);
        }
        if (blnSvRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/HealthPlanMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsPM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/HealthPlanMaster.aspx");
    }
}