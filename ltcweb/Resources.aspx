﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Resources.aspx.cs" Inherits="Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="glyphicon-user_add"></i>&nbsp;Resources</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
                <tbody>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td colspan="8">
                                        <div align="center"><span class="headline">COUNSELING RESOURCES</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="8">Life is full of many opportunities for personal growth and development. We all have so much to learn about ourselves and our relationships. At any given moment you can find yourself scratching your head trying to figure out where to go from here. There are many resources that are available to assist us in this growth process. Lifestyle Therapy and Coaching seeks to provide resources that you can use to grow and achieve your objectives in life. </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr style="background-color: lightblue;">
                                    <td>Seminar Series </td>
                                    <td>
                                        <h3>Description</h3>
                                    </td>
                                    <td>
                                        <h3 align="center">CD</h3>
                                    </td>
                                    <td>
                                        <h3 align="center">DVD</h3>
                                    </td>
                                    <td>
                                        <h3 align="center">MP3</h3>
                                    </td>
                                    <td>
                                        <h3 align="center">Price</h3>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Finding and Being the Right Mate</strong></td>
                                    <td>Joseph Follette discusses what to look for in a mate and how to prepare for marriage.</td>
                                    <td>
                                        <div align="center"></div>
                                    </td>
                                    <td>
                                        <div align="center">
                                            <img src="http://www.lifestyletherapycoach.com/images/graphics/checkmarkB.png" alt="check">
                                        </div>
                                    </td>
                                    <td>
                                        <div align="center"></div>
                                    </td>
                                    <td><strong>$20</strong></td>
                                    <td>
                                        <%--<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="7RJ7UAUXKWGXS">
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                        </form>--%>
                                         <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="7RJ7UAUXKWGXS">
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                        </form>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Finding and Being the RIGHT Mate Smoothe Mix</strong></td>
                                    <td>Smooth sounds behind inspirational talk on finding the right mate.</td>
                                    <td>
                                        <div align="center">
                                            <img src="http://www.lifestyletherapycoach.com/images/graphics/checkmarkB.png" alt="check">
                                        </div>
                                    </td>
                                    <td>
                                        <div align="center"></div>
                                    </td>
                                    <td>
                                        <div align="center">
                                            <img src="http://www.lifestyletherapycoach.com/images/graphics/checkmarkB.png" alt="check">
                                        </div>
                                    </td>
                                    <td>$5</td>
                                    <td>
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="V2YPDCFYEMDMY">
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                        </form>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Selfishness vs. Self-fulfillment in Marriage</strong></td>
                                    <td>Joseph Follette discusses what it takes to be fulfilled in marriage.</td>
                                    <td>
                                        <div align="center"></div>
                                    </td>
                                    <td>
                                        <div align="center">
                                            <img src="http://www.lifestyletherapycoach.com/images/graphics/checkmarkB.png" alt="check">
                                        </div>
                                    </td>
                                    <td>
                                        <div align="center"></div>
                                    </td>
                                    <td><strong>$20</strong></td>
                                    <td>
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="AA3N6NDE2UNQ6">
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                        </form>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>When Depression Strikes Home</strong></td>
                                    <td>Joseph Follette discusses how to deal with depression in your marriage.</td>
                                    <td>
                                        <div align="center"></div>
                                    </td>
                                    <td>
                                        <div align="center">
                                            <img src="http://www.lifestyletherapycoach.com/images/graphics/checkmarkB.png" alt="check">
                                        </div>
                                    </td>
                                    <td>
                                        <div align="center"></div>
                                    </td>
                                    <td><strong>$20</strong></td>
                                    <td>
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="UPCVCZSJCCXNJ">
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                        </form>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Marital Intimacy Booster Series</strong></td>
                                    <td>
                                        <p>Joseph Follette takes you throught the 5 C's of Boosting Your Intimacy in marriage</p>
                                        <p></p>
                                    </td>
                                    <td>
                                        <div align="center"></div>
                                    </td>
                                    <td>
                                        <div align="center">
                                            <img src="http://www.lifestyletherapycoach.com/images/graphics/checkmarkB.png" alt="check">
                                        </div>
                                    </td>
                                    <td>
                                        <div align="center"></div>
                                    </td>
                                    <td><strong>$59</strong></td>
                                    <td>
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="FDG3EENNT5SKA">
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                        </form>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td height="31">
                                        <div align="center">
                                            <script type="text/javascript" data-pp-pubid="ad1d96e645" data-pp-placementtype="468x60"> (function (d, t) {
     "use strict";
     var s = d.getElementsByTagName(t)[0], n = d.createElement(t);
     n.src = "//paypal.adtag.where.com/merchant.js";
     s.parentNode.insertBefore(n, s);
 }(document, "script"));
                                            </script>
                                        </div>
                                    </td>
                                    <td height="31">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="31">
                                        <h2><span class="headline"><strong>Counseling Services</strong></span></h2>
                                    </td>
                                    <td height="31">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <p align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=C5347RYB2EV78" target="_self">Therapy/Counseling/Coaching/Copay</a></p>
                                        <p align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=35JVVKW3BUD9A" target="_blank">Therapy with Alicia Lomack</a></p>
                                        <p align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=S2DWM4ZZ8KHV4" target="_self">Therapy with Celina Villalobos</a></p>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=XZW3LKT66E328">Group Therapy Session ($45)</a></div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=TALDYPY8RY624">Group Therapy Monthly Fee ($150)</a></div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr style="background-color:lightblue;">
                                                <td>
                                                    <h2><strong>Supervision</strong> <strong>Fees</strong></h2>
                                                </td>
                                                <td>
                                                    <div align="left">
                                                        <h2><strong>Session Fees</strong></h2>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div align="left">
                                                        <h2><strong>Email Coaching</strong></h2>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                                        <input type="hidden" name="cmd" value="_s-xclick">
                                                        <input type="hidden" name="hosted_button_id" value="QPM58TBZN5LD2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <input type="hidden" name="on0" value="Payment Amount">Payment Amount</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <select name="os0">
                                                                        <option value="Monthly">Monthly $300.00 USD</option>
                                                                        <option value="Weekly">Weekly $85.00 USD</option>
                                                                        <option value="Two Months">Two Months $600.00 USD</option>
                                                                        <option value="Three Months">Three Months $900.00 USD</option>
                                                                        <option value="Four Months">Four Months $1,200.00 USD</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <input type="hidden" name="currency_code" value="USD">
                                                        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                                    </form>
                                                </td>
                                                <td>
                                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                                        <input type="hidden" name="cmd" value="_s-xclick">
                                                        <input type="hidden" name="hosted_button_id" value="Q8WKPCLYBANFE">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <input type="hidden" name="on0" value="Purchase Minutes">Purchase Minutes</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <select name="os0">
                                                                        <option value="5 Minutes">5 Minutes $10.00 USD</option>
                                                                        <option value="10 Minutes">10 Minutes $15.00 USD</option>
                                                                        <option value="15 Minutes">15 Minutes $20.00 USD</option>
                                                                        <option value="30 Minutes">30 Minutes $40.00 USD</option>
                                                                        <option value="50 Minutes">50 Minutes $81.00 USD</option>
                                                                        <option value="60 Minutes">60 Minutes $96.00 USD</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <input type="hidden" name="currency_code" value="USD">
                                                        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                                    </form>
                                                </td>
                                                <td>
                                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                                        <input type="hidden" name="cmd2" value="_s-xclick">
                                                        <input type="hidden" name="hosted_button_id4" value="FJSWHG5HGE6NU">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <input type="hidden" name="on" value="Purchase Minutes">
                                                                    Purchase Minutes</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <select name="os">
                                                                        <option value="5 Emails">5 Emails $25.00</option>
                                                                        <option value="10 Emails">10 Emails $45.00</option>
                                                                        <option value="20 Emails">20 Emails $80.00</option>
                                                                        <option value="Annual Unlimited Email">Annual Unlimited Email $100.00</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <input type="hidden" name="currency_code2" value="USD">
                                                        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit4" alt="PayPal - The safer, easier way to pay online!">
                                                        <img alt="1" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                                    </form>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr style="background-color:lightblue;">
                                    <td height="25">
                                        <h2>Counseling Packages</h2>
                                    </td>
                                    <td height="25">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>
                                        <div align="left"><strong>6 Sessions</strong> $450</div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                            <input type="hidden" name="cmd5" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id5" value="AEFHNCKN9LTM2">
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit5" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                        </form>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr style="background-color:lightblue;">
                                    <td height="25">
                                        <h2>Programs</h2>
                                    </td>
                                    <td height="25">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=5MHDUC2LB7KDW">RecoveringMyLife Registration Fee</a></div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=E3CF5MB98SM4W">RecoveringMyLife Program Monthly Pay</a></div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=PQTECWG2P3W7Q">RecoveringMyLife Registration Down Payment</a>&nbsp;</div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <p align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VHJMMSB99J35U" target="_self">Sizzling Hot Marriage Maker Registration Fee</a></p>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=U6MXRWKLXRSZQ">Sizzling Hot Marriage Maker Monthly Pay</a></div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="left"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=PNJSPHGXZ3FS8">Sizzling Hot Marriage Maker Down Payment</a></div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><a href="http://www.youtube.com/watch?v=QFddFVVJb4Q">Add-On Program Monthly $50 Fee</a></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr style="background-color:lightblue;">
                                    <td height="25">
                                        <h2>Assessments</h2>
                                    </td>
                                    <td height="25">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=TPLYHBVPKUBL2" target="_blank">Prepare-Enrich Relationship Assessment $199</a></strong></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td><strong><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=V8MATE5UGTE6J">Prepare-Enrich Relationship Assessment $70 (3 Payments) </a></strong></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <ul>
                                <strong class="headline">Huntsville Mental Health Resources</strong>
                                <div id="abw2">
                                    <div id="abm2">
                                        <div id="abc2">
                                            <div id="articlebody2">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td>

                                                                <p>&nbsp;</p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        HELPline, 716-1000             
                                    </div>
                                </div>
                                <li>Family Services Center, 600 St. Clair Bldg. 3, 256-551-1610</li>
                                <li>Mental Health Center, 4040 So. Memorial Pkwy., 256-533-1970</li>
                                <li>Mental Health Association, 701 Andrew Jackson Way, 256-536-9441</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style1"></td>
                    </tr>
                </tbody>
            </table>
            <p>
                <iframe src="http://astore.amazon.com/freedomclubco-20" width="100%" height="950" frameborder="2" scrolling="no"></iframe>
            </p>
            <table width="849">
                <tr>
                    <td>
                        <script charset="utf-8" type="text/javascript" src="http://ws.amazon.com/widgets/q?rt=tf_sw&ServiceVersion=20070822&MarketPlace=US&ID=V20070822/US/freedomclubco-20/8002/bb33be96-446e-4a7f-9825-84ad6957219d"> </script>
                        <noscript>
          <div align="center"><A HREF="http://ws.amazon.com/widgets/q?rt=tf_sw&ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Ffreedomclubco-20%2F8002%2Fbb33be96-446e-4a7f-9825-84ad6957219d&Operation=NoScript">Amazon.com Widgets</A></div>
        </noscript>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

