﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Globalization;
using clsDAL;

public partial class UserControl_Assessment_ucInsurance : System.Web.UI.UserControl
{
    clsClientInsurance clsINS = new clsClientInsurance();
    clsInsuranceMaster clsINSM = new clsInsuranceMaster();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();

    clsUsers clsUsr = new clsUsers();
    private Int64 intUserID;
    private static Int64 intEditID;

    private static string strAddEditFlag;
    private string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void EmptyControls()
    {
        txtAuthoraztion.Text = "";
        txtcoinspay.Text = "";
        txtEffDate.Text = "";
        txtEmployeer.Text = "";
        txtInsPhoneNo.Text = "";
        txtINSSubscriber.Text = "";
        txtINSSubscriberGrpNO.Text = "";
        txtINSSubscriberID.Text = "";
        txtInsurancePay.Text = "";
        txtNote.Text = "";
        txtSubDOB.Text = "";
    }

    private string ValidatePage()
    {
        string strResult = "";
        if (txtEmployeer.Text.Trim() == "")
        { strResult += "Please Enter Employer Name <br/>"; }
        if (txtINSSubscriber.Text.Trim() == "")
        { strResult += "Please Enter Insurance Subscriber <br/>"; }
        if (txtINSSubscriberID.Text.Trim() == "")
        { strResult += "Please Enter Subscriber ID <br/>"; }
        return strResult;
    }
    
    private void BindGrid()
    {
        Guid objKey = new Guid(strMemberkey);
        dtInsurance.DataSource = clsINS.GetAllRecordByUserid(objKey);
        dtInsurance.DataBind();
    }

    private void BindCombo()
    {
        ddlInsurance.DataSource = clsINSM.GetAllForCombo();
        ddlInsurance.DataTextField = "inr_insurancename";
        ddlInsurance.DataValueField = "inr_id";
        ddlInsurance.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            strAddEditFlag = "ADD";
            txtSubDOB.Text = DateTime.Now.ToString("MM/dd/yyyy",CultureInfo.InvariantCulture);
            txtEffDate.Text = DateTime.Now.ToString("MM/dd/yyyy",CultureInfo.InvariantCulture);
            BindCombo();
            BindGrid();
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string strResult = ValidatePage();
        if (strResult != "")
        {
            lblMessage.Text = strResult;
            return;
        }
        DateTime dtEffectDate;
        if (!(DateTime.TryParseExact(txtEffDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtEffectDate)))
        {
            lblMessage.Text = "Invalid Effective Date format please enter in mm/dd/yyyy"; return;
        }
        clsINS.ins_effectdate = dtEffectDate;
        DateTime dtSubDOB;
        if (!(DateTime.TryParseExact(txtEffDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtSubDOB)))
        {
            lblMessage.Text = "Invalid Subscriber Date of Birth format please enter in mm/dd/yyyy"; return;
        }
        clsINS.ins_subscriberdob = dtSubDOB;
        clsINS.ins_authorization = txtAuthoraztion.Text;
        clsINS.ins_copayinspay = txtcoinspay.Text;
        clsINS.ins_insurancepay = txtInsurancePay.Text;
        clsINS.ins_employer = txtEmployeer.Text;
        clsINS.ins_inscompany = Convert.ToInt64(ddlInsurance.SelectedValue);
        clsINS.ins_insugroupnumber = txtINSSubscriberGrpNO.Text;
        clsINS.ins_insurancephone = txtInsPhoneNo.Text;
        clsINS.ins_note = txtNote.Text;
        ListItem li = ddlobtauth.SelectedItem;
        if (li != null)
        {
            clsINS.ins_obtauthorization = Convert.ToInt64(ddlobtauth.SelectedValue);
        }
        clsINS.ins_relationtosubscriber = ddlRelation.SelectedItem.Text;
        clsINS.ins_selftcharges = chkpayself.Checked;
        clsINS.ins_subscid = txtINSSubscriberID.Text;
        clsINS.ins_subscriber = txtINSSubscriber.Text;
        clsINS.ins_default = chkprimary.Checked;
        Guid objKey = new Guid(strMemberkey);
        clsINS.ins_userid = objKey;
        Boolean blnSave;
        if (strAddEditFlag == "ADD")
        {
            blnSave = clsINS.Save(); strAddEditFlag = "ADD";
        }
        else { blnSave = clsINS.Update(intEditID); }
        if (blnSave == true)
        { 
            lblMessage.Text = "Saved Successfully.";
            clsASR.storcrd_stepid = 10;
            clsASR.stprcrd_userid = objKey;
            Boolean stprslt = clsASR.Save();
            if (stprslt == true)
            {
                Response.Redirect("~/Client/Assessments/ClientPersoanlHistory.aspx");
            }
            strAddEditFlag = "ADD"; 
            BindGrid(); 
            EmptyControls(); 
        }
        else { lblMessage.Text = "Error in Save Process " + clsINS.GetSetErrorMessage; }
    }

    protected void dtInsurance_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            intEditID = Convert.ToInt64(e.CommandArgument);
            Boolean blnEdit = clsINS.GetRecordByIDInProperties(intEditID);
            if (blnEdit == true)
            { EditEntry(); }
            else { lblMessage.Text = "Error... Edit Process not proper complete please try again."; }
        }
    }

    private void EditEntry()
    {
        strAddEditFlag = "EDIT";
        txtAuthoraztion.Text = clsINS.ins_authorization;
        txtcoinspay.Text = clsINS.ins_copayinspay;
        txtInsurancePay.Text = clsINS.ins_insurancepay;
        txtEffDate.Text = clsINS.ins_effectdate.ToString("MM/dd/yyyy",CultureInfo.InvariantCulture);
        txtEmployeer.Text = clsINS.ins_employer;
        ListItem li = ddlInsurance.Items.FindByValue(clsINS.ins_inscompany.ToString());
        if (li != null)
        {
            ddlInsurance.SelectedIndex = ddlInsurance.Items.IndexOf(li);
        }
        txtINSSubscriberGrpNO.Text = clsINS.ins_insugroupnumber;
        txtInsPhoneNo.Text = clsINS.ins_insurancephone;
        txtNote.Text = clsINS.ins_note;


        ListItem li1 = ddlobtauth.Items.FindByValue(clsINS.ins_obtauthorization.ToString());
        if (li1 != null)
        {
            ddlobtauth.SelectedIndex = ddlobtauth.Items.IndexOf(li);
        }

        ddlRelation.Text = clsINS.ins_relationtosubscriber;
        chkpayself.Checked = clsINS.ins_selftcharges;
        txtINSSubscriberID.Text = clsINS.ins_subscid;
        txtINSSubscriber.Text = clsINS.ins_subscriber;
        txtSubDOB.Text = clsINS.ins_subscriberdob.ToString("MM/dd/yyyy",CultureInfo.InvariantCulture);
        chkprimary.Checked = clsINS.ins_default;
    }
}