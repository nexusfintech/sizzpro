﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MemberManagement.aspx.cs" Inherits="Users_MemberManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkCreateNew" runat="server"><i class="glyphicon-user_add"></i> Create New <%= MySession.Current.MemberAlias %></asp:HyperLink>
            </li>
            <li>
                <a href="#"><i class="icon-key"></i> Reset Password</a>
            </li>
            <li>
               <a href="#"><i class="icon-envelope"></i> Send Mail</a>
            </li>
            <li>
                <a href="#"><i class="icon-table"></i> Login History</a>
            </li>
        </ul>
    </div>
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Filters</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="glyphicon-table"></i>
                    <asp:Button ID="cmdShowAll" runat="server" CssClass="btn" Text="View Active" OnClick="cmdShowAll_Click"/></a>
            </li>
            <li>
                <a>
                    <i class="glyphicon-table"></i>
                    <asp:Button ID="cmdInactive" runat="server" CssClass="btn" Text="View InActive" OnClick="cmdInactive_Click"/></a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <UC:UsersList ID="ucUserList" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
    <!-- dataTables -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/jquery.dataTables.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/TableTools.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/ColReorderWithResize.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/ColVis.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/jquery.dataTables.columnFilter.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/jquery.dataTables.grouping.js") %>'></script>
</asp:Content>

