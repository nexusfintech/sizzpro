﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SiteMap.aspx.cs" Inherits="SiteMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-sitemap"></i>&nbsp;Site Map</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div class="span4">
                <strong>MAIN NAVIGATION LINKS:</strong><br />
                <a href="Default.aspx">Main Homepage</a><br />
                <a href="AboutUs.aspx">About Us</a><br />
                <a href="Philosophy.aspx">Our Philosophy</a><br />
                <a href="OurTherapists.aspx">Our Services</a><br />
                <a href="Resources.aspx">Resources</a><br />
                <a href="ContactUS.aspx">Contact Us </a>

            </div>
            <div class="span4">
                <strong>COUNSELING/THERAPY  SPECIALTIES:</strong>
                <br />
                <a href="depression.aspx">Depression Counseling</a>
                <br />
                <a href="anxiety.aspx">Anxiety Counseling</a>
                <br />
                <a href="family-counselling.aspx">Family Counseling</a>
                <br />
                <a href="marriage-counseling.aspx">Marriage Counseling</a>
                <br />
                <a href="couples-counselling.aspx">Couples Counseling</a>
                <br />
                <a href="individual-counselling.aspx">Individual Counseling</a>
                <br />
                <a href="grief-loss.aspx">Grief and Loss Therapy</a>
                <br />
                <a href="sexuality-issues.aspx">Sexuality Issues</a>
                <br />
                <a href="addictions-recovery.aspx">Addictions Recovery Therapy</a>
                <br />
                <a href="groups.aspx">Life Recovery Group</a>
                <br />
                <a href="sexualabusetherapy.aspx">Sexual Abuse Therapy</a><br />
            </div>
            <div class="span4">
                <strong>ADDITIONAL WEBSITE INFORMATION   LINKS: </strong>
                <br />
                <a href="FAQ.aspx">Frequently Asked Questions</a>
                <br />
                <a href="http://lifestyletherapycoach.com/network/blog/">BLOG</a>
                <br />
                <a href="Registration.aspx">Intake Form</a>
                <br />
                <a href="Client/Appointment.aspx">Take Appointment</a><br />
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <p><strong>PROGRAMS, EVENTS AND OFFERS: </strong></p>
            <p><a href="http://www.sizzlinghotmarriage.com" target="_blank">Sizzling Hot Marriage Maker Program</a> : Defying Ordinary Parenting Program</p>
            <p>
                <a href="http://www.sizzlinghotmarriage.com/shm/positions.asp">POSITIONS AVAILABLE</a><br />
            </p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

