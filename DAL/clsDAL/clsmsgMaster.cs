﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;
namespace clsDAL
{
    public class clsmsgMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsmsgMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsmsgMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        //public Int32 PrimeryKey
        //{
        //    get { return _Mid; }
        //    set
        //    {
        //        _Mid = value;
        //        if (clsCMD.Parameters.Contains("@Mid"))
        //        { clsCMD.Parameters["@Mid"].Value = value; }
        //        else { clsCMD.Parameters.Add("@Mid", SqlDbType.Int).Value = value; }
        //    }
        //}
        #endregion

        #region Declare Class's Field Based Properties
        //private Int32 _Mid;
        //public Int32 Mid
        //{
        //    get { return _Mid; }
        //    set
        //    {
        //        _Mid = value;
        //        if (clsCMD.Parameters.Contains("@Mid"))
        //        { clsCMD.Parameters["@Mid"].Value = value; }
        //        else { clsCMD.Parameters.Add("@Mid", SqlDbType.Int).Value = value; }
        //    }
        //}
        private String _Mtext;
        public String Mtext
        {
            get { return _Mtext; }
            set
            {
                _Mtext = value;
                if (clsCMD.Parameters.Contains("@Mtext"))
                { clsCMD.Parameters["@Mtext"].Value = value; }
                else { clsCMD.Parameters.Add("@Mtext", SqlDbType.NVarChar).Value = value; }
            }
        }
        private Guid _Msender;
        public Guid Msender
        {
            get { return _Msender; }
            set
            {
                _Msender = value;
                if (clsCMD.Parameters.Contains("@Msender"))
                { clsCMD.Parameters["@Msender"].Value = value; }
                else { clsCMD.Parameters.Add("@Msender", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private DateTime _Mdatetime;
        public DateTime Mdatetime
        {
            get { return _Mdatetime; }
            set
            {
                _Mdatetime = value;
                if (clsCMD.Parameters.Contains("@Mdatetime"))
                { clsCMD.Parameters["@Mdatetime"].Value = value; }
                else { clsCMD.Parameters.Add("@Mdatetime", SqlDbType.DateTime).Value = value; }
            }
        }
        public List<Guid> ReciverIds { get; set; }
        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                //Mid = Convert.ToInt32(dr["Mid"]);
                Mtext = Convert.ToString(dr["Mtext"]);
                Msender = new Guid(dr["Msender"].ToString());
                Mdatetime = Convert.ToDateTime(dr["Mdatetime"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            //Mid = Convert.ToInt32("0");
            Mtext = Convert.ToString("NA");
            Msender = new Guid();
            Mdatetime = Convert.ToDateTime(DateTime.Now);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            //this.Mid = 0;
            //this.AddEditDeleteFlag = "ADD";
            try
            {

                clsCMD.CommandText = "sp_tbl_msgMasterAddEditDelete";
                clsCMD.Parameters.AddWithValue("@Mid", SqlDbType.Int);
                clsCMD.Parameters["@Mid"].Direction = ParameterDirection.Output;
                clsCMD.ExecuteNonQuery();
                int msgId = Convert.ToInt32(clsCMD.Parameters["@Mid"].Value);
                if (msgId > 0)
                {

                    clsCMD.CommandText = "sp_tbl_msgRecAddEditDelete";
                    foreach (Guid items in ReciverIds)
                    {
                        clsCMD.Parameters.Clear();
                        clsCMD.Parameters.AddWithValue("@M_id", msgId);
                        clsCMD.Parameters.AddWithValue("@Mreceiver", items.ToString());
                        clsCMD.Parameters.AddWithValue("@ReadStatus", false);
                        clsCMD.ExecuteNonQuery();
                    }
                    blnResult = true; GetSetErrorMessage = "Entry Saved Successfully.";
                }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int32 Mid)
        {
            Boolean blnResult = false;
            //this.Mid = Mid;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_tbl_msgMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int32 Mid)
        {
            Boolean blnResult = false;
            //this.Mid = Mid;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_tbl_msgMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_tbl_msgMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsmsgMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsmsgMaster_PropertiesList> lstResult = new List<clsmsgMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].Mid = Convert.ToInt32(dr["Mid"]);
                lstResult[i].Mtext = Convert.ToString(dr["Mtext"]);
                lstResult[i].Msender = new Guid(dr["Msender"].ToString());
                lstResult[i].Mdatetime = Convert.ToDateTime(dr["Mdatetime"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int32 Mid)
        {
            Boolean blnResult = false;
            try
            {
                //this.Mid = Mid;
                clsCMD.CommandText = "sp_tbl_msgMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int32 Mid)
        {
            Boolean blnResult = false;
            try
            {
                //this.Mid = Mid;
                clsCMD.CommandText = "sp_tbl_msgMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion


        #region Methods for Message Receiver

        public DataTable GetMessageByGUID(Guid recId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.Parameters.Clear();
                clsCMD.Parameters.AddWithValue("@Mreceiver", recId);
                clsCMD.CommandText = "[sp_tbl_msgRecGetData]";
                SetGetSPFlag = "BYUSERGUID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        //COUNT TOTAL MSGS AND THEIR SENDER
        public DataTable countMsg(Guid recId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.Parameters.Clear();
                clsCMD.Parameters.AddWithValue("@Mreceiver", recId);
                clsCMD.CommandText = "[sp_tbl_msgRecGetData]";
                SetGetSPFlag = "UNREADCOUNTS";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public void updateStatus(Guid recId)
        {

            try
            {
                clsCMD.Parameters.Clear();
                clsCMD.Parameters.AddWithValue("@Mreceiver", recId);
                clsCMD.CommandText = "[sp_tbl_msgRecAddEditDelete]";
                SetGetSPFlag = "UPDATESTATUS";
                clsCMD.ExecuteNonQuery();
                //SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                //SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;

            }

        }
        #endregion

        public DataTable GetSentByGUID(Guid senderId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.Parameters.Clear();
                clsCMD.Parameters.AddWithValue("@Mdatetime", DateTime.Now);
                clsCMD.Parameters.AddWithValue("@Msender", senderId);
                SetGetSPFlag = "BYGUID";
                clsCMD.CommandText = "sp_tbl_msgMasterGetData";



                //SetGetSPFlag = "BYUSERGUID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }
    }
    public class clsmsgMaster_PropertiesList
    {
        public Int32 Mid { get; set; }
        public String Mtext { get; set; }
        public Guid Msender { get; set; }
        public DateTime Mdatetime { get; set; }
    }
}