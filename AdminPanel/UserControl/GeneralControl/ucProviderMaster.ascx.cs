﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class UserControl_GeneralControl_ucProviderMaster : System.Web.UI.UserControl
{
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    private Guid intId = new Guid();
    private string strName = string.Empty;

    public Guid SelectedValue
    {
        get { intId = new Guid(ddlProvider.SelectedValue.ToString()); return intId; }
        set { intId = value; }
    }

    public string SelectedName
    {
        get { strName = ddlProvider.SelectedItem.Text; return strName; }
        set { strName = value; }
    }

    public int Width
    {
        set { ddlProvider.Width = Unit.Pixel(value); }
    }

    public void Bind()
    {
        ddlProvider.DataSource = clsCPM.GetAllComboByRoleandParent("User", (Guid)Membership.GetUser().ProviderUserKey);
        ddlProvider.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}