﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;
using System.Web.Security;

public partial class Workbook_LessonAddEdit : System.Web.UI.Page
{
    clsLessions clsLSN = new clsLessions();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsMembership mem = new clsMembership();
    clsobjectsharing clsOBS = new clsobjectsharing();
    private void EditEntry()
    {
        Boolean blnResult = clsLSN.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnResult == true)
        {
            txtDescription.Text = clsLSN.lsn_description;
            txtLessonName.Text = clsLSN.lsn_name;
            txtshort.Text = clsLSN.lsn_short;
            ListItem liInterval = ddlSelectMember.Items.FindByValue(clsLSN.lsn_UserId.ToString());
            if (liInterval != null)
            {
                ddlSelectMember.SelectedIndex = ddlSelectMember.Items.IndexOf(liInterval);
            }
            lsncd.Value = clsLSN.lsn_code.ToString();
            lsnsts.Value = clsLSN.lsn_sts.ToString();
        }
        else { Response.Redirect("~/Workbook/ChapterMaster.aspx"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (MySession.Current.AddEditFlag == "null")
            {
                Response.Redirect("~/Workbook/LessonMaster.aspx");
            }
            if (User.IsInRole("administrator"))
            {
                BindCombo();
            }
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                EditEntry();
            }
        }
    }

    private void BindCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllCombo();
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }
    private string PageValidation()
    {
        string strResult = "";
        if (txtLessonName.Text.Trim() == "")
        { strResult += "Please Input Lesson Name <br/>"; return strResult; }
        if (txtshort.Text.Trim() == "")
        { strResult += "Please Input Lesson short <br/>"; return strResult; }
        return strResult;
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Boolean blnSave = false;
        string strResult = PageValidation();
        if (strResult != "")
        { lblmessage.Text = strResult; return; }
        clsLSN.lsn_description = txtDescription.Text;
        clsLSN.lsn_name = txtLessonName.Text;
        clsLSN.lsn_short = txtshort.Text;
        if (User.IsInRole("administrator"))
        {
            clsLSN.lsn_UserId = new Guid(ddlSelectMember.SelectedValue.ToString());
        }
        else
        {
            clsLSN.lsn_UserId = (Guid)Membership.GetUser().ProviderUserKey;
        }
        clsLSN.lsn_OwnerId = mem.SetOwner(clsLSN.lsn_UserId);
        if (MySession.Current.AddEditFlag == "ADD")
        {
            clsLSN.lsn_code = Guid.NewGuid();
            blnSave = clsLSN.Save();
            if (blnSave)
            {
                clsLSN.GetRecordByCodeInProperties(clsLSN.lsn_code);
                clsOBS.shr_sharedobjid = clsLSN.lsn_id;
                clsOBS.shr_object = "LESSON";
                clsOBS.shr_ownerid = clsLSN.lsn_UserId;
                clsOBS.shr_sharedwithid = clsLSN.lsn_UserId;
                blnSave = clsOBS.Save();
            }
        }
        else
        {
            clsLSN.lsn_sts = Convert.ToBoolean(lsnsts.Value);
            clsLSN.lsn_code = new Guid(lsncd.Value);
            blnSave = clsLSN.Update(MySession.Current.EditID);
        }
        if (blnSave == true)
        {
            Response.Redirect("~/Workbook/LessonMaster.aspx");
        }
        else
        {
            lblmessage.Text = "Error in save ..." + clsLSN.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Workbook/LessonMaster.aspx");
    }
}