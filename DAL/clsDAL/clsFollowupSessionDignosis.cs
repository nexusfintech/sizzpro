﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsFollowupSessionDignosis
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsFollowupSessionDignosis()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsFollowupSessionDignosis()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _fsd_id; }
            set
            {
                _fsd_id = value;
                if (clsCMD.Parameters.Contains("@fsd_id"))
                { clsCMD.Parameters["@fsd_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fsd_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _fsd_id;
        public Int64 fsd_id
        {
            get { return _fsd_id; }
            set
            {
                _fsd_id = value;
                if (clsCMD.Parameters.Contains("@fsd_id"))
                { clsCMD.Parameters["@fsd_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fsd_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _fsd_sessionid;
        public Guid fsd_sessionid
        {
            get { return _fsd_sessionid; }
            set
            {
                _fsd_sessionid = value;
                if (clsCMD.Parameters.Contains("@fsd_sessionid"))
                { clsCMD.Parameters["@fsd_sessionid"].Value = value; }
                else { clsCMD.Parameters.Add("@fsd_sessionid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _fsd_cptcode;
        public Int64 fsd_cptcode
        {
            get { return _fsd_cptcode; }
            set
            {
                _fsd_cptcode = value;
                if (clsCMD.Parameters.Contains("@fsd_cptcode"))
                { clsCMD.Parameters["@fsd_cptcode"].Value = value; }
                else { clsCMD.Parameters.Add("@fsd_cptcode", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _fsd_dignosiscode;
        public Int64 fsd_dignosiscode
        {
            get { return _fsd_dignosiscode; }
            set
            {
                _fsd_dignosiscode = value;
                if (clsCMD.Parameters.Contains("@fsd_dignosiscode"))
                { clsCMD.Parameters["@fsd_dignosiscode"].Value = value; }
                else { clsCMD.Parameters.Add("@fsd_dignosiscode", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _fsd_procedure;
        public Int64 fsd_procedure
        {
            get { return _fsd_procedure; }
            set
            {
                _fsd_procedure = value;
                if (clsCMD.Parameters.Contains("@fsd_procedure"))
                { clsCMD.Parameters["@fsd_procedure"].Value = value; }
                else { clsCMD.Parameters.Add("@fsd_procedure", SqlDbType.BigInt).Value = value; }
            }
        }
        private Double _fsd_charge;
        public Double fsd_charge
        {
            get { return _fsd_charge; }
            set
            {
                _fsd_charge = value;
                if (clsCMD.Parameters.Contains("@fsd_charge"))
                { clsCMD.Parameters["@fsd_charge"].Value = value; }
                else { clsCMD.Parameters.Add("@fsd_charge", SqlDbType.Money).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                fsd_id = Convert.ToInt64(dr["fsd_id"]);
                fsd_sessionid = new Guid(dr["fsd_sessionid"].ToString());
                fsd_cptcode = Convert.ToInt64(dr["fsd_cptcode"]);
                fsd_dignosiscode = Convert.ToInt64(dr["fsd_dignosiscode"]);
                fsd_procedure = Convert.ToInt64(dr["fsd_procedure"]);
                fsd_charge = Convert.ToDouble(dr["fsd_charge"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            fsd_id = Convert.ToInt64("0");
            fsd_sessionid = new Guid();
            fsd_cptcode = Convert.ToInt64("0");
            fsd_dignosiscode = Convert.ToInt64("0");
            fsd_procedure = Convert.ToInt64("0");
            fsd_charge = Convert.ToDouble("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.fsd_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_FollowupSessionDignosisAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 fsd_id)
        {
            Boolean blnResult = false;
            this.fsd_id = fsd_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_FollowupSessionDignosisAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 fsd_id)
        {
            Boolean blnResult = false;
            this.fsd_id = fsd_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_FollowupSessionDignosisAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_FollowupSessionDignosisGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordBySession(Guid sessionid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.fsd_sessionid = sessionid;
                clsCMD.CommandText = "sp_FollowupSessionDignosisGetData";
                SetGetSPFlag = "BYSESSION";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }
        public List<clsFollowupSessionDignosis_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsFollowupSessionDignosis_PropertiesList> lstResult = new List<clsFollowupSessionDignosis_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].fsd_id = Convert.ToInt64(dr["fsd_id"]);
                lstResult[i].fsd_sessionid = new Guid(dr["fsd_sessionid"].ToString());
                lstResult[i].fsd_cptcode = Convert.ToInt64(dr["fsd_cptcode"]);
                lstResult[i].fsd_dignosiscode = Convert.ToInt64(dr["fsd_dignosiscode"]);
                lstResult[i].fsd_procedure = Convert.ToInt64(dr["fsd_procedure"]);
                lstResult[i].fsd_charge = Convert.ToDouble(dr["fsd_charge"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 fsd_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fsd_id = fsd_id;
                clsCMD.CommandText = "sp_FollowupSessionDignosisGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 fsd_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fsd_id = fsd_id;
                clsCMD.CommandText = "sp_FollowupSessionDignosisGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsFollowupSessionDignosis_PropertiesList
    {
        public Int64 fsd_id { get; set; }
        public Guid fsd_sessionid { get; set; }
        public Int64 fsd_cptcode { get; set; }
        public Int64 fsd_dignosiscode { get; set; }
        public Int64 fsd_procedure { get; set; }
        public Double fsd_charge { get; set; }
    }
}