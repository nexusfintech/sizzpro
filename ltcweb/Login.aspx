﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-user"></i>&nbsp;Login</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div style="margin: 0 auto; width: 50%;">
                <div class="login-body">
                    <h2><i class="icon-key"></i>&nbsp;SIGN IN</h2>
                    <div class='form-validate'>
                        <div class="control-group">
                            <div class="txtUsername">
                                <asp:TextBox ID="txtUsername" runat="server" placeholder="Email address" class='input-block-level' data-rule-required="true" data-rule-email="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="txtPassword">
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="Password" class='input-block-level' data-rule-required="true"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <h5>
                                <asp:Label ID="lblMessage_page" runat="server" ForeColor="Red" Text=""></asp:Label></h5>
                        </div>
                        <div class="submit" align="right">
                            <asp:Button ID="cmdLogin" runat="server" Text="Login" CssClass="btn btn-primary" OnClick="cmdLogin_Click" />
                        </div>
                    </div>
                    <div class="forget">
                        <a href="ForgotPassword.aspx?action=reset"><span>Forgot password?</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

