﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserMessage.aspx.cs" Inherits="UserMessage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12" id="content_message" runat="server" visible="false">
            <div class="box">
                <div class="box-title">
                    <h3>
                        <i class="icon-user"></i>
                        User Registration Message</h3>
                </div>
                <div class="box-content nopadding">
                    <h5 style="text-align: center">
                        <asp:Label ID="lblpagemessage" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </h5>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div id="content_successfull" class="span12" runat="server" visible="false">
                <h2>User Registration Successfull.</h2>
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-user"></i>User Registration Complete</h3>
                    </div>
                    <div class="box-content nopadding" style="text-align: center">
                        <p>
                            Welcome to our portal, &nbsp;
                                        <asp:Label ID="lblusername" runat="server" Text="{username}"></asp:Label>
                        </p>
                        <br />
                        <p>your registration process complete successfully. your account detail is sent in your email account to registred email account.</p>
                        <h3>Registred Email Account :  
                                        <small>
                                            <asp:Label ID="lblEmailAccount" runat="server" Text="{registredmailaccount}"></asp:Label>
                                        </small>
                        </h3>
                        <h3>User Name :
                                        <small>
                                            <asp:Label ID="lblusernamedisp" runat="server" Text="{username}"></asp:Label>
                                        </small>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div id="content_activation" class="span12" runat="server" visible="false">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-smile"></i>Email Verification</h3>
                    </div>
                    <div class="box-content" style="text-align: left; font-weight: bold">
                        <p>
                            Congretulations , &nbsp;
                                        <asp:Label ID="lblusernameactivation" runat="server" Text="{username}"></asp:Label>
                        </p>
                        <p>
                            User Email address Verification process is completed. now you can login with your credential of sent with welcome mail.
                        </p>
                        <h5>
                            <div class="span6">Registred Email Account :  
                                        <small>
                                            <asp:Label ID="lblemailactivation" runat="server" Text="{registredmailaccount}"></asp:Label>
                                        </small></div>
                        <div class="span6">
                            User Name :
                                        <small>
                                            <asp:Label ID="lblusernameactivation1" runat="server" Text="{username}"></asp:Label>
                                        </small></div>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div id="content_activationfail" class="span12" runat="server" visible="false">
                <h2 style="color: red;">Email Account Verification is Incomplete.</h2>
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-search"></i>Email Varification Incomplete</h3>
                    </div>
                    <div class="box-content">
                        <p>
                            your email account varification is not complete please go to your registred mail account inbox and check our welcome mail and open activation link.
                                        please copy and past to your browser and activate your account. after then u can login successfully.                                         
                        </p>
                        <h3>Registred Email Account :  
                                        <small>
                                            <asp:Label ID="lblemailinactive" runat="server" Text="{registredmailaccount}"></asp:Label>
                                        </small>
                        </h3>
                        <h3>User Name :
                                        <small>
                                            <asp:Label ID="lblusernameinactive" runat="server" Text="{username}"></asp:Label>
                                        </small>
                        </h3>
                    </div>
                    <div>
                        <h2 style="color: red;">Resend Verification Mail.</h2>
                        <div class="box box-bordered box-color">
                            <div class="box-title">
                                <h3><i class="icon-mail-forward"></i>Resend Email</h3>
                            </div>
                            <div class="box-content">
                                <h4>
                                    <asp:Label ID="lbllinkgen" runat="server" Text=""></asp:Label>
                                </h4>
                                <p>if you not received email verification email so please click below resend button.</p>
                                <asp:Button ID="cmdresend" runat="server" CssClass="btn" Text="Resend Email" OnClick="cmdresend_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div id="content_linkexpired" class="span12" runat="server" visible="false">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-mail-forward"></i>Resend Email</h3>
                    </div>
                    <div class="box-content">
                        <h4>
                            <asp:Label ID="lbllinkgen1" runat="server" Text=""></asp:Label>
                        </h4>
                        <p>if you not received email verification email so please click below resend button.</p>
                        <asp:Button ID="cmdExpiredResendLink" runat="server" CssClass="btn" Text="Resend Email" OnClick="cmdExpiredResendLink_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

