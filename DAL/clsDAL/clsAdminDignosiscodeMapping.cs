﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsAdminDignosiscodeMapping
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsAdminDignosiscodeMapping()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsAdminDignosiscodeMapping()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _ucm_id; }
            set
            {
                _ucm_id = value;
                if (clsCMD.Parameters.Contains("@ucm_id"))
                { clsCMD.Parameters["@ucm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ucm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _ucm_id;
        public Int64 ucm_id
        {
            get { return _ucm_id; }
            set
            {
                _ucm_id = value;
                if (clsCMD.Parameters.Contains("@ucm_id"))
                { clsCMD.Parameters["@ucm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ucm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _ucm_userid;
        public Guid ucm_userid
        {
            get { return _ucm_userid; }
            set
            {
                _ucm_userid = value;
                if (clsCMD.Parameters.Contains("@ucm_userid"))
                { clsCMD.Parameters["@ucm_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@ucm_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _ucm_codeid;
        public Int64 ucm_codeid
        {
            get { return _ucm_codeid; }
            set
            {
                _ucm_codeid = value;
                if (clsCMD.Parameters.Contains("@ucm_codeid"))
                { clsCMD.Parameters["@ucm_codeid"].Value = value; }
                else { clsCMD.Parameters.Add("@ucm_codeid", SqlDbType.BigInt).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                ucm_id = Convert.ToInt64(dr["ucm_id"]);
                ucm_userid = new Guid(dr["ucm_userid"].ToString());
                ucm_codeid = Convert.ToInt64(dr["ucm_codeid"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            ucm_id = Convert.ToInt64("0");
            ucm_userid = new Guid();
            ucm_codeid = Convert.ToInt64("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.ucm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_AdminDignosiscodeMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 ucm_id)
        {
            Boolean blnResult = false;
            this.ucm_id = ucm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_AdminDignosiscodeMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 ucm_id)
        {
            Boolean blnResult = false;
            this.ucm_id = ucm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_AdminDignosiscodeMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_AdminDignosiscodeMappingGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByAdmin(Guid adminid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.ucm_userid = adminid;
                clsCMD.CommandText = "sp_AdminDignosiscodeMappingGetData";
                SetGetSPFlag = "ALLBYADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordForDrop()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_AdminDignosiscodeMappingGetData";
                SetGetSPFlag = "FORDRP";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsAdminDignosiscodeMapping_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsAdminDignosiscodeMapping_PropertiesList> lstResult = new List<clsAdminDignosiscodeMapping_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].ucm_id = Convert.ToInt64(dr["ucm_id"]);
                lstResult[i].ucm_userid = new Guid(dr["ucm_userid"].ToString());
                lstResult[i].ucm_codeid = Convert.ToInt64(dr["ucm_codeid"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 ucm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.ucm_id = ucm_id;
                clsCMD.CommandText = "sp_AdminDignosiscodeMappingGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 ucm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.ucm_id = ucm_id;
                clsCMD.CommandText = "sp_AdminDignosiscodeMappingGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsAdminDignosiscodeMapping_PropertiesList
    {
        public Int64 ucm_id { get; set; }
        public Guid ucm_userid { get; set; }
        public Int64 ucm_codeid { get; set; }
    }
}