﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_BillingProviderMaster : System.Web.UI.Page
{
    clsBillingproviderMaster clsBPM = new clsBillingproviderMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptBp.DataSource = clsBPM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        rptBp.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Master/BillingProviderAddEdit.aspx");
    }
    protected void rptBp_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/BillingProviderAddEdit.aspx");
        }
        if (e.CommandName == "DELETE")
        {
            Boolean blnRslt = clsBPM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (blnRslt)
            {
                MySession.Current.MESSAGE = "Record Deleted Successfully";
                Response.Redirect("~/Master/BillingProviderMaster.aspx");
            }
            else
            {
                lblmessage.Text = clsBPM.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
}