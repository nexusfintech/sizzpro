﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsPayerMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsPayerMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsPayerMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _pm_id; }
            set
            {
                _pm_id = value;
                if (clsCMD.Parameters.Contains("@pm_id"))
                { clsCMD.Parameters["@pm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _pm_id;
        public Int64 pm_id
        {
            get { return _pm_id; }
            set
            {
                _pm_id = value;
                if (clsCMD.Parameters.Contains("@pm_id"))
                { clsCMD.Parameters["@pm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _pm_Payerid;
        public String pm_Payerid
        {
            get { return _pm_Payerid; }
            set
            {
                _pm_Payerid = value;
                if (clsCMD.Parameters.Contains("@pm_Payerid"))
                { clsCMD.Parameters["@pm_Payerid"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_Payerid", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _pm_PayerName;
        public String pm_PayerName
        {
            get { return _pm_PayerName; }
            set
            {
                _pm_PayerName = value;
                if (clsCMD.Parameters.Contains("@pm_PayerName"))
                { clsCMD.Parameters["@pm_PayerName"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_PayerName", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private Boolean _pm_ENR;
        public Boolean pm_ENR
        {
            get { return _pm_ENR; }
            set
            {
                _pm_ENR = value;
                if (clsCMD.Parameters.Contains("@pm_ENR"))
                { clsCMD.Parameters["@pm_ENR"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_ENR", SqlDbType.Bit).Value = value; }
            }
        }
        private String _pm_TYP;
        public String pm_TYP
        {
            get { return _pm_TYP; }
            set
            {
                _pm_TYP = value;
                if (clsCMD.Parameters.Contains("@pm_TYP"))
                { clsCMD.Parameters["@pm_TYP"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_TYP", SqlDbType.NVarChar,50).Value = value; }
            }
        }
        private Int64 _pm_state;
        public Int64 pm_state
        {
            get { return _pm_state; }
            set
            {
                _pm_state = value;
                if (clsCMD.Parameters.Contains("@pm_state"))
                { clsCMD.Parameters["@pm_state"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_state", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _pm_LOB;
        public String pm_LOB
        {
            get { return _pm_LOB; }
            set
            {
                _pm_LOB = value;
                if (clsCMD.Parameters.Contains("@pm_LOB"))
                { clsCMD.Parameters["@pm_LOB"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_LOB", SqlDbType.NVarChar,50).Value = value; }
            }
        }
        private Boolean _pm_RTE1;
        public Boolean pm_RTE1
        {
            get { return _pm_RTE1; }
            set
            {
                _pm_RTE1 = value;
                if (clsCMD.Parameters.Contains("@pm_RTE1"))
                { clsCMD.Parameters["@pm_RTE1"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_RTE1", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _pm_RTE2;
        public Boolean pm_RTE2
        {
            get { return _pm_RTE2; }
            set
            {
                _pm_RTE2 = value;
                if (clsCMD.Parameters.Contains("@pm_RTE2"))
                { clsCMD.Parameters["@pm_RTE2"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_RTE2", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _pm_RTS;
        public Boolean pm_RTS
        {
            get { return _pm_RTS; }
            set
            {
                _pm_RTS = value;
                if (clsCMD.Parameters.Contains("@pm_RTS"))
                { clsCMD.Parameters["@pm_RTS"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_RTS", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _pm_ERA;
        public Boolean pm_ERA
        {
            get { return _pm_ERA; }
            set
            {
                _pm_ERA = value;
                if (clsCMD.Parameters.Contains("@pm_ERA"))
                { clsCMD.Parameters["@pm_ERA"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_ERA", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _pm_SEC;
        public Boolean pm_SEC
        {
            get { return _pm_SEC; }
            set
            {
                _pm_SEC = value;
                if (clsCMD.Parameters.Contains("@pm_SEC"))
                { clsCMD.Parameters["@pm_SEC"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_SEC", SqlDbType.Bit).Value = value; }
            }
        }
        private String _pm_note;
        public String pm_note
        {
            get { return _pm_note; }
            set
            {
                _pm_note = value;
                if (clsCMD.Parameters.Contains("@pm_note"))
                { clsCMD.Parameters["@pm_note"].Value = value; }
                else { clsCMD.Parameters.Add("@pm_note", SqlDbType.NVarChar).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                pm_id = Convert.ToInt64(dr["pm_id"]);
                pm_Payerid = Convert.ToString(dr["pm_Payerid"]);
                pm_PayerName = Convert.ToString(dr["pm_PayerName"]);
                pm_ENR = Convert.ToBoolean(dr["pm_ENR"]);
                pm_TYP = dr["pm_TYP"].ToString();
                pm_state = Convert.ToInt64(dr["pm_state"]);
                pm_LOB = dr["pm_LOB"].ToString();
                pm_RTE1 = Convert.ToBoolean(dr["pm_RTE1"]);
                pm_RTE2 = Convert.ToBoolean(dr["pm_RTE2"]);
                pm_RTS = Convert.ToBoolean(dr["pm_RTS"]);
                pm_ERA = Convert.ToBoolean(dr["pm_ERA"]);
                pm_SEC = Convert.ToBoolean(dr["pm_SEC"]);
                pm_note = Convert.ToString(dr["pm_note"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            pm_id = Convert.ToInt64("0");
            pm_Payerid = Convert.ToString("NA");
            pm_PayerName = Convert.ToString("NA");
            pm_ENR = Convert.ToBoolean(false);
            pm_TYP = Convert.ToString("NA");
            pm_state = Convert.ToInt64("0");
            pm_LOB = Convert.ToString("NA");
            pm_RTE1 = Convert.ToBoolean(false);
            pm_RTE2 = Convert.ToBoolean(false);
            pm_RTS = Convert.ToBoolean(false);
            pm_ERA = Convert.ToBoolean(false);
            pm_SEC = Convert.ToBoolean(false);
            pm_note = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.pm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_PayerMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 pm_id)
        {
            Boolean blnResult = false;
            this.pm_id = pm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_PayerMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 pm_id)
        {
            Boolean blnResult = false;
            this.pm_id = pm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_PayerMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_PayerMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsPayerMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsPayerMaster_PropertiesList> lstResult = new List<clsPayerMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].pm_id = Convert.ToInt64(dr["pm_id"]);
                lstResult[i].pm_Payerid = Convert.ToString(dr["pm_Payerid"]);
                lstResult[i].pm_PayerName = Convert.ToString(dr["pm_PayerName"]);
                lstResult[i].pm_ENR = Convert.ToBoolean(dr["pm_ENR"]);
                lstResult[i].pm_TYP = Convert.ToBoolean(dr["pm_TYP"]);
                lstResult[i].pm_state = Convert.ToInt64(dr["pm_state"]);
                lstResult[i].pm_LOB = Convert.ToBoolean(dr["pm_LOB"]);
                lstResult[i].pm_RTE1 = Convert.ToBoolean(dr["pm_RTE1"]);
                lstResult[i].pm_RTE2 = Convert.ToBoolean(dr["pm_RTE2"]);
                lstResult[i].pm_RTS = Convert.ToBoolean(dr["pm_RTS"]);
                lstResult[i].pm_ERA = Convert.ToBoolean(dr["pm_ERA"]);
                lstResult[i].pm_SEC = Convert.ToBoolean(dr["pm_SEC"]);
                lstResult[i].pm_note = Convert.ToString(dr["pm_note"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 pm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.pm_id = pm_id;
                clsCMD.CommandText = "sp_PayerMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 pm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.pm_id = pm_id;
                clsCMD.CommandText = "sp_PayerMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsPayerMaster_PropertiesList
    {
        public Int64 pm_id { get; set; }
        public String pm_Payerid { get; set; }
        public String pm_PayerName { get; set; }
        public Boolean pm_ENR { get; set; }
        public Boolean pm_TYP { get; set; }
        public Int64 pm_state { get; set; }
        public Boolean pm_LOB { get; set; }
        public Boolean pm_RTE1 { get; set; }
        public Boolean pm_RTE2 { get; set; }
        public Boolean pm_RTS { get; set; }
        public Boolean pm_ERA { get; set; }
        public Boolean pm_SEC { get; set; }
        public String pm_note { get; set; }
    }
}