﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Spousesandsignificant.aspx.cs" Inherits="Users_Spousesandsignificant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cpMainContent" runat="Server">
    <asp:HiddenField ID="AssesmentStep" runat="server" Value="6" />
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-group"></i>
                Client's Spouses & Signicant
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:SpouseSingficant ID="ucSpouseSignicant" runat="server" />
        </div>
    </div>
      <div class="row-fluid">
        <hr />
        <div class="span12">
            <asp:Button ID="cmdNext" runat="server" Text="Next Childrens >>" CssClass="btn btn-primary" OnClick="cmdNext_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

