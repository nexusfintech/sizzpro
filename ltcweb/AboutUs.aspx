﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-group"></i> About Us</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <p>
                Lifestyle Therapy & Coaching offers Individuals, Families, Couples and Groups Positive Solutions for Life's Challenges. We all have challenges that sometimes are very difficult to get through. A word of encouragement or wise advice can make a world of difference in your life. Nobody knows everything! So it is important that we seek out the advice of others who have the wisdom and knowledge we need to make decisions. Hopefully you can use our resources made available to you on this site as a source of insight, understanding, guidance, encouragement, ideas, intelligence, etc. to brighten your path toward the life you want.Marriage, family and individual counseling opens the door to a better life and better relationships. Research indicates there are numerous benefits in seeking professional assistance when dealing with many common issues faced by families and individuals.
            </p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <p><b>Our Plan</b> is to Offer Positive Solutions for Life's Challenges Helping You Get Where You Want To Be.</p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <p><b>Our Purpose</b> is to allow God to use our education, talents and abilities to help support and maintain strong marriages and families.</p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <p><b>Our Premise</b> is to provide all clients with effective counseling and coaching services.</p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <p><b>Our Promise</b> is to: </p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <ul>
                <li>Utilize the systems model along with other treatment theories and methodologies to provide effective treatment</li>
                <li>Maintain strict confidentiality for all clients.</li>
                <li>Respect the wishes of all clients.</li>
                <li>Work alongside church ministries to provide Christian counseling services.</li>
                <li>Help maintain, restore, and support local area families.</li>
            </ul>
            <p>&nbsp;</p>
            <p>
                <strong>Our professional therapy services are available in several ways. We offer:</strong>
            </p>
            <ul>
                <li>Traditional in-office counseling for individuals, families, couples and small groups.</li>
                <li>In home counseling (special request only)</li>
                <li>Telephone Counseling</li>
                <li>Email Counseling</li>
                <li>Video Conferencing</li>
                <li>Seminars in protecting and strengthening marriage</li>
                <li>Online training modules</li>
                <li>Mental health screenings</li>
                <li>Psychometric testing</li>
                <li>Coaching services and Coaching Certification</li>
            </ul>
            <p>&nbsp;</p>
            <p>
                <strong>We provide therapy, coaching and support for many different problems. Some of our specialties include:</strong>
            </p>
            <ul>
                <li>Marriage</li>
                <li>Parenting</li>
                <li>Relationships</li>
                <li>Depression</li>
                <li>Anxiety</li>
                <li>Anger</li>
                <li>Communication</li>
                <li>Premarital</li>
            </ul>
            <p>&nbsp;</p>
            <p>
                <strong>We offer seminars and educational modules. Our upcoming offerings include:</strong>
            </p>
            <ul>
                <li><a href="http://www.sizzlinghotmarriage.com" target="_blank">Sizzling Hot Marriage Series</a></li>
            </ul>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

