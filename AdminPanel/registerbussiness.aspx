﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSite.master" AutoEventWireup="true" CodeFile="registerbussiness.aspx.cs" Inherits="registerbussiness" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
     <div class="row-fluid" style="border: 1px solid #0072D6;">
        <div class="span12">
            <%--<asp:Literal ID="ltrPageBody" runat="server"></asp:Literal>--%>
            <h4 style="text-align: center">Register your business with Sizzpro</h4>
            <hr />
            <div class="form-horizontal form-bordered">
                <div class="control-group">
                    <label for="txtusername" class="control-label">FirstName</label>
                    <div class="controls">
                        <asp:TextBox ID="txtfirstname" runat="server" CssClass="input-large" placeholder="first name"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtusername" class="control-label">LastName</label>
                    <div class="controls">
                        <asp:TextBox ID="txtlastname" runat="server" CssClass="input-large" placeholder="last name"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtusername" class="control-label">Email (UserName)</label>
                    <div class="controls">
                        <asp:TextBox ID="txtusername" runat="server" CssClass="input-large" placeholder="first name"></asp:TextBox>
                        <span class="help-block">your email address will be used as your username</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtpassword" class="control-label">Password </label>
                    <div class="controls">
                        <asp:TextBox ID="txtpassword" runat="server" CssClass="input-large" TextMode="Password" placeholder="your password"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtpassword" class="control-label">Re-Password</label>
                    <div class="controls">
                        <asp:TextBox ID="txtrepass" runat="server" CssClass="input-large" TextMode="Password" placeholder="re-enter password"></asp:TextBox>
                    </div>
                </div>
                <div class="form-actions">
                    <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Register With SizzPro" OnClick="cmdSave_Click" />
                    <%--<asp:Button ID="cmdCancel" runat="server" CssClass="btn" Text="Return To Cart" OnClick="cmdCancel_Click" />--%>
                    &nbsp;
                    <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

