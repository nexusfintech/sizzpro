﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="informedConsentDetails.aspx.cs" Inherits="informedConsentDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <asp:Panel runat="server" ID="InformConcentData">
        <div class="row-fluid">
            <h4>Please fill out following aditional details which we will use to complete your inform concent</h4>
            <hr />
            <p><strong>CREDIT CARD DETAIL</strong></p>
            <p>We requires you to keep a credit card on file. This will help us collect co-payments, deductibles and anyunpaid balances. This will also help us collect any deductibles that may be added towards your treatment. (It’s best to know yourbenefits before coming). It is also your responsibility to make sure your credit card does not expire and continues to stay updated.</p>
            <br />
            <div class='form-horizontal'>
                <div class="control-group">
                    <label for="txtFirstName" class="control-label"><strong>Credit Card Type : </strong></label>
                    <div class="controls">
                        <asp:DropDownList ID="creditcardtype" runat="server">
                            <asp:ListItem> select card type  </asp:ListItem>
                            <asp:ListItem Text="Visa" Value="Visa"></asp:ListItem>
                            <asp:ListItem Text="Master Card" Value="Master Card"></asp:ListItem>
                            <asp:ListItem Text="Discover" Value="Discover"></asp:ListItem>
                            <asp:ListItem Text="American Express" Value="American Express"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtPhoneNo" class="control-label"><strong>Name as it appears on Card :</strong></label>
                    <div class="controls">
                        <asp:TextBox ID="nameoncreditcard" runat="server" placeholder="card holder name"></asp:TextBox>
                    </div>
                </div>

                <div class="control-group">
                    <label for="txtPhoneNo" class="control-label"><strong>Credit Card number:</strong></label>
                    <div class="controls">
                        <asp:TextBox ID="cardnumber" runat="server" placeholder="credit card number"></asp:TextBox>
                    </div>
                </div>

                <div class="control-group">
                    <label for="creditcardexpirationdate" class="control-label"><strong>Expiration Date :</strong></label>
                    <div class="controls">
                        <asp:TextBox ID="creditcardexpirationdate" runat="server" CssClass="datepick"></asp:TextBox>
                    </div>
                </div>

                <div class="control-group">
                    <label for="creditcardsecnumber" class="control-label"><strong>Number: </strong></label>
                    <div class="controls">
                        <asp:TextBox ID="creditcardsecnumber" runat="server" placeholder="Number"></asp:TextBox>
                        (3-4 digits located on the back of the card) 
                    </div>
                </div>
            </div>
            <hr />
            <p><strong>AUTHORIZATION FOR CORRESPONDENCE</strong></p>
            <p>I hereby authorize provider to contact me for purposes of canceling, rescheduling, or verifying appointments, or for any other reason effecting my treatment.</p>
            <div class='form-horizontal'>
                <div class="control-group">
                    <div class="controls">
                        <asp:CheckBoxList ID="corspondenceby" runat="server" CssClass="checkbox">
                            <asp:ListItem Value="1">send mail to my home</asp:ListItem>
                            <asp:ListItem Value="2">call me on my cell phone/leave a message on my voice mail</asp:ListItem>
                            <asp:ListItem Value="3">call me at home/leave a message with a family member or voice mail</asp:ListItem>
                            <asp:ListItem Value="4">call me at work</asp:ListItem>
                        </asp:CheckBoxList>
                    </div>
                </div>
                <%--  <div class="checkbox">
                <asp:CheckBox ID="CheckBox1" runat="server" />send mail to my home.
            </div>
            <div class="checkbox">
                <asp:CheckBox ID="CheckBox2" runat="server" />call me on my cell phone/leave a message on my voice mail
            </div>
            <div class="checkbox">
                <asp:CheckBox ID="CheckBox3" runat="server" />call me at home/leave a message with a family member or voice mail
            </div>
            <div class="checkbox">
                <asp:CheckBox ID="CheckBox4" runat="server" />call me at work
            </div>--%>
            </div>
            <hr />
            <p><strong>AUTHORIZATION TO RELEASE INFORMATION </strong>(Optional)</p>
            <p>
                <strong>INTEGRATED CARE</strong>- In order to provide Total Health &amp; Wellness we need to communicate with your primary care physician your progress in treatment with us.
            </p>
            <p>
                I give my permission to provider to communicate with follwing physian for the purpose of improving my medical care.
            </p>
            <div class='form-horizontal'>
                <div class="control-group">
                    <label for="txtPhoneNo" class="control-label"><strong>Physian Name:</strong></label>
                    <div class="controls">
                        <asp:TextBox ID="authorisationphysian" runat="server" placeholder="physian name"></asp:TextBox>
                    </div>
                </div>

                <div class="control-group">
                    <label for="txtPhoneNo" class="control-label"><strong>Address:</strong></label>
                    <div class="controls">
                        <asp:TextBox ID="authorizationaddress" runat="server" placeholder="physian address"></asp:TextBox>
                    </div>
                </div>

                <div class="control-group">
                    <label for="txtPhoneNo" class="control-label"><strong>City :</strong></label>
                    <div class="controls">
                        <asp:TextBox ID="authorizationcity" runat="server" placeholder="physian city"></asp:TextBox>
                    </div>
                </div>

                <div class="control-group">
                    <label for="txtPhoneNo" class="control-label"><strong>State: </strong></label>
                    <div class="controls">
                        <asp:TextBox ID="authorizationstate" runat="server" placeholder="physian state"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtPhoneNo" class="control-label"><strong>Zip: </strong></label>
                    <div class="controls">
                        <asp:TextBox ID="authorizationzip" runat="server" placeholder="physian zip"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtPhoneNo" class="control-label"><strong>Phone: </strong></label>
                    <div class="controls">
                        <asp:TextBox ID="authorizationphn" runat="server" placeholder="physian phone"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtPhoneNo" class="control-label"><strong>Fax: </strong></label>
                    <div class="controls">
                        <asp:TextBox ID="authorizationfax" runat="server" placeholder="physian fax"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtPhoneNo" class="control-label"><strong>Authorization Expiration Date :</strong></label>
                    <div class="controls">
                        <asp:TextBox ID="authorizationend" runat="server" placeholder="expiration date" CssClass="datepick"></asp:TextBox>
                    </div>
                </div>
            </div>
            <p>I acknowledge that I have been given a copy of this document. I understand my rights as a client.</p>
            <hr />
            <p>
                I have read the above authorization and understand it. I agree to hold Lifestyle Therapy &amp; Coaching harmless for claims or damages in connection with our work together. This is a contract between myself and Lifestyle Therapy &amp; Coaching and I understand that it is also a release of potential liability.
            </p>
            <p>
                I acknowledge I have discussed the following with my healthcare provider:
            <ol>
                <li>The condition that the treatment is to address;</li>
                <li>The nature of the treatment;</li>
                <li>The risks and benefits of that treatment; and</li>
                <li>Any alternatives to that treatment</li>
            </ol>
            </p>
            <br />
            <p>I have had the opportunity to ask questions and receive answers regarding the treatment.</p>
            <br />
            <p>I consent to the treatments offered or recommended to me by my healthcare provider, including osseous and soft tissue manipulation. I intend this consent to apply to all my present and future care from all professionals at Lifestyle Therapy &amp; Coaching. <strong>If there happens to be any medical emergency while in treatment at Lifestyle, if there is any physical or mental incapacity on my part I give permission for clinicians and staff to act on my behalf.</strong></p>
            <br />
            <p>
                <div class="checkbox">
                    <asp:CheckBox ID="chk" runat="server" /><strong style="color: red">By fillling this form i have read and understood the terms and conditions and set out in this agreement.</strong>
                </div>
            </p>
        </div>
        <div class="row-fluid">
            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save and Continue" OnClick="cmdSave_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;
         <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

