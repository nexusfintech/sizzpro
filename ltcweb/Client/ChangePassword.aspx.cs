﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ChangePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void cmdChnagePass_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtNewPassword.Text == txtConformPAssword.Text)
            {
                Guid objProviderkey = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                MembershipUser mu = Membership.GetUser(objProviderkey);
                string strResetedPassword = mu.ResetPassword("nexus@admin");
                Boolean blnResult = mu.ChangePassword(strResetedPassword, txtNewPassword.Text);
                if (blnResult == true)
                {
                    lblMessage.Text = "Password Changed Successfully.";
                    Boolean blnMail = clsGetTemplate.GetTemplate("PasswordChanged");
                    if (blnMail == true)
                    {
                        MembershipUser tt = Membership.GetUser(Membership.GetUser().UserName);
                        string userEmail = tt.Email;
                        clsGetTemplate.HtmlTemplate = clsGetTemplate.HtmlTemplate.Replace("{username}", tt.UserName);
                        clsGetTemplate.SendMail(userEmail);
                        lblMessage.Text = "Password Changed Successfully.";
                        FormsAuthentication.SignOut();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                    else { lblMessage.Text = clsGetTemplate.HtmlTemplate; }
                }
                else { lblMessage.Text = "Password Not Changed."; }
            }
            else { lblMessage.Text = "Confirm Password Not Matched..."; }
        }
        catch (Exception ex)
        { lblMessage.Text = ex.Message; }
    }
}