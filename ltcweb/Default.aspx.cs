﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using clsDAL;
using System.Text.RegularExpressions;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //MembershipUser mu = Membership.GetUser("administrator");
        //mu.UnlockUser();
        //Membership.UpdateUser(mu);

        //try
        //{
        //    SqlConnection sqlCon = new SqlConnection("data source=mssql1006.c10.hostexcellence.com ;initial catalog=C336506_SizzProdb;integrated security=false;user id=C336506_sizzprouser;password=Sizz123");
        //    if (sqlCon.State == ConnectionState.Closed)
        //    {
        //        sqlCon.Open();
        //        Response.Write("Connection is open");
        //        sqlCon.Close();
        //    }
        //}
        //catch (Exception ex)
        //{
        //    Response.Write(ex.Message);
        //}

    }

    protected void cmdRegister_Click(object sender, EventArgs e)
    {
        string strRegEx = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        if (txtNewsName.Text.Trim() != "" || txtEmailAddress.Text.Trim() != "")
        {
            Match validemail = Regex.Match(txtEmailAddress.Text.Trim(), strRegEx);
            if (!validemail.Success)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert1", "alert('Invalid Email Address Format.');", true);
                return;
            }
            clsNewsLatter clsNewsRegi = new clsNewsLatter();
            clsNewsRegi.nwl_name = txtNewsName.Text.Trim();
            clsNewsRegi.nwl_emailaddress = txtEmailAddress.Text.Trim();
            clsNewsRegi.nwl_datetime = DateTime.Now;
            Boolean blnResult = clsNewsRegi.Save();
            if (blnResult == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert2", "alert('Registration Successfully.');", true);
            }
            else { ScriptManager.RegisterStartupScript(this, GetType(), "showalert3", "alert('Error..: " + clsNewsRegi.GetSetErrorMessage + "');", true); }
        }
        else { ScriptManager.RegisterStartupScript(this, GetType(), "showalert4", "alert('Invalid input Name and Email.');", true); }
    }
}