﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="grief-loss.aspx.cs" Inherits="grief_loss" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-question"></i>&nbsp;GRIEF & LOSS</h2>
        </div>
    </div>
    <hr />
     <div class="row-fluid">
        <div class="span12">
            <img src="img/grif_loss.png" />
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <p align="left">
                <strong>Grief and Loss</strong><br />
                One of the most difficult times in life is when  we lose someone close to us.&nbsp; All of us,  at some time, will experience the grief associated with loss.&nbsp; With this grief comes a host of changes in  our life for which we are often unprepared.&nbsp;  How one copes with this loss will be a determining factor in the quality  of one&rsquo;s future life.&nbsp;
            </p>
            <p align="left">&nbsp;</p>
            <p align="left">Grief is a process of stages that must be fully  traversed before healing can be complete.&nbsp;&nbsp;&nbsp;  This process includes strong  emotions, such as sadness and anger.&nbsp;  Some will have physical reactions, such as not sleeping or even waves of  nausea.&nbsp; Many will experience spiritual  reactions to a death such as questioning their beliefs and feeling disappointed  in their religion while others find that they feel more strongly than ever about  their faith.</p>
            <p align="left">It is widely recognized that  there are five stages of the grieving process and each person&rsquo;s experience will  be unique in the time it takes for him/her to get through these stages.  However, it is important that one grieve properly in order for healing to  transpire.</p>
            <p align="left">&nbsp;</p>
            <p align="left">
                <strong>The  Five Stages of Grief
                    <br />
                </strong>
            </p>
            <p align="left"><a href="http://grief.com/the-five-stages-of-grief/"><strong>by Elizabeth K&uuml;bler-Ross and David  Kessler</strong></a><strong> </strong></p>
            <div align="left">
                <p><strong><em>Denial</em></strong></p>
            </div>
            <p align="left">
                This  first stage of grieving helps us to survive the loss. In this stage, the world  becomes meaningless and overwhelming. Life makes no sense. We are in a state of  shock and denial. We go numb. We wonder how we can go on, if we can go on, why  we should go on. We try to find a way to simply get through each day. Denial  and shock help us to cope and make survival possible. Denial helps us to pace  our feelings of grief. There is a grace in denial. It is nature&rsquo;s way of  letting in only as much as we can handle.<br />
                As you  accept the reality of the loss and start to ask yourself questions, you are  unknowingly beginning the healing process. You are becoming stronger, and the  denial is beginning to fade. But as you proceed, all the feelings you were  denying begin to surface.
            </p>
            <div align="left">
                <p><strong><em>Anger</em></strong></p>
            </div>
            <p align="left">
                Anger  is a necessary stage of the healing process. Be willing to feel your anger,  even though it may seem endless. The more you truly feel it, the more it will  begin to dissipate and the more you will heal. There are many other emotions  under the anger and you will get to them in time, but anger is the emotion we  are most used to managing. The truth is that anger has no limits. It can extend  not only to your friends, the doctors, your family, yourself and your loved one  who died, but also to God. You may ask, &ldquo;Where is God in this?<br />
                Underneath  anger is pain, your pain. It is natural to feel deserted and abandoned, but we  live in a society that fears anger. Anger is strength and it can be an anchor,  giving temporary structure to the nothingness of loss. At first grief feels  like being lost at sea: no connection to anything. Then you get angry at  someone, maybe a person who didn&rsquo;t attend the funeral, maybe a person who isn&rsquo;t  around, maybe a person who is different now that your loved one has died.  Suddenly you have a structure &ndash; - your anger toward them. The anger becomes a  bridge over the open sea, a connection from you to them. It is something to  hold onto; and a connection made from the strength of anger feels better than nothing.  We usually know more about suppressing anger than feeling it. The anger is just  another indication of the intensity of your love.
            </p>
            <div align="left">
                <p><strong><em>Bargaining</em></strong></p>
            </div>
            <p align="left">
                Before  a loss, it seems like you will do anything if only your loved one would be  spared. &ldquo;Please God, &rdquo; you bargain, &ldquo;I will never be angry at my wife again if  you&rsquo;ll just let her live.&rdquo; After a loss, bargaining may take the form of a  temporary truce. &ldquo;What if I devote the rest of my life to helping others. Then  can I wake up and realize this has all been a bad dream?&rdquo;<br />
                We  become lost in a maze of &ldquo;If only&hellip;&rdquo; or &ldquo;What if&hellip;&rdquo; statements. We want life  returned to what is was; we want our loved one restored. We want to go back in  time: find the tumor sooner, recognize the illness more quickly, stop the  accident from happening&hellip;if only, if only, if only. Guilt is often bargaining&rsquo;s  companion. The &ldquo;if onlys&rdquo; cause us to find fault in ourselves and what we  &ldquo;think&rdquo; we could have done differently. We may even bargain with the pain. We  will do anything not to feel the pain of this loss. We remain in the past,  trying to negotiate our way out of the hurt. People often think of the stages as  lasting weeks or months. They forget that the stages are responses to feelings  that can last for minutes or hours as we flip in and out of one and then  another. We do not enter and leave each individual stage in a linear fashion.  We may feel one, then another and back again to the first one.
            </p>
            <div align="left">
                <p><strong><em>Depression</em></strong></p>
            </div>
            <p align="left">
                After  bargaining, our attention moves squarely into the present. Empty feelings  present themselves, and grief enters our lives on a deeper level, deeper than  we ever imagined. This depressive stage feels as though it will last forever.  It&rsquo;s important to understand that this depression is not a
                <img src="/images/people/Individuals/DepressedOlderMan.jpg" width="239" height="230" hspace="20" vspace="0" border="1" align="left">sign of mental  illness. It is the appropriate response to a great loss. We withdraw from life,  left in a fog of intense sadness, wondering, perhaps, if there is any point in  going on alone? Why go on at all? Depression after a loss is too often seen as  unnatural: a state to be fixed, something to snap out of. The first question to  ask yourself is whether or not the situation you&rsquo;re in is actually depressing.  The loss of a loved one is a very depressing situation, and depression is a  normal and appropriate response. To not experience depression after a loved one  dies would be unusual. When a loss fully settles in your soul, the realization  that your loved one didn&rsquo;t get better this time and is not coming back is  understandably depressing. If grief is a process of healing, then depression is  one of the many necessary steps along the way.
            </p>
            <div align="left">
                <p><strong><em>Acceptance</em></strong></p>
            </div>
            <p align="left">
                Acceptance  is often confused with the notion of being &ldquo;all right&rdquo; or &ldquo;OK&rdquo; with what has  happened. This is not the case. Most people don&rsquo;t ever feel OK or all right  about the loss of a loved one. This stage is about accepting the reality that  our loved one is physically gone and recognizing that this new reality is the  permanent reality. We will never like this reality or make it OK, but  eventually we accept it. We learn to live with it. It is the new norm with  which we must learn to live. We must try to live now in a world where our loved  one is missing. In resisting this new norm, at first many people want to  maintain life as it was before a loved one died. In time, through bits and  pieces of acceptance, however, we see that we cannot maintain the past intact.  It has been forever changed and we must readjust. We must learn to reorganize  roles, re-assign them to others or take them on ourselves.<br />
            </p>
            <p align="left">Finding  acceptance may be just having more good days than bad ones. As we begin to live  again and enjoy our life, we often feel that in doing so, we are betraying our  loved one. We can never replace what has been lost, but we can make new connections,  new meaningful relationships, new inter-dependencies. Instead of denying our  feelings, we listen to our needs; we move, we change, we grow, we evolve. We  may start to reach out to others and become involved in their lives. We invest  in our friendships and in our relationship with ourselves. We begin to live  again, but we cannot do so until we have given grief its time.</p>
            <p align="left">
                <strong>We Can  Help</strong><br />
                No matter if your loss is recent, or you are  struggling with a loss that happened in the past, we can help. Our work with  you will address the five stages of grief, determine which stage you are in,  and create a personal plan to help you move past the issues you are  experiencing.&nbsp; Through this process of  healing, we will help you find comfort, closure and hope.
            </p>
            <p align="left">
                <strong>Resources</strong><br />
                <a href="http://grief.com/">Grief.com</a>
                <br />
                <a href="http://www.webmd.com/balance/tc/grief-and-grieving-topic-overview">More  about grief and loss from WebMD.com</a>
                <br />
                <a href="http://www.amazon.com/Those-Who-Grieve-Comfort-Sorrow/dp/1582292175/ref=sr_1_1?s=books&ie=UTF8&qid=1298996459&sr=1-1">Book &ndash;  &ldquo;For Those Who Grieve&rdquo;</a>
            </p>
            <p align="left">
                <strong>Our Services</strong><br />
                We offer counseling for many issues faced  today by individuals, couples, and families.&nbsp;  Some of the services are offer are:
            </p>
            <blockquote>
                <p>
                    Help for<br />
                    Anxiety/Phobia Disorders<br />
                    Depressive and Mood Disorders<br />
                    Sexuality Issues<br />
                    Sexual assault victims (incest &amp; rape)<br />
                    Substance Abuse<br />
                    Grief and Loss<br />
                    Child Behavior<br />
                    Adolescent Issues<br />
                    Parenting<br />
                    We also provide Marriage counseling,  Pre-Marital counseling<br />
                    And more&hellip;
                </p>
            </blockquote>
            <p align="left">
                <strong>Our Philosophy</strong><br />
                We believe everyone deserves to live the most  fulfilling life possible.&nbsp; Unfortunately,  life transitions can create difficulties which prevent individuals and families  from experiencing this kind of fullness and joy.&nbsp; Through our counseling program, we help those  who are suffering identify the cause and begin on the road to healing.&nbsp; We provide compassionate support in helping  our clients pinpoint and utilize the tools needed to weather life&rsquo;s storms, be  they larger or small.
            </p>
            <p align="left">It is our purpose to provide the  best counseling experience possible from a compassionate, non-judgmental, and  unbiased standpoint.&nbsp; We believe we are  called to use our education, talents and abilities to help support and maintain  strong individuals, marriages and families.</p>
            <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

