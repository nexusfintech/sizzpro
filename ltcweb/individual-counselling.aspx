﻿<%@ Page Title="INDIVIDUAL COUNSELING" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="individual-counselling.aspx.cs" Inherits="individual_counselling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-user"></i>&nbsp;INDIVIDUAL COUNSELING</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <img src="img/Indivual-Cousil.png" />
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <p>
                <strong>Individual counseling</strong>&nbsp;provides a one-on-one atmosphere to individuals seeking guidance in their personal lives. The confidential and supportive environment helps people to feel comfortable enough to disclose their problems so that they may obtain the skills necessary to resolve or work through their issues.<br/>
                <br/>
                Individual counseling&nbsp;can be used to help with many issues such as depression, anxiety, self-esteem issues, stress and much more. The&nbsp; counselor&nbsp;will explore the individual&rsquo;s feelings, concerns, and behaviors in order to help them acquire the skills necessary to resolve their personal issues.<br>
                <br/>
                Individual counseling can give a person the tools they need to better their lives, accomplish their goals, and help them better understand themselves by exploring where they&rsquo;ve been, and where they&rsquo;re going.
            </p>
            <h3>Who can benefit from counseling?</h3>
            <p>Just about anyone can benefit. No problem is too big or small. Listed below are just a few examples of some common concerns which bring people into therapy:</p>
            <ul>
                <li>Symptoms of stress, anxiety, and depression</li>
                <li>College adjustment issues such as homesickness, academic problems, roommate conflicts, and long-distance relationships</li>
                <li>Interpersonal difficulties, family problems, romantic relationship concerns, problems with assertiveness, and other issues</li>
                <li>Bereavement and grief related to the loss of a loved one (such as relationship breakups, deaths, parental divorce, or other major losses)</li>
                <li>Questions/confusion about identity, self-image, sexuality, gender, or religious concerns</li>
                <li>Concerns about body image, food, eating, or weight, as well as treatment for eating disorders</li>
                <li>Experience with sexual assault, relationship violence, stalking, abuse, or other trauma</li>
                <li>Anxiety, stress, and panic attacks</li>
                <li>Thoughts of suicide, death, or hurting others</li>
                <li>Behaviors that can be harmful to you, like drug or alcohol abuse or cutting</li>
            </ul>
            <h3>When to Seek Counseling</h3>
            <p>Our counselors are trained to intervene or provide support for a countless number of issues, far too many to list in any comprehensive way. While counseling might be helpful in numerous situations, there are some conditions in which we would strongly encourage you to seek counseling services:</p>
            <ul type="disc">
                <li>You are unhappy on most days or feel a sense of hopelessness</li>
                <li>You worry excessively or are constantly on edge</li>
                <li>You are unable to concentrate on your home, work or school activities</li>
                <li>You are unable to sleep at night or constantly feel tired</li>
                <li>You have experienced a change in your appetite or your weight</li>
                <li>You have experienced a loss (e.g., a relationship breakup, a parent's death)</li>
                <li>You have increased your use of alcohol or other drugs (including cigarettes)</li>
                <li>You feel overwhelmed by what is going on in your life</li>
                <li>You are having thoughts about hurting yourself or someone else</li>
            </ul>
            <h3>How to Get the Most Out of Your Counseling Experience</h3>
            <p><strong>
                <img src="/images/pictures/studentcounseling.jpg" width="296" height="147" hspace="18" vspace="0" border="1" align="left">* Define your goals.</strong>&nbsp;Think about what you would like to get out of counseling. It might be helpful to write a list of events, relationship issues, or feelings that you think are contributing to your distress. Take time before each session to consider your expectations for that session. Self-exploration and change involve hard work, and sometimes painful feelings are stirred up in the process of healing. Counselors are trained to pay close attention to these issues and will probably encourage you to discuss these feelings openly.</p>
            <p><strong>* Be an active participant.&nbsp;</strong>This is your counseling experience, so be as active as you can in deciding how to use the time. Be honest with the counselor and give her or him feedback about how you see the sessions progressing.</p>
            <p><strong>* Be patient with yourself.</strong>&nbsp;Growth takes time, effort, and patience. All of your coping skills, behavior patterns, and self-perceptions have been learned and reinforced over a long period of time, so change can be difficult and slow at times.</p>
            <p><strong>* Ask questions.&nbsp;</strong>Ask questions about the counseling process, any methods used by the counselor, or about any other services at LTC. Your counselor is there to assist you.</p>
            <p><strong>* Follow your counselor's recommendations.</strong>&nbsp;Take the time between sessions to complete any activities suggested by your counselor. Counseling is intended to improve your life in the "real world," so making efforts to try out and practice new behaviors, approaches, or ways of thinking could be a crucial element to the success of your counseling experience.&nbsp;</p>
            <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

