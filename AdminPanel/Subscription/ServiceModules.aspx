﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ServiceModules.aspx.cs" Inherits="Subscription_ServiceModules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkMappingtoWB" runat="server" NavigateUrl="~/Subscription/ServicesMaster.aspx"><i class="icon-asterisk"></i>&nbsp;Back To Packages</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span6">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Package & Modules
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="dtServModules" runat="server">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Package Name</th>
                                        <th>Module Name</th>
                                        <th style="text-align: center;">Remove Module</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("srvc_servicename") %></td>
                                <td><%# Eval("mdl_name") %></td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdRemove" CommandName="REMOVE" CssClass="btn btn-green" runat="server" OnClick="cmdRemove_Click" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "smm_id") %>'><i class="icon-trash"></i> &nbsp;Remove</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Package Module Add</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <div class="control-group">
                            <label for="txtCode" class="control-label">Select Module</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlservicemodules" runat="server" CssClass="select2-me input-large"></asp:DropDownList>
                                <span class="help-block">Select Module to add in current Package</span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" CssClass="btn btn-primary" OnClick="cmdSave_Click" runat="server"><i class="icon-ok"></i> Save</asp:LinkButton>
                            &nbsp;
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

