﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AuthorizeNet;
using clsDAL;
using System.Data;

public partial class procedetopay : System.Web.UI.Page
{
    clssubscriptionservices clsSS = new clssubscriptionservices();
    clssubscriptionservicemodulemapping clsSSM = new clssubscriptionservicemodulemapping();
    clsUserspermission clsUPM = new clsUserspermission();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsuserpackagesubscribtion clsUPS = new clsuserpackagesubscribtion();
    Guid userid { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        userid = new Guid(Request.QueryString["ui"].ToString());
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {

        if (Session["finalpay"] != null)
        {
            decimal amount = Convert.ToDecimal(Session["finalpay"]);
            var request = new AuthorizationRequest(txtccnum.Text.ToString(), txtexp.Text.ToString(), amount, string.Empty);
            //These are optional calls to the API
            if (txtcardcode.Text.ToString() == string.Empty)
            {
                lblmessage.Text = "Enter 3 Digits CCV code of your credit card";
                return;
            }
            else
            {
                request.AddCardCode(txtcardcode.Text.ToString());
            }

            //Customer info - this is used for Fraud Detection
            //request.AddCustomer("id", "first", "last", "address", "state", "zip");

            //order number
            //request.AddInvoice("invoiceNumber");

            //Custom values that will be returned with the response
            //request.AddMerchantValue("merchantValue", "value");

            //Shipping Address
            //request.AddShipping("id", "first", "last", "address", "state", "zip");


            //step 2 - create the gateway, sending in your credentials and setting the Mode to Test (boolean flag)
            //which is true by default
            //this login and key are the shared dev account - you should get your own if you 
            //want to do more testing
            var gate = new Gateway("7dg54q8fQEgx", "4zq2Vm86RHHg59js", true);

            //step 3 - make some money
            var response = gate.Send(request);


            // code for userto service mapping
            List<servicecart> cart = (List<servicecart>)Session["cart"];
            List<Guid> stafflist = clsCPM.GetAllUserByRoleandParent("user", userid, true);
            Boolean blnSvRslt = false;
            foreach (servicecart crtitm in cart)
            {
                clsUPS.us_userid = userid;
                clsUPS.us_packageid = crtitm.id;
                clsUPS.us_subscribeddate = DateTime.Now;
                clsUPS.us_status = true;
                Boolean blnUsSvRslt = clsUPS.Save();
                if (blnUsSvRslt)
                {
                    DataTable dtrslt = clsSS.GetServiceModules(crtitm.service);
                    foreach (DataRow dr in dtrslt.Rows)
                    {
                        clsUPM.upm_userid = userid;
                        clsUPM.upm_all = true;
                        clsUPM.upm_objectid = Convert.ToInt64(dr["ModuleID"]);
                        blnSvRslt = clsUPM.Save();
                        if (stafflist.Count > 0)
                        {
                            foreach (Guid stf in stafflist)
                            {
                                clsUPM.upm_userid = stf;
                                clsUPM.Save();
                            }
                        }
                    }
                }
            }

            if (blnSvRslt)
            {
                MySession.Current.MSGCODE = "servicepurchased";
                Response.Redirect("~/message.aspx");
            }
            else
            {
                MySession.Current.MSGCODE = "mappingerror";
                Response.Redirect("~/message.aspx");
            }
        }
        // code for account manage

        // redirect to message with purchage success
        else
        {
            Response.Redirect("~/mycart.aspx");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/mycart.aspx");
    }
}