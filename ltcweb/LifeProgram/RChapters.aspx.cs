﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class shpweb_LifeProgram_RChapters : System.Web.UI.Page
{
    clsWorkBook clsworkbook = new clsWorkBook();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (hidden.Value != "")
        {
            Session["lessonid"] = hidden.Value;
            Response.Redirect("~/LifeProgram/Lesson.aspx");
            return;
        }
        if (!IsPostBack)
        {
             _GridFill();
        }
    }
    void _GridFill()
    {
        string strusername = User.Identity.Name;
        int intcid = Convert.ToInt32(Session["workbookid"]);
        if (intcid == 0)
        {
            Response.Redirect("~/Client/DashBoard.aspx");
        }
        RTTitle.DataSource = clsworkbook.WorkbookRead(intcid);
        RT.DataSource = clsworkbook.ChapterTitle(intcid);
        RTLink.DataSource = clsworkbook.ChapterStatus(intcid, strusername);
        RTLink.DataBind();
        RT.DataBind();
        RTTitle.DataBind();
    }
}