﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using clsDAL;
using System.Web.Security;
using System.IO;
using System.Data;
using clsDAL;
using System.Web.Security;


/// <summary>
/// Summary description for CheckPermission
/// </summary>
public class UserPermision
{
    clsUserspermission clsUSRP = new clsDAL.clsUserspermission();
    Guid UserId = (Guid)Membership.GetUser().ProviderUserKey;

    public Boolean CheckAllPermission(Int64 ObjectId)
    {
        Boolean IsPermited = false;
        clsUSRP.SetGetSPFlag = "CHECKALL";
        IsPermited=clsUSRP.CheckRecords(UserId, ObjectId);
        return IsPermited;
    }

    public Boolean CheckAddPermission(Int64 ObjectId)
    {
        Boolean IsPermited = false;
        clsUSRP.SetGetSPFlag = "CHECKALL";
        IsPermited = clsUSRP.CheckRecords(UserId, ObjectId);
        if (IsPermited == false)
        {
            clsUSRP.SetGetSPFlag = "CHECKADD";
            IsPermited = clsUSRP.CheckRecords(UserId, ObjectId);
        }
        return IsPermited;
    }

    public Boolean CheckViewPermission(Int64 ObjectId)
    {
        Boolean IsPermited = false;
        clsUSRP.SetGetSPFlag = "CHECKALL";
        IsPermited = clsUSRP.CheckRecords(UserId, ObjectId);
        if (IsPermited == false)
        {
            clsUSRP.SetGetSPFlag = "CHECKVIEW";
            IsPermited = clsUSRP.CheckRecords(UserId, ObjectId);
        }
        return IsPermited;
    }

    public Boolean CheckEditPermission(Int64 ObjectId)
    {
        Boolean IsPermited = false;
        clsUSRP.SetGetSPFlag = "CHECKALL";
        IsPermited = clsUSRP.CheckRecords(UserId, ObjectId);
        if (IsPermited == false)
        {
            clsUSRP.SetGetSPFlag = "CHECKEDIT";
            IsPermited = clsUSRP.CheckRecords(UserId, ObjectId);
        }
        return IsPermited;
    }

    public Boolean CheckDeletePermission(Int64 ObjectId)
    {
        Boolean IsPermited = false;
        clsUSRP.SetGetSPFlag = "CHECKALL";
        IsPermited = clsUSRP.CheckRecords(UserId, ObjectId);
        if (IsPermited == false)
        {
            clsUSRP.SetGetSPFlag = "CHECKDLT";
            IsPermited = clsUSRP.CheckRecords(UserId, ObjectId);
        }
        return IsPermited;
    }
}
