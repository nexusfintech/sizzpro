﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsInsuranceplanMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsInsuranceplanMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsInsuranceplanMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _insp_id; }
            set
            {
                _insp_id = value;
                if (clsCMD.Parameters.Contains("@insp_id"))
                { clsCMD.Parameters["@insp_id"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _insp_id;
        public Int64 insp_id
        {
            get { return _insp_id; }
            set
            {
                _insp_id = value;
                if (clsCMD.Parameters.Contains("@insp_id"))
                { clsCMD.Parameters["@insp_id"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _insp_inscmpid;
        public Int64 insp_inscmpid
        {
            get { return _insp_inscmpid; }
            set
            {
                _insp_inscmpid = value;
                if (clsCMD.Parameters.Contains("@insp_inscmpid"))
                { clsCMD.Parameters["@insp_inscmpid"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_inscmpid", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _insp_groupnumber;
        public String insp_groupnumber
        {
            get { return _insp_groupnumber; }
            set
            {
                _insp_groupnumber = value;
                if (clsCMD.Parameters.Contains("@insp_groupnumber"))
                { clsCMD.Parameters["@insp_groupnumber"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_groupnumber", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _insp_planname;
        public String insp_planname
        {
            get { return _insp_planname; }
            set
            {
                _insp_planname = value;
                if (clsCMD.Parameters.Contains("@insp_planname"))
                { clsCMD.Parameters["@insp_planname"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_planname", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private Double _insp_deductible;
        public Double insp_deductible
        {
            get { return _insp_deductible; }
            set
            {
                _insp_deductible = value;
                if (clsCMD.Parameters.Contains("@insp_deductible"))
                { clsCMD.Parameters["@insp_deductible"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_deductible", SqlDbType.Money).Value = value; }
            }
        }
        private Double _insp_copay;
        public Double insp_copay
        {
            get { return _insp_copay; }
            set
            {
                _insp_copay = value;
                if (clsCMD.Parameters.Contains("@insp_copay"))
                { clsCMD.Parameters["@insp_copay"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_copay", SqlDbType.Money).Value = value; }
            }
        }
        private Double _insp_coinsurance;
        public Double insp_coinsurance
        {
            get { return _insp_coinsurance; }
            set
            {
                _insp_coinsurance = value;
                if (clsCMD.Parameters.Contains("@insp_coinsurance"))
                { clsCMD.Parameters["@insp_coinsurance"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_coinsurance", SqlDbType.Money).Value = value; }
            }
        }
        private Boolean _insp_covermhpo;
        public Boolean insp_covermhpo
        {
            get { return _insp_covermhpo; }
            set
            {
                _insp_covermhpo = value;
                if (clsCMD.Parameters.Contains("@insp_covermhpo"))
                { clsCMD.Parameters["@insp_covermhpo"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_covermhpo", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _insp_coverchiropractor;
        public Boolean insp_coverchiropractor
        {
            get { return _insp_coverchiropractor; }
            set
            {
                _insp_coverchiropractor = value;
                if (clsCMD.Parameters.Contains("@insp_coverchiropractor"))
                { clsCMD.Parameters["@insp_coverchiropractor"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_coverchiropractor", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _insp_coverdietition;
        public Boolean insp_coverdietition
        {
            get { return _insp_coverdietition; }
            set
            {
                _insp_coverdietition = value;
                if (clsCMD.Parameters.Contains("@insp_coverdietition"))
                { clsCMD.Parameters["@insp_coverdietition"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_coverdietition", SqlDbType.Bit).Value = value; }
            }
        }
        private String _insp_cptcodes;
        public String insp_cptcodes
        {
            get { return _insp_cptcodes; }
            set
            {
                _insp_cptcodes = value;
                if (clsCMD.Parameters.Contains("@insp_cptcodes"))
                { clsCMD.Parameters["@insp_cptcodes"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_cptcodes", SqlDbType.NVarChar).Value = value; }
            }
        }
        private Guid _insp_userid;
        public Guid insp_userid
        {
            get { return _insp_userid; }
            set
            {
                _insp_userid = value;
                if (clsCMD.Parameters.Contains("@insp_userid"))
                { clsCMD.Parameters["@insp_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Boolean _insp_sts;
        public Boolean insp_sts
        {
            get { return _insp_sts; }
            set
            {
                _insp_sts = value;
                if (clsCMD.Parameters.Contains("@insp_sts"))
                { clsCMD.Parameters["@insp_sts"].Value = value; }
                else { clsCMD.Parameters.Add("@insp_sts", SqlDbType.Bit).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                insp_id = Convert.ToInt64(dr["insp_id"]);
                insp_inscmpid = Convert.ToInt64(dr["insp_inscmpid"]);
                insp_groupnumber = Convert.ToString(dr["insp_groupnumber"]);
                insp_planname = Convert.ToString(dr["insp_planname"]);
                insp_deductible = Convert.ToDouble(dr["insp_deductible"]);
                insp_copay = Convert.ToDouble(dr["insp_copay"]);
                insp_coinsurance = Convert.ToDouble(dr["insp_coinsurance"]);
                insp_covermhpo = Convert.ToBoolean(dr["insp_covermhpo"]);
                insp_coverchiropractor = Convert.ToBoolean(dr["insp_coverchiropractor"]);
                insp_coverdietition = Convert.ToBoolean(dr["insp_coverdietition"]);
                insp_cptcodes = Convert.ToString(dr["insp_cptcodes"]);
                insp_userid = new Guid(dr["insp_userid"].ToString());
                insp_sts = Convert.ToBoolean(dr["insp_sts"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            insp_id = Convert.ToInt64("0");
            insp_inscmpid = Convert.ToInt64("0");
            insp_groupnumber = Convert.ToString("NA");
            insp_planname = Convert.ToString("NA");
            insp_deductible = Convert.ToDouble("0");
            insp_copay = Convert.ToDouble("0");
            insp_coinsurance = Convert.ToDouble("0");
            insp_covermhpo = Convert.ToBoolean(false);
            insp_coverchiropractor = Convert.ToBoolean(false);
            insp_coverdietition = Convert.ToBoolean(false);
            insp_cptcodes = Convert.ToString("NA");
            insp_userid = new Guid();
            insp_sts = Convert.ToBoolean(false);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.insp_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_InsuranceplanMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 insp_id)
        {
            Boolean blnResult = false;
            this.insp_id = insp_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_InsuranceplanMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 insp_id)
        {
            Boolean blnResult = false;
            this.insp_id = insp_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_InsuranceplanMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_InsuranceplanMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByAdmin(Guid adminid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.insp_userid = adminid;
                clsCMD.CommandText = "sp_InsuranceplanMasterGetData";
                SetGetSPFlag = "ALLBYADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsInsuranceplanMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsInsuranceplanMaster_PropertiesList> lstResult = new List<clsInsuranceplanMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].insp_id = Convert.ToInt64(dr["insp_id"]);
                lstResult[i].insp_inscmpid = Convert.ToInt64(dr["insp_inscmpid"]);
                lstResult[i].insp_groupnumber = Convert.ToString(dr["insp_groupnumber"]);
                lstResult[i].insp_planname = Convert.ToString(dr["insp_planname"]);
                lstResult[i].insp_deductible = Convert.ToDouble(dr["insp_deductible"]);
                lstResult[i].insp_copay = Convert.ToDouble(dr["insp_copay"]);
                lstResult[i].insp_coinsurance = Convert.ToDouble(dr["insp_coinsurance"]);
                lstResult[i].insp_covermhpo = Convert.ToBoolean(dr["insp_covermhpo"]);
                lstResult[i].insp_coverchiropractor = Convert.ToBoolean(dr["insp_coverchiropractor"]);
                lstResult[i].insp_coverdietition = Convert.ToBoolean(dr["insp_coverdietition"]);
                lstResult[i].insp_cptcodes = Convert.ToString(dr["insp_cptcodes"]);
                lstResult[i].insp_userid = new Guid(dr["insp_userid"].ToString());
                lstResult[i].insp_sts = Convert.ToBoolean(dr["insp_sts"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 insp_id)
        {
            Boolean blnResult = false;
            try
            {
                this.insp_id = insp_id;
                clsCMD.CommandText = "sp_InsuranceplanMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 insp_id)
        {
            Boolean blnResult = false;
            try
            {
                this.insp_id = insp_id;
                clsCMD.CommandText = "sp_InsuranceplanMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsInsuranceplanMaster_PropertiesList
    {
        public Int64 insp_id { get; set; }
        public Int64 insp_inscmpid { get; set; }
        public String insp_groupnumber { get; set; }
        public String insp_planname { get; set; }
        public Double insp_deductible { get; set; }
        public Double insp_copay { get; set; }
        public Double insp_coinsurance { get; set; }
        public Boolean insp_covermhpo { get; set; }
        public Boolean insp_coverchiropractor { get; set; }
        public Boolean insp_coverdietition { get; set; }
        public String insp_cptcodes { get; set; }
        public Guid insp_userid { get; set; }
        public Boolean insp_sts { get; set; }
    }
}