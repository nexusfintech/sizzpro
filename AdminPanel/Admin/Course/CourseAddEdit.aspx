﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CourseAddEdit.aspx.cs" Inherits="Admin_Course_CourseAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkCourse" runat="server" NavigateUrl="~/Admin/Course/CourseMaster.aspx"><i class="icon-retweet"></i>&nbsp;Services</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Service Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <% if (User.IsInRole("administrator"))
                           { %>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Member</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectMember" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                <span class="help-block">Select Member for below appointment course creation</span>
                            </div>
                        </div>
                        <%} %>
                        <div class="control-group">
                            <label for="txtCourseName" class="control-label">Service Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtCourseName" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtDescription" class="control-label">Description</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDescription" runat="server" CssClass="input-xlarge" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtGroupName" class="control-label">Category</label>
                            <div class="controls">
                                <asp:TextBox ID="txtGroupName" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtMinutes" class="control-label">Appoinment Duration</label>
                            <div class="controls">
                                <div class="span2">
                                    <asp:TextBox ID="txtHour" runat="server" CssClass="spinner input-mini"></asp:TextBox>
                                    <span class="help-block">Hour</span>
                                </div>
                                <div class="span2">
                                    <asp:TextBox ID="txtMinutes" runat="server" CssClass="spinner input-mini" ></asp:TextBox>
                                    <span class="help-block">Min</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                            <asp:Button ID="cmdCancel" runat="server" CssClass="btn" Text="Cancel" OnClick="cmdCancel_Click" />
                            &nbsp;
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $('#<%= txtHour.ClientID %>').spinner({ min: 0, max: 9 });
        $('#<%= txtMinutes.ClientID %>').spinner({ min: 0, max: 60 });
    </script>
</asp:Content>

