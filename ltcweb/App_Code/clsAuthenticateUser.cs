﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

public class clsAuthenticateUser
{
    clsConnectionlifeprogram clsSQLCon = new clsConnectionlifeprogram();
    public bool AuthenticateUser(string uname, string upass)
    {
        bool isAuthenticate = false;
        SqlConnection sqlcon = new SqlConnection();
        try
        {
            Boolean blnCon = clsSQLCon.GetConnection();
            if (blnCon == true)
            {
                sqlcon = clsSQLCon.SqlDBCon;
                using (SqlCommand cmd = new SqlCommand("cp_authenticate_user", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@username", uname);
                    cmd.Parameters.AddWithValue("@password", upass);
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows == true)
                    {
                        isAuthenticate = true;
                    }
                    else
                    {
                        isAuthenticate = false;
                    }
                    rdr.Close();
                }
            }
        }
        catch
        {
            isAuthenticate = false;
        }
        finally
        {
            sqlcon.Close();
        }
        return isAuthenticate;
    }
}

