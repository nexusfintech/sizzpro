﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Client_PreviousAppointment : System.Web.UI.Page
{
    clsAppointmentMaster clsAPNT = new clsAppointmentMaster();

    private void BindGridData()
    {
        clsAPNT.apt_clientid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        rptAppontment.DataSource = clsAPNT.GetAllPreviousClientWise();
        rptAppontment.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        { BindGridData(); }
    }
}