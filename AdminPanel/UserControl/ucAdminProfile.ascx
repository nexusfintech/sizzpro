﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAdminProfile.ascx.cs" Inherits="UserControl_ucAdminProfile" %>

<h4>
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
</h4>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3>
                    <i class="icon-user"></i>
                    <asp:Label ID="lblProfileHead" runat="server"></asp:Label>
                </h3>
            </div>
            <div class="box-content nopadding">
                <ul class="tabs tabs-inline tabs-top">
                    <li class='active'>
                        <a href="#profile" data-toggle='tab'><i class="icon-user"></i>Individual Information</a>
                    </li>
                    <li>
                        <a href="#organizational" data-toggle='tab'><i class="glyphicon-notes"></i>Organization Profile </a>
                    </li>
                </ul>
                <div class="tab-content padding tab-content-inline tab-content-bottom">
                    <div class="tab-pane active" id="profile">
                        <div class="form-horizontal">
                            <div class="row-fluid">
                                <div class="span2">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="max-width: 400px; max-height: 400px;">
                                            <asp:Image ID="imgProfilePhoto" runat="server" ImageUrl="~/img/demo/user-2.jpg" Height="200px" Width="200px" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <asp:FileUpload ID="fuprofilephoto" runat="server" />
                                        <div>
                                            <%--<span class="btn btn-file">
                                                <span class="fileupload-new">Select image
                                                </span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" name='imagefile' />
                                            </span>--%>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="span10">
                                    <div class="control-group">
                                        <label for="name" class="control-label right">First Name :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtFirstname" runat="server" data-rule-required="true" CssClass="input-xlarge"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="country" class="control-label right">Middle Name :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtMiddlename" runat="server" data-rule-required="true" CssClass="input-xlarge focused"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="name" class="control-label right">Last Name :</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <asp:TextBox ID="txtLastname" runat="server" data-rule-required="true" CssClass="input-xlarge focused"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="name" class="control-label right">Email :</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <asp:TextBox ID="txtEmail" runat="server" data-rule-required="true" CssClass="input-xlarge focused" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="email" class="control-label right">Date of Birth :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtDOB" runat="server" data-rule-required="true" CssClass="input-medium datepick"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="pw" class="control-label right">SSN :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtSSN" runat="server" data-rule-required="true" CssClass="input-xlarge focused"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" CssClass="btn btn-primary" runat="server" OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                        </div>
                    </div>

                    <div class="tab-pane" id="organizational">
                        <div class="form-horizontal">
                            <div class="row-fluid">
                                <%--<div class="span2">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="max-width: 400px; max-height: 400px;">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/demo/user-2.jpg" Height="200px" Width="200px" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                        <div>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="span10">
                                    <div class="control-group">
                                        <label for="name" class="control-label right">Organization Name :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtOrgName" runat="server" data-rule-required="true" CssClass="input-xlarge"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="country" class="control-label right">Office Address :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtOfcAddress" runat="server" data-rule-required="true" CssClass="input-xlarge" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="name" class="control-label right">Office Phone :</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <asp:TextBox ID="txtOfcPhone" runat="server" data-rule-required="true" CssClass="input-xlarge"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="name" class="control-label right">Fax :</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <asp:TextBox ID="txtFax" runat="server" data-rule-required="true" CssClass="input-xlarge"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="email" class="control-label right">Tax Id :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtTaxId" runat="server" data-rule-required="true" CssClass="input-xlarge"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="pw" class="control-label right">Legal Name :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtlglName" runat="server" data-rule-required="true" CssClass="input-xlarge"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="pw" class="control-label right">Legal Address :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtLglAddress" runat="server" CssClass="input-xlarge" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="lnkOrgSave" CssClass="btn btn-primary" runat="server" OnClick="lnkOrgSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
