﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
public partial class Admin_SessionManagement_ClaimMaster : System.Web.UI.Page
{
    clsClientClaim clsCC = new clsClientClaim();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            binddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void binddata()
    {
        rptClmMaster.DataSource = clsCC.GetAllRecord();
        rptClmMaster.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/ClaimManagment/ClaimAddEdit.aspx");
    }
    protected void rptClmMaster_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if(e.CommandName == "EDIT")
        {
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            MySession.Current.AddEditFlag = "EDIT";
            Response.Redirect("~/Admin/ClaimManagment/ClaimAddEdit.aspx");
        }
    }
    protected void cmdSession_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/SessionManagement/FollowupSession.aspx");
    }
}