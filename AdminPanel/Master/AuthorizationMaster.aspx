﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="AuthorizationMaster.aspx.cs" Inherits="Master_AuthorizationMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
     <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn" Text="Add New Authorization" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Authorization Master
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptAutho" runat="server" OnItemCommand="rptAutho_ItemCommand">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Authorized Session</th>
                                        <th>CPT Code</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align:center"><%# Eval("auth_authorizationnumber") %></td>
                                <td style="text-align:center"><%# Eval("auth_startdate","{0:MM/dd/yyyy}") %></td>
                                <td style="text-align:center"><%# Eval("auth_enddate","{0:MM/dd/yyyy}") %></td>
                                <td style="text-align:center"><%# Eval("auth_authorisedsession") %></td>
                                <td style="text-align:center"><%# Eval("cd_code") %></td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "auth_id") %>'><i class="icon-edit-sign"></i> &nbsp;Edit</asp:LinkButton>
                                    <asp:LinkButton ID="cmdDlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "auth_id") %>' rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i> </asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>