﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsFollowupDignosisProcedure
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsFollowupDignosisProcedure()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsFollowupDignosisProcedure()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _sdp_id; }
            set
            {
                _sdp_id = value;
                if (clsCMD.Parameters.Contains("@sdp_id"))
                { clsCMD.Parameters["@sdp_id"].Value = value; }
                else { clsCMD.Parameters.Add("@sdp_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _sdp_id;
        public Int64 sdp_id
        {
            get { return _sdp_id; }
            set
            {
                _sdp_id = value;
                if (clsCMD.Parameters.Contains("@sdp_id"))
                { clsCMD.Parameters["@sdp_id"].Value = value; }
                else { clsCMD.Parameters.Add("@sdp_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _sdp_sessionid;
        public Guid sdp_sessionid
        {
            get { return _sdp_sessionid; }
            set
            {
                _sdp_sessionid = value;
                if (clsCMD.Parameters.Contains("@sdp_sessionid"))
                { clsCMD.Parameters["@sdp_sessionid"].Value = value; }
                else { clsCMD.Parameters.Add("@sdp_sessionid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _sdp_cptcode;
        public Int64 sdp_cptcode
        {
            get { return _sdp_cptcode; }
            set
            {
                _sdp_cptcode = value;
                if (clsCMD.Parameters.Contains("@sdp_cptcode"))
                { clsCMD.Parameters["@sdp_cptcode"].Value = value; }
                else { clsCMD.Parameters.Add("@sdp_cptcode", SqlDbType.BigInt).Value = value; }
            }
        }
        private Double _sdp_linecharge;
        public Double sdp_linecharge
        {
            get { return _sdp_linecharge; }
            set
            {
                _sdp_linecharge = value;
                if (clsCMD.Parameters.Contains("@sdp_linecharge"))
                { clsCMD.Parameters["@sdp_linecharge"].Value = value; }
                else { clsCMD.Parameters.Add("@sdp_linecharge", SqlDbType.Money).Value = value; }
            }
        }
        private String _sdp_dignosiscode;
        public String sdp_dignosiscode
        {
            get { return _sdp_dignosiscode; }
            set
            {
                _sdp_dignosiscode = value;
                if (clsCMD.Parameters.Contains("@sdp_dignosiscode"))
                { clsCMD.Parameters["@sdp_dignosiscode"].Value = value; }
                else { clsCMD.Parameters.Add("@sdp_dignosiscode", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _sdp_moda;
        public String sdp_moda
        {
            get { return _sdp_moda; }
            set
            {
                _sdp_moda = value;
                if (clsCMD.Parameters.Contains("@sdp_moda"))
                { clsCMD.Parameters["@sdp_moda"].Value = value; }
                else { clsCMD.Parameters.Add("@sdp_moda", SqlDbType.NVarChar, 5).Value = value; }
            }
        }
        private String _sdp_modb;
        public String sdp_modb
        {
            get { return _sdp_modb; }
            set
            {
                _sdp_modb = value;
                if (clsCMD.Parameters.Contains("@sdp_modb"))
                { clsCMD.Parameters["@sdp_modb"].Value = value; }
                else { clsCMD.Parameters.Add("@sdp_modb", SqlDbType.NVarChar, 5).Value = value; }
            }
        }
        private String _sdp_modc;
        public String sdp_modc
        {
            get { return _sdp_modc; }
            set
            {
                _sdp_modc = value;
                if (clsCMD.Parameters.Contains("@sdp_modc"))
                { clsCMD.Parameters["@sdp_modc"].Value = value; }
                else { clsCMD.Parameters.Add("@sdp_modc", SqlDbType.NVarChar, 5).Value = value; }
            }
        }
        private String _sdp_modd;
        public String sdp_modd
        {
            get { return _sdp_modd; }
            set
            {
                _sdp_modd = value;
                if (clsCMD.Parameters.Contains("@sdp_modd"))
                { clsCMD.Parameters["@sdp_modd"].Value = value; }
                else { clsCMD.Parameters.Add("@sdp_modd", SqlDbType.NVarChar, 5).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                sdp_id = Convert.ToInt64(dr["sdp_id"]);
                sdp_sessionid =new Guid(dr["sdp_sessionid"].ToString());
                sdp_cptcode = Convert.ToInt64(dr["sdp_cptcode"]);
                sdp_linecharge = Convert.ToDouble(dr["sdp_linecharge"]);
                sdp_dignosiscode = Convert.ToString(dr["sdp_dignosiscode"]);
                sdp_moda = Convert.ToString(dr["sdp_moda"]);
                sdp_modb = Convert.ToString(dr["sdp_modb"]);
                sdp_modc = Convert.ToString(dr["sdp_modc"]);
                sdp_modd = Convert.ToString(dr["sdp_modd"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            sdp_id = Convert.ToInt64("0");
            sdp_sessionid = new Guid();
            sdp_cptcode = Convert.ToInt64("0");
            sdp_linecharge = Convert.ToDouble("0");
            sdp_dignosiscode = Convert.ToString("NA");
            sdp_moda = Convert.ToString("NA");
            sdp_modb = Convert.ToString("NA");
            sdp_modc = Convert.ToString("NA");
            sdp_modd = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.sdp_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_FollowupDignosisProcedureAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 sdp_id)
        {
            Boolean blnResult = false;
            this.sdp_id = sdp_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_FollowupDignosisProcedureAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 sdp_id)
        {
            Boolean blnResult = false;
            this.sdp_id = sdp_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_FollowupDignosisProcedureAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_FollowupDignosisProcedureGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsFollowupDignosisProcedure_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsFollowupDignosisProcedure_PropertiesList> lstResult = new List<clsFollowupDignosisProcedure_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].sdp_id = Convert.ToInt64(dr["sdp_id"]);
                lstResult[i].sdp_sessionid = new Guid(dr["sdp_sessionid"].ToString());
                lstResult[i].sdp_cptcode = Convert.ToInt64(dr["sdp_cptcode"]);
                lstResult[i].sdp_linecharge = Convert.ToDouble(dr["sdp_linecharge"]);
                lstResult[i].sdp_dignosiscode = Convert.ToString(dr["sdp_dignosiscode"]);
                lstResult[i].sdp_moda = Convert.ToString(dr["sdp_moda"]);
                lstResult[i].sdp_modb = Convert.ToString(dr["sdp_modb"]);
                lstResult[i].sdp_modc = Convert.ToString(dr["sdp_modc"]);
                lstResult[i].sdp_modd = Convert.ToString(dr["sdp_modd"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 sdp_id)
        {
            Boolean blnResult = false;
            try
            {
                this.sdp_id = sdp_id;
                clsCMD.CommandText = "sp_FollowupDignosisProcedureGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 sdp_id)
        {
            Boolean blnResult = false;
            try
            {
                this.sdp_id = sdp_id;
                clsCMD.CommandText = "sp_FollowupDignosisProcedureGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public DataTable GetAllRecordBySession(Guid sessionid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.sdp_sessionid = sessionid;
                clsCMD.CommandText = "sp_FollowupDignosisProcedureGetData";
                SetGetSPFlag = "BYSESSION";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }
        #endregion

    }
    public class clsFollowupDignosisProcedure_PropertiesList
    {
        public Int64 sdp_id { get; set; }
        public Guid sdp_sessionid { get; set; }
        public Int64 sdp_cptcode { get; set; }
        public Double sdp_linecharge { get; set; }
        public String sdp_dignosiscode { get; set; }
        public String sdp_moda { get; set; }
        public String sdp_modb { get; set; }
        public String sdp_modc { get; set; }
        public String sdp_modd { get; set; }
    }
}