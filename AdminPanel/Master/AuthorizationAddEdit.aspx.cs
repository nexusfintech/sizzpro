﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
using System.Globalization;

public partial class Master_AuthorizationAddEdit : System.Web.UI.Page
{
    clsCPTCodeMaster clsCCM = new clsCPTCodeMaster();
    clsauthorizationMaster clsAM = new clsauthorizationMaster();
    DateTime dtStrt;
    DateTime dtEnd;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindddl();
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                loaddata();
            }
        }
    }
    private void loaddata()
    {
        Boolean blnRslt = clsAM.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {

            txtauthssn.Text = clsAM.auth_authorisedsession.ToString();
            txtauthcode.Text = clsAM.auth_authorizationnumber;
            txtstartdate.Text = clsAM.auth_startdate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            txtenddate.Text = clsAM.auth_enddate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            ListItem lstcpt = ddlCptCode.Items.FindByValue(clsAM.auth_cptcode.ToString());
            if (lstcpt != null)
            {
                ddlCptCode.SelectedIndex = ddlCptCode.Items.IndexOf(lstcpt);
            }
        }
        else
        {
            MySession.Current.MESSAGE = clsAM.GetSetErrorMessage;
            Response.Redirect("~/Master/BillingProviderMaster.aspx");
        }
    }

    private void bindddl()
    {
        ddlCptCode.Items.Clear();
        ddlCptCode.SelectedIndex = -1;
        ddlCptCode.SelectedValue = null;
        ddlCptCode.ClearSelection();
        ddlCptCode.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<< -- Pleasse Select -- >>";
        lst.Selected = true;
        ddlCptCode.Items.Add(lst);
        ddlCptCode.DataSource = clsCCM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        ddlCptCode.DataTextField = "cd_code";
        ddlCptCode.DataValueField = "cd_id";
        ddlCptCode.DataBind();
    }
    private string validatepage()
    {
        string result = string.Empty;
        if (txtauthcode.Text == string.Empty)
        {
            return result = "Authorization Code Required";
        }
        else if (txtstartdate.Text == string.Empty)
        {
            return result = "Start Date is Required";
        }
        else if (!(DateTime.TryParseExact(txtstartdate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtStrt)))
        {
            return result = "Invalid Start Date format please enter in mm/dd/yyyy";
        }
        else if (txtenddate.ToString() == string.Empty)
        {
            return result = "End Date is Required";
        }
        else if (!(DateTime.TryParseExact(txtenddate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtEnd)))
        {
            return result = "Invalid End Date format please enter in mm/dd/yyyy";
        }
        else if (txtauthssn.Text == string.Empty)
        {
            return result = "Authorised Session is Required";
        }
        else
        {
            return result;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string vldtreslt = validatepage();
        if (vldtreslt != string.Empty)
        {
            lblmessage.Text = vldtreslt;
            msgdiv.Visible = true;
            return;
        }

        clsAM.auth_authorizationnumber = txtauthcode.Text;
        clsAM.auth_authorisedsession = Convert.ToInt64(txtauthssn.Text);
        clsAM.auth_cptcode = Convert.ToInt64(ddlCptCode.SelectedValue);
        clsAM.auth_startdate = dtStrt;
        clsAM.auth_enddate = dtEnd;
        clsAM.auth_userid = (Guid)Membership.GetUser().ProviderUserKey;
        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnRslt = clsAM.Save();
        }
        else
        {
            blnRslt = clsAM.Update(MySession.Current.EditID);
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/AuthorizationMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsAM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/AuthorizationMaster.aspx");
    }
}