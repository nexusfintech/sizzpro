﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WorkbookAddEdit.aspx.cs" Inherits="Workbook_WorkbookAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkCreateNew" runat="server" NavigateUrl="~/Workbook/WorkbookMaster.aspx"><i class="icon-retweet"></i>&nbsp;WorkBooks</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </div>
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Workbook Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <% if (User.IsInRole("administrator"))
                           { %>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Creater</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectMember" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                <span class="help-block">Select Creater for whom Workbook Is created</span>
                            </div>
                        </div>
                        <%} %>
                        <asp:HiddenField runat="server" ID="wrkcd" />
                        <asp:HiddenField runat="server" ID="wrksts" />
                        <div class="control-group">
                            <label for="txtworkbookname" class="control-label">Workbook Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtworkbookname" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtshort" class="control-label">Workbook Short</label>
                            <div class="controls">
                                <asp:TextBox ID="txtshort" runat="server" CssClass="input-medium"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtshort" class="control-label">Locked Price</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on">$</span>
                                    <asp:TextBox ID="txtlockedprice" runat="server" CssClass="input-small"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtshort" class="control-label">Unlocked Price</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on">$</span>
                                    <asp:TextBox ID="txtunlockedprice" runat="server" CssClass="input-small"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtDescription" class="control-label">Description</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDescription" runat="server" class='ckeditor' TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" runat="server" CssClass="btn btn-primary" OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                            <asp:LinkButton ID="cmdCancel" runat="server" CssClass="btn" OnClick="cmdCancel_Click"><i class="icon-remove"></i> Cancle</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

