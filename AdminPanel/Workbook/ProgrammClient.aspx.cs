﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;
using System.Data.SqlClient;
public partial class Workbook_ClientProgramm : System.Web.UI.Page
{
    clsProgramAssignUser clsPAU = new clsProgramAssignUser();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindddata();
        }
    }
    public void bindddata()
    {
        if (User.IsInRole("administrator"))
        {
            dtClientPrg.DataSource = clsPAU.GetAllProgrammClient();
            dtClientPrg.DataBind();
        }
        else
        {
            if (MySession.Current.ObjOwnerID != null)
            {
                dtClientPrg.DataSource = clsPAU.GetProgrammClient(MySession.Current.ObjOwnerID);
                dtClientPrg.DataBind();
            }
            else
            {
                Response.Redirect("~/Workbook/ProgramMaster.aspx");
            }
        }
    }
    protected void lnkWrkbooks_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = (LinkButton)sender;
        string[] args = lnkbtn.CommandArgument.Split('#');
        MySession.Current.EditID = Convert.ToInt64(args[0]);
        MySession.Current.AssignmentClientId = new Guid(args[1]);
        Response.Redirect("~/Workbook/ProgrammWorkbook.aspx");
    }
}