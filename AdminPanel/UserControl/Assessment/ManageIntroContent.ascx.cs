﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class UserControl_Assessment_ManageIntroContent : System.Web.UI.UserControl
{
    clsInitialAssessmentsIntro clsIntro = new clsInitialAssessmentsIntro();
    clsUsers clsUsr = new clsUsers();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();

    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        lblBoxHeader.Text = "Manage " + mu.UserName + "'s Intro Content";
    }

    private void EditEntry()
    {
        Guid objKey = new Guid(strMemberkey);
        Boolean blnResult = clsIntro.GetRecordByUserIDInProperties(objKey);
        if (blnResult == true)
        {
            txtContent.Text = clsIntro.itr_introtext;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Boolean isRcrdFound = clsASR.GetRecordByStepID(1, new Guid(strMemberkey));
            if (isRcrdFound)
            {
                cmdReschedule.Visible = true;
            }
            SetValue();
            EditEntry();
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Guid objKey = new Guid(strMemberkey);
        clsIntro.itr_user_id = objKey;
        clsIntro.itr_introtext = txtContent.Text.Trim();
        Boolean blnResult = clsIntro.Save(); 
        if (blnResult == true)
        {
            clsASR.storcrd_stepid = 1;
            clsASR.stprcrd_userid = objKey;
            Boolean stprslt = clsASR.Save();
            if (stprslt == true)
            {
                lblMessage.Text = "Save Successfully."; 
            }
            else
            {
                lblMessage.Text = "Error in Save..." + clsIntro.GetSetErrorMessage;
            }
        }
        else { 
            lblMessage.Text = "Error in Save..." + clsIntro.GetSetErrorMessage; 
        }

    }

    protected void cmdReschedule_Click(object sender, EventArgs e)
    {
        Boolean blnresult = clsASR.DeleteByStepID(1, new Guid(strMemberkey));
        if(blnresult == true)
        {
            lblMessage.Text = clsASR.GetSetErrorMessage;
        }
        else {
            lblMessage.Text = "Error in Operation..." + clsASR.GetSetErrorMessage; 
        }
    }
}