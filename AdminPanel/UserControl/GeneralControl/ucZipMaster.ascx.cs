﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class UserControl_GeneralControl_ucZipMaster : System.Web.UI.UserControl
{
    clsZipMaster clszip = new clsZipMaster();
    private Int64 intId = 0;
    private string strName = "";
    private string strMasterflag = "";

    public Int64 SelectedValue
    {
        get { intId = Convert.ToInt64(ddlZipmaster.SelectedValue); return intId; }
        set { intId = value; }
    }

    public string SelectedName
    {
        get { strName = ddlZipmaster.SelectedItem.Text; return strName; }
        set { strName = value; }
    }

    public string MasterFlag
    {
        get { return strMasterflag; }
        set { strMasterflag = value; }
    }

    public int Width
    {
        set { ddlZipmaster.Width = Unit.Pixel(value); }
    }

    public void Bind()
    {
        ddlZipmaster.DataSource = clszip.GetAllByMasterFlag(MasterFlag);
        ddlZipmaster.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}