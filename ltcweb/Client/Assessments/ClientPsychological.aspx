﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientPsychological.aspx.cs" Inherits="Clients_Assessment_ClientPsychological" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cpMainContent" runat="Server">
    <asp:HiddenField ID="AssesmentStep" runat="server" Value="17" />
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-heart"></i>
                Client's Psychological Information
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:Psychological ID="ucpsychological" runat="server" />
        </div>
    </div>
     <div class="row-fluid">
        <hr />
        <div class="span12">
            <asp:Button ID="cmdNext" runat="server" Text="Next Addiction >>" CssClass="btn btn-primary" OnClick="cmdNext_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $("textarea").keyup(function (e) {
                while ($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                    $(this).height($(this).height() + 1);
                };
            });
        });
    </script>
</asp:Content>

