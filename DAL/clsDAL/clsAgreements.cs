﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsAgreements
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsAgreements()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsAgreements()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _agr_id; }
            set
            {
                _agr_id = value;
                if (clsCMD.Parameters.Contains("@agr_id"))
                { clsCMD.Parameters["@agr_id"].Value = value; }
                else { clsCMD.Parameters.Add("@agr_id", SqlDbType.BigInt).Value = value; }
            }
        }
        
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _agr_id;
        public Int64 agr_id
        {
            get { return _agr_id; }
            set
            {
                _agr_id = value;
                if (clsCMD.Parameters.Contains("@agr_id"))
                { clsCMD.Parameters["@agr_id"].Value = value; }
                else { clsCMD.Parameters.Add("@agr_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _agr_createdby;
        public Guid agr_createdby
        {
            get { return _agr_createdby; }
            set
            {
                _agr_createdby = value;
                if (clsCMD.Parameters.Contains("@agr_createdby"))
                { clsCMD.Parameters["@agr_createdby"].Value = value; }
                else { clsCMD.Parameters.Add("@agr_createdby", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private string _agr_name;
        public string agr_name
        {
            get { return _agr_name; }
            set
            {
                _agr_name = value;
                if (clsCMD.Parameters.Contains("@agr_name"))
                { clsCMD.Parameters["@agr_name"].Value = value; }
                else { clsCMD.Parameters.Add("@agr_name", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        
        private String _agr_content;
        public String agr_content
        {
            get { return _agr_content; }
            set
            {
                _agr_content = value;
                if (clsCMD.Parameters.Contains("@agr_content"))
                { clsCMD.Parameters["@agr_content"].Value = value; }
                else { clsCMD.Parameters.Add("@agr_content", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                agr_id = Convert.ToInt64(dr["agr_id"]);
                agr_name = Convert.ToString(dr["agr_name"]);
                agr_createdby = new Guid(dr["agr_createdby"].ToString());
                agr_content = Convert.ToString(dr["agr_content"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            agr_id = Convert.ToInt64("0");
            agr_createdby = new Guid();
            agr_content = string.Empty;
            agr_name = string.Empty;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.agr_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_tbl_AgreementsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 agr_id)
        {
            Boolean blnResult = false;
            this.agr_id = agr_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_tbl_AgreementsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 agr_id)
        {
            Boolean blnResult = false;
            this.agr_id = agr_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_tbl_AgreementsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_tbl_AgreementsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByUser(Guid UserId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.agr_createdby = UserId;
                clsCMD.CommandText = "sp_tbl_AgreementsGetData";
                SetGetSPFlag = "BYUSER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsAgreements_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsAgreements_PropertiesList> lstResult = new List<clsAgreements_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].agr_id = Convert.ToInt64(dr["agr_id"]);
                lstResult[i].agr_createdby = new Guid(dr["agr_createdby"].ToString());
                lstResult[i].agr_content = Convert.ToString(dr["agr_content"]);

                i++;
            }
            return lstResult;
        }
        
        public Boolean GetRecordByIDInProperties(Int64 agr_id)
        {
            Boolean blnResult = false;
            try
            {
                this.agr_id = agr_id;
                clsCMD.CommandText = "sp_tbl_AgreementsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 agr_id)
        {
            Boolean blnResult = false;
            try
            {
                this.agr_id = agr_id;
                clsCMD.CommandText = "sp_tbl_AgreementsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsAgreements_PropertiesList
    {
        public Int64 agr_id { get; set; }
        public Guid agr_createdby { get; set; }
        public String agr_content { get; set; }
        public String agr_name { get; set; }
    }
}