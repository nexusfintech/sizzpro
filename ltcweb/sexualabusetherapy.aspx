﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="sexualabusetherapy.aspx.cs" Inherits="sexualabusetherapy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-group"></i>&nbsp;Sexual Abuse Therapy</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <p>Anyone who has been a victim of a crime will tell you it changes your life forever. You never really feel safe and sometimes anxiety can get the best of you. Many who have suffered sexual abuse express frustration with being unable to get over the experience not being able to live a happy productive life. They experience constant frustration of living with the memory and personal impact this experience has had on them. </p>
          <p>Life without recovery can be painful. This life includes depressed moods, angry explosions, self-consciousness, rocky relationships, insecurity, substance abuse, promiscuity, anxiety, and constantly feeling overwhelmed. Many are often suicidal and engage in self-destructive behavior. It is imperative that individuals having experienced sexual abuse seek treatment to normalize their experience and develop the skills they need to live beyond the abuse. Our <a href="groups.aspx">life recovery program</a> has helped many work through their issues of the past to discover the life of their dreams.</p>
          <p><strong>FREE Sexual Abuse Recovery Workshop for Women</strong></p>
          <p>If you have suffered sexual trauma in your life and you desire to feel better about yourself it is important for you do get some help from caring professionals who can guide you along the path of healing. Therapy will help you feel less shame about your experience. Therapy will help you see yourself in a more positive light and help you get through all those negative feelings. This  one hour seminar will open your  eyes to some powerful strategies to deal with the negative thoughts and behaviors that you may be experiencing. Joseph L. Follette, Jr., LMFT has been treating women who have been abused for many years and will offer helpful advice to get you started on your path to healing. Salena Potter, M.A. will share her personal experience of recovery from rape. You should find this seminar very informative and inspirational. </p>
          <p>  This seminar is offered every first Thursday of the month. Click <a href="http://lifestyletherapycoach.com/seminar/seminar.asp?id=13">here</a> to get information about the  next  free seminar.</p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

