﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;


/// <summary>
/// Summary description for clsFormat
/// </summary>
public class clsFormat
{
    public clsFormat()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static Boolean ValidateDate(string strValue)
    {
        Match match = Regex.Match(strValue, @"^(((0?[1-9]|1[012])/(0?[1-9]|1\d|2[0-8])|(0?[13456789]|1[012])/(29|30)|(0?[13578]|1[02])/31)/(19|[2-9]\d)\d{2}|0?2/29/((19|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00)))$", RegexOptions.IgnoreCase);
        if (match.Success)
        { return true; }
        else { return false; }
    }
}