﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsEmailTemplate
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsEmailTemplate()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsEmailTemplate()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _emt_id; }
            set
            {
                _emt_id = value;
                if (clsCMD.Parameters.Contains("@emt_id"))
                { clsCMD.Parameters["@emt_id"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _emt_id;
        public Int64 emt_id
        {
            get { return _emt_id; }
            set
            {
                _emt_id = value;
                if (clsCMD.Parameters.Contains("@emt_id"))
                { clsCMD.Parameters["@emt_id"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _emt_groupid;
        public Int64 emt_groupid
        {
            get { return _emt_groupid; }
            set
            {
                _emt_groupid = value;
                if (clsCMD.Parameters.Contains("@emt_groupid"))
                { clsCMD.Parameters["@emt_groupid"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_groupid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _emt_mailaccountid;
        public Int64 emt_mailaccountid
        {
            get { return _emt_mailaccountid; }
            set
            {
                _emt_mailaccountid = value;
                if (clsCMD.Parameters.Contains("@emt_mailaccountid"))
                { clsCMD.Parameters["@emt_mailaccountid"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_mailaccountid", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _emt_templatename;
        public String emt_templatename
        {
            get { return _emt_templatename; }
            set
            {
                _emt_templatename = value;
                if (clsCMD.Parameters.Contains("@emt_templatename"))
                { clsCMD.Parameters["@emt_templatename"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_templatename", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _emt_templatesubject;
        public String emt_templatesubject
        {
            get { return _emt_templatesubject; }
            set
            {
                _emt_templatesubject = value;
                if (clsCMD.Parameters.Contains("@emt_templatesubject"))
                { clsCMD.Parameters["@emt_templatesubject"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_templatesubject", SqlDbType.NVarChar, 50).Value = value; }
            }
        }


        private String _emt_ccmail;
        public String emt_ccmail
        {
            get { return _emt_ccmail; }
            set
            {
                _emt_ccmail = value;
                if (clsCMD.Parameters.Contains("@emt_ccmail"))
                { clsCMD.Parameters["@emt_ccmail"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_ccmail", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        private String _emt_bccmail;
        public String emt_bccmail
        {
            get { return _emt_bccmail; }
            set
            {
                _emt_bccmail = value;
                if (clsCMD.Parameters.Contains("@emt_bccmail"))
                { clsCMD.Parameters["@emt_bccmail"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_bccmail", SqlDbType.NVarChar, 50).Value = value; }
            }
        }


        private String _emt_templatebody;
        public String emt_templatebody
        {
            get { return _emt_templatebody; }
            set
            {
                _emt_templatebody = value;
                if (clsCMD.Parameters.Contains("@emt_templatebody"))
                { clsCMD.Parameters["@emt_templatebody"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_templatebody", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private String _emt_templateflag;
        public String emt_templateflag
        {
            get { return _emt_templateflag; }
            set
            {
                _emt_templateflag = value;
                if (clsCMD.Parameters.Contains("@emt_templateflag"))
                { clsCMD.Parameters["@emt_templateflag"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_templateflag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _emt_templatetype;
        public String emt_templatetype
        {
            get { return _emt_templatetype; }
            set
            {
                _emt_templatetype = value;
                if (clsCMD.Parameters.Contains("@emt_templatetype"))
                { clsCMD.Parameters["@emt_templatetype"].Value = value; }
                else { clsCMD.Parameters.Add("@emt_templatetype", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                emt_id = Convert.ToInt64(dr["emt_id"]);
                emt_groupid = Convert.ToInt64(dr["emt_groupid"]);
                emt_mailaccountid = Convert.ToInt64(dr["emt_mailaccountid"]);
                emt_templatename = Convert.ToString(dr["emt_templatename"]);
                emt_templatesubject = Convert.ToString(dr["emt_templatesubject"]);
                emt_ccmail = Convert.ToString(dr["emt_ccmail"]);
                emt_bccmail = Convert.ToString(dr["emt_bccmail"]);
                emt_templatebody = Convert.ToString(dr["emt_templatebody"]);
                emt_templateflag = Convert.ToString(dr["emt_templateflag"]);
                emt_templatetype = Convert.ToString(dr["emt_templatetype"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            emt_id = Convert.ToInt64("0");
            emt_groupid = Convert.ToInt64("0");
            emt_mailaccountid = Convert.ToInt64("0");
            emt_templatename = Convert.ToString("NA");
            emt_templatesubject = Convert.ToString("NA");
            emt_ccmail = Convert.ToString("NA");
            emt_bccmail = Convert.ToString("NA");
            emt_templatebody = Convert.ToString("NA");
            emt_templateflag = Convert.ToString("NA");
            emt_templatetype = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function
        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.emt_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailtemplateAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 emt_id)
        {
            Boolean blnResult = false;
            this.emt_id = emt_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailtemplateAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 emt_id)
        {
            Boolean blnResult = false;
            this.emt_id = emt_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailtemplateAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailtemplateGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByGroupID(Int64 intGroupID)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.emt_groupid = intGroupID;
                clsCMD.CommandText = "sp_emailtemplateGetData";
                SetGetSPFlag = "BYGROUPID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByType(String strTemplateType)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.emt_templatetype = strTemplateType;
                clsCMD.CommandText = "sp_emailtemplateGetData";
                SetGetSPFlag = "BYTYPE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByFlag(String strTemplateFlag)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.emt_templateflag = strTemplateFlag;
                clsCMD.CommandText = "sp_emailtemplateGetData";
                SetGetSPFlag = "BYTYPE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsEmailTemplate_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsEmailTemplate_PropertiesList> lstResult = new List<clsEmailTemplate_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].emt_id = Convert.ToInt64(dr["emt_id"]);
                lstResult[i].emt_groupid = Convert.ToInt64(dr["emt_groupid"]);
                lstResult[i].emt_mailaccountid = Convert.ToInt64(dr["emt_mailaccountid"]);
                lstResult[i].emt_templatename = Convert.ToString(dr["emt_templatename"]);
                lstResult[i].emt_templatesubject = Convert.ToString(dr["emt_templatesubject"]);
                lstResult[i].emt_ccmail = Convert.ToString(dr["emt_ccmail"]);
                lstResult[i].emt_bccmail = Convert.ToString(dr["emt_bccmail"]);
                lstResult[i].emt_templatebody = Convert.ToString(dr["emt_templatebody"]);
                lstResult[i].emt_templateflag = Convert.ToString(dr["emt_templateflag"]);
                lstResult[i].emt_templatetype = Convert.ToString(dr["emt_templatetype"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 emt_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.emt_id = emt_id;
                clsCMD.CommandText = "sp_emailtemplateGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByNameInProperties(String strTemplateName)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.emt_templatename = strTemplateName;
                clsCMD.CommandText = "sp_emailtemplateGetData";
                SetGetSPFlag = "BYNAME";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByFlagTypeInProperties(String strTemplateFlag, String strTemplateType)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.emt_templateflag = strTemplateFlag;
                this.emt_templatetype = strTemplateType;
                clsCMD.CommandText = "sp_emailtemplateGetData";
                SetGetSPFlag = "BYTYPEFLAG";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(String emt_templatename)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.emt_templatename = emt_templatename;
                clsCMD.CommandText = "sp_emailtemplateGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }
    public class clsEmailTemplate_PropertiesList
    {
        public Int64 emt_id { get; set; }
        public Int64 emt_groupid { get; set; }
        public Int64 emt_mailaccountid { get; set; }
        public String emt_templatename { get; set; }
        public String emt_templatesubject { get; set; }
        public String emt_ccmail { get; set; }
        public String emt_bccmail { get; set; }
        public String emt_templatebody { get; set; }
        public String emt_templateflag { get; set; }
        public String emt_templatetype { get; set; }
    }
}