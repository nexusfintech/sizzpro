﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsNewsLatter
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else
                {this.clsCon = clsSQLCon.SqlDBCon;
                    clsCMD.Connection = clsCon;}
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsNewsLatter()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsNewsLatter()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _nwl_id; }
            set
            {
                _nwl_id = value;
                if (clsCMD.Parameters.Contains("@nwl_id"))
                { clsCMD.Parameters["@nwl_id"].Value = value; }
                else { clsCMD.Parameters.Add("@nwl_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _nwl_id;
        public Int64 nwl_id
        {
            get { return _nwl_id; }
            set
            {
                _nwl_id = value;
                if (clsCMD.Parameters.Contains("@nwl_id"))
                { clsCMD.Parameters["@nwl_id"].Value = value; }
                else { clsCMD.Parameters.Add("@nwl_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _nwl_name;
        public String nwl_name
        {
            get { return _nwl_name; }
            set
            {
                _nwl_name = value;
                if (clsCMD.Parameters.Contains("@nwl_name"))
                { clsCMD.Parameters["@nwl_name"].Value = value; }
                else { clsCMD.Parameters.Add("@nwl_name", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _nwl_emailaddress;
        public String nwl_emailaddress
        {
            get { return _nwl_emailaddress; }
            set
            {
                _nwl_emailaddress = value;
                if (clsCMD.Parameters.Contains("@nwl_emailaddress"))
                { clsCMD.Parameters["@nwl_emailaddress"].Value = value; }
                else { clsCMD.Parameters.Add("@nwl_emailaddress", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private DateTime _nwl_datetime;
        public DateTime nwl_datetime
        {
            get { return _nwl_datetime; }
            set
            {
                _nwl_datetime = value;
                if (clsCMD.Parameters.Contains("@nwl_datetime"))
                { clsCMD.Parameters["@nwl_datetime"].Value = value; }
                else { clsCMD.Parameters.Add("@nwl_datetime", SqlDbType.DateTime).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                nwl_id = Convert.ToInt64(dr["nwl_id"]);
                nwl_name = Convert.ToString(dr["nwl_name"]);
                nwl_emailaddress = Convert.ToString(dr["nwl_emailaddress"]);
                nwl_datetime = Convert.ToDateTime(dr["nwl_datetime"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            nwl_id = Convert.ToInt64("0");
            nwl_name = Convert.ToString("NA");
            nwl_emailaddress = Convert.ToString("NA");
            nwl_datetime = Convert.ToDateTime(DateTime.Now);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.nwl_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "NewslatterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 nwl_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.nwl_id = nwl_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "NewslatterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 nwl_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.nwl_id = nwl_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "NewslatterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "NewslatterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsNewsLatter_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsNewsLatter_PropertiesList> lstResult = new List<clsNewsLatter_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].nwl_id = Convert.ToInt64(dr["nwl_id"]);
                lstResult[i].nwl_name = Convert.ToString(dr["nwl_name"]);
                lstResult[i].nwl_emailaddress = Convert.ToString(dr["nwl_emailaddress"]);
                lstResult[i].nwl_datetime = Convert.ToDateTime(dr["nwl_datetime"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 nwl_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.nwl_id = nwl_id;
                clsCMD.CommandText = "NewslatterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 nwl_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.nwl_id = nwl_id;
                clsCMD.CommandText = "NewslatterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }
    public class clsNewsLatter_PropertiesList
    {
        public Int64 nwl_id { get; set; }
        public String nwl_name { get; set; }
        public String nwl_emailaddress { get; set; }
        public DateTime nwl_datetime { get; set; }
    }
}