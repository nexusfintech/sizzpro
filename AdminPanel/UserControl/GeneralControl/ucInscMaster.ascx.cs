﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class UserControl_GeneralControl_ucInscMaster : System.Web.UI.UserControl
{
    clsInsuranceCompanyMaster clsICM = new clsInsuranceCompanyMaster();
    private Int64 intId = 0;
    private string strName = string.Empty;

    public Int64 SelectedValue
    {
        get { intId = Convert.ToInt64(ddlInscC.SelectedValue); return intId; }
        set { intId = value; }
    }

    public string SelectedName
    {
        get { strName = ddlInscC.SelectedItem.Text; return strName; }
        set { strName = value; }
    }

    public int Width
    {
        set { ddlInscC.Width = Unit.Pixel(value); }
    }

    public void Bind()
    {
        ddlInscC.DataSource = clsICM.GetAllRecord();
        ddlInscC.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}