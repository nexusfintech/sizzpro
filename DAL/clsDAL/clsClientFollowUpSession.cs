﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsClientFollowUpSession
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsClientFollowUpSession()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsClientFollowUpSession()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _cfs_id; }
            set
            {
                _cfs_id = value;
                if (clsCMD.Parameters.Contains("@cfs_id"))
                { clsCMD.Parameters["@cfs_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _cfs_id;
        public Int64 cfs_id
        {
            get { return _cfs_id; }
            set
            {
                _cfs_id = value;
                if (clsCMD.Parameters.Contains("@cfs_id"))
                { clsCMD.Parameters["@cfs_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _cfs_userid;
        public Guid cfs_userid
        {
            get { return _cfs_userid; }
            set
            {
                _cfs_userid = value;
                if (clsCMD.Parameters.Contains("@cfs_userid"))
                { clsCMD.Parameters["@cfs_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private DateTime _cfs_sessiondate;
        public DateTime cfs_sessiondate
        {
            get { return _cfs_sessiondate; }
            set
            {
                _cfs_sessiondate = value;
                if (clsCMD.Parameters.Contains("@cfs_sessiondate"))
                { clsCMD.Parameters["@cfs_sessiondate"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_sessiondate", SqlDbType.DateTime).Value = value; }
            }
        }
        private DateTime _cfs_nextappoinment;
        public DateTime cfs_nextappoinment
        {
            get { return _cfs_nextappoinment; }
            set
            {
                _cfs_nextappoinment = value;
                if (clsCMD.Parameters.Contains("@cfs_nextappoinment"))
                { clsCMD.Parameters["@cfs_nextappoinment"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_nextappoinment", SqlDbType.DateTime).Value = value; }
            }
        }
        private Int64 _cfs_pos;
        public Int64 cfs_pos
        {
            get { return _cfs_pos; }
            set
            {
                _cfs_pos = value;
                if (clsCMD.Parameters.Contains("@cfs_pos"))
                { clsCMD.Parameters["@cfs_pos"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_pos", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _cfs_facility;
        public Int64 cfs_facility
        {
            get { return _cfs_facility; }
            set
            {
                _cfs_facility = value;
                if (clsCMD.Parameters.Contains("@cfs_facility"))
                { clsCMD.Parameters["@cfs_facility"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_facility", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _cfs_payer;
        public Int64 cfs_payer
        {
            get { return _cfs_payer; }
            set
            {
                _cfs_payer = value;
                if (clsCMD.Parameters.Contains("@cfs_payer"))
                { clsCMD.Parameters["@cfs_payer"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_payer", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _cfs_provider;
        public Guid cfs_provider
        {
            get { return _cfs_provider; }
            set
            {
                _cfs_provider = value;
                if (clsCMD.Parameters.Contains("@cfs_provider"))
                { clsCMD.Parameters["@cfs_provider"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_provider", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _cfs_itempurchsed;
        public Int64 cfs_itempurchsed
        {
            get { return _cfs_itempurchsed; }
            set
            {
                _cfs_itempurchsed = value;
                if (clsCMD.Parameters.Contains("@cfs_itempurchsed"))
                { clsCMD.Parameters["@cfs_itempurchsed"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_itempurchsed", SqlDbType.BigInt).Value = value; }
            }
        }
        private Double _cfs_itemcharge;
        public Double cfs_itemcharge
        {
            get { return _cfs_itemcharge; }
            set
            {
                _cfs_itemcharge = value;
                if (clsCMD.Parameters.Contains("@cfs_itemcharge"))
                { clsCMD.Parameters["@cfs_itemcharge"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_itemcharge", SqlDbType.Money).Value = value; }
            }
        }
        private DateTime _cfs_intime;
        public DateTime cfs_intime
        {
            get { return _cfs_intime; }
            set
            {
                _cfs_intime = value;
                if (clsCMD.Parameters.Contains("@cfs_intime"))
                { clsCMD.Parameters["@cfs_intime"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_intime", SqlDbType.DateTime).Value = value; }
            }
        }
        private DateTime _cfs_outtime;
        public DateTime cfs_outtime
        {
            get { return _cfs_outtime; }
            set
            {
                _cfs_outtime = value;
                if (clsCMD.Parameters.Contains("@cfs_outtime"))
                { clsCMD.Parameters["@cfs_outtime"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_outtime", SqlDbType.DateTime).Value = value; }
            }
        }
        private Int64 _cfs_totalhrs;
        public Int64 cfs_totalhrs
        {
            get { return _cfs_totalhrs; }
            set
            {
                _cfs_totalhrs = value;
                if (clsCMD.Parameters.Contains("@cfs_totalhrs"))
                { clsCMD.Parameters["@cfs_totalhrs"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_totalhrs", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _cfs_show;
        public Int64 cfs_show
        {
            get { return _cfs_show; }
            set
            {
                _cfs_show = value;
                if (clsCMD.Parameters.Contains("@cfs_show"))
                { clsCMD.Parameters["@cfs_show"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_show", SqlDbType.BigInt).Value = value; }
            }
        }

        private Guid _cfs_sessionid;
        public Guid cfs_sessionid
        {
            get { return _cfs_sessionid; }
            set
            {
                _cfs_sessionid = value;
                if (clsCMD.Parameters.Contains("@cfs_sessionid"))
                { clsCMD.Parameters["@cfs_sessionid"].Value = value; }
                else { clsCMD.Parameters.Add("@cfs_sessionid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                cfs_id = Convert.ToInt64(dr["cfs_id"]);
                cfs_userid = new Guid(dr["cfs_userid"].ToString());
                cfs_sessiondate = Convert.ToDateTime(dr["cfs_sessiondate"]);
                cfs_nextappoinment = Convert.ToDateTime(dr["cfs_nextappoinment"]);
                cfs_pos = Convert.ToInt64(dr["cfs_pos"]);
                cfs_facility = Convert.ToInt64(dr["cfs_facility"]);
                cfs_payer = Convert.ToInt64(dr["cfs_payer"]);
                cfs_provider = new Guid(dr["cfs_provider"].ToString());
                cfs_itempurchsed = Convert.ToInt64(dr["cfs_itempurchsed"]);
                cfs_itemcharge = Convert.ToDouble(dr["cfs_itemcharge"]);
                cfs_intime = Convert.ToDateTime(dr["cfs_intime"]);
                cfs_outtime = Convert.ToDateTime(dr["cfs_outtime"]);
                cfs_totalhrs = Convert.ToInt64(dr["cfs_totalhrs"]);
                cfs_show = Convert.ToInt64(dr["cfs_show"]);
                cfs_sessionid = new Guid(dr["cfs_sessionid"].ToString());
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            cfs_id = Convert.ToInt64("0");
            cfs_userid = new Guid();
            cfs_sessiondate = Convert.ToDateTime(DateTime.Now);
            cfs_nextappoinment = Convert.ToDateTime(DateTime.Now);
            cfs_pos = Convert.ToInt64("0");
            cfs_facility = Convert.ToInt64("0");
            cfs_payer = Convert.ToInt64("0");
            cfs_provider = new Guid();
            cfs_itempurchsed = Convert.ToInt64("0");
            cfs_itemcharge = Convert.ToDouble("0");
            cfs_intime = Convert.ToDateTime(DateTime.Now);
            cfs_outtime = Convert.ToDateTime(DateTime.Now);
            cfs_totalhrs = Convert.ToInt64("0");
            cfs_show = Convert.ToInt64("0");
            cfs_sessionid = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.cfs_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_ClientFollowUpSessionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                {
                    blnResult = true;
                    clsCMD.CommandText = "Select @@identity from tbl_ClientFollowUpSession";
                    clsCMD.CommandType = CommandType.Text;
                    this.PrimeryKey = Convert.ToInt64(clsCMD.ExecuteScalar());
                    GetSetErrorMessage = "Entry Saved Successfully."; 
                }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 cfs_id)
        {
            Boolean blnResult = false;
            this.cfs_id = cfs_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_ClientFollowUpSessionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 cfs_id)
        {
            Boolean blnResult = false;
            this.cfs_id = cfs_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_ClientFollowUpSessionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_ClientFollowUpSessionGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByClient(Guid clientid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.cfs_userid = clientid;
                clsCMD.CommandText = "sp_ClientFollowUpSessionGetData";
                SetGetSPFlag = "BYCLIENT";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordBySession(Guid Sessionid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.cfs_sessionid = Sessionid;
                clsCMD.CommandText = "sp_ClientFollowUpSessionGetData";
                SetGetSPFlag = "BYSESSIONID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsClientFollowUpSession_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsClientFollowUpSession_PropertiesList> lstResult = new List<clsClientFollowUpSession_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].cfs_id = Convert.ToInt64(dr["cfs_id"]);
                lstResult[i].cfs_userid = new Guid(dr["cfs_userid"].ToString());
                lstResult[i].cfs_sessiondate = Convert.ToDateTime(dr["cfs_sessiondate"]);
                lstResult[i].cfs_nextappoinment = Convert.ToDateTime(dr["cfs_nextappoinment"]);
                lstResult[i].cfs_pos = Convert.ToInt64(dr["cfs_pos"]);
                lstResult[i].cfs_facility = Convert.ToInt64(dr["cfs_facility"]);
                lstResult[i].cfs_payer = Convert.ToInt64(dr["cfs_payer"]);
                lstResult[i].cfs_provider = new Guid(dr["cfs_provider"].ToString());
                lstResult[i].cfs_itempurchsed = Convert.ToInt64(dr["cfs_itempurchsed"]);
                lstResult[i].cfs_itemcharge = Convert.ToDouble(dr["cfs_itemcharge"]);
                lstResult[i].cfs_intime = Convert.ToDateTime(dr["cfs_intime"]);
                lstResult[i].cfs_outtime = Convert.ToDateTime(dr["cfs_outtime"]);
                lstResult[i].cfs_totalhrs = Convert.ToInt64(dr["cfs_totalhrs"]);
                lstResult[i].cfs_show = Convert.ToInt64(dr["cfs_show"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 cfs_id)
        {
            Boolean blnResult = false;
            try
            {
                this.cfs_id = cfs_id;
                clsCMD.CommandText = "sp_ClientFollowUpSessionGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 cfs_id)
        {
            Boolean blnResult = false;
            try
            {
                this.cfs_id = cfs_id;
                clsCMD.CommandText = "sp_ClientFollowUpSessionGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsClientFollowUpSession_PropertiesList
    {
        public Int64 cfs_id { get; set; }
        public Guid cfs_userid { get; set; }
        public DateTime cfs_sessiondate { get; set; }
        public DateTime cfs_nextappoinment { get; set; }
        public Int64 cfs_pos { get; set; }
        public Int64 cfs_facility { get; set; }
        public Int64 cfs_payer { get; set; }
        public Guid cfs_provider { get; set; }
        public Int64 cfs_itempurchsed { get; set; }
        public Double cfs_itemcharge { get; set; }
        public DateTime cfs_intime { get; set; }
        public DateTime cfs_outtime { get; set; }
        public Int64 cfs_totalhrs { get; set; }
        public Int64 cfs_show { get; set; }
        public Guid cfs_sessionid { get; set; }
    }
}