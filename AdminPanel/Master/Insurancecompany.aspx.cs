﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_Insurancecompany : System.Web.UI.Page
{
    clsAdmininsurancecompanyMapping clsAIM = new clsAdmininsurancecompanyMapping();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptCmpny.DataSource = clsAIM.GetAllRecordbyAdmin((Guid)Membership.GetUser().ProviderUserKey);
        rptCmpny.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Master/InsurancecompanyAddEdit.aspx");
    }
    protected void rptCmpny_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if(e.CommandName=="EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/InsurancecompanyAddEdit.aspx");    
        }
        if(e.CommandName == "DELETE")
        {
            Boolean blnRslt = clsAIM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (blnRslt)
            {
                MySession.Current.MESSAGE = "Record Deleted Sucessfully";
                Response.Redirect("~/Master/Insurancecompany.aspx");
            }
            else
            {
                lblmessage.Text = clsAIM.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
}