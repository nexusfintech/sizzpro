﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class UserControl_GeneralControl_ucPosMaster : System.Web.UI.UserControl
{
    clsPlaceofServicesMaster clsPSM = new clsPlaceofServicesMaster();

    private Int64 intId = 0;
    private string strName =string.Empty;

    public Int64 SelectedValue
    {
        get { intId = Convert.ToInt64(ddlpos.SelectedValue); return intId; }
        set { intId = value; }
    }

    public string SelectedName
    {
        get { strName = ddlpos.SelectedItem.Text; return strName; }
        set { strName = value; }
    }

    public int Width
    {
        set { ddlpos.Width = Unit.Pixel(value); }
    }

    public void Bind()
    {
        ddlpos.DataSource = clsPSM.GetAllRecord();
        ddlpos.DataBind();

        ListItem itm = ddlpos.Items.FindByValue("11");
        if (itm != null)
        {
            ddlpos.SelectedIndex = ddlpos.Items.IndexOf(itm);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}