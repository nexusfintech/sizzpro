﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientIntialAssessment.aspx.cs" Inherits="Clients_ClientIntialAssessment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#lnkYou").click(function () {
                alert("Handler for .click() called.");
                return false;
            });
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <UC:AssessmentStepLinks ID="ucasslinks" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-group"></i>
                Initial Assessment
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:UserProfile ID="ucProfile" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

