﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;

public partial class Subscription_ServicesMaster : System.Web.UI.Page
{
    clssubscriptionservices clsSSR = new clssubscriptionservices();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            binddata();
        }
    }

    private void binddata()
    {
        dtServices.DataSource = clsSSR.GetAllRecord() ;
        dtServices.DataBind();
    }

    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Subscription/PackageAddEdit.aspx");
    }
    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = (LinkButton)sender;
        if (lnkbtn.CommandName == "MODULES")
        {
            Session["serviceid"] = lnkbtn.CommandArgument;
            Response.Redirect("~/Subscription/ServiceModules.aspx");
        }
        else
        {
            //EDIT
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(lnkbtn.CommandArgument);
            Response.Redirect("~/Subscription/PackageAddEdit.aspx");
        }
    }
}