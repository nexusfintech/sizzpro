﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdminRegistration.aspx.cs" Inherits="Users_UserRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeftBar" runat="server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkManagement" runat="server"><i class="icon-retweet"></i>&nbsp;Administrator Management</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-bordered box-color">
        <div class="box-title">
            <h3><i class="icon-edit"></i>Administrator Creation</h3>
        </div>
        <div class="box-content nopadding">
            <div class="form-horizontal form-bordered">
                <div class="control-group">
                    <label for="txtFirstname" class="control-label">First Name</label>
                    <div class="controls">
                        <asp:TextBox ID="txtFirstname" runat="server" CssClass="input-xlarge"></asp:TextBox>
                    </div>
                </div>
                <%--<div class="control-group">
                    <label for="txtMiddleName" class="control-label">Middle Name</label>
                    <div class="controls">
                        <asp:TextBox ID="txtMiddleName" runat="server" CssClass="input-xlarge"></asp:TextBox>
                    </div>
                </div>--%>
                <div class="control-group">
                    <label for="txtlastname" class="control-label">Last Name</label>
                    <div class="controls">
                        <asp:TextBox ID="txtlastname" runat="server" CssClass="input-xlarge"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label for="password" class="control-label">Email</label>
                    <div class="controls">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="input-xlarge"></asp:TextBox>
                        <span class="help-block">This email id will be used for account verification process and other important notifications.</span>
                    </div>
                </div>
                <div class="form-actions">
                    <asp:Button ID="cmdCreateUser" runat="server" CssClass="btn btn-primary" Text="Create User" OnClick="cmdCreateUser_Click" />
                </div>
                <asp:Label ID="lblMessage" runat="server" class="input-block-level" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>