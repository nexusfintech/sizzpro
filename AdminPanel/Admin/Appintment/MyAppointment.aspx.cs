﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class Admin_Appintment_MyAppointment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hduserid.Value = Membership.GetUser().ProviderUserKey.ToString();
    }

    protected void cmdNewApointment_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewAppointment.aspx");
    }
}