﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsFollowupSoap
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsFollowupSoap()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsFollowupSoap()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _fsoap_id; }
            set
            {
                _fsoap_id = value;
                if (clsCMD.Parameters.Contains("@fsoap_id"))
                { clsCMD.Parameters["@fsoap_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fsoap_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _fsoap_id;
        public Int64 fsoap_id
        {
            get { return _fsoap_id; }
            set
            {
                _fsoap_id = value;
                if (clsCMD.Parameters.Contains("@fsoap_id"))
                { clsCMD.Parameters["@fsoap_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fsoap_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _fsoap_sessionid;
        public Guid fsoap_sessionid
        {
            get { return _fsoap_sessionid; }
            set
            {
                _fsoap_sessionid = value;
                if (clsCMD.Parameters.Contains("@fsoap_sessionid"))
                { clsCMD.Parameters["@fsoap_sessionid"].Value = value; }
                else { clsCMD.Parameters.Add("@fsoap_sessionid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _fsoap_clientid;
        public Guid fsoap_clientid
        {
            get { return _fsoap_clientid; }
            set
            {
                _fsoap_clientid = value;
                if (clsCMD.Parameters.Contains("@fsoap_clientid"))
                { clsCMD.Parameters["@fsoap_clientid"].Value = value; }
                else { clsCMD.Parameters.Add("@fsoap_clientid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _s_reason;
        public String s_reason
        {
            get { return _s_reason; }
            set
            {
                _s_reason = value;
                if (clsCMD.Parameters.Contains("@s_reason"))
                { clsCMD.Parameters["@s_reason"].Value = value; }
                else { clsCMD.Parameters.Add("@s_reason", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _o_behaviour;
        public String o_behaviour
        {
            get { return _o_behaviour; }
            set
            {
                _o_behaviour = value;
                if (clsCMD.Parameters.Contains("@o_behaviour"))
                { clsCMD.Parameters["@o_behaviour"].Value = value; }
                else { clsCMD.Parameters.Add("@o_behaviour", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _o_reasponse;
        public String o_reasponse
        {
            get { return _o_reasponse; }
            set
            {
                _o_reasponse = value;
                if (clsCMD.Parameters.Contains("@o_reasponse"))
                { clsCMD.Parameters["@o_reasponse"].Value = value; }
                else { clsCMD.Parameters.Add("@o_reasponse", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _a_intervatation;
        public String a_intervatation
        {
            get { return _a_intervatation; }
            set
            {
                _a_intervatation = value;
                if (clsCMD.Parameters.Contains("@a_intervatation"))
                { clsCMD.Parameters["@a_intervatation"].Value = value; }
                else { clsCMD.Parameters.Add("@a_intervatation", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _p_treatmentplan;
        public String p_treatmentplan
        {
            get { return _p_treatmentplan; }
            set
            {
                _p_treatmentplan = value;
                if (clsCMD.Parameters.Contains("@p_treatmentplan"))
                { clsCMD.Parameters["@p_treatmentplan"].Value = value; }
                else { clsCMD.Parameters.Add("@p_treatmentplan", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _p_progresstogoal;
        public String p_progresstogoal
        {
            get { return _p_progresstogoal; }
            set
            {
                _p_progresstogoal = value;
                if (clsCMD.Parameters.Contains("@p_progresstogoal"))
                { clsCMD.Parameters["@p_progresstogoal"].Value = value; }
                else { clsCMD.Parameters.Add("@p_progresstogoal", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _p_newclinicissue;
        public String p_newclinicissue
        {
            get { return _p_newclinicissue; }
            set
            {
                _p_newclinicissue = value;
                if (clsCMD.Parameters.Contains("@p_newclinicissue"))
                { clsCMD.Parameters["@p_newclinicissue"].Value = value; }
                else { clsCMD.Parameters.Add("@p_newclinicissue", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _p_nextvisitplan;
        public String p_nextvisitplan
        {
            get { return _p_nextvisitplan; }
            set
            {
                _p_nextvisitplan = value;
                if (clsCMD.Parameters.Contains("@p_nextvisitplan"))
                { clsCMD.Parameters["@p_nextvisitplan"].Value = value; }
                else { clsCMD.Parameters.Add("@p_nextvisitplan", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _o_present;
        public String o_present
        {
            get { return _o_present; }
            set
            {
                _o_present = value;
                if (clsCMD.Parameters.Contains("@o_present"))
                { clsCMD.Parameters["@o_present"].Value = value; }
                else { clsCMD.Parameters.Add("@o_present", SqlDbType.NVarChar).Value = value; }
            }
        }
        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                fsoap_id = Convert.ToInt64(dr["fsoap_id"]);
                fsoap_sessionid = new Guid(dr["fsoap_sessionid"].ToString());
                fsoap_clientid = new Guid(dr["fsoap_clientid"].ToString());
                s_reason = Convert.ToString(dr["s_reason"]);
                o_behaviour = Convert.ToString(dr["o_behaviour"]);
                o_reasponse = Convert.ToString(dr["o_reasponse"]);
                a_intervatation = Convert.ToString(dr["a_intervatation"]);
                p_treatmentplan = Convert.ToString(dr["p_treatmentplan"]);
                p_progresstogoal = Convert.ToString(dr["p_progresstogoal"]);
                p_newclinicissue = Convert.ToString(dr["p_newclinicissue"]);
                p_nextvisitplan = Convert.ToString(dr["p_nextvisitplan"]);
                o_present=Convert.ToString(dr["o_present"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            fsoap_id = Convert.ToInt64("0");
            fsoap_sessionid = new Guid();
            fsoap_clientid = new Guid();
            s_reason = Convert.ToString("NA");
            o_behaviour = Convert.ToString("NA");
            o_reasponse = Convert.ToString("NA");
            a_intervatation = Convert.ToString("NA");
            p_treatmentplan = Convert.ToString("NA");
            p_progresstogoal = Convert.ToString("NA");
            p_newclinicissue = Convert.ToString("NA");
            p_nextvisitplan = Convert.ToString("NA");
            o_present = String.Empty;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.fsoap_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_FollowupSoapAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 fsoap_id)
        {
            Boolean blnResult = false;
            this.fsoap_id = fsoap_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_FollowupSoapAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 fsoap_id)
        {
            Boolean blnResult = false;
            this.fsoap_id = fsoap_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_FollowupSoapAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_FollowupSoapGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public Boolean GetRecordBySession(Guid Sessionid)
        {
            Boolean blnResult = false;
            try
            {
                this.fsoap_sessionid = Sessionid;
                clsCMD.CommandText = "sp_FollowupSoapGetData";
                SetGetSPFlag = "BYSESSION";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public List<clsFollowupSoap_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsFollowupSoap_PropertiesList> lstResult = new List<clsFollowupSoap_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].fsoap_id = Convert.ToInt64(dr["fsoap_id"]);
                lstResult[i].fsoap_sessionid = new Guid(dr["fsoap_sessionid"].ToString());
                lstResult[i].fsoap_clientid = new Guid(dr["fsoap_clientid"].ToString());
                lstResult[i].s_reason = Convert.ToString(dr["s_reason"]);
                lstResult[i].o_behaviour = Convert.ToString(dr["o_behaviour"]);
                lstResult[i].o_reasponse = Convert.ToString(dr["o_reasponse"]);
                lstResult[i].a_intervatation = Convert.ToString(dr["a_intervatation"]);
                lstResult[i].p_treatmentplan = Convert.ToString(dr["p_treatmentplan"]);
                lstResult[i].p_progresstogoal = Convert.ToString(dr["p_progresstogoal"]);
                lstResult[i].p_newclinicissue = Convert.ToString(dr["p_newclinicissue"]);
                lstResult[i].p_nextvisitplan = Convert.ToString(dr["p_nextvisitplan"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 fsoap_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fsoap_id = fsoap_id;
                clsCMD.CommandText = "sp_FollowupSoapGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 fsoap_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fsoap_id = fsoap_id;
                clsCMD.CommandText = "sp_FollowupSoapGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsFollowupSoap_PropertiesList
    {
        public Int64 fsoap_id { get; set; }
        public Guid fsoap_sessionid { get; set; }
        public Guid fsoap_clientid { get; set; }
        public String s_reason { get; set; }
        public String o_behaviour { get; set; }
        public String o_reasponse { get; set; }
        public String a_intervatation { get; set; }
        public String p_treatmentplan { get; set; }
        public String p_progresstogoal { get; set; }
        public String p_newclinicissue { get; set; }
        public String p_nextvisitplan { get; set; }
        public String o_present { get; set; }
    }
}