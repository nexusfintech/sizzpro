﻿<%@ Page Title="Couples-Counselling" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="couples-counselling.aspx.cs" Inherits="couples_counselling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-question"></i>&nbsp;Couples-Counselling</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <img src="img/angry_couple.png" />
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h4>COUPLES COUNSELING</h4>
            <p>
                Couples therapy, helps couples  whether married or not to understand and resolve conflicts and improve their relationship. Couples therapy gives couples the tools to communicate better, negotiate differences, problem solve and even argue in a healthier way.<br />
                Couples therapy, or marriage counseling, is generally provided by licensed therapists known as marriage and family therapists. These therapists provide the same mental health services as other therapists, but with a specific focus &mdash; a couple's relationship.
            </p>
            <p>
                Couples therapy can be short term. You may need only a few sessions to help you weather a crisis. Or you may need couples therapy for several months, particularly if your relationship has greatly deteriorated. As with individual psychotherapy, you typically see a marriage counselor or therapist once a week.
            </p>
            <p>
                Illness, infidelity, sex, anger, communication problems &mdash; all can contribute to distress in marriages or other relationships. Marriage counseling or couples counseling can help resolve conflicts and heal wounds.<br>
                <br />
                Your partner comes home from work, makes a beeline for the liquor cabinet and then sulks off silently. You haven't had a real conversation for weeks. A few arguments over money or late nights out, sure, but no heart-to-hearts. Sex? What's that?
            </p>
            <p>Your relationship is on the rocks, and you both know it. But you aren't sure how to fix things &mdash; or if you really want to.</p>
            <p>It may be time for couples therapy or marriage counseling. Couples therapy can help you rebuild your relationship. Or decide that you'll both be better off if you split up. Either way, couples therapy can help you understand your relationship better and make well-thought-out decisions.</p>
            <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

