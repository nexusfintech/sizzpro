﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSibling.ascx.cs" Inherits="UserControl_ClientProfile_ucSibling" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-user"></i>
                    Sibling
                </h3>
            </div>
            <div class="box-content nopadding">
                <div class='form-horizontal form-column form-bordered'>

                    <div class="control-group">
                        <label for="txtFirstName" class="control-label">First Name</label>
                        <div class="controls">
                            <asp:TextBox ID="txtFirstName" runat="server" placeholder="Firstname" data-rule-required="true" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtMiddlename" class="control-label">Middle Name</label>
                        <div class="controls">
                            <asp:TextBox ID="txtMiddlename" runat="server" placeholder="Middlename" data-rule-required="true" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtlastname" class="control-label">Last Name</label>
                        <div class="controls">
                            <asp:TextBox ID="txtlastname" runat="server" placeholder="Last Name" data-rule-required="true" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                                        <div class="control-group">
                        <label for="txtAge" class="control-label">Age</label>
                        <div class="controls">
                            <asp:TextBox ID="txtAge" runat="server" placeholder="18 Years" data-rule-required="true" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="chklevewithclient" class="control-label">Gender</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlGender" runat="server">
                                <asp:ListItem Text="Male"></asp:ListItem>
                                <asp:ListItem Text="Female"></asp:ListItem>
                                <asp:ListItem Text="TransGender"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    
                    <div class="control-group">
                        <label for="txtlastname" class="control-label"></label>
                        <div class="controls">
                            <div class="check-line">
                                <asp:CheckBox ID="chkLive" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                                <label class='inline'>Lives With Client?</label>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="password" class="control-label">Description</label>
                        <div class="controls">
                            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" placeholder="Description" Width="98%" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-actions">
                        <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <asp:Repeater ID="dtSiblingDisp" runat="server" OnItemCommand="dtSiblingDisp_ItemCommand">
            <HeaderTemplate>
                <table class="table table-hover table-nomargin dataTable table-bordered">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th>Gender</th>
                            <th>Age</th>
                            <th>Live With Client</th>
                            <th>Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("csb_firstname") %></td>
                    <td><%# Eval("csb_middlename") %></td>
                    <td><%# Eval("csb_lastname") %></td>
                    <td><%# Eval("csb_gender") %></td>
                    <td><%# Eval("csb_age") %></td>
                    <td><%# Eval("csb_livewithclient") %></td>
                    <td><%# Eval("csb_description") %></td>
                    <td>
                        <i class="icon-edit">
                            <asp:Button ID="cmdEdit" runat="server" Text="Edit" CssClass="btn cancel" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "csb_id") %>' />
                        </i>
                    </td>
                    <td>
                        <i class="icon-remove">
                            <asp:Button ID="cmdDelete" runat="server" Text="Delete" CssClass="btn cancel" CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "csb_id") %>' />
                        </i>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
