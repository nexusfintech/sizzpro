﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class registerbussiness : System.Web.UI.Page
{
    clsEmailAccounts clsMailAccount = new clsEmailAccounts();
    clsProfileMaster clsProfile = new clsProfileMaster();
    clsEmailTemplate clsMailTemplate = new clsEmailTemplate();
    clsSendMail clsMailSend = new clsSendMail();
    clsProfessionMaster clsPRO = new clsProfessionMaster();
    clsSettings clsSTG = new clsSettings();
    clsuserproffessionmapping clsUPM = new clsuserproffessionmapping();
    public string redirect = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string validate = validatepage();
        if (validate != string.Empty)
        {
            lblmessage.Text = validate;
            return;
        }
        MembershipCreateStatus memstatus;
        MembershipUser memuser = Membership.CreateUser(txtusername.Text.Trim(), txtpassword.Text.ToString(), txtusername.Text.Trim(), "admin@sizzpro", "nexus@admin", true, out memstatus);
        if (memuser == null)
        {
            lblmessage.Text = GetErrorMessage(memstatus);
            return;
        }
        else
        {
            String[] strRoles = new String[1];
            strRoles[0] = "Member";
            Roles.AddUserToRoles(txtusername.Text.Trim(), strRoles);
            MembershipUser mu = Membership.GetUser(txtusername.Text.Trim());
            Guid userguid = (Guid)mu.ProviderUserKey;
            Boolean blnTemplate = clsMailTemplate.GetRecordByNameInProperties("UserCreateWelcomeMail");
            if (blnTemplate == true)
            {
                Boolean blnMailAccount = clsMailAccount.GetRecordByIDInProperties(clsMailTemplate.emt_mailaccountid);
                if (blnMailAccount == true)
                {
                    Boolean blnAccountBind = clsMailSend.BindEmailAccount(clsMailAccount.ema_emailaddress);
                    if (blnAccountBind == true)
                    {
                        String strGUID = Guid.NewGuid().ToString();
                        String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                        String strhost = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                        //string strlink = strhost + "AdminPanel/users/UserMessage.aspx?message=activation&tokenid=" + strGUID;
                        string strlink = strhost + "Users/UserMessage.aspx?message=activation&tokenid=" + strGUID;
                        clsMailSend.ToMailAddress = txtusername.Text.Trim();
                        clsMailSend.Subject = clsMailTemplate.emt_templatesubject;
                        if (clsMailTemplate.emt_ccmail.Trim() != "") { clsMailSend.CCMailAddress = clsMailTemplate.emt_ccmail.Trim(); }
                        if (clsMailTemplate.emt_bccmail.Trim() != "") { clsMailSend.BCCMailAddress = clsMailTemplate.emt_bccmail.Trim(); }
                        String strHtmlBody = clsMailTemplate.emt_templatebody.Trim();
                        strHtmlBody = strHtmlBody.Replace("{username}", txtusername.Text.Trim());
                        strHtmlBody = strHtmlBody.Replace("{password}", txtpassword.Text.ToString());
                        strHtmlBody = strHtmlBody.Replace("{activationlink}", strlink);
                        clsMailSend.HtmlBody = strHtmlBody;
                        clsMailSend.Priority = System.Net.Mail.MailPriority.Normal;
                        Boolean blnSendMail = clsMailSend.SendMail();
                        if (blnSendMail == true)
                        {
                            //clsSTG.stg_settingflag = "PROFESSION";
                            //clsSTG.stg_intvalue = Convert.ToInt64(ddlProfession.SelectedItem.Value);
                            //clsSTG.Save(userguid);
                            //clsUPM.prfm_userid = userguid;
                            //clsUPM.prfm_memberalias = "NA";
                            //clsUPM.prfm_useralias = "User";
                            //clsUPM.prfm_clientalias = "Client";
                            //clsUPM.Save();

                            clsUsers clsUsr = new clsUsers();
                            Boolean blnUsr = clsUsr.GetRecordByIDInProperties(userguid);
                            if (blnUsr == true)
                            {
                                clsProfile.prm_user_Id = clsUsr.User_Id;
                                clsProfile.prm_emailid = txtusername.Text.Trim();
                                clsProfile.prm_passwordreset = true;
                                clsProfile.prm_userId = userguid.ToString();
                                clsProfile.prm_emailverify = false;
                                clsProfile.prm_passwordreset = true;
                                clsProfile.prm_userrole = "Member";
                                clsProfile.prm_tokenid = strGUID;
                                clsProfile.prm_tokendatetime = DateTime.Now;
                                clsProfile.prm_firstname = txtfirstname.Text;
                                clsProfile.prm_lastname = txtlastname.Text;
                                Boolean blnProfile = clsProfile.Save();

                                AddUserParentMapping(userguid);

                                if (blnProfile == true)
                                {
                                    if(Session["cart"] != null)
                                    {
                                        Response.Redirect("~/processpayment.aspx?ui="+ userguid.ToString());
                                    }
                                    else
                                    {
                                        Response.Redirect("~/message.aspx?mt=newuserregister");
                                    }
                                }
                                else
                                {
                                    lblmessage.Text = "Error in user profile creation.";
                                }
                            }
                            else { lblmessage.Text = "Error in getting user primary key."; }
                        }
                        else { lblmessage.Text = "Error in sending welcome mail."; }
                    }
                    else { lblmessage.Text = "Error in Mail Account's Setting Bind To Mail Sender Engine"; }
                }
                else { lblmessage.Text = "Invalid Mail Account Binding In Email Template"; }
            }
            else { lblmessage.Text = "User Create Successfully. but error in Welcome Mail Sending.. please resend manualy"; }
        }
    }


    private string validatepage()
    {
        string validate = string.Empty;
        if (txtfirstname.Text.ToString() == string.Empty)
        {
            validate = "FirstName is required";
            return validate;
        }
        if (txtlastname.Text.ToString() == string.Empty)
        {
            validate = "LasttName is required";
            return validate;
        }
        if (txtusername.Text.ToString() == string.Empty)
        {
            validate = "UserName is required";
            return validate;
        }
        if (txtpassword.Text.ToString() == string.Empty)
        {
            validate = "password is required";
            return validate;
        }
        if (txtrepass.Text.ToString() == string.Empty)
        {
            validate = "Re-enter same password";
            return validate;
        }
        if (txtpassword.Text.ToString() != txtrepass.Text.ToString())
        {
            validate = "password doesnot match";
            return validate;
        }
        return validate;
    }

    public string GetErrorMessage(MembershipCreateStatus status)
    {
        switch (status)
        {
            case MembershipCreateStatus.DuplicateUserName:
                return "Username already exists. Please enter a different user name.";

            case MembershipCreateStatus.DuplicateEmail:
                return "A username for that e-mail address already exists. Please enter a different e-mail address.";

            case MembershipCreateStatus.InvalidPassword:
                return "The password provided is invalid. Please enter a valid password value.";

            case MembershipCreateStatus.InvalidEmail:
                return "The e-mail address provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidAnswer:
                return "The password retrieval answer provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidQuestion:
                return "The password retrieval question provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidUserName:
                return "The user name provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.ProviderError:
                return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            case MembershipCreateStatus.UserRejected:
                return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            default:
                return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
        }
    }

    private void AddUserParentMapping(Guid guUserid)
    {
        clsClientParentMapping clsMap = new clsClientParentMapping();
        clsUsers clsUsr = new clsUsers();

        clsMap.cpm_parentid = new Guid(Membership.GetUser("administrator").ProviderUserKey.ToString());
        clsMap.cpm_userid = guUserid;
        clsMap.Save();
    }
}