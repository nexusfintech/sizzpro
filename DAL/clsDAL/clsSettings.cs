﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsSettings
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsSettings()
        {
            clsCMD.Parameters.Clear();
            //clsConnection clsSQLCon = new clsConnection();
            //if (!clsSQLCon.GetConnection())
            //{ GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            //else { this.clsCon = clsSQLCon.SqlDBCon; }
            //if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsSettings()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _stg_id; }
            set
            {
                _stg_id = value;
                if (clsCMD.Parameters.Contains("@stg_id"))
                { clsCMD.Parameters["@stg_id"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _stg_id;
        public Int64 stg_id
        {
            get { return _stg_id; }
            set
            {
                _stg_id = value;
                if (clsCMD.Parameters.Contains("@stg_id"))
                { clsCMD.Parameters["@stg_id"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _stg_settingflag;
        public String stg_settingflag
        {
            get { return _stg_settingflag; }
            set
            {
                _stg_settingflag = value;
                if (clsCMD.Parameters.Contains("@stg_settingflag"))
                { clsCMD.Parameters["@stg_settingflag"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_settingflag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _stg_stingvalue;
        public String stg_stingvalue
        {
            get { return _stg_stingvalue; }
            set
            {
                _stg_stingvalue = value;
                if (clsCMD.Parameters.Contains("@stg_stingvalue"))
                { clsCMD.Parameters["@stg_stingvalue"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_stingvalue", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _stg_stringmaxvalue;
        public String stg_stringmaxvalue
        {
            get { return _stg_stringmaxvalue; }
            set
            {
                _stg_stringmaxvalue = value;
                if (clsCMD.Parameters.Contains("@stg_stringmaxvalue"))
                { clsCMD.Parameters["@stg_stringmaxvalue"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_stringmaxvalue", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private DateTime _stg_datetimevalue;
        public DateTime stg_datetimevalue
        {
            get { return _stg_datetimevalue; }
            set
            {
                _stg_datetimevalue = value;
                if (clsCMD.Parameters.Contains("@stg_datetimevalue"))
                { clsCMD.Parameters["@stg_datetimevalue"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_datetimevalue", SqlDbType.DateTime).Value = value; }
            }
        }
        private Int64 _stg_intvalue;
        public Int64 stg_intvalue
        {
            get { return _stg_intvalue; }
            set
            {
                _stg_intvalue = value;
                if (clsCMD.Parameters.Contains("@stg_intvalue"))
                { clsCMD.Parameters["@stg_intvalue"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_intvalue", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _stg_doublevalue;
        public Int64 stg_doublevalue
        {
            get { return _stg_doublevalue; }
            set
            {
                _stg_doublevalue = value;
                if (clsCMD.Parameters.Contains("@stg_doublevalue"))
                { clsCMD.Parameters["@stg_doublevalue"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_doublevalue", SqlDbType.Int).Value = value; }
            }
        }
        private Double _stg_moneyvalue;
        public Double stg_moneyvalue
        {
            get { return _stg_moneyvalue; }
            set
            {
                _stg_moneyvalue = value;
                if (clsCMD.Parameters.Contains("@stg_moneyvalue"))
                { clsCMD.Parameters["@stg_moneyvalue"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_moneyvalue", SqlDbType.Money).Value = value; }
            }
        }
        private Int64 _stg_boolevalue;
        public Int64 stg_boolevalue
        {
            get { return _stg_boolevalue; }
            set
            {
                _stg_boolevalue = value;
                if (clsCMD.Parameters.Contains("@stg_boolevalue"))
                { clsCMD.Parameters["@stg_boolevalue"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_boolevalue", SqlDbType.BigInt).Value = value; }
            }
        }

        private Guid _stg_userid;
        public Guid stg_userid
        {
            get { return _stg_userid; }
            set
            {
                _stg_userid = value;
                if (clsCMD.Parameters.Contains("@stg_userid"))
                { clsCMD.Parameters["@stg_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@stg_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                stg_id = Convert.ToInt64(dr["stg_id"]);
                stg_settingflag = Convert.ToString(dr["stg_settingflag"]);
                stg_stingvalue = Convert.ToString(dr["stg_stingvalue"]);
                stg_stringmaxvalue = Convert.ToString(dr["stg_stringmaxvalue"]);
                stg_datetimevalue = Convert.ToDateTime(dr["stg_datetimevalue"]);
                stg_intvalue = Convert.ToInt64(dr["stg_intvalue"]);
                stg_doublevalue = Convert.ToInt32(dr["stg_doublevalue"]);
                stg_moneyvalue = Convert.ToDouble(dr["stg_moneyvalue"]);
                stg_boolevalue = Convert.ToInt64(dr["stg_boolevalue"]);
                if (dr["stg_userid"] != DBNull.Value || dr["stg_userid"].ToString().Trim() != "")
                {
                    stg_userid = new Guid(dr["stg_userid"].ToString());
                }
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            stg_id = Convert.ToInt64("0");
            stg_settingflag = Convert.ToString("NA");
            stg_stingvalue = Convert.ToString("NA");
            stg_stringmaxvalue = Convert.ToString("NA");
            stg_datetimevalue = Convert.ToDateTime(DateTime.Now);
            stg_intvalue = Convert.ToInt64("0");
            stg_doublevalue = Convert.ToInt32("0");
            stg_moneyvalue = Convert.ToDouble("0");
            stg_boolevalue = Convert.ToInt64("0");
            stg_userid = Guid.NewGuid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.stg_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "settingsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Save(Guid guUserid)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.stg_id = 0;
            this.stg_userid = guUserid;
            this.AddEditDeleteFlag = "ADDUSER";
            try
            {
                clsCMD.CommandText = "settingsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 stg_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.stg_id = stg_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "settingsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 stg_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.stg_id = stg_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "settingsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "settingsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsSettings_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsSettings_PropertiesList> lstResult = new List<clsSettings_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].stg_id = Convert.ToInt64(dr["stg_id"]);
                lstResult[i].stg_settingflag = Convert.ToString(dr["stg_settingflag"]);
                lstResult[i].stg_stingvalue = Convert.ToString(dr["stg_stingvalue"]);
                lstResult[i].stg_stringmaxvalue = Convert.ToString(dr["stg_stringmaxvalue"]);
                lstResult[i].stg_datetimevalue = Convert.ToDateTime(dr["stg_datetimevalue"]);
                lstResult[i].stg_intvalue = Convert.ToInt64(dr["stg_intvalue"]);
                lstResult[i].stg_doublevalue = Convert.ToInt32(dr["stg_doublevalue"]);
                lstResult[i].stg_moneyvalue = Convert.ToDouble(dr["stg_moneyvalue"]);
                lstResult[i].stg_boolevalue = Convert.ToInt64(dr["stg_boolevalue"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 stg_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.stg_id = stg_id;
                clsCMD.CommandText = "settingsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByFLAGInProperties(string strFlag)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.stg_settingflag = strFlag;
                clsCMD.CommandText = "settingsGetData";
                SetGetSPFlag = "BYFLAG";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByFlagUserInProperties(string strFlag,Guid guUserid)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.stg_settingflag = strFlag;
                this.stg_userid = guUserid;
                clsCMD.CommandText = "settingsGetData";
                SetGetSPFlag = "BYFLAGUSER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 stg_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.stg_id = stg_id;
                clsCMD.CommandText = "settingsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }

    public class clsSettings_PropertiesList
    {
        public Int64 stg_id { get; set; }
        public String stg_settingflag { get; set; }
        public String stg_stingvalue { get; set; }
        public String stg_stringmaxvalue { get; set; }
        public DateTime stg_datetimevalue { get; set; }
        public Int64 stg_intvalue { get; set; }
        public Int64 stg_doublevalue { get; set; }
        public Double stg_moneyvalue { get; set; }
        public Int64 stg_boolevalue { get; set; }
        public Guid stg_userid { get; set; }
    }
}