﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;

public partial class LifeProgram_Download : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (User.Identity.IsAuthenticated == true)
        {
            if (Session["download"] != null)
            {
                string strFileName = ConfigurationManager.AppSettings["filepath"].ToString() + Session["download"].ToString();

                if (File.Exists(strFileName))
                {
                    Response.Clear();
                    Response.ContentType = "application/force-download";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + Session["download"]);
                    Response.TransmitFile(strFileName);
                    Response.End();
                }
            }
            Session.Remove("download");
        }
        else
        {
            Response.Redirect("~/");
        }
    }
}