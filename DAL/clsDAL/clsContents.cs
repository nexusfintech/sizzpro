﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsContents
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsContents()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsContents()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _cnt_id; }
            set
            {
                _cnt_id = value;
                if (clsCMD.Parameters.Contains("@cnt_id"))
                { clsCMD.Parameters["@cnt_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cnt_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _cnt_id;
        public Int64 cnt_id
        {
            get { return _cnt_id; }
            set
            {
                _cnt_id = value;
                if (clsCMD.Parameters.Contains("@cnt_id"))
                { clsCMD.Parameters["@cnt_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cnt_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _cnt_key;
        public String cnt_key
        {
            get { return _cnt_key; }
            set
            {
                _cnt_key = value;
                if (clsCMD.Parameters.Contains("@cnt_key"))
                { clsCMD.Parameters["@cnt_key"].Value = value; }
                else { clsCMD.Parameters.Add("@cnt_key", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _cnt_html;
        public String cnt_html
        {
            get { return _cnt_html; }
            set
            {
                _cnt_html = value;
                if (clsCMD.Parameters.Contains("@cnt_html"))
                { clsCMD.Parameters["@cnt_html"].Value = value; }
                else { clsCMD.Parameters.Add("@cnt_html", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _cnt_title;
        public String cnt_title
        {
            get { return _cnt_title; }
            set
            {
                _cnt_title = value;
                if (clsCMD.Parameters.Contains("@cnt_title"))
                { clsCMD.Parameters["@cnt_title"].Value = value; }
                else { clsCMD.Parameters.Add("@cnt_title", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _cnt_metakeywords;
        public String cnt_metakeywords
        {
            get { return _cnt_metakeywords; }
            set
            {
                _cnt_metakeywords = value;
                if (clsCMD.Parameters.Contains("@cnt_metakeywords"))
                { clsCMD.Parameters["@cnt_metakeywords"].Value = value; }
                else { clsCMD.Parameters.Add("@cnt_metakeywords", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _cnt_metadescription;
        public String cnt_metadescription
        {
            get { return _cnt_metadescription; }
            set
            {
                _cnt_metadescription = value;
                if (clsCMD.Parameters.Contains("@cnt_metadescription"))
                { clsCMD.Parameters["@cnt_metadescription"].Value = value; }
                else { clsCMD.Parameters.Add("@cnt_metadescription", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                cnt_id = Convert.ToInt64(dr["cnt_id"]);
                cnt_key = Convert.ToString(dr["cnt_key"]);
                cnt_html = Convert.ToString(dr["cnt_html"]);
                cnt_title = Convert.ToString(dr["cnt_title"]);
                cnt_metakeywords = Convert.ToString(dr["cnt_metakeywords"]);
                cnt_metadescription = Convert.ToString(dr["cnt_metadescription"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            cnt_id = Convert.ToInt64("0");
            cnt_key = Convert.ToString("NA");
            cnt_html = Convert.ToString("NA");
            cnt_title = Convert.ToString("NA");
            cnt_metakeywords = Convert.ToString("NA");
            cnt_metadescription = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.cnt_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_ContentsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 cnt_id)
        {
            Boolean blnResult = false;
            this.cnt_id = cnt_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_ContentsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 cnt_id)
        {
            Boolean blnResult = false;
            this.cnt_id = cnt_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_ContentsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_ContentsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsContents_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsContents_PropertiesList> lstResult = new List<clsContents_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].cnt_id = Convert.ToInt64(dr["cnt_id"]);
                lstResult[i].cnt_key = Convert.ToString(dr["cnt_key"]);
                lstResult[i].cnt_html = Convert.ToString(dr["cnt_html"]);
                lstResult[i].cnt_title = Convert.ToString(dr["cnt_title"]);
                lstResult[i].cnt_metakeywords = Convert.ToString(dr["cnt_metakeywords"]);
                lstResult[i].cnt_metadescription = Convert.ToString(dr["cnt_metadescription"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 cnt_id)
        {
            Boolean blnResult = false;
            try
            {
                this.cnt_id = cnt_id;
                clsCMD.CommandText = "sp_ContentsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean GetRecordByIDMenuKey(String key)
        {
            Boolean blnResult = false;
            try
            {
                this.cnt_key=key;
                clsCMD.CommandText = "sp_ContentsGetData";
                SetGetSPFlag = "BYKEY";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 cnt_id)
        {
            Boolean blnResult = false;
            try
            {
                this.cnt_id = cnt_id;
                clsCMD.CommandText = "sp_ContentsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsContents_PropertiesList
    {
        public Int64 cnt_id { get; set; }
        public String cnt_key { get; set; }
        public String cnt_html { get; set; }
        public String cnt_title { get; set; }
        public String cnt_metakeywords { get; set; }
        public String cnt_metadescription { get; set; }
    }
}