﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Master_InsauraceCompanyMaster : System.Web.UI.Page
{
    clsInsuranceCompanyMaster clsICM = new clsInsuranceCompanyMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptCmpny.DataSource = clsICM.GetAllRecord();
        rptCmpny.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Master/InsuranceCompanyMasterAddEdit.aspx");
    }
    protected void rptCmpny_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if(e.CommandName=="EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/InsuranceCompanyMasterAddEdit.aspx");
        }
        if(e.CommandName == "DELETE")
        {
            Boolean blnRslt = clsICM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if(blnRslt)
            {
                MySession.Current.MESSAGE = "Record Deleted Successfully";
                Response.Redirect("~/Master/InsauraceCompanyMaster.aspx");
            }
        }
    }
}