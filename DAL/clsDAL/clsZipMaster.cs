﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsZipMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsZipMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();

            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsZipMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _zip_id; }
            set
            {
                _zip_id = value;
                if (clsCMD.Parameters.Contains("@zip_id"))
                { clsCMD.Parameters["@zip_id"].Value = value; }
                else { clsCMD.Parameters.Add("@zip_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _zip_id;
        public Int64 zip_id
        {
            get { return _zip_id; }
            set
            {
                _zip_id = value;
                if (clsCMD.Parameters.Contains("@zip_id"))
                { clsCMD.Parameters["@zip_id"].Value = value; }
                else { clsCMD.Parameters.Add("@zip_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _zip_name;
        public String zip_name
        {
            get { return _zip_name; }
            set
            {
                _zip_name = value;
                if (clsCMD.Parameters.Contains("@zip_name"))
                { clsCMD.Parameters["@zip_name"].Value = value; }
                else { clsCMD.Parameters.Add("@zip_name", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _zip_shortname;
        public String zip_shortname
        {
            get { return _zip_shortname; }
            set
            {
                _zip_shortname = value;
                if (clsCMD.Parameters.Contains("@zip_shortname"))
                { clsCMD.Parameters["@zip_shortname"].Value = value; }
                else { clsCMD.Parameters.Add("@zip_shortname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _zip_code;
        public String zip_code
        {
            get { return _zip_code; }
            set
            {
                _zip_code = value;
                if (clsCMD.Parameters.Contains("@zip_code"))
                { clsCMD.Parameters["@zip_code"].Value = value; }
                else { clsCMD.Parameters.Add("@zip_code", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _zip_zipcode;
        public String zip_zipcode
        {
            get { return _zip_zipcode; }
            set
            {
                _zip_zipcode = value;
                if (clsCMD.Parameters.Contains("@zip_zipcode"))
                { clsCMD.Parameters["@zip_zipcode"].Value = value; }
                else { clsCMD.Parameters.Add("@zip_zipcode", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _zip_parentid;
        public Int64 zip_parentid
        {
            get { return _zip_parentid; }
            set
            {
                _zip_parentid = value;
                if (clsCMD.Parameters.Contains("@zip_parentid"))
                { clsCMD.Parameters["@zip_parentid"].Value = value; }
                else { clsCMD.Parameters.Add("@zip_parentid", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _zip_flag;
        public String zip_flag
        {
            get { return _zip_flag; }
            set
            {
                _zip_flag = value;
                if (clsCMD.Parameters.Contains("@zip_flag"))
                { clsCMD.Parameters["@zip_flag"].Value = value; }
                else { clsCMD.Parameters.Add("@zip_flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                zip_id = Convert.ToInt64(dr["zip_id"]);
                zip_name = Convert.ToString(dr["zip_name"]);
                zip_shortname = Convert.ToString(dr["zip_shortname"]);
                zip_code = Convert.ToString(dr["zip_code"]);
                zip_zipcode = Convert.ToString(dr["zip_zipcode"]);
                zip_parentid = Convert.ToInt64(dr["zip_parentid"]);
                zip_flag = Convert.ToString(dr["zip_flag"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            zip_id = Convert.ToInt64("0");
            zip_name = Convert.ToString("NA");
            zip_shortname = Convert.ToString("NA");
            zip_code = Convert.ToString("NA");
            zip_zipcode = Convert.ToString("NA");
            zip_parentid = Convert.ToInt64("0");
            zip_flag = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function
        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.zip_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_zipmasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 zip_id)
        {
            Boolean blnResult = false;
            this.zip_id = zip_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_zipmasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 zip_id)
        {
            Boolean blnResult = false;
            this.zip_id = zip_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_zipmasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_zipmasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsZipMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsZipMaster_PropertiesList> lstResult = new List<clsZipMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].zip_id = Convert.ToInt64(dr["zip_id"]);
                lstResult[i].zip_name = Convert.ToString(dr["zip_name"]);
                lstResult[i].zip_shortname = Convert.ToString(dr["zip_shortname"]);
                lstResult[i].zip_code = Convert.ToString(dr["zip_code"]);
                lstResult[i].zip_zipcode = Convert.ToString(dr["zip_zipcode"]);
                lstResult[i].zip_parentid = Convert.ToInt64(dr["zip_parentid"]);
                lstResult[i].zip_flag = Convert.ToString(dr["zip_flag"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 zip_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.zip_id = zip_id;
                clsCMD.CommandText = "sp_zipmasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 zip_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.zip_id = zip_id;
                clsCMD.CommandText = "sp_zipmasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public DataTable GetAllByMasterFlag(String strMasterFlag)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.zip_flag = strMasterFlag;
                clsCMD.CommandText = "sp_zipmasterGetData";
                SetGetSPFlag = "BYFLAG";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        #endregion

    }

    public class clsZipMaster_PropertiesList
    {
        public Int64 zip_id { get; set; }
        public String zip_name { get; set; }
        public String zip_shortname { get; set; }
        public String zip_code { get; set; }
        public String zip_zipcode { get; set; }
        public Int64 zip_parentid { get; set; }
        public String zip_flag { get; set; }
    }
}