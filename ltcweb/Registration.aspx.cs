﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Web.Security;

public partial class Registration : System.Web.UI.Page
{
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsEmailAccounts clsMailAccount = new clsEmailAccounts();
    clsProfileMaster clsProfile = new clsProfileMaster();
    clsEmailTemplate clsMailTemplate = new clsEmailTemplate();
    clsSendMail clsMailSend = new clsSendMail();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private Boolean ValidateDate(string strValue)
    {
        Match vldDate = Regex.Match(strValue, "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$");
        return vldDate.Success;
    }

    private string ValidatePage()
    {
        string strResult = "";
        if (txtFirstName.Text.Trim() == "")
        { strResult += "Please enter First Name <br/>"; return strResult; }
        if (txtLastName.Text.Trim() == "")
        { strResult += "Please enter Last Name <br/>"; return strResult; }
        if (txtPassword.Text.Trim() == "")
        { strResult += "Please enter Password <br/>"; return strResult; }
        if (txtEmail.Text.Trim() == "")
        { strResult += "Please enter Email <br/>"; return strResult; }
        else
        {
            string strRegEx = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Match validemail = Regex.Match(txtEmail.Text.Trim(), strRegEx);
            if (!validemail.Success)
            {
                strResult += "Please Enter Valid Email Address <br/>";
                return strResult;
            }
        }
        if (txtDOB.Text.Trim() == "")
        { strResult += "Please enter Date of Birth <br/>"; return strResult; }
        else
        {
            Boolean blnDob = ValidateDate(txtDOB.Text.Trim());
            if (blnDob == false)
            { strResult += "Invalid Birth Date <br/>"; return strResult; }
        }
        if (txtAddress.Text.Trim() == "")
        { strResult += "Please enter Address <br/>"; return strResult; }
        if (txtZipCode.Text.Trim() == "")
        { strResult += "Please enter Zip Code <br/>"; return strResult; }
        if (txtCity.Text.Trim() == "")
        { strResult += "Please enter City <br/>"; return strResult; }
        if (txtPhoneNo.Text.Trim() == "")
        { strResult += "Please enter Phone No <br/>"; return strResult; }
        if (txtMobileNo.Text.Trim() == "")
        { strResult += "Please enter Mobile No <br/>"; return strResult; }


        return strResult;
    }

    public string GetErrorMessage(MembershipCreateStatus status)
    {
        switch (status)
        {
            case MembershipCreateStatus.DuplicateUserName:
                return "Username already exists. Please enter a different user name.";

            case MembershipCreateStatus.DuplicateEmail:
                return "A username for that e-mail address already exists. Please enter a different e-mail address.";

            case MembershipCreateStatus.InvalidPassword:
                return "The password provided is invalid. Please enter a valid password value.";

            case MembershipCreateStatus.InvalidEmail:
                return "The e-mail address provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidAnswer:
                return "The password retrieval answer provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidQuestion:
                return "The password retrieval question provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidUserName:
                return "The user name provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.ProviderError:
                return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            case MembershipCreateStatus.UserRejected:
                return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            default:
                return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
        }
    }

    protected void cmdRegister_Click(object sender, EventArgs e)
    {
        string strResult = ValidatePage();
        if (strResult != "")
        {
            lblMessage.Text = strResult;
            return;
        }
        MembershipCreateStatus memstatus;
            string strPassword = txtPassword.Text.Trim();
        MembershipUser memuser = Membership.CreateUser(txtEmail.Text.Trim(), strPassword, txtEmail.Text.Trim(), "admin@sizzpro", "nexus@admin", true, out memstatus);
        if (memuser == null)
        {
            lblMessage.Text = GetErrorMessage(memstatus);
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        else
        {
            String[] strRoles = new String[1];
            strRoles[0] = "Client";
            Roles.AddUserToRoles(txtEmail.Text.Trim(), strRoles);
            MembershipUser mu = Membership.GetUser(txtEmail.Text.Trim());
            Guid userguid = (Guid)mu.ProviderUserKey;
            Boolean blnTemplate = clsMailTemplate.GetRecordByNameInProperties("UserCreateWelcomeMail");
            if (blnTemplate == true)
            {
                Boolean blnMailAccount = clsMailAccount.GetRecordByIDInProperties(clsMailTemplate.emt_mailaccountid);
                if (blnMailAccount == true)
                {
                    Boolean blnAccountBind = clsMailSend.BindEmailAccount(clsMailAccount.ema_emailaddress);
                    if (blnAccountBind == true)
                    {
                        String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                        String strhost = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                        string strlink = strhost + "UserMessage.aspx?message=activation&tokenid=" + userguid.ToString();
                        clsMailSend.ToMailAddress = txtEmail.Text.Trim();
                        clsMailSend.Subject = clsMailTemplate.emt_templatesubject;
                        if (clsMailTemplate.emt_ccmail.Trim() != "") { clsMailSend.CCMailAddress = clsMailTemplate.emt_ccmail.Trim(); }
                        if (clsMailTemplate.emt_bccmail.Trim() != "") { clsMailSend.BCCMailAddress = clsMailTemplate.emt_bccmail.Trim(); }
                        String strHtmlBody = clsMailTemplate.emt_templatebody.Trim();
                        strHtmlBody = strHtmlBody.Replace("{username}", txtEmail.Text.Trim());
                        strHtmlBody = strHtmlBody.Replace("{password}", strPassword);
                        strHtmlBody = strHtmlBody.Replace("{activationlink}", strlink);
                        clsMailSend.HtmlBody = strHtmlBody;
                        clsMailSend.Priority = System.Net.Mail.MailPriority.Normal;
                        Boolean blnSendMail = clsMailSend.SendMail();
                        if (blnSendMail == true)
                        {
                            string[] strGeoValue = hdclient.Value.Split('@');
                            clsUsers clsUsr = new clsUsers();
                            Boolean blnUsr = clsUsr.GetRecordByIDInProperties(userguid);
                            clsProfile.prm_firstname = txtFirstName.Text;
                            clsProfile.prm_lastname = txtLastName.Text;
                            clsProfile.prm_user_Id = clsUsr.User_Id;
                            clsProfile.prm_emailid = txtEmail.Text.Trim();
                            clsProfile.prm_dob = Convert.ToDateTime(txtDOB.Text.Trim());
                            clsProfile.prm_useraddress = txtAddress.Text.Trim();
                            clsProfile.prm_zipcode = txtZipCode.Text.Trim();
                            clsProfile.prm_usercity = txtCity.Text.Trim();
                            clsProfile.prm_userstate = ucState.SelectedValue;
                            clsProfile.prm_usercountry = txtCountry.SelectedValue;
                            clsProfile.prm_userphone = txtPhoneNo.Text.Trim();
                            clsProfile.prm_usermobile = txtMobileNo.Text.Trim();
                            //if (strGeoValue.Length > 0)
                            //{
                            //    clsProfile.prm_geocountcode = strGeoValue[2].ToString();
                            //    clsProfile.prm_geocountry = strGeoValue[1].ToString();
                            //    clsProfile.prm_geoisp = strGeoValue[0].ToString();
                            //    clsProfile.prm_georegion = strGeoValue[3].ToString();
                            //    clsProfile.prm_geotimezone = strGeoValue[6].ToString();
                            //    clsProfile.prm_geozipcode = strGeoValue[7].ToString();
                            //    clsProfile.prm_ipaddress = strGeoValue[0].ToString();
                            //    clsProfile.prm_latlong = strGeoValue[4].ToString() + "," + strGeoValue[5].ToString();
                            //}
                            clsProfile.prm_passwordreset = true;
                            clsProfile.prm_userId = userguid.ToString();
                            clsProfile.prm_emailverify = false;
                            clsProfile.prm_userrole = "Client";
                            clsProfile.prm_tokenid = userguid.ToString();
                            clsProfile.prm_tokendatetime = DateTime.Now;
                            Boolean blnProfile = clsProfile.Save();

                            string strMemberID = ConfigurationManager.AppSettings["memberuserid"].ToString();

                            AddUserParentMapping(userguid);

                            if (blnProfile == true)
                            {
                                Response.Redirect("~/informedConsentDetails.aspx?ui="+userguid);
                            }
                            else { lblMessage.Text = "Error in user profile creation."; }
                        }
                        else { lblMessage.Text = "Error in sending welcome mail."; }
                    }
                    else { lblMessage.Text = "Error in Mail Account's Setting Bind To Mail Sender Engine"; }
                }
                else { lblMessage.Text = "Invalid Mail Account Binding In Email Template"; }
            }
            else { lblMessage.Text = "User Create Successfully. but error in Welcome Mail Sending.. please resend manualy"; }
        }
    }

    private void AddUserParentMapping(Guid guUserid)
    {
        clsClientParentMapping clsMap = new clsClientParentMapping();
        clsUsers clsUsr = new clsUsers();
        MembershipUser mu=Membership.GetUser(ConfigurationManager.AppSettings["memberuserid"].ToString());
        clsMap.cpm_parentid =(Guid)mu.ProviderUserKey;
        clsMap.cpm_userid = guUserid;
        clsMap.Save();
    }
}