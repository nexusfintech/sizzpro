﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InitialAssessment.aspx.cs" Inherits="Client_InitialAssessment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-user"></i>&nbsp;Initial Assessment of <%= User.Identity.Name %></h2>
        </div>
    </div>
    <hr />
    <UC:ManageIntroContent ID="ucManageIntro" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:UserProfile ID="ucManageProfile" runat="server" Visible="false" MemberKey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:UserFamilyOrigion ID="ucFamiliOrigion" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:SpouseSingficant ID="ucSpouse" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:UserSibling ID="ucSibling" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:ChildrenDetail ID="ucChildren" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:ClientFinancial ID="ucFinancial" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:ClientMedical ID="ucMedical" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <%--<UC:ClientSymptoms ID="ucSymptoms" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />--%>
    <UC:Symptoms id="ucMental" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:Psychological ID="ucPsychological" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:PhysicalWellBeing ID="ucWellbeing" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:Addiction ID="ucAddiction" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:AssessmentFamily ID="ucFamily" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:AssessmentProblem ID="ucProblem" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
    <UC:AssessmentReadiness ID="ucReadiness" runat="server" Visible="false" Memberkey='<%# Membership.GetUser(User.Identity.Name).ProviderUserKey %>' />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

