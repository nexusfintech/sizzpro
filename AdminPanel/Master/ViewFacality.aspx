﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="ViewFacality.aspx.cs" Inherits="Master_ViewFacalityaspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="box box-color box-bordered">
        <div class="box-title">
            <h3>
                <i class="glyphicon-projector"></i>
                Facility List
            </h3>
        </div>
        <div class="box-content nopadding">
            <div class="row-fluid">
                <div class="span12">
                    <asp:Repeater ID="rptFclty" runat="server">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered">
                                <thead>
                                    <tr>
                                        <th>Provider</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Phone</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("Provider") %></td>
                                <td><%# Eval("fsl_name") %></td>
                                <td><%# Eval("fsl_address") %></td>
                                <td><%# Eval("fsl_city") %></td>
                                <td><%# Eval("fsl_phone") %></td>
                                <%--<td style="text-align: center">
                                    <div id="action" runat="server">
                                        <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "fsl_id") %>'><i class="icon-edit"></i> Edit</asp:LinkButton>
                                    </div>
                                </td>--%>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>