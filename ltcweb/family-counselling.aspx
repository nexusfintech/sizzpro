﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="family-counselling.aspx.cs" Inherits="family_counselling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-group"></i>&nbsp;FAMILY COUNSELLING</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <img src="img/family-counsil.png" />
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h4>FAMILY COUNSELLING</h4>
            <p>When most people think of “family” they experience feelings of happiness, good memories, and love.  The family unit is the first social system to which we are involved in our developing years, and like individuals, families grow and change over time.  Some of these changes include adding new members through birth or adoption, moving to a new city, career changes, and saying goodbye to members who have passed away.</p>
            <p>
                Some families experience difficulties during these transitions and are at a loss for how to adapt and move forward.  Communication between members becomes difficult, conflict seems more dominant than happiness, and stress levels can seem overwhelming.  It is at some point in these times when it becomes necessary for a family to seek professional assistance in getting back on the right track. 
            There is no one definition of “perfect” that is applicable to all families.  Like individuals, families are unique in their own diversities.  However, many families experience the same difficulties during similar times of growth.  Our counseling services will help you take a look at your situation from various angles in an effort to find internal resources to strengthen and empower your family.  We work from several counseling models which include creating optimal family structure, teaching positive communication skills, and building teamwork within the system.  Our goal is to help you restore the happiness which can create and imbue lifelong positive memories.
            </p>
            <h4>Family Resources online</h4>
            <ul>
                <li><a href="http://www.familyresource.com/">Familyresource.com</a></li>
                <li><a href="http://www.focusonthefamily.com/">Helping  Families Thrive</a></li>
                <li><a href="http://www.acf.hhs.gov/">U.S.  Department of Health &amp; Human Services Administration for Children &amp;  Families</a></li>
                <li><a href="http://ag.udel.edu/extension/fam/FM/issue/survivecrisis.htm">Surviving a  Family Crisis</a></li>
            </ul>
            <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

