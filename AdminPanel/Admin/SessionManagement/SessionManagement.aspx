﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="SessionManagement.aspx.cs" Inherits="Admin_SessionManagement_SessionManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
       <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Action</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
               <a><i class="icon-plus"></i> <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn btn-primary" Text="Add New Session" OnClick="cmdCreateNew_Click"/></a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <div class="box">
        <div class="box-title">
            <h3>
                <i class="glyphicon-projector"></i>
                Client Session Management
                <small><asp:Label ID="lblClientName" runat="server" Text="Not Found..!!"></asp:Label></small>
            </h3>
        </div>
        <div class="box-content nopadding">
            <asp:Repeater ID="rptSession" runat="server" OnItemCommand="rptSession_ItemCommand">
                <HeaderTemplate>
                    <table class="table table-hover table-colored-header table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 10%;">Date</th>
                                <th style="width: 10%;">Client Name</th>
                                <th style="width: 10%;">Session Type</th>
                                <th style="width: 10%;">Location</th>
                                <th style="width: 10%;">Total Charged</th>
                                <th style="width: 10%;"><%= MySession.Current.UserAlias%> </th>
                                <th style="text-align: center; width: 10%;">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="text-align:center">
                            <%# Eval("ssn_date","{0:MM/dd/yyyy}")%>
                        </td>
                        <td style="text-align:center"><%# Eval("prm_firstname") %>&nbsp;<%# Eval("prm_lastname") %></td>
                        <td style="text-align:center"><%# Eval("ssn_sessiontype") %></td>
                        <td style="text-align:center"><%# Eval("ssn_location") %></td>
                        <td style="text-align:center"> 
                            $ <%# Eval("ssn_totalcharged","{0:f2}") %>
                        </td>
                        <td style="text-align:center">
                            $ <%# Eval("ssn_therapist","{0:f2}") %>
                        </td>
                        <td style="text-align:center">
                            <asp:LinkButton ID="cmdEdit" runat="server"  CssClass="btn btn-warning" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ssn_id") %>'><i class="icon-edit-sign"></i> Edit</asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>

