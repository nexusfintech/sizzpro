﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RChapters.aspx.cs" Inherits="shpweb_LifeProgram_RChapters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <style type="text/css">
        .alternate
        {
            background-color: #b7d0f3;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h5>You are in, <a href="DashBoard.aspx">Home</a>  &gt;&gt; 
                        <a href="MyReport.aspx">Report</a> &gt;&gt;<a href="RChapters.aspx"> Chapters</a>
            </h5>
            <hr />
            <asp:Repeater ID="RTTitle" runat="server">
                <ItemTemplate>
                    <div class="row-fluid">
                        <div class="span12">
                            <h2><i class="icon-book"></i>&nbsp;<%#Eval("Name") %></h2>
                        </div>
                    </div>

                </ItemTemplate>
            </asp:Repeater>
            <asp:Repeater ID="RT" runat="server">
                <HeaderTemplate>
                    <table style="margin: auto; width: 790px; float: left" border="0">
                        <thead>
                            <tr style="text-align: center; font-weight: bold; background-color: #296a9d; color: #FFFFFF;">
                                <th style="width: 50px; height: 30px;">No.</th>
                                <th style="text-align: left; padding-left: 5px; width: 600px;">Chapter Title</th>
                            </tr>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr style="text-align: center;">
                        <td style="height: 30px" class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>"><%# Container.ItemIndex +1 %></td>
                        <td style="text-align: left; padding-left: 5px;" class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>"><%#Eval("Title") %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:Repeater ID="RTLink" runat="server">
                <HeaderTemplate>
                    <table style="margin: auto; width: 350px; float: left" border="0">
                        <thead>
                            <tr style="text-align: center; font-weight: bold; background-color: #296a9d; color: #FFFFFF;">
                                <th style="width: 50px; height: 30px">Status</th>
                                <th style="width: 50px;">Email</th>
                                <th style="width: 50px;">View</th>
                            </tr>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr style="text-align: center;">
                        <td class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>">
                            <asp:Image ID="imgsts" runat="server" ImageUrl='<%#Eval("urlsts") %>' Style="height: 30px; width: 30px;" />
                        </td>
                        <td class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/img/email.png" Style="height: 30px; width: 30px;" />
                        </td>
                        <td class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>">
                            <a href='<%#Eval("href") %>' id='<%#Eval("ID") %>' name="view">
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%#Eval("urlgo") %>' Style="height: 30px; width: 30px;" />
                            </a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <input type="hidden" id="hidden" name="hidden" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("[name='view']").click(function () {
                var id = this.id;
                $("#<%= hidden.ClientID %>").val(id);
                $("#form1").submit();
                return false;
            });
        });
    </script>
</asp:Content>
