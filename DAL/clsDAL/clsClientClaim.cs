﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsClientClaim
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsClientClaim()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsClientClaim()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _cm_id; }
            set
            {
                _cm_id = value;
                if (clsCMD.Parameters.Contains("@cm_id"))
                { clsCMD.Parameters["@cm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _cm_id;
        public Int64 cm_id
        {
            get { return _cm_id; }
            set
            {
                _cm_id = value;
                if (clsCMD.Parameters.Contains("@cm_id"))
                { clsCMD.Parameters["@cm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private DateTime _cm_datecreated;
        public DateTime cm_datecreated
        {
            get { return _cm_datecreated; }
            set
            {
                _cm_datecreated = value;
                if (clsCMD.Parameters.Contains("@cm_datecreated"))
                { clsCMD.Parameters["@cm_datecreated"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_datecreated", SqlDbType.DateTime).Value = value; }
            }
        }
        private Guid _cm_sesssionid;
        public Guid cm_sesssionid
        {
            get { return _cm_sesssionid; }
            set
            {
                _cm_sesssionid = value;
                if (clsCMD.Parameters.Contains("@cm_sesssionid"))
                { clsCMD.Parameters["@cm_sesssionid"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_sesssionid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _cm_pataintid;
        public Guid cm_pataintid
        {
            get { return _cm_pataintid; }
            set
            {
                _cm_pataintid = value;
                if (clsCMD.Parameters.Contains("@cm_pataintid"))
                { clsCMD.Parameters["@cm_pataintid"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_pataintid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _cm_FormType;
        public String cm_FormType
        {
            get { return _cm_FormType; }
            set
            {
                _cm_FormType = value;
                if (clsCMD.Parameters.Contains("@cm_FormType"))
                { clsCMD.Parameters["@cm_FormType"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_FormType", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private DateTime _cm_timein;
        public DateTime cm_timein
        {
            get { return _cm_timein; }
            set
            {
                _cm_timein = value;
                if (clsCMD.Parameters.Contains("@cm_timein"))
                { clsCMD.Parameters["@cm_timein"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_timein", SqlDbType.DateTime).Value = value; }
            }
        }
        private DateTime _cm_timeout;
        public DateTime cm_timeout
        {
            get { return _cm_timeout; }
            set
            {
                _cm_timeout = value;
                if (clsCMD.Parameters.Contains("@cm_timeout"))
                { clsCMD.Parameters["@cm_timeout"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_timeout", SqlDbType.DateTime).Value = value; }
            }
        }
        private Double _cm_totalcharge;
        public Double cm_totalcharge
        {
            get { return _cm_totalcharge; }
            set
            {
                _cm_totalcharge = value;
                if (clsCMD.Parameters.Contains("@cm_totalcharge"))
                { clsCMD.Parameters["@cm_totalcharge"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_totalcharge", SqlDbType.Money).Value = value; }
            }
        }
        private Int64 _cm_insurancecompany;
        public Int64 cm_insurancecompany
        {
            get { return _cm_insurancecompany; }
            set
            {
                _cm_insurancecompany = value;
                if (clsCMD.Parameters.Contains("@cm_insurancecompany"))
                { clsCMD.Parameters["@cm_insurancecompany"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_insurancecompany", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _cm_insurancebilling;
        public String cm_insurancebilling
        {
            get { return _cm_insurancebilling; }
            set
            {
                _cm_insurancebilling = value;
                if (clsCMD.Parameters.Contains("@cm_insurancebilling"))
                { clsCMD.Parameters["@cm_insurancebilling"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_insurancebilling", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Boolean _cm_issecondry;
        public Boolean cm_issecondry
        {
            get { return _cm_issecondry; }
            set
            {
                _cm_issecondry = value;
                if (clsCMD.Parameters.Contains("@cm_issecondry"))
                { clsCMD.Parameters["@cm_issecondry"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_issecondry", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _cm_isemergency;
        public Boolean cm_isemergency
        {
            get { return _cm_isemergency; }
            set
            {
                _cm_isemergency = value;
                if (clsCMD.Parameters.Contains("@cm_isemergency"))
                { clsCMD.Parameters["@cm_isemergency"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_isemergency", SqlDbType.Bit).Value = value; }
            }
        }
        private DateTime _cm_submiteddate;
        public DateTime cm_submiteddate
        {
            get { return _cm_submiteddate; }
            set
            {
                _cm_submiteddate = value;
                if (clsCMD.Parameters.Contains("@cm_submiteddate"))
                { clsCMD.Parameters["@cm_submiteddate"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_submiteddate", SqlDbType.DateTime).Value = value; }
            }
        }
        private Int64 _cm_status;
        public Int64 cm_status
        {
            get { return _cm_status; }
            set
            {
                _cm_status = value;
                if (clsCMD.Parameters.Contains("@cm_status"))
                { clsCMD.Parameters["@cm_status"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_status", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _cm_claimnote;
        public String cm_claimnote
        {
            get { return _cm_claimnote; }
            set
            {
                _cm_claimnote = value;
                if (clsCMD.Parameters.Contains("@cm_claimnote"))
                { clsCMD.Parameters["@cm_claimnote"].Value = value; }
                else { clsCMD.Parameters.Add("@cm_claimnote", SqlDbType.NVarChar).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                cm_id = Convert.ToInt64(dr["cm_id"]);
                cm_datecreated = Convert.ToDateTime(dr["cm_datecreated"]);
                cm_sesssionid = new Guid(dr["cm_sesssionid"].ToString());
                cm_pataintid = new Guid(dr["cm_pataintid"].ToString());
                cm_FormType = Convert.ToString(dr["cm_FormType"]);
                cm_timein = Convert.ToDateTime(dr["cm_timein"]);
                cm_timeout = Convert.ToDateTime(dr["cm_timeout"]);
                cm_totalcharge = Convert.ToDouble(dr["cm_totalcharge"]);
                cm_insurancecompany = Convert.ToInt64(dr["cm_insurancecompany"]);
                cm_insurancebilling = Convert.ToString(dr["cm_insurancebilling"]);
                cm_issecondry = Convert.ToBoolean(dr["cm_issecondry"]);
                cm_isemergency = Convert.ToBoolean(dr["cm_isemergency"]);
                cm_submiteddate = Convert.ToDateTime(dr["cm_submiteddate"]);
                cm_status = Convert.ToInt64(dr["cm_status"]);
                cm_claimnote = Convert.ToString(dr["cm_claimnote"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            cm_id = Convert.ToInt64("0");
            cm_datecreated = Convert.ToDateTime(DateTime.Now);
            cm_sesssionid = new Guid();
            cm_pataintid = new Guid();
            cm_FormType = Convert.ToString("NA");
            cm_timein = Convert.ToDateTime(DateTime.Now);
            cm_timeout = Convert.ToDateTime(DateTime.Now);
            cm_totalcharge = Convert.ToDouble("0");
            cm_insurancecompany = Convert.ToInt64("0");
            cm_insurancebilling = Convert.ToString("NA");
            cm_issecondry = Convert.ToBoolean(false);
            cm_isemergency = Convert.ToBoolean(false);
            cm_submiteddate = Convert.ToDateTime(DateTime.Now);
            cm_status = Convert.ToInt64("0");
            cm_claimnote = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.cm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_ClientClaimAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 cm_id)
        {
            Boolean blnResult = false;
            this.cm_id = cm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_ClientClaimAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 cm_id)
        {
            Boolean blnResult = false;
            this.cm_id = cm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_ClientClaimAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_ClientClaimGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsClientClaim_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsClientClaim_PropertiesList> lstResult = new List<clsClientClaim_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].cm_id = Convert.ToInt64(dr["cm_id"]);
                lstResult[i].cm_datecreated = Convert.ToDateTime(dr["cm_datecreated"]);
                lstResult[i].cm_sesssionid = new Guid(dr["cm_sesssionid"].ToString());
                lstResult[i].cm_pataintid = new Guid(dr["cm_pataintid"].ToString());
                lstResult[i].cm_FormType = Convert.ToString(dr["cm_FormType"]);
                lstResult[i].cm_timein = Convert.ToDateTime(dr["cm_timein"]);
                lstResult[i].cm_timeout = Convert.ToDateTime(dr["cm_timeout"]);
                lstResult[i].cm_totalcharge = Convert.ToDouble(dr["cm_totalcharge"]);
                lstResult[i].cm_insurancecompany = Convert.ToInt64(dr["cm_insurancecompany"]);
                lstResult[i].cm_insurancebilling = Convert.ToString(dr["cm_insurancebilling"]);
                lstResult[i].cm_issecondry = Convert.ToBoolean(dr["cm_issecondry"]);
                lstResult[i].cm_isemergency = Convert.ToBoolean(dr["cm_isemergency"]);
                lstResult[i].cm_submiteddate = Convert.ToDateTime(dr["cm_submiteddate"]);
                lstResult[i].cm_status = Convert.ToInt64(dr["cm_status"]);
                lstResult[i].cm_claimnote = Convert.ToString(dr["cm_claimnote"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 cm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.cm_id = cm_id;
                clsCMD.CommandText = "sp_ClientClaimGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 cm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.cm_id = cm_id;
                clsCMD.CommandText = "sp_ClientClaimGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsClientClaim_PropertiesList
    {
        public Int64 cm_id { get; set; }
        public DateTime cm_datecreated { get; set; }
        public Guid cm_sesssionid { get; set; }
        public Guid cm_pataintid { get; set; }
        public String cm_FormType { get; set; }
        public DateTime cm_timein { get; set; }
        public DateTime cm_timeout { get; set; }
        public Double cm_totalcharge { get; set; }
        public Int64 cm_insurancecompany { get; set; }
        public String cm_insurancebilling { get; set; }
        public Boolean cm_issecondry { get; set; }
        public Boolean cm_isemergency { get; set; }
        public DateTime cm_submiteddate { get; set; }
        public Int64 cm_status { get; set; }
        public String cm_claimnote { get; set; }
    }
}