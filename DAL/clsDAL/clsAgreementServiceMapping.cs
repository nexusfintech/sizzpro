﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsAgreementServiceMapping
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsAgreementServiceMapping()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsAgreementServiceMapping()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _asm_id; }
            set
            {
                _asm_id = value;
                if (clsCMD.Parameters.Contains("@asm_id"))
                { clsCMD.Parameters["@asm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@asm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _asm_id;
        public Int64 asm_id
        {
            get { return _asm_id; }
            set
            {
                _asm_id = value;
                if (clsCMD.Parameters.Contains("@asm_id"))
                { clsCMD.Parameters["@asm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@asm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _asm_agreement;
        public Int64 asm_agreement
        {
            get { return _asm_agreement; }
            set
            {
                _asm_agreement = value;
                if (clsCMD.Parameters.Contains("@asm_agreement"))
                { clsCMD.Parameters["@asm_agreement"].Value = value; }
                else { clsCMD.Parameters.Add("@asm_agreement", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _asm_service;
        public Int64 asm_service
        {
            get { return _asm_service; }
            set
            {
                _asm_service = value;
                if (clsCMD.Parameters.Contains("@asm_service"))
                { clsCMD.Parameters["@asm_service"].Value = value; }
                else { clsCMD.Parameters.Add("@asm_service", SqlDbType.BigInt).Value = value; }
            }
        }
        private Boolean _asm_isactive;
        public Boolean asm_isactive
        {
            get { return _asm_isactive; }
            set
            {
                _asm_isactive = value;
                if (clsCMD.Parameters.Contains("@asm_isactive"))
                { clsCMD.Parameters["@asm_isactive"].Value = value; }
                else { clsCMD.Parameters.Add("@asm_isactive", SqlDbType.Bit).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                asm_id = Convert.ToInt64(dr["asm_id"]);
                asm_agreement = Convert.ToInt64(dr["asm_agreement"]);
                asm_service = Convert.ToInt64(dr["asm_service"]);
                asm_isactive = Convert.ToBoolean(dr["asm_isactive"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            asm_id = Convert.ToInt64("0");
            asm_agreement = Convert.ToInt64("0");
            asm_service = Convert.ToInt64("0");
            asm_isactive = Convert.ToBoolean(true);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.asm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_tbl_AgreementServiceMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 asm_id)
        {
            Boolean blnResult = false;
            this.asm_id = asm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_tbl_AgreementServiceMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 asm_id)
        {
            Boolean blnResult = false;
            this.asm_id = asm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_tbl_AgreementServiceMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_tbl_AgreementServiceMappingGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsAgreementServiceMapping_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsAgreementServiceMapping_PropertiesList> lstResult = new List<clsAgreementServiceMapping_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].asm_id = Convert.ToInt64(dr["asm_id"]);
                lstResult[i].asm_agreement = Convert.ToInt64(dr["asm_agreement"]);
                lstResult[i].asm_service = Convert.ToInt64(dr["asm_service"]);
                lstResult[i].asm_isactive = Convert.ToBoolean(dr["asm_isactive"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 asm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.asm_id = asm_id;
                clsCMD.CommandText = "sp_tbl_AgreementServiceMappingGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean GetRecordByServiceinProperties(Int64 asm_ser)
        {
            Boolean blnResult = false;
            try
            {
                this.asm_service = asm_ser;
                clsCMD.CommandText = "sp_tbl_AgreementServiceMappingGetData";
                SetGetSPFlag = "BYSER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public DataTable GetRecordByAgreement(Int64 agreementid)
        {
           DataTable dtResult = new DataTable();
            try
            {
                this.asm_agreement = agreementid;
                clsCMD.CommandText = "sp_tbl_AgreementServiceMappingGetData";
                SetGetSPFlag = "BYAGR";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public Boolean CheckDuplicateRow(Int64 asm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.asm_id = asm_id;
                clsCMD.CommandText = "sp_tbl_AgreementServiceMappingGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsAgreementServiceMapping_PropertiesList
    {
        public Int64 asm_id { get; set; }
        public Int64 asm_agreement { get; set; }
        public Int64 asm_service { get; set; }
        public Boolean asm_isactive { get; set; }
    }
}