﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class UserControl_Assessment_ucAssessmentProblem : System.Web.UI.UserControl
{
    clsInitialAssessment clsASS = new clsInitialAssessment();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();

    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    private void EditValue()
    {
        Boolean blnEdit = false;
        Guid objKey = new Guid(strMemberkey);
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, rdoproblempreviconsu.ClientID);
        if (blnEdit == true)
        {
            ListItem li = rdoproblempreviconsu.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                rdoproblempreviconsu.SelectedIndex = rdoproblempreviconsu.Items.IndexOf(li);
            }
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, rdoupsetstatus.ClientID);
        if (blnEdit == true)
        {
            ListItem li = rdoproblempreviconsu.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                rdoupsetstatus.SelectedIndex = rdoupsetstatus.Items.IndexOf(li);
            }
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtmainproblems.ClientID);
        if (blnEdit == true)
        {
            txtmainproblems.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblefewwortherapy.ClientID);
        if (blnEdit == true)
        {
            txtproblefewwortherapy.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemapproximate.ClientID);
        if (blnEdit == true)
        {
            txtproblemapproximate.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblembackground.ClientID);
        if (blnEdit == true)
        {
            txtproblembackground.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblembetternoties.ClientID);
        if (blnEdit == true)
        {
            txtproblembetternoties.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemdofellbetter.ClientID);
        if (blnEdit == true)
        {
            txtproblemdofellbetter.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemexprience.ClientID);
        if (blnEdit == true)
        {
            txtproblemexprience.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemgretstrenghts.ClientID);
        if (blnEdit == true)
        {
            txtproblemgretstrenghts.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemgretweakness.ClientID);
        if (blnEdit == true)
        {
            txtproblemgretweakness.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemhopegettherapy.ClientID);
        if (blnEdit == true)
        {
            txtproblemhopegettherapy.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemknowbetter.ClientID);
        if (blnEdit == true)
        {
            txtproblemknowbetter.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemlasttherapy.ClientID);
        if (blnEdit == true)
        {
            txtproblemlasttherapy.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemotheragencies.ClientID);
        if (blnEdit == true)
        {
            txtproblemotheragencies.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemsignificant.ClientID);
        if (blnEdit == true)
        {
            txtproblemsignificant.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemtryresolve.ClientID);
        if (blnEdit == true)
        {
            txtproblemtryresolve.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtproblemwhatdate.ClientID);
        if (blnEdit == true)
        {
            txtproblemwhatdate.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtprolemwithwhom.ClientID);
        if (blnEdit == true)
        {
            txtprolemwithwhom.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtptoblemoutcome.ClientID);
        if (blnEdit == true)
        {
            txtptoblemoutcome.Text = clsASS.ini_valuetext;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtrequesthelp.ClientID);
        if (blnEdit == true)
        {
            txtrequesthelp.Text = clsASS.ini_valuetext;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            EditValue();
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Boolean blnSave = false;
            Guid objKey = new Guid(strMemberkey);
            clsASS.ini_userid = objKey;
            ListItem li = rdoproblempreviconsu.SelectedItem;
            if (li != null)
            {
                clsASS.ini_valueflag = rdoproblempreviconsu.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(rdoproblempreviconsu.SelectedValue);
                blnSave = clsASS.Save();
            }
            ListItem li1 = rdoupsetstatus.SelectedItem;
            if (li1 != null)
            {
                clsASS.ini_valueflag = rdoupsetstatus.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(rdoupsetstatus.SelectedValue);
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtmainproblems.ClientID;
                clsASS.ini_valuetext = txtmainproblems.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblefewwortherapy.ClientID;
                clsASS.ini_valuetext = txtproblefewwortherapy.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemapproximate.ClientID;
                clsASS.ini_valuetext = txtproblemapproximate.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblembackground.ClientID;
                clsASS.ini_valuetext = txtproblembackground.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblembetternoties.ClientID;
                clsASS.ini_valuetext = txtproblembetternoties.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemdofellbetter.ClientID;
                clsASS.ini_valuetext = txtproblemdofellbetter.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemexprience.ClientID;
                clsASS.ini_valuetext = txtproblemexprience.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemgretstrenghts.ClientID;
                clsASS.ini_valuetext = txtproblemgretstrenghts.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemgretweakness.ClientID;
                clsASS.ini_valuetext = txtproblemgretweakness.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemhopegettherapy.ClientID;
                clsASS.ini_valuetext = txtproblemhopegettherapy.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemknowbetter.ClientID;
                clsASS.ini_valuetext = txtproblemknowbetter.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemlasttherapy.ClientID;
                clsASS.ini_valuetext = txtproblemlasttherapy.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemotheragencies.ClientID;
                clsASS.ini_valuetext = txtproblemotheragencies.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemsignificant.ClientID;
                clsASS.ini_valuetext = txtproblemsignificant.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemtryresolve.ClientID;
                clsASS.ini_valuetext = txtproblemtryresolve.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtproblemwhatdate.ClientID;
                clsASS.ini_valuetext = txtproblemwhatdate.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtprolemwithwhom.ClientID;
                clsASS.ini_valuetext = txtprolemwithwhom.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtptoblemoutcome.ClientID;
                clsASS.ini_valuetext = txtptoblemoutcome.Text.Trim();
                blnSave = clsASS.Save();
            }

            if (txtmainproblems.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtrequesthelp.ClientID;
                clsASS.ini_valuetext = txtrequesthelp.Text.Trim();
                blnSave = clsASS.Save();
            }

            clsASR.storcrd_stepid = 19;
            clsASR.stprcrd_userid = objKey;
            Boolean stprslt = clsASR.Save();
            if (stprslt == true)
            {
                Response.Redirect("~/Client/Assessments/ClientReadiness.aspx");
            }
            lblMessage.Text = "Save Successfully.";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error.. in saving while " + clsASS.ini_valueflag + " and System Error.." + ex.Message;
        }
    }
}