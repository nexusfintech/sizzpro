﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.IO;
using clsDAL;
using System.Data;
using System.Net;
using System.Text;

public partial class MasterPage : System.Web.UI.MasterPage
{
    clsProfileMaster clsProfile = new clsProfileMaster();
    clsUsers clsUSR = new clsUsers();
    clsSettings clsSTG = new clsSettings();
    clsAssesmetStepMaster clsASM = new clsAssesmetStepMaster();

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.DataBind();
        //FormsAuthentication.SignOut();
        if (Page.User.Identity.IsAuthenticated == true)
        { dvLoginBox.Visible = false; }
        else { dvLoginBox.Visible = true; }
        txtUsername.Focus();

        if (Page.User.Identity.IsAuthenticated)
        {
            DataTable dt = clsASM.GetNotCompletedSteps((Guid)Membership.GetUser().ProviderUserKey);
            if (dt.Rows.Count > 0)
            {
                rptIntialAssesment.DataSource = clsASM.GetNotCompletedSteps((Guid)Membership.GetUser().ProviderUserKey);
                rptIntialAssesment.DataBind();
            }
            else
            {
                DataTable table = new DataTable();
                table.Columns.Add("stp_stepurl", typeof(string));
                table.Columns.Add("stp_stepname", typeof(string));

                table.Rows.Add("MyAssesment.aspx", "View Assesment");
                rptIntialAssesment.DataSource = table;
                rptIntialAssesment.DataBind();
            }
        }
    }

    protected void cmdLogin_Click(object sender, EventArgs e)
    {
        if (txtUsername.Text.Trim() == "" || txtPassword.Text.Trim() == "")
        {
            lblMessage.Text = "Please enter username or password";
            txtUsername.Focus();
            return;
        }
        else
        {
            //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:6966/api/auth");
            //    request.Method = "POST";
            //    request.ContentType = "application/x-www-form-urlencoded";
            //    request.Accept = "application/json";
            //    Stream dataStream = request.GetRequestStream();
            //    string postData = "&UserName=" + txtUsername.Text.ToString() + "&Password=" + txtPassword.Text.ToString();
            //    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            //    dataStream.Write(byteArray, 0, byteArray.Length);
            //    dataStream.Close();
            //     HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //    Stream responseStream = response.GetResponseStream();
            //    StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
            //    var pageContent = myStreamReader.ReadToEnd();
            //    if (pageContent == "true")
            //    {
            //        string[] usersInRole = Roles.GetRolesForUser(txtUsername.Text.Trim());
            //        if (usersInRole[0] == "Administrator" || usersInRole[0] == "Member" || usersInRole[0] == "User")
            //        {
            //            lblMessage.Text = "Only client can login from here..";
            //            return;
            //        }
            //        MembershipUser mu = Membership.GetUser(txtUsername.Text.Trim());
            //        if (mu != null)
            //        {
            //            Guid userguid = (Guid)mu.ProviderUserKey;
            //            Session["form_memberkey"] = userguid.ToString();
            //            clsUSR.GetRecordByIDInProperties(userguid);
            //            Session["form_userid"] = clsUSR.User_Id;
            //            Boolean blnProfile = clsProfile.GetRecordByMemberKeyInProperties(userguid.ToString());
            //            if (blnProfile == true)
            //            {
            //                Session["userloginname"] = clsProfile.prm_firstname + " " + clsProfile.prm_lastname;
            //                if (clsProfile.prm_emailverify == false)
            //                {
            //                    Session["msg_header"] = "Your Email Account Verification is not completed";
            //                    Session["msg_boxheader"] = "Hello, " + txtUsername.Text;
            //                    Session["msg_message"] = "Your email account not verify please check mail inbox you will get verification mail in that have verification link. you have to verify by that link";
            //                    FormsAuthentication.SignOut(); Response.Redirect("~/Messages.aspx");
            //                }
            //            }
            //            else
            //            {
            //                Session["msg_header"] = "Your Email Account Verification is not completed or Profile Corrupted";
            //                Session["msg_boxheader"] = "Hello, " + txtUsername.Text;
            //                Session["msg_message"] = "Your email account not verify please check mail inbox you will get verification mail in that have verification link. you have to verify by that link";
            //                Response.Redirect("~/Messages.aspx");
            //            }
            //        }
            //        else
            //        {
            //            Session["msg_header"] = "Membership Authication Failed";
            //            Session["msg_boxheader"] = "Hello, " + txtUsername.Text;
            //            Session["msg_message"] = "Membership verification authetication process is not completed.";
            //            Response.Redirect("~/Messages.aspx");
            //        }
            //        Boolean blnSession = clsSTG.GetRecordByFLAGInProperties("WEBSESSION");
            //        double dblSession = 20;
            //        if (blnSession == true)
            //        { dblSession = (double)clsSTG.stg_intvalue; }
            //        Session.Timeout = Convert.ToInt32(dblSession);
            //        // Success, create non-persistent authentication cookie.
            //        FormsAuthentication.SetAuthCookie(
            //                this.txtUsername.Text.Trim(), false);
            //        FormsAuthenticationTicket ticket1 =
            //           new FormsAuthenticationTicket(
            //                1,                                   // version
            //                this.txtUsername.Text.Trim(),   // get username  from the form
            //                DateTime.Now,                        // issue time is now
            //                DateTime.Now.AddMinutes(dblSession),         // expires in 10 minutes
            //                false,      // cookie is not persistent
            //                usersInRole[0] // role assignment is stored
            //            // in userData
            //                );
            //        HttpCookie cookie1 = new HttpCookie(
            //          FormsAuthentication.FormsCookieName,
            //          FormsAuthentication.Encrypt(ticket1));
            //        Response.Cookies.Add(cookie1);

            //        // 4. Do the redirect. 
            //        String returnUrl1;
            //        // the login is successful
            //        if (Request.QueryString["ReturnUrl"] == null)
            //        {
            //            Response.Redirect("~/Client/Dashboard.aspx");
            //        }
            //        //login not unsuccessful 
            //        else
            //        {
            //            returnUrl1 = Request.QueryString["ReturnUrl"];
            //            Response.Redirect(returnUrl1);
            //        }
            //    }
            //    else
            //    {
            //        lblMessage.Visible = true; lblMessage.Text = "Invalid Username or Password"; MembershipUser mu = Membership.GetUser(txtUsername.Text.Trim());
            //        txtPassword.Focus();
            //    }
            //}

           // Boolean blnArry = Array.IndexOf(usersInRole, "Client") >= 0;
            if (Membership.ValidateUser(txtUsername.Text.Trim(), txtPassword.Text))
            {
                string[] usersInRole = Roles.GetRolesForUser(txtUsername.Text.Trim());
                if (usersInRole[0] == "Administrator" || usersInRole[0] == "Member" || usersInRole[0] == "User")
                {
                    lblMessage.Text = "Only client can login from here..";
                    return;
                }

                MembershipUser mu = Membership.GetUser(txtUsername.Text.Trim());
                if (mu != null)
                {
                    Guid userguid = (Guid)mu.ProviderUserKey;
                    Session["form_memberkey"] = userguid.ToString();
                    clsUSR.GetRecordByIDInProperties(userguid);
                    Session["form_userid"] = clsUSR.User_Id;
                    Boolean blnProfile = clsProfile.GetRecordByMemberKeyInProperties(userguid.ToString());
                    if (blnProfile == true)
                    {
                        Session["userloginname"] = clsProfile.prm_firstname + " " + clsProfile.prm_lastname;
                        if (clsProfile.prm_emailverify == false)
                        {
                            Session["msg_header"] = "Your Email Account Verification is not completed";
                            Session["msg_boxheader"] = "Hello, " + txtUsername.Text;
                            Session["msg_message"] = "Your email account not verify please check mail inbox you will get verification mail in that have verification link. you have to verify by that link";
                            FormsAuthentication.SignOut(); Response.Redirect("~/Messages.aspx");
                        }
                    }
                    else
                    {
                        Session["msg_header"] = "Your Email Account Verification is not completed or Profile Corrupted";
                        Session["msg_boxheader"] = "Hello, " + txtUsername.Text;
                        Session["msg_message"] = "Your email account not verify please check mail inbox you will get verification mail in that have verification link. you have to verify by that link";
                        Response.Redirect("~/Messages.aspx");
                    }
                }
                else
                {
                    Session["msg_header"] = "Membership Authication Failed";
                    Session["msg_boxheader"] = "Hello, " + txtUsername.Text;
                    Session["msg_message"] = "Membership verification authetication process is not completed.";
                    Response.Redirect("~/Messages.aspx");
                }
                Boolean blnSession = clsSTG.GetRecordByFLAGInProperties("WEBSESSION");
                double dblSession = 20;
                if (blnSession == true)
                { dblSession = (double)clsSTG.stg_intvalue; }
                Session.Timeout = Convert.ToInt32(dblSession);
                // Success, create non-persistent authentication cookie.
                FormsAuthentication.SetAuthCookie(
                        this.txtUsername.Text.Trim(), false);
                FormsAuthenticationTicket ticket1 =
                   new FormsAuthenticationTicket(
                        1,                                   // version
                        this.txtUsername.Text.Trim(),   // get username  from the form
                        DateTime.Now,                        // issue time is now
                        DateTime.Now.AddMinutes(dblSession),         // expires in 10 minutes
                        false,      // cookie is not persistent
                        usersInRole[0] // role assignment is stored
                    // in userData
                        );
                HttpCookie cookie1 = new HttpCookie(
                  FormsAuthentication.FormsCookieName,
                  FormsAuthentication.Encrypt(ticket1));
                Response.Cookies.Add(cookie1);

                // 4. Do the redirect. 
                String returnUrl1;
                // the login is successful
                if (Request.QueryString["ReturnUrl"] == null)
                {
                    Response.Redirect("~/Client/Dashboard.aspx");
                }
                //login not unsuccessful 
                else
                {
                    returnUrl1 = Request.QueryString["ReturnUrl"];
                    Response.Redirect(returnUrl1);
                }
            }
            else
            {
                lblMessage.Visible = true; lblMessage.Text = "Invalid Username or Password"; MembershipUser mu = Membership.GetUser(txtUsername.Text.Trim());
                txtPassword.Focus();
                if (mu != null)
                {
                    if (mu.IsLockedOut)
                    {
                        lblMessage.Text = "User is locked. Please contact to administrator";
                    }
                }
            }
        }
    }
}
