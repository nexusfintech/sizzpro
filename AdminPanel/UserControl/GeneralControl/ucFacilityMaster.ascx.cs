﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class UserControl_GeneralControl_ucFacilityMaster : System.Web.UI.UserControl
{
    clsFacilities clsFM = new clsFacilities();
    private Int64 intId = 0;
    private string strName = string.Empty;

    public Int64 SelectedValue
    {
        get { intId = Convert.ToInt64(ddlFacility.SelectedValue); return intId; }
        set { intId = value; }
    }

    public string SelectedName
    {
        get { strName = ddlFacility.SelectedItem.Text; return strName; }
        set { strName = value; }
    }

    public int Width
    {
        set { ddlFacility.Width = Unit.Pixel(value); }
    }

    public void Bind()
    {
        ddlFacility.DataSource = clsFM.GetAllRecordByMem((Guid)Membership.GetUser().ProviderUserKey);
        ddlFacility.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}