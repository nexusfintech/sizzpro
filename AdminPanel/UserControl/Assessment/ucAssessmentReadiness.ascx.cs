﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class UserControl_Assessment_ucAssessmentReadiness : System.Web.UI.UserControl
{
    clsInitialAssessment clsASS = new clsInitialAssessment();
    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    private void EditValue()
    {
        Boolean blnEdit = false;
        Guid objKey = new Guid(strMemberkey);
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtreadinessfrequntly.ClientID);
        if (blnEdit == true)
        { txtreadinessfrequntly.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness1.ClientID);
        if (blnEdit == true)
        { chkreadiness1.Checked = clsASS.ini_valuebit; }


        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness10.ClientID);
        if (blnEdit == true)
        {
            chkreadiness10.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness11.ClientID);
        if (blnEdit == true)
        {
            chkreadiness11.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness12.ClientID);
        if (blnEdit == true)
        {
            chkreadiness12.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness13.ClientID);
        if (blnEdit == true)
        {
            chkreadiness13.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness14.ClientID);
        if (blnEdit == true)
        {
            chkreadiness14.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness15.ClientID);
        if (blnEdit == true)
        {
            chkreadiness15.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness16.ClientID);
        if (blnEdit == true)
        {
            chkreadiness16.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness17.ClientID);
        if (blnEdit == true)
        {
            chkreadiness17.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness18.ClientID);
        if (blnEdit == true)
        {
            chkreadiness18.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness19.ClientID);
        if (blnEdit == true)
        {
            chkreadiness19.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness2.ClientID);
        if (blnEdit == true)
        {
            chkreadiness2.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness20.ClientID);
        if (blnEdit == true)
        {
            chkreadiness20.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness21.ClientID);
        if (blnEdit == true)
        {
            chkreadiness21.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness22.ClientID);
        if (blnEdit == true)
        {
            chkreadiness22.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness23.ClientID);
        if (blnEdit == true)
        {
            chkreadiness23.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness24.ClientID);
        if (blnEdit == true)
        {
            chkreadiness24.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness25.ClientID);
        if (blnEdit == true)
        {
            chkreadiness25.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness26.ClientID);
        if (blnEdit == true)
        {
            chkreadiness26.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness27.ClientID);
        if (blnEdit == true)
        {
            chkreadiness27.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness28.ClientID);
        if (blnEdit == true)
        {
            chkreadiness28.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness29.ClientID);
        if (blnEdit == true)
        {
            chkreadiness29.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness3.ClientID);
        if (blnEdit == true)
        {
            chkreadiness3.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness30.ClientID);
        if (blnEdit == true)
        {
            chkreadiness30.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness31.ClientID);
        if (blnEdit == true)
        {
            chkreadiness31.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness32.ClientID);
        if (blnEdit == true)
        {
            chkreadiness32.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness4.ClientID);
        if (blnEdit == true)
        {
            chkreadiness4.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness5.ClientID);
        if (blnEdit == true)
        {
            chkreadiness5.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness6.ClientID);
        if (blnEdit == true)
        {
            chkreadiness6.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness7.ClientID);
        if (blnEdit == true)
        {
            chkreadiness7.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness8.ClientID);
        if (blnEdit == true)
        {
            chkreadiness8.Checked = clsASS.ini_valuebit;
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkreadiness9.ClientID);
        if (blnEdit == true)
        {
            chkreadiness9.Checked = clsASS.ini_valuebit;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            EditValue();
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Boolean blnSave = false;
            Guid objKey = new Guid(strMemberkey);
            clsASS.ini_userid = objKey;
            if (txtreadinessfrequntly.Text.Trim() != "")
            {
                clsASS.ini_valueflag = txtreadinessfrequntly.ClientID;
                clsASS.ini_valuetext = txtreadinessfrequntly.Text.Trim();
                blnSave = clsASS.Save();
            }

            clsASS.ini_valueflag = chkreadiness1.ClientID;
            clsASS.ini_valuebit = chkreadiness1.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness10.ClientID;
            clsASS.ini_valuebit = chkreadiness10.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness11.ClientID;
            clsASS.ini_valuebit = chkreadiness11.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness12.ClientID;
            clsASS.ini_valuebit = chkreadiness12.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness13.ClientID;
            clsASS.ini_valuebit = chkreadiness13.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness14.ClientID;
            clsASS.ini_valuebit = chkreadiness14.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness15.ClientID;
            clsASS.ini_valuebit = chkreadiness15.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness16.ClientID;
            clsASS.ini_valuebit = chkreadiness16.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness17.ClientID;
            clsASS.ini_valuebit = chkreadiness17.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness18.ClientID;
            clsASS.ini_valuebit = chkreadiness18.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness19.ClientID;
            clsASS.ini_valuebit = chkreadiness19.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness2.ClientID;
            clsASS.ini_valuebit = chkreadiness2.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness20.ClientID;
            clsASS.ini_valuebit = chkreadiness20.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness21.ClientID;
            clsASS.ini_valuebit = chkreadiness21.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness22.ClientID;
            clsASS.ini_valuebit = chkreadiness22.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness23.ClientID;
            clsASS.ini_valuebit = chkreadiness23.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness24.ClientID;
            clsASS.ini_valuebit = chkreadiness24.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness25.ClientID;
            clsASS.ini_valuebit = chkreadiness25.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness26.ClientID;
            clsASS.ini_valuebit = chkreadiness26.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness27.ClientID;
            clsASS.ini_valuebit = chkreadiness27.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness28.ClientID;
            clsASS.ini_valuebit = chkreadiness28.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness29.ClientID;
            clsASS.ini_valuebit = chkreadiness29.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness3.ClientID;
            clsASS.ini_valuebit = chkreadiness3.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness30.ClientID;
            clsASS.ini_valuebit = chkreadiness30.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness31.ClientID;
            clsASS.ini_valuebit = chkreadiness31.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness32.ClientID;
            clsASS.ini_valuebit = chkreadiness32.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness4.ClientID;
            clsASS.ini_valuebit = chkreadiness4.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness5.ClientID;
            clsASS.ini_valuebit = chkreadiness5.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness6.ClientID;
            clsASS.ini_valuebit = chkreadiness6.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness7.ClientID;
            clsASS.ini_valuebit = chkreadiness7.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness8.ClientID;
            clsASS.ini_valuebit = chkreadiness8.Checked;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = chkreadiness9.ClientID;
            clsASS.ini_valuebit = chkreadiness9.Checked;
            blnSave = clsASS.Save();

            lblMessage.Text = "Save Successfully.";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error.. save in process of " + clsASS.ini_valueflag + " " + ex.Message; ;
        }
    }
}