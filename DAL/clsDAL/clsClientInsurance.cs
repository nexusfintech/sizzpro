﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsClientInsurance
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsClientInsurance()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsClientInsurance()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _ins_id; }
            set
            {
                _ins_id = value;
                if (clsCMD.Parameters.Contains("@ins_id"))
                { clsCMD.Parameters["@ins_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _ins_id;
        public Int64 ins_id
        {
            get { return _ins_id; }
            set
            {
                _ins_id = value;
                if (clsCMD.Parameters.Contains("@ins_id"))
                { clsCMD.Parameters["@ins_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _ins_userid;
        public Guid ins_userid
        {
            get { return _ins_userid; }
            set
            {
                _ins_userid = value;
                if (clsCMD.Parameters.Contains("@ins_userid"))
                { clsCMD.Parameters["@ins_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _ins_employer;
        public String ins_employer
        {
            get { return _ins_employer; }
            set
            {
                _ins_employer = value;
                if (clsCMD.Parameters.Contains("@ins_employer"))
                { clsCMD.Parameters["@ins_employer"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_employer", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _ins_inscompany;
        public Int64 ins_inscompany
        {
            get { return _ins_inscompany; }
            set
            {
                _ins_inscompany = value;
                if (clsCMD.Parameters.Contains("@ins_inscompany"))
                { clsCMD.Parameters["@ins_inscompany"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_inscompany", SqlDbType.BigInt).Value = value; }
            }
        }
        private DateTime _ins_effectdate;
        public DateTime ins_effectdate
        {
            get { return _ins_effectdate; }
            set
            {
                _ins_effectdate = value;
                if (clsCMD.Parameters.Contains("@ins_effectdate"))
                { clsCMD.Parameters["@ins_effectdate"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_effectdate", SqlDbType.DateTime).Value = value; }
            }
        }
        private String _ins_subscriber;
        public String ins_subscriber
        {
            get { return _ins_subscriber; }
            set
            {
                _ins_subscriber = value;
                if (clsCMD.Parameters.Contains("@ins_subscriber"))
                { clsCMD.Parameters["@ins_subscriber"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_subscriber", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ins_subscid;
        public String ins_subscid
        {
            get { return _ins_subscid; }
            set
            {
                _ins_subscid = value;
                if (clsCMD.Parameters.Contains("@ins_subscid"))
                { clsCMD.Parameters["@ins_subscid"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_subscid", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ins_insugroupnumber;
        public String ins_insugroupnumber
        {
            get { return _ins_insugroupnumber; }
            set
            {
                _ins_insugroupnumber = value;
                if (clsCMD.Parameters.Contains("@ins_insugroupnumber"))
                { clsCMD.Parameters["@ins_insugroupnumber"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_insugroupnumber", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ins_copayinspay;
        public String ins_copayinspay
        {
            get { return _ins_copayinspay; }
            set
            {
                _ins_copayinspay = value;
                if (clsCMD.Parameters.Contains("@ins_copayinspay"))
                { clsCMD.Parameters["@ins_copayinspay"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_copayinspay", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ins_insurancepay;
        public String ins_insurancepay
        {
            get { return _ins_insurancepay; }
            set
            {
                _ins_insurancepay = value;
                if (clsCMD.Parameters.Contains("@ins_insurancepay"))
                { clsCMD.Parameters["@ins_insurancepay"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_insurancepay", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private DateTime _ins_subscriberdob;
        public DateTime ins_subscriberdob
        {
            get { return _ins_subscriberdob; }
            set
            {
                _ins_subscriberdob = value;
                if (clsCMD.Parameters.Contains("@ins_subscriberdob"))
                { clsCMD.Parameters["@ins_subscriberdob"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_subscriberdob", SqlDbType.DateTime).Value = value; }
            }
        }
        private String _ins_insurancephone;
        public String ins_insurancephone
        {
            get { return _ins_insurancephone; }
            set
            {
                _ins_insurancephone = value;
                if (clsCMD.Parameters.Contains("@ins_insurancephone"))
                { clsCMD.Parameters["@ins_insurancephone"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_insurancephone", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ins_relationtosubscriber;
        public String ins_relationtosubscriber
        {
            get { return _ins_relationtosubscriber; }
            set
            {
                _ins_relationtosubscriber = value;
                if (clsCMD.Parameters.Contains("@ins_relationtosubscriber"))
                { clsCMD.Parameters["@ins_relationtosubscriber"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_relationtosubscriber", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _ins_obtauthorization;
        public Int64 ins_obtauthorization
        {
            get { return _ins_obtauthorization; }
            set
            {
                _ins_obtauthorization = value;
                if (clsCMD.Parameters.Contains("@ins_obtauthorization"))
                { clsCMD.Parameters["@ins_obtauthorization"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_obtauthorization", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _ins_authorization;
        public String ins_authorization
        {
            get { return _ins_authorization; }
            set
            {
                _ins_authorization = value;
                if (clsCMD.Parameters.Contains("@ins_authorization"))
                { clsCMD.Parameters["@ins_authorization"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_authorization", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ins_note;
        public String ins_note
        {
            get { return _ins_note; }
            set
            {
                _ins_note = value;
                if (clsCMD.Parameters.Contains("@ins_note"))
                { clsCMD.Parameters["@ins_note"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_note", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Boolean _ins_selftcharges;
        public Boolean ins_selftcharges
        {
            get { return _ins_selftcharges; }
            set
            {
                _ins_selftcharges = value;
                if (clsCMD.Parameters.Contains("@ins_selftcharges"))
                { clsCMD.Parameters["@ins_selftcharges"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_selftcharges", SqlDbType.Bit).Value = value; }
            }
        }

        private Boolean _ins_default;
        public Boolean ins_default
        {
            get { return _ins_default; }
            set
            {
                _ins_default = value;
                if (clsCMD.Parameters.Contains("@ins_default"))
                { clsCMD.Parameters["@ins_default"].Value = value; }
                else { clsCMD.Parameters.Add("@ins_default", SqlDbType.Bit).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                ins_id = Convert.ToInt64(dr["ins_id"]);
                ins_userid = new Guid(dr["ins_userid"].ToString());
                ins_employer = Convert.ToString(dr["ins_employer"]);
                ins_inscompany = Convert.ToInt64(dr["ins_inscompany"]);
                ins_effectdate = Convert.ToDateTime(dr["ins_effectdate"]);
                ins_subscriber = Convert.ToString(dr["ins_subscriber"]);
                ins_subscid = Convert.ToString(dr["ins_subscid"]);
                ins_insugroupnumber = Convert.ToString(dr["ins_insugroupnumber"]);
                ins_copayinspay = Convert.ToString(dr["ins_copayinspay"]);
                ins_insurancepay = Convert.ToString(dr["ins_insurancepay"]);
                ins_subscriberdob = Convert.ToDateTime(dr["ins_subscriberdob"]);
                ins_insurancephone = Convert.ToString(dr["ins_insurancephone"]);
                ins_relationtosubscriber = Convert.ToString(dr["ins_relationtosubscriber"]);
                ins_obtauthorization = Convert.ToInt64(dr["ins_obtauthorization"]);
                ins_authorization = Convert.ToString(dr["ins_authorization"]);
                ins_note = Convert.ToString(dr["ins_note"]);
                ins_selftcharges = Convert.ToBoolean(dr["ins_selftcharges"]);
                ins_default = Convert.ToBoolean(dr["ins_default"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            ins_id = Convert.ToInt64("0");
            ins_userid = new Guid();
            ins_employer = Convert.ToString("NA");
            ins_inscompany = Convert.ToInt64("0");
            ins_effectdate = Convert.ToDateTime(DateTime.Now);
            ins_subscriber = Convert.ToString("NA");
            ins_subscid = Convert.ToString("NA");
            ins_insugroupnumber = Convert.ToString("NA");
            ins_copayinspay = Convert.ToString("NA");
            ins_insurancepay = Convert.ToString("NA");
            ins_subscriberdob = Convert.ToDateTime(DateTime.Now);
            ins_insurancephone = Convert.ToString("NA");
            ins_relationtosubscriber = Convert.ToString("NA");
            ins_obtauthorization = Convert.ToInt64("0");
            ins_authorization = Convert.ToString("NA");
            ins_note = Convert.ToString("NA");
            ins_selftcharges = Convert.ToBoolean(false);
            ins_default = Convert.ToBoolean(false);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }


        public Boolean Save()
        {
            Boolean blnResult = false;
            this.ins_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_InsuranceAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 ins_id)
        {
            Boolean blnResult = false;
            this.ins_id = ins_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_InsuranceAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 ins_id)
        {
            Boolean blnResult = false;
            this.ins_id = ins_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_InsuranceAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_InsuranceGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByUserid(Guid Userid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.ins_userid = Userid;
                clsCMD.CommandText = "sp_InsuranceGetData";
                SetGetSPFlag = "BYUSERID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsClientInsurance_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsClientInsurance_PropertiesList> lstResult = new List<clsClientInsurance_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].ins_id = Convert.ToInt64(dr["ins_id"]);
                lstResult[i].ins_userid = new Guid(dr["ins_userid"].ToString());
                lstResult[i].ins_employer = Convert.ToString(dr["ins_employer"]);
                lstResult[i].ins_inscompany = Convert.ToInt64(dr["ins_inscompany"]);
                lstResult[i].ins_effectdate = Convert.ToDateTime(dr["ins_effectdate"]);
                lstResult[i].ins_subscriber = Convert.ToString(dr["ins_subscriber"]);
                lstResult[i].ins_subscid = Convert.ToString(dr["ins_subscid"]);
                lstResult[i].ins_insugroupnumber = Convert.ToString(dr["ins_insugroupnumber"]);
                lstResult[i].ins_copayinspay = Convert.ToString(dr["ins_copayinspay"]);
                lstResult[i].ins_insurancepay = Convert.ToString(dr["ins_insurancepay"]);
                lstResult[i].ins_subscriberdob = Convert.ToDateTime(dr["ins_subscriberdob"]);
                lstResult[i].ins_insurancephone = Convert.ToString(dr["ins_insurancephone"]);
                lstResult[i].ins_relationtosubscriber = Convert.ToString(dr["ins_relationtosubscriber"]);
                lstResult[i].ins_obtauthorization = Convert.ToInt64(dr["ins_obtauthorization"]);
                lstResult[i].ins_authorization = Convert.ToString(dr["ins_authorization"]);
                lstResult[i].ins_note = Convert.ToString(dr["ins_note"]);
                lstResult[i].ins_selftcharges = Convert.ToBoolean(dr["ins_selftcharges"]);
                lstResult[i].ins_default = Convert.ToBoolean(dr["ins_default"]);
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 ins_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.ins_id = ins_id;
                clsCMD.CommandText = "sp_InsuranceGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 ins_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.ins_id = ins_id;
                clsCMD.CommandText = "sp_InsuranceGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }

    public class clsClientInsurance_PropertiesList
    {
        public Int64 ins_id { get; set; }
        public Guid ins_userid { get; set; }
        public String ins_employer { get; set; }
        public Int64 ins_inscompany { get; set; }
        public DateTime ins_effectdate { get; set; }
        public String ins_subscriber { get; set; }
        public String ins_subscid { get; set; }
        public String ins_insugroupnumber { get; set; }
        public String ins_copayinspay { get; set; }
        public String ins_insurancepay { get; set; }
        public DateTime ins_subscriberdob { get; set; }
        public String ins_insurancephone { get; set; }
        public String ins_relationtosubscriber { get; set; }
        public Int64 ins_obtauthorization { get; set; }
        public String ins_authorization { get; set; }
        public String ins_note { get; set; }
        public Boolean ins_selftcharges { get; set; }
        public Boolean ins_default { get; set; }
    }
}