﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="ClaimAddEdit.aspx.cs" Inherits="Admin_ClaimManagment_ClaimAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="glyphicon-retweet"></i>
                    <asp:Button ID="cmdClmMstr" runat="server" CssClass="btn btn-primary" Text="Back To Claim" OnClick="cmdClmMstr_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Claim Add/Edit</h3>
                </div>
                <div class="box-content">
                    <div class="box box-bordered box-color">
                        <div class="box-title">
                            <h3><i class="icon-edit"></i>General</h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class="form-horizontal form-column form-bordered">
                                <div class="control-group">
                                    <label for="txtDate" class="control-label">Sessoin</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlSession" runat="server" CssClass="select2-me input-xxlarge"></asp:DropDownList>
                                        <%--<asp:LinkButton ID="cmdView" runat="server" CssClass="btn btn-primary" OnClick="cmdView_Click"><i class="icon-search"></i> View Session</asp:LinkButton>--%>
                                    </div>
                                </div>
                                <div class="span12">
                                    <div class="span6">
                                        <div class="control-group">
                                            <label for="txtDateNoteSubmit" class="control-label">Claim Created Date</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtcreateddate" runat="server" CssClass="input-large datepick"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="txtDate" class="control-label">In Time</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtIntime" runat="server" CssClass="input-small timepick" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="" class="control-label">Form Type</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddlFormType" runat="server" CssClass="select2-me input-xlarge">
                                                    <asp:ListItem Value="hcfa">HCFA</asp:ListItem>
                                                    <asp:ListItem Value="ubo4">U04B</asp:ListItem>
                                                    <asp:ListItem Value="ada">ADA</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="txtTotalPay" class="control-label"></label>
                                            <div class="controls">
                                                <div class="span12 check-line">
                                                    <asp:CheckBox ID="chksecondry" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                                                    <label class='inline'>Is Secondary</label>
                                                </div>
                                                <div class="span12 check-line">
                                                    <asp:CheckBox ID="chkemergency" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                                                    <label class='inline'>EMG</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6">

                                        <div class="control-group">
                                            <label for="ddlFacilities" class="control-label">Insurance company</label>
                                            <div class="controls">
                                                <UC:InscCMaster ID="ddlinscommaster" runat="server" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="txtDate" class="control-label">Out Time</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtOutTime" runat="server" CssClass="input-small timepick" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="ddlProvider" class="control-label">Insurance Billing Type</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddlbillingtype" runat="server" CssClass="select2-me input-small">
                                                    <asp:ListItem Value="paper">PAPER</asp:ListItem>
                                                    <asp:ListItem Value="edi">EDI</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="txtbehaviour" class="control-label">Additional Note</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtextranote" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box box-bordered box-color">
                        <div class="box-title">
                            <h3><i class="icon-edit"></i>Cliam Submission Detail</h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class="form-horizontal form-column form-bordered">
                                <div class="span6">
                                    <div class="control-group">
                                        <label for="txtDateNoteSubmit" class="control-label">Claim Submited Date</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtsubmiteddate" runat="server" CssClass="input-large datepick"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6">
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Claim Status</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="ddlClmStatus" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-bordered box-color">
                        <div class="box-title">
                            <h3><i class="icon-edit"></i>Billing Detail</h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class="form-horizontal form-column form-bordered">
                                <div class="span6">
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Charge</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtTotalCharge" runat="server" CssClass="input-medium"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6">
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Day Or Unit</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtdayorunit" runat="server" CssClass="input-medium"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="form-actions">
                    <asp:LinkButton ID="cmdSave" CssClass="btn btn-primary" runat="server" OnClick="cmdSave_Click"><i class="icon-ok-circle"></i> Save</asp:LinkButton>
                    <asp:LinkButton ID="cmdCancel" CssClass="btn" runat="server" OnClick="cmdCancel_Click"><i class="icon-remove-circle "></i> Cancle</asp:LinkButton>
                </div>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $("#<%=txtIntime.ClientID %>").timepicker({
            showMeridian: false,
        });
        $("#<%=txtOutTime.ClientID %>").timepicker({
            showMeridian: false,
        });
    </script>
</asp:Content>
