﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucChildrenDetail.ascx.cs" Inherits="UserControl_ClientProfile_ucChildrenDetail" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-user"></i>
                    Children Detail
                </h3>
            </div>
            <div class="box-content nopadding">
                <div class='form-horizontal form-column form-bordered'>

                    <div class="control-group">
                        <label for="txtFirstName" class="control-label">Name</label>
                        <div class="controls">
                            <asp:TextBox ID="txtName" runat="server" placeholder="Children Name" data-rule-required="true" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtAge" class="control-label">Age</label>
                        <div class="controls">
                            <asp:TextBox ID="txtAge" runat="server" placeholder="18 Years" data-rule-required="true" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtDOB" class="control-label">Birth Date</label>
                        <div class="controls">
                            <asp:TextBox ID="txtDOB" runat="server" CssClass="input-medium datepick" data-rule-required="true" value="02/16/12"></asp:TextBox>
                            <span class="help-block">Enter Birthdate in MM/DD/YYYY format.</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="chklevewithclient" class="control-label">Gender</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlGender" runat="server">
                                <asp:ListItem Text="Male"></asp:ListItem>
                                <asp:ListItem Text="Female"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtParName" class="control-label">Parent's Name</label>
                        <div class="controls">
                            <asp:TextBox ID="txtParName" runat="server" data-rule-required="true" placeholder="Parent's Name" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="chkLive" class="control-label"></label>
                        <div class="controls">
                            <div class="check-line">
                                <asp:CheckBox ID="chkLive" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                                <label class='inline'>Lives With Parent..?</label>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtResidence" class="control-label">Residence</label>
                        <div class="controls">
                            <asp:TextBox ID="txtResidence" runat="server" TextMode="MultiLine" Width="98%" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-actions">
                        <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <asp:Repeater ID="dtChildren" runat="server" OnItemCommand="dtChildren_ItemCommand">
            <HeaderTemplate>
                <table class="table table-hover table-nomargin dataTable table-bordered">
                    <thead>
                        <tr>
                            <th>Children Name</th>
                            <th>Age</th>
                            <th>Birth Date</th>
                            <th>Gender</th>
                            <th>Parent Name</th>
                            <th>Live With Parent</th>
                            <th>Residance</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("chd_name") %></td>
                    <td><%# Eval("chd_age") %></td>
                    <td><%# Eval("chd_BirthDate","{0:MM/dd/yyyy}") %></td>
                    <td><%# Eval("chd_gender") %></td>
                    <td><%# Eval("chd_parentname") %></td>
                    <td><%# Eval("chd_liveparent") %></td>
                    <td><%# Eval("chd_residence") %></td>
                    <td>
                        <i class="icon-edit">
                            <asp:Button ID="cmdEdit" runat="server" Text="Edit" CssClass="btn cancel" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "chd_id") %>' />
                        </i>
                    </td>
                      <td>
                        <i class="icon-remove">
                            <asp:Button ID="cmdDelete" runat="server" Text="Delete" CssClass="btn cancel" CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "chd_id") %>' />
                        </i>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
