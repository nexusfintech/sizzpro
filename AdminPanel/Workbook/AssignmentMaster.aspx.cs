﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Workbook_AssignmentMaster : System.Web.UI.Page
{
    clsAssignments clsASN = new clsAssignments();
    clsobjectsharing clsOBS = new clsobjectsharing();

    private void BindGrid()
    {
        if (User.IsInRole("administrator"))
        {
            dtAssignment.DataSource = clsASN.GetAllRecord();
        }
        else
        {
            dtAssignment.DataSource = clsASN.GetAllRecordByUser((Guid)Membership.GetUser().ProviderUserKey);
        }
        dtAssignment.DataBind();
    }

    private void BindGridNotMapped()
    {
        if (User.IsInRole("administrator"))
        {
            dtAssignment.DataSource = clsASN.GetAllNotMapped();
        }
        else if (User.IsInRole("member"))
        {
            dtAssignment.DataSource = clsASN.GetAllNotMappedByMember((Guid)Membership.GetUser().ProviderUserKey);
        }
        dtAssignment.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGrid();
            if (MySession.Current.MESSAGE != string.Empty)
            {
                lblmessage.Text = MySession.Current.MESSAGE;
                msgdiv.Visible = true;
                MySession.Current.MESSAGE = string.Empty;
            }
        }
    }

    protected void dtAssignment_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = e.CommandName;
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Workbook/AssignmentAddEdit.aspx");
        }
        if (e.CommandName == "SHARE")
        {
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Workbook/AssignmentSharing.aspx");
        }
        if (e.CommandName == "FREEZ")
        {
            Boolean BlnGetrslt = clsASN.GetRecordByIDInProperties(Convert.ToInt64(e.CommandArgument));
            Int64 Version = clsASN.GetAssignmentVersion(clsASN.asn_code);
            Boolean BlnRslt = clsASN.ChangeStatus(Convert.ToInt64(e.CommandArgument), true,Version + 1);
            if (BlnRslt)
            {
                Response.Redirect("~/Workbook/AssignmentMaster.aspx");
            }
            else
            {
                lblmessage.Text = "Error Occured During Freez Your Workbook" + clsASN.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
        if (e.CommandName == "NEWVER")
        {
            Boolean BlnRslt = clsASN.GetRecordByIDInProperties(Convert.ToInt64(e.CommandArgument));
            if (BlnRslt)
            {
                clsASN.asn_sts= false;
                Boolean blnSvRsl = clsASN.Save();
                if (blnSvRsl == true)
                {
                    clsASN.GetRecordByCodeInProperties(clsASN.asn_code);
                    clsOBS.shr_sharedobjid = clsASN.asn_id;
                    clsOBS.shr_object = "ASSIGNMENT";
                    clsOBS.shr_ownerid = clsASN.asn_UserId;
                    clsOBS.shr_sharedwithid = clsASN.asn_UserId;
                    blnSvRsl = clsOBS.Save();
                    Response.Redirect("~/Workbook/AssignmentMaster.aspx");
                }
                else
                {
                    lblmessage.Text = "Error Occured During Creatig New Copy" + clsASN.GetSetErrorMessage;
                    msgdiv.Visible = true;
                }
            }
            else
            {
                lblmessage.Text = "Error Occured During Featchng Your Assignment" + clsASN.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Workbook/AssignmentAddEdit.aspx");
    }

    protected void cmdHideMapped_Click(object sender, EventArgs e)
    {
        BindGridNotMapped();
    }

    protected void cmdShowAll_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void cmdMyRec_Click(object sender, EventArgs e)
    {

    }
    protected void dtAssignment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;
            Guid UserId = new Guid(DataBinder.Eval(DI, "asn_UserId").ToString());
            if (UserId == (Guid)Membership.GetUser().ProviderUserKey || User.IsInRole("administrator"))
            {
                Boolean Isact = Convert.ToBoolean(DataBinder.Eval(DI, "asn_sts"));
                if (Isact)
                {
                    LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdFreez");
                    lnkf.Enabled = false;
                    lnkf.CssClass = "btn btn-success";
                    //LinkButton lnks = (LinkButton)e.Item.FindControl("cmdShare");
                    //lnks.Enabled = true;
                }
                else
                {
                    LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdFreez");
                    lnkf.Enabled = true;

                    //LinkButton lnks = (LinkButton)e.Item.FindControl("cmdShare");
                    //lnks.Enabled = false;
                    //lnks.CssClass = "btn btn-success";
                }
                var dv = e.Item.FindControl("action");
                dv.Visible = true;
            }
            else
            {
                var dv = e.Item.FindControl("share");
                dv.Visible = true;
                var c = e.Item.FindControl("dvf");
                c.Visible = true;
            }
        }
    }
}