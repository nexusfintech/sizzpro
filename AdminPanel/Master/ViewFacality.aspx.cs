﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_ViewFacalityaspx : System.Web.UI.Page
{
    clsFacilities clsFCL = new clsFacilities();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            loaddata();
        }
    }
    private void loaddata()
    {
        rptFclty.DataSource = clsFCL.GetAllRecordForAdmin();
        rptFclty.DataBind();
    }
}