﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class message : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (MySession.Current.MSGCODE != string.Empty)
        {
            string messagetype = MySession.Current.MSGCODE;

            if (messagetype == "newuserregister")
            {
                newuserregister.Visible = true;
            }
            else if (messagetype == "servicepurchased")
            {
                servicepurchased.Visible = true;
            }
            else if (messagetype == "mappingerror")
            {
                mappingerreo.Visible = true;
            }
        }
        else
        {
            InvalidReq.Visible = true;
        }
    }
}