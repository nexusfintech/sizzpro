﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
using System.Globalization;

public partial class UserControl_ucProfileMaster : System.Web.UI.UserControl
{
    clsProfileMaster clsProfile = new clsProfileMaster();
    clsUsers clsUsr = new clsUsers();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();

    private string strMemberKey;
    private string[] strUserRole;
    private static string strTokenid;
    private static DateTime dtTokendatetime;
    private Int64 intUserID;

    public String MemberKey
    {
        get { return strMemberKey; }
        set { strMemberKey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberKey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
        strUserRole = Roles.GetRolesForUser(mu.UserName);
    }

    private void UploadPhoto()
    {
        int intFileSizeLimit = 1024;
        string strServerDirPath = Server.MapPath("~/Document/UserProfile/" + strMemberKey);
        string strPhotoGraphName = "Photograph.jpg";
        if (!Directory.Exists(strServerDirPath))
        {
            Directory.CreateDirectory(strServerDirPath);
        }
        string strExtensionName = Path.GetExtension(strServerDirPath + "/" + strPhotoGraphName);
        int intFileSize = fuprofilephoto.PostedFile.ContentLength / 1024;
        if (intFileSize < intFileSizeLimit)
        {
            if (strExtensionName.Equals(".jpg"))
            {
                if (fuprofilephoto.PostedFile.FileName != "")
                {
                    fuprofilephoto.PostedFile.SaveAs(strServerDirPath + "/" + strPhotoGraphName);
                }
                imgProfilePhoto.ImageUrl = "~/Document/UserProfile/" + strMemberKey + "/Photograph.jpg";
            }
            else
            {
                lblMessage.Text = "Only .jpg file are allowed, try again!";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        else { lblMessage.Text = "File size exceeded than limit " + intFileSizeLimit + " KB, Please upload smaller file."; lblMessage.ForeColor = System.Drawing.Color.Red; }
    }

    private Boolean EditProfile()
    {
        Boolean blnResult = false;
        blnResult = clsProfile.GetRecordByMemberKeyInProperties(strMemberKey);
        if (blnResult == true)
        {
            clsProfile.prm_claimidprefix = 0;
            txtDOB.Text = clsProfile.prm_dob.ToString("MM/dd/yyyy",CultureInfo.InvariantCulture);
            txtIntakedate.Text = clsProfile.prm_intakedate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            ddlEmployed.SelectedValue = clsProfile.prm_empid;
            txtEmail.Text = clsProfile.prm_emailid;
            txtemrAdd1.Text = clsProfile.prm_emr_add1;
            txtemrAdd2.Text = clsProfile.prm_emr_add2;
            chkrfsAddsameasclnt.Checked = clsProfile.prm_emr_addsameclnt;
            txtemrCity.Text = clsProfile.prm_emr_city;
            txtemrFirstname.Text = clsProfile.prm_emr_firstname;
            txtemrLastname.Text = clsProfile.prm_emr_lastname;
            txtemrPhone.Text = clsProfile.prm_emr_phoneno;
            ddlemrPhonetype.SelectedName = clsProfile.prm_emr_phonetype;
            ddlemrRelation.SelectedValue = clsProfile.prm_emr_relationship;
            ddlemrState.SelectedValue = clsProfile.prm_emr_state;
            txtemrZipcode.Text = clsProfile.prm_emr_zipcode;
            ddlEthnicity.SelectedValue = clsProfile.prm_ethincity;
            txtFirstname.Text = clsProfile.prm_firstname;
            ddlGender.SelectedValue = clsProfile.prm_gender;
            ddlIdentifiedclient.SelectedValue = clsProfile.prm_identifiedclient;
            clsProfile.prm_initial = "";
            ddlLanguage.SelectedValue = clsProfile.prm_langpreferance;
            txtLastname.Text = clsProfile.prm_lastname;
            ddlMaritalStatus.SelectedValue = clsProfile.prm_maritalstatus;
            txtMiddlename.Text = clsProfile.prm_middlename;
            txtrfsAdd1.Text = clsProfile.prm_rfs_add1;
            txtrfsAdd2.Text = clsProfile.prm_rfs_add2;
            chkrfsAddsameasclnt.Checked = clsProfile.prm_rfs_addsameclnt;
            txtrfsCity.Text = clsProfile.prm_rfs_city;
            txtrfsFirstname.Text = clsProfile.prm_rfs_firstname;
            txtrfsLastname.Text = clsProfile.prm_rfs_lastname;
            ddlrfsRelationship.SelectedValue = clsProfile.prm_rfs_reltoclient;
            ddlrfsState.SelectedValue = clsProfile.prm_rfs_state;
            txtrfsZipcode.Text = clsProfile.prm_rfs_zipcode;
            txtscdFirstname.Text = clsProfile.prm_scd_firstname;
            txtscdLastname.Text = clsProfile.prm_scd_lastname;
            txtscdPhone.Text = clsProfile.prm_scd_phoneno;
            ddlscdPhonetype.SelectedName = clsProfile.prm_scd_phonetype;
            ddlscdRelationship.SelectedValue = clsProfile.prm_scd_relationship;
            txtSSN.Text = clsProfile.prm_ssn;
            chkStudent.Checked = clsProfile.prm_student;
            strTokenid = clsProfile.prm_tokenid;
            dtTokendatetime = Convert.ToDateTime(clsProfile.prm_tokendatetime);
            imgProfilePhoto.ImageUrl = "~/Document/UserProfile/" + strMemberKey + "/Photograph.jpg";
        }
        else { lblMessage.Text = "Profile data not found if your are first time so please fill detail and click save button."; }
        Guid objKey = new Guid(strMemberKey);
        clsProfile.GetRecordByMemberKeyInProperties(objKey.ToString());
        lblProfileHead.Text = clsProfile.prm_firstname + " " + clsProfile.prm_lastname + " 's Profile Management";
        return blnResult;
    }

    private Boolean ValidateDate(string strValue)
    {
        Match vldDate = Regex.Match(strValue, "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$");
        return vldDate.Success;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        { SetValue(); EditProfile(); }

    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        if (!ValidateDate(txtDOB.Text.Trim()))
        { lblMessage.Text = "Invalid Date of Birth"; return; }
        if (!ValidateDate(txtIntakedate.Text.Trim()))
        { lblMessage.Text = "Invalid Intake Date"; return; }

        if (txtFirstname.Text.Trim() == "")
        { lblMessage.Text = "Please Enter First Name"; return; }
        if (txtMiddlename.Text.Trim() == "")
        { lblMessage.Text = "Please Enter Middle Name"; return; }
        if (txtLastname.Text.Trim() == "")
        { lblMessage.Text = "Please Enter Last Name"; return; }
        if (txtSSN.Text.Trim() == "")
        { lblMessage.Text = "Please Enter SSN No"; return; }

        DateTime dtBirthDate;
        if (!(DateTime.TryParseExact(txtDOB.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtBirthDate)))
        {
            lblMessage.Text = "Invalid Birth Date format. Please enter in mm/dd/yyyy"; return;
        }
        DateTime dtIntekdate;
        if (!(DateTime.TryParseExact(txtIntakedate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtIntekdate)))
        {
            lblMessage.Text = "Invalid Intake Date format. Please enter in mm/dd/yyyy"; return;
        }

        SetValue();
        clsProfile.GetRecordByMemberKeyInProperties(strMemberKey);
        clsProfile.prm_claimidprefix = 0;
        clsProfile.prm_dob = dtBirthDate;
        clsProfile.prm_intakedate = dtIntekdate;
        clsProfile.prm_emailid = txtEmail.Text.Trim();
        clsProfile.prm_emailverify = true;
        clsProfile.prm_empid = ddlEmployed.SelectedValue;
        clsProfile.prm_emr_add1 = txtemrAdd1.Text;
        clsProfile.prm_emr_add2 = txtemrAdd2.Text;
        clsProfile.prm_emr_addsameclnt = chkrfsAddsameasclnt.Checked;
        clsProfile.prm_emr_city = txtemrCity.Text;
        clsProfile.prm_emr_firstname = txtemrFirstname.Text;
        clsProfile.prm_emr_lastname = txtemrLastname.Text;
        clsProfile.prm_emr_phoneno = txtemrPhone.Text;
        clsProfile.prm_emr_phonetype = ddlemrPhonetype.SelectedName;
        clsProfile.prm_emr_relationship = ddlemrRelation.SelectedValue;
        clsProfile.prm_emr_state = ddlemrState.SelectedValue;
        clsProfile.prm_emr_zipcode = txtemrZipcode.Text;
        clsProfile.prm_ethincity = ddlEthnicity.SelectedValue;
        clsProfile.prm_firstname = txtFirstname.Text.Trim();
        clsProfile.prm_gender = ddlGender.SelectedValue;
        //clsProfile.prm_id = 0;
        clsProfile.prm_identifiedclient = ddlIdentifiedclient.SelectedValue;
        clsProfile.prm_initial = "";
        clsProfile.prm_langpreferance = ddlLanguage.SelectedValue;
        clsProfile.prm_lastname = txtLastname.Text;
        clsProfile.prm_maritalstatus = 0;
        clsProfile.prm_middlename = txtMiddlename.Text.Trim();
        //clsProfile.prm_passwordreset = "";
        //clsProfile.prm_paymentoption = "";
        //clsProfile.prm_provider = 0;
        clsProfile.prm_rfs_add1 = txtrfsAdd1.Text.Trim();
        clsProfile.prm_rfs_add2 = txtrfsAdd2.Text;
        clsProfile.prm_rfs_addsameclnt = chkrfsAddsameasclnt.Checked;
        clsProfile.prm_rfs_city = txtrfsCity.Text;
        clsProfile.prm_rfs_firstname = txtrfsFirstname.Text;
        clsProfile.prm_rfs_lastname = txtrfsLastname.Text;
        //clsProfile.prm_rfs_note ="";
        clsProfile.prm_rfs_reltoclient = ddlrfsRelationship.SelectedValue;
        clsProfile.prm_rfs_state = ddlrfsState.SelectedValue;
        clsProfile.prm_rfs_zipcode = txtrfsZipcode.Text;
        clsProfile.prm_scd_firstname = txtscdFirstname.Text;
        clsProfile.prm_scd_lastname = txtscdLastname.Text;
        clsProfile.prm_scd_phoneno = txtscdPhone.Text;
        clsProfile.prm_scd_phonetype = ddlscdPhonetype.SelectedName;
        clsProfile.prm_scd_relationship = ddlscdRelationship.SelectedValue;
        clsProfile.prm_ssn = txtSSN.Text;
        clsProfile.prm_status = 1;
        clsProfile.prm_userId = strMemberKey;
        clsProfile.prm_user_Id = intUserID;
        clsProfile.prm_userrole = strUserRole[0].ToString();
        clsProfile.prm_student = chkStudent.Checked;
        clsProfile.prm_tokendatetime = dtTokendatetime;
        clsProfile.prm_tokenid = strTokenid;
        Boolean blnSave = clsProfile.Save();
        if (blnSave == true)
        { UploadPhoto(); lblMessage.Text = "Profile Saved Successfully."; }
        else { lblMessage.Text = "Profile Not Saved Please Try Again. " + clsProfile.GetSetErrorMessage; }
    }
    protected void cmdContinue_Click(object sender, EventArgs e)
    {
        clsASR.storcrd_stepid = 2;
        clsASR.stprcrd_userid =(Guid) Membership.GetUser().ProviderUserKey;
        Boolean stprslt = clsASR.Save();
        if (stprslt == true)
        {
            Response.Redirect("~/Client/Assessments/UserFamilyOrigion.aspx");
        }
        else
        {
            Response.Redirect("~/Client/Assessments/ClientIntialAssessment.aspx");
        }
    }
}