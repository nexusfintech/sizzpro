﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_Authorizationview : System.Web.UI.Page
{
    clsauthorizationMaster clsAM = new clsauthorizationMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptAutho.DataSource = clsAM.GetAllForSa();
        rptAutho.DataBind();
    }
}