﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsInitialAssessment
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsInitialAssessment()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsInitialAssessment()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _ini_id; }
            set
            {
                _ini_id = value;
                if (clsCMD.Parameters.Contains("@ini_id"))
                { clsCMD.Parameters["@ini_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ini_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _ini_id;
        public Int64 ini_id
        {
            get { return _ini_id; }
            set
            {
                _ini_id = value;
                if (clsCMD.Parameters.Contains("@ini_id"))
                { clsCMD.Parameters["@ini_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ini_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _ini_parentid;
        public Int64 ini_parentid
        {
            get { return _ini_parentid; }
            set
            {
                _ini_parentid = value;
                if (clsCMD.Parameters.Contains("@ini_parentid"))
                { clsCMD.Parameters["@ini_parentid"].Value = value; }
                else { clsCMD.Parameters.Add("@ini_parentid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _ini_userid;
        public Guid ini_userid
        {
            get { return _ini_userid; }
            set
            {
                _ini_userid = value;
                if (clsCMD.Parameters.Contains("@ini_userid"))
                { clsCMD.Parameters["@ini_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@ini_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _ini_valueflag;
        public String ini_valueflag
        {
            get { return _ini_valueflag; }
            set
            {
                _ini_valueflag = value;
                if (clsCMD.Parameters.Contains("@ini_valueflag"))
                { clsCMD.Parameters["@ini_valueflag"].Value = value; }
                else { clsCMD.Parameters.Add("@ini_valueflag", SqlDbType.NVarChar, 1000).Value = value; }
            }
        }
        private DateTime _ini_valuedate;
        public DateTime ini_valuedate
        {
            get { return _ini_valuedate; }
            set
            {
                _ini_valuedate = value;
                if (clsCMD.Parameters.Contains("@ini_valuedate"))
                { clsCMD.Parameters["@ini_valuedate"].Value = value; }
                else { clsCMD.Parameters.Add("@ini_valuedate", SqlDbType.DateTime).Value = value; }
            }
        }
        private Int64 _ini_valueint;
        public Int64 ini_valueint
        {
            get { return _ini_valueint; }
            set
            {
                _ini_valueint = value;
                if (clsCMD.Parameters.Contains("@ini_valueint"))
                { clsCMD.Parameters["@ini_valueint"].Value = value; }
                else { clsCMD.Parameters.Add("@ini_valueint", SqlDbType.BigInt).Value = value; }
            }
        }
        private decimal _ini_valuedouble;
        public decimal ini_valuedouble
        {
            get { return _ini_valuedouble; }
            set
            {
                _ini_valuedouble = value;
                if (clsCMD.Parameters.Contains("@ini_valuedouble"))
                { clsCMD.Parameters["@ini_valuedouble"].Value = value; }
                else { clsCMD.Parameters.Add("@ini_valuedouble", SqlDbType.Decimal).Value = value; }
            }
        }
        private Boolean _ini_valuebit;
        public Boolean ini_valuebit
        {
            get { return _ini_valuebit; }
            set
            {
                _ini_valuebit = value;
                if (clsCMD.Parameters.Contains("@ini_valuebit"))
                { clsCMD.Parameters["@ini_valuebit"].Value = value; }
                else { clsCMD.Parameters.Add("@ini_valuebit", SqlDbType.Bit).Value = value; }
            }
        }
        private String _ini_valuetext;
        public String ini_valuetext
        {
            get { return _ini_valuetext; }
            set
            {
                _ini_valuetext = value;
                if (clsCMD.Parameters.Contains("@ini_valuetext"))
                { clsCMD.Parameters["@ini_valuetext"].Size = value.Length; clsCMD.Parameters["@ini_valuetext"].Value = value; }
                else { clsCMD.Parameters.Add("@ini_valuetext", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                ini_id = Convert.ToInt64(dr["ini_id"]);
                ini_parentid = Convert.ToInt64(dr["ini_parentid"]);
                ini_userid = new Guid(dr["ini_userid"].ToString());
                ini_valueflag = Convert.ToString(dr["ini_valueflag"]);
                ini_valuedate = Convert.ToDateTime(dr["ini_valuedate"]);
                ini_valueint = Convert.ToInt64(dr["ini_valueint"]);
                ini_valuedouble = Convert.ToDecimal(dr["ini_valuedouble"]);
                ini_valuebit = Convert.ToBoolean(dr["ini_valuebit"]);
                ini_valuetext = Convert.ToString(dr["ini_valuetext"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            ini_id = Convert.ToInt64("0");
            ini_parentid = Convert.ToInt64("0");
            ini_userid = new Guid();
            ini_valueflag = Convert.ToString("NA");
            ini_valuedate = Convert.ToDateTime(DateTime.Now);
            ini_valueint = Convert.ToInt64("0");
            ini_valuedouble = Convert.ToDecimal("0");
            ini_valuebit = Convert.ToBoolean(false);
            ini_valuetext = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
       }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.ini_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_initialassessmentsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
                CloseConnection();
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 ini_id)
        {
            Boolean blnResult = false;
            this.ini_id = ini_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_initialassessmentsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
                CloseConnection();
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 ini_id)
        {
            Boolean blnResult = false;
            this.ini_id = ini_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_initialassessmentsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
                CloseConnection();
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_initialassessmentsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
                CloseConnection();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsInitialAssessment_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsInitialAssessment_PropertiesList> lstResult = new List<clsInitialAssessment_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].ini_id = Convert.ToInt64(dr["ini_id"]);
                lstResult[i].ini_parentid = Convert.ToInt64(dr["ini_parentid"]);
                lstResult[i].ini_userid = new Guid(dr["ini_userid"].ToString());
                lstResult[i].ini_valueflag = Convert.ToString(dr["ini_valueflag"]);
                lstResult[i].ini_valuedate = Convert.ToDateTime(dr["ini_valuedate"]);
                lstResult[i].ini_valueint = Convert.ToInt64(dr["ini_valueint"]);
                lstResult[i].ini_valuedouble = Convert.ToDecimal(dr["ini_valuedouble"]);
                lstResult[i].ini_valuebit = Convert.ToBoolean(dr["ini_valuebit"]);
                lstResult[i].ini_valuetext = Convert.ToString(dr["ini_valuetext"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 ini_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.ini_id = ini_id;
                clsCMD.CommandText = "sp_initialassessmentsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
                CloseConnection();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean GetRecordUseridFlagInProperties(Guid Userid,String strFlag)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.ini_userid = Userid;
                this.ini_valueflag = strFlag;
                clsCMD.CommandText = "sp_initialassessmentsGetData";
                SetGetSPFlag = "BYUSERIDFLAG";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
                CloseConnection();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 ini_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.ini_id = ini_id;
                clsCMD.CommandText = "sp_initialassessmentsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
                CloseConnection();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsInitialAssessment_PropertiesList
    {
        public Int64 ini_id { get; set; }
        public Int64 ini_parentid { get; set; }
        public Guid ini_userid { get; set; }
        public String ini_valueflag { get; set; }
        public DateTime ini_valuedate { get; set; }
        public Int64 ini_valueint { get; set; }
        public Decimal ini_valuedouble { get; set; }
        public Boolean ini_valuebit { get; set; }
        public String ini_valuetext { get; set; }
    }
}