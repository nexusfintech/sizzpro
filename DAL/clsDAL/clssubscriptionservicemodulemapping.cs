﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clssubscriptionservicemodulemapping
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clssubscriptionservicemodulemapping()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clssubscriptionservicemodulemapping()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _smm_id; }
            set
            {
                _smm_id = value;
                if (clsCMD.Parameters.Contains("@smm_id"))
                { clsCMD.Parameters["@smm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@smm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _smm_id;
        public Int64 smm_id
        {
            get { return _smm_id; }
            set
            {
                _smm_id = value;
                if (clsCMD.Parameters.Contains("@smm_id"))
                { clsCMD.Parameters["@smm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@smm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _smm_serviceid;
        public Int64 smm_serviceid
        {
            get { return _smm_serviceid; }
            set
            {
                _smm_serviceid = value;
                if (clsCMD.Parameters.Contains("@smm_serviceid"))
                { clsCMD.Parameters["@smm_serviceid"].Value = value; }
                else { clsCMD.Parameters.Add("@smm_serviceid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _smm_moduleid;
        public Int64 smm_moduleid
        {
            get { return _smm_moduleid; }
            set
            {
                _smm_moduleid = value;
                if (clsCMD.Parameters.Contains("@smm_moduleid"))
                { clsCMD.Parameters["@smm_moduleid"].Value = value; }
                else { clsCMD.Parameters.Add("@smm_moduleid", SqlDbType.BigInt).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                smm_id = Convert.ToInt64(dr["smm_id"]);
                smm_serviceid = Convert.ToInt64(dr["smm_serviceid"]);
                smm_moduleid = Convert.ToInt64(dr["smm_moduleid"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            smm_id = Convert.ToInt64("0");
            smm_serviceid = Convert.ToInt64("0");
            smm_moduleid = Convert.ToInt64("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.smm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_subscriptionservicemodulemappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 smm_id)
        {
            Boolean blnResult = false;
            this.smm_id = smm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_subscriptionservicemodulemappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 smm_id)
        {
            Boolean blnResult = false;
            this.smm_id = smm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_subscriptionservicemodulemappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_subscriptionservicemodulemappingGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByServiceId(Int64 serviceid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.smm_serviceid = serviceid;
                clsCMD.CommandText = "sp_subscriptionservicemodulemappingGetData";
                SetGetSPFlag = "BYSERVICEID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetNotMapedModule(Int64 serviceid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.smm_serviceid = serviceid;
                clsCMD.CommandText = "sp_subscriptionservicemodulemappingGetData";
                SetGetSPFlag = "NOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clssubscriptionservicemodulemapping_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clssubscriptionservicemodulemapping_PropertiesList> lstResult = new List<clssubscriptionservicemodulemapping_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].smm_id = Convert.ToInt64(dr["smm_id"]);
                lstResult[i].smm_serviceid = Convert.ToInt64(dr["smm_serviceid"]);
                lstResult[i].smm_moduleid = Convert.ToInt64(dr["smm_moduleid"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 smm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.smm_id = smm_id;
                clsCMD.CommandText = "sp_subscriptionservicemodulemappingGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 smm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.smm_id = smm_id;
                clsCMD.CommandText = "sp_subscriptionservicemodulemappingGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clssubscriptionservicemodulemapping_PropertiesList
    {
        public Int64 smm_id { get; set; }
        public Int64 smm_serviceid { get; set; }
        public Int64 smm_moduleid { get; set; }
    }
}