﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="Authorizationview.aspx.cs" Inherits="Master_Authorizationview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
         <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
          <%--  <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn" Text="Add New Authorization" OnClick="cmdCreateNew_Click" />
                </a>
            </li>--%>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Authorization Master
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptAutho" runat="server">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Organization</th>
                                        <th>Code</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Auhtorised Session</th>
                                        <th>CPT Code</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("op_organizationname") %></td>
                                <td><%# Eval("auth_authorizationnumber") %></td>
                                <td><%# Eval("auth_startdate","{0:dd/MM/yyyy}") %></td>
                                <td><%# Eval("auth_enddate","{0:dd/MM/yyyy}") %></td>
                                <td><%# Eval("auth_authorisedsession") %></td>
                                <td><%# Eval("cd_code") %></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

