﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL.Conn;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using clsDAL;
public partial class Admin_Home : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection();
    SqlCommand cmd = new SqlCommand();


    int noOfmember = 0, noOfuser = 0, noOfclient = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        MySession.Current.noOfAdmin = getData("select count(*) as Administrator from UsersInRoles where RoleId='79749ee8-bf99-4513-b215-0fecf863391a'");
        MySession.Current.noOfmember = getData("select count(*) as Members from UsersInRoles where RoleId='eacdbc85-9798-4c55-be2a-d9ed59d774d2'");
        MySession.Current.noOfuser = getData("select count(*) as Users from UsersInRoles where RoleId='40a02901-3969-4275-9a47-477c13ed7fda'");
        MySession.Current.noOfclient = getData("select count(*) as Cliens from UsersInRoles where RoleId='80de9b84-62be-4432-811a-f1668259d0bd'");
        MySession.Current.noOfserv = getData(" select COUNT (*) from tbl_AppointmentCourse");
        if (!IsPostBack)
        {

        }
    }


    public int getData(string qry)
    {
        OpenConnection();
        int cnt = 0;
        cmd.CommandText = qry;
        cmd.Connection = con;
        cnt = Convert.ToInt32(cmd.ExecuteScalar());
        CloseConnection();
        return cnt;
    }

    private void OpenConnection()
    {
        clsConnection clsSQLCon = new clsConnection();
        if (con.State == ConnectionState.Closed)
        {
            if (!clsSQLCon.GetConnection())
            {
                //GetSetErrorMessage = "Database Server Connection Failed...!!";
            }
            else { this.con = clsSQLCon.SqlDBCon; }
        }
    }
    private void CloseConnection()
    {
        if (con.State == ConnectionState.Open)
        { con.Close(); }
    }
    public Boolean GetConnection()
    {
        Boolean blnResult = false;
        try
        {
            //SqlDBCon.ConnectionString = @"Data Source=ADMIN\SQLEXPRESS;Initial Catalog=dbshp;Integrated Security=False;User ID=sa;Password=12345";
            //SqlDBCon.ConnectionString = @"Data Source=ADMIN\SQLEXPRESS;Initial Catalog=dbshp;Integrated Security=False;User ID=sa;Password=12345;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";
            con.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ToString();
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
                blnResult = true;
            }
        }
        catch (Exception ex)
        {
            blnResult = false;
            //strGeneratedError = ex.Message;
        }
        return blnResult;
    }

}