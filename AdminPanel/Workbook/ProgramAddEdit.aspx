﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProgramAddEdit.aspx.cs" Inherits="Workbook_ProgramAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkCreateNew" runat="server" NavigateUrl="~/Workbook/ProgramMaster.aspx"><i class="icon-retweet"></i>&nbsp;Programs</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
         <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        </div>
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Program Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <% if (User.IsInRole("administrator"))
                           { %>
                        <div class="control-group">
                            <label for="textfield" class="control-label"><%= MySession.Current.MemberAlias %></label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectMember" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                <span class="help-block">Select Creater for whom Programm Is created</span>
                            </div>
                        </div>
                        <%} %>  
                        <asp:HiddenField runat="server" ID="prgcd"/>
                        <asp:HiddenField runat="server" ID="prgsts"/>
                        <div class="control-group">
                            <label for="txtProgramName" class="control-label">Program Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtProgramName" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtProgramShort" class="control-label">Program Short</label>
                            <div class="controls">
                                <asp:TextBox ID="txtProgramShort" runat="server" CssClass="input-medium"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtDescription" class="control-label">Description</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDescription" runat="server" class='ckeditor' TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" runat="server" CssClass="btn btn-primary"  OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                            <asp:LinkButton ID="cmdCancel" runat="server" OnClick="cmdCancel_Click" CssClass="btn btn-cancle"><i class="icon-remove"></i> Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

