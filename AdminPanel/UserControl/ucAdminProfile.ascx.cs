﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;

public partial class UserControl_ucAdminProfile : System.Web.UI.UserControl
{
    clsProfileMaster clsProfile = new clsProfileMaster();
    clsUsers clsUsr = new clsUsers();
    clsOrganizationalProfile clsORP = new clsOrganizationalProfile();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();

    private string strMemberKey;
    private string[] strUserRole;
    private static string strTokenid;
    private static DateTime dtTokendatetime;
    private Int64 intUserID;


    public String MemberKey
    {
        get { return strMemberKey; }
        set { strMemberKey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberKey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
        strUserRole = Roles.GetRolesForUser(mu.UserName);
    }

    private void UploadPhoto()
    {
        int intFileSizeLimit = 1024;
        string strServerDirPath = Server.MapPath("~/Document/UserProfile/" + strMemberKey);
        string strPhotoGraphName = "Photograph.jpg";
        if (!Directory.Exists(strServerDirPath))
        {
            Directory.CreateDirectory(strServerDirPath);
        }
        string strExtensionName = Path.GetExtension(strServerDirPath + "/" + strPhotoGraphName);
        int intFileSize = fuprofilephoto.PostedFile.ContentLength / 1024;
        if (intFileSize < intFileSizeLimit)
        {
            if (strExtensionName.Equals(".jpg"))
            {
                if (fuprofilephoto.PostedFile.FileName != "")
                {
                    fuprofilephoto.PostedFile.SaveAs(strServerDirPath + "/" + strPhotoGraphName);
                }
                imgProfilePhoto.ImageUrl = "~/Document/UserProfile/" + strMemberKey + "/Photograph.jpg";
            }
            else
            {
                lblMessage.Text = "Only .jpg file are allowed, try again!";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        else { lblMessage.Text = "File size exceeded than limit " + intFileSizeLimit + " KB, Please upload smaller file."; lblMessage.ForeColor = System.Drawing.Color.Red; }

    }

    private Boolean EditProfile()
    {
        Boolean blnResult = false;
        blnResult = clsProfile.GetRecordByMemberKeyInProperties(strMemberKey);
        if (blnResult == true)
        {
            clsProfile.prm_claimidprefix = 0;
            txtDOB.Text = clsProfile.prm_dob.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            txtEmail.Text = clsProfile.prm_emailid;
            txtFirstname.Text = clsProfile.prm_firstname;
            clsProfile.prm_initial = "";
            txtLastname.Text = clsProfile.prm_lastname;
            txtMiddlename.Text = clsProfile.prm_middlename;
            txtSSN.Text = clsProfile.prm_ssn;
            strTokenid = clsProfile.prm_tokenid;
            dtTokendatetime = Convert.ToDateTime(clsProfile.prm_tokendatetime);
            imgProfilePhoto.ImageUrl = "~/Document/UserProfile/" + strMemberKey + "/Photograph.jpg";
            Boolean blnRslt = clsORP.GetRecordByMemberKey(new Guid(strMemberKey));
            if (blnRslt == true)
            {
                txtOrgName.Text = clsORP.op_organizationname;
                txtOfcAddress.Text = clsORP.op_address;
                txtLglAddress.Text = clsORP.op_legaladdress;
                txtlglName.Text = clsORP.op_legalname;
                txtOfcPhone.Text = clsORP.op_officephone;
                txtFax.Text = clsORP.op_fax;
                txtTaxId.Text = clsORP.op_taxid;
            }
        }
        else
        {
            lblMessage.Text = "Profile data not found if your are first time so please fill detail and click save button.";
        }
        //Guid objKey = new Guid(strMemberKey);
        //clsProfile.GetRecordByMemberKeyInProperties(objKey.ToString());
        lblProfileHead.Text = clsProfile.prm_firstname + " " + clsProfile.prm_lastname + " 's Profile Management";
        return blnResult;
    }

    private Boolean ValidateDate(string strValue)
    {
        Match vldDate = Regex.Match(strValue, "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$");
        return vldDate.Success;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Boolean isRcrdFound = clsASR.GetRecordByStepID(2, new Guid(MemberKey));
            if (isRcrdFound)
            {
            }
            SetValue();
            EditProfile();
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        if (!ValidateDate(txtDOB.Text.Trim()))
        { lblMessage.Text = "Invalid Date of Birth"; return; }
        if (txtFirstname.Text.Trim() == "")
        { lblMessage.Text = "Please Input First Name"; return; }
        if (txtMiddlename.Text.Trim() == "")
        { lblMessage.Text = "Please Input Middle Name"; return; }
        if (txtLastname.Text.Trim() == "")
        { lblMessage.Text = "Please Input Last Name"; return; }
        if (txtSSN.Text.Trim() == "")
        { lblMessage.Text = "Please Input SSN No"; return; }

        DateTime dtBirthDate;
        if (!(DateTime.TryParseExact(txtDOB.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtBirthDate)))
        {
            lblMessage.Text = "Invalid Birth Date format please enter in mm/dd/yyyy"; return;
        }
        SetValue();
        clsProfile.GetRecordByMemberKeyInProperties(strMemberKey);
        clsProfile.prm_claimidprefix = 0;
        clsProfile.prm_dob = dtBirthDate;
        clsProfile.prm_emailid = txtEmail.Text.Trim();
        clsProfile.prm_emailverify = true;
        clsProfile.prm_firstname = txtFirstname.Text.Trim();
        clsProfile.prm_initial = "";
        clsProfile.prm_lastname = txtLastname.Text;
        clsProfile.prm_maritalstatus = 0;
        clsProfile.prm_middlename = txtMiddlename.Text.Trim();
        clsProfile.prm_ssn = txtSSN.Text;
        clsProfile.prm_status = 1;
        clsProfile.prm_userId = strMemberKey;
        clsProfile.prm_user_Id = intUserID;
        clsProfile.prm_userrole = strUserRole[0].ToString();
        clsProfile.prm_tokendatetime = dtTokendatetime;
        clsProfile.prm_tokenid = strTokenid;
        Boolean blnSave = clsProfile.UpdateByMemberKey(strMemberKey);
        if (blnSave == true)
        {
            UploadPhoto();
            lblMessage.Text = "Profile Save Successfully.";
        }
        else
        {
            lblMessage.Text = "Profile Not Saved Please Try Again." + clsProfile.GetSetErrorMessage;
        }
    }
    protected void lnkOrgSave_Click(object sender, EventArgs e)
    {
        if (txtOrgName.Text.Trim() == "")
        {
            lblMessage.Text = "Please Input Organisation Name";
            return;
        }
        if (txtOfcAddress.Text.Trim() == string.Empty)
        {
            lblMessage.Text = "Please Input Office Address";
            return;
        }
        if (txtOfcPhone.Text.Trim() == string.Empty)
        {
            lblMessage.Text = "Please Input Office Phone";
            return;
        }

        clsORP.op_userid = new Guid(MemberKey);
        clsORP.op_organizationname = txtOrgName.Text;
        clsORP.op_address = txtOfcAddress.Text;
        clsORP.op_legaladdress = txtLglAddress.Text;
        clsORP.op_legalname = txtlglName.Text;
        clsORP.op_officephone = txtOfcPhone.Text;
        clsORP.op_fax = txtFax.Text;
        clsORP.op_taxid = txtTaxId.Text;
        Boolean blnSvRslt = clsORP.Save();
        if(blnSvRslt)
        {
            lblMessage.Text = "Profile Save Successfully.";
            return;
        }
        else
        {
            lblMessage.Text = "Profile Not Saved Please Try Again." + clsORP.GetSetErrorMessage;
        }
    }
}