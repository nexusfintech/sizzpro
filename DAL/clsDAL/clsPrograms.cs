﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsPrograms
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }
        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsPrograms()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsPrograms()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _prg_id; }
            set
            {
                _prg_id = value;
                if (clsCMD.Parameters.Contains("@prg_id"))
                { clsCMD.Parameters["@prg_id"].Value = value; }
                else { clsCMD.Parameters.Add("@prg_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _prg_id;
        public Int64 prg_id
        {
            get { return _prg_id; }
            set
            {
                _prg_id = value;
                if (clsCMD.Parameters.Contains("@prg_id"))
                { clsCMD.Parameters["@prg_id"].Value = value; }
                else { clsCMD.Parameters.Add("@prg_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _prg_code;
        public Guid prg_code
        {
            get { return _prg_code; }
            set
            {
                _prg_code = value;
                if (clsCMD.Parameters.Contains("@prg_code"))
                { clsCMD.Parameters["@prg_code"].Value = value; }
                else { clsCMD.Parameters.Add("@prg_code", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _prg_name;
        public String prg_name
        {
            get { return _prg_name; }
            set
            {
                _prg_name = value;
                if (clsCMD.Parameters.Contains("@prg_name"))
                { clsCMD.Parameters["@prg_name"].Value = value; }
                else { clsCMD.Parameters.Add("@prg_name", SqlDbType.NVarChar, 1024).Value = value; }
            }
        }
        private String _prg_short;
        public String prg_short
        {
            get { return _prg_short; }
            set
            {
                _prg_short = value;
                if (clsCMD.Parameters.Contains("@prg_short"))
                { clsCMD.Parameters["@prg_short"].Value = value; }
                else { clsCMD.Parameters.Add("@prg_short", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prg_description;
        public String prg_description
        {
            get { return _prg_description; }
            set
            {
                _prg_description = value;
                if (clsCMD.Parameters.Contains("@prg_description"))
                { clsCMD.Parameters["@prg_description"].Size = value.Length; clsCMD.Parameters["@prg_description"].Value = value; }
                else { clsCMD.Parameters.Add("@prg_description", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Guid _prg_UserId;
        public Guid prg_UserId
        {
            get { return _prg_UserId; }
            set
            {
                _prg_UserId = value;
                if (clsCMD.Parameters.Contains("@prg_UserId"))
                { clsCMD.Parameters["@prg_UserId"].Value = value; }
                else { clsCMD.Parameters.Add("@prg_UserId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _prg_version;
        public Int64 prg_version
        {
            get { return _prg_version; }
            set
            {
                _prg_version = value;
                if (clsCMD.Parameters.Contains("@prg_version"))
                { clsCMD.Parameters["@prg_version"].Value = value; }
                else { clsCMD.Parameters.Add("@prg_version", SqlDbType.BigInt).Value = value; }
            }
        }
        private Boolean _prg_sts;
        public Boolean prg_sts
        {
            get { return _prg_sts; }
            set
            {
                _prg_sts = value;
                if (clsCMD.Parameters.Contains("@prg_sts"))
                { clsCMD.Parameters["@prg_sts"].Value = value; }
                else { clsCMD.Parameters.Add("@prg_sts", SqlDbType.Bit).Value = value; }
            }
        }
        private Guid _prg_OwnerId;
        public Guid prg_OwnerId
        {
            get { return _prg_OwnerId; }
            set
            {
                _prg_OwnerId = value;
                if (clsCMD.Parameters.Contains("@prg_OwnerId"))
                { clsCMD.Parameters["@prg_OwnerId"].Value = value; }
                else { clsCMD.Parameters.Add("@prg_OwnerId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                prg_id = Convert.ToInt64(dr["prg_id"]);
                prg_code = new Guid(dr["prg_code"].ToString());
                prg_name = Convert.ToString(dr["prg_name"]);
                prg_short = Convert.ToString(dr["prg_short"]);
                prg_description = Convert.ToString(dr["prg_description"]);
                prg_UserId = new Guid(dr["prg_UserId"].ToString());
                prg_OwnerId = new Guid(dr["prg_OwnerId"].ToString());
                prg_version = Convert.ToInt64(dr["prg_version"]);
                prg_sts = Convert.ToBoolean(dr["prg_sts"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            prg_id = Convert.ToInt64("0");
            prg_code = new Guid();
            prg_name = Convert.ToString("NA");
            prg_short = Convert.ToString("NA");
            prg_description = Convert.ToString("NA");
            prg_UserId = new Guid();
            prg_OwnerId = new Guid();
            prg_sts = false;
            prg_version = 0;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.prg_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "programsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 prg_id)
        {
            Boolean blnResult = false;
            this.prg_id = prg_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "programsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 prg_id)
        {
            Boolean blnResult = false;
            this.prg_id = prg_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "programsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..
        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "programsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }
        public DataTable GetAllRecordByMember(Guid MemberId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "programsGetData";
                this.prg_UserId = MemberId;
                SetGetSPFlag = "ALLBYMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }
        public DataTable GetAllRecordByUser(Guid UserId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "programsGetData";
                this.prg_UserId = UserId;
                SetGetSPFlag = "ALLBYUSER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }
        public List<clsPrograms_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsPrograms_PropertiesList> lstResult = new List<clsPrograms_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].prg_id = Convert.ToInt64(dr["prg_id"]);
                lstResult[i].prg_name = Convert.ToString(dr["prg_name"]);
                lstResult[i].prg_short = Convert.ToString(dr["prg_short"]);
                lstResult[i].prg_description = Convert.ToString(dr["prg_description"]);
                lstResult[i].prg_OwnerId = new Guid((dr["prg_OwnerId"].ToString)());
                lstResult[i].prg_UserId = new Guid(dr["prg_UserId"].ToString());
                lstResult[i].prg_sts = Convert.ToBoolean(dr["prg_sts"]);
                lstResult[i].prg_version = Convert.ToInt64(dr["prg_version"]);
                lstResult[i].prg_code = new Guid(dr["prg_code"].ToString());
                i++;
            }
            return lstResult;
        }
        public Boolean GetRecordByIDInProperties(Int64 prg_id)
        {
            Boolean blnResult = false;
            try
            {
                this.prg_id = prg_id;
                clsCMD.CommandText = "programsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean GetRecordByCodeInProperties(Guid prgcode)
        {
            Boolean blnResult = false;
            try
            {
                this.prg_code = prgcode;
                clsCMD.CommandText = "programsGetData";
                SetGetSPFlag = "BYCODE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }
        public Boolean CheckDuplicateRow(Guid prg_code)
        {
            Boolean blnResult = false;
            try
            {
                this.prg_code = prg_code; ;
                clsCMD.CommandText = "programsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Int64 GetProgrammVersion(Guid PrgCode)
        {
            OpenConnection();
            Int64 Version = 0;
            try
            {
                this.prg_code = PrgCode;
                clsCMD.CommandText = "programsGetData";
                SetGetSPFlag = "FINDVER";
                Version = Convert.ToInt64(clsCMD.ExecuteScalar());
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                Version = -1;
            }
            return Version;
        }
        public Boolean ChangeStatus(Int64 prgid, Boolean status, Int64 ver)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.prg_id = prgid;
            this.prg_sts = status;
            this.prg_version = ver;
            this.AddEditDeleteFlag = "CHANGESTS";
            try
            {
                clsCMD.CommandText = "programsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }
        #endregion
    }

    public class clsPrograms_PropertiesList
    {
        public Int64 prg_id { get; set; }
        public Guid prg_code { get; set; }
        public String prg_name { get; set; }
        public String prg_short { get; set; }
        public String prg_description { get; set; }
        public Guid prg_UserId { get; set; }
        public Guid prg_OwnerId { get; set; }
        public Boolean prg_sts { get; set; }
        public Int64 prg_version { get; set; }
    }
}