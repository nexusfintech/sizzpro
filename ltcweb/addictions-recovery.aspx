﻿<%@ Page Title="addictions-recovery" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="addictions-recovery.aspx.cs" Inherits="addictions_recovery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-question"></i>&nbsp;ADDICTIONS RECOVERY</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <p>The reason why it is often difficult to admit you have become addicted to something or someone is because you want to believe you have control over yourself. It is very painful to admit your behavior is out of control. You have become addicted to something when despite the negative consequences you continue the same behavior. It doesn't really matter the frequency of acting out. If you continue the behavior or thought despite negative consequences daily, weekly, monthly or whenever a specific trigger occurs it is an addiction. You can be addicted to a thought, a person, a substance, a behavior. Addictions are usually very destructive and often end in a premature death. That death can be a loss of life or it can be the loss of a relationship or precious opportunity in life. Addictions are destructive and if you recognize you are on the wrong path with a behavior or thought it is important for you to talk to someone who can help you work out of it.</p>
            <p><strong>Drug addiction is a complex illness</strong>&nbsp;characterized by intense and, at times, uncontrollable drug craving, along with compulsive drug seeking and use that persist even in the face of devastating consequences. While the path to drug addiction begins with the voluntary act of taking drugs, over time a person's ability to choose not to do so becomes compromised, and seeking and consuming the drug becomes compulsive. This behavior results largely from the effects of prolonged drug exposure on brain functioning. Addiction is a brain disease that affects multiple brain circuits, including those involved in reward and motivation, learning and memory, and inhibitory control over behavior. (NIH) </p>
            <p>Drug addiction is especially life threatening. Don't delay gettting help. Whatever your addiction find out more about our solution to your problem at <a href="http://www.RecoveringMyLife.com">RecoveringMyLife.com</a>. We would love to help you get to a better place in your life.</p>
            <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

