﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Workbook.aspx.cs" Inherits="shpweb_LifeProgram_Workbook" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h5>You are in, <a href="../Client/Dashboard.aspx">Home</a> &gt;&gt; 
                        <a href="Workbook.aspx">Workbook</a>
            </h5>
            <hr />
            <asp:Repeater ID="rptWorkBook" runat="server">
                <ItemTemplate>
                    <div class="row-fluid">
                        <div class="span12">
                            <h2><i class="icon-book"></i>&nbsp;<%#Eval("wrb_name") %></h2>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <p>
                                <%#Eval("wrb_description")%>
                            </p>
                        </div>
                    </div>
                   <%-- <div class="row-fluid">
                        <div class="span12">
                            <h2><i class="icon-font"></i>&nbsp;Description</h2>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span12">
                            <p>
                                <%#Eval("wrb_description")%>
                            </p>
                        </div>
                    </div>--%>
                    <%--    <div class="row-fluid">
                        <div class="span12">
                            <h2><i class="icon-archive"></i>&nbsp;Summary</h2>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span12">
                            <p>
                                <%#Eval("Summary")%>
                            </p>
                        </div>
                    </div>--%>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="span4">
                            </div>
                            <div class="span4" style="text-align: center;">
                                <a href="Chapters.aspx" name="view" id='<%#Eval("wrb_id") %>'>
                                    <img src="../img/goto.png" style="height: 100px;" />
                                </a>
                            </div>
                            <div class="span4">
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>

        </div>
    </div>
    <input type="hidden" id="hidden" name="hidden" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("[name='view']").click(function () {
                var id = this.id;
                $("#<%= hidden.ClientID %>").val(id);
                $("#form1").submit();
                return false;
            });
        });
    </script>
</asp:Content>
