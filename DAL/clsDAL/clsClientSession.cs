﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsClientSession
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsClientSession()
        {
            clsCMD.Parameters.Clear();
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsClientSession()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _ssn_id; }
            set
            {
                _ssn_id = value;
                if (clsCMD.Parameters.Contains("@ssn_id"))
                { clsCMD.Parameters["@ssn_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_id", SqlDbType.BigInt).Value = value; }
            }
        }

        public Int64 SCOPE_IDENTITY
        {
            get { return _ssn_id; }
            set
            {
                _ssn_id = value;
                if (clsCMD.Parameters.Contains("@PrimaryKey"))
                { clsCMD.Parameters["@PrimaryKey"].Direction = ParameterDirection.Output; }
                else { clsCMD.Parameters.Add("@PrimaryKey", SqlDbType.BigInt).Direction = ParameterDirection.Output;  }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _ssn_id;
        public Int64 ssn_id
        {
            get { return _ssn_id; }
            set
            {
                _ssn_id = value;
                if (clsCMD.Parameters.Contains("@ssn_id"))
                { clsCMD.Parameters["@ssn_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private DateTime _ssn_date;
        public DateTime ssn_date
        {
            get { return _ssn_date; }
            set
            {
                _ssn_date = value;
                if (clsCMD.Parameters.Contains("@ssn_date"))
                { clsCMD.Parameters["@ssn_date"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_date", SqlDbType.DateTime).Value = value; }
            }
        }
        private Guid _ssn_clientid;
        public Guid ssn_clientid
        {
            get { return _ssn_clientid; }
            set
            {
                _ssn_clientid = value;
                if (clsCMD.Parameters.Contains("@ssn_clientid"))
                { clsCMD.Parameters["@ssn_clientid"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_clientid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _ssn_itemid;
        public Int64 ssn_itemid
        {
            get { return _ssn_itemid; }
            set
            {
                _ssn_itemid = value;
                if (clsCMD.Parameters.Contains("@ssn_itemid"))
                { clsCMD.Parameters["@ssn_itemid"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_itemid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _ssn_fslid;
        public Int64 ssn_fslid
        {
            get { return _ssn_fslid; }
            set
            {
                _ssn_fslid = value;
                if (clsCMD.Parameters.Contains("@ssn_fslid"))
                { clsCMD.Parameters["@ssn_fslid"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_fslid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _ssn_proid;
        public Int64 ssn_proid
        {
            get { return _ssn_proid; }
            set
            {
                _ssn_proid = value;
                if (clsCMD.Parameters.Contains("@ssn_proid"))
                { clsCMD.Parameters["@ssn_proid"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_proid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Double _ssn_itempurchaseamt;
        public Double ssn_itempurchaseamt
        {
            get { return _ssn_itempurchaseamt; }
            set
            {
                _ssn_itempurchaseamt = value;
                if (clsCMD.Parameters.Contains("@ssn_itempurchaseamt"))
                { clsCMD.Parameters["@ssn_itempurchaseamt"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_itempurchaseamt", SqlDbType.Money).Value = value; }
            }
        }
        private String _ssn_payer;
        public String ssn_payer
        {
            get { return _ssn_payer; }
            set
            {
                _ssn_payer = value;
                if (clsCMD.Parameters.Contains("@ssn_payer"))
                { clsCMD.Parameters["@ssn_payer"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_payer", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 _ssn_totalHours;
        public Int64 ssn_totalHours
        {
            get { return _ssn_totalHours; }
            set
            {
                _ssn_totalHours = value;
                if (clsCMD.Parameters.Contains("@ssn_totalHours"))
                { clsCMD.Parameters["@ssn_totalHours"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_totalHours", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _ssn_clientnumber;
        public String ssn_clientnumber
        {
            get { return _ssn_clientnumber; }
            set
            {
                _ssn_clientnumber = value;
                if (clsCMD.Parameters.Contains("@ssn_clientnumber"))
                { clsCMD.Parameters["@ssn_clientnumber"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_clientnumber", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Double _ssn_copaydedrecd;
        public Double ssn_copaydedrecd
        {
            get { return _ssn_copaydedrecd; }
            set
            {
                _ssn_copaydedrecd = value;
                if (clsCMD.Parameters.Contains("@ssn_copaydedrecd"))
                { clsCMD.Parameters["@ssn_copaydedrecd"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_copaydedrecd", SqlDbType.Money).Value = value; }
            }
        }
        private DateTime _ssn_timein;
        public DateTime ssn_timein
        {
            get { return _ssn_timein; }
            set
            {
                _ssn_timein = value;
                if (clsCMD.Parameters.Contains("@ssn_timein"))
                { clsCMD.Parameters["@ssn_timein"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_timein", SqlDbType.DateTime).Value = value; }
            }
        }
        private DateTime _ssn_timeout;
        public DateTime ssn_timeout
        {
            get { return _ssn_timeout; }
            set
            {
                _ssn_timeout = value;
                if (clsCMD.Parameters.Contains("@ssn_timeout"))
                { clsCMD.Parameters["@ssn_timeout"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_timeout", SqlDbType.DateTime).Value = value; }
            }
        }
        private Double _ssn_clntpayrecd;
        public Double ssn_clntpayrecd
        {
            get { return _ssn_clntpayrecd; }
            set
            {
                _ssn_clntpayrecd = value;
                if (clsCMD.Parameters.Contains("@ssn_clntpayrecd"))
                { clsCMD.Parameters["@ssn_clntpayrecd"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_clntpayrecd", SqlDbType.Money).Value = value; }
            }
        }
        private Double _ssn_billettoclient;
        public Double ssn_billettoclient
        {
            get { return _ssn_billettoclient; }
            set
            {
                _ssn_billettoclient = value;
                if (clsCMD.Parameters.Contains("@ssn_billettoclient"))
                { clsCMD.Parameters["@ssn_billettoclient"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_billettoclient", SqlDbType.Money).Value = value; }
            }
        }
        private Double _ssn_billedtoinsurance;
        public Double ssn_billedtoinsurance
        {
            get { return _ssn_billedtoinsurance; }
            set
            {
                _ssn_billedtoinsurance = value;
                if (clsCMD.Parameters.Contains("@ssn_billedtoinsurance"))
                { clsCMD.Parameters["@ssn_billedtoinsurance"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_billedtoinsurance", SqlDbType.Money).Value = value; }
            }
        }
        private String _ssn_location;
        public String ssn_location
        {
            get { return _ssn_location; }
            set
            {
                _ssn_location = value;
                if (clsCMD.Parameters.Contains("@ssn_location"))
                { clsCMD.Parameters["@ssn_location"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_location", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private DateTime _ssn_insdatesubmit;
        public DateTime ssn_insdatesubmit
        {
            get { return _ssn_insdatesubmit; }
            set
            {
                _ssn_insdatesubmit = value;
                if (clsCMD.Parameters.Contains("@ssn_insdatesubmit"))
                { clsCMD.Parameters["@ssn_insdatesubmit"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_insdatesubmit", SqlDbType.DateTime).Value = value; }
            }
        }
        private String _ssn_insclmnumber;
        public String ssn_insclmnumber
        {
            get { return _ssn_insclmnumber; }
            set
            {
                _ssn_insclmnumber = value;
                if (clsCMD.Parameters.Contains("@ssn_insclmnumber"))
                { clsCMD.Parameters["@ssn_insclmnumber"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_insclmnumber", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ssn_insclmnote;
        public String ssn_insclmnote
        {
            get { return _ssn_insclmnote; }
            set
            {
                _ssn_insclmnote = value;
                if (clsCMD.Parameters.Contains("@ssn_insclmnote"))
                { clsCMD.Parameters["@ssn_insclmnote"].Size = value.Length; clsCMD.Parameters["@ssn_insclmnote"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_insclmnote", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Double _ssn_recdfrominsu;
        public Double ssn_recdfrominsu
        {
            get { return _ssn_recdfrominsu; }
            set
            {
                _ssn_recdfrominsu = value;
                if (clsCMD.Parameters.Contains("@ssn_recdfrominsu"))
                { clsCMD.Parameters["@ssn_recdfrominsu"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_recdfrominsu", SqlDbType.Money).Value = value; }
            }
        }
        private DateTime _ssn_datenotesubmit;
        public DateTime ssn_datenotesubmit
        {
            get { return _ssn_datenotesubmit; }
            set
            {
                _ssn_datenotesubmit = value;
                if (clsCMD.Parameters.Contains("@ssn_datenotesubmit"))
                { clsCMD.Parameters["@ssn_datenotesubmit"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_datenotesubmit", SqlDbType.DateTime).Value = value; }
            }
        }
        private Double _ssn_totalreceived;
        public Double ssn_totalreceived
        {
            get { return _ssn_totalreceived; }
            set
            {
                _ssn_totalreceived = value;
                if (clsCMD.Parameters.Contains("@ssn_totalreceived"))
                { clsCMD.Parameters["@ssn_totalreceived"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_totalreceived", SqlDbType.Money).Value = value; }
            }
        }
        private String _ssn_todaygoal;
        public String ssn_todaygoal
        {
            get { return _ssn_todaygoal; }
            set
            {
                _ssn_todaygoal = value;
                if (clsCMD.Parameters.Contains("@ssn_todaygoal"))
                { clsCMD.Parameters["@ssn_todaygoal"].Size = value.Length; clsCMD.Parameters["@ssn_todaygoal"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_todaygoal", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Double _ssn_submitcollection;
        public Double ssn_submitcollection
        {
            get { return _ssn_submitcollection; }
            set
            {
                _ssn_submitcollection = value;
                if (clsCMD.Parameters.Contains("@ssn_submitcollection"))
                { clsCMD.Parameters["@ssn_submitcollection"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_submitcollection", SqlDbType.Money).Value = value; }
            }
        }
        private Int64 _ssn_show;
        public Int64 ssn_show
        {
            get { return _ssn_show; }
            set
            {
                _ssn_show = value;
                if (clsCMD.Parameters.Contains("@ssn_show"))
                { clsCMD.Parameters["@ssn_show"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_show", SqlDbType.BigInt).Value = value; }
            }
        }
        private Double _ssn_recdfrmcollection;
        public Double ssn_recdfrmcollection
        {
            get { return _ssn_recdfrmcollection; }
            set
            {
                _ssn_recdfrmcollection = value;
                if (clsCMD.Parameters.Contains("@ssn_recdfrmcollection"))
                { clsCMD.Parameters["@ssn_recdfrmcollection"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_recdfrmcollection", SqlDbType.Money).Value = value; }
            }
        }
        private DateTime _ssn_nextappointment;
        public DateTime ssn_nextappointment
        {
            get { return _ssn_nextappointment; }
            set
            {
                _ssn_nextappointment = value;
                if (clsCMD.Parameters.Contains("@ssn_nextappointment"))
                { clsCMD.Parameters["@ssn_nextappointment"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_nextappointment", SqlDbType.DateTime).Value = value; }
            }
        }
        private Double _ssn_adjustwrite;
        public Double ssn_adjustwrite
        {
            get { return _ssn_adjustwrite; }
            set
            {
                _ssn_adjustwrite = value;
                if (clsCMD.Parameters.Contains("@ssn_adjustwrite"))
                { clsCMD.Parameters["@ssn_adjustwrite"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_adjustwrite", SqlDbType.Money).Value = value; }
            }
        }
        private String _ssn_sessiontype;
        public String ssn_sessiontype
        {
            get { return _ssn_sessiontype; }
            set
            {
                _ssn_sessiontype = value;
                if (clsCMD.Parameters.Contains("@ssn_sessiontype"))
                { clsCMD.Parameters["@ssn_sessiontype"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_sessiontype", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Double _ssn_totalcharged;
        public Double ssn_totalcharged
        {
            get { return _ssn_totalcharged; }
            set
            {
                _ssn_totalcharged = value;
                if (clsCMD.Parameters.Contains("@ssn_totalcharged"))
                { clsCMD.Parameters["@ssn_totalcharged"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_totalcharged", SqlDbType.Money).Value = value; }
            }
        }
        private Boolean _ssn_noteenter;
        public Boolean ssn_noteenter
        {
            get { return _ssn_noteenter; }
            set
            {
                _ssn_noteenter = value;
                if (clsCMD.Parameters.Contains("@ssn_noteenter"))
                { clsCMD.Parameters["@ssn_noteenter"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_noteenter", SqlDbType.Bit).Value = value; }
            }
        }
        private Double _ssn_totalpaid;
        public Double ssn_totalpaid
        {
            get { return _ssn_totalpaid; }
            set
            {
                _ssn_totalpaid = value;
                if (clsCMD.Parameters.Contains("@ssn_totalpaid"))
                { clsCMD.Parameters["@ssn_totalpaid"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_totalpaid", SqlDbType.Money).Value = value; }
            }
        }
        private Boolean _ssn_placedinvoce;
        public Boolean ssn_placedinvoce
        {
            get { return _ssn_placedinvoce; }
            set
            {
                _ssn_placedinvoce = value;
                if (clsCMD.Parameters.Contains("@ssn_placedinvoce"))
                { clsCMD.Parameters["@ssn_placedinvoce"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_placedinvoce", SqlDbType.Bit).Value = value; }
            }
        }
        private Double _ssn_balancedue;
        public Double ssn_balancedue
        {
            get { return _ssn_balancedue; }
            set
            {
                _ssn_balancedue = value;
                if (clsCMD.Parameters.Contains("@ssn_balancedue"))
                { clsCMD.Parameters["@ssn_balancedue"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_balancedue", SqlDbType.Money).Value = value; }
            }
        }
        private Double _ssn_cofacilitionfee;
        public Double ssn_cofacilitionfee
        {
            get { return _ssn_cofacilitionfee; }
            set
            {
                _ssn_cofacilitionfee = value;
                if (clsCMD.Parameters.Contains("@ssn_cofacilitionfee"))
                { clsCMD.Parameters["@ssn_cofacilitionfee"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_cofacilitionfee", SqlDbType.Money).Value = value; }
            }
        }
        private Double _ssn_therapist;
        public Double ssn_therapist
        {
            get { return _ssn_therapist; }
            set
            {
                _ssn_therapist = value;
                if (clsCMD.Parameters.Contains("@ssn_therapist"))
                { clsCMD.Parameters["@ssn_therapist"].Value = value; }
                else { clsCMD.Parameters.Add("@ssn_therapist", SqlDbType.Money).Value = value; }
            }
        }
        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                ssn_id = Convert.ToInt64(dr["ssn_id"]);
                ssn_date = Convert.ToDateTime(dr["ssn_date"]);
                ssn_clientid = new Guid(dr["ssn_clientid"].ToString());
                ssn_itemid = Convert.ToInt64(dr["ssn_itemid"]);
                ssn_fslid = Convert.ToInt64(dr["ssn_fslid"]);
                ssn_proid = Convert.ToInt64(dr["ssn_proid"]);
                ssn_itempurchaseamt = Convert.ToDouble(dr["ssn_itempurchaseamt"]);
                ssn_payer = Convert.ToString(dr["ssn_payer"]);
                ssn_totalHours = Convert.ToInt64(dr["ssn_totalHours"]);
                ssn_clientnumber = Convert.ToString(dr["ssn_clientnumber"]);
                ssn_copaydedrecd = Convert.ToDouble(dr["ssn_copaydedrecd"]);
                ssn_timein = Convert.ToDateTime(dr["ssn_timein"]);
                ssn_timeout = Convert.ToDateTime(dr["ssn_timeout"]);
                ssn_clntpayrecd = Convert.ToDouble(dr["ssn_clntpayrecd"]);
                ssn_billettoclient = Convert.ToDouble(dr["ssn_billettoclient"]);
                ssn_billedtoinsurance = Convert.ToDouble(dr["ssn_billedtoinsurance"]);
                ssn_location = Convert.ToString(dr["ssn_location"]);
                ssn_insdatesubmit = Convert.ToDateTime(dr["ssn_insdatesubmit"]);
                ssn_insclmnumber = Convert.ToString(dr["ssn_insclmnumber"]);
                ssn_insclmnote = Convert.ToString(dr["ssn_insclmnote"]);
                ssn_recdfrominsu = Convert.ToDouble(dr["ssn_recdfrominsu"]);
                ssn_datenotesubmit = Convert.ToDateTime(dr["ssn_datenotesubmit"]);
                ssn_totalreceived = Convert.ToDouble(dr["ssn_totalreceived"]);
                ssn_todaygoal = Convert.ToString(dr["ssn_todaygoal"]);
                ssn_submitcollection = Convert.ToDouble(dr["ssn_submitcollection"]);
                ssn_show = Convert.ToInt64(dr["ssn_show"]);
                ssn_recdfrmcollection = Convert.ToDouble(dr["ssn_recdfrmcollection"]);
                ssn_nextappointment = Convert.ToDateTime(dr["ssn_nextappointment"]);
                ssn_adjustwrite = Convert.ToDouble(dr["ssn_adjustwrite"]);
                ssn_sessiontype = Convert.ToString(dr["ssn_sessiontype"]);
                ssn_totalcharged = Convert.ToDouble(dr["ssn_totalcharged"]);
                ssn_noteenter = Convert.ToBoolean(dr["ssn_noteenter"]);
                ssn_totalpaid = Convert.ToDouble(dr["ssn_totalpaid"]);
                ssn_placedinvoce = Convert.ToBoolean(dr["ssn_placedinvoce"]);
                ssn_balancedue = Convert.ToDouble(dr["ssn_balancedue"]);
                ssn_cofacilitionfee = Convert.ToDouble(dr["ssn_cofacilitionfee"]);
                ssn_therapist = Convert.ToDouble(dr["ssn_therapist"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            ssn_id = Convert.ToInt64("0");
            ssn_date = Convert.ToDateTime(DateTime.Now);
            ssn_clientid = new Guid();
            ssn_itemid = Convert.ToInt64("0");
            ssn_fslid = Convert.ToInt64("0");
            ssn_proid = Convert.ToInt64("0");
            ssn_itempurchaseamt = Convert.ToDouble("0");
            ssn_payer = Convert.ToString("NA");
            ssn_totalHours = 0;
            ssn_clientnumber = Convert.ToString("NA");
            ssn_copaydedrecd = Convert.ToDouble("0");
            ssn_timein = Convert.ToDateTime(DateTime.Now);
            ssn_timeout = Convert.ToDateTime(DateTime.Now);
            ssn_clntpayrecd = 0;
            ssn_billettoclient = Convert.ToDouble("0");
            ssn_billedtoinsurance = Convert.ToDouble("0");
            ssn_location = Convert.ToString("NA");
            ssn_insdatesubmit = Convert.ToDateTime(DateTime.Now);
            ssn_insclmnumber = Convert.ToString("NA");
            ssn_insclmnote = Convert.ToString("NA");
            ssn_recdfrominsu = Convert.ToDouble("0");
            ssn_datenotesubmit = Convert.ToDateTime(DateTime.Now);
            ssn_totalreceived = Convert.ToDouble("0");
            ssn_todaygoal = Convert.ToString("NA");
            ssn_submitcollection = Convert.ToDouble("0");
            ssn_show = 0;
            ssn_recdfrmcollection = Convert.ToDouble("0");
            ssn_nextappointment = Convert.ToDateTime(DateTime.Now);
            ssn_adjustwrite = Convert.ToDouble("0");
            ssn_sessiontype = Convert.ToString("NA");
            ssn_totalcharged = Convert.ToDouble("0");
            ssn_noteenter = false;
            ssn_totalpaid = Convert.ToDouble("0");
            ssn_placedinvoce = false;
            ssn_balancedue = Convert.ToDouble("0");
            ssn_cofacilitionfee = Convert.ToDouble("0");
            ssn_therapist = Convert.ToDouble("0");
            SCOPE_IDENTITY = 0;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ssn_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "ClientSessionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; _ssn_id = Convert.ToInt64(clsCMD.Parameters["@PrimaryKey"].Value); }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 ssn_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ssn_id = ssn_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "ClientSessionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 ssn_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ssn_id = ssn_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "ClientSessionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion
       
        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "ClientSessionGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordClientWise()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "ClientSessionGetData";
                SetGetSPFlag = "BYCLIENTID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordDateRangeWise(DateTime dtFromDate,DateTime dtToDate)
        {
            OpenConnection();
            this.ssn_date = dtFromDate;
            this.ssn_datenotesubmit = dtToDate;
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "ClientSessionGetData";
                SetGetSPFlag = "BYDATEWISE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordItemWise(Int64 intItemID)
        {
            OpenConnection();
            this.ssn_itemid = intItemID;
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "ClientSessionGetData";
                SetGetSPFlag = "BYITEMWISE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordProviderWise(Int64 intProvID)
        {
            OpenConnection();
            this.ssn_proid = intProvID;
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "ClientSessionGetData";
                SetGetSPFlag = "BYPROVIDERWISE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordFacilitiesWise(Int64 intFSLID)
        {
            OpenConnection();
            this.ssn_fslid = intFSLID;
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "ClientSessionGetData";
                SetGetSPFlag = "BYFACILITIESWISE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsClientSession_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsClientSession_PropertiesList> lstResult = new List<clsClientSession_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].ssn_id = Convert.ToInt64(dr["ssn_id"]);
                lstResult[i].ssn_date = Convert.ToDateTime(dr["ssn_date"]);
                lstResult[i].ssn_clientid = new Guid(dr["ssn_clientid"].ToString());
                lstResult[i].ssn_itemid = Convert.ToInt64(dr["ssn_itemid"]);
                lstResult[i].ssn_fslid = Convert.ToInt64(dr["ssn_fslid"]);
                lstResult[i].ssn_proid = Convert.ToInt64(dr["ssn_proid"]);
                lstResult[i].ssn_itempurchaseamt = Convert.ToDouble(dr["ssn_itempurchaseamt"]);
                lstResult[i].ssn_payer = Convert.ToString(dr["ssn_payer"]);
                lstResult[i].ssn_totalHours = Convert.ToInt64(dr["ssn_totalHours"]);
                lstResult[i].ssn_clientnumber = Convert.ToString(dr["ssn_clientnumber"]);
                lstResult[i].ssn_copaydedrecd = Convert.ToDouble(dr["ssn_copaydedrecd"]);
                lstResult[i].ssn_timein = Convert.ToDateTime(dr["ssn_timein"]);
                lstResult[i].ssn_timeout = Convert.ToDateTime(dr["ssn_timeout"]);
                lstResult[i].ssn_clntpayrecd = Convert.ToDouble(dr["ssn_clntpayrecd"]);
                lstResult[i].ssn_billettoclient = Convert.ToDouble(dr["ssn_billettoclient"]);
                lstResult[i].ssn_billedtoinsurance = Convert.ToDouble(dr["ssn_billedtoinsurance"]);
                lstResult[i].ssn_location = Convert.ToString(dr["ssn_location"]);
                lstResult[i].ssn_insdatesubmit = Convert.ToDateTime(dr["ssn_insdatesubmit"]);
                lstResult[i].ssn_insclmnumber = Convert.ToString(dr["ssn_insclmnumber"]);
                lstResult[i].ssn_insclmnote = Convert.ToString(dr["ssn_insclmnote"]);
                lstResult[i].ssn_recdfrominsu = Convert.ToDouble(dr["ssn_recdfrominsu"]);
                lstResult[i].ssn_datenotesubmit = Convert.ToDateTime(dr["ssn_datenotesubmit"]);
                lstResult[i].ssn_totalreceived = Convert.ToDouble(dr["ssn_totalreceived"]);
                lstResult[i].ssn_todaygoal = Convert.ToString(dr["ssn_todaygoal"]);
                lstResult[i].ssn_submitcollection = Convert.ToDouble(dr["ssn_submitcollection"]);
                lstResult[i].ssn_show = Convert.ToInt64(dr["ssn_show"]);
                lstResult[i].ssn_recdfrmcollection = Convert.ToDouble(dr["ssn_recdfrmcollection"]);
                lstResult[i].ssn_nextappointment = Convert.ToDateTime(dr["ssn_nextappointment"]);
                lstResult[i].ssn_adjustwrite = Convert.ToDouble(dr["ssn_adjustwrite"]);
                lstResult[i].ssn_sessiontype = Convert.ToString(dr["ssn_sessiontype"]);
                lstResult[i].ssn_totalcharged = Convert.ToDouble(dr["ssn_totalcharged"]);
                lstResult[i].ssn_noteenter = Convert.ToBoolean(dr["ssn_noteenter"]);
                lstResult[i].ssn_totalpaid = Convert.ToDouble(dr["ssn_totalpaid"]);
                lstResult[i].ssn_placedinvoce = Convert.ToBoolean(dr["ssn_placedinvoce"]);
                lstResult[i].ssn_balancedue = Convert.ToDouble(dr["ssn_balancedue"]);
                lstResult[i].ssn_cofacilitionfee = Convert.ToDouble(dr["ssn_cofacilitionfee"]);
                lstResult[i].ssn_therapist = Convert.ToDouble(dr["ssn_therapist"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 ssn_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ssn_id = ssn_id;
                clsCMD.CommandText = "ClientSessionGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 ssn_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ssn_id = ssn_id;
                clsCMD.CommandText = "ClientSessionGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion
    }

    public class clsClientSession_PropertiesList
    {
        public Int64 ssn_id { get; set; }
        public DateTime ssn_date { get; set; }
        public Guid ssn_clientid { get; set; }
        public Int64 ssn_itemid { get; set; }
        public Int64 ssn_fslid { get; set; }
        public Int64 ssn_proid { get; set; }
        public Double ssn_itempurchaseamt { get; set; }
        public String ssn_payer { get; set; }
        public Int64 ssn_totalHours { get; set; }
        public String ssn_clientnumber { get; set; }
        public Double ssn_copaydedrecd { get; set; }
        public DateTime ssn_timein { get; set; }
        public DateTime ssn_timeout { get; set; }
        public Double ssn_clntpayrecd { get; set; }
        public Double ssn_billettoclient { get; set; }
        public Double ssn_billedtoinsurance { get; set; }
        public String ssn_location { get; set; }
        public DateTime ssn_insdatesubmit { get; set; }
        public String ssn_insclmnumber { get; set; }
        public String ssn_insclmnote { get; set; }
        public Double ssn_recdfrominsu { get; set; }
        public DateTime ssn_datenotesubmit { get; set; }
        public Double ssn_totalreceived { get; set; }
        public String ssn_todaygoal { get; set; }
        public Double ssn_submitcollection { get; set; }
        public Int64 ssn_show { get; set; }
        public Double ssn_recdfrmcollection { get; set; }
        public DateTime ssn_nextappointment { get; set; }
        public Double ssn_adjustwrite { get; set; }
        public String ssn_sessiontype { get; set; }
        public Double ssn_totalcharged { get; set; }
        public Boolean ssn_noteenter { get; set; }
        public Double ssn_totalpaid { get; set; }
        public Boolean ssn_placedinvoce { get; set; }
        public Double ssn_balancedue { get; set; }
        public Double ssn_cofacilitionfee { get; set; }
        public Double ssn_therapist { get; set; }
    }
}