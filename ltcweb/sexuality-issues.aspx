﻿<%@ Page Title="Sexuality-Issues" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="sexuality-issues.aspx.cs" Inherits="sexuality_issues" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-question"></i>&nbsp;SEXUALITY ISSUES</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <p>
                Having a sexual issue or concern is very normal. At some point in one&rsquo;s lifetime, a sexual issue is likely to arise. These issues can occur in both men and women. The issues can have a gradual or a sudden onset. Usually there is some confusion, shame, or depression that can result.&nbsp;<br/>
                <br/>
                Sexual issues include: sexual dysfunction, problems with arousal or desire, overactive sex drive, painful intercourse, concerns over masturbation, or fetishes.&nbsp;Sexual problems can be manifested in addictive behaviors such as looking at pornographic materials or other sexual obsessions.<br/>
                <br/>
                Sexual issues can manifest in physical or psychological problems, They can have diverse or unknown origins. They can result in problems with relationships, interpersonal problems or issues around intimacy.&nbsp;<br/>
                <br/>
                The counselors at Lifestyle can help you understand and cope with what you or your loved one may be experiencing. Counselors can help you identify successful strategies to address both the emotional and physical aspects of sexual problems.&nbsp;
            </p>
            <p align="center"><a href="Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

