﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;
namespace clsDAL
{
    public class clsAssesmetStepMaster
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsAssesmetStepMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsAssesmetStepMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _stp_id; }
            set
            {
                _stp_id = value;
                if (clsCMD.Parameters.Contains("@stp_id"))
                { clsCMD.Parameters["@stp_id"].Value = value; }
                else { clsCMD.Parameters.Add("@stp_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _stp_id;
        public Int64 stp_id
        {
            get { return _stp_id; }
            set
            {
                _stp_id = value;
                if (clsCMD.Parameters.Contains("@stp_id"))
                { clsCMD.Parameters["@stp_id"].Value = value; }
                else { clsCMD.Parameters.Add("@stp_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _stp_stepname;
        public String stp_stepname
        {
            get { return _stp_stepname; }
            set
            {
                _stp_stepname = value;
                if (clsCMD.Parameters.Contains("@stp_stepname"))
                { clsCMD.Parameters["@stp_stepname"].Value = value; }
                else { clsCMD.Parameters.Add("@stp_stepname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _stp_stepurl;
        public String stp_stepurl
        {
            get { return _stp_stepurl; }
            set
            {
                _stp_stepurl = value;
                if (clsCMD.Parameters.Contains("@stp_stepurl"))
                { clsCMD.Parameters["@stp_stepurl"].Value = value; }
                else { clsCMD.Parameters.Add("@stp_stepurl", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        private Guid _FlagUser;
        public Guid FlagUser
        {
            get { return _FlagUser; }
            set
            {
                _FlagUser = value;
                if (clsCMD.Parameters.Contains("@FlagUser"))
                { clsCMD.Parameters["@FlagUser"].Value = value; }
                else { clsCMD.Parameters.Add("@FlagUser", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                stp_id = Convert.ToInt64(dr["stp_id"]);
                stp_stepname = Convert.ToString(dr["stp_stepname"]);
                stp_stepurl = Convert.ToString(dr["stp_stepurl"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            stp_id = Convert.ToInt64("0");
            stp_stepname = Convert.ToString("NA");
            stp_stepurl = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.stp_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_tbl_AssesmetStepMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 stp_id)
        {
            Boolean blnResult = false;
            this.stp_id = stp_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_tbl_AssesmetStepMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 stp_id)
        {
            Boolean blnResult = false;
            this.stp_id = stp_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_tbl_AssesmetStepMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_tbl_AssesmetStepMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetNotCompletedSteps(Guid  userid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.FlagUser = userid;
                clsCMD.CommandText = "sp_tbl_AssesmetStepMasterGetData";
                SetGetSPFlag = "NOTCOMPLETED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsAssesmetStepMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsAssesmetStepMaster_PropertiesList> lstResult = new List<clsAssesmetStepMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].stp_id = Convert.ToInt64(dr["stp_id"]);
                lstResult[i].stp_stepname = Convert.ToString(dr["stp_stepname"]);
                lstResult[i].stp_stepurl = Convert.ToString(dr["stp_stepurl"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 stp_id)
        {
            Boolean blnResult = false;
            try
            {
                this.stp_id = stp_id;
                clsCMD.CommandText = "sp_tbl_AssesmetStepMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 stp_id)
        {
            Boolean blnResult = false;
            try
            {
                this.stp_id = stp_id;
                clsCMD.CommandText = "sp_tbl_AssesmetStepMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsAssesmetStepMaster_PropertiesList
    {
        public Int64 stp_id { get; set; }
        public String stp_stepname { get; set; }
        public String stp_stepurl { get; set; }
    }
}
