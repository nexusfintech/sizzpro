﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
using clsDAL.Conn;
using System.Data;
using System.Security.Permissions;
using System.IO;

public partial class MasterPageUM : System.Web.UI.MasterPage
{
    clsmsgMaster clsMsg = new clsmsgMaster();
    DataTable dtmsg = new DataTable();
    string memKey;
    clsModulePageMapping clsMPM = new clsModulePageMapping();
    clsProfileMaster clsProfile = new clsProfileMaster();

    public int cnt = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.DataBind();
        if (HttpContext.Current.User.Identity.IsAuthenticated)
        {
            memKey = Membership.GetUser().ProviderUserKey.ToString();
            lnkProfile.NavigateUrl = "~/Users/UserProfileManagement.aspx?memberkey=" + MySession.Current.UserGuID;
            lnkAccountSetting.NavigateUrl = "~/Users/usermanage.aspx?memberkey=" + MySession.Current.UserGuID;
            imguserimage.ImageUrl = "~/Document/UserProfile/" + MySession.Current.UserGuID + "/Photograph.jpg";
        }
        else
        {
            Response.Redirect("~/Login.aspx?ReturnUrl=" + Path.GetFileName(Request.Url.AbsolutePath));
        }
        if (!IsPostBack)
        {
            getCountmsg();
            if (MySession.Current.UserLoginFullname == string.Empty)
            {
                if (clsProfile.GetRecordByMemberKeyInProperties(Membership.GetUser().ProviderUserKey.ToString()))
                {
                    MySession.Current.UserLoginFullname = clsProfile.prm_firstname + " " + clsProfile.prm_lastname;
                }
            }
        }
    }
    private void getCountmsg()
    {
        dtmsg.Clear();
        dtmsg = clsMsg.countMsg(new Guid(memKey));

        foreach (DataRow rw in dtmsg.Rows)
        {
            cnt += Convert.ToInt32(rw["Msgs"].ToString());
        }
        cntMsg.DataSource = dtmsg;
        cntMsg.DataBind();
    }
}
