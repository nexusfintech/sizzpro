﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucProviderMaster.ascx.cs" Inherits="UserControl_GeneralControl_ucProviderMaster" %>

<asp:DropDownList ID="ddlProvider" runat="server" DataTextField="UserName" DataValueField="UserId" data-rel="chosen" CssClass="select2-me input-xlarge">
</asp:DropDownList>
