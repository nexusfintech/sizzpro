﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.IO;
using System.Data;
using clsDAL;
using System.Configuration;
using System.Net;

public partial class Client_Dashboard : System.Web.UI.Page
{
    clsProgramAssignUser clsPAU = new clsProgramAssignUser();
    clsAppointmentMaster clsAPNT = new clsAppointmentMaster();
    clsICdetails icPathDetail = new clsICdetails();

    Guid userId = new Guid();

    protected void Page_Load(object sender, EventArgs e)
    {
        userId = (Guid)Membership.GetUser().ProviderUserKey;
        Boolean rslt = icPathDetail.CheckDuplicateRow(userId);
        if (rslt == true)
        {
            lnkBtnDonwload.Visible = false;
        }
        if (hidden.Value != "")
        {
            Session["workbookid"] = hidden.Value;
            Response.Redirect("~/LifeProgram/Workbook.aspx");
            return;
        }
        if (!IsPostBack)
        {
            _GridFill();
        }
    }

    void _GridFill()
    {
        Guid guUser = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        clsPAU.pau_userid = guUser;
        rptWorkbookDispaly.DataSource = clsPAU.GetWorkBookByUser();
        rptWorkbookDispaly.DataBind();

        clsAPNT.apt_clientid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        rptAppontment.DataSource = clsAPNT.GetAllRecordClientWise();
        rptAppontment.DataBind();
    }
    protected void rptAppontment_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "DELETE")
        {
            Int64 intDelID = Convert.ToInt64(e.CommandArgument);
            clsAPNT.DeleteByPKID(intDelID);
            _GridFill();
        }
    }

    protected void lnkBtnDonwload_Click(object sender, EventArgs e)
    {
        //string filepath = icPathDetail.getPathByGuid(userId);
        string fp = ConfigurationManager.AppSettings["SizzUrl"].ToString() + "/Document/IC_" + userId.ToString() + ".pdf";
        WebClient client = new WebClient();
        Byte[] buffer = client.DownloadData(fp);
        if (buffer != null)
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-length", buffer.Length.ToString());
            Response.BinaryWrite(buffer);
        }
    }
}