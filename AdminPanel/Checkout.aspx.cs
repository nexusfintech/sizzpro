﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;

public partial class Checkout : System.Web.UI.Page
{
    clssubscriptionservices clsSSRV = new clssubscriptionservices();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session.Clear();
            loaddata();
        }
    }

    private void loaddata()
    {
        DataTable dtResult = clsSSRV.GetAllRecord();
        dtSubModul.DataSource = dtResult;
        dtSubModul.DataBind();
    }
    protected void LinkButton_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        Int64 id = Convert.ToInt64(lnk.CommandArgument);

        List<servicecart> cart;
        servicecart crtitm = new servicecart();
        Boolean blnRslt = clsSSRV.GetRecordByIDInProperties(id);
        if (blnRslt)
        {
            if (clsSSRV.srvc_servicename == "SizzPRO Suite")
            {
                cart = new List<servicecart>();
            }
            else
            {
                cart = (List<servicecart>)Session["cart"];
                if (cart == null)
                {
                    cart = new List<servicecart>();
                }
            }

            crtitm.id = cart.Count + 1;
            if (clsSSRV.srvc_recuringcharge != 0)
            {
                crtitm.price = clsSSRV.srvc_recuringcharge.ToString();
            }
            if (clsSSRV.srvc_fixcharge != 0)
            {
                crtitm.price = clsSSRV.srvc_fixcharge.ToString();
            }
            crtitm.service = clsSSRV.srvc_servicename;
            cart.Add(crtitm);
            Session["cart"] = cart;
            loaddata();
        }
        else
        {
            lblMsg.Text = clsSSRV.GetSetErrorMessage;
        }
    }

    protected void LinkbuttonRemove_Click(object sender, EventArgs e)
    {
        LinkButton lnkrmv = (LinkButton)sender;
        int id = Convert.ToInt32(lnkrmv.CommandArgument.ToString());

        List<servicecart> cart = (List<servicecart>)Session["cart"];
        servicecart ci = new servicecart();
        servicecart crtresult = cart.Find(
             delegate(servicecart crt)
             {
                 return crt.id == id;
             }
             );
        if (crtresult != null)
        {
            cart.Remove(crtresult);
            Session["cart"] = cart;
            loaddata();
        }
    }
    protected void dtSubModul_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (Session["cart"] != null)
            {
                List<servicecart> cart = (List<servicecart>)Session["cart"];
                foreach(servicecart crtitm in cart)
                {
                    LinkButton lnkBtn=(LinkButton) e.Item.FindControl("lnkadd");
                    if(lnkBtn.CommandName.ToString() == crtitm.service)
                    {
                        lnkBtn.Visible = false;
                    }
                    LinkButton lnkBtnrmv = (LinkButton)e.Item.FindControl("lnkrmv");
                    if (lnkBtnrmv.CommandName.ToString() == crtitm.service)
                    {
                        lnkBtnrmv.CommandArgument = crtitm.id.ToString();
                        lnkBtnrmv.Visible = true;
                    }

                }
            }
        }
    }
}