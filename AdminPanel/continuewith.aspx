﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSite.master" AutoEventWireup="true" CodeFile="continuewith.aspx.cs" Inherits="continuewith" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid" style="border: 1px solid #0072D6;">
        <div class="span12">
            <%--<asp:Literal ID="ltrPageBody" runat="server"></asp:Literal>--%>
            <h5 style="text-align: center">Select user type below which you want to continue</h5>
            <hr />
        </div>
        <div class="span12">
            <div class="span3"></div>
            <div class="span6">
                <asp:Button ID="btnexisting" runat="server" Text="Continue As Existing User" Font-Bold="true" CssClass="btn btn-block btn-primary" OnClick="btnexisting_Click"/>
                <br />
                <asp:Button ID="btnnew" runat="server" Text="Continue As New User" Font-Bold="true" CssClass="btn btn-block btn-primary" OnClick="btnnew_Click"/>
                <br/> 
            </div>
            <div class="span3"></div>
        </div>  
    </div>
</asp:Content>

