﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class UserControl_ContactUS : System.Web.UI.Page
{
    clsSendMail clsMail = new clsSendMail();


    private string ValidatePage()
    {
        string strResult = "";
        if (txtName.Text.Trim() == "")
        { strResult += "Please Enter Your Name <br/>"; return strResult; }
        if (txtEmail.Text.Trim() == "")
        { strResult += "Please Enter Email Address <br/>"; return strResult; }
        if (txtSubject.Text.Trim() == "")
        { strResult += "Please Enter Subject <br/>"; return strResult; }
        if (txtMessage.Text.Trim() == "")
        { strResult += "Please Enter Message <br/>"; return strResult; }
        return strResult;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void cmdSend_Click(object sender, EventArgs e)
    {
        string strResult = ValidatePage();
        if (strResult != "")
        {
            lblMessage.Text = strResult;
            return;
        }
        //insert msg in database as notification


        Boolean blnMailAct = clsMail.BindEmailAccount("azure_3b588217fd94a16e4729eeb6efd200b4@azure.com");

        if (blnMailAct == true)
        {
            clsMail.ToMailAddress = "admin@lifestyletherapycoach.com";
            clsMail.Priority = System.Net.Mail.MailPriority.High;
            clsMail.Subject = "Message from Client from Lifestyletherapy : " + txtSubject.Text;
            clsMail.HtmlBody = "Name : " + txtName.Text + "<br/>" +
                               "Email : " + txtEmail.Text + "<br/>" +
                               "Subject : " + txtSubject.Text + "<br/>" +
                               "Message : " + txtMessage.Text;
            clsMail.SendMail();
            clsMail.ToMailAddress = txtEmail.Text;
            clsMail.Priority = System.Net.Mail.MailPriority.High;
            clsMail.Subject = "Thank you for Contact to Lifestyletherapy : " + txtSubject.Text;
            clsMail.HtmlBody = "We will contact to you soon <br/>" +
                                "Name : " + txtName.Text + "<br/>" +
                               "Email : " + txtEmail.Text + "<br/>" +
                               "Subject : " + txtSubject.Text + "<br/>" +
                               "Message : " + txtMessage.Text;
            clsMail.SendMail();
            lblMessage.Text = "Thanks. We have recieve your message. We will reply to you shortly!";
        }
        else { lblMessage.Text = "Error in Email Account binding."; }

    }
}