﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Client_ClientProfile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ucProfile.MemberKey = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
    }
}