﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;
namespace clsDAL
{
    public class clsICdetails
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsICdetails()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsICdetails()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int32 PrimeryKey
        {
            get { return _IC_id; }
            set
            {
                _IC_id = value;
                if (clsCMD.Parameters.Contains("@IC_id"))
                { clsCMD.Parameters["@IC_id"].Value = value; }
                else { clsCMD.Parameters.Add("@IC_id", SqlDbType.Int).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int32 _IC_id;
        public Int32 IC_id
        {
            get { return _IC_id; }
            set
            {
                _IC_id = value;
                if (clsCMD.Parameters.Contains("@IC_id"))
                { clsCMD.Parameters["@IC_id"].Value = value; }
                else { clsCMD.Parameters.Add("@IC_id", SqlDbType.Int).Value = value; }
            }
        }
        private Guid _Client_id;
        public Guid Client_id
        {
            get { return _Client_id; }
            set
            {
                _Client_id = value;
                if (clsCMD.Parameters.Contains("@Client_id"))
                { clsCMD.Parameters["@Client_id"].Value = value; }
                else { clsCMD.Parameters.Add("@Client_id", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _User_id;
        public Guid User_id
        {
            get { return _User_id; }
            set
            {
                _User_id = value;
                if (clsCMD.Parameters.Contains("@User_id"))
                { clsCMD.Parameters["@User_id"].Value = value; }
                else { clsCMD.Parameters.Add("@User_id", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _IC_path;
        public String IC_path
        {
            get { return _IC_path; }
            set
            {
                _IC_path = value;
                if (clsCMD.Parameters.Contains("@IC_path"))
                { clsCMD.Parameters["@IC_path"].Value = value; }
                else { clsCMD.Parameters.Add("@IC_path", SqlDbType.NVarChar).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                IC_id = Convert.ToInt32(dr["IC_id"]);
                Client_id = new Guid(dr["Client_id"].ToString());
                User_id = new Guid(dr["User_id"].ToString());
                IC_path = Convert.ToString(dr["IC_path"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            IC_id = Convert.ToInt32("0");
            Client_id = new Guid();
            User_id = new Guid();
            IC_path = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.IC_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_tbl_ICdetailsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int32 IC_id)
        {
            Boolean blnResult = false;
            this.IC_id = IC_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_tbl_ICdetailsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int32 IC_id)
        {
            Boolean blnResult = false;
            this.IC_id = IC_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_tbl_ICdetailsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_tbl_ICdetailsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public string getPathByGuid(Guid userid)
        {
            string path = "";
            try
            {
                clsCMD.CommandText = "select IC_path from tbl_icdetails where Client_id='" + userid + "' ";
                clsCMD.CommandType = CommandType.Text;
                //SetGetSPFlag = "ALL";
                path = clsCMD.ExecuteScalar().ToString();


            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;

            }

            return path;
        }

        public List<clsICdetails_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsICdetails_PropertiesList> lstResult = new List<clsICdetails_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].IC_id = Convert.ToInt32(dr["IC_id"]);
                lstResult[i].Client_id = new Guid(dr["Client_id"].ToString());
                lstResult[i].IC_path = Convert.ToString(dr["IC_path"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int32 IC_id)
        {
            Boolean blnResult = false;
            try
            {
                this.IC_id = IC_id;
                clsCMD.CommandText = "sp_tbl_ICdetailsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Guid Clientid)
        {
            Boolean blnResult = false;
            try
            {
                this.Client_id = Clientid;
                clsCMD.CommandText = "sp_tbl_ICdetailsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRowWithUserId(Guid Clientid,Guid UserId)
        {
            Boolean blnResult = false;
            try
            {
                this.Client_id = Clientid;
                this.User_id = UserId;
                clsCMD.CommandText = "sp_tbl_ICdetailsGetData";
                SetGetSPFlag = "CHECKUSERANDCLIENT";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsICdetails_PropertiesList
    {
        public Int32 IC_id { get; set; }
        public Guid Client_id { get; set; }
        public String IC_path { get; set; }
    }
}