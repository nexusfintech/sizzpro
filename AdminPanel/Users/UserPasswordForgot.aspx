﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageOuter.master" AutoEventWireup="true" CodeFile="UserPasswordForgot.aspx.cs" Inherits="Users_UserPasswordForgot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphLeftBar" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblpagemessage" runat="server" Text=""></asp:Label>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3>
                        <i class="icon-lock"></i>
                        Retrieve Forgot Password
                    </h3>
                </div>
                <div id="dvPasswordReset" class="box-content" runat="server" visible="false">
                    <div class="span12">
                        <div class="box box-bordered box-color">
                            <div class="box-title">
                                <h3><i class="icon-user"></i>Reset Password</h3>
                            </div>
                            <div class="box-content nopadding">
                                <div class='form-horizontal form-bordered'>
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">Email Address</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtUsername" runat="server" placeholder="Email Address" class="complexify-me input-xlarge"></asp:TextBox>
                                            <span class="help-block">Password reset link will be sent to your registered email id.</span>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <asp:Button ID="cmdGetUsername" runat="server" CssClass="btn btn-primary" Text="Reset Password" OnClick="cmdGetUsername_Click" />
                                        <asp:Label ID="lblGetPassword" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="dvPasswordChange" class="box-content" runat="server" visible="false">
                    <div class="span12">
                        <div class="box box-bordered box-color">
                            <div class="box-title">
                                <h3><i class="icon-user"></i>Change Password</h3>
                            </div>
                            <div class="box-content nopadding">
                                <div class='form-horizontal form-bordered'>
                                    <div class="control-group">
                                        <label for="txtNewPassword" class="control-label">New Password</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtNewPassword" runat="server" placeholder="New Password" TextMode="Password" class="complexify-me input-xlarge"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtConformPassword" class="control-label">Confirm Password</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtConformPassword" runat="server" placeholder="Confirm Password" TextMode="Password" class="complexify-me input-xlarge"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <asp:Button ID="cmdChangePassword" runat="server" CssClass="btn btn-primary" Text="Change Password" OnClick="cmdChangePassword_Click" />
                                        <asp:Label ID="lblChangepass" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

