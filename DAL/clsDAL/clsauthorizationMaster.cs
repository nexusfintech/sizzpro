﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsauthorizationMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsauthorizationMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsauthorizationMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _auth_id; }
            set
            {
                _auth_id = value;
                if (clsCMD.Parameters.Contains("@auth_id"))
                { clsCMD.Parameters["@auth_id"].Value = value; }
                else { clsCMD.Parameters.Add("@auth_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _auth_id;
        public Int64 auth_id
        {
            get { return _auth_id; }
            set
            {
                _auth_id = value;
                if (clsCMD.Parameters.Contains("@auth_id"))
                { clsCMD.Parameters["@auth_id"].Value = value; }
                else { clsCMD.Parameters.Add("@auth_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _auth_userid;
        public Guid auth_userid
        {
            get { return _auth_userid; }
            set
            {
                _auth_userid = value;
                if (clsCMD.Parameters.Contains("@auth_userid"))
                { clsCMD.Parameters["@auth_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@auth_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _auth_authorizationnumber;
        public String auth_authorizationnumber
        {
            get { return _auth_authorizationnumber; }
            set
            {
                _auth_authorizationnumber = value;
                if (clsCMD.Parameters.Contains("@auth_authorizationnumber"))
                { clsCMD.Parameters["@auth_authorizationnumber"].Value = value; }
                else { clsCMD.Parameters.Add("@auth_authorizationnumber", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private DateTime _auth_startdate;
        public DateTime auth_startdate
        {
            get { return _auth_startdate; }
            set
            {
                _auth_startdate = value;
                if (clsCMD.Parameters.Contains("@auth_startdate"))
                { clsCMD.Parameters["@auth_startdate"].Value = value; }
                else { clsCMD.Parameters.Add("@auth_startdate", SqlDbType.DateTime).Value = value; }
            }
        }
        private DateTime _auth_enddate;
        public DateTime auth_enddate
        {
            get { return _auth_enddate; }
            set
            {
                _auth_enddate = value;
                if (clsCMD.Parameters.Contains("@auth_enddate"))
                { clsCMD.Parameters["@auth_enddate"].Value = value; }
                else { clsCMD.Parameters.Add("@auth_enddate", SqlDbType.DateTime).Value = value; }
            }
        }
        private Int64 _auth_authorisedsession;
        public Int64 auth_authorisedsession
        {
            get { return _auth_authorisedsession; }
            set
            {
                _auth_authorisedsession = value;
                if (clsCMD.Parameters.Contains("@auth_authorisedsession"))
                { clsCMD.Parameters["@auth_authorisedsession"].Value = value; }
                else { clsCMD.Parameters.Add("@auth_authorisedsession", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _auth_cptcode;
        public Int64 auth_cptcode
        {
            get { return _auth_cptcode; }
            set
            {
                _auth_cptcode = value;
                if (clsCMD.Parameters.Contains("@auth_cptcode"))
                { clsCMD.Parameters["@auth_cptcode"].Value = value; }
                else { clsCMD.Parameters.Add("@auth_cptcode", SqlDbType.BigInt).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                auth_id = Convert.ToInt64(dr["auth_id"]);
                auth_userid = new Guid(dr["auth_userid"].ToString());
                auth_authorizationnumber = Convert.ToString(dr["auth_authorizationnumber"]);
                auth_startdate = Convert.ToDateTime(dr["auth_startdate"]);
                auth_enddate = Convert.ToDateTime(dr["auth_enddate"]);
                auth_authorisedsession = Convert.ToInt64(dr["auth_authorisedsession"]);
                auth_cptcode = Convert.ToInt64(dr["auth_cptcode"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            auth_id = Convert.ToInt64("0");
            auth_userid = new Guid();
            auth_authorizationnumber = Convert.ToString("NA");
            auth_startdate = Convert.ToDateTime(DateTime.Now);
            auth_enddate = Convert.ToDateTime(DateTime.Now);
            auth_authorisedsession = Convert.ToInt64("0");
            auth_cptcode = Convert.ToInt64("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.auth_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_authorizationMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 auth_id)
        {
            Boolean blnResult = false;
            this.auth_id = auth_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_authorizationMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 auth_id)
        {
            Boolean blnResult = false;
            this.auth_id = auth_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_authorizationMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_authorizationMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllForSa()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_authorizationMasterGetData";
                SetGetSPFlag = "ALLFORSA";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByAdmin(Guid adminid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.auth_userid = adminid;
                clsCMD.CommandText = "sp_authorizationMasterGetData";
                SetGetSPFlag = "ALLBYADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsauthorizationMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsauthorizationMaster_PropertiesList> lstResult = new List<clsauthorizationMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].auth_id = Convert.ToInt64(dr["auth_id"]);
                lstResult[i].auth_userid = new Guid(dr["auth_userid"].ToString());
                lstResult[i].auth_authorizationnumber = Convert.ToString(dr["auth_authorizationnumber"]);
                lstResult[i].auth_startdate = Convert.ToDateTime(dr["auth_startdate"]);
                lstResult[i].auth_enddate = Convert.ToDateTime(dr["auth_enddate"]);
                lstResult[i].auth_authorisedsession = Convert.ToInt64(dr["auth_authorisedsession"]);
                lstResult[i].auth_cptcode = Convert.ToInt64(dr["auth_cptcode"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 auth_id)
        {
            Boolean blnResult = false;
            try
            {
                this.auth_id = auth_id;
                clsCMD.CommandText = "sp_authorizationMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 auth_id)
        {
            Boolean blnResult = false;
            try
            {
                this.auth_id = auth_id;
                clsCMD.CommandText = "sp_authorizationMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsauthorizationMaster_PropertiesList
    {
        public Int64 auth_id { get; set; }
        public Guid auth_userid { get; set; }
        public String auth_authorizationnumber { get; set; }
        public DateTime auth_startdate { get; set; }
        public DateTime auth_enddate { get; set; }
        public Int64 auth_authorisedsession { get; set; }
        public Int64 auth_cptcode { get; set; }
    }
}