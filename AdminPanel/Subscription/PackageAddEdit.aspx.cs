﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Subscription_ServiceAddEdit : System.Web.UI.Page
{
    clssubscriptionservices clsSS = new clssubscriptionservices();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                EditEntry();
            }
        }
    }
    private void EditEntry()
    {
        Boolean blnResult = clsSS.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnResult == true)
        {
            txtServiceName.Text = clsSS.srvc_servicename;
            txtDuration.Text = clsSS.srvc_periodval.ToString();
            ListItem list = ddlInterval.Items.FindByValue(clsSS.srvc_periodinterval);
            if (list != null)
            {
                ddlInterval.SelectedIndex = ddlInterval.Items.IndexOf(list);
            }
            if (clsSS.srvc_fixcharge != 0)
            {
                txtFixcharge.Text = clsSS.srvc_fixcharge.ToString();
            }
            if (clsSS.srvc_recuringcharge != 0)
            {
                txtRecurringCharge.Text = clsSS.srvc_recuringcharge.ToString();
            }
        }
        else
        {
            Response.Redirect("~/Subscription/PackageMaster.aspx");
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string ValidateResult=validatepage();
        if(ValidateResult != string.Empty)
        {
            lblmessage.Text = ValidateResult;
            return;
        }
        clsSS.srvc_servicename = txtServiceName.Text.ToString();
        clsSS.srvc_periodval =Convert.ToInt64(txtDuration.Text);
        clsSS.srvc_periodinterval = ddlInterval.SelectedValue.ToString();
        if(txtFixcharge.Text.ToString() != string.Empty)
        {
            clsSS.srvc_fixcharge = Convert.ToInt64(txtFixcharge.Text);
        }
        if(txtRecurringCharge.Text.ToString() != string.Empty)
        {
            clsSS.srvc_recuringcharge = Convert.ToInt64(txtRecurringCharge.Text);
        }
        Boolean blnSaveRslt = false;
        if (MySession.Current.AddEditFlag == "EDIT")
        {
            blnSaveRslt = clsSS.Update(MySession.Current.EditID);
        }
        else
        {
             blnSaveRslt = clsSS.Save();
        }
        if (blnSaveRslt == true)
        {
            Response.Redirect("~/Subscription/PackageMaster.aspx");
        }
        else
        {
            lblmessage.Text = "Error : " + clsSS.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    private string validatepage()
    {
        string RsltVldt = string.Empty;
        if(txtServiceName.Text.ToString() == string.Empty)
        {
            RsltVldt = "Please Insert Service Name";
            return RsltVldt;
        }
        if(txtDuration.Text.ToString() == string.Empty)
        {
            RsltVldt = "Please Insert Service Duration";
            return RsltVldt;
        }
        if(txtRecurringCharge.Text.ToString() == string.Empty && txtFixcharge.Text.ToString() == string.Empty)
        {
            RsltVldt = "Please Insert Any one Charge";
            return RsltVldt;
        }
        return RsltVldt;
    }
}