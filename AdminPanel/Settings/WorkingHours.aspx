﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="WorkingHours.aspx.cs" Inherits="Settings_WorkingHours" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="glyphicon-settings"></i>Working Hours Time Settings</h3>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label for="txtStartTime" class="control-label">Start Time</label>
                            <div class="controls">
                                <asp:TextBox ID="txtStartTime" runat="server" CssClass="input-small"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtEndTime" class="control-label">End Time</label>
                            <div class="controls">
                                <asp:TextBox ID="txtEndTime" runat="server" CssClass="input-small"></asp:TextBox>
                            </div>
                    </div>
                    <div class="control-group">
                        <label for="ddlProfession" class="control-label">Profession</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlProfession" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-actions">
                        <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

