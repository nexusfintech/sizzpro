﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsProfileMaster
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsProfileMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsProfileMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _prm_id; }
            set
            {
                _prm_id = value;
                if (clsCMD.Parameters.Contains("@prm_id"))
                { clsCMD.Parameters["@prm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _prm_id;
        public Int64 prm_id
        {
            get { return _prm_id; }
            set
            {
                _prm_id = value;
                if (clsCMD.Parameters.Contains("@prm_id"))
                { clsCMD.Parameters["@prm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _prm_initial;
        public String prm_initial
        {
            get { return _prm_initial; }
            set
            {
                _prm_initial = value;
                if (clsCMD.Parameters.Contains("@prm_initial"))
                { clsCMD.Parameters["@prm_initial"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_initial", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_firstname;
        public String prm_firstname
        {
            get { return _prm_firstname; }
            set
            {
                _prm_firstname = value;
                if (clsCMD.Parameters.Contains("@prm_firstname"))
                { clsCMD.Parameters["@prm_firstname"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_firstname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_middlename;
        public String prm_middlename
        {
            get { return _prm_middlename; }
            set
            {
                _prm_middlename = value;
                if (clsCMD.Parameters.Contains("@prm_middlename"))
                { clsCMD.Parameters["@prm_middlename"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_middlename", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_lastname;
        public String prm_lastname
        {
            get { return _prm_lastname; }
            set
            {
                _prm_lastname = value;
                if (clsCMD.Parameters.Contains("@prm_lastname"))
                { clsCMD.Parameters["@prm_lastname"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_lastname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private DateTime _prm_dob;
        public DateTime prm_dob
        {
            get { return _prm_dob; }
            set
            {
                _prm_dob = value;
                if (clsCMD.Parameters.Contains("@prm_dob"))
                { if (value == null) { clsCMD.Parameters["@prm_dob"].Value = DBNull.Value; } else { clsCMD.Parameters["@prm_dob"].Value = value; } }
                else { if (value == null) { clsCMD.Parameters.Add("@prm_dob", SqlDbType.DateTime).Value = DBNull.Value; } else { clsCMD.Parameters.Add("@prm_dob", SqlDbType.DateTime).Value = value; } }
            }
        }
        private DateTime _prm_intakedate;
        public DateTime prm_intakedate
        {
            get { return _prm_intakedate; }
            set
            {
                _prm_intakedate = value;
                if (clsCMD.Parameters.Contains("@prm_intakedate"))
                { if (value == null) { clsCMD.Parameters["@prm_intakedate"].Value = DBNull.Value; } else { clsCMD.Parameters["@prm_intakedate"].Value = value; } }
                else { if (value == null) { clsCMD.Parameters.Add("@prm_intakedate", SqlDbType.DateTime).Value = DBNull.Value; } else { clsCMD.Parameters.Add("@prm_intakedate", SqlDbType.DateTime).Value = value; } }
            }
        }
        private String _prm_ssn;
        public String prm_ssn
        {
            get { return _prm_ssn; }
            set
            {
                _prm_ssn = value;
                if (clsCMD.Parameters.Contains("@prm_ssn"))
                { clsCMD.Parameters["@prm_ssn"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_ssn", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _prm_emailid;
        public String prm_emailid
        {
            get { return _prm_emailid; }
            set
            {
                _prm_emailid = value;
                if (clsCMD.Parameters.Contains("@prm_emailid"))
                { clsCMD.Parameters["@prm_emailid"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emailid", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _prm_gender;
        public Int64 prm_gender
        {
            get { return _prm_gender; }
            set
            {
                _prm_gender = value;
                if (clsCMD.Parameters.Contains("@prm_gender"))
                { clsCMD.Parameters["@prm_gender"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_gender", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _prm_ethincity;
        public Int64 prm_ethincity
        {
            get { return _prm_ethincity; }
            set
            {
                _prm_ethincity = value;
                if (clsCMD.Parameters.Contains("@prm_ethincity"))
                { clsCMD.Parameters["@prm_ethincity"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_ethincity", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _prm_langpreferance;
        public Int64 prm_langpreferance
        {
            get { return _prm_langpreferance; }
            set
            {
                _prm_langpreferance = value;
                if (clsCMD.Parameters.Contains("@prm_langpreferance"))
                { clsCMD.Parameters["@prm_langpreferance"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_langpreferance", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _prm_maritalstatus;
        public Int64 prm_maritalstatus
        {
            get { return _prm_maritalstatus; }
            set
            {
                _prm_maritalstatus = value;
                if (clsCMD.Parameters.Contains("@prm_maritalstatus"))
                { clsCMD.Parameters["@prm_maritalstatus"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_maritalstatus", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _prm_empid;
        public Int64 prm_empid
        {
            get { return _prm_empid; }
            set
            {
                _prm_empid = value;
                if (clsCMD.Parameters.Contains("@prm_empid"))
                { clsCMD.Parameters["@prm_empid"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_empid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Boolean _prm_student;
        public Boolean prm_student
        {
            get { return _prm_student; }
            set
            {
                _prm_student = value;
                if (clsCMD.Parameters.Contains("@prm_student"))
                { clsCMD.Parameters["@prm_student"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_student", SqlDbType.Bit).Value = value; }
            }
        }
        private Int64 _prm_identifiedclient;
        public Int64 prm_identifiedclient
        {
            get { return _prm_identifiedclient; }
            set
            {
                _prm_identifiedclient = value;
                if (clsCMD.Parameters.Contains("@prm_identifiedclient"))
                { clsCMD.Parameters["@prm_identifiedclient"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_identifiedclient", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _prm_paymentoption;
        public Int64 prm_paymentoption
        {
            get { return _prm_paymentoption; }
            set
            {
                _prm_paymentoption = value;
                if (clsCMD.Parameters.Contains("@prm_paymentoption"))
                { clsCMD.Parameters["@prm_paymentoption"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_paymentoption", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _prm_claimidprefix;
        public Int64 prm_claimidprefix
        {
            get { return _prm_claimidprefix; }
            set
            {
                _prm_claimidprefix = value;
                if (clsCMD.Parameters.Contains("@prm_claimidprefix"))
                { clsCMD.Parameters["@prm_claimidprefix"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_claimidprefix", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _prm_provider;
        public Int64 prm_provider
        {
            get { return _prm_provider; }
            set
            {
                _prm_provider = value;
                if (clsCMD.Parameters.Contains("@prm_provider"))
                { clsCMD.Parameters["@prm_provider"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_provider", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _prm_status;
        public Int64 prm_status
        {
            get { return _prm_status; }
            set
            {
                _prm_status = value;
                if (clsCMD.Parameters.Contains("@prm_status"))
                { clsCMD.Parameters["@prm_status"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_status", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _prm_rfs_firstname;
        public String prm_rfs_firstname
        {
            get { return _prm_rfs_firstname; }
            set
            {
                _prm_rfs_firstname = value;
                if (clsCMD.Parameters.Contains("@prm_rfs_firstname"))
                { clsCMD.Parameters["@prm_rfs_firstname"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_rfs_firstname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_rfs_lastname;
        public String prm_rfs_lastname
        {
            get { return _prm_rfs_lastname; }
            set
            {
                _prm_rfs_lastname = value;
                if (clsCMD.Parameters.Contains("@prm_rfs_lastname"))
                { clsCMD.Parameters["@prm_rfs_lastname"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_rfs_lastname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _prm_rfs_reltoclient;
        public Int64 prm_rfs_reltoclient
        {
            get { return _prm_rfs_reltoclient; }
            set
            {
                _prm_rfs_reltoclient = value;
                if (clsCMD.Parameters.Contains("@prm_rfs_reltoclient"))
                { clsCMD.Parameters["@prm_rfs_reltoclient"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_rfs_reltoclient", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _prm_rfs_add1;
        public String prm_rfs_add1
        {
            get { return _prm_rfs_add1; }
            set
            {
                _prm_rfs_add1 = value;
                if (clsCMD.Parameters.Contains("@prm_rfs_add1"))
                { clsCMD.Parameters["@prm_rfs_add1"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_rfs_add1", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _prm_rfs_add2;
        public String prm_rfs_add2
        {
            get { return _prm_rfs_add2; }
            set
            {
                _prm_rfs_add2 = value;
                if (clsCMD.Parameters.Contains("@prm_rfs_add2"))
                { clsCMD.Parameters["@prm_rfs_add2"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_rfs_add2", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private Boolean _prm_rfs_addsameclnt;
        public Boolean prm_rfs_addsameclnt
        {
            get { return _prm_rfs_addsameclnt; }
            set
            {
                _prm_rfs_addsameclnt = value;
                if (clsCMD.Parameters.Contains("@prm_rfs_addsameclnt"))
                { clsCMD.Parameters["@prm_rfs_addsameclnt"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_rfs_addsameclnt", SqlDbType.Bit).Value = value; }
            }
        }
        private String _prm_rfs_city;
        public String prm_rfs_city
        {
            get { return _prm_rfs_city; }
            set
            {
                _prm_rfs_city = value;
                if (clsCMD.Parameters.Contains("@prm_rfs_city"))
                { clsCMD.Parameters["@prm_rfs_city"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_rfs_city", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _prm_rfs_state;
        public Int64 prm_rfs_state
        {
            get { return _prm_rfs_state; }
            set
            {
                _prm_rfs_state = value;
                if (clsCMD.Parameters.Contains("@prm_rfs_state"))
                { clsCMD.Parameters["@prm_rfs_state"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_rfs_state", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _prm_rfs_zipcode;
        public String prm_rfs_zipcode
        {
            get { return _prm_rfs_zipcode; }
            set
            {
                _prm_rfs_zipcode = value;
                if (clsCMD.Parameters.Contains("@prm_rfs_zipcode"))
                { clsCMD.Parameters["@prm_rfs_zipcode"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_rfs_zipcode", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_rfs_note;
        public String prm_rfs_note
        {
            get { return _prm_rfs_note; }
            set
            {
                _prm_rfs_note = value;
                if (clsCMD.Parameters.Contains("@prm_rfs_note"))
                { clsCMD.Parameters["@prm_rfs_note"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_rfs_note", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private String _prm_emr_firstname;
        public String prm_emr_firstname
        {
            get { return _prm_emr_firstname; }
            set
            {
                _prm_emr_firstname = value;
                if (clsCMD.Parameters.Contains("@prm_emr_firstname"))
                { clsCMD.Parameters["@prm_emr_firstname"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_firstname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_emr_lastname;
        public String prm_emr_lastname
        {
            get { return _prm_emr_lastname; }
            set
            {
                _prm_emr_lastname = value;
                if (clsCMD.Parameters.Contains("@prm_emr_lastname"))
                { clsCMD.Parameters["@prm_emr_lastname"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_lastname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _prm_emr_relationship;
        public Int64 prm_emr_relationship
        {
            get { return _prm_emr_relationship; }
            set
            {
                _prm_emr_relationship = value;
                if (clsCMD.Parameters.Contains("@prm_emr_relationship"))
                { clsCMD.Parameters["@prm_emr_relationship"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_relationship", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _prm_emr_phonetype;
        public String prm_emr_phonetype
        {
            get { return _prm_emr_phonetype; }
            set
            {
                _prm_emr_phonetype = value;
                if (clsCMD.Parameters.Contains("@prm_emr_phonetype"))
                { clsCMD.Parameters["@prm_emr_phonetype"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_phonetype", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_emr_phoneno;
        public String prm_emr_phoneno
        {
            get { return _prm_emr_phoneno; }
            set
            {
                _prm_emr_phoneno = value;
                if (clsCMD.Parameters.Contains("@prm_emr_phoneno"))
                { clsCMD.Parameters["@prm_emr_phoneno"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_phoneno", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Boolean _prm_emr_addsameclnt;
        public Boolean prm_emr_addsameclnt
        {
            get { return _prm_emr_addsameclnt; }
            set
            {
                _prm_emr_addsameclnt = value;
                if (clsCMD.Parameters.Contains("@prm_emr_addsameclnt"))
                { clsCMD.Parameters["@prm_emr_addsameclnt"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_addsameclnt", SqlDbType.Bit).Value = value; }
            }
        }
        private String _prm_emr_add1;
        public String prm_emr_add1
        {
            get { return _prm_emr_add1; }
            set
            {
                _prm_emr_add1 = value;
                if (clsCMD.Parameters.Contains("@prm_emr_add1"))
                { clsCMD.Parameters["@prm_emr_add1"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_add1", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _prm_emr_add2;
        public String prm_emr_add2
        {
            get { return _prm_emr_add2; }
            set
            {
                _prm_emr_add2 = value;
                if (clsCMD.Parameters.Contains("@prm_emr_add2"))
                { clsCMD.Parameters["@prm_emr_add2"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_add2", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _prm_emr_city;
        public String prm_emr_city
        {
            get { return _prm_emr_city; }
            set
            {
                _prm_emr_city = value;
                if (clsCMD.Parameters.Contains("@prm_emr_city"))
                { clsCMD.Parameters["@prm_emr_city"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_city", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _prm_emr_state;
        public Int64 prm_emr_state
        {
            get { return _prm_emr_state; }
            set
            {
                _prm_emr_state = value;
                if (clsCMD.Parameters.Contains("@prm_emr_state"))
                { clsCMD.Parameters["@prm_emr_state"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_state", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _prm_emr_zipcode;
        public String prm_emr_zipcode
        {
            get { return _prm_emr_zipcode; }
            set
            {
                _prm_emr_zipcode = value;
                if (clsCMD.Parameters.Contains("@prm_emr_zipcode"))
                { clsCMD.Parameters["@prm_emr_zipcode"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emr_zipcode", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_scd_firstname;
        public String prm_scd_firstname
        {
            get { return _prm_scd_firstname; }
            set
            {
                _prm_scd_firstname = value;
                if (clsCMD.Parameters.Contains("@prm_scd_firstname"))
                { clsCMD.Parameters["@prm_scd_firstname"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_scd_firstname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_scd_lastname;
        public String prm_scd_lastname
        {
            get { return _prm_scd_lastname; }
            set
            {
                _prm_scd_lastname = value;
                if (clsCMD.Parameters.Contains("@prm_scd_lastname"))
                { clsCMD.Parameters["@prm_scd_lastname"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_scd_lastname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _prm_scd_relationship;
        public Int64 prm_scd_relationship
        {
            get { return _prm_scd_relationship; }
            set
            {
                _prm_scd_relationship = value;
                if (clsCMD.Parameters.Contains("@prm_scd_relationship"))
                { clsCMD.Parameters["@prm_scd_relationship"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_scd_relationship", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _prm_scd_phonetype;
        public String prm_scd_phonetype
        {
            get { return _prm_scd_phonetype; }
            set
            {
                _prm_scd_phonetype = value;
                if (clsCMD.Parameters.Contains("@prm_scd_phonetype"))
                { clsCMD.Parameters["@prm_scd_phonetype"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_scd_phonetype", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_scd_phoneno;
        public String prm_scd_phoneno
        {
            get { return _prm_scd_phoneno; }
            set
            {
                _prm_scd_phoneno = value;
                if (clsCMD.Parameters.Contains("@prm_scd_phoneno"))
                { clsCMD.Parameters["@prm_scd_phoneno"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_scd_phoneno", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Boolean _prm_passwordreset;
        public Boolean prm_passwordreset
        {
            get { return _prm_passwordreset; }
            set
            {
                _prm_passwordreset = value;
                if (clsCMD.Parameters.Contains("@prm_passwordreset"))
                { clsCMD.Parameters["@prm_passwordreset"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_passwordreset", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _prm_emailverify;
        public Boolean prm_emailverify
        {
            get { return _prm_emailverify; }
            set
            {
                _prm_emailverify = value;
                if (clsCMD.Parameters.Contains("@prm_emailverify"))
                { clsCMD.Parameters["@prm_emailverify"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_emailverify", SqlDbType.Bit).Value = value; }
            }
        }
        private Int64 _prm_user_Id;
        public Int64 prm_user_Id
        {
            get { return _prm_user_Id; }
            set
            {
                _prm_user_Id = value;
                if (clsCMD.Parameters.Contains("@prm_user_Id"))
                { clsCMD.Parameters["@prm_user_Id"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_user_Id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _prm_userId;
        public String prm_userId
        {
            get { return _prm_userId; }
            set
            {
                _prm_userId = value;
                if (clsCMD.Parameters.Contains("@prm_userId"))
                { clsCMD.Parameters["@prm_userId"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_userId", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _prm_parentid;
        public String prm_parentid
        {
            get { return _prm_parentid; }
            set
            {
                _prm_parentid = value;
                if (clsCMD.Parameters.Contains("@prm_parentid"))
                { clsCMD.Parameters["@prm_parentid"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_parentid", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _prm_userrole;
        public String prm_userrole
        {
            get { return _prm_userrole; }
            set
            {
                _prm_userrole = value;
                if (clsCMD.Parameters.Contains("@prm_userrole"))
                { clsCMD.Parameters["@prm_userrole"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_userrole", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _prm_tokenid;
        public String prm_tokenid
        {
            get { return _prm_tokenid; }
            set
            {
                _prm_tokenid = value;
                if (clsCMD.Parameters.Contains("@prm_tokenid"))
                { clsCMD.Parameters["@prm_tokenid"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_tokenid", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private DateTime? _prm_tokendatetime;
        public DateTime? prm_tokendatetime
        {
            get { return _prm_tokendatetime; }
            set
            {
                _prm_tokendatetime = value;
                if (clsCMD.Parameters.Contains("@prm_tokendatetime"))
                { if (value == null) { clsCMD.Parameters["@prm_tokendatetime"].Value = DBNull.Value; } else { clsCMD.Parameters["@prm_tokendatetime"].Value = value; } }
                else { if (value == null) { clsCMD.Parameters.Add("@prm_tokendatetime", SqlDbType.DateTime).Value = DBNull.Value; } else { clsCMD.Parameters.Add("@prm_tokendatetime", SqlDbType.DateTime).Value = value; } }
            }
        }

        private String _prm_useraddress;
        public String prm_useraddress
        {
            get { return _prm_useraddress; }
            set
            {
                _prm_useraddress = value;
                if (clsCMD.Parameters.Contains("@prm_useraddress"))
                { clsCMD.Parameters["@prm_useraddress"].Size = value.Length; clsCMD.Parameters["@prm_useraddress"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_useraddress", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }

        private String _prm_zipcode;
        public String prm_zipcode
        {
            get { return _prm_zipcode; }
            set
            {
                _prm_zipcode = value;
                if (clsCMD.Parameters.Contains("@prm_zipcode"))
                { clsCMD.Parameters["@prm_zipcode"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_zipcode", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        private String _prm_usercity;
        public String prm_usercity
        {
            get { return _prm_usercity; }
            set
            {
                _prm_usercity = value;
                if (clsCMD.Parameters.Contains("@prm_usercity"))
                { clsCMD.Parameters["@prm_usercity"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_usercity", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        private Int64 _prm_userstate;
        public Int64 prm_userstate
        {
            get { return _prm_userstate; }
            set
            {
                _prm_userstate = value;
                if (clsCMD.Parameters.Contains("@prm_userstate"))
                { clsCMD.Parameters["@prm_userstate"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_userstate", SqlDbType.BigInt).Value = value; }
            }
        }

        private Int64 _prm_usercountry;
        public Int64 prm_usercountry
        {
            get { return _prm_usercountry; }
            set
            {
                _prm_usercountry = value;
                if (clsCMD.Parameters.Contains("@prm_usercountry"))
                { clsCMD.Parameters["@prm_usercountry"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_usercountry", SqlDbType.BigInt).Value = value; }
            }
        }

        private String _prm_userphone;
        public String prm_userphone
        {
            get { return _prm_userphone; }
            set
            {
                _prm_userphone = value;
                if (clsCMD.Parameters.Contains("@prm_userphone"))
                { clsCMD.Parameters["@prm_userphone"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_userphone", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        private String _prm_usermobile;
        public String prm_usermobile
        {
            get { return _prm_usermobile; }
            set
            {
                _prm_usermobile = value;
                if (clsCMD.Parameters.Contains("@prm_usermobile"))
                { clsCMD.Parameters["@prm_usermobile"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_usermobile", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        private Boolean _prm_infoconsnt;
        public Boolean prm_infoconsnt
        {
            get { return _prm_infoconsnt; }
            set
            {
                _prm_infoconsnt = value;
                if (clsCMD.Parameters.Contains("@prm_infoconsnt"))
                { clsCMD.Parameters["@prm_infoconsnt"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_infoconsnt", SqlDbType.Bit).Value = value; }
            }
        }

        private String _prm_registerweblink;
        public String prm_registerweblink
        {
            get { return _prm_registerweblink; }
            set
            {
                _prm_registerweblink = value;
                if (clsCMD.Parameters.Contains("@prm_registerweblink"))
                { clsCMD.Parameters["@prm_registerweblink"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_registerweblink", SqlDbType.NVarChar, 500).Value = value; }
            }
        }

        private DateTime _prm_registertimestamp;
        public DateTime prm_registertimestamp
        {
            get { return _prm_registertimestamp; }
            set
            {
                _prm_registertimestamp = value;
                if (clsCMD.Parameters.Contains("@prm_registertimestamp"))
                { clsCMD.Parameters["@prm_registertimestamp"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_registertimestamp", SqlDbType.DateTime).Value = value; }
            }
        }

        private String _prm_ipaddress;
        public String prm_ipaddress
        {
            get { return _prm_ipaddress; }
            set
            {
                _prm_ipaddress = value;
                if (clsCMD.Parameters.Contains("@prm_ipaddress"))
                { clsCMD.Parameters["@prm_ipaddress"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_ipaddress", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        private String _prm_latlong;
        public String prm_latlong
        {
            get { return _prm_latlong; }
            set
            {
                _prm_latlong = value;
                if (clsCMD.Parameters.Contains("@prm_latlong"))
                { clsCMD.Parameters["@prm_latlong"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_latlong", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        private String _prm_geocountry;
        public String prm_geocountry
        {
            get { return _prm_geocountry; }
            set
            {
                _prm_geocountry = value;
                if (clsCMD.Parameters.Contains("@prm_geocountry"))
                { clsCMD.Parameters["@prm_geocountry"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_geocountry", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_georegion;
        public String prm_georegion
        {
            get { return _prm_georegion; }
            set
            {
                _prm_georegion = value;
                if (clsCMD.Parameters.Contains("@prm_georegion"))
                { clsCMD.Parameters["@prm_georegion"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_georegion", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_geozipcode;
        public String prm_geozipcode
        {
            get { return _prm_geozipcode; }
            set
            {
                _prm_geozipcode = value;
                if (clsCMD.Parameters.Contains("@prm_geozipcode"))
                { clsCMD.Parameters["@prm_geozipcode"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_geozipcode", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        private String _prm_geotimezone;
        public String prm_geotimezone
        {
            get { return _prm_geotimezone; }
            set
            {
                _prm_geotimezone = value;
                if (clsCMD.Parameters.Contains("@prm_geotimezone"))
                { clsCMD.Parameters["@prm_geotimezone"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_geotimezone", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        private String _prm_geocountcode;
        public String prm_geocountcode
        {
            get { return _prm_geocountcode; }
            set
            {
                _prm_geocountcode = value;
                if (clsCMD.Parameters.Contains("@prm_geocountcode"))
                { clsCMD.Parameters["@prm_geocountcode"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_geocountcode", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prm_geoisp;
        public String prm_geoisp
        {
            get { return _prm_geoisp; }
            set
            {
                _prm_geoisp = value;
                if (clsCMD.Parameters.Contains("@prm_geoisp"))
                { clsCMD.Parameters["@prm_geoisp"].Value = value; }
                else { clsCMD.Parameters.Add("@prm_geoisp", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                prm_id = Convert.ToInt64(dr["prm_id"]);
                prm_initial = Convert.ToString(dr["prm_initial"]);
                prm_firstname = Convert.ToString(dr["prm_firstname"]);
                prm_middlename = Convert.ToString(dr["prm_middlename"]);
                prm_lastname = Convert.ToString(dr["prm_lastname"]);
                prm_dob = Convert.ToDateTime(dr["prm_dob"]);
                prm_intakedate = Convert.ToDateTime(dr["prm_intakedate"]);
                prm_ssn = Convert.ToString(dr["prm_ssn"]);
                prm_emailid = Convert.ToString(dr["prm_emailid"]);
                prm_gender = Convert.ToInt64(dr["prm_gender"]);
                prm_ethincity = Convert.ToInt64(dr["prm_ethincity"]);
                prm_langpreferance = Convert.ToInt64(dr["prm_langpreferance"]);
                prm_maritalstatus = Convert.ToInt64(dr["prm_maritalstatus"]);
                prm_empid = Convert.ToInt64(dr["prm_empid"]);
                prm_student = Convert.ToBoolean(dr["prm_student"]);
                prm_identifiedclient = Convert.ToInt64(dr["prm_identifiedclient"]);
                prm_paymentoption = Convert.ToInt64(dr["prm_paymentoption"]);
                prm_claimidprefix = Convert.ToInt64(dr["prm_claimidprefix"]);
                prm_provider = Convert.ToInt64(dr["prm_provider"]);
                prm_status = Convert.ToInt64(dr["prm_status"]);
                prm_rfs_firstname = Convert.ToString(dr["prm_rfs_firstname"]);
                prm_rfs_lastname = Convert.ToString(dr["prm_rfs_lastname"]);
                prm_rfs_reltoclient = Convert.ToInt64(dr["prm_rfs_reltoclient"]);
                prm_rfs_add1 = Convert.ToString(dr["prm_rfs_add1"]);
                prm_rfs_add2 = Convert.ToString(dr["prm_rfs_add2"]);
                prm_rfs_addsameclnt = Convert.ToBoolean(dr["prm_rfs_addsameclnt"]);
                prm_rfs_city = Convert.ToString(dr["prm_rfs_city"]);
                prm_rfs_state = Convert.ToInt64(dr["prm_rfs_state"]);
                prm_rfs_zipcode = Convert.ToString(dr["prm_rfs_zipcode"]);
                prm_rfs_note = Convert.ToString(dr["prm_rfs_note"]);
                prm_emr_firstname = Convert.ToString(dr["prm_emr_firstname"]);
                prm_emr_lastname = Convert.ToString(dr["prm_emr_lastname"]);
                prm_emr_relationship = Convert.ToInt64(dr["prm_emr_relationship"]);
                prm_emr_phonetype = Convert.ToString(dr["prm_emr_phonetype"]);
                prm_emr_phoneno = Convert.ToString(dr["prm_emr_phoneno"]);
                prm_emr_addsameclnt = Convert.ToBoolean(dr["prm_emr_addsameclnt"]);
                prm_emr_add1 = Convert.ToString(dr["prm_emr_add1"]);
                prm_emr_add2 = Convert.ToString(dr["prm_emr_add2"]);
                prm_emr_city = Convert.ToString(dr["prm_emr_city"]);
                prm_emr_state = Convert.ToInt64(dr["prm_emr_state"]);
                prm_emr_zipcode = Convert.ToString(dr["prm_emr_zipcode"]);
                prm_scd_firstname = Convert.ToString(dr["prm_scd_firstname"]);
                prm_scd_lastname = Convert.ToString(dr["prm_scd_lastname"]);
                prm_scd_relationship = Convert.ToInt64(dr["prm_scd_relationship"]);
                prm_scd_phonetype = Convert.ToString(dr["prm_scd_phonetype"]);
                prm_scd_phoneno = Convert.ToString(dr["prm_scd_phoneno"]);
                prm_passwordreset = Convert.ToBoolean(dr["prm_passwordreset"]);
                prm_emailverify = Convert.ToBoolean(dr["prm_emailverify"]);
                prm_user_Id = Convert.ToInt64(dr["prm_user_Id"]);
                prm_userId = Convert.ToString(dr["prm_userId"]);
                prm_parentid = Convert.ToString(dr["prm_parentid"]);
                prm_userrole = Convert.ToString(dr["prm_userrole"]);
                prm_tokenid = Convert.ToString(dr["prm_tokenid"]);
                prm_tokendatetime = Convert.ToDateTime(dr["prm_tokendatetime"]);

                prm_useraddress = Convert.ToString(dr["prm_useraddress"]);
                prm_zipcode = Convert.ToString(dr["prm_zipcode"]);
                prm_usercity = Convert.ToString(dr["prm_usercity"]);
                prm_userstate = Convert.ToInt64(dr["prm_userstate"]);
                prm_usercountry = Convert.ToInt64(dr["prm_usercountry"]);
                prm_userphone = Convert.ToString(dr["prm_userphone"]);
                prm_usermobile = Convert.ToString(dr["prm_usermobile"]);
                prm_infoconsnt = Convert.ToBoolean(dr["prm_infoconsnt"]);
                prm_registerweblink = Convert.ToString(dr["prm_registerweblink"]);
                prm_registertimestamp = Convert.ToDateTime(dr["prm_registertimestamp"]);
                prm_ipaddress = Convert.ToString(dr["prm_ipaddress"]);
                prm_latlong = Convert.ToString(dr["prm_latlong"]);
                prm_geocountry = Convert.ToString(dr["prm_geocountry"]);
                prm_georegion = Convert.ToString(dr["prm_georegion"]);
                prm_geozipcode = Convert.ToString(dr["prm_geozipcode"]);
                prm_geotimezone = Convert.ToString(dr["prm_geotimezone"]);
                prm_geocountcode = Convert.ToString(dr["prm_geocountcode"]);
                prm_geoisp = Convert.ToString(dr["prm_geoisp"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            prm_id = Convert.ToInt64("0");
            prm_initial = Convert.ToString("NA");
            prm_firstname = Convert.ToString("NA");
            prm_middlename = Convert.ToString("NA");
            prm_lastname = Convert.ToString("NA");
            prm_dob = Convert.ToDateTime(DateTime.Now);
            prm_intakedate = Convert.ToDateTime(DateTime.Now);
            prm_ssn = Convert.ToString("NA");
            prm_emailid = Convert.ToString("NA");
            prm_gender = Convert.ToInt64("0");
            prm_ethincity = Convert.ToInt64("0");
            prm_langpreferance = Convert.ToInt64("0");
            prm_maritalstatus = Convert.ToInt64("0");
            prm_empid = Convert.ToInt64("0");
            prm_student = Convert.ToBoolean(false);
            prm_identifiedclient = Convert.ToInt64("0");
            prm_paymentoption = Convert.ToInt64("0");
            prm_claimidprefix = Convert.ToInt64("0");
            prm_provider = Convert.ToInt64("0");
            prm_status = Convert.ToInt64("0");
            prm_rfs_firstname = Convert.ToString("NA");
            prm_rfs_lastname = Convert.ToString("NA");
            prm_rfs_reltoclient = Convert.ToInt64("0");
            prm_rfs_add1 = Convert.ToString("NA");
            prm_rfs_add2 = Convert.ToString("NA");
            prm_rfs_addsameclnt = Convert.ToBoolean(false);
            prm_rfs_city = Convert.ToString("NA");
            prm_rfs_state = Convert.ToInt64("0");
            prm_rfs_zipcode = Convert.ToString("NA");
            prm_rfs_note = Convert.ToString("NA");
            prm_emr_firstname = Convert.ToString("NA");
            prm_emr_lastname = Convert.ToString("NA");
            prm_emr_relationship = Convert.ToInt64("0");
            prm_emr_phonetype = Convert.ToString("NA");
            prm_emr_phoneno = Convert.ToString("NA");
            prm_emr_addsameclnt = Convert.ToBoolean(false);
            prm_emr_add1 = Convert.ToString("NA");
            prm_emr_add2 = Convert.ToString("NA");
            prm_emr_city = Convert.ToString("NA");
            prm_emr_state = Convert.ToInt64("0");
            prm_emr_zipcode = Convert.ToString("NA");
            prm_scd_firstname = Convert.ToString("NA");
            prm_scd_lastname = Convert.ToString("NA");
            prm_scd_relationship = Convert.ToInt64("0");
            prm_scd_phonetype = Convert.ToString("NA");
            prm_scd_phoneno = Convert.ToString("NA");
            prm_passwordreset = Convert.ToBoolean(false);
            prm_emailverify = Convert.ToBoolean(false);
            prm_user_Id = Convert.ToInt64("0");
            prm_userId = Convert.ToString("NA");
            prm_parentid = Convert.ToString("NA");
            prm_userrole = Convert.ToString("NA");
            prm_tokenid = Convert.ToString("NA");
            prm_tokendatetime = DateTime.Now;

            prm_useraddress = Convert.ToString("");
            prm_zipcode = Convert.ToString("");
            prm_usercity = Convert.ToString("");
            prm_userstate = Convert.ToInt64("0");
            prm_usercountry = Convert.ToInt64("0");
            prm_userphone = Convert.ToString("");
            prm_usermobile = Convert.ToString("");
            prm_infoconsnt = Convert.ToBoolean(false);
            prm_registerweblink = Convert.ToString("");
            prm_registertimestamp = DateTime.Now;
            prm_ipaddress = Convert.ToString("");
            prm_latlong = Convert.ToString("");
            prm_geocountry = Convert.ToString("");
            prm_georegion = Convert.ToString("");
            prm_geozipcode = Convert.ToString("");
            prm_geotimezone = Convert.ToString("");
            prm_geocountcode = Convert.ToString("");
            prm_geoisp = Convert.ToString("");

        }
        #endregion

        #region Declare Add/Edit/Delete Function

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.prm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_profilemasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 prm_id)
        {
            Boolean blnResult = false;
            this.prm_id = prm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_profilemasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean UpdateByMemberKey(string strMemberKey)
        {
            Boolean blnResult = false;
            this.prm_id = prm_id;
            this.AddEditDeleteFlag = "EDITBYMEMBERKEY";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_profilemasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean EmailVerify(string strTokenID, Boolean blnVerifyFlag)
        {
            Boolean blnResult = false;
            this.prm_tokenid = strTokenID;
            this.prm_emailverify = blnVerifyFlag;
            this.AddEditDeleteFlag = "EMAILVERIFY";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_profilemasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Email Account Verifyed Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Email Account Verification process."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean PasswordReset(string strUserID)
        {
            Boolean blnResult = false;
            this.prm_userId = strUserID;
            this.AddEditDeleteFlag = "PWDRESET";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_profilemasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Email Account Verifyed Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Email Account Verification process."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }
        
        public Boolean UpdateTokenID(string strMemberKey, string strTokenID)
        {
            Boolean blnResult = false;
            this.prm_userId = strMemberKey;
            this.prm_tokenid = strTokenID;
            this.AddEditDeleteFlag = "UPDATETOKEN";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_profilemasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                {
                    blnResult = true;
                    GetSetErrorMessage = "Save Successfully";
                }
                else
                {
                    blnResult = false;
                    GetSetErrorMessage = "Error in Process";
                }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 prm_id)
        {
            Boolean blnResult = false;
            this.prm_id = prm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_profilemasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_profilemasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetClientList()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_profilemasterGetData";
                SetGetSPFlag = "GETCLIENTLIST";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetClientListByParent(Guid ParentId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.prm_parentid = ParentId.ToString();
                clsCMD.CommandText = "sp_profilemasterGetData";
                SetGetSPFlag = "GETCLIENTLISTBYPARENT";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetClientListBymemberParent(Guid ParentId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.prm_parentid = ParentId.ToString();
                clsCMD.CommandText = "sp_profilemasterGetData";
                SetGetSPFlag = "GETCLIENTLISTBYMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsProfileMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsProfileMaster_PropertiesList> lstResult = new List<clsProfileMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].prm_id = Convert.ToInt64(dr["prm_id"]);
                lstResult[i].prm_initial = Convert.ToString(dr["prm_initial"]);
                lstResult[i].prm_firstname = Convert.ToString(dr["prm_firstname"]);
                lstResult[i].prm_middlename = Convert.ToString(dr["prm_middlename"]);
                lstResult[i].prm_lastname = Convert.ToString(dr["prm_lastname"]);
                lstResult[i].prm_dob = Convert.ToDateTime(dr["prm_dob"]);
                lstResult[i].prm_intakedate = Convert.ToDateTime(dr["prm_intakedate"]);
                lstResult[i].prm_ssn = Convert.ToString(dr["prm_ssn"]);
                lstResult[i].prm_emailid = Convert.ToString(dr["prm_emailid"]);
                lstResult[i].prm_gender = Convert.ToInt64(dr["prm_gender"]);
                lstResult[i].prm_ethincity = Convert.ToInt64(dr["prm_ethincity"]);
                lstResult[i].prm_langpreferance = Convert.ToInt64(dr["prm_langpreferance"]);
                lstResult[i].prm_maritalstatus = Convert.ToInt64(dr["prm_maritalstatus"]);
                lstResult[i].prm_empid = Convert.ToInt64(dr["prm_empid"]);
                lstResult[i].prm_student = Convert.ToBoolean(dr["prm_student"]);
                lstResult[i].prm_identifiedclient = Convert.ToInt64(dr["prm_identifiedclient"]);
                lstResult[i].prm_paymentoption = Convert.ToInt64(dr["prm_paymentoption"]);
                lstResult[i].prm_claimidprefix = Convert.ToInt64(dr["prm_claimidprefix"]);
                lstResult[i].prm_provider = Convert.ToInt64(dr["prm_provider"]);
                lstResult[i].prm_status = Convert.ToInt64(dr["prm_status"]);
                lstResult[i].prm_rfs_firstname = Convert.ToString(dr["prm_rfs_firstname"]);
                lstResult[i].prm_rfs_lastname = Convert.ToString(dr["prm_rfs_lastname"]);
                lstResult[i].prm_rfs_reltoclient = Convert.ToInt64(dr["prm_rfs_reltoclient"]);
                lstResult[i].prm_rfs_add1 = Convert.ToString(dr["prm_rfs_add1"]);
                lstResult[i].prm_rfs_add2 = Convert.ToString(dr["prm_rfs_add2"]);
                lstResult[i].prm_rfs_addsameclnt = Convert.ToBoolean(dr["prm_rfs_addsameclnt"]);
                lstResult[i].prm_rfs_city = Convert.ToString(dr["prm_rfs_city"]);
                lstResult[i].prm_rfs_state = Convert.ToInt64(dr["prm_rfs_state"]);
                lstResult[i].prm_rfs_zipcode = Convert.ToString(dr["prm_rfs_zipcode"]);
                lstResult[i].prm_rfs_note = Convert.ToString(dr["prm_rfs_note"]);
                lstResult[i].prm_emr_firstname = Convert.ToString(dr["prm_emr_firstname"]);
                lstResult[i].prm_emr_lastname = Convert.ToString(dr["prm_emr_lastname"]);
                lstResult[i].prm_emr_relationship = Convert.ToInt64(dr["prm_emr_relationship"]);
                lstResult[i].prm_emr_phonetype = Convert.ToString(dr["prm_emr_phonetype"]);
                lstResult[i].prm_emr_phoneno = Convert.ToString(dr["prm_emr_phoneno"]);
                lstResult[i].prm_emr_addsameclnt = Convert.ToBoolean(dr["prm_emr_addsameclnt"]);
                lstResult[i].prm_emr_add1 = Convert.ToString(dr["prm_emr_add1"]);
                lstResult[i].prm_emr_add2 = Convert.ToString(dr["prm_emr_add2"]);
                lstResult[i].prm_emr_city = Convert.ToString(dr["prm_emr_city"]);
                lstResult[i].prm_emr_state = Convert.ToInt64(dr["prm_emr_state"]);
                lstResult[i].prm_emr_zipcode = Convert.ToString(dr["prm_emr_zipcode"]);
                lstResult[i].prm_scd_firstname = Convert.ToString(dr["prm_scd_firstname"]);
                lstResult[i].prm_scd_lastname = Convert.ToString(dr["prm_scd_lastname"]);
                lstResult[i].prm_scd_relationship = Convert.ToInt64(dr["prm_scd_relationship"]);
                lstResult[i].prm_scd_phonetype = Convert.ToString(dr["prm_scd_phonetype"]);
                lstResult[i].prm_scd_phoneno = Convert.ToString(dr["prm_scd_phoneno"]);
                lstResult[i].prm_passwordreset = Convert.ToBoolean(dr["prm_passwordreset"]);
                lstResult[i].prm_emailverify = Convert.ToBoolean(dr["prm_emailverify"]);
                lstResult[i].prm_user_Id = Convert.ToInt64(dr["prm_user_Id"]);
                lstResult[i].prm_userId = Convert.ToString(dr["prm_userId"]);
                lstResult[i].prm_parentid = Convert.ToString(dr["prm_parentid"]);
                lstResult[i].prm_userrole = Convert.ToString(dr["prm_userrole"]);
                lstResult[i].prm_tokenid = Convert.ToString(dr["prm_tokenid"]);
                lstResult[i].prm_tokendatetime = Convert.ToDateTime(dr["prm_tokendatetime"]);
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 prm_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.prm_id = prm_id;
                clsCMD.CommandText = "sp_profilemasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByMemberKeyInProperties(String strMemberKey)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.prm_userId = strMemberKey;
                clsCMD.CommandText = "sp_profilemasterGetData";
                SetGetSPFlag = "BYMEMBERKEY";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByTokenIDInProperties(String strTokenID)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.prm_tokenid = strTokenID;
                clsCMD.CommandText = "sp_profilemasterGetData";
                SetGetSPFlag = "BYTOKENID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 prm_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.prm_id = prm_id;
                clsCMD.CommandText = "sp_profilemasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public DataTable GetUserList(Guid logedInUser)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.prm_userId = logedInUser.ToString();
                SetGetSPFlag = "GETCONTACT";
                clsCMD.CommandText = "sp_profilemasterGetData";

                //SetGetSPFlag = "GETCLIENTLIST";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        #endregion
    }

    public class clsProfileMaster_PropertiesList
    {
        public Int64 prm_id { get; set; }
        public String prm_initial { get; set; }
        public String prm_firstname { get; set; }
        public String prm_middlename { get; set; }
        public String prm_lastname { get; set; }
        public DateTime prm_dob { get; set; }
        public DateTime prm_intakedate { get; set; }
        public String prm_ssn { get; set; }
        public String prm_emailid { get; set; }
        public Int64 prm_gender { get; set; }
        public Int64 prm_ethincity { get; set; }
        public Int64 prm_langpreferance { get; set; }
        public Int64 prm_maritalstatus { get; set; }
        public Int64 prm_empid { get; set; }
        public Boolean prm_student { get; set; }
        public Int64 prm_identifiedclient { get; set; }
        public Int64 prm_paymentoption { get; set; }
        public Int64 prm_claimidprefix { get; set; }
        public Int64 prm_provider { get; set; }
        public Int64 prm_status { get; set; }
        public String prm_rfs_firstname { get; set; }
        public String prm_rfs_lastname { get; set; }
        public Int64 prm_rfs_reltoclient { get; set; }
        public String prm_rfs_add1 { get; set; }
        public String prm_rfs_add2 { get; set; }
        public Boolean prm_rfs_addsameclnt { get; set; }
        public String prm_rfs_city { get; set; }
        public Int64 prm_rfs_state { get; set; }
        public String prm_rfs_zipcode { get; set; }
        public String prm_rfs_note { get; set; }
        public String prm_emr_firstname { get; set; }
        public String prm_emr_lastname { get; set; }
        public Int64 prm_emr_relationship { get; set; }
        public String prm_emr_phonetype { get; set; }
        public String prm_emr_phoneno { get; set; }
        public Boolean prm_emr_addsameclnt { get; set; }
        public String prm_emr_add1 { get; set; }
        public String prm_emr_add2 { get; set; }
        public String prm_emr_city { get; set; }
        public Int64 prm_emr_state { get; set; }
        public String prm_emr_zipcode { get; set; }
        public String prm_scd_firstname { get; set; }
        public String prm_scd_lastname { get; set; }
        public Int64 prm_scd_relationship { get; set; }
        public String prm_scd_phonetype { get; set; }
        public String prm_scd_phoneno { get; set; }
        public Boolean prm_passwordreset { get; set; }
        public Boolean prm_emailverify { get; set; }
        public Int64 prm_user_Id { get; set; }
        public String prm_userId { get; set; }
        public String prm_parentid { get; set; }
        public String prm_userrole { get; set; }
        public String prm_tokenid { get; set; }
        public DateTime prm_tokendatetime { get; set; }

        public String prm_useraddress { get; set; }
        public String prm_zipcode { get; set; }
        public String prm_usercity { get; set; }
        public Int64 prm_userstate { get; set; }
        public Int64 prm_usercountry { get; set; }
        public String prm_userphone { get; set; }
        public String prm_usermobile { get; set; }
        public Boolean prm_infoconsnt { get; set; }
        public String prm_registerweblink { get; set; }
        public DateTime prm_registertimestamp { get; set; }
        public String prm_ipaddress { get; set; }
        public String prm_latlong { get; set; }
        public String prm_geocountry { get; set; }
        public String prm_georegion { get; set; }
        public String prm_geozipcode { get; set; }
        public String prm_geotimezone { get; set; }
        public String prm_geocountcode { get; set; }
        public String prm_geoisp { get; set; }
    }
}