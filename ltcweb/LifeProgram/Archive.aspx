﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Archive.aspx.cs" Inherits="LifeProgram_Archive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="glyphicon-dashboard"></i>
                        Archives
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-column form-bordered' style="padding-bottom: 5px;">
                        <div>
                            <div class="span12">
                                <h2>Sexual Abuse Recovery Seminars</h2>
                            </div>
                        </div>

                        <div>
                            <div class="span12" style="font-weight: bold; background-color: #296a9d; color: #FFFFFF; padding-top: 5px;">
                                <div style="float: left; width: 600px; text-align: center">
                                    Title
                                </div>

                                <div style="float: left; width: 250px; text-align: center">
                                    Play
                                </div>

                                <div style="float: left; width: 250px; text-align: center">
                                    DownLoad
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="span12" style="background-color: #FFFFFF; color: #000000; padding-top: 5px;">
                                <div id="s_title" style="float: left; width: 600px; text-align: center;">
                                    Salena Potter's Story - Achieving Healthy Relationships After Having Been Sexually Abused - April 5, 2012
                                </div>

                                <div id="s_play" style="float: left; width: 250px; text-align: center; padding-top: 15px">
                                    <script src="http://www.lifestyletherapycoach.com/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        AC_FL_RunContent('type', 'application/x-shockwave-flash', 'data', 'dewplayer/dewplayer.swf?mp3=seminars/SASeminar_SalenaPotter_2012APR05.mp3', 'width', '150', 'height', '20', 'id', 'dewplayer', 'wmode', 'transparent', 'movie', 'dewplayer/dewplayer?mp3=seminars/SASeminar_SalenaPotter_2012APR05.mp3'); //end AC code
                                    </script>
                                </div>

                                <div id="s_dwnld" style="float: left; width: 250px; text-align: center;">
                                    <a href="" id="SASeminar_SalenaPotter_2012APR05.mp3" name="dwnldlink">
                                        <img src="../img/download_button.png" /></a>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="span12" style="background-color: #b7d0f3; color: #000000; padding-top: 5px;">
                                <div id="Div1" style="float: left; width: 600px; text-align: center">
                                    Rhonda Stalb's Story - Achieving Healthy Relationships After Having Been Sexually Abused - May 3, 2012
                                </div>

                                <div id="Div2" style="float: left; width: 250px; text-align: center; padding-top: 15px">
                                    <script src="http://www.lifestyletherapycoach.com/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        AC_FL_RunContent('type', 'application/x-shockwave-flash', 'data', 'dewplayer/dewplayer.swf?mp3=seminars/SASeminar-HowSAaffectsMaritalRelationships-RhondaStalb.mp3', 'width', '150', 'height', '20', 'id', 'dewplayer', 'wmode', 'transparent', 'movie', 'dewplayer/dewplayer?mp3=seminars/SASeminar-HowSAaffectsMaritalRelationships-RhondaStalb.mp3'); //end AC code
                                    </script>
                                </div>

                                <div id="Div3" style="float: left; width: 250px; text-align: center">
                                    <a href="" id="SASeminar-HowSAaffectsMaritalRelationships-RhondaStalb.mp3" name="dwnldlink">
                                        <img src="../img/download_button.png" /></a>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="span12" style="background-color: #FFFFFF; color: #000000; padding-top: 5px;">
                                <div id="Div4" style="float: left; width: 600px; text-align: center;">
                                    Erica's Story - Achieving Healthy Relationships After Having Been Sexually Abused - June 7, 2012
                                </div>

                                <div id="Div5" style="float: left; width: 250px; text-align: center; padding-top: 15px">
                                    <script src="http://www.lifestyletherapycoach.com/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        AC_FL_RunContent('type', 'application/x-shockwave-flash', 'data', 'dewplayer/dewplayer.swf?mp3=seminars/SASeminar_Erica_Jones_2012JUN06.mp3', 'width', '150', 'height', '20', 'id', 'dewplayer', 'wmode', 'transparent', 'movie', 'dewplayer/dewplayer?mp3=seminars/SASeminar_Erica_Jones_2012JUN06.mp3'); //end AC code
                                    </script>
                                </div>

                                <div id="Div6" style="float: left; width: 250px; text-align: center">
                                    <a href="" id="SASeminar_Erica_Jones_2012JUN06.mp3" name="dwnldlink">
                                        <img src="../img/download_button.png" /></a>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="span12" style="background-color: #b7d0f3; color: #000000; padding-top: 5px;">
                                <div id="Div7" style="float: left; width: 600px; text-align: center;">
                                    Lynn's Story - Achieving Healthy Relationships After Having Been Sexually Abused - July 7, 2012
                                </div>

                                <div id="Div8" style="float: left; width: 250px; text-align: center; padding-top: 15px">
                                    <script src="http://www.lifestyletherapycoach.com/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        AC_FL_RunContent('type', 'application/x-shockwave-flash', 'data', 'dewplayer/dewplayer.swf?mp3=seminars/SASeminar_Lynn_2012JUL12.mp3', 'width', '150', 'height', '20', 'id', 'dewplayer', 'wmode', 'transparent', 'movie', 'dewplayer/dewplayer?mp3=seminars/SASeminar_Lynn_2012JUL12.mp3'); //end AC code
                                    </script>
                                </div>

                                <div id="Div9" style="float: left; width: 250px; text-align: center">
                                    <a href="" id="SASeminar_Lynn_2012JUL12.mp3" name="dwnldlink">
                                        <img src="../img/download_button.png" /></a>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="span12" style="background-color: #FFFFFF; color: #000000; padding-top: 5px;">
                                <div id="Div10" style="float: left; width: 600px; text-align: center;">
                                    Naomi's Story - Achieving Healthy Relationships After Having Been Sexually Abused - August 2, 2012
                                </div>

                                <div id="Div11" style="float: left; width: 250px; text-align: center; padding-top: 15px">
                                    <script src="http://www.lifestyletherapycoach.com/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        AC_FL_RunContent('type', 'application/x-shockwave-flash', 'data', 'dewplayer/dewplayer.swf?mp3=seminars/SASeminar_NaomiColeman_2012AUG02.mp3', 'width', '150', 'height', '20', 'id', 'dewplayer', 'wmode', 'transparent', 'movie', 'dewplayer/dewplayer?mp3=seminars/SASeminar_NaomiColeman_2012AUG02.mp3'); //end AC code
                                    </script>
                                </div>

                                <div id="Div12" style="float: left; width: 250px; text-align: center">
                                    <a href="" id="SASeminar_NaomiColeman_2012AUG02.mp3" name="dwnldlink">
                                        <img src="../img/download_button.png" /></a>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="span12" style="background-color: #b7d0f3; color: #000000; padding-top: 5px;">
                                <div id="Div13" style="float: left; width: 600px; text-align: center;">
                                    Victoria's Story - Achieving Healthy Relationships After Having Been Sexually Abused - December 6, 2012
                                </div>

                                <div id="Div14" style="float: left; width: 250px; text-align: center; padding-top: 15px">
                                    <script src="http://www.lifestyletherapycoach.com/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        AC_FL_RunContent('type', 'application/x-shockwave-flash', 'data', 'dewplayer/dewplayer.swf?mp3=seminars/SASeminar_Vicky_Miller_2012DEC06.mp3', 'width', '150', 'height', '20', 'id', 'dewplayer', 'wmode', 'transparent', 'movie', 'dewplayer/dewplayer?mp3=seminars/SASeminar_Vicky_Miller_2012DEC06.mp3'); //end AC code
                                    </script>
                                </div>

                                <div id="Div15" style="float: left; width: 250px; text-align: center">
                                    <a href="" id="SASeminar_Vicky_Miller_2012DEC06.mp3" name="dwnldlink">
                                        <img src="../img/download_button.png" /></a>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="span12" style="background-color: #FFFFFF; color: #000000; padding-top: 5px;">
                                <div id="Div16" style="float: left; width: 600px; text-align: center;">
                                    Garlena Hines Story - Achieving Healthy Relationships After Having Been Sexually Abused - April 4, 2013
                                </div>

                                <div id="Div17" style="float: left; width: 250px; text-align: center; padding-top: 15px">
                                    <script src="http://www.lifestyletherapycoach.com/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        AC_FL_RunContent('type', 'application/x-shockwave-flash', 'data', 'dewplayer/dewplayer.swf?mp3=seminars/SASeminar_GarlenaHines_2013APR04.mp3', 'width', '150', 'height', '20', 'id', 'dewplayer2', 'wmode', 'transparent', 'movie', 'dewplayer/dewplayer?mp3=seminars/SASeminar_GarlenaHines_2013APR04.mp3'); //end AC code
                                    </script>
                                </div>

                                <div id="Div18" style="float: left; width: 250px; text-align: center">
                                    <a href="" id="SASeminar_GarlenaHines_2013APR04.mp3" name="dwnldlink">
                                        <img src="../img/download_button.png" /></a>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="span12" style="background-color: #b7d0f3; color: #000000; padding-top: 5px;">
                                <div id="Div22" style="float: left; width: 600px; text-align: center;">
                                    Christine's Story Part 1 - Achieving Healthy Relationships After Having Been Sexually Abused - May 2, 2013
                                </div>

                                <div id="Div23" style="float: left; width: 250px; text-align: center; padding-top: 15px">
                                    <script src="http://www.lifestyletherapycoach.com/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        AC_FL_RunContent('type', 'application/x-shockwave-flash', 'data', 'dewplayer/dewplayer.swf?mp3=seminars/SASeminar_Christine_2013MAY02.mp3', 'width', '150', 'height', '20', 'id', 'dewplayer2', 'wmode', 'transparent', 'movie', 'dewplayer/dewplayer?mp3=seminars/SASeminar_Christine_2013MAY02.mp3'); //end AC code
                                    </script>
                                </div>

                                <div id="Div24" style="float: left; width: 250px; text-align: center">
                                    <a href="" id="SASeminar_Christine_2013MAY02.mp3" name="dwnldlink">
                                        <img src="../img/download_button.png" /></a>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="span12" style="background-color: #FFFFFF; color: #000000; padding-top: 5px;">
                                <div id="Div19" style="float: left; width: 600px; text-align: center;">
                                    Christine's Story Part 2 - Achieving Healthy Relationships After Having Been Sexually Abused -June 6, 2013
                                </div>

                                <div id="Div20" style="float: left; width: 250px; text-align: center; padding-top: 15px">
                                    <script src="http://www.lifestyletherapycoach.com/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                        AC_FL_RunContent('type', 'application/x-shockwave-flash', 'data', 'dewplayer/dewplayer.swf?mp3=seminars/SASeminar_Christine_2013JUN06.mp3', 'width', '150', 'height', '20', 'id', 'dewplayer3', 'wmode', 'transparent', 'movie', 'dewplayer/dewplayer?mp3=seminars/SASeminar_Christine_2013JUN06.mp3'); //end AC code
                                    </script>
                                </div>

                                <div id="Div21" style="float: left; width: 250px; text-align: center">
                                    <a href="" id="SASeminar_Christine_2013JUN06.mp3" name="dwnldlink">
                                        <img src="../img/download_button.png" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hidden" name="hidden" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("[name='dwnldlink']").click(function () {
                var id = this.id;
                $("#<%= hidden.ClientID %>").val(id);
                $("#form1").submit();
                return false;
            });
        });
    </script>
</asp:Content>

