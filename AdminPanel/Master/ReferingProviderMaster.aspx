﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="ReferingProviderMaster.aspx.cs" Inherits="Master_referingProvider" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <%if (!User.IsInRole("administrator"))
      { %>
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn" Text="Add New Provider" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>
    </div>
    <%} %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Refering Provider List
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptRfrPro" runat="server" OnItemCommand="rptRfrPro_ItemCommand">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <%if (User.IsInRole("administrator"))
                                          { %>
                                        <th>Organisation</th>
                                        <%} %>
                                        <th>Provider Name</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Fax</th>
                                        <th>Email</th>
                                        <%if (!User.IsInRole("administrator"))
                                          { %>
                                        <th></th>
                                        <%} %>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <%if (User.IsInRole("administrator"))
                                  { %>
                                <td><%# Eval("op_organizationname") %></td>
                                <%} %>
                                <td><%# Eval("rp_name") %></td>
                                <td><%# Eval("rp_address") %></td>
                                <td><%# Eval("rp_contact") %></td>
                                <td><%# Eval("rp_fax") %></td>
                                <td><%# Eval("rp_email") %></td>
                                <%if (!User.IsInRole("administrator"))
                                  { %>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rp_id") %>'><i class="icon-edit-sign"></i> &nbsp;Edit</asp:LinkButton>
                                    <asp:LinkButton ID="cmdDlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rp_id") %>' rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i> </asp:LinkButton>
                                </td>
                                <%} %>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

