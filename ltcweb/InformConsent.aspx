﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InformConsent.aspx.cs" Inherits="InformConsent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="glyphicon-user_add"></i>&nbsp;Informed Consent</h2>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3>
                        <i class="icon-edit"></i>Informed Consent for Clinical Services</h3>
                </div>
                <div class="box-content">
                    <div class='form-horizontal'>
                        <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red" Font-Size="Large"></asp:Label>
                        <p>Welcome. It is important to us that you fully understand your rights and responsibilities as our client. Please carefully read this document and attest with your signature below. All additional family members need to complete this form.</p>
                        <div class="control-group">
                            <label for="txtFirstname" class="control-label">
                                First Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtFirstname" runat="server" name="txtFirstname" class="input-xlarge"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtLastname" class="control-label">
                                Last Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtLastname" runat="server" name="txtLastname" class="input-xlarge"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">
                                Email</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on">@</span>
                                    <asp:TextBox ID="txtEmail" runat="server" placeholder="username"></asp:TextBox>
                                </div>
                                <span class="help-block">Email Address will consider as username</span>
                            </div>
                        </div>
                        <h5><i class="glyphicon-clock"></i>&nbsp;BUSINESS HOURS</h5>
                        <p>Our standard office hours are currently Monday – Thursday from 8:00 AM to 6:00 PM and on Fridays 8:00-2:00 PM. </p>
                        <h5><i class="glyphicon-calendar"></i>&nbsp;APPOINTMENTS</h5>
                        <p>Appointments  are normally 50 minutes in duration. First time visits may go up to 80 minutes.  We see clients by appointment only. To change or cancel an appointment you must  give advance notice. Please allow at least 24 hours notice for any  cancellations. <u>Individuals who do not show for an appointment or cancel less  than 24 hours before the appointment will be charged a $75 fee</u>. The late  fee will not be filed to insurance company. Appointments can be made or cancelled by calling  our office at (256) 850-4426 or <a href="Client/Appointment.aspx" target="_blank">appointment.lifestyletherapycoach.com</a>.</p>
                        <h5><i class="glyphicon-hospital"></i>&nbsp;EMERGENCY SERVICES</h5>
                        <p>We are not available for emergencies or after hours calls. If you have a history of frequent emergencies, or if you anticipate circumstances that will require emergency intervention, your needs will be better served by local providers who are designed to address these critical situations. If you do have an emergency while in treatment with us, you will need to use the available emergency options. Of course call 911 for immediate emergency assistance.</p>
                        <h5><i class="glyphicon-phone"></i>&nbsp;TELEPHONE CALLS</h5>
                        <p>If  you have administrative questions or concerns, please leave a message at our  answering service at (256) 850-4426. We receive messages immediately.  If you request a telephone consultation, please  leave your name,  number and the best times to  reach you. We will return your call as soon as our schedule permits.  Please be clear about your request. Telephone consultations are billed at the  regular rate. Skype sessions are also billed at the same rate. Email, text and social networking communications are billed per incident or as a package deal. See our resources page for more information (<a href="Resources.aspx" target="_blank">payment.lifestyletherapycoach.com</a>).</p>
                        <h5><i class="icon-legal"></i>&nbsp;YOUR RIGHTS</h5>
                        <p>
                            As  a provider of mental, emotional and relational counseling services, our role is  to provide professional assessment and/or treatment. To establish and maintain  a good working relationship, there are certain rights and principles, of which  you should be aware. The goal is for you to have all necessary information  prior to the start of treatment.
                        </p>
                        <ul>
                            <li>You have the right to receive treatment  from another Therapist.</li>
                            <li>You have the right to refuse treatment or  to end therapy at any time without any moral or legal (except fee agreement)  obligation.</li>
                            <li>You have the right to ask questions about  everything that has taken place at any time with regard to administrative or  clinical functions at our office.</li>
                            <li>You have the right to voice your opinion,  recommendations and grievances in relation to policies and services without  fear of interference, coercion, discrimination or reprisal.</li>
                            <li>You have the right to continuity of care.  If discharge, termination, or transfer becomes necessary you will be given  adequate assistance and information to make the transition.</li>
                            <li>You will have the right to refuse  electronic recording. Permission to record will be done only after you have  consented to the same in writing and have also been informed of the intended  use of the recording.</li>
                            <li>You understand that communication between  you, as a client, and the counselor is protected by law and that we can only  release information about our sessions to others with your written permission.  However, there are a few exceptions:</li>
                        </ul>

                        <blockquote>
                            <p>1.  In most judicial proceedings, you have the right to prevent your counselor from  testifying, however, in child custody and adoption proceedings and proceedings  in which your emotional condition is an important element, a judge may require your  counselor’s testimony. If you are involved in litigation, or are anticipating litigation,  and you choose to include your mental or emotional state as part of the litigation,  your counselor may have to reveal part or all of your treatment or evaluation  records, impressions, and recommendations. Please note that payments for  therapists court appearance must be recieved 7 days prior to court case. Therapists' time is billed in half-day (4 hour) increments at $250 per hour.</p>
                            <br />
                            <p>2.  If your counselor is  called as a witness in criminal proceedings, opposing counsel may  have some limited access to your treatment records. Testimony may also be  ordered in (a) legal proceedings relating to psychiatric hospitalization; (b)  in malpractice and disciplinary proceedings brought against a mental health professional;  (c) court-ordered mental health evaluation; and, (d) certain legal cases where  the client has died. Our counselors are prohibited to testify in divorce proceedings where both parties have been seen by the counselor.</p>
                            <br />
                            <p>3.  There are some circumstances when a counselor is required to breach  confidentiality without a client’s permission. This occurs if the counselor  suspects the neglect or abuse of a minor, in which case he/she must file a  report with the appropriate state agency. If, in your counselor’s professional  judgment, it is believed that a client is threatening serious harm to another, he/she  is required to take protective action that may include notifying the police, warning  the intended victim, or seeking the client's hospitalization. If a client  threatens to harm him or herself, the counselor may be required to seek  hospitalization for the client.</p>
                            <br />
                            <p>4.  The clear intent of these requirements is that your counselor has both a legal  and ethical responsibility to take action to protect endangered individuals  from harm when their judgment indicates that such danger exists. Fortunately,  these situations rarely arise.</p>
                        </blockquote>
                        <h5><i class="icon-lock"></i>&nbsp;CONCERNING CONFIDENTIALITY </h5>
                        <p>1. On occasion it may be helpful or necessary for your counselor to consult about a case with another professional. Many of our counselors are being supervised for training/licensing and will staff their cases with their supervisor. In these consultations, every effort is made to avoid revealing the identity of the client. The consultant is, of course, also legally bound to maintain confidentiality.</p>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="box">
                                    <div class="box-title">
                                        <h3>
                                            <i class="icon-money"></i>
                                            FEES
								</h3>
                                    </div>
                                    <div class="box-content nopadding">
                                        <table class="table table-hover table-nomargin table-condensed">
                                            <thead>
                                                <tr>
                                                    <th>Services</th>
                                                    <th>Rate</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Assessment Session (60 minutes)</td>
                                                    <td>$220 </td>
                                                </tr>
                                                <tr>
                                                    <td>Follow-up Session (50 minutes)</td>
                                                    <td>$150</td>
                                                </tr>
                                                <tr>
                                                    <td>Extended Session (<30 minutes)</td>
                                                    <td>$75</td>
                                                </tr>
                                                <tr>
                                                    <td>Group Session</td>
                                                    <td>$80</td>
                                                </tr>
                                                <tr>
                                                    <td>No Show/Late Cancel</td>
                                                    <td>$75</td>
                                                </tr>
                                                <tr>
                                                    <td>Average Insurance Copayment</td>
                                                    <td>$10 - $50</td>
                                                </tr>
                                                <tr>
                                                    <td>Average # of Sessions</td>
                                                    <td>$8-16</td>
                                                </tr>
                                                <tr>
                                                    <td>Return Check Fee</td>
                                                    <td>$32 </td>
                                                </tr>
                                                <tr>
                                                    <td>Tests and Measures </td>
                                                    <td>$199 </td>
                                                </tr>
                                                <tr>
                                                    <td>Psychosocial Assessment</td>
                                                    <td>$350</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>Many of our clients take advantage of our <b>treatment programs</b>. Our treatment programs provide a more comprehensive approach to addressing the complex challenges many of our clients face. Relationship problems often involve significant clinical issues that generally take several months to work through. ie. Substance abuse, affair, depression, anxiety, grief, pornography, sexual abuse, sexual problems, anger management. Our extensive treatment programs ensure that our clients make positive lifestyle changes that last a lifetime. What you focus on you cause to happen. We currently offer the <a href="http://www.recoveringmylife.com">Lifestyle Life Recovery</a> and<a href="shmMaker.asp"> Sizzling Hot Marriage Maker </a>programs.</p>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="box">
                                    <div class="box-title">
                                        <h3>
                                            <i class="icon-circle"></i>
                                            Programs
								</h3>
                                    </div>
                                    <div class="box-content nopadding">
                                        <table class="table table-hover table-nomargin table-condensed">
                                            <thead>
                                                <tr>
                                                    <th>Programs</th>
                                                    <th>Rate</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Program Fee - 12-Month Therapist Retainer/Concierge with Mobile, Email & Text Support, An Assessment Session, An Assessment Test, Online Workbook, Case Management, and Priority Scheduling</td>
                                                    <td>$499</td>
                                                </tr>
                                                <tr>
                                                    <td>Prepaid Therapy (6 Sessions)</td>
                                                    <td>$599</td>
                                                </tr>
                                                <tr>
                                                    <td>Recovery Group Therapy (12 Sessions)</td>
                                                    <td>$360</td>
                                                </tr>
                                                <tr>
                                                    <td>Minimum Down Payment</td>
                                                    <td>$250</td>
                                                </tr>
                                                <tr>
                                                    <td>Installment Fee (per payment)</td>
                                                    <td>$10</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                            Insurance may only cover a portion of program fee.<br />
                            Insurance payments and copayments may cover therapy fees.<br />
                            Clients are responsible for fees unpaid by insurance.<br />
                            Other than the $99 monthly installment plan all fees must be paid by the last session.<br />
                            We accept cash, checks and credit/debit cards.<br />
                            1.5% late fees on unpaid balances each month.<br />
                            We initiate collections after 60 days of non-payment.<br />
                            Fees can also be paid at <a href="Resources.aspx" target="_blank">payment.lifestyletherapycoach.com</a>.
                        </p>
                        <p>We do offer sliding scale fees for clients who can establish need.</p>
                        <p><strong>Referrals  - </strong>If  you find our services helpful please be sure to let others know we have  openings we are looking to fill. We are thankful for your kind words about us.</p>

                        <hr />
                        <div class="form-actions">
                            <asp:Button ID="cmdRegister" runat="server" CssClass="btn btn-primary" Text="Proceed For Registration"/>
                        </div>

                        <p><b>Please complete our intake assessment application prior to your first session.</b> You can download our application below. You can email to admin@LifestyleTherapyCoach.com or print and bring in with you.</p>
                        <p><strong><a href="/downloads/INTAKE_FORM.doc" target="_blank">Intake Form - WORD VERSION</a></strong> (Right click | Save link as)</p>
                        <p><strong><a href="/downloads/INTAKE_FORM.pdf" target="_blank">Intake Form - PDF</a></strong> <a href="/downloads/INTAKE_FORM.pdf"><strong>VERSION</strong></a></p>
                        <p><strong>Referral Forms</strong></p>
                        <p>Release of Information - <a href="/downloads/Release_of_Information_Update.pdf" target="_blank">PDF</a> <a href="/downloads/Release_of_Information_Update.doc" target="_blank">DOC</a></p>
                        <p>Physician Referral - <a href="/downloads/PhysicianReferralForm.pdf" target="_blank">PDF</a> <a href="/downloads/PhysicianReferralForm.doc" target="_blank">DOC</a></p>
                        <p><a href="Client/Appointment.aspx" class="wrapper">Referral Scheduling</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

