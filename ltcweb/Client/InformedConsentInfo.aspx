﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InformedConsentInfo.aspx.cs" Inherits="InformedConsentN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div style="text-align: justify; border: 2px black solid; padding: 5px 15px 5px 15px;">
        <h3 style="text-align: center">Informed Consent</h3>
        <p>
            <strong>
                <img src="../img/logo-Lifestyle-ChiroCare-HYFB.png" style="height: 91px; width: 236px" /></strong>
        </p>

        <p>The clinicians and staff of Lifestyle Therapy &amp; Coaching appreciate the confidence you have shown in choosing us to provide for your health care needs. We are committed to providing you the best possible care. It is important to us that you fully understand your rights and responsibilities as our client. Please carefully read this document and attest with your signature below. All additional adult family members in treatment need to complete a copy of this form as well.</p>

        <p><strong>BUSINESS HOURS</strong></p>

        <p>Our normal office hours are currently Monday &ndash; Thursday from 8:10 AM to 6:00 PM and on Fridays 8:10-4:00 PM. Therapy &amp; Coaching appointments are normally 50 minutes in duration.Chiropractic appointments vary in length. Dietetic appointments are 30 -50 minutes in length.Appointments can be made by calling our office at (256) 850-4426 or <a href="file:///C:/Users/Joseph/Documents/Counseling/Forms/appointment.lifestyletherapycoach.com">appointment.lifestyletherapycoach.com</a>.</p>

        <p><strong>EMERGENCY SERVICES</strong></p>

        <p>We are not available for emergencies or after-hours calls. If you have a history of frequent emergencies, or if you anticipate circumstances that will require emergency intervention, your needs will be better served by local providers who are designed to address these critical situations. If you do have an emergency while in treatment with us, you will need to use the available emergency options. Some of the emergency rooms in this area are: Crestwood Hospital - Huntsville Hospital - Decatur General West</p>

        <p><strong>TELEPHONE CALLS</strong></p>

        <p>If you have administrative questions or concerns, please leave a message at our answering service at (256) 850-4426. We generally receive messages within the hour.&nbsp; If you request a telephone consultation, please leave your name and the best number to be reached. Also leave the best times to reach you as well. We will return your call as soon as our schedule permits. Please be clear about your request. Telephone consultations are billed at the regular rate. Skype sessions are also billed at the same rate.</p>

        <br />

        <h3 style="color: cadetblue">Informed Consent for Clinical Services</h3>

        <p>As a provider of professional services, our role is to provide professional assessment and treatment to help you reach your goals. To establish and maintain a good working relationship, there are certain rights and principles, of which you should be aware. The goal is for you to have all necessary information prior to the start of treatment.</p>

        <ul>
            <li>You have the right to receive treatment from another provider.</li>
            <li>You have the right to refuse treatment or to end treatment at any time without any moral or legal (except fee agreement) obligation.</li>
            <li>You have the right to ask questions about everything that has taken place at any time with regard to administrative or clinical functions at our office.</li>
            <li>You have the right to voice your opinion, recommendations and grievances in relation to policies and services without fear of interference, coercion, discrimination or reprisal.</li>
            <li>You have the right to receive a copy of your records. However, a request for records more than likely indicates termination of therapy. Record requests cost $1 per page.</li>
            <li>You have the right to continuity of care. If discharge, termination, or transfer becomes necessary you will be given adequate assistance and information to make the transition.</li>
            <li>You understand that communication between you, as a client, and the professional is protected by law (HIPAA) and that we can only release information about our sessions to others with your written permission.</li>
        </ul>

        <br />

        <p><strong>In the case of mental health treatment </strong>there are a few exceptions to our confidentiality rules:</p>

        <ol>
            <li>In most judicial proceedings, YOU have the right to prevent your counselor from testifying, however, in child custody and adoption proceedings and proceedings in which your emotional condition is an important element, a judge may require your counselor&rsquo;s testimony. If you are involved in litigation, or are anticipating litigation, and you choose to include your mental or emotional state as part of the litigation, your counselor may have to reveal part or all of your treatment or evaluation records, impressions, and recommendations.</li>
            <li>If you are called as a witness in criminal proceedings, opposing counsel may have some limited access to your treatment records. Testimony may also be ordered in (a) legal proceedings relating to psychiatric hospitalization; (b) in malpractice and disciplinary proceedings brought against a mental health professional; (c) court-ordered mental health evaluation; and, (d) certain legal cases where the client has died. Our counselors are prohibited to testify in divorce proceedings where both parties have been seen by the counselor.</li>
            <li>There are some circumstances when a counselor is required to breach confidentiality without a client&rsquo;s permission. This occurs if the counselor suspects the neglect or abuse of a minor, in which case he/she must file a report with the appropriate state agency. If, in your counselor&rsquo;s professional judgment, it is believed that a client is threatening serious harm to another, he/she is required to take protective action that may include notifying the police, warning the intended victim, or seeking the client&#39;s hospitalization. If a client threatens to harm him or herself, the counselor may be required to seek hospitalization for the client.</li>
            <li>On occasion it may be helpful or necessary for your counselor to consult about a case with another professional. Counselors in training consult with supervisors to ensure they are following protocol and providing effective treatment. In these consultations, client confidentiality is maintained.</li>
            <li>From time to time a counseling session may be recorded for the sake of therapeutic intervention or training.&nbsp; Recordings are kept confidential and your written permission will be required prior to recording.&nbsp;</li>
            <li>In the case of third party reimbursement we are often required to provide the insurer with a clinical diagnostic impression and sometimes a treatment plan or summary.</li>
        </ol>

        <br />

        <p>The clear intent of these requirements is that your counselor has both a legal and ethical responsibility to take action to protect endangered individuals from harm when their judgment indicates that such danger exists. Fortunately, these situations rarely arise.</p>

        <br />

        <p><strong>In the case of chiropractic treatment:</strong></p>

        <ol>
            <li>I hereby request and consent to the performance of chiropractic adjustments and other chiropractic procedures, including various modes of physical therapy and diagnostic X-rays, on me (or on the patient named below, for whom I am legally responsible) by the doctor of chiropractic who now or in the future works at Lifestyle Therapy &amp; Coaching.</li>
        </ol>

        <br />

        <ol>
            <li>I recognize that there are some risks that may be associated with treatment, in particular:
	            <ol>
                    <li>While rare, some patients have experienced rib fractures or muscle and ligament sprains or strains following treatment;</li>
                    <li>There have been rare reported cases of disc injuries following cervical and lumbar spinal adjustment although no scientific study bas ever demonstrated such injuries are caused, or may be caused, by spinal or soft tissue manipulation or treatment.</li>
                    <li>There have been reported cases of injury to a vertebral artery following osseous spinal manipulation. Vertebral artery injuries have been known to cause a stroke, sometimes with serious neurological impairment, and may, on rare occasion, result in paralysis or death. The possibility of such injuries resulting from cervical spine manipulation is extremely remote;</li>
                </ol>
            </li>
        </ol>

        <ol>
            <li>Osseous and soft tissue manipulation bas been the subject of government reports and multi-disciplinary studies conducted over many years and have demonstrated it to be highly effective treatment of spinal conditions including general pain and loss of mobility, headaches and other related symptoms. Musculoskeletal care contributes to your overall well-being. The risk of injuries or complications from treatment is substantially lower than that associated with many medical or other treatments, medications, and procedures given for the same symptoms.</li>
        </ol>

        <p><strong>In the case of Dietetic/Medical Nutrition Therapy:</strong></p>

        <ol>
            <li>I give consent to receive Medical Nutrition Therapy from the Registered Dietitian at Lifestyle Therapy &amp; Coaching. The consult will provide information and guidance about health factors within my own control (my diet, nutrition, and lifestyle) in order to nourish and support my health and wellness.&nbsp;</li>
            <li>I understand that a Registered Dietitian/Nutritionist and Nutrition Educator and does not dispense medical advice nor prescribe treatment. Rather, she provides education to enhance my knowledge of health as it relates to foods, dietary supplements, and behaviors associated with eating. While nutritional and botanical support can be an important compliment to my medical care, I understand nutrition counseling is not a substitute for the diagnosis, treatment, or care of disease by a medical provider.</li>
        </ol>

        <br />

        <h3 style="color: cadetblue">Patient Financial Responsibility Disclosure Statement</h3>

        <p>The services you allow us to provide imply a financial responsibility on your part. This responsibility obligates you to ensure payment in full for services. As a courtesy we will bill your insurance carrier on your behalf. However, you are ultimately responsible for payment for any and all services rendered by Lifestyle Therapy &amp; Coaching. Your signature below forms a binding agreement between Lifestyle Therapy &amp; Coaching and the client or responsible party, and authorizes Lifestyle Therapy &amp; Coaching to: release to insurer any medical information necessary to process your insurance claims, and authorize payment of benefits directly to clinicians on your behalf.</p>

        <br />

        <p><strong>INSURANCE</strong></p>

        <p>While the filing of an insurance claim is a courtesy that we extend to our clients, it is your responsibility to:</p>

        <ol>
            <li>Bring your insurance card to each visit,</li>
            <li>Notify our office of any changes to your insurance information.</li>
            <li>Know your co-pay and deductible and be prepared to pay at the time of each visit,</li>
            <li>Know your insurance benefits and coverage,</li>
            <li>Determine if your clinician(s) are network providers prior to first visit,</li>
            <li>Pay for any remaining amount not covered by your insurance policy.</li>
        </ol>

        <p>&nbsp;</p>

        <p><strong>PAYMENTS</strong></p>

        <ol>
            <li>All co-pays, co-insurance, and deductibles are due <strong>PRIOR</strong> to services being rendered and is required by your insurance to be paid each visit.</li>
            <li>Procedures, services and products which are excluded from coverage, based on your plan&rsquo;s determination of medical necessity, will be your responsibility.&nbsp;</li>
            <li>If you do not know your co-pay we will collect a minimum fee of $30. Our billing department will bill or credit your account accordingly after your insurance pays their portion. If you are not prepared or able to pay your co-pay prior to your visit, we will kindly reschedule your appointment for a more convenient time.</li>
            <li>Over payments will be refunded after all charges have been processed and paid by your insurance company. A refund check or credit to your card will be applied or mailed within 30 days of your request.</li>
            <li>Self-pay clients will be required to pay a $150 deposit for their visit, at the time of check in. The deposit will be applied to the balance due. Payment on unpaid balances is expected within 30 days. If you are unable to pay the balance in full, please contact the billing department to discuss payment plan options. Scheduling additional appointments will be on hold till balances are paid in full or arrangement have been made.</li>
            <li>You will be required to keep a credit card on file to help us collect any unpaid balances. It is your responsibility to keep your credit card information up to date. By completing this form and signing below, you authorize Lifestyle to charge your credit card for any fees or costs due and owing to Lifestyle including, but not limited to, treatment costs not covered or declined by insurance, copays, deductibles, late cancellation fees, interest, and collection costs.</li>
            <li>If you miss more than two appointments without calling or rescheduling, you may be terminated from treatment.</li>
            <li>Clients are responsible for package fees whether or not they use the allotted sessions. If you request a refund on the unused portion of a package, used sessions will be charged at base rates. No refunds on packages are issued after two weeks of services being rendered.</li>
            <li>A late fee of 15% of unpaid balances will be assessed each month.</li>
            <li>We initiate collections after 60 days of non-payment. Should collection proceedings or other legal action become necessary to collect an overdue account, the client/responsible party, understands that Lifestyle Therapy &amp; Coaching has the right to disclose to an outside collection agency all relevant personal account information necessary to collect payment for services rendered. The client/responsible party understands that they are responsible for all costs of collections including, but not limited to, all court costs and attorney&rsquo;s fees. A collection fee will be added to the outstanding balance.</li>
        </ol>

        <p>&nbsp;</p>

        <table>
            <tbody>
                <tr style="border-bottom: 1px solid">
                    <td style="width: 234px">
                        <p><strong>Services</strong></p>
                    </td>
                    <td style="width: 91px">
                        <p><strong>Base Rates</strong></p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>Assessment Session</p>
                    </td>
                    <td style="width: 91px">
                        <p>$220</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>Follow-up Session</p>
                    </td>
                    <td style="width: 91px">
                        <p>$150</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>Extended Session (&lt;30 min)</p>
                    </td>
                    <td style="width: 91px">
                        <p>$75</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>Group Session</p>
                    </td>
                    <td style="width: 91px">
                        <p>$80</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>No Show/Late Cancel</p>
                    </td>
                    <td style="width: 91px">
                        <p>$75</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>Average # of Sessions</p>
                    </td>
                    <td style="width: 91px">
                        <p>8-16</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>Return Check Fee</p>
                    </td>
                    <td style="width: 91px">
                        <p>$35</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>Declined Credit/Debit Card Fee</p>
                    </td>
                    <td style="width: 91px">
                        <p>$35</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>Broken Payment Agreements</p>
                    </td>
                    <td style="width: 91px">
                        <p>$35</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>Tests and Measures</p>
                    </td>
                    <td style="width: 91px">
                        <p>$199</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 234px">
                        <p>Psychosocial Assessment</p>
                    </td>
                    <td style="width: 91px">
                        <p>$350</p>
                    </td>
                </tr>
            </tbody>
        </table>

        <p>For our self-pay clients,<strong> we offer treatment packages </strong>that provide a more personalized and integrated approach to addressing the complex challenges our clients face. Our treatment packages generally are the most economical approach to treatment as they afford our clients an opportunity to access our income-based sliding scale. Insurance covered clients will be required to pay their normal co-pays and deductibles for treatment packages.</p>


        <p><strong>TREATEMENT PACKAGES </strong></p>

        <p>(Number of sessions do not include assessment session.)</p>

        <table border="1" cellpadding="5" cellspacing="0" style="width: 690px; border-style: dotted">
            <tbody>
                <tr>
                    <td style="width: 360px">
                        <p>&nbsp;</p>
                    </td>
                    <td colspan="2" style="width: 81px">
                        <p><strong>Counseling</strong></p>
                    </td>
                    <td colspan="2" style="width: 90px">
                        <p><strong>Chiropractic</strong></p>
                    </td>
                    <td colspan="2" style="width: 90px">
                        <p><strong>Dietetic</strong></p>
                    </td>
                    <td style="width: 69px">
                        <p><strong>Testing Fees</strong></p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 41px">
                        <p>Sess</p>
                    </td>
                    <td style="width: 41px">
                        <p>Grp</p>
                    </td>
                    <td style="width: 45px">
                        <p>Sess</p>
                    </td>
                    <td style="width: 45px">
                        <p>Grp</p>
                    </td>
                    <td style="width: 45px">
                        <p>Sess</p>
                    </td>
                    <td style="width: 45px">
                        <p>Grp</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Basic Therapy Package</p>
                    </td>
                    <td style="width: 41px">
                        <p>4</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Total Life Makeover</p>
                    </td>
                    <td style="width: 41px">
                        <p>12</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>12</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>4</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Chiropractic Rehabilitation Episode Level II</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>12</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Chiropractic Rehabilitation Episode Level I</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>6</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Chiropractic Wellness Program</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>5</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Lifestyle Life Recovery Program (3 month)</p>
                    </td>
                    <td style="width: 41px">
                        <p>12</p>
                    </td>
                    <td style="width: 41px">
                        <p>12*</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Sizzling Hot Marriage Maker-Marital/Premarital (TPrepare-Enrich)</p>
                    </td>
                    <td style="width: 41px">
                        <p>12</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Parent-Child Intervention Program</p>
                    </td>
                    <td style="width: 41px">
                        <p>6</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Lifestyle Anger Management Program T</p>
                    </td>
                    <td style="width: 41px">
                        <p>12</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="height: 14px; width: 360px">
                        <p>Weight Control Program (6 month)</p>
                    </td>
                    <td style="height: 14px; width: 41px">
                        <p>6</p>
                    </td>
                    <td style="height: 14px; width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 14px; width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 14px; width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 14px; width: 45px">
                        <p>6</p>
                    </td>
                    <td style="height: 14px; width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 14px; width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="height: 14px; width: 360px">
                        <p>Nutrition Management &amp; Wellness Program</p>
                    </td>
                    <td style="height: 14px; width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 14px; width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 14px; width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 14px; width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 14px; width: 45px">
                        <p>5</p>
                    </td>
                    <td style="height: 14px; width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 14px; width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Diabetes Management Program (3 month)</p>
                    </td>
                    <td style="width: 41px">
                        <p>3</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>6</p>
                    </td>
                    <td style="width: 45px">
                        <p>3*</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 360px">
                        <p>Heart Disease Management Program (3 month)</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="width: 45px">
                        <p>3</p>
                    </td>
                    <td style="width: 45px">
                        <p>3*</p>
                    </td>
                    <td style="width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td style="height: 19px; width: 360px">
                        <p><strong>Totals</strong></p>
                    </td>
                    <td style="height: 19px; width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 19px; width: 41px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 19px; width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 19px; width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 19px; width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 19px; width: 45px">
                        <p>&nbsp;</p>
                    </td>
                    <td style="height: 19px; width: 69px">
                        <p>&nbsp;</p>
                    </td>
                </tr>
            </tbody>
        </table>

        <p>*Optional T-Additional Testing Required [Additional tests and measurement fees are not included in packages fees.]</p>

    </div>
    <hr />
    <div class="checkbox">
        <asp:CheckBox ID="chk" runat="server" />
        <p style="color: red; font-weight: bold">
            I have read and understood the terms and conditions set out in this agreement.
        </p>
    </div>
    <div class="form-actions">
        <asp:Button ID="cmdGo" runat="server" CssClass="btn btn-primary" Text="Agreed and Continue" OnClick="cmdGo_Click" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

