﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsFacilities
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsFacilities()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsFacilities()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _fsl_id; }
            set
            {
                _fsl_id = value;
                if (clsCMD.Parameters.Contains("@fsl_id"))
                { clsCMD.Parameters["@fsl_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fsl_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _fsl_id;
        public Int64 fsl_id
        {
            get { return _fsl_id; }
            set
            {
                _fsl_id = value;
                if (clsCMD.Parameters.Contains("@fsl_id"))
                { clsCMD.Parameters["@fsl_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fsl_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _fsl_name;
        public String fsl_name
        {
            get { return _fsl_name; }
            set
            {
                _fsl_name = value;
                if (clsCMD.Parameters.Contains("@fsl_name"))
                { clsCMD.Parameters["@fsl_name"].Value = value; }
                else { clsCMD.Parameters.Add("@fsl_name", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _fsl_address;
        public String fsl_address
        {
            get { return _fsl_address; }
            set
            {
                _fsl_address = value;
                if (clsCMD.Parameters.Contains("@fsl_address"))
                { clsCMD.Parameters["@fsl_address"].Value = value; }
                else { clsCMD.Parameters.Add("@fsl_address", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _fsl_city;
        public String fsl_city
        {
            get { return _fsl_city; }
            set
            {
                _fsl_city = value;
                if (clsCMD.Parameters.Contains("@fsl_city"))
                { clsCMD.Parameters["@fsl_city"].Value = value; }
                else { clsCMD.Parameters.Add("@fsl_city", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _fsl_state;
        public String fsl_state
        {
            get { return _fsl_state; }
            set
            {
                _fsl_state = value;
                if (clsCMD.Parameters.Contains("@fsl_state"))
                { clsCMD.Parameters["@fsl_state"].Value = value; }
                else { clsCMD.Parameters.Add("@fsl_state", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _fsl_zip;
        public String fsl_zip
        {
            get { return _fsl_zip; }
            set
            {
                _fsl_zip = value;
                if (clsCMD.Parameters.Contains("@fsl_zip"))
                { clsCMD.Parameters["@fsl_zip"].Value = value; }
                else { clsCMD.Parameters.Add("@fsl_zip", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _fsl_phone;
        public String fsl_phone
        {
            get { return _fsl_phone; }
            set
            {
                _fsl_phone = value;
                if (clsCMD.Parameters.Contains("@fsl_phone"))
                { clsCMD.Parameters["@fsl_phone"].Value = value; }
                else { clsCMD.Parameters.Add("@fsl_phone", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _fsl_fax;
        public String fsl_fax
        {
            get { return _fsl_fax; }
            set
            {
                _fsl_fax = value;
                if (clsCMD.Parameters.Contains("@fsl_fax"))
                { clsCMD.Parameters["@fsl_fax"].Value = value; }
                else { clsCMD.Parameters.Add("@fsl_fax", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Guid _fsl_userid;
        public Guid fsl_userid
        {
            get { return _fsl_userid; }
            set
            {
                _fsl_userid = value;
                if (clsCMD.Parameters.Contains("@fsl_userid"))
                { clsCMD.Parameters["@fsl_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@fsl_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                fsl_id = Convert.ToInt64(dr["fsl_id"]);
                fsl_name = Convert.ToString(dr["fsl_name"]);
                fsl_address = Convert.ToString(dr["fsl_address"]);
                fsl_city = Convert.ToString(dr["fsl_city"]);
                fsl_state = Convert.ToString(dr["fsl_state"]);
                fsl_zip = Convert.ToString(dr["fsl_zip"]);
                fsl_phone = Convert.ToString(dr["fsl_phone"]);
                fsl_fax = Convert.ToString(dr["fsl_fax"]);
                fsl_userid = new Guid(dr["fsl_userid"].ToString());
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            fsl_id = Convert.ToInt64("0");
            fsl_name = Convert.ToString("NA");
            fsl_address = Convert.ToString("NA");
            fsl_city = Convert.ToString("NA");
            fsl_state = Convert.ToString("NA");
            fsl_zip = Convert.ToString("NA");
            fsl_phone = Convert.ToString("NA");
            fsl_fax = Convert.ToString("NA");
            fsl_userid = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.fsl_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "spFacilitiesAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 fsl_id)
        {
            Boolean blnResult = false;
            this.fsl_id = fsl_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "spFacilitiesAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 fsl_id)
        {
            Boolean blnResult = false;
            this.fsl_id = fsl_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "spFacilitiesAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "spFacilitiesGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByMem(Guid userid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.fsl_userid = userid;
                clsCMD.CommandText = "spFacilitiesGetData";
                SetGetSPFlag = "ALLBYMEM";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordForAdmin()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "spFacilitiesGetData";
                SetGetSPFlag = "ALLFORADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsFacilities_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsFacilities_PropertiesList> lstResult = new List<clsFacilities_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].fsl_id = Convert.ToInt64(dr["fsl_id"]);
                lstResult[i].fsl_name = Convert.ToString(dr["fsl_name"]);
                lstResult[i].fsl_address = Convert.ToString(dr["fsl_address"]);
                lstResult[i].fsl_city = Convert.ToString(dr["fsl_city"]);
                lstResult[i].fsl_state = Convert.ToString(dr["fsl_state"]);
                lstResult[i].fsl_zip = Convert.ToString(dr["fsl_zip"]);
                lstResult[i].fsl_phone = Convert.ToString(dr["fsl_phone"]);
                lstResult[i].fsl_fax = Convert.ToString(dr["fsl_fax"]);
                lstResult[i].fsl_userid =new Guid(dr["fsl_userid"].ToString());

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 fsl_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fsl_id = fsl_id;
                clsCMD.CommandText = "spFacilitiesGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 fsl_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fsl_id = fsl_id;
                clsCMD.CommandText = "spFacilitiesGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsFacilities_PropertiesList
    {
        public Int64 fsl_id { get; set; }
        public String fsl_name { get; set; }
        public String fsl_address { get; set; }
        public String fsl_city { get; set; }
        public String fsl_state { get; set; }
        public String fsl_zip { get; set; }
        public String fsl_phone { get; set; }
        public String fsl_fax { get; set; }
        public Guid fsl_userid { get; set; }
    }
}