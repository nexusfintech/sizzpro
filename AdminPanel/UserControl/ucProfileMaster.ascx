﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucProfileMaster.ascx.cs" Inherits="UserControl_ucProfileMaster" %>

<h4>
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
</h4>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3>
                    <i class="icon-user"></i>
                    <asp:Label ID="lblProfileHead" runat="server"></asp:Label>
                </h3>
            </div>
            <div class="box-content nopadding">
                <ul class="tabs tabs-inline tabs-top">
                    <li class='active'>
                        <a href="#profile" data-toggle='tab'><i class="icon-user"></i>Individual Information</a>
                    </li>
                    <li>
                        <a href="#demographic" data-toggle='tab'><i class="glyphicon-notes"></i>Demographic Information </a>
                    </li>
                    <li>
                        <a href="#referral" data-toggle='tab'><i class="glyphicon-parents"></i>Referral Source</a>
                    </li>
                    <li>
                        <a href="#emergency" data-toggle='tab'><i class="glyphicon-parents"></i>Emergency Contact</a>
                    </li>
                    <li>
                        <a href="#secondary" data-toggle='tab'><i class="glyphicon-parents"></i>Secondary Contact </a>
                    </li>
                </ul>
                <div class="tab-content padding tab-content-inline tab-content-bottom">
                    <div class="tab-pane active" id="profile">
                        <div class="form-horizontal">
                            <div class="row-fluid">
                                <div class="span2">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="max-width: 400px; max-height: 400px;">
                                            <asp:Image ID="imgProfilePhoto" runat="server" ImageUrl="~/img/demo/user-2.jpg" Height="200px" Width="200px" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <asp:FileUpload ID="fuprofilephoto" runat="server" />
                                        <div>
                                            <%--<span class="btn btn-file">
                                                <span class="fileupload-new">Select image
                                                </span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" name='imagefile' />
                                            </span>--%>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="span10">
                                    <div class="control-group">
                                        <label for="name" class="control-label right">First Name :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtFirstname" runat="server" data-rule-required="true" CssClass="input-xlarge"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="country" class="control-label right">Middle Name :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtMiddlename" runat="server" data-rule-required="true" CssClass="input-xlarge focused"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="name" class="control-label right">Last Name :</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <asp:TextBox ID="txtLastname" runat="server" data-rule-required="true" CssClass="input-xlarge focused"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="name" class="control-label right">Email :</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <asp:TextBox ID="txtEmail" runat="server" data-rule-required="true" CssClass="input-xlarge focused" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="email" class="control-label right">Date of Birth :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtDOB" runat="server" data-rule-required="true" CssClass="input-medium datepick"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="pw" class="control-label right">SSN :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtSSN" runat="server" data-rule-required="true" CssClass="input-xlarge focused"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="pw" class="control-label right">Intake Date :</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtIntakedate" runat="server" CssClass="input-medium datepick"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="pw" class="control-label right">Indetified Client :</label>
                                        <div class="controls">
                                            <UC:StaticMaster ID="ddlIdentifiedclient" runat="server" DisplayFlag="IDENTIFIEDCLIENT" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="demographic">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label for="asdf" class="control-label">Gender :</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlGender" runat="server" DisplayFlag="GENDER" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="asdf" class="control-label">Ethnicity :</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlEthnicity" runat="server" DisplayFlag="ETHNICITY" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="asdf" class="control-label">Language Preference :</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlLanguage" runat="server" DisplayFlag="LANGUAGE" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="asdf" class="control-label">Marital Status :</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlMaritalStatus" runat="server" DisplayFlag="MARITALSTATUS" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="asdf" class="control-label">Employed ?</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlEmployed" runat="server" DisplayFlag="EMPLOYED" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="asdf" class="control-label">Student ?</label>
                                <div class="controls">
                                    <asp:CheckBox ID="chkStudent" runat="server" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="asdf" class="control-label">Student Status :</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlStudentstatus" runat="server" DisplayFlag="STUDENTSTATUS" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="referral">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">First Name :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtrfsFirstname" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Last Name :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtrfsLastname" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Relationship to client:</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlrfsRelationship" runat="server" DisplayFlag="RELATIONSHIP" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Address same as client ?</label>
                                <div class="controls">
                                    <asp:CheckBox ID="chkrfsAddsameasclnt" runat="server" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Address :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtrfsAdd1" runat="server" CssClass="input-xlarge focused" placeholder="Line-1"></asp:TextBox>
                                    <asp:TextBox ID="txtrfsAdd2" runat="server" CssClass="input-xlarge focused" placeholder="Line-2"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">City :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtrfsCity" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">State :</label>
                                <div class="controls">
                                    <UC:ZipMaster ID="ddlrfsState" runat="server" MasterFlag="STATE" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Zipcode :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtrfsZipcode" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="emergency">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">First Name :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtemrFirstname" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Last Name :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtemrLastname" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Relationship :</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlemrRelation" runat="server" DisplayFlag="RELATIONSHIP" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Phone :</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlemrPhonetype" runat="server" DisplayFlag="PHONETYPE" />
                                    <asp:TextBox ID="txtemrPhone" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Address :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtemrAdd1" runat="server" CssClass="input-xlarge focused" placeholder="Line-1"></asp:TextBox>
                                    <asp:TextBox ID="txtemrAdd2" runat="server" CssClass="input-xlarge focused" placeholder="Line-2"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">City :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtemrCity" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">State :</label>
                                <div class="controls">
                                    <UC:ZipMaster ID="ddlemrState" runat="server" MasterFlag="STATE" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Zipcode :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtemrZipcode" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="secondary">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">First Name :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtscdFirstname" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Last Name :</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtscdLastname" runat="server" CssClass="input-xlarge focused"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Relationship :</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlscdRelationship" runat="server" DisplayFlag="RELATIONSHIP" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Phone :</label>
                                <div class="controls">
                                    <UC:StaticMaster ID="ddlscdPhonetype" runat="server" DisplayFlag="PHONETYPE" />
                                    <asp:TextBox ID="txtscdPhone" runat="server" CssClass="input-xlarge focused" placeholder="Phone Number"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                </div>
               <%-- <div class="modal-footer">
                    <asp:Button ID="cmdSaveandCon" runat="server" CssClass="btn btn-primary" Text="Save and Continue" OnClick="cmdSaveandCon_Click" />
                    <asp:Button ID="cmdReschedule" runat="server" CssClass="btn btn-primary" Text="Re-Assesment" Visible="false" OnClick="cmdReschedule_Click" />
                </div>--%>
            </div>
        </div>
    </div>
</div>
