﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsAppointmentMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }


        public clsAppointmentMaster()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsAppointmentMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _apt_id; }
            set
            {
                _apt_id = value;
                if (clsCMD.Parameters.Contains("@apt_id"))
                { clsCMD.Parameters["@apt_id"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _apt_id;
        public Int64 apt_id
        {
            get { return _apt_id; }
            set
            {
                _apt_id = value;
                if (clsCMD.Parameters.Contains("@apt_id"))
                { clsCMD.Parameters["@apt_id"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _apt_code;
        public String apt_code
        {
            get { return _apt_code; }
            set
            {
                _apt_code = value;
                if (clsCMD.Parameters.Contains("@apt_code"))
                { clsCMD.Parameters["@apt_code"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_code", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _apt_title;
        public String apt_title
        {
            get { return _apt_title; }
            set
            {
                _apt_title = value;
                if (clsCMD.Parameters.Contains("@apt_title"))
                { clsCMD.Parameters["@apt_title"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_title", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _apt_description;
        public String apt_description
        {
            get { return _apt_description; }
            set
            {
                _apt_description = value;
                if (clsCMD.Parameters.Contains("@apt_description"))
                { clsCMD.Parameters["@apt_description"].Size = value.Length; clsCMD.Parameters["@apt_description"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_description", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private DateTime _apt_apointmentdate;
        public DateTime apt_apointmentdate
        {
            get { return _apt_apointmentdate; }
            set
            {
                _apt_apointmentdate = value;
                if (clsCMD.Parameters.Contains("@apt_apointmentdate"))
                { clsCMD.Parameters["@apt_apointmentdate"].Value = value.ToShortDateString(); }
                else { clsCMD.Parameters.Add("@apt_apointmentdate", SqlDbType.Date).Value = value.ToShortDateString(); }
            }
        }
        private DateTime _apt_startdatetime;
        public DateTime apt_startdatetime
        {
            get { return _apt_startdatetime; }
            set
            {
                _apt_startdatetime = value;
                if (clsCMD.Parameters.Contains("@apt_startdatetime"))
                { clsCMD.Parameters["@apt_startdatetime"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_startdatetime", SqlDbType.DateTime).Value = value; }
            }
        }
        private DateTime _apt_enddatetime;
        public DateTime apt_enddatetime
        {
            get { return _apt_enddatetime; }
            set
            {
                _apt_enddatetime = value;
                if (clsCMD.Parameters.Contains("@apt_enddatetime"))
                { clsCMD.Parameters["@apt_enddatetime"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_enddatetime", SqlDbType.DateTime).Value = value; }
            }
        }
        private Guid _apt_memberid;
        public Guid apt_memberid
        {
            get { return _apt_memberid; }
            set
            {
                _apt_memberid = value;
                if (clsCMD.Parameters.Contains("@apt_memberid"))
                { clsCMD.Parameters["@apt_memberid"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_memberid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _apt_userid;
        public Guid apt_userid
        {
            get { return _apt_userid; }
            set
            {
                _apt_userid = value;
                if (clsCMD.Parameters.Contains("@apt_userid"))
                { clsCMD.Parameters["@apt_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _apt_clientid;
        public Guid apt_clientid
        {
            get { return _apt_clientid; }
            set
            {
                _apt_clientid = value;
                if (clsCMD.Parameters.Contains("@apt_clientid"))
                { clsCMD.Parameters["@apt_clientid"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_clientid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _apt_courseid;
        public Int64 apt_courseid
        {
            get { return _apt_courseid; }
            set
            {
                _apt_courseid = value;
                if (clsCMD.Parameters.Contains("@apt_courseid"))
                { clsCMD.Parameters["@apt_courseid"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_courseid", SqlDbType.BigInt).Value = value; }
            }
        }

        private Boolean _apt_iscompleted;
        public Boolean apt_iscompleted
        {
            get { return _apt_iscompleted; }
            set
            {
                _apt_iscompleted = value;
                if (clsCMD.Parameters.Contains("@apt_iscompleted"))
                { clsCMD.Parameters["@apt_iscompleted"].Value = value; }
                else { clsCMD.Parameters.Add("@apt_iscompleted", SqlDbType.Bit).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                apt_id = Convert.ToInt64(dr["apt_id"]);
                apt_code = Convert.ToString(dr["apt_code"]);
                apt_title = Convert.ToString(dr["apt_title"]);
                apt_description = Convert.ToString(dr["apt_description"]);
                apt_apointmentdate = Convert.ToDateTime(dr["apt_apointmentdate"]);
                apt_startdatetime = Convert.ToDateTime(dr["apt_startdatetime"]);
                apt_enddatetime = Convert.ToDateTime(dr["apt_enddatetime"]);
                apt_memberid = new Guid(dr["apt_memberid"].ToString());
                apt_userid = new Guid(dr["apt_userid"].ToString());
                apt_clientid = new Guid(dr["apt_clientid"].ToString());
                apt_courseid = Convert.ToInt64(dr["apt_courseid"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            apt_id = Convert.ToInt64("0");
            apt_code = Convert.ToString("NA");
            apt_title = Convert.ToString("NA");
            apt_description = Convert.ToString("NA");
            apt_apointmentdate = Convert.ToDateTime(DateTime.Now);
            apt_startdatetime = DateTime.Now;
            apt_enddatetime = DateTime.Now;
            apt_memberid = new Guid();
            apt_userid = new Guid();
            apt_clientid = new Guid();
            apt_courseid = Convert.ToInt64("0");

        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.apt_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "AppointmentMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                {
                    blnResult = true; 
                    GetSetErrorMessage = "Entry Saved Successfully.";
                }
                else
                {
                    blnResult = false; 
                    GetSetErrorMessage = "Error in new entry operation.";
                }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(String appoinmentId)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.apt_code = appoinmentId;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "AppointmentMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean UpdateAppoinmentStatus(String apt_code)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.apt_code = apt_code;
            this.AddEditDeleteFlag = "EDITSTS";
            try
            {
                clsCMD.CommandText = "AppointmentMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 apt_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.apt_id = apt_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "AppointmentMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByAPTCode(String apt_code)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.apt_code = apt_code;
            this.AddEditDeleteFlag = "DELETEBYAPTCODE";
            try
            {
                clsCMD.CommandText = "AppointmentMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "AppointmentMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordClientWise()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "AppointmentMasterGetData";
                SetGetSPFlag = "GETCLIENTWISE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordUserWise()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "AppointmentMasterGetData";
                SetGetSPFlag = "GETUSERWISE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllPreviousClientWise()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "AppointmentMasterGetData";
                SetGetSPFlag = "GETCLIENTWISEPREVIOUS";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllPreviousUserWise()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "AppointmentMasterGetData";
                SetGetSPFlag = "GETUSERWISEPREVIOUS";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetTimeWiseAppointment()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "AppointmentMasterGetData";
                SetGetSPFlag = "GETTIMERANGE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsAppointmentMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsAppointmentMaster_PropertiesList> lstResult = new List<clsAppointmentMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].apt_id = Convert.ToInt64(dr["apt_id"]);
                lstResult[i].apt_code = Convert.ToString(dr["apt_code"]);
                lstResult[i].apt_title = Convert.ToString(dr["apt_title"]);
                lstResult[i].apt_description = Convert.ToString(dr["apt_description"]);
                lstResult[i].apt_apointmentdate = Convert.ToDateTime(dr["apt_apointmentdate"]);
                lstResult[i].apt_startdatetime = Convert.ToDateTime(dr["apt_startdatetime"]);
                lstResult[i].apt_enddatetime = Convert.ToDateTime(dr["apt_enddatetime"]);
                lstResult[i].apt_memberid = new Guid(dr["apt_memberid"].ToString());
                lstResult[i].apt_userid = new Guid(dr["apt_userid"].ToString());
                lstResult[i].apt_clientid = new Guid(dr["apt_clientid"].ToString());
                lstResult[i].apt_courseid = Convert.ToInt64(dr["apt_courseid"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(String appoinmentId)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.apt_code = appoinmentId;
                clsCMD.CommandText = "AppointmentMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 apt_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.apt_id = apt_id;
                clsCMD.CommandText = "AppointmentMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion
    }

    public class clsAppointmentMaster_PropertiesList
    {
        public Int64 apt_id { get; set; }
        public String apt_code { get; set; }
        public String apt_title { get; set; }
        public String apt_description { get; set; }
        public DateTime apt_apointmentdate { get; set; }
        public DateTime apt_startdatetime { get; set; }
        public DateTime apt_enddatetime { get; set; }
        public Guid apt_memberid { get; set; }
        public Guid apt_userid { get; set; }
        public Guid apt_clientid { get; set; }
        public Int64 apt_courseid { get; set; }
    }
}