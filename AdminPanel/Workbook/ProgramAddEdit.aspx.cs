﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;
using System.Web.Security;

public partial class Workbook_ProgramAddEdit : System.Web.UI.Page
{
    clsPrograms clsPRG = new clsPrograms();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsMembership mem = new clsMembership();
    clsobjectsharing clsOBS = new clsobjectsharing();

    private void EditEntry()
    {
        Boolean blnResult = clsPRG.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnResult == true)
        {
            txtDescription.Text = clsPRG.prg_description;
            txtProgramName.Text = clsPRG.prg_name;
            txtProgramShort.Text = clsPRG.prg_short;
            ListItem liInterval = ddlSelectMember.Items.FindByValue(clsPRG.prg_UserId.ToString());
            if (liInterval != null)
            {
                ddlSelectMember.SelectedIndex = ddlSelectMember.Items.IndexOf(liInterval);
            }
            prgcd.Value = clsPRG.prg_code.ToString();
            prgsts.Value = clsPRG.prg_sts.ToString();
        }
        else { Response.Redirect("~/Workbook/ProgramMaster.aspx"); }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (MySession.Current.AddEditFlag == "null")
            {
                Response.Redirect("~/Workbook/ProgramMaster.aspx");
            }
            if (User.IsInRole("administrator"))
            {
                BindCombo();
            }
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                EditEntry();
            }
        }
    }

    private void BindCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRole("Member");
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }

    private string PageValidation()
    {
        string strResult = "";
        if (txtProgramName.Text.Trim() == "")
        { strResult += "Please Input Program Name <br/>"; }
        if (txtProgramShort.Text.Trim() == "")
        { strResult += "Please Input Program short <br/>"; }
        return strResult;
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Boolean blnSave = false;
        string strResult = PageValidation();
        if (strResult != "")
        {
            lblmessage.Text = strResult;
            msgdiv.Visible = true;
            return;
        }
        clsPRG.prg_description = txtDescription.Text;
        clsPRG.prg_name = txtProgramName.Text;
        clsPRG.prg_short = txtProgramShort.Text;
        if (User.IsInRole("administrator"))
        {
            clsPRG.prg_UserId = new Guid(ddlSelectMember.SelectedValue.ToString());
        }
        else
        {
            clsPRG.prg_UserId = (Guid)Membership.GetUser().ProviderUserKey;
        }
        clsPRG.prg_OwnerId = mem.SetOwner(clsPRG.prg_UserId);
        if (MySession.Current.AddEditFlag == "ADD")
        {
            clsPRG.prg_code = Guid.NewGuid();
            blnSave = clsPRG.Save();
            if (blnSave == true)
            {
                clsPRG.GetRecordByCodeInProperties(clsPRG.prg_code);
                clsOBS.shr_sharedobjid = clsPRG.prg_id;
                clsOBS.shr_object = "PROGRAMM";
                clsOBS.shr_ownerid = clsPRG.prg_UserId;
                clsOBS.shr_sharedwithid = clsPRG.prg_UserId;
                blnSave = clsOBS.Save();
            }
        }
        else
        {
            clsPRG.prg_code = new Guid(prgcd.Value);
            clsPRG.prg_sts = Convert.ToBoolean(prgsts.Value);
            blnSave = clsPRG.Update(MySession.Current.EditID);
        }
        if (blnSave == true)
        {
            Response.Redirect("~/Workbook/ProgramMaster.aspx");
        }
        else { lblmessage.Text = "Error in save ..." + clsPRG.GetSetErrorMessage; }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Workbook/ProgramMaster.aspx");
    }
}