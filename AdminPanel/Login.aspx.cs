﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    clsContents content = new clsContents();
    clsProfileMaster clsProfile = new clsProfileMaster();
    clsSettings clsSTG = new clsSettings();
    clsUsers clsUSR = new clsUsers();
    clsProfessionMaster clsPROF = new clsProfessionMaster();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsuserproffessionmapping clsUPM = new clsuserproffessionmapping();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void cmdlogin_Click(object sender, EventArgs e)
    {
        var conn = new clsDAL.Conn.clsConnection().strGeneratedError;

        bool isc = new clsDAL.Conn.clsConnection().GetConnection();

        string[] usersInRole = Roles.GetRolesForUser(txtusername.Text.Trim());
        if (usersInRole.Length != 0)
        {
            if (usersInRole[0] == "Client")
            {
                lblmessage.Text = "Client can't login from here..";
                return;
            }
        }
        else
        {
            lblmessage.Text = "Invalid Username or Password";
            return;
        }
        if (Membership.ValidateUser(txtusername.Text.Trim(), txtpassword.Text))
        {
            MembershipUser mu = Membership.GetUser(txtusername.Text.Trim());
            Guid userguid = (Guid)mu.ProviderUserKey;
            MySession.Current.UserGuID = userguid;
            MySession.Current.UserLoginName = txtusername.Text.Trim();
            string[] roleNames = Roles.GetRolesForUser(txtusername.Text.Trim());
            MySession.Current.UserRole = roleNames[0];
            clsUSR.GetRecordByIDInProperties(userguid);
            MySession.Current.UserLoginID = clsUSR.User_Id;
            Boolean blnProfile = clsProfile.GetRecordByMemberKeyInProperties(userguid.ToString());
            if (blnProfile == true)
            {
                if (clsProfile.prm_emailverify == false)
                { FormsAuthentication.SignOut(); Response.Redirect("~/users/UserMessage.aspx?message=activationincomplete&tokenid=" + clsProfile.prm_tokenid); return; }
            }
            else
            {
                Response.Redirect("~/users/UserMessage.aspx?message=activationincomplete&tokenid=" + clsProfile.prm_tokenid);
                return;
            }
            // Success, create non-persistent authentication cookie.
            Boolean blnSession = clsSTG.GetRecordByFLAGInProperties("WEBSESSION");
            double dblSession = 20;
            if (blnSession == true)
            { dblSession = (double)clsSTG.stg_intvalue; }
            Session.Timeout = Convert.ToInt16(dblSession);
            FormsAuthentication.SetAuthCookie(
                    this.txtusername.Text.Trim(), false);
            FormsAuthenticationTicket ticket1 =
               new FormsAuthenticationTicket(
                    1,                                   // version
                    this.txtusername.Text.Trim(),   // get username  from the form
                    DateTime.Now,                        // issue time is now
                    DateTime.Now.AddMinutes(dblSession),         // expires in 10 minutes
                    false,      // cookie is not persistent
                    usersInRole[0] // role assignment is stored
                // in userData
                    );
            HttpCookie cookie1 = new HttpCookie(
              FormsAuthentication.FormsCookieName,
              FormsAuthentication.Encrypt(ticket1));
            Response.Cookies.Add(cookie1);
            Session.Timeout = Convert.ToInt32(dblSession);

            //get profession alies session value
            Boolean blnStg = clsSTG.GetRecordByFlagUserInProperties("PROFESSION", userguid);
            if (blnStg == true)
            {
                Boolean blnProf = clsUPM.GetRecordByUserIdInProperties(userguid);
                if (blnProf == true)
                {
                    MySession.Current.MemberAlias = clsUPM.prfm_memberalias;
                    MySession.Current.UserAlias = clsUPM.prfm_useralias;
                    MySession.Current.ClientAlias = clsUPM.prfm_clientalias;
                }
            }
            if (clsProfile.GetRecordByMemberKeyInProperties(userguid.ToString()))
            {
                MySession.Current.UserLoginFullname = clsProfile.prm_firstname + " " + clsProfile.prm_lastname;
            }
            else { MySession.Current.UserLoginFullname = "Update Profile"; }
            //if (usersInRole[0].ToLower() == "member")
            //{
            //    MySession.Current.MemberID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            //}
            //else {
            //    if (clsUSR.GetRecordByIDInProperties(userguid))
            //    {
            //        if (clsCPM.GetRecordByParentIDInProperties(clsUSR.User_Id))
            //        {
            //        }
            //        else { return; }
            //    }
            //    else { return; }
            //}

            // 4. Do the redirect. 
            if (roleNames[0] == "administrator")
            {
                this.MasterPageFile = "MasterPageUM.master";
            }
            String returnUrl1;
            // the login is successful
            if (Request.QueryString["ReturnUrl"] == null)
            {
                Response.Redirect("~/Admin/Home.aspx");
            }
            //login not unsuccessful 
            else
            {
                returnUrl1 = Request.QueryString["ReturnUrl"];
                Response.Redirect(returnUrl1);
            }
        }
        else { lblmessage.Visible = true; lblmessage.Text = "Invalid Username or Password"; txtpassword.Focus(); }
    }
}