﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsReferringprovidersMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsReferringprovidersMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsReferringprovidersMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _rp_id; }
            set
            {
                _rp_id = value;
                if (clsCMD.Parameters.Contains("@rp_id"))
                { clsCMD.Parameters["@rp_id"].Value = value; }
                else { clsCMD.Parameters.Add("@rp_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _rp_id;
        public Int64 rp_id
        {
            get { return _rp_id; }
            set
            {
                _rp_id = value;
                if (clsCMD.Parameters.Contains("@rp_id"))
                { clsCMD.Parameters["@rp_id"].Value = value; }
                else { clsCMD.Parameters.Add("@rp_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _rp_userid;
        public Guid rp_userid
        {
            get { return _rp_userid; }
            set
            {
                _rp_userid = value;
                if (clsCMD.Parameters.Contains("@rp_userid"))
                { clsCMD.Parameters["@rp_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@rp_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _rp_name;
        public String rp_name
        {
            get { return _rp_name; }
            set
            {
                _rp_name = value;
                if (clsCMD.Parameters.Contains("@rp_name"))
                { clsCMD.Parameters["@rp_name"].Value = value; }
                else { clsCMD.Parameters.Add("@rp_name", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _rp_address;
        public String rp_address
        {
            get { return _rp_address; }
            set
            {
                _rp_address = value;
                if (clsCMD.Parameters.Contains("@rp_address"))
                { clsCMD.Parameters["@rp_address"].Value = value; }
                else { clsCMD.Parameters.Add("@rp_address", SqlDbType.NVarChar, 300).Value = value; }
            }
        }
        private String _rp_contact;
        public String rp_contact
        {
            get { return _rp_contact; }
            set
            {
                _rp_contact = value;
                if (clsCMD.Parameters.Contains("@rp_contact"))
                { clsCMD.Parameters["@rp_contact"].Value = value; }
                else { clsCMD.Parameters.Add("@rp_contact", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _rp_fax;
        public String rp_fax
        {
            get { return _rp_fax; }
            set
            {
                _rp_fax = value;
                if (clsCMD.Parameters.Contains("@rp_fax"))
                { clsCMD.Parameters["@rp_fax"].Value = value; }
                else { clsCMD.Parameters.Add("@rp_fax", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _rp_email;
        public String rp_email
        {
            get { return _rp_email; }
            set
            {
                _rp_email = value;
                if (clsCMD.Parameters.Contains("@rp_email"))
                { clsCMD.Parameters["@rp_email"].Value = value; }
                else { clsCMD.Parameters.Add("@rp_email", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                rp_id = Convert.ToInt64(dr["rp_id"]);
                rp_userid = new Guid(dr["rp_userid"].ToString());
                rp_name = Convert.ToString(dr["rp_name"]);
                rp_address = Convert.ToString(dr["rp_address"]);
                rp_contact = Convert.ToString(dr["rp_contact"]);
                rp_fax = Convert.ToString(dr["rp_fax"]);
                rp_email = Convert.ToString(dr["rp_email"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            rp_id = Convert.ToInt64("0");
            rp_userid = new Guid();
            rp_name = Convert.ToString("NA");
            rp_address = Convert.ToString("NA");
            rp_contact = Convert.ToString("NA");
            rp_fax = Convert.ToString("NA");
            rp_email = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.rp_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_ReferringprovidersMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 rp_id)
        {
            Boolean blnResult = false;
            this.rp_id = rp_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_ReferringprovidersMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 rp_id)
        {
            Boolean blnResult = false;
            this.rp_id = rp_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_ReferringprovidersMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_ReferringprovidersMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByAdmin(Guid userid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_ReferringprovidersMasterGetData";
                rp_userid = userid;
                SetGetSPFlag = "ALLADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordForSA()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_ReferringprovidersMasterGetData";
                SetGetSPFlag = "ALLSA";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsReferringprovidersMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsReferringprovidersMaster_PropertiesList> lstResult = new List<clsReferringprovidersMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].rp_id = Convert.ToInt64(dr["rp_id"]);
                lstResult[i].rp_userid = new Guid(dr["rp_userid"].ToString());
                lstResult[i].rp_name = Convert.ToString(dr["rp_name"]);
                lstResult[i].rp_address = Convert.ToString(dr["rp_address"]);
                lstResult[i].rp_contact = Convert.ToString(dr["rp_contact"]);
                lstResult[i].rp_fax = Convert.ToString(dr["rp_fax"]);
                lstResult[i].rp_email = Convert.ToString(dr["rp_email"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 rp_id)
        {
            Boolean blnResult = false;
            try
            {
                this.rp_id = rp_id;
                clsCMD.CommandText = "sp_ReferringprovidersMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 rp_id)
        {
            Boolean blnResult = false;
            try
            {
                this.rp_id = rp_id;
                clsCMD.CommandText = "sp_ReferringprovidersMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsReferringprovidersMaster_PropertiesList
    {
        public Int64 rp_id { get; set; }
        public Guid rp_userid { get; set; }
        public String rp_name { get; set; }
        public String rp_address { get; set; }
        public String rp_contact { get; set; }
        public String rp_fax { get; set; }
        public String rp_email { get; set; }
    }
}