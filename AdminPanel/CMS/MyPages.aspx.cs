﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMS_MyPages : System.Web.UI.Page
{
    clsContents clscntn = new clsContents();

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            bindpages();
        }
    }

    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/CMS/AddEditPage.aspx");
    }

    private void bindpages()
    {
        DataTable dtRecords=clscntn.GetAllRecord();
        dtPages.DataSource = dtRecords;
        dtPages.DataBind();
    }
    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        LinkButton btnManage = (LinkButton)sender;
        MySession.Current.AddEditFlag = "EDIT";
        MySession.Current.EditID =Convert.ToInt64(btnManage.CommandArgument);
        Response.Redirect("~/CMS/AddEditPage.aspx");
    }
}