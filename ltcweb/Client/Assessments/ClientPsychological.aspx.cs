﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class Clients_Assessment_ClientPsychological : System.Web.UI.Page
{
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();
    protected void Page_Load(object sender, EventArgs e)
    {
        Boolean isRcrdFound = clsASR.GetRecordByStepID(Convert.ToInt64(AssesmentStep.Value), (Guid)Membership.GetUser().ProviderUserKey);
        if (!isRcrdFound)
        {
            ucpsychological.Memberkey = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        }
        else
        {
            nextstep();
        }
    }
    protected void cmdNext_Click(object sender, EventArgs e)
    {
        nextstep();
    }
    public void nextstep()
    {
        Response.Redirect("~/Client/Assessments/ClientAddiction.aspx");
    }
}