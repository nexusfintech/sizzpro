﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class Users_UserFamilyOrigion : System.Web.UI.Page
{
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();
    protected void Page_Load(object sender, EventArgs e)
    {
        Boolean isRcrdFound = clsASR.GetRecordByStepID(Convert.ToInt64(AssesmentStep.Value), (Guid)Membership.GetUser().ProviderUserKey);
        if (!isRcrdFound)
        {
            ucFather.Memberkey = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            ucMatFather.Memberkey = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            ucMatMother.Memberkey = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            ucMother.Memberkey = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            ucPatFather.Memberkey = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            ucPatMother.Memberkey = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        }
        else
        {
            nextstep();
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Boolean blnResult = false;
        blnResult = ucFather.Save();
        blnResult = ucMother.Save();
        blnResult = ucMatFather.Save();
        blnResult = ucMatMother.Save();
        blnResult = ucPatFather.Save();
        blnResult = ucPatMother.Save();
        if (blnResult == true)
        {
            lblMessage.Text = "Save Successfully.";
        }
    }
        
    protected void cmdNext_Click(object sender, EventArgs e)
    {
        clsASR.storcrd_stepid = Convert.ToInt32(AssesmentStep.Value);
        clsASR.stprcrd_userid = (Guid)Membership.GetUser().ProviderUserKey;
        Boolean stprslt = clsASR.Save();
        if(stprslt)
        {
            nextstep();
        }
        else
        {
            Response.Redirect("~/Client/Assessments/UserFamilyOrigion.aspx");
            Label1.Text = "your step is not completed please click here again";
        }
    }

    public void nextstep()
    {
        Response.Redirect("~/Client/Assessments/Spousesandsignificant.aspx");
    }
}