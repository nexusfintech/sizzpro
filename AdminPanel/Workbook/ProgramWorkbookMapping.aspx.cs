﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;

public partial class Workbook_ProgramWorkbookMapping : System.Web.UI.Page
{
    clsPrograms clsPRG = new clsPrograms();
    clsProgramToWorkbook clsPTW = new clsProgramToWorkbook();
    clsClientParentMapping clsCPM = new clsClientParentMapping();

    private void BindCombo(Guid UserId)
    {
        ddlPrograms.Items.Clear();
        ddlPrograms.SelectedIndex = -1;
        ddlPrograms.SelectedValue = null;
        ddlPrograms.ClearSelection();
        ddlPrograms.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlPrograms.Items.Add(lst);
        ddlPrograms.DataSource = clsPRG.GetAllRecordByMember(UserId);
        ddlPrograms.DataValueField = "prg_id";
        ddlPrograms.DataTextField = "prg_name";
        ddlPrograms.DataBind();
    }

    private void BindMemberCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRole("Member");
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }

    private void BindListView(Int64 intProgID, Guid UserID)
    {
        DataTable dtleft = clsPTW.GetAllByProgramIDNotMapped(intProgID, UserID);
        DataTable dtright = clsPTW.GetAllByProgramID(intProgID, UserID);

        RptrLeft.DataSource = dtleft;
        RptrLeft.DataBind();

        RptrRight.DataSource = dtright;
        RptrRight.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (User.IsInRole("administrator"))
            {
                BindMemberCombo();
            }
            else
            {
                BindCombo((Guid)Membership.GetUser().ProviderUserKey);
            }
        }
    }

    protected void cmdSelect_Click(object sender, EventArgs e)
    {
        Session["prgid"] = ddlPrograms.SelectedValue.ToString();
        hdcomboid.Value = ddlPrograms.SelectedValue.ToString();
        Int64 intPRGID = Convert.ToInt64(Session["prgid"]);
        lblBoxHeader.Text = "( " + ddlPrograms.SelectedItem.Text + " ) Program Selected";
        pnlMapping.Visible = true;
        if (User.IsInRole("administrator"))
        {
            BindListView(intPRGID, new Guid(ddlSelectMember.SelectedValue.ToString()));
        }
        else
        {
            BindListView(intPRGID, (Guid)Membership.GetUser().ProviderUserKey);
        }
    }

    protected void ddlSelectMember_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCombo(new Guid(ddlSelectMember.SelectedValue.ToString()));
    }
}