﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Workbook_AssignmentSharing : System.Web.UI.Page
{
    clsUserspermission clsUPM = new clsUserspermission();
    clsAssignments clsasn = new clsAssignments();
    clsobjectsharing clsOBS = new clsobjectsharing();
    private Guid UserId { get; set; }
    private Boolean blnGetResult { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (MySession.Current.EditID != 0)
        {
            blnGetResult = clsasn.GetRecordByIDInProperties(MySession.Current.EditID);
            if (!IsPostBack)
            {
                if (blnGetResult)
                {
                    this.UserId = clsasn.asn_UserId;
                    BindAdmin(MySession.Current.EditID);
                    BindData();
                }
                else
                {
                    Response.Redirect("~/Workbook/AssignmentMaster.aspx");         
                }
            }

        }
        else
        {
            Response.Redirect("~/Workbook/AssignmentMaster.aspx");
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        if (ddladmins.SelectedIndex != 0)
        {
            clsOBS.shr_sharedwithid = new Guid(ddladmins.SelectedValue.ToString());
            if (DropDownList1.SelectedIndex == 0)
            {
                shareassignment(MySession.Current.EditID);
            }
            else
            { 
                // code for paid asharing
            }
        }
        else
        {
            lblmessage.Text = "Please Select Any User";
        }
    }
    public void BindAdmin(Int64 WrkId)
    {
        clsUPM.upm_userid = UserId;
        clsUPM.SetGetSPFlag = "USERLISTFORAS";
        DataTable dtSrc = clsUPM.GetAdminList(WrkId);
        if (dtSrc.Rows.Count > 0)
        {
            ddladmins.Items.Clear();
            ddladmins.SelectedIndex = -1;
            ddladmins.SelectedValue = null;
            ddladmins.ClearSelection();
            ddladmins.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddladmins.Items.Add(lst);
            ddladmins.DataValueField = "Userid";
            ddladmins.DataTextField = "UserName";
            ddladmins.DataSource = dtSrc;
            ddladmins.DataBind();
        }
        else
        {
            ddladmins.Items.Clear();
            ddladmins.SelectedIndex = -1;
            ddladmins.SelectedValue = null;
            ddladmins.ClearSelection();
            ddladmins.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<- No Record Found ->>";
            lst.Selected = true;
            ddladmins.Items.Add(lst);
        }
    }
    public void BindData()
    {
        clsOBS.shr_ownerid = UserId;
        clsOBS.shr_sharedobjid = MySession.Current.EditID;
        clsOBS.SetGetSPFlag = "ASSIGNMENT";
        DataTable dtRslt = clsOBS.GetRecords();
        dtSharing.DataSource = dtRslt;
        dtSharing.DataBind();
    }

    public void shareassignment(Int64 AsnId)
    {
        Boolean blnCRslt = blnGetResult;
        if (blnCRslt)
        {
            clsOBS.shr_object = "ASSIGNMENT";
            clsOBS.shr_sharedobjid = AsnId;
            clsOBS.shr_ownerid = clsasn.asn_UserId;
            clsOBS.Save();
            Response.Redirect("~/Workbook/AssignmentSharing.aspx");
        }
        else
        {
            Response.Redirect("~/Workbook/AssignmentMaster.aspx");
        }
    }
}