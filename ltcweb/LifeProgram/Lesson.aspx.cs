﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class LifeProgram_Lesson : System.Web.UI.Page
{
    clsChapters clsCHP = new clsChapters();
    clsChapterToLession clsCTL = new clsChapterToLession();

    public string GetImageName(string strStatus, string strPOSID)
    {
        string strResult = "";
        if (strStatus == "0")
        {
            Int64 intCHPID = Convert.ToInt64(Session["lessonid"]);
            Guid usrGUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            Int64 intPrvStatus = clsCTL.GetPreviousLessonStatus(Convert.ToInt64(strPOSID), intCHPID, usrGUID);
            if (intPrvStatus == 2)
            {
                strResult = "unlock.png";
            }
            else {
                strResult = "lock.png";
                strStatus = intPrvStatus.ToString();
            }
        }
        if (strStatus == "1")
        { strResult = "unlock.png"; }
        if (strStatus == "2")
        { strResult = "status.png"; }
        return strResult;
    }

    public string GetLinkImageName(string strStatus, string strPOSID)
    {
        string strResult = "";
        if (strStatus == "0")
        {
            Int64 intCHPID = Convert.ToInt64(Session["lessonid"]);
            Guid usrGUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            Int64 intPrvStatus = clsCTL.GetPreviousLessonStatus(Convert.ToInt64(strPOSID), intCHPID, usrGUID);
            if (intPrvStatus == 2)
            {
                strResult = "Go.png";
            }
            else
            {
                strResult = "Go_Black.png";
                strStatus = intPrvStatus.ToString();
            }
        }
        if (strStatus == "1")
        { strResult = "Go.png"; }
        if (strStatus == "2")
        { strResult = "Go.png"; }
        return strResult;
    }

    public string GetLinkStatus(string strStatus, string strPOSID)
    {
        string strResult = "";
        if (strStatus == "0")
        {
            Int64 intCHPID = Convert.ToInt64(Session["lessonid"]);
            Guid usrGUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            Int64 intPrvStatus = clsCTL.GetPreviousLessonStatus(Convert.ToInt64(strPOSID), intCHPID, usrGUID);
            if (intPrvStatus == 2)
            {
                strResult = "view";
            }
            else
            {
                strResult = "";
                strStatus = intPrvStatus.ToString();
            }
        }
        if (strStatus == "1")
        { strResult = "view"; }
        if (strStatus == "2")
        { strResult = "view"; }
        return strResult;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (hidden.Value != "")
        {
            Session["assignmentid"] = hidden.Value;
            Response.Redirect("~/LifeProgram/Assignment.aspx");
            return;
        }
        if (!IsPostBack)
        {
            _GridFill();
        }
    }

    void _GridFill()
    {
        string strusername = User.Identity.Name;
        int intlid = Convert.ToInt32(Session["lessonid"]);
        if (intlid == 0)
        {
            Response.Redirect("~/Client/DashBoard.aspx");
        }

        rptChapter.DataSource = clsCHP.GetByPrimaryKeyID(intlid);
        rptChapter.DataBind();

        Guid usrGUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        clsCTL.ctl_userid = usrGUID;
        rptLessons.DataSource = clsCTL.GetLessionChapterWise(intlid);
        rptLessons.DataBind();

        //RTtable.DataSource = clsworkbook.lessonTitle(intlid);
        //RT.DataSource = clsworkbook.lessonDetails(intlid);
        //RTLink.DataSource = clsworkbook.lessonstatus(intlid, strusername);
        //RTLink.DataBind();
        //RTtable.DataBind();
        //RT.DataBind();
    }
}