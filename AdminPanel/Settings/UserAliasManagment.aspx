﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="UserAliasManagment.aspx.cs" Inherits="Settings_UserAliasManagment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="glyphicon-settings"></i>User Alias Settings</h3>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <% if (HttpContext.Current.User.IsInRole("administrator"))
                           { %>
                        <div class="control-group">
                            <label for="txtStartTime" class="control-label">Member Alias</label>
                            <div class="controls">
                                <asp:TextBox ID="txtMemberAlias" runat="server" CssClass="input-medium"></asp:TextBox>
                            </div>
                        </div>
                        <%} %>
                        <div class="control-group">
                            <label for="txtEndTime" class="control-label">User Alias</label>
                            <div class="controls">
                                <asp:TextBox ID="txtUserAlias" runat="server" CssClass="input-medium"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="ddlProfession" class="control-label">Client Alias</label>
                            <div class="controls">
                                <asp:TextBox ID="txtClientAlias" runat="server" CssClass="input-medium"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

