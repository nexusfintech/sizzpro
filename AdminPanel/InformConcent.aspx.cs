﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Data;
using System.IO;

public partial class InfrormConcent : System.Web.UI.Page
{
    clsClientInformconcent clsCIC = new clsClientInformconcent();
    clsICdetails icPathDetail = new clsICdetails();
    clsAgreementServiceMapping clsASM = new clsAgreementServiceMapping();
    clsAgreements clsAGR = new clsAgreements();
    clsAgreemetFile clsAGF = new clsAgreemetFile();

    protected void Page_Load(object sender, EventArgs e)    
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["mode"] != null)
            {
                string mode = Request.QueryString["mode"].ToString();
                if (mode.ToLower() == "download")
                {
                    DownloadConcentFile();
                }
                else if (mode.ToLower() == "create")
                {
                    GenerateConcentFile();
                }
            }
        }
    }

    private void GenerateConcentFile()
    {
        // sent from user site in querystring

        Guid ClientId=new Guid(Request.QueryString["c"]);
        Int64 serviceid = Convert.ToInt64(Request.QueryString["s"]);

        bool blnResult = clsASM.GetRecordByServiceinProperties(serviceid);
        if (blnResult == true)
        {
            bool blnRsltAgr = clsAGR.GetRecordByIDInProperties(serviceid);
            if (blnRsltAgr == true)
            {
                DataTable dt = clsCIC.GetRecordByClient(ClientId);
                string Html = clsAGR.agr_content;
                //string filepath = ("Document\\" + clsAGR.agr_id.ToString() + ClientId + ".pdf").ToString();
                //filepath = Request.PhysicalApplicationPath + filepath;
                string filepath = Server.MapPath("~/Document/") + clsAGR.agr_id.ToString() + ClientId + ".pdf";
                //replace place holder with value

                foreach (DataRow dr in dt.Rows)
                {
                    string flag = "{" + dr["cic_valueflag"].ToString() + "}";

                    if (flag.Contains("corspondenceby"))
                    {
                        string[] values = dr["cic_value"].ToString().Split('@');
                        string value = string.Empty;
                        foreach (string h in values)
                        {
                            value = value + h + "<br/>";
                        }
                        Html = Html.Replace(flag, value);
                    }
                    else
                    {
                        string value = dr["cic_value"].ToString();
                        Html = Html.Replace(flag, value);
                    }
                }

                bool isfilecreted = HtmlToPdf(Html, filepath);
                if (isfilecreted == true)
                {
                    clsAGF.af_agrid = clsAGR.agr_id;
                    clsAGF.af_userid = ClientId;
                    clsAGF.af_filepath = filepath;
                    bool issave = clsAGF.Save();
                    if (issave == true)
                    {
                        Response.StatusCode = 1;
                    }
                    else
                    {
                        Response.StatusCode = 2;
                    }
                }
                else
                {
                    Response.StatusCode = 3;
                }
            }
        }
    }

    private Boolean HtmlToPdf(string html, string Filepath)
    {
        bool iscreated = false;
        //creation of pdf file
        Document document = new Document(PageSize.A4, 30, 30, 30, 30);
        PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Filepath, FileMode.Create));
        document.Open();
        iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
        HTMLWorker hw = new HTMLWorker(document);
        StringReader sr = new StringReader(html);
        XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
        document.Close();
        // update creted status
        iscreated = true;
        return iscreated;
    }

    private void DownloadConcentFile()
    {
        Guid ClientId=new Guid(Request.QueryString["c"].ToString());

        string filepath = Request.PhysicalApplicationPath + "document\\" + "IC_" + ClientId + ".pdf";
        if (File.Exists(filepath))
        {
            Response.StatusCode = 1;
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment;filename=" + "IC_" + ClientId + ".pdf");
            Response.TransmitFile(filepath);
        }
        else
        {
            Response.StatusCode = 2;
        }
    }
}