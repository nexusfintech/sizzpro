﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
using System.Data;

public partial class Users_UserRegistration : System.Web.UI.Page
{
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsEmailAccounts clsMailAccount = new clsEmailAccounts();
    clsProfileMaster clsProfile = new clsProfileMaster();
    clsEmailTemplate clsMailTemplate = new clsEmailTemplate();
    clsSendMail clsMailSend = new clsSendMail();

    private void BindCombo()
    {
        ddlSelectUser.Items.Clear();
        ddlSelectUser.SelectedIndex = -1;
        ddlSelectUser.SelectedValue = null;
        ddlSelectUser.ClearSelection();
        ddlSelectUser.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectUser.Items.Add(lst);
        ddlSelectUser.DataValueField = "Userid";
        ddlSelectUser.DataTextField = "UserName";
        DataTable dtResult = new DataTable();
        dtResult = clsCPM.GetAllComboByRole("Member");
        ddlSelectUser.DataSource = dtResult;
        ddlSelectUser.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lnkManagement.NavigateUrl = "~/Users/UserClientManage.aspx";
        if (!Page.IsPostBack)
        {
            if (User.IsInRole("administratork"))
            {
                BindCombo();
            }
        }
    }

    protected void cmdCreateUser_Click(object sender, EventArgs e)
    {
        if (txtFirstname.Text.Trim() == "")
        {
            lblMessage.Text = "Please Enter Firstname";
            return;
        }
        //if (txtMiddleName.Text.Trim() == "")
        //{
        //    lblMessage.Text = "Please Enter Middlename";
        //    return;
        //}
        if (txtlastname.Text.Trim() == "")
        {
            lblMessage.Text = "Please Enter Lastname";
            return;
        }
        if (txtEmail.Text.Trim() == "")
        {
            lblMessage.Text = "Please Enter Email Address";
            return;
        }

        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var stringChars = new char[8];
        var random = new Random();
        for (int i = 0; i < stringChars.Length; i++)
        {
            stringChars[i] = chars[random.Next(chars.Length)];
        }
        string password = new String(stringChars);

        MembershipCreateStatus memstatus;
        MembershipUser memuser = Membership.CreateUser(txtEmail.Text.Trim(), password.Trim().ToUpper().ToString(), txtEmail.Text.Trim(), "admin@sizzpro", "nexus@admin", true, out memstatus);
        if (memuser == null)
        {
            lblMessage.Text = GetErrorMessage(memstatus);
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        else
        {
            String[] strRoles = new String[1];
            strRoles[0] = "Client";
            Roles.AddUserToRoles(txtEmail.Text.Trim(), strRoles);
            MembershipUser mu = Membership.GetUser(txtEmail.Text.Trim());
            Guid userguid = (Guid)mu.ProviderUserKey;
            Boolean blnTemplate = clsMailTemplate.GetRecordByNameInProperties("UserCreateWelcomeMail");
            if (blnTemplate == true)
            {
                Boolean blnMailAccount = clsMailAccount.GetRecordByIDInProperties(clsMailTemplate.emt_mailaccountid);
                if (blnMailAccount == true)
                {
                    Boolean blnAccountBind = clsMailSend.BindEmailAccount(clsMailAccount.ema_emailaddress);
                    if (blnAccountBind == true)
                    {
                        String strGUID = Guid.NewGuid().ToString();
                        String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                        String strhost = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                        string strlink = strhost + "Users/UserMessage.aspx?message=activation&tokenid=" + strGUID;
                        clsMailSend.ToMailAddress = txtEmail.Text.Trim();
                        clsMailSend.Subject = clsMailTemplate.emt_templatesubject;
                        if (clsMailTemplate.emt_ccmail.Trim() != "") { clsMailSend.CCMailAddress = clsMailTemplate.emt_ccmail.Trim(); }
                        if (clsMailTemplate.emt_bccmail.Trim() != "") { clsMailSend.BCCMailAddress = clsMailTemplate.emt_bccmail.Trim(); }
                        String strHtmlBody = clsMailTemplate.emt_templatebody.Trim();
                        strHtmlBody = strHtmlBody.Replace("{username}", txtEmail.Text.Trim());
                        strHtmlBody = strHtmlBody.Replace("{password}", password.ToUpper());
                        strHtmlBody = strHtmlBody.Replace("{activationlink}", strlink);
                        clsMailSend.HtmlBody = strHtmlBody;
                        clsMailSend.Priority = System.Net.Mail.MailPriority.Normal;
                        Boolean blnSendMail = clsMailSend.SendMail();
                        if (blnSendMail == true)
                        {
                            clsUsers clsUsr = new clsUsers();
                            Boolean blnUsr = clsUsr.GetRecordByIDInProperties(userguid);
                            if (blnUsr == true)
                            {
                                clsProfile.prm_firstname = txtFirstname.Text;
                                //clsProfile.prm_middlename = txtMiddleName.Text;
                                clsProfile.prm_lastname = txtlastname.Text;
                                clsProfile.prm_user_Id = clsUsr.User_Id;
                                clsProfile.prm_emailid = txtEmail.Text.Trim();
                                clsProfile.prm_passwordreset = true;
                                clsProfile.prm_userId = userguid.ToString();
                                clsProfile.prm_emailverify = false;
                                clsProfile.prm_passwordreset = true;
                                clsProfile.prm_userrole = "Client";
                                clsProfile.prm_tokenid = strGUID;
                                clsProfile.prm_tokendatetime = DateTime.Now;
                                Boolean blnProfile = clsProfile.Save();

                                AddUserParentMapping(userguid);

                                if (blnProfile == true)
                                { lblMessage.Text = "Client created successfully."; Response.Redirect("UserClientManage.aspx"); }
                                else { lblMessage.Text = "Error in user profile creation."; }
                            }
                            else { lblMessage.Text = "Error in getting user primary key."; }
                        }
                        else { lblMessage.Text = "Error in sending welcome mail."; }
                    }
                    else { lblMessage.Text = "Error in Mail Account's Setting Bind To Mail Sender Engine"; }
                }
                else { lblMessage.Text = "Invalid Mail Account Binding In Email Template"; }
            }
            else { lblMessage.Text = "User Create Successfully. but error in Welcome Mail Sending.. please resend manualy"; }
        }
    }

    private void AddUserParentMapping(Guid guUserid)
    {
        clsClientParentMapping clsMap = new clsClientParentMapping();
        clsUsers clsUsr = new clsUsers();

        if (User.IsInRole("administrator"))
        {
            clsMap.cpm_parentid = new Guid(ddlSelectUser.SelectedItem.Value);
        }
        else if (User.IsInRole("Member"))
        {
            clsMap.cpm_parentid = (Guid)Membership.GetUser().ProviderUserKey;
        }
        else if (User.IsInRole("User"))
        {
            clsMap.cpm_parentid = (Guid)Membership.GetUser().ProviderUserKey;
        }
        clsMap.cpm_userid = guUserid;
        clsMap.Save();
    }

    public string GetErrorMessage(MembershipCreateStatus status)
    {
        switch (status)
        {
            case MembershipCreateStatus.DuplicateUserName:
                return "Username already exists. Please enter a different user name.";

            case MembershipCreateStatus.DuplicateEmail:
                return "A username for that e-mail address already exists. Please enter a different e-mail address.";

            case MembershipCreateStatus.InvalidPassword:
                return "The password provided is invalid. Please enter a valid password value.";

            case MembershipCreateStatus.InvalidEmail:
                return "The e-mail address provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidAnswer:
                return "The password retrieval answer provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidQuestion:
                return "The password retrieval question provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidUserName:
                return "The user name provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.ProviderError:
                return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            case MembershipCreateStatus.UserRejected:
                return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            default:
                return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
        }
    }
}