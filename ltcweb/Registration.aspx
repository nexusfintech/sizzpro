﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <script type="text/javascript">
        function GetUserInfo(data) {
            alert("call");
            var clntdetail;
            clntdetail = data.host + "@" + data.countryName + "@" + data.city + "@" + data.region + "@" + data.latitude + "@" + data.longitude + "@" + data.timezone + "@" + data.lang;
            alert(clntdetail);
            document.getElementById('<%= hdclient.ClientID %>').value = clntdetail;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="glyphicon-user_add"></i>&nbsp;New User Register</h2>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <div class="box">
                <div class="box-title">
                    <h3>Registration Detail
                    </h3>
                </div>
                <div class="box-content">
                    <div class='form-horizontal '>
                        <div class="control-group">
                            <label for="txtFirstName" class="control-label">First Name*</label>
                            <div class="controls">
                                <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtLastName" class="control-label">Last Name*</label>
                            <div class="controls">
                                <asp:TextBox ID="txtLastName" runat="server" placeholder="Last Name" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtPassword" class="control-label">Password*</label>
                            <div class="controls">
                                <asp:TextBox ID="txtPassword" runat="server" placeholder="********" TextMode="Password" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtEmail" class="control-label">Email*</label>
                            <div class="controls">
                                <asp:TextBox ID="txtEmail" runat="server" placeholder="Email Address" class="input-xlarge" data-rule-email="true" data-rule-required="true"></asp:TextBox>
                                <span class="help-block">Email address will be consider as a username </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtDOB" class="control-label">Date of Birth*</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDOB" runat="server" data-rule-required="true" CssClass="input-medium datepick"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="txtAddress" class="control-label">Address*</label>
                            <div class="controls">
                                <asp:TextBox ID="txtAddress" runat="server" placeholder="Address" TextMode="MultiLine" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtZipCode" class="control-label">Zip*</label>
                            <div class="controls">
                                <asp:TextBox ID="txtZipCode" runat="server" placeholder="Zip Code" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtCity" class="control-label">City*</label>
                            <div class="controls">
                                <asp:TextBox ID="txtCity" runat="server" placeholder="City" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtState" class="control-label">State*</label>
                            <div class="controls">
                                <UC:ZipMaster ID="ucState" runat="server" MasterFlag="STATE" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtCountry" class="control-label">Country*</label>
                            <div class="controls">
                                <UC:ZipMaster ID="txtCountry" runat="server" MasterFlag="COUNTRY" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="txtPhoneNo" class="control-label">Phone No*</label>
                            <div class="controls">
                                <asp:TextBox ID="txtPhoneNo" runat="server" placeholder="Phone No" data-rule-required="true" class="complexify-me mask_phone"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtMobileNo" class="control-label">Mobile*</label>
                            <div class="controls">
                                <asp:TextBox ID="txtMobileNo" runat="server" placeholder="Mobile No" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <asp:Button ID="cmdRegister" runat="server" CssClass="btn btn-primary" Text="Proceed For Registration" OnClick="cmdRegister_Click" />
            </div>
        </div>
        <div class="span6">
            <div class="box">
                <div class="box-title">
                    <h3>&nbsp;
                    </h3>
                </div>
                <div class="box-content">
                    <h4>
                        <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdclient" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript" src="http://smart-ip.net/geoip-json?callback=GetUserInfo"></script>
</asp:Content>

