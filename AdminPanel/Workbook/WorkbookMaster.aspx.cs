﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Workbook_WorkbookMaster : System.Web.UI.Page
{
    clsWorkBook clsWB = new clsWorkBook();
    clsobjectsharing clsOBS = new clsobjectsharing();

    private Guid loginuserid = (Guid)Membership.GetUser().ProviderUserKey;
    private void BindGrid()
    {
        if (User.IsInRole("administrator"))
        {
            dtWorkbook.DataSource = clsWB.GetAllRecord();
        }
        else
        {
            dtWorkbook.DataSource = clsWB.GetAllRecordByUser((Guid)Membership.GetUser().ProviderUserKey);
        }
        dtWorkbook.DataBind();
    }
    private void BindGridNotMapped()
    {
        if (User.IsInRole("administrator"))
        {
            dtWorkbook.DataSource = clsWB.GetAllNotMapped();
        }
        else
        {
            dtWorkbook.DataSource = clsWB.GetAllNotMappedByMember((Guid)Membership.GetUser().ProviderUserKey);
        }
        dtWorkbook.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGrid();
            if (MySession.Current.MESSAGE != string.Empty)
            {
                lblmessage.Text = MySession.Current.MESSAGE;
                msgdiv.Visible = true;
                MySession.Current.MESSAGE = string.Empty;
            }
        }
    }
    protected void dtWorkbook_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = e.CommandName;
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Workbook/WorkbookAddEdit.aspx");
        }
        if (e.CommandName == "SHARE")
        {
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Workbook/WorkbookSharing.aspx");
        }
        if (e.CommandName == "FREEZ")
        {
            Boolean BlnGetrslt = clsWB.GetRecordByIDInProperties(Convert.ToInt64(e.CommandArgument));
            Int64 Version = clsWB.GetWorkBookVersion(clsWB.wrb_code);
            Boolean BlnRslt = clsWB.ChangeStatus(Convert.ToInt64(e.CommandArgument), true, Version + 1);
            if (BlnRslt)
            {
                Response.Redirect("~/Workbook/WorkbookMaster.aspx");
            }
            else
            {
                lblmessage.Text = "Error Occured During Freez Your Workbook" + clsWB.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
        if (e.CommandName == "NEWVER")
        {
            Boolean BlnRslt = clsWB.GetRecordByIDInProperties(Convert.ToInt64(e.CommandArgument));
            if (BlnRslt)
            {
                clsWB.wrb_sts = false;
                Boolean blnSvRsl = clsWB.Save();
                if (blnSvRsl == true)
                {
                    clsWB.GetRecordByCodeInProperties(clsWB.wrb_code);
                    clsOBS.shr_sharedobjid = clsWB.wrb_id;
                    clsOBS.shr_object = "WORKBOOK";
                    clsOBS.shr_ownerid = clsWB.wrb_UserId;
                    clsOBS.shr_sharedwithid = clsWB.wrb_UserId;
                    blnSvRsl = clsOBS.Save();
                    // get all chapters and make copy

                    Response.Redirect("~/Workbook/WorkbookMaster.aspx");
                }
                else
                {
                    lblmessage.Text = "Error Occured During Creatig New Copy" + clsWB.GetSetErrorMessage;
                    msgdiv.Visible = true;
                }
            }
            else
            {
                lblmessage.Text = "Error Occured During Featchng Your Workbook" + clsWB.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Workbook/WorkbookAddEdit.aspx");
    }
    protected void cmdHideMapped_Click(object sender, EventArgs e)
    {
        BindGridNotMapped();
    }
    protected void cmdShowAll_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void cmdMyRec_Click(object sender, EventArgs e)
    {

    }
    protected void dtWorkbook_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;
            Guid UserId = new Guid(DataBinder.Eval(DI, "wrb_UserId").ToString());

            Boolean Isact = Convert.ToBoolean(DataBinder.Eval(DI, "wrb_sts"));
            if (Isact)
            {
                LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdFreez");
                if (UserId == loginuserid)
                {
                    lnkf.Enabled = false;
                    lnkf.CssClass = "btn btn-success";
                }
                else
                {
                    lnkf.Visible = false;
                }
                LinkButton lnks = (LinkButton)e.Item.FindControl("cmdShare");
                lnks.Enabled = true;
            }
            else
            {
                LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdFreez");
                lnkf.Enabled = true;

                LinkButton lnks = (LinkButton)e.Item.FindControl("cmdShare");
                lnks.Enabled = false;
                lnks.CssClass = "btn btn-success";
            }

            if (!User.IsInRole("administrator"))
            {
                if (UserId != loginuserid)
                {
                    LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdEdit");
                    lnkf.Visible = false;
                }
            }
            var dv = e.Item.FindControl("action");
            dv.Visible = true;
        }
    }
    private void createchapter(Int64 chpid)
    {
        clsChapters clsCHP = new clsChapters();
        Boolean blnRslt = clsCHP.GetRecordByIDInProperties(chpid);
        if (blnRslt)
        {
            clsCHP.chp_sts = false;
            clsCHP.Save();
            clsCHP.GetRecordByCodeInProperties(clsCHP.chp_code);
            clsOBS.shr_sharedobjid = clsCHP.chp_id;
            clsOBS.shr_object = "CHAPTER";
            clsOBS.shr_ownerid = clsCHP.chp_Userid;
            clsOBS.shr_sharedwithid = clsCHP.chp_Userid;
            clsOBS.Save();
        }
    }
    private void createlesson(Int64 lsnid)
    {
        clsLessions clsLSN = new clsLessions();
        Boolean blnRslt = clsLSN.GetRecordByIDInProperties(lsnid);
        if (blnRslt)
        {
            clsLSN.lsn_sts = false;
            clsLSN.Save();
            clsLSN.GetRecordByCodeInProperties(clsLSN.lsn_code);
            clsOBS.shr_sharedobjid = clsLSN.lsn_id;
            clsOBS.shr_object = "LESSON";
            clsOBS.shr_ownerid = clsLSN.lsn_UserId;
            clsOBS.shr_sharedwithid = clsLSN.lsn_UserId;
            clsOBS.Save();
        }
    }
    private void createassignment(Int64 asnid)
    {
        clsAssignments clsASN = new clsAssignments();
        Boolean blnRslt = clsASN.GetRecordByIDInProperties(asnid);
        if (blnRslt)
        {
            clsASN.asn_sts = false;
            clsASN.Save();
            clsASN.GetRecordByCodeInProperties(clsASN.asn_code);
            clsOBS.shr_sharedobjid = clsASN.asn_id;
            clsOBS.shr_object = "ASSIGNMENT";
            clsOBS.shr_ownerid = clsASN.asn_UserId;
            clsOBS.shr_sharedwithid = clsASN.asn_UserId;
            clsOBS.Save();
        }
    }
}