﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data.SqlClient;
using System.Data;

public partial class Workbook_AssignmentAnswer : System.Web.UI.Page
{
    clsWorkbookAnswer clsWA = new clsWorkbookAnswer();
    clsAssignments clsASN = new clsAssignments();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Loaddata();
        }
    }

    public void Loaddata()
    {
        Int64 asnid = MySession.Current.EditID;
        Boolean blnAnsRslt = clsASN.GetRecordByIDInProperties(asnid);
        if (blnAnsRslt)
        {
            Guid clntid = MySession.Current.AssignmentClientId;
            blnAnsRslt = clsWA.ReadAnswer(asnid, clntid);
            if (blnAnsRslt)
            {
                AsnQue.Text = clsASN.asn_description;
                AsnAnswer.Text = clsWA.wns_answer;
            }
        }
    }
}