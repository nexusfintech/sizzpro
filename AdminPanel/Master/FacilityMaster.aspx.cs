﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Masters_FacilityMaster : System.Web.UI.Page
{
    clsFacilities clsFCL = new clsFacilities();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            loaddata();
        }
        if(MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptFclty.DataSource = clsFCL.GetAllRecordByMem((Guid)Membership.GetUser().ProviderUserKey);
        rptFclty.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Master/FacilityAddEdit.aspx");
    }
    protected void dtAssignment_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = e.CommandName;
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/FacilityAddEdit.aspx");
        }
    }
}