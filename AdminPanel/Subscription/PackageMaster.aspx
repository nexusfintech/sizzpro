﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PackageMaster.aspx.cs" Inherits="Subscription_ServicesMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <%-- <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
               <asp:HyperLink ID="lnkMappingtoWB" runat="server" NavigateUrl="~/Subscription/ServiceModuleMapping.aspx"><i class="icon-asterisk"></i>&nbsp;Mapping modules</asp:HyperLink>
            </li>
        </ul>
    </div>--%>
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn btn-primary" Text="Create New Package" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-color box-bordered">
        <div class="box-title">
            <h3>
                <i class="glyphicon-projector"></i>
                Packages
            </h3>
        </div>
        <div class="box-content nopadding">
            <asp:Repeater ID="dtServices" runat="server">
                <HeaderTemplate>
                    <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Package Name</th>
                                <th>Fix Charge</th>
                                <th>Recuring Charge</th>
                                <%--<th>Usages Charge</th>--%>
                                <th style="text-align: center;">Edit</th>
                                <th style="text-align: center;">Manage Modules</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("srvc_servicename") %></td>
                        <td>$ <%# Eval("srvc_fixcharge") %> / <%# Eval("srvc_periodinterval") %></td>
                        <td>$ <%# Eval("srvc_recuringcharge") %>  / <%# Eval("srvc_periodinterval") %></td>
                        <%--<td><%# Eval("srvc_usagescharge") %></td>--%>
                        <td style="text-align: center">
                            <asp:LinkButton ID="cmdEdit" CommandName="EDIT" CssClass="btn btn-green" runat="server" OnClick="cmdEdit_Click" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "srvc_id") %>'><i class="icon-edit"></i> &nbsp;Edit</asp:LinkButton>
                        </td>
                        <td style="text-align: center">
                            <asp:LinkButton ID="cmdModules" runat="server" CssClass="btn btn-green" CommandName="MODULES" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "srvc_id") %>' OnClick="cmdEdit_Click"><i class="icon-cog"></i>&nbsp;Manage Modules</asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

