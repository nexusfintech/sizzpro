﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="aboutcelina.aspx.cs" Inherits="aboutcelina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2>ABOUT Celina Viilalobos</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <iframe width="560" height="315" src="http://www.youtube.com/embed/tGjkurW1KRE" frameborder="0" allowfullscreen></iframe>
            <p>Celina recently moved to Huntsville Alabama from Chihuahua Chihuahua Mexico. There for many years she worked as a Licensed Psychologist serving several different populations and helping individuals, couples and families work through their issues and reach their goals. Currently under the supervision of Joseph L. Follette Jr. LMFT - Approved Supervisor, Celina is working to become a Licensed Marriage & Family Therapist here in the USA. Celina especially enjoys working with adolescents and adults. She has experience with domestic violence, grief rcovery and anger management. Most of her clients say that they experience Celina as being warm, kind and gentle in her approach to therapy. While Spanish is her first language she speaks English as well as some French. She loves helping people and looks forward to helping you work through the challenges you are currently facing.</p>
            <p><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=S2DWM4ZZ8KHV4" target="_blank">Pay here for Celina Villalobos</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

