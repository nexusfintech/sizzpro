﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsRefaundreasonmaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsRefaundreasonmaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsRefaundreasonmaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _rrm_id; }
            set
            {
                _rrm_id = value;
                if (clsCMD.Parameters.Contains("@rrm_id"))
                { clsCMD.Parameters["@rrm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@rrm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _rrm_id;
        public Int64 rrm_id
        {
            get { return _rrm_id; }
            set
            {
                _rrm_id = value;
                if (clsCMD.Parameters.Contains("@rrm_id"))
                { clsCMD.Parameters["@rrm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@rrm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _rrm_reason;
        public String rrm_reason
        {
            get { return _rrm_reason; }
            set
            {
                _rrm_reason = value;
                if (clsCMD.Parameters.Contains("@rrm_reason"))
                { clsCMD.Parameters["@rrm_reason"].Value = value; }
                else { clsCMD.Parameters.Add("@rrm_reason", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Guid _rrm_userid;
        public Guid rrm_userid
        {
            get { return _rrm_userid; }
            set
            {
                _rrm_userid = value;
                if (clsCMD.Parameters.Contains("@rrm_userid"))
                { clsCMD.Parameters["@rrm_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@rrm_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                rrm_id = Convert.ToInt64(dr["rrm_id"]);
                rrm_reason = Convert.ToString(dr["rrm_reason"]);
                rrm_userid = new Guid(dr["rrm_userid"].ToString());
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            rrm_id = Convert.ToInt64("0");
            rrm_reason = Convert.ToString("NA");
            rrm_userid = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.rrm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_RefaundreasonmasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 rrm_id)
        {
            Boolean blnResult = false;
            this.rrm_id = rrm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_RefaundreasonmasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 rrm_id)
        {
            Boolean blnResult = false;
            this.rrm_id = rrm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_RefaundreasonmasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_RefaundreasonmasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByAdmin(Guid adminid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.rrm_userid = adminid;
                clsCMD.CommandText = "sp_RefaundreasonmasterGetData";
                SetGetSPFlag = "ALLBYADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsRefaundreasonmaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsRefaundreasonmaster_PropertiesList> lstResult = new List<clsRefaundreasonmaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].rrm_id = Convert.ToInt64(dr["rrm_id"]);
                lstResult[i].rrm_reason = Convert.ToString(dr["rrm_reason"]);
                lstResult[i].rrm_userid = new Guid(dr["rrm_userid"].ToString());

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 rrm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.rrm_id = rrm_id;
                clsCMD.CommandText = "sp_RefaundreasonmasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 rrm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.rrm_id = rrm_id;
                clsCMD.CommandText = "sp_RefaundreasonmasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsRefaundreasonmaster_PropertiesList
    {
        public Int64 rrm_id { get; set; }
        public String rrm_reason { get; set; }
        public Guid rrm_userid { get; set; }
    }
}