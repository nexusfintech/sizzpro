﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
using System.Globalization;
using System.Data;

public partial class Admin_SessionManagement_FollowupSesionAddEdit : System.Web.UI.Page
{
    clsClientFollowUpSession clsCFS = new clsClientFollowUpSession();

    //clsFollowupSessionDignosis clsFSD = new clsFollowupSessionDignosis();
    clsFollowupDignosisProcedure clsFDP = new clsFollowupDignosisProcedure();
    clsFollowupDignosis clsFD = new clsFollowupDignosis();
    clsFollowupSessionBill clsFSB = new clsFollowupSessionBill();
    clsMentalstatusMaster clsMSM = new clsMentalstatusMaster();
    clsFollowupSoap clsFS = new clsFollowupSoap();

    DateTime SessionDate;
    DateTime NextSessionDate;
    Guid clientid;
    private Guid sessionid { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["sessionuserid"] == null)
        {
            Response.Redirect("~/Users/UserClientManage.aspx");
        }
        else
        {
            clientid = new Guid(Session["sessionuserid"].ToString());
        }
        if (!IsPostBack)
        {
            if (MySession.Current.SESSIONID == new Guid())
            {
                MySession.Current.SESSIONID = Guid.NewGuid();
            }
            else
            {
                if (MySession.Current.AddEditFlag == "EDIT")
                {
                    //bindlistbox();
                    binddata();
                }
            }
        }
        else
        {
            lblmessage.Text = string.Empty;
            msgdiv.Visible = false;
        }
        sessionid = MySession.Current.SESSIONID;
        bindprocedures();
    }
    //private void bindlistbox()
    //{
    //    DataTable dtapperance = clsMSM.GetAllRecordByFlag("APPEARANCE");
    //    foreach (DataRow dr in dtapperance.Rows)
    //    {
    //        ListItem li = new ListItem(dr["msm_name"].ToString(), dr["msm_value"].ToString());
    //        li.Attributes.Add("class", "item");
    //        ddlapperance.Items.Add(li);
    //    }
    //    DataTable dtspch = clsMSM.GetAllRecordByFlag("SPEECH");
    //    foreach (DataRow dr in dtspch.Rows)
    //    {
    //        ListItem li = new ListItem(dr["msm_name"].ToString(), dr["msm_value"].ToString());
    //        li.Attributes.Add("class", "item");
    //        ddlspeech.Items.Add(li);
    //    }
    //    DataTable dtmood = clsMSM.GetAllRecordByFlag("MOOD");
    //    foreach (DataRow dr in dtmood.Rows)
    //    {
    //        ListItem li = new ListItem(dr["msm_name"].ToString(), dr["msm_value"].ToString());
    //        li.Attributes.Add("class", "item");
    //        ddlmood.Items.Add(li);
    //    }
    //    DataTable dtaffect = clsMSM.GetAllRecordByFlag("AFFECT");
    //    foreach (DataRow dr in dtaffect.Rows)
    //    {
    //        ListItem li = new ListItem(dr["msm_name"].ToString(), dr["msm_value"].ToString());
    //        li.Attributes.Add("class", "item");
    //        ddlaffect.Items.Add(li);
    //    }
    //    DataTable dtthtfrm = clsMSM.GetAllRecordByFlag("THOUGHTFORM");
    //    foreach (DataRow dr in dtthtfrm.Rows)
    //    {
    //        ListItem li = new ListItem(dr["msm_name"].ToString(), dr["msm_value"].ToString());
    //        li.Attributes.Add("class", "item");
    //        ddlthoughtform.Items.Add(li);
    //    }
    //    DataTable dtthtcntn = clsMSM.GetAllRecordByFlag("THOUGHTCONTENT");
    //    foreach (DataRow dr in dtthtcntn.Rows)
    //    {
    //        ListItem li = new ListItem(dr["msm_name"].ToString(), dr["msm_value"].ToString());
    //        li.Attributes.Add("class", "item");
    //        ddlthoughtcontent.Items.Add(li);
    //    }
    //}
    private void binddata()
    {
        Boolean blnRslt = clsCFS.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {
            MySession.Current.SESSIONID = clsCFS.cfs_sessionid;
            sessionid = MySession.Current.SESSIONID;
            bindprocedures();

            txtDate.Text = Convert.ToDateTime(clsCFS.cfs_sessiondate.ToString()).ToString("MM/dd/yyyy");
            txtNextApnt.Text = Convert.ToDateTime(clsCFS.cfs_nextappoinment.ToString()).ToString("MM/dd/yyyy");
            txtIntime.Text = Convert.ToDateTime(clsCFS.cfs_intime.ToString()).TimeOfDay.ToString();
            txtOutTime.Text = Convert.ToDateTime(clsCFS.cfs_outtime.ToString()).TimeOfDay.ToString();
            txtTotalHrs.Text = (clsCFS.cfs_totalhrs / 60).ToString();

            ddlPos.SelectedValue = clsCFS.cfs_pos;
            ddlPayer.SelectedValue = clsCFS.cfs_payer;
            ddlFclty.SelectedValue = clsCFS.cfs_facility;
            ddlProvider.SelectedValue = clsCFS.cfs_provider;
            ddlShow.SelectedValue = clsCFS.cfs_show;

            blnRslt = clsFSB.GetRecordBySessionId(clsCFS.cfs_sessionid);
            if (blnRslt)
            {
                txtClientPayment.Text = clsFSB.fsb_clientpayment.ToString();
                txtInsurancepayment.Text = clsFSB.fsb_insurancepayment.ToString();
                txtTotalCharged.Text = clsFSB.fsb_totalcharged.ToString();
                txtAdjustmnt.Text = clsFSB.fsb_writeoff.ToString();
                txtTotalRcved.Text = clsFSB.fsb_totalrecieved.ToString();
                txtBalanceDue.Text = clsFSB.fsb_balancedue.ToString();
            }
            bindCasenote();
            binddignosiscode();
        }
    }
    protected void cmdclaim_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/ClaimManagment/ClaimMaster.aspx");
    }
    private string validatepage()
    {
        string validateresult = string.Empty;

        if (!(DateTime.TryParseExact(txtDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out SessionDate)))
        {
            return validateresult = "Please Enter Date in mm/DD/yyyy Formate";
        }
        else if (!(DateTime.TryParseExact(txtNextApnt.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out NextSessionDate)))
        {
            return validateresult = "Please Enter Date in mm/DD/yyyy Formate";
        }
        //else if (!DateTime.TryParseExact(txtIntime.Text, "hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtInTime))
        //{
        //    return validateresult = "Invalid InTime Please enter in hh:mm tt";
        //}
        //else if (!DateTime.TryParseExact(txtOutTime.Text, "hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtOutTime))
        //{
        //    return validateresult = "Invalid OutTime Please enter in hh:mm tt";
        //}
        else
        {
            return validateresult;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        LinkButton cmnd = (LinkButton)sender;
        string vldtrslt = validatepage();
        if (vldtrslt != string.Empty)
        {
            lblmessage.Text = vldtrslt;
            msgdiv.Visible = true;
            return;
        }
        bool rslt = savedgenerealata();
        if (rslt)
        {
            this.savecasenote();
            rslt = savebillingdata();
            if (rslt)
            {
                MySession.Current.MESSAGE = "Session Saved Successfully";
                Response.Redirect("~/Admin/SessionManagement/FollowupSession.aspx");
            }
            else
            {
                lblmessage.Text = clsFSB.GetSetErrorMessage;
                msgdiv.Visible = true;
                return;
            }
        }
        else
        {
            lblmessage.Text = clsCFS.GetSetErrorMessage;
            msgdiv.Visible = true;
            return;
        }
    }
    private double GetHours(DateTime dtStartDate, DateTime dtEndDate)
    {
        TimeSpan span = dtEndDate.Subtract(dtStartDate);
        double dblResult = span.TotalHours;
        return (dblResult * 60);
    }
    private bool savedgenerealata()
    {
        clsCFS.cfs_facility = ddlFclty.SelectedValue;
        clsCFS.cfs_provider = ddlProvider.SelectedValue;
        clsCFS.cfs_pos = ddlPos.SelectedValue;
        clsCFS.cfs_payer = ddlPayer.SelectedValue;

        clsCFS.cfs_userid = clientid;
        clsCFS.cfs_sessiondate = SessionDate;
        clsCFS.cfs_nextappoinment = NextSessionDate;
        clsCFS.cfs_sessionid = sessionid;

        var dt = DateTime.Now.Date; // time is zero by default
        string it = txtIntime.Text;
        var tm = TimeSpan.Parse(it);
        DateTime dtInTime = dt + tm;
        clsCFS.cfs_intime = dtInTime;
        string ot = txtOutTime.Text;
        tm = TimeSpan.Parse(ot);
        DateTime dtOutTime = dt + tm;
        clsCFS.cfs_outtime = dtOutTime;
        clsCFS.cfs_totalhrs = Convert.ToInt64(GetHours(dtInTime, dtOutTime));
        clsCFS.cfs_show = Convert.ToInt64(ddlShow.SelectedValue);
        Boolean blnSvRslt;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnSvRslt = clsCFS.Save();
        }
        else
        {
            blnSvRslt = clsCFS.Update(MySession.Current.EditID);
        }
        return blnSvRslt;
    }
    private bool savecasenote()
    {
        clsFS.fsoap_sessionid = sessionid;
        bool blnrslt = false;
        //foreach (ListItem item in ddlapperance.Items)
        //{
        //    if (item.Selected)
        //    {
        //        if (clsFCN.fcn_apper == string.Empty)
        //        {
        //            clsFCN.fcn_apper = item.Value;
        //        }
        //        else
        //        {
        //            clsFCN.fcn_apper += "#" + item.Value;
        //        }
        //    }
        //}
        //foreach (ListItem item in ddlaffect.Items)
        //{
        //    if (item.Selected)
        //    {
        //        if (clsFCN.fcn_affect == string.Empty)
        //        {
        //            clsFCN.fcn_affect = item.Value;
        //        }
        //        else
        //        {
        //            clsFCN.fcn_affect += "#" + item.Value;
        //        }
        //    }
        //}
        //foreach (ListItem item in ddlspeech.Items)
        //{
        //    if (item.Selected)
        //    {
        //        if (clsFCN.fcn_speech == string.Empty)
        //        {
        //            clsFCN.fcn_speech = item.Value;
        //        }
        //        else
        //        {
        //            clsFCN.fcn_speech += "#" + item.Value;
        //        }
        //    }
        //}
        //foreach (ListItem item in ddlthoughtform.Items)
        //{
        //    if (item.Selected)
        //    {
        //        if (clsFCN.fcn_thoughtForm == string.Empty)
        //        {
        //            clsFCN.fcn_thoughtForm = item.Value;
        //        }
        //        else
        //        {
        //            clsFCN.fcn_thoughtForm += "#" + item.Value;
        //        }
        //    }
        //}
        //foreach (ListItem item in ddlthoughtcontent.Items)
        //{
        //    if (item.Selected)
        //    {
        //        if (clsFCN.fcn_thughtContent == string.Empty)
        //        {
        //            clsFCN.fcn_thughtContent = item.Value;
        //        }
        //        else
        //        {
        //            clsFCN.fcn_thughtContent += "#" + item.Value;
        //        }
        //    }
        //}
        //foreach (ListItem item in ddlmood.Items)
        //{
        //    if (item.Selected)
        //    {
        //        if (clsFCN.fcn_mood == string.Empty)
        //        {
        //            clsFCN.fcn_mood = item.Value;
        //        }
        //        else
        //        {
        //            clsFCN.fcn_mood += "#" + item.Value;
        //        }
        //    }
        //}
        clsFS.p_progresstogoal = txtprogress.Text;
        clsFS.s_reason = txtVisitReason.Text;
        clsFS.p_treatmentplan = txttreatmentplan.Text;
        clsFS.p_nextvisitplan = txtnextvisitplan.Text;
        clsFS.o_behaviour = txtbehaviour.Text;
        clsFS.a_intervatation = txtintervatation.Text;
        clsFS.o_reasponse = txtreasponse.Text;
        clsFS.p_newclinicissue = txtnewissue.Text;
        clsFS.o_present = txtwhopresent.Text;
        clsFS.fsoap_id = Convert.ToInt64(fcn_id.Value);
        if (MySession.Current.AddEditFlag == "EDIT")
        {
            if (clsFS.fsoap_id != 0)
            {
                blnrslt = clsFS.Update(clsFS.fsoap_id);
            }
            else
            {
                blnrslt = clsFS.Save();
            }
        }
        else
        {
            blnrslt = clsFS.Save();
        }
        return blnrslt;
    }
    private bool savebillingdata()
    {
        clsFSB.fsb_sessionid = sessionid;
        clsFSB.fsb_clientpayment = Convert.ToInt64(txtClientPayment.Text);
        clsFSB.fsb_insurancepayment = Convert.ToInt64(txtInsurancepayment.Text);
        clsFSB.fsb_totalcharged = Convert.ToInt64(txtTotalCharged.Text);
        clsFSB.fsb_totalrecieved = Convert.ToInt64(txtTotalRcved.Text);
        clsFSB.fsb_writeoff = Convert.ToInt64(txtAdjustmnt.Text);
        clsFSB.fsb_balancedue = Convert.ToInt64(txtBalanceDue.Text);
        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnRslt = clsFSB.Save();
        }
        else
        {
            blnRslt = clsFSB.Update(MySession.Current.EditID);
        }
        return blnRslt;
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/SessionManagement/FollowupSession.aspx");
    }
    protected void cmdSsn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/SessionManagement/FollowupSession.aspx");
    }
    protected void rptDgnProcedure_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            Session["ProcedureEditid"] = Convert.ToInt64(e.CommandArgument);
            filldignosisdata(Convert.ToInt64(e.CommandArgument));
        }
        if (e.CommandName == "DELETE")
        {
            Boolean blnRslt = clsFDP.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (blnRslt)
            {
                lblmessage.Text = "Data Deleted Successfully";
                msgdiv.Visible = true;
                bindprocedures();
                //Response.Redirect("~/Admin/SessionManagement/FollowupSesionAddEdit.aspx");
            }
            else
            {
                lblmessage.Text = clsFDP.GetSetErrorMessage;
                msgdiv.Visible = true;
                return;
            }
        }
    }
    private void filldignosisdata(Int64 id)
    {
        Boolean blnRslt = clsFDP.GetRecordByIDInProperties(id);
        if (blnRslt)
        {
            ddlCptCode.SelectedValue = clsFDP.sdp_cptcode;
            //ddlProcedure.SelectedValue = clsFSD.fsd_procedure;
            txtCharge.Text = clsFDP.sdp_linecharge.ToString();
            txtmoda.Text = clsFDP.sdp_moda;
            txtmodb.Text = clsFDP.sdp_modb;
            txtmodc.Text = clsFDP.sdp_modc;
            txtmodd.Text = clsFDP.sdp_modd;
            txtdignosis.Text = clsFDP.sdp_dignosiscode;
        }
        else
        {
            lblmessage.Text = clsFDP.GetSetErrorMessage;
            msgdiv.Visible = true;
            return;
        }
    }
    protected void rptDgnProcedure_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;
            long charge = Convert.ToInt64(DataBinder.Eval(DI, "sdp_linecharge"));
            TotalChrg.Value = (Convert.ToInt64(TotalChrg.Value) + charge).ToString();
        }
    }
    private string validatedignosis()
    {
        string vldtrslt = string.Empty;
        if (txtCharge.Text == string.Empty)
        {
            return vldtrslt = "Dignosis Charge is Required";
        }
        else
        {
            return vldtrslt;
        }
    }
    protected void cmdSaveandNew_Click(object sender, EventArgs e)
    {
        // save dignosis detail
        string vldtnrslt = validatedignosis();
        if (vldtnrslt != string.Empty)
        {
            lblmessage.Text = vldtnrslt;
            msgdiv.Visible = true;
            return;
        }
        clsFDP.sdp_cptcode = ddlCptCode.SelectedValue;
        clsFDP.sdp_linecharge = Convert.ToDouble(txtCharge.Text);
        clsFDP.sdp_sessionid = sessionid;
        clsFDP.sdp_moda = txtmoda.Text;
        clsFDP.sdp_modb = txtmodb.Text;
        clsFDP.sdp_modc = txtmodc.Text;
        clsFDP.sdp_modd = txtmodd.Text;
        clsFDP.sdp_dignosiscode = txtdignosis.Text;

        Boolean blnRslt = false;

        if (Session["ProcedureEditid"] == null)
        {
            blnRslt = clsFDP.Save();
        }
        else
        {
            blnRslt = clsFDP.Update(Convert.ToInt64(Session["ProcedureEditid"]));
        }
        if (blnRslt)
        {
            bindprocedures();
            Session.Remove("ProcedureEditid");
        }
        else
        {
            lblmessage.Text = clsFDP.GetSetErrorMessage;
            msgdiv.Visible = true;
            return;
        }
    }
    private void bindprocedures()
    {
        TotalChrg.Value = 0.ToString();
        rptDgnProcedure.DataSource = clsFDP.GetAllRecordBySession(sessionid);
        rptDgnProcedure.DataBind();
        txtTotalCharged.Text = TotalChrg.Value.ToString();
    }
    private void bindCasenote()
    {
        Boolean blnRslt = clsFS.GetRecordBySession(sessionid);
        if (blnRslt)
        {
            //string[] affect = clsFS.fcn_affect.Split('#');
            //foreach (ListItem item in ddlaffect.Items)
            //{
            //    foreach (string af in affect)
            //    {
            //        if (af == item.Value.ToString())
            //        {
            //            item.Selected = true;
            //        }
            //    }
            //    item.Attributes.Add("CssClass", "item");
            //}
            //string[] appear = clsFCN.fcn_apper.Split('#');
            //foreach (ListItem item in ddlapperance.Items)
            //{
            //    foreach (string ap in appear)
            //    {
            //        if (ap == item.Value.ToString())
            //        {
            //            item.Selected = true;
            //        }
            //    }
            //    item.Attributes.Add("CssClass", "item");
            //}
            //string[] speech = clsFCN.fcn_speech.Split('#');
            //foreach (ListItem item in ddlspeech.Items)
            //{
            //    foreach (string sp in speech)
            //    {
            //        if (sp == item.Value.ToString())
            //        {
            //            item.Selected = true;
            //        }
            //    }
            //    item.Attributes.Add("CssClass", "item");
            //}
            //string[] mood = clsFCN.fcn_mood.Split('#');
            //foreach (ListItem item in ddlmood.Items)
            //{
            //    foreach (string md in mood)
            //    {
            //        if (md == item.Value.ToString())
            //        {
            //            item.Selected = true;
            //        }
            //    }
            //    item.Attributes.Add("CssClass", "item");
            //}
            //string[] ThoughtForm = clsFCN.fcn_thoughtForm.Split('#');
            //foreach (ListItem item in ddlthoughtform.Items)
            //{
            //    foreach (string tf in ThoughtForm)
            //    {
            //        if (tf == item.Value.ToString())
            //        {
            //            item.Selected = true;
            //        }
            //    }
            //    item.Attributes.Add("CssClass", "item");
            //}
            //string[] ThoughtContent = clsFCN.fcn_thughtContent.Split('#');
            //foreach (ListItem item in ddlthoughtcontent.Items)
            //{
            //    foreach (string tc in ThoughtContent)
            //    {
            //        if (tc == item.Value.ToString())
            //        {
            //            item.Selected = true;
            //        }
            //    }
            //    item.Attributes.Add("CssClass", "item");
            //}
            fcn_id.Value = clsFS.fsoap_id.ToString();
            txtprogress.Text = clsFS.p_progresstogoal;
            txtVisitReason.Text = clsFS.s_reason;
            txttreatmentplan.Text = clsFS.p_treatmentplan;
            txtnextvisitplan.Text = clsFS.p_nextvisitplan;
            txtbehaviour.Text = clsFS.o_behaviour;
            txtintervatation.Text = clsFS.a_intervatation;
            txtreasponse.Text = clsFS.o_reasponse;
            txtnewissue.Text = clsFS.p_newclinicissue;
            txtwhopresent.Text = clsFS.o_present;
        }
    }
    protected void lnkAddDcode_Click(object sender, EventArgs e)
    {
        clsFD.fd_dignosiscode = ddldgcode.SelectedValue;
        clsFD.fd_pointer = ddlpointer.SelectedValue;
        clsFD.fd_sessionid = sessionid;
        Boolean blnRslt = false;
        if (Session["DgcEditid"] == null)
        {
            blnRslt = clsFD.Save();
        }
        else
        {
            blnRslt = clsFD.Update(Convert.ToInt64(Session["DgcEditid"]));
        }
        if (blnRslt)
        {
            binddignosiscode();
            Session.Remove("DgcEditid");
        }
    }
    private void binddignosiscode()
    {
        rptDignosisCode.DataSource = clsFD.GetAllRecordBySession(sessionid);
        rptDignosisCode.DataBind();
    }
    protected void rptDignosisCode_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "DELETE")
        {
            Boolean blnRslt = clsFD.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (blnRslt)
            {
                lblmessage.Text = "Data Deleted Successfully";
                msgdiv.Visible = true;
                binddignosiscode();

            }
            else
            {
                lblmessage.Text = clsFDP.GetSetErrorMessage;
                msgdiv.Visible = true;
                return;
            }
        }
    }
}