﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OurServices.aspx.cs" Inherits="OurServices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="glyphicon-sampler"></i>Our Services</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <h5><i class="glyphicon-sampler"></i> INDIVIDUAL COUNSELING</h5>
            <p>When you need to talk to someone about something going on with your personally that may or may not involve someone else, it is good to sit down with a counselor. A counselor will listen to you and give you the feedback you need to feel better or make the decision you need to make. <a href="individual-counselling.aspx" class="ministrylinks">MORE...</a></p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h5><i class="glyphicon-sampler"></i> COUPLES COUNSELING</h5>
            <p>Frequently couples have differences that they are unable to solve alone. A counselor is able to without partiality hear both sides and help you come to a peaceful resolution. Men and women are very different and it is sometime extremely difficult to communite with one another. Counseling can be very helpful in helping you understand one another and build the level of intimacy you desire. <a href="couples-counselling.aspx" class="ministrylinks">MORE...</a></p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h5><i class="glyphicon-sampler"></i> FAMILY COUNSELING</h5>
            <p>Families face many challenges. From the time a baby is born to the time when you are dealing with aging parents, there are so many issues you must face and figure out. A counselor can be extremely helpful in being an additional resource for you. Another set of ears to hear what each of you is saying and another voice to say what needs to be said that may not be heard otherwise. <a href="family-counselling.aspx" class="ministrylinks">MORE...</a></p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h5><i class="glyphicon-sampler"></i> DEPRESSION THERAPY</h5>
            <p>When burdens get so hard to bear it is easy to withdraw and feel defeated. It is important to talk to a counselor who can help you understand what is going on and help you see things in a way that will lead to more productive thoughts and behaviors. <a href="depression.aspx" class="ministrylinks">MORE...</a></p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h5><i class="glyphicon-sampler"></i> ANXIETY THERAPY</h5>
            <p>Sometimes you can get so stressed that you become afraid of bad things happening to you. Talking to a counselor alone will relieve some of your stress. A counselor can teach you ways to manage your anxiety and even eliminate it so that you can have a strong and healthy emotional outlook on life. <a href="anxiety.aspx" class="ministrylinks">MORE...</a></p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h5><i class="glyphicon-sampler"></i> GRIEF & LOSS THERAPY</h5>
            <p>It is really hard to get used to something or someone being gone. Sometimes you can't get them out of your mind. It can be so bad that you may not be able to function. Talking to a counselor will help you come to the point of being able to accept your loss and move on. <a href="grief-loss.aspx" class="ministrylinks">MORE...</a></p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h5><i class="glyphicon-sampler"></i> SEXUALITY ISSUES</h5>
            <p>Sexual problems are sometimes very difficult to talk about. Talkng to a counselor is confidential so you don't have to worry about being embarrassed. Your counselor can help you work through your sexual problems and find solutions. <a href="sexuality-issues.aspx" class="ministrylinks">MORE...</a></p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h5><i class="glyphicon-sampler"></i> ADDICTIONS RECOVERY</h5>
            <p>Humans are creatures of habit and sometimes we find ourselves habitually doing things that are bad for us. Counselors are trained to help you change your bad habits with good habits. The first step in making those changes is admitting you can't do it on your own. Reach out to a counselor who can help you get where you want to be in your life. <a href="addictions-recovery.aspx" class="ministrylinks">MORE...</a></p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

