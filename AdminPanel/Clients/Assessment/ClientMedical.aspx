﻿<%@ Page Title="Medical" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientMedical.aspx.cs" Inherits="Clients_Assessment_ClientMedical" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <UC:AssessmentStepLinks ID="ucasslinks" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-stethoscope"></i>
                Client's Medical Information
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:ClientMedical ID="ucMedical" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

