﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Admin_SessionManagement_SessionAddEdit : System.Web.UI.Page
{
    clsProfileMaster clsPRO = new clsProfileMaster();
    clsClientSession clsSSN = new clsClientSession();

    private void BindCombo()
    {
        // code 
    }
    private void BindProfileDetail()
    {
        if (clsPRO.GetRecordByMemberKeyInProperties(Session["sessionuserid"].ToString()))
        {
            lblClientname.Text = clsPRO.prm_firstname + " " + clsPRO.prm_lastname;
        }
        else { lblClientname.Text = "Not Found..!!"; }
    }
    private double GetHours(DateTime dtStartDate, DateTime dtEndDate)
    {
        TimeSpan span = dtEndDate.Subtract(dtStartDate);
        double dblResult = span.TotalHours;
        return dblResult;
    }
    private string ValidatePage()
    {
        string strResult = "";
        DateTime dtValidate;
        if (!(DateTime.TryParseExact(txtDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtValidate)))
        { strResult += "Invalid Date format please enter in mm/dd/yyyy <br/>"; }
        if (!(DateTime.TryParseExact(txtDateNoteSubmit.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtValidate)))
        { strResult += "Invalid Date format please enter in mm/dd/yyyy <br/>"; }
        if (!(DateTime.TryParseExact(txtDtdubmittoinsurace.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtValidate)))
        { strResult += "Invalid Date format please enter in mm/dd/yyyy <br/>"; }
        if (!(DateTime.TryParseExact(txtNextApnt.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtValidate)))
        { strResult += "Invalid Date format please enter in mm/dd/yyyy <br/>"; }
        if (!(DateTime.TryParseExact(txtIntime.Text, "hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtValidate)))
        { strResult += "Invalid Time format please enter in hh:mm AM/PM <br/>"; }
        if (!(DateTime.TryParseExact(txtOutTime.Text, "hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtValidate)))
        { strResult += "Invalid Date format please enter in hh:mm AM/PM <br/>"; }
        if (txtLocation.Text.Trim() == "")
        { strResult += "Please Input Location <br/>"; }
        if (txtTotalHrs.Text.Trim() == "")
        { strResult += "Please Input Total Hours <br/>"; }
        if (txtPayer.Text.Trim() == "")
        { strResult += "Please Input Payer <br/>"; }

        double dblResult;
        if (!Double.TryParse(txtAdjustWriteoff.Text, out dblResult))
        { strResult += "Please input atleast 0 in Adjust Write off"; txtAdjustWriteoff.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtBalanceDue.Text, out dblResult))
        { strResult += "Please input atleast 0 in Due Balance"; txtBalanceDue.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtBilledClient.Text, out dblResult))
        { strResult += "Please input atleast 0 in Billed to Client"; txtBilledClient.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtClientPaymentReceived.Text, out dblResult))
        { strResult += "Please input atleast 0 in Client Payment Received"; txtClientPaymentReceived.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtcofacilitesFee.Text, out dblResult))
        { strResult += "Please input atleast 0 in CO Facilities Fee"; txtcofacilitesFee.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtcopayDadrecd.Text, out dblResult))
        { strResult += "Please input atleast 0 in CO-Pay/Dad Recd."; txtcopayDadrecd.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtTotalReceivedIns.Text, out dblResult))
        { strResult += "Please input atleast 0 in Total Received Insurance"; txtTotalReceivedIns.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtRecdfromInsurance.Text, out dblResult))
        { strResult += "Please input atleast 0 in Received From Insurance"; txtRecdfromInsurance.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtReceviedFromCollection.Text, out dblResult))
        { strResult += "Please input atleast 0 in Received From Collection"; txtReceviedFromCollection.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtSubmittoCollection.Text, out dblResult))
        { strResult += "Please input atleast 0 in Submit to Collection"; txtSubmittoCollection.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtSubmittoinsurance.Text, out dblResult))
        { strResult += "Please input atleast 0 in Submit To Insurance"; txtSubmittoinsurance.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtTherapist.Text, out dblResult))
        { strResult += "Please input atleast 0 in Therapist"; txtTherapist.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtTotalCharged.Text, out dblResult))
        { strResult += "Please input atleast 0 in Total Charged"; txtTotalCharged.BackColor = System.Drawing.Color.LightPink; }
        if (!Double.TryParse(txtTotalPaid.Text, out dblResult))
        { strResult += "Please input atleast 0 in Total Paid"; txtTotalPaid.BackColor = System.Drawing.Color.LightPink; }
        Int64 intResult;
        if (!Int64.TryParse(txtShow.Text, out intResult))
        { strResult += "Please input atleast 0 in Show"; txtShow.BackColor = System.Drawing.Color.LightPink; }
        if (!Int64.TryParse(txtTotalHrs.Text, out intResult))
        { strResult += "Please input atleast 0 in Total Hours"; txtTotalHrs.BackColor = System.Drawing.Color.LightPink; }
        else { if (intResult <= 0) { strResult += "Please input hours is greater then 0"; } }

        return strResult;
    }
    private void EditEntry()
    {
        if (clsSSN.GetRecordByIDInProperties(MySession.Current.EditID))
        {
            ListItem li;
            li = ddlFacilities.Items.FindByValue(clsSSN.ssn_fslid.ToString());
            if (li != null)
            { ddlFacilities.SelectedIndex = ddlFacilities.Items.IndexOf(li); }

            li = ddlITem.Items.FindByValue(clsSSN.ssn_itemid.ToString());
            if (li != null)
            { ddlITem.SelectedIndex = ddlITem.Items.IndexOf(li); }

            li = ddlProvider.Items.FindByValue(clsSSN.ssn_proid.ToString());
            if (li != null)
            { ddlProvider.SelectedIndex = ddlProvider.Items.IndexOf(li); }

            txtAdjustWriteoff.Text = clsSSN.ssn_adjustwrite.ToString();
            txtBalanceDue.Text = clsSSN.ssn_balancedue.ToString();
            txtBilledClient.Text = clsSSN.ssn_billettoclient.ToString();
            txtClaimNote.Text = clsSSN.ssn_insclmnote;
            txtClaimNumber.Text = clsSSN.ssn_clientnumber;
            txtClientPaymentReceived.Text = clsSSN.ssn_clntpayrecd.ToString();
            txtcofacilitesFee.Text = clsSSN.ssn_cofacilitionfee.ToString();
            txtcopayDadrecd.Text = clsSSN.ssn_copaydedrecd.ToString();
            txtDate.Text = clsSSN.ssn_date.ToString("MM/dd/yyyy");
            txtDateNoteSubmit.Text = clsSSN.ssn_datenotesubmit.ToString("MM/dd/yyyy");
            txtDtdubmittoinsurace.Text = clsSSN.ssn_insdatesubmit.ToString("MM/dd/yyyy");
            txtTotalReceivedIns.Text = clsSSN.ssn_totalreceived.ToString();
            txtIntime.Text = clsSSN.ssn_timein.ToString("hh:mm tt");
            txtLocation.Text = clsSSN.ssn_location;
            txtNextApnt.Text = clsSSN.ssn_nextappointment.ToString("MM/dd/yyyy");
            txtOutTime.Text = clsSSN.ssn_timeout.ToString("hh:mm tt");
            txtPayer.Text = clsSSN.ssn_payer;
            txtRecdfromInsurance.Text = clsSSN.ssn_recdfrominsu.ToString();
            txtReceviedFromCollection.Text = clsSSN.ssn_recdfrmcollection.ToString();
            txtSessiontype.Text = clsSSN.ssn_sessiontype;
            txtShow.Text = clsSSN.ssn_show.ToString();
            txtSubmittoCollection.Text = clsSSN.ssn_submitcollection.ToString();
            txtSubmittoinsurance.Text = clsSSN.ssn_billedtoinsurance.ToString();
            txtTherapist.Text = clsSSN.ssn_therapist.ToString();
            txttodaygoal.Text = clsSSN.ssn_todaygoal;
            txtTotalCharged.Text = clsSSN.ssn_totalcharged.ToString();
            txtTotalHrs.Text = clsSSN.ssn_totalHours.ToString();
            txtTotalPaid.Text = clsSSN.ssn_totalpaid.ToString();
            chkNoteentered.Checked = clsSSN.ssn_noteenter;
            chkPlaceoninvoice.Checked = clsSSN.ssn_placedinvoce;
        }
        else { lblmessage.Text = "Error in edit operation please try again. <br/>" + clsSSN.GetSetErrorMessage; }
    }
    private void ComissionCalculation()
    {
        DateTime dtInTime;
        DateTime.TryParseExact(txtIntime.Text, "hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtInTime);
        DateTime dtOutTime;
        DateTime.TryParseExact(txtOutTime.Text, "hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtOutTime);
        double dblHours = GetHours(dtInTime, dtOutTime);
        double dblReceivedPayment = Convert.ToDouble(txtClientPaymentReceived.Text) + Convert.ToDouble(txtRecdfromInsurance.Text);
        txtClientPaymentReceived.Text = dblReceivedPayment.ToString();

        double dblTotalDue = (Convert.ToDouble(txtTotalCharged.Text) - Convert.ToDouble(txtClientPaymentReceived.Text)) - Convert.ToDouble(txtAdjustWriteoff.Text);
        txtBalanceDue.Text = dblTotalDue.ToString();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["sessionuserid"] != null)
        {
            lblmode.Text = "in " + MySession.Current.AddEditFlag + " Mode";
            if (!Page.IsPostBack)
            {
                BindCombo();
                txtDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtDateNoteSubmit.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtDtdubmittoinsurace.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtNextApnt.Text = DateTime.Now.ToString("MM/dd/yyyy");
                BindProfileDetail();
                if (MySession.Current.AddEditFlag == "EDIT")
                { EditEntry(); }
            }
        }
        else { Response.Redirect("~/Users/UserClientManage.aspx"); }
    }
    protected void cmdclaim_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/ClaimManagment/ClaimMaster.aspx");
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string strResult = ValidatePage();
        if (strResult != "")
        { lblmessage.Text = strResult; return; }
        Guid guUserID = new Guid(Session["sessionuserid"].ToString());
        clsSSN.ssn_clientid = guUserID;

        clsSSN.ssn_fslid = Convert.ToInt64(ddlFacilities.SelectedItem.Value);
        clsSSN.ssn_itemid = Convert.ToInt64(ddlITem.SelectedItem.Value);
        clsSSN.ssn_proid = Convert.ToInt64(ddlProvider.SelectedItem.Value);

        DateTime dtInTime;
        if (!DateTime.TryParseExact(txtIntime.Text, "hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtInTime))
        {
            lblmessage.Text = "Invalid InTime Please enter in hh:mm tt";
            return;
        }
        DateTime dtOutTime;
        if (!DateTime.TryParseExact(txtOutTime.Text, "hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtOutTime))
        {
            lblmessage.Text = "Invalid OutTime Please enter in hh:mm tt";
            return;
        }

        ComissionCalculation();

        clsSSN.ssn_adjustwrite = Convert.ToDouble(txtAdjustWriteoff.Text);
        clsSSN.ssn_balancedue = Convert.ToDouble(txtBalanceDue.Text);
        clsSSN.ssn_billettoclient = Convert.ToDouble(txtBilledClient.Text);
        clsSSN.ssn_insclmnote = txtClaimNote.Text;
        clsSSN.ssn_clientnumber = txtClaimNumber.Text;
        clsSSN.ssn_clntpayrecd = Convert.ToDouble(txtClientPaymentReceived.Text);
        clsSSN.ssn_cofacilitionfee = Convert.ToDouble(txtcofacilitesFee.Text);
        clsSSN.ssn_copaydedrecd = Convert.ToDouble(txtcopayDadrecd.Text);
        clsSSN.ssn_date = Convert.ToDateTime(txtDate.Text);
        clsSSN.ssn_datenotesubmit = Convert.ToDateTime(txtDateNoteSubmit.Text);
        clsSSN.ssn_insdatesubmit = Convert.ToDateTime(txtDtdubmittoinsurace.Text);
        clsSSN.ssn_totalreceived = Convert.ToDouble(txtTotalReceivedIns.Text);
        clsSSN.ssn_timein = dtInTime;
        clsSSN.ssn_location = txtLocation.Text;
        clsSSN.ssn_nextappointment = Convert.ToDateTime(txtNextApnt.Text);
        clsSSN.ssn_timeout = dtOutTime;
        clsSSN.ssn_payer = txtPayer.Text;
        clsSSN.ssn_recdfrominsu = Convert.ToDouble(txtRecdfromInsurance.Text);
        clsSSN.ssn_recdfrmcollection = Convert.ToDouble(txtReceviedFromCollection.Text);
        clsSSN.ssn_sessiontype = txtSessiontype.Text;
        clsSSN.ssn_show = Convert.ToInt64(txtShow.Text);
        clsSSN.ssn_submitcollection = Convert.ToDouble(txtSubmittoCollection.Text);
        clsSSN.ssn_billedtoinsurance = Convert.ToDouble(txtSubmittoinsurance.Text);
        clsSSN.ssn_therapist = Convert.ToDouble(txtTherapist.Text);
        clsSSN.ssn_todaygoal = txttodaygoal.Text;
        clsSSN.ssn_totalcharged = Convert.ToDouble(txtTotalCharged.Text);
        clsSSN.ssn_totalHours = Convert.ToInt64(txtTotalHrs.Text);
        clsSSN.ssn_totalpaid = Convert.ToDouble(txtTotalPaid.Text);
        clsSSN.ssn_noteenter = chkNoteentered.Checked;
        clsSSN.ssn_placedinvoce = chkPlaceoninvoice.Checked;

        Boolean blnResult = false;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnResult = clsSSN.Save();
        }
        else
        {
            blnResult = clsSSN.Update(MySession.Current.EditID);
        }
        if (blnResult == true)
        {
            lblmessage.Text = "Save Successfully.";
            txtLocation.Text = "";
            txtTotalHrs.Text = "0";
            txtPayer.Text = "";
            msgdiv.Visible = true;
        }
        else
        {
            lblmessage.Text = "Error in saving operation.." + clsSSN.GetSetErrorMessage;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/SessionManagement/SessionManagement.aspx");
    }
}