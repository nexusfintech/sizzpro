﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <style type="text/css">
        .fadein
        {
            position: relative;
            height: 400px;
            margin: 0 auto;
        }

            .fadein img
            {
                position: absolute;
                left: 0;
                top: 0;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div style="height: 10px;"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="fadein">
                <img src="img/banner/banner1.jpg" width="1160 px">
                <img src="img/banner/banner2.jpg" width="1160 px">
                <img src="img/banner/banner3.jpg" width="1160 px">
                <img src="img/banner/banner4.jpg" width="1160 px">
                <img src="img/banner/banner5.jpg" width="1160 px">
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="span4">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="icon-reorder"></i>
                            Who We Are
                        </h3>
                    </div>
                    <div class="box-content">
                        Lifestyle Therapy &amp; Coaching&nbsp;offers&nbsp;Individuals, Families, Couples and Groups Positive Solutions for Life's Challenges. We want  to help you get where you want to be in your life and relationships. We all have challenges that sometimes are very difficult to get through. Some encouragement or wise advice can make a world of difference in your life.
                        <br />
                        <br />
                        Marriage, family and individual counseling opens the door to a better life and better relationships. Research indicates there are numerous benefits in seeking professional assistance when dealing with many common issues faced by families and individuals.   [<a href="AboutUs.aspx">LEARN MORE</a>]<br />
                        <br />
                        Marriage &amp; Family Therapy is a specialty that sees the world from a systems perspective. We see your problem as not just you but an assortment of events, relationships, and entities historically involved with you. If one particular family member has a problem we will want to see the entire family because we believe that the entire family participates in the problem and can help solve the problem.</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="icon-reorder"></i>
                            Our Services
                        </h3>
                    </div>
                    <div class="box-content">
                        <p>In our practice we see individuals from all walks of life and situations. We believe in offering flexible services that meet our clients needs. We offer  services in our office in Huntsville, Alabama as well a in   local halls, churches, telephone, skype and online. Counseling services are therapeutic in nature and tend to address specific psycho-emotional or relational problems that need a solution. Coaching services are motivational in nature and tend to be focused on helping you do what you already know to do. </p>
                        <p>&nbsp;</p>
                        <p>
                            We often host seminars, conventions, and group mental health screenings. We offer group counseling and online training modules. We also do psychometric testing. 
                        [<a href="OurServices.aspx">LEARN MORE</a>]
                        </p>
                        <p>&nbsp;</p>
                        <p>Our goal is for our clients to feel like coming back to see us again because we have helped them find the solutions thery were looking for and  made them feel welcome.</p>
                        <br />
                        <strong>COUNSELING SERVICES:</strong><br />
                        <a href="depression.aspx" class="alter-link2">Depression</a> : <a href="anxiety.aspx" class="alter-link2">Anxiety</a> : <a href="marriage-counseling.aspx" class="alter-link2">Marriage Counseling</a> : <a href="couples-counselling.aspx" class="alter-link2">Couples Counseling</a> : <a href="family-counselling.aspx" class="alter-link2">Familiy Counseling</a> : <a href="individual-counselling.aspx" class="alter-link2">Individual Counseling</a> : <a href="grief-loss.aspx" class="alter-link2">Grief &amp; Loss</a><a href="young-families.aspx" class="alter-link2"></a> : <a href="sexuality-issues.aspx" class="alter-link2">Sexuality</a> : <a href="addictions-recovery.aspx" class="alter-link2">Addiction Recovery</a>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3>
                            <i class="icon-reorder"></i>
                            Getting Started
                        </h3>
                    </div>
                    <div class="box-content">
                        <p>The first step in getting started is to <a href="Client/Appointment.aspx">set up your appointment</a>.   In many studies the participants  reported that their  anxiety level reduced significantly once they took action to make an appointment. If you have insurance or an EAP you may want to give them a call to see if you need preauthorization and to verify coverage.</p>
                        <p>&nbsp;</p>
                        <p>
                            Next  you should check your email for our confirmation. Be sure to check your spam folder if you don't see it. Put our phone number in your phone and mark your calendar. If you need time off from work be sure to make prior arrangements.  Don't hesitate to call if needed. Our answering service is available 24-7.<br />
                            <br />
                            Finally, please complete our  convenient online intake form so we can be prepared for your visit. [<a href="Client/Appointment.aspx">INTAKE FORM</a>] Read <a href="/faq.html">FAQ</a> or our <a href="GettingStarted.aspx">Getting Started Guide</a> for more info.
                        </p>
                        <p>&nbsp;</p>
                        <p>We look forward to seeing you.</p>
                        <p>&nbsp;</p>
                        <p><strong>OUR ONLINE SERVICES</strong></p>
                        <p><a href="http://www.sizzlinghotmarriage.com" target="_self">Sizzling Hot Marriage</a></p>
                        <p><a href="http://www.recoveringmylife.com" target="_blank">Lifestyle Life Recovery Program</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <a href="http://christiancounselingceu.com/" rel="nofollow">
                <img border="0" src="http://www.lifestyletherapycoach.com/lifeprogram/recovering-mylife/images/other/christiancounselingceu.jpg">
            </a>
            <p align="justify">Need to talk to a <a href="http://www.lifestyletherapycoach.com"><strong>marriage counselor</strong></a> about your marriage or relationship? At Lifestyle Therapy and Coaching we believe in saving marriages and families. Are you experiencing problems with divorce, separation, infidelity, spousal abuse, anxiety, depression, alcohol and drug abuse, domestic violence, parenting, emotional affairs, sex addiction, pornography, anger management, PTSD, sexual problems, debt or financial hardship or the effects of an affair.  Do you want to <a href="http://www.lifestyletherapycoach.com/lifeprogram/recovering-mylife/index.html">recover from addiction</a>? Are you thinking about getting married and are in need of a Christian marriage therapist or coach? No matter where you live <strong>WE CAN HELP!</strong> Our counselors have flexible hours and are available via telephone, Skype and in-person.</p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-envelope"></i>
                        Registration for News latter
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-column form-bordered'>
                        <div class="control-group">
                            <label for="txtEmailAddress" class="control-label">Email Address</label>
                            <div class="controls">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="span4">
                                            <asp:TextBox ID="txtNewsName" runat="server" placeholder="Firstname Lastname" Width="100%"></asp:TextBox>
                                        </div>
                                        <div class="span4">
                                            <div class="input-prepend">
                                                <span class="add-on">@</span>
                                                <asp:TextBox ID="txtEmailAddress" runat="server" placeholder="Email address ex. abc@xyz.com" Width="100%"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="span4">
                                            <asp:Button ID="cmdRegister" runat="server" Text="Register" CssClass="btn btn-primary" OnClick="cmdRegister_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div id="modal-2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-header">
            <h3 id="myModalLabel">WelCome To SizzPro </h3>
        </div>
        <div class="modal-body">
            <p style="text-align: center">
                <strong>
                    This site is under construction , so demo of this site is available here.
                    <br /><br />
                    If you want to continue with us then please press below button .
                    <br /><br />
                    Thanks to Contact Us
                </strong>
            </p>
        </div>
        <div class="modal-footer">
            <a class="btn btn-primary" href="http://lifestyletherapycoach.com/">CONTINUE TO LIFESTYLE-THERAPHY</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $(function () {
            //$("#modal-2").modal('show');
            $('.fadein img:gt(0)').hide();
            setInterval(function () { $('.fadein :first-child').fadeOut().next('img').fadeIn().end().appendTo('.fadein'); }, 10000);
        });
    </script>
</asp:Content>

