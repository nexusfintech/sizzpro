﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Subscription_PackageSubscriptionAddEdit : System.Web.UI.Page
{
    clssubscriptionservices clsSSR = new clssubscriptionservices();
    clsUserspermission clsUSRP = new clsDAL.clsUserspermission();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindadmins();
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
       
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Subscription/PackageSubscriptionMaster.aspx");
    }

    private void bindpackages(Guid User)
    {
        DataTable dtResult = clsSSR.GetNotSubscribedPackages(User);
        if (dtResult != null)
        {
            ddlSelectPackage.Items.Clear();
            ddlSelectPackage.SelectedIndex = -1;
            ddlSelectPackage.SelectedValue = null;
            ddlSelectPackage.ClearSelection();
            ddlSelectPackage.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddlSelectPackage.Items.Add(lst);
            ddlSelectPackage.DataValueField = "srvc_id";
            ddlSelectPackage.DataTextField = "srvc_servicename";
            ddlSelectPackage.DataSource = dtResult;
            ddlSelectPackage.DataBind();
        }
        else
        {
            string msg = clsSSR.GetSetErrorMessage;
            if(msg == "101")
            {
                lblmsg.Text= "Selected admin have subscribed for Sizzpro Suit";
            }
            ddlSelectPackage.Items.Clear();
            ddlSelectPackage.SelectedIndex = -1;
            ddlSelectPackage.SelectedValue = null;
            ddlSelectPackage.ClearSelection();
            ddlSelectPackage.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-No Record Available->>";
            lst.Selected = true;
            ddlSelectPackage.Items.Add(lst);
        }
    }
    private void bindadmins()
    {
        DataTable dtResult = clsUSRP.GetAllAdmin();
        if (dtResult.Rows.Count > 0)
        {
            ddlSelectMember.Items.Clear();
            ddlSelectMember.SelectedIndex = -1;
            ddlSelectMember.SelectedValue = null;
            ddlSelectMember.ClearSelection();
            ddlSelectMember.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddlSelectMember.Items.Add(lst);
            ddlSelectMember.DataValueField = "upm_userid";
            ddlSelectMember.DataTextField = "UserName";
            ddlSelectMember.DataSource = dtResult;
            ddlSelectMember.DataBind();
        }
        else
        {
            ddlSelectMember.Items.Clear();
            ddlSelectMember.SelectedIndex = -1;
            ddlSelectMember.SelectedValue = null;
            ddlSelectMember.ClearSelection();
            ddlSelectMember.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-No Record Available->>";
            lst.Selected = true;
            ddlSelectPackage.Items.Add(lst);
        }
    }
    protected void ddlSelectMember_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindpackages(new Guid(ddlSelectMember.SelectedValue));
    }
}