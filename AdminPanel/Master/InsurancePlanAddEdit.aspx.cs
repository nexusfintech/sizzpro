﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
using System.Data;

public partial class Master_InsurancePlanAddEdit : System.Web.UI.Page
{
    clsInsuranceplanMaster clsIPM = new clsInsuranceplanMaster();
    clsInsuranceCompanyMaster clsICM = new clsInsuranceCompanyMaster();
    clsCPTCodeMaster clsCCM = new clsCPTCodeMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindmaster();
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                loaddata();
            }
        }
    }
    private void loaddata()
    {
        Boolean blnRslt = clsIPM.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {
            txtcoinsurance.Text = clsIPM.insp_coinsurance.ToString();
            txtcopay.Text = clsIPM.insp_copay.ToString();
            txtdeductible.Text = clsIPM.insp_deductible.ToString();
            txtgrpnumber.Text = clsIPM.insp_groupnumber;
            txtname.Text = clsIPM.insp_planname;

            ListItem lstcmpny = ddlInscCmpny.Items.FindByValue(clsIPM.insp_inscmpid.ToString());
            if (lstcmpny != null)
            {
                ddlInscCmpny.SelectedIndex = ddlInscCmpny.Items.IndexOf(lstcmpny);
            }
            if (!clsIPM.insp_coverchiropractor)
            {
                ListItem li = ddlchi.Items.FindByValue("0");
                if (li != null)
                {
                    ddlchi.SelectedIndex = ddlchi.Items.IndexOf(li);
                }
            }
            if (!clsIPM.insp_coverdietition)
            {
                ListItem li = ddldit.Items.FindByValue("0");
                if (li != null)
                {
                    ddldit.SelectedIndex = ddldit.Items.IndexOf(li);
                }
            }
            if (!clsIPM.insp_covermhpo)
            {
                ListItem li = ddlmho.Items.FindByValue("0");
                if (li != null)
                {
                    ddlmho.SelectedIndex = ddlmho.Items.IndexOf(li);
                }
            }
            string[] cptc = clsIPM.insp_cptcodes.Split(',');
            foreach (ListItem item in ddlcptcodes.Items)
            {
                foreach(string s in cptc)
                {
                    if(s == item.Value.ToString())
                    {
                        item.Selected = true;
                    }
                }
                item.Attributes.Add("CssClass", "item");
            }
        }
        else
        {
            MySession.Current.MESSAGE = clsIPM.GetSetErrorMessage;
            Response.Redirect("~/Master/InsurancePlanMaster.aspx");
        }
    }
    private void bindmaster()
    {
        ddlInscCmpny.Items.Clear();
        ddlInscCmpny.SelectedIndex = -1;
        ddlInscCmpny.SelectedValue = null;
        ddlInscCmpny.ClearSelection();
        ddlInscCmpny.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<< -- Pleasse Select -- >>";
        lst.Selected = true;
        ddlInscCmpny.Items.Add(lst);
        ddlInscCmpny.DataSource = clsICM.GetAllRecord();
        ddlInscCmpny.DataTextField = "insc_name";
        ddlInscCmpny.DataValueField = "insc_id";
        ddlInscCmpny.DataBind();

        DataTable dt = clsCCM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);

        foreach(DataRow dr in dt.Rows)
        {
            ListItem li = new ListItem(dr["cd_description"].ToString(), dr["cd_code"].ToString());
            li.Attributes.Add("class", "item");
            ddlcptcodes.Items.Add(li);
        }
    }

    private string validatepage()
    {
        string result = string.Empty;
        if (ddlInscCmpny.SelectedIndex == 0)
        {
            result = "Insurance Company Required";
            return result;
        }
        if (txtname.Text == string.Empty)
        {
            result = "Insurance Name Required";
            return result;
        }
        else if (txtgrpnumber.Text == string.Empty)
        {
            result = "Insurance GroupNumber Required";
            return result;
        }
        else
        {
            return result;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string vldtreslt = validatepage();
        if (vldtreslt != string.Empty)
        {
            lblmessage.Text = vldtreslt;
            msgdiv.Visible = true;
            return;
        }
        clsIPM.insp_inscmpid=Convert.ToInt64(ddlInscCmpny.SelectedValue);
        clsIPM.insp_planname = txtname.Text;
        clsIPM.insp_groupnumber = txtgrpnumber.Text;
        clsIPM.insp_coinsurance = Convert.ToDouble(txtcoinsurance.Text);
        clsIPM.insp_copay = Convert.ToDouble(txtcopay.Text);
        clsIPM.insp_deductible = Convert.ToDouble(txtdeductible.Text);
        clsIPM.insp_userid = (Guid)Membership.GetUser().ProviderUserKey;
        clsIPM.insp_cptcodes = string.Empty;
        foreach (ListItem item in ddlcptcodes.Items)
        {
            if(item.Selected)
            {
                clsIPM.insp_cptcodes = clsIPM.insp_cptcodes + "," + item.Value;   
            }
        }
        clsIPM.insp_cptcodes = clsIPM.insp_cptcodes.TrimStart(',');
        clsIPM.insp_sts = true;
        if(ddlchi.SelectedValue == "1")
        {
            clsIPM.insp_coverchiropractor = true;
        }
        if(ddldit.SelectedValue == "1")
        {
            clsIPM.insp_coverdietition = true;
        }
        if(ddlmho.SelectedValue == "1")
        {
            clsIPM.insp_covermhpo = true;
        }

        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnRslt = clsIPM.Save();
        }
        else
        {
            blnRslt = clsIPM.Update(MySession.Current.EditID);
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/InsurancePlanMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsCCM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/InsurancePlanMaster.aspx");
    }
}