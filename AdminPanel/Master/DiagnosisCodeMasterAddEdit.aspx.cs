﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Master_DiagnosisCodeAddEdit : System.Web.UI.Page
{
    clsDiagnosisCodeMaster clsDCM = new clsDiagnosisCodeMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                loaddata();
            }
        }
    }
    private void loaddata()
    {
        Boolean blnRslt = clsDCM.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {
            txtcode.Text = clsDCM.dcm_code;
            txtdescription.Text = clsDCM.dcm_description;
            txttermdate.Text = clsDCM.dcm_terminateddate.ToShortDateString();
            if(clsDCM.dcm_ispayable)
            {
                chkpayble.Checked = true;
            }
            if(clsDCM.dcm_hcc)
            {
                chkhcc.Checked = true;
            }
        }
        else
        {
            MySession.Current.MESSAGE = clsDCM.GetSetErrorMessage;
            Response.Redirect("~/Master/DiagnosisCodeMaster.aspx");
        }
    }

    private string validatepage()
    {
        string vldtrslt = string.Empty;
        if (txtcode.Text == string.Empty)
        {
            return vldtrslt = "Dignosis Code Required";
        }
        else if (txtdescription.Text == string.Empty)
        {
            return vldtrslt = "Code Description Required";
        }
        else 
        {
            return vldtrslt;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string vldrslt=validatepage();
        if(vldrslt != string.Empty)
        {
            lblmessage.Text = vldrslt;
            msgdiv.Visible = true;
            return;
        }
        clsDCM.dcm_codetype = ddlType.SelectedValue;
        clsDCM.dcm_code = txtcode.Text;
        clsDCM.dcm_description = txtdescription.Text;
        if(txttermdate.Text != string.Empty && txttermdate.Text!= null)
        {
            clsDCM.dcm_terminateddate = Convert.ToDateTime(txttermdate.Text);
        }
        if(chkhcc.Checked)
        {
            clsDCM.dcm_hcc = true;
        }
        if(chkpayble.Checked)
        {
            clsDCM.dcm_ispayable = true;
        }
        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnRslt = clsDCM.Save();
        }
        else
        {
            blnRslt = clsDCM.Update(MySession.Current.EditID);
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/DiagnosisCodeMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsDCM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/DiagnosisCodeMaster.aspx");
    }
}