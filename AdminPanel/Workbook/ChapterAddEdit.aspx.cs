﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;
using System.Web.Security;

public partial class Workbook_ChapterAddEdit : System.Web.UI.Page
{
    clsChapters clsCHP = new clsChapters();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsMembership mem = new clsMembership();
    clsobjectsharing clsOBS = new clsobjectsharing();

    private void EditEntry()
    {
        Boolean blnResult = clsCHP.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnResult == true)
        {
            txtDescription.Text = clsCHP.chp_description;
            txtChapterName.Text = clsCHP.chp_name;
            txtshort.Text = clsCHP.chp_short;
            ListItem liInterval = ddlSelectMember.Items.FindByValue(clsCHP.chp_Userid.ToString());
            if (liInterval != null)
            {
                ddlSelectMember.SelectedIndex = ddlSelectMember.Items.IndexOf(liInterval);
            }
            chpcd.Value = clsCHP.chp_code.ToString();
            chpsts.Value = clsCHP.chp_sts.ToString();
        }
        else { Response.Redirect("~/Workbook/ChapterMaster.aspx"); }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (MySession.Current.AddEditFlag == "null")
            {
                Response.Redirect("~/Workbook/ChapterMaster.aspx");
            }
            if (User.IsInRole("administrator"))
            {
                BindCombo();
            }
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                EditEntry();
            }
        }
    }

    private void BindCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllCombo();
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }

    private string PageValidation()
    {
        string strResult = "";
        if (txtChapterName.Text.Trim() == "")
        { strResult += "Please Input Chapter Name <br/>"; }
        if (txtshort.Text.Trim() == "")
        { strResult += "Please Input Chapter short <br/>"; }
        return strResult;
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Boolean blnSave = false;
        string strResult = PageValidation();
        if (strResult != "")
        { lblmessage.Text = strResult; return; }
        clsCHP.chp_description = txtDescription.Text;
        clsCHP.chp_name = txtChapterName.Text;
        clsCHP.chp_short = txtshort.Text;
        if (User.IsInRole("administrator"))
        {
            clsCHP.chp_Userid = new Guid(ddlSelectMember.SelectedValue.ToString());
        }
        else
        {
            clsCHP.chp_Userid = (Guid)Membership.GetUser().ProviderUserKey;
        }
        clsCHP.chp_OwnerId = mem.SetOwner(clsCHP.chp_Userid);

        if (MySession.Current.AddEditFlag == "ADD")
        {
            clsCHP.chp_code = Guid.NewGuid();
            blnSave = clsCHP.Save();
            if (blnSave == true)
            {
                clsCHP.GetRecordByCodeInProperties(clsCHP.chp_code);
                clsOBS.shr_sharedobjid = clsCHP.chp_id;
                clsOBS.shr_object = "CHAPTER";
                clsOBS.shr_ownerid = clsCHP.chp_Userid;
                clsOBS.shr_sharedwithid = clsCHP.chp_Userid;
                blnSave = clsOBS.Save();
            }
        }
        else
        {
            clsCHP.chp_sts = Convert.ToBoolean(chpsts.Value);
            clsCHP.chp_code = new Guid(chpcd.Value);
            blnSave = clsCHP.Update(MySession.Current.EditID);
        }
        if (blnSave == true)
        {
            Response.Redirect("~/Workbook/ChapterMaster.aspx");
        }
        else { lblmessage.Text = "Error in save ..." + clsCHP.GetSetErrorMessage; }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Workbook/ChapterMaster.aspx");
    }
}