﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_RefundReasonMaster : System.Web.UI.Page
{
    clsRefaundreasonmaster clsRRM = new clsRefaundreasonmaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptPos.DataSource = clsRRM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        rptPos.DataBind();
    }
    private string validatepage()
    {
        string vldtresult = string.Empty;
        if (txtreason.Text == string.Empty)
        {
            return vldtresult = "Reason field Required";
        }
        else
        {
            return vldtresult;
        }
    }
    protected void cmdDlt_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = (LinkButton)sender;
        Boolean blndltrslt = clsRRM.DeleteByPKID(Convert.ToInt64(lnkbtn.CommandArgument));
        if (blndltrslt)
        {
            MySession.Current.MESSAGE = "Record Deleted Successfully";
            Response.Redirect("~/Master/RefundReasonMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsRRM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string validationresult = validatepage();
        if (validationresult != string.Empty)
        {
            lblmessage.Text = validationresult;
            msgdiv.Visible = true;
            return;
        }
        clsRRM.rrm_reason = txtreason.Text.ToString();
        clsRRM.rrm_userid = (Guid)Membership.GetUser().ProviderUserKey;

        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "EDIT")
        {
            blnRslt = clsRRM.Update(MySession.Current.EditID);
            MySession.Current.AddEditFlag = "ADD";
        }
        else
        {
            blnRslt = clsRRM.Save();
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/RefundReasonMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsRRM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
}