﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class existinguserpayment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void cmdLogin_Click(object sender, EventArgs e)
    {
        if(Membership.ValidateUser(txtusername.Text.ToString(), txtpassword.Text.ToString()))
        {
            MembershipUser muser = Membership.GetUser(txtusername.Text.ToString());
            Response.Redirect("~/processpayment.aspx?ui="+muser.ProviderUserKey);
        }
        else
        {
            lblmessage.Text = "Invalid UserName or Password";
        }
    }
}