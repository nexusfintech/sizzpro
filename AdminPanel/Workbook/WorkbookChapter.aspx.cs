﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using clsDAL;

public partial class Workbook_WorkbookChapter : System.Web.UI.Page
{
    clsWorkbookToChapters clsWTC = new clsWorkbookToChapters();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddata();
        }
    }
    public void binddata()
    {
        if (MySession.Current.EditID != 0)
        {
            dtwrkchp.DataSource = clsWTC.GetWorkbookChapter(MySession.Current.EditID);
            dtwrkchp.DataBind();
        }
        else
        {
            Response.Redirect("~/Workbook/ProgrammClient.aspx");
        }
    }
    protected void lnkLsn_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = (LinkButton)sender;
        MySession.Current.EditID = Convert.ToInt64(lnkbtn.CommandArgument);
        Response.Redirect("~/Workbook/ChapterLesson.aspx");
    }
}