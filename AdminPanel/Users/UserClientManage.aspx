﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserClientManage.aspx.cs" Inherits="Users_UserClientManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkCreateNew" runat="server"><i class="glyphicon-user_add"></i> Add New <%= MySession.Current.ClientAlias %></asp:HyperLink>
            </li>
            <li>
                <a href="#"><i class="icon-key"></i>Reset Password</a>
            </li>
            <li>
                <a href="#"><i class="icon-envelope"></i>Send Mail</a>
            </li>
            <li>
                <a href="#"><i class="icon-table"></i>Login History</a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        <%= MySession.Current.ClientAlias %> List
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="dtClientDisp" runat="server" OnItemCommand="dtClientDisp_ItemCommand">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered">
                                <thead>
                                    <tr>
                                        <%--<th>Username</th>--%>
                                        <th>Full Name</th>
                                        <th>Date of Birth</th>
                                        <th>Intake Date</th>
                                        <th>SSN</th>
                                        <th>Email ID</th>
                                        <%--<th>Informed Consent</th>--%>
                                        <th>Assessment</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <%--<td><%# Eval("UserName") %></td>--%>
                                <td><%# Eval("fullname") %></td>
                                <td><%# Eval("prm_dob","{0:MM/dd/yyyy}") %></td>
                                <td><%# Eval("prm_intakedate","{0:MM/dd/yyyy}") %></td>
                                <td><%# Eval("prm_ssn") %></td>
                                <td><%# Eval("prm_emailid") %></td>
                                <%--<td><%# Eval("User_Id") %></td>
                                <td><%# Eval("UserId") %></td>--%>
                              <%--  <td style="text-align: center;">
                                    <asp:Button ID="Button1" runat="server" Text="View" CssClass="btn cancel" CommandName="VIEW" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Userid" ) +"@"+ DataBinder.Eval(Container.DataItem, "User_Id") %>' />
                                </td>--%>
                                <td style="text-align: center;">
                                    <%--<a class="clientmanagelink  btn" id="UserLink" memberkey='<%# Eval("Userid") %>' user_id='<%# Eval("User_Id") %>' href="javascript:void(0);" title="Edit"><i class="icon-edit"></i>&nbsp;Edit</a>--%>
                                    <asp:LinkButton ID="Button2" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Userid" ) +"@"+ DataBinder.Eval(Container.DataItem, "User_Id") %>'><i class="icon-edit"></i> Edit</asp:LinkButton>
                                </td>
                                <td style="text-align: center;">
                                    <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="SESSION" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Userid") %>'><i class="icon-hdd"></i> Session</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                            </table>
                        </FooterTemplate>
                    </asp:Repeater>

                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdmemberkey" Value='<%# Eval("Userid") %>' runat="server" />
    <asp:HiddenField ID="hduserid" Value='<%# Eval("User_Id") %>' runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
    <!-- dataTables -->
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/jquery.dataTables.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/TableTools.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/ColReorderWithResize.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/ColVis.min.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/jquery.dataTables.columnFilter.js") %>'></script>
    <script src='<%= Page.ResolveClientUrl("~/js/plugins/datatable/jquery.dataTables.grouping.js") %>'></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".clientmanagelink").click(function (obj) {
                $("#<%=hdmemberkey.ClientID %>").val($(this).attr("memberkey"));
                $("#<%=hduserid.ClientID %>").val($(this).attr("user_id"));
                $("#form1").submit();
            });
        });
    </script>
</asp:Content>

