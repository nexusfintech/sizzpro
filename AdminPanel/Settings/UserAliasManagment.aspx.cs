﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class Settings_UserAliasManagment : System.Web.UI.Page
{
    clsuserproffessionmapping clsUPM = new clsuserproffessionmapping();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddata();
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        clsUPM.prfm_clientalias = txtClientAlias.Text.Trim();
        clsUPM.prfm_useralias = txtUserAlias.Text.Trim();
        clsUPM.prfm_memberalias = txtMemberAlias.Text.Trim();
        bool updated = clsUPM.Update((Guid)Membership.GetUser().ProviderUserKey);
        if(updated)
        {
            MySession.Current.MemberAlias = clsUPM.prfm_memberalias;
            MySession.Current.UserAlias = clsUPM.prfm_useralias;
            MySession.Current.ClientAlias = clsUPM.prfm_clientalias;
            lblMessage.Text = "Alias Saved SuccessFully";
            lblMessage.Visible = true;
        }
    }

    public void binddata()
    {
        bool getrec = clsUPM.GetRecordByUserIdInProperties((Guid)Membership.GetUser().ProviderUserKey);
        if (getrec)
        {
            txtMemberAlias.Text = clsUPM.prfm_memberalias;
            txtUserAlias.Text = clsUPM.prfm_useralias;
            txtClientAlias.Text = clsUPM.prfm_clientalias;
        }
    }
}
