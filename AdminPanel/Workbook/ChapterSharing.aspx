﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ChapterSharing.aspx.cs" Inherits="Workbook_ChapterSharing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkMappingtoWB" runat="server" NavigateUrl="~/Workbook/ChapterMaster.aspx"><i class="glyphicon-retweet_2"></i>&nbsp;Chapters</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span6">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Chapters & Sharing
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="dtSharing" runat="server">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Shared With</th>
                                        <%--<th style="text-align: center;">Remove</th>--%>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("UserName") %></td>
                                <%-- <td style="text-align: center">
                                <asp:LinkButton ID="cmdRemove" CommandName="REMOVE" CssClass="btn btn-inverse" runat="server" OnClick="cmdRemove_Click" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "shr_id") %>'><i class="icon-trash"></i> &nbsp;Remove</asp:LinkButton>
                            </td>--%>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="glyphicon-retweet"></i>Chapter Sharing</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <div class="control-group">
                            <label for="txtCode" class="control-label">Select User</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddladmins" runat="server" CssClass="select2-me input-large"></asp:DropDownList>
                                <span class="help-block">Select User to Share Chapter With </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Type</label>
                            <div class="controls">

                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem>FREE</asp:ListItem>
                                    <asp:ListItem>PAID</asp:ListItem>
                                </asp:DropDownList>
                                <span class="help-block">which way you want to share</span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" runat="server" CssClass="btn btn-primary" OnClick="cmdSave_Click"><i class="glyphicon-ok_2"></i> Share</asp:LinkButton>
                            &nbsp;
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

