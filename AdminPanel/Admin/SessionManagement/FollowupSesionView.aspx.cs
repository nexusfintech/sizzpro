﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;

public partial class Admin_SessionManagement_FollowupSesionView : System.Web.UI.Page
{

    clsClientFollowUpSession clsCFS = new clsClientFollowUpSession();
    clsFollowupDignosisProcedure clsFDP = new clsFollowupDignosisProcedure();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if(Request.QueryString["tag"] != null && Request.QueryString["tag"] != string.Empty  )
            {
                if (Request.QueryString["tag"].ToString() == "claim")
                {
                    
                }
            }
            if (Session["sessionid"] != null)
            {
                binddata(new Guid(Session["sessionid"].ToString()));
            }
            else
            {
                Response.Redirect("~/Admin/SessionManagement/FollowupSession.aspx");
            }
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void binddata(Guid SessionId)
    {
        DataTable dtresult = clsCFS.GetAllRecordBySession(SessionId);
        if(dtresult.Rows.Count > 0)
        {
            rptDgndtail.DataSource = clsFDP.GetAllRecordBySession(SessionId);
            rptDgndtail.DataBind();

            DataRow dr = dtresult.Rows[0];
            lblpataintname.Text = dr["Clientname"].ToString();
            lbladjustment.Text =String.Format("{0:C}",dr["fsb_writeoff"]);
            lblbaldue.Text = String.Format("{0:C}", dr["fsb_balancedue"]);
            lblclientname.Text = dr["Clientname"].ToString();
            lblclientpaid.Text =String.Format("{0:C}",dr["fsb_clientpayment"]);
            lbldate.Text = Convert.ToDateTime(dr["cfs_sessiondate"]).ToString("MM/dd/yyyy");
            lblfcltyphone.Text = dr["fsl_phone"].ToString();
            lblinsurancepaid.Text = String.Format("{0:C}", dr["fsb_insurancepayment"].ToString());
            lbllocation.Text = dr["pos_place"].ToString();
            lblnextdate.Text = Convert.ToDateTime(dr["cfs_nextappoinment"]).ToString("MM/dd/yyyy");
            lblfclty.Text = dr["fsl_name"].ToString();
            lblfcltyaddress.Text = dr["fsl_address"].ToString();
            lblfcltyphone.Text = dr["fsl_phone"].ToString();
            lblinsaddress.Text = dr["insc_address"].ToString();
            lblinsname.Text = dr["insc_name"].ToString();
            lblinsphone.Text = dr["insc_phone"].ToString();
            lbltotalcharge.Text =  String.Format("{0:C}",dr["fsb_totalcharged"]);
            lbltotalreceved.Text = String.Format("{0:C}", dr["fsb_totalrecieved"]);
            lblttlcharge.Text =String.Format("{0:C}", dr["fsb_totalcharged"]);
            ltlbeahviour.Text = dr["o_behaviour"].ToString();
            ltlinterventation.Text = dr["a_intervatation"].ToString();
            ltlrlresp.Text = dr["o_reasponse"].ToString();
            ltltreatmentplan.Text = dr["p_treatmentplan"].ToString();
            ltlvisitreson.Text = dr["s_reason"].ToString();
            ltlprogress.Text = dr["p_progresstogoal"].ToString();
            ltlnewclinicissue.Text = dr["p_newclinicissue"].ToString();
        }
    }
    protected void cmdReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/SessionManagement/FollowupSession.aspx");
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {

    }
    protected void cmdClmSbmt_Click(object sender, EventArgs e)
    {

    }
    protected void cmdclaim_Click(object sender, EventArgs e)
    {

    }
}