﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="HealthPlanMaster.aspx.cs" Inherits="Master_HealthPlanMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn" Text="Add New Plan" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        health Plan List
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptPayer" runat="server" OnItemCommand="rptPayer_ItemCommand" OnItemDataBound="rptPayer_ItemDataBound">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Payer Id</th>
                                        <th>Payer Name</th>
                                        <th>ENR</th>
                                        <th>TYPE</th>
                                        <th>LOB</th>
                                        <th>RTE1</th>
                                        <th>RTE2</th>
                                        <th>RTS</th>
                                        <th>ERA</th>
                                        <th>SEC</th>
                                        <th>NOTE</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("pm_Payerid") %></td>
                                <td><%# Eval("pm_PayerName") %></td>
                                <td class="qty" style="text-align:center">
                                    <asp:Label ID="lblenr" runat="server" CssClass="label label-blue"></asp:Label>
                                </td>
                                <td><%# Eval("pm_TYP") %></td>
                                <td><%# Eval("pm_LOB") %></td>
                                <td class="qty" style="text-align:center">
                                    <asp:Label ID="lblrte1" runat="server" CssClass="label label-blue"></asp:Label>
                                </td>
                                <td class="qty" style="text-align:center">
                                    <asp:Label ID="lblrte2" runat="server" CssClass="label label-blue"></asp:Label>
                                </td>
                                <td class="qty" style="text-align:center">
                                    <asp:Label ID="lblrts" runat="server" CssClass="label label-blue"></asp:Label>
                                </td>
                                <td class="qty" style="text-align:center">
                                    <asp:Label ID="lblera" runat="server" CssClass="label label-blue"></asp:Label>
                                </td>
                                <td class="qty" style="text-align:center">
                                    <asp:Label ID="lblsec" runat="server" CssClass="label label-blue"></asp:Label>
                                </td>
                                <td class="qty" style="text-align:center">
                                    <a href="javascript:void(0);" class="btn btn-primary" rel="popover" title="PAYER NOTE" data-placement="top" data-content="<%# Eval("pm_note") %>">#</a>
                                </td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "pm_id") %>'><i class="icon-edit-sign"></i> &nbsp;Edit</asp:LinkButton>
                                    <asp:LinkButton ID="cmdDlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "pm_id") %>' rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i> </asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>

