﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAssessmentProblem.ascx.cs" Inherits="UserControl_Assessment_ucAssessmentProblem" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-remove-sign"></i>
                    Problems
                </h3>
            </div>
            <div class="box-content">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="span12">
                        <label>Please state in your own words the nature of your main problem(s)</label>
                        <asp:TextBox ID="txtmainproblems" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>State in your words the reasons for which you are requesting help. (One simple statement)</label>
                        <asp:TextBox ID="txtrequesthelp" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>How upset are you right now about your problem(s)?</label>
                        <asp:RadioButtonList ID="rdoupsetstatus" runat="server" RepeatDirection="Horizontal" CellPadding="5">
                            <asp:ListItem Value="1" Text="Mildly"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Moderately"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Very"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Extremely"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Totally"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="span12">
                        <label>Please provide some background information about your problem</label>
                        <asp:TextBox ID="txtproblembackground" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Significant personal and family history that help to understand the problem</label>
                        <asp:TextBox ID="txtproblemsignificant" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Give approximate dates and events surrounding the beginning of your problem(s)</label>
                        <asp:TextBox ID="txtproblemapproximate" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>What are some of the agencies or outside people involved in your problem?</label>
                        <asp:TextBox ID="txtproblemotheragencies" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>What are some of the things you have done to try to resolve this problem?</label>
                        <asp:TextBox ID="txtproblemtryresolve" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>What are some of your greatest strengths that can help you with this problem?</label>
                        <asp:TextBox ID="txtproblemgretstrenghts" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>What are some of your greatest weaknesses that have contributed to this problem?</label>
                        <asp:TextBox ID="txtproblemgretweakness" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>What do you hope to get out of coming to therapy? What do you hope to accomplish?</label>
                        <asp:TextBox ID="txtproblemhopegettherapy" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>How will you know you are feeling better?</label>
                        <asp:TextBox ID="txtproblemknowbetter" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Who will be the first to notice you are feeling better?</label>
                        <asp:TextBox ID="txtproblembetternoties" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>What will you do when you are feeling good?</label>
                        <asp:TextBox ID="txtproblemdofellbetter" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>In a few words, what do you think therapy is about? </label>
                        <asp:TextBox ID="txtproblefewwortherapy" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>How long do you think therapy should last?</label>
                        <asp:TextBox ID="txtproblemlasttherapy" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <div class="span6">
                            <label>Have you previously been in counseling? </label>
                        </div>
                        <div class="span6">
                            <asp:RadioButtonList ID="rdoproblempreviconsu" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="2" Text="No"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span6">
                            <div class="control-group">
                                <label for="txtprolemwithwhom" class="control-label">With whom?</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtprolemwithwhom" runat="server" placeholder="With whom?" class="complexify-me"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                             <div class="control-group">
                                <label for="txtproblemwhatdate" class="control-label">What dates?</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtproblemwhatdate" runat="server" placeholder="What dates?" class="complexify-me"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <label>What was the outcome?</label>
                        <asp:TextBox ID="txtptoblemoutcome" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>How would you want this experience to be different?</label>
                        <asp:TextBox ID="txtproblemexprience" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
