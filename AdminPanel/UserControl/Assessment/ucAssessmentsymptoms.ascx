﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAssessmentsymptoms.ascx.cs" Inherits="UserControl_Assessment_ucAssessmentsymptoms" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-github-alt"></i>
                    Mental Health
                </h3>
            </div>
            <div class="box-content nopadding">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlEpilepsy" class="control-label">Epilepsy</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlEpilepsy" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlHighbloodpressure" class="control-label">High blood pressure</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlHighbloodpressure" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAlcoholproblems" class="control-label">Alcohol problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAlcoholproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlDrugproblems" class="control-label">Drug problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlDrugproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlUnusualphysicalsymptoms" class="control-label">Unusual physical symptoms</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlUnusualphysicalsymptoms" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlMentalillness" class="control-label">Mental illness</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlMentalillness" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAnxiety" class="control-label">Anxiety</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAnxiety" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlDepression" class="control-label">Depression</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlddlDepression" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlStressedout" class="control-label">Stressed out</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStressedout" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlFinancialproblems" class="control-label">Financial problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlFinancialproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAbusiverelationship" class="control-label">Abusive relationship</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlddlAbusiverelationship" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlEvilthoughts" class="control-label">Evil thoughts</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlEvilthoughts" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlLonely" class="control-label">Lonely</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlLonely" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlLegalproblems" class="control-label">Legal problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlLegalproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlCareerproblems" class="control-label">Career problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlCareerproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlUnhappiness" class="control-label">Unhappiness</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlUnhappiness" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlMaritaldifficulties" class="control-label">Marital difficulties</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlMaritaldifficulties" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAnger" class="control-label">Anger</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAnger" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlSexualproblems" class="control-label">Sexual problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlSexualproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlInferiorityfeelings" class="control-label">Inferiority feelings</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlInferiorityfeelings" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlParentingproblems" class="control-label">Parenting problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlParentingproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlTirednessLessEnergy" class="control-label">Tiredness/Less Energy</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlTirednessLessEnergy" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlGuilt" class="control-label">Guilt</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlGuilt" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlFear" class="control-label">Fear</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlFear" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlStomachproblems" class="control-label">Stomach problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStomachproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlSchoolproblems" class="control-label">School problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlSchoolproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlChildren" class="control-label">Children</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlChildren" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlDivorce" class="control-label">Divorce</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlDivorce" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlFamilyproblems" class="control-label">Family problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlFamilyproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAmbition" class="control-label">Ambition</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAmbition" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAbortion" class="control-label">Abortion</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAbortion" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlFriends" class="control-label">Friends</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlFriends" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAppetite" class="control-label">Appetite</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAppetite" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlWorkdissatisfaction" class="control-label">Work dissatisfaction</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlWorkdissatisfaction" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlShyness" class="control-label">Shyness</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlShyness" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlDifficultymakingdecisions" class="control-label">Difficulty making decisions</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlDifficultymakingdecisions" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlHeadaches" class="control-label">Headaches</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlHeadaches" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlMemory" class="control-label">Memory</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlMemory" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlsymptom1" class="control-label">Insomnia/Sleeping Problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="DropDownList38" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlOvereating" class="control-label">Overeating</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlOvereating" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlNervousness" class="control-label">Nervousness</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlNervousness" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlEatingproblems" class="control-label">Eating problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlEatingproblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlTakingtoomanyrisks" class="control-label">Taking too many risks</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlTakingtoomanyrisks" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlProcrastinating" class="control-label">Procrastinating</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlProcrastinating" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlTakingdrugs" class="control-label">Taking drugs</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlTakingdrugs" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAggressivebehavior" class="control-label">Aggressive behavior</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAggressivebehavior" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlLazy" class="control-label">Lazy</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlLazy" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlWorkingtoohard" class="control-label">Working too hard</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlWorkingtoohard" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlDrinkingtoomuch" class="control-label">Drinking too much</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlDrinkingtoomuch" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlActingImpulsively" class="control-label">Acting Impulsively</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlActingImpulsively" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlConcentrationdifficulties" class="control-label">Concentration difficulties</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlConcentrationdifficulties" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlSmoking" class="control-label">Smoking</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlSmoking" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlOddbehavior" class="control-label">Odd behavior</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlOddbehavior" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlNervoustic" class="control-label">Nervous tic</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlNervoustic" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlCompulsionorobsession" class="control-label">Compulsion or obsession</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlCompulsionorobsession" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlSuicidethoughtsattempts" class="control-label">Suicide thoughts/attempts</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlSuicidethoughtsattempts" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlCantkeepajob" class="control-label">Cant keep a job</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlCantkeepajob" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlPhobicavoidance" class="control-label">Phobic avoidance</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlPhobicavoidance" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlVomiting" class="control-label">Vomiting</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlVomiting" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlVictimofaCrime" class="control-label">Victim of a Crime</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlVictimofaCrime" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlSleepDisturbance" class="control-label">Sleep Disturbance</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlSleepDisturbance" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlWithdrawal" class="control-label">Withdrawal</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlWithdrawal" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlBaddreams" class="control-label">Bad dreams</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlBaddreams" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlLossofcontrol" class="control-label">Loss of control</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlLossofcontrol" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlOutburstsoftemper" class="control-label">Outbursts of temper</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlOutburstsoftemper" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAccident" class="control-label">Accident</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAccident" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlNausea" class="control-label">Nausea</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlNausea" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlDeployment" class="control-label">Deployment</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlDeployment" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlStrangeorunusualsensations" class="control-label">Strange or unusual sensations</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStrangeorunusualsensations" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlSelfesteemconfidence" class="control-label">Self-esteem/confidence</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlSelfesteemconfidence" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlDomesticViolence" class="control-label">Domestic Violence</label>
                                <div class="controls">
                                    <asp:DropDownList ID="DropDownList70" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlBadIllness" class="control-label">Bad Illness</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlBadIllness" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
