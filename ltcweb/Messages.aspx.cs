﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Messages : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["msg_boxheader"] != null)
        {
            lblboxheader.Text = Session["msg_boxheader"].ToString();
            ltrmessage.Text = Session["msg_message"].ToString();
        }
        else { Response.Redirect("Default.aspx"); }

    }
}