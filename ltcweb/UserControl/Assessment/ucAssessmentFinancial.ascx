﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAssessmentFinancial.ascx.cs" Inherits="UserControl_Assessment_ucInsurance" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-user"></i>
                    Financial Information
                </h3>
            </div>
            <div class="box-content nopadding">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="control-group">
                        <label for="txtEmployeer" class="control-label">Employer Name</label>
                        <div class="controls">
                            <asp:TextBox ID="txtEmployeer" runat="server" placeholder="Employer Name" data-rule-required="true" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtINSCompany" class="control-label">Insurance Company</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlInsurance" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtEffDate" class="control-label">Effective Date</label>
                        <div class="controls">
                            <asp:TextBox ID="txtEffDate" runat="server" CssClass="input-medium datepick" value="02/16/12"></asp:TextBox>
                            <span class="help-block">Enter Date in MM/DD/YYYY format.</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtINSSubscriber" class="control-label">Insurance Subscriber</label>
                        <div class="controls">
                            <asp:TextBox ID="txtINSSubscriber" runat="server" placeholder="Insurance Subscriber" data-rule-required="true" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtINSSubscriberID" class="control-label">Subscriber ID</label>
                        <div class="controls">
                            <asp:TextBox ID="txtINSSubscriberID" runat="server" placeholder="Subscriber ID" data-rule-required="true" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtINSSubscriberGrpNO" class="control-label">Insurance Group Number</label>
                        <div class="controls">
                            <asp:TextBox ID="txtINSSubscriberGrpNO" runat="server" placeholder="Insurance Group Number" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtcoinspay" class="control-label">Co-Pay</label>
                        <div class="controls">
                            <asp:TextBox ID="txtcoinspay" runat="server" placeholder="Co-Pay" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtcoinspay" class="control-label">Insurance Pay</label>
                        <div class="controls">
                            <asp:TextBox ID="txtInsurancePay" runat="server" placeholder="Insurance Pay" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtSubDOB" class="control-label">Subscriber DOB</label>
                        <div class="controls">
                            <asp:TextBox ID="txtSubDOB" runat="server" CssClass="input-medium datepick" value="02/16/12"></asp:TextBox>
                            <span class="help-block">Enter Date in MM/DD/YYYY format.</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtInsPhoneNo" class="control-label">Ins. Phone</label>
                        <div class="controls">
                            <asp:TextBox ID="txtInsPhoneNo" runat="server" placeholder="Phone No" class="complexify-me mask_phone"></asp:TextBox>
                            <span class="help-block">Format: (999) 999-9999</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtsubrelation" class="control-label">Your relationship to Subscriber</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlRelation" runat="server">
                                <asp:ListItem Text="Spouse" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Child"></asp:ListItem>
                                <asp:ListItem Text="Father"></asp:ListItem>
                                <asp:ListItem Text="Mother"></asp:ListItem>
                                <asp:ListItem Text="Brother"></asp:ListItem>
                                <asp:ListItem Text="Sister"></asp:ListItem>
                                <asp:ListItem Text="Father In-Low"></asp:ListItem>
                                <asp:ListItem Text="Mother In-Low"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtAuthoraztion" class="control-label">Authorization</label>
                        <div class="controls">
                            <asp:TextBox ID="txtAuthoraztion" runat="server" placeholder="Authorization" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="ddlobtauth" class="control-label">Have you obtained prior authorization? </label>
                        <div class="controls">
                            <asp:RadioButtonList ID="ddlobtauth" runat="server" RepeatDirection="Horizontal" CellPadding="10">
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Not Required"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="chkpayself" class="control-label"></label>
                        <div class="controls">
                            <div class="check-line">
                                <asp:CheckBox ID="chkpayself" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                                <label class='inline'>I recognize that if my insurance does not pay this claim I am responsible for all charges?</label>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="chkprimary" class="control-label"></label>
                        <div class="controls">
                            <div class="check-line">
                                <asp:CheckBox ID="chkprimary" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                                <label class='inline'>I want to set this insurance as primary</label>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtNote" class="control-label">Note</label>
                        <div class="controls">
                            <asp:TextBox ID="txtNote" runat="server" placeholder="Description" TextMode="MultiLine" Width="98%" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-actions">
                        <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <asp:Repeater ID="dtInsurance" runat="server" OnItemCommand="dtInsurance_ItemCommand">
            <HeaderTemplate>
                <table class="table table-hover table-nomargin dataTable table-bordered">
                    <thead>
                        <tr>
                            <th>Employer Name</th>
                            <th>Insurance Company</th>
                            <th>Effective Date</th>
                            <th>Subscriber</th>
                            <th>Subscriber Id</th>
                            <th>Ground Number</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("ins_employer") %></td>
                    <td><%# Eval("inr_insurancename") %></td>
                    <td><%# Eval("ins_effectdate","{0:MM/dd/yyyy}") %></td>
                    <td><%# Eval("ins_subscriber") %></td>
                    <td><%# Eval("ins_subscid") %></td>
                    <td><%# Eval("ins_insugroupnumber") %></td>
                    <td>
                        <i class="icon-edit">
                            <asp:Button ID="cmdEdit" runat="server" Text="Edit" CssClass="btn cancel" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ins_id") %>' />
                        </i>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
