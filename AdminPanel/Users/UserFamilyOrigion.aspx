﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserFamilyOrigion.aspx.cs" Inherits="Users_UserFamilyOrigion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <UC:AssessmentStepLinks ID="ucasslinks" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-group"></i>
                Client's Family Origion
            </h3>
        </div>
        <div class="box-content nopadding">
            <ul class="tabs tabs-inline tabs-top">
                <li class='active'>
                    <a href="#parent" data-toggle='tab'><i class="glyphicon-parents"></i>Parents</a>
                </li>
                <li>
                    <a href="#maternal" data-toggle='tab'><i class="glyphicon-parents"></i>Maternal</a>
                </li>
                <li>
                    <a href="#paternal" data-toggle='tab'><i class="glyphicon-parents"></i>Paternal</a>
                </li>
            </ul>
            <div class="row-fluid">
                <h4>
                    <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                </h4>
            </div>
            <div class="tab-content padding tab-content-inline tab-content-bottom">
                <div class="tab-pane active" id="parent">
                    <UC:UserFamilyOrigion ID="ucFather" runat="server" HeaderName="Father" RelationFlag="FATHER" />
                    <UC:UserFamilyOrigion ID="ucMother" runat="server" HeaderName="Mother" RelationFlag="MOTHER" />
                </div>
                <div class="tab-pane" id="maternal">
                    <UC:UserFamilyOrigion ID="ucMatMother" runat="server" HeaderName="Maternal Grand Mother" RelationFlag="MATMOTHER" />
                    <UC:UserFamilyOrigion ID="ucMatFather" runat="server" HeaderName="Maternal Grand Father" RelationFlag="MATMOTHER" />
                </div>
                <div class="tab-pane" id="paternal">
                    <UC:UserFamilyOrigion ID="ucPatMother" runat="server" HeaderName="Paternal Grand Mother" RelationFlag="PATMOTHER" />
                    <UC:UserFamilyOrigion ID="ucPatFather" runat="server" HeaderName="Paternal Grand Father" RelationFlag="PATFATHER" />
                </div>

            </div>
            <div class="span12">
                <div class="form-actions">
                    <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                    <asp:Button ID="cmdReschedule" runat="server" CssClass="btn btn-primary" Text="Re-Assesment" Visible="false" OnClick="cmdReschedule_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

