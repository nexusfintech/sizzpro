﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsChapters
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsChapters()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsChapters()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _chp_id; }
            set
            {
                _chp_id = value;
                if (clsCMD.Parameters.Contains("@chp_id"))
                { clsCMD.Parameters["@chp_id"].Value = value; }
                else { clsCMD.Parameters.Add("@chp_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _chp_id;
        public Int64 chp_id
        {
            get { return _chp_id; }
            set
            {
                _chp_id = value;
                if (clsCMD.Parameters.Contains("@chp_id"))
                { clsCMD.Parameters["@chp_id"].Value = value; }
                else { clsCMD.Parameters.Add("@chp_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _chp_code;
        public Guid chp_code
        {
            get { return _chp_code; }
            set
            {
                _chp_code = value;
                if (clsCMD.Parameters.Contains("@chp_code"))
                { clsCMD.Parameters["@chp_code"].Value = value; }
                else { clsCMD.Parameters.Add("@chp_code", SqlDbType.UniqueIdentifier, 50).Value = value; }
            }
        }
        private String _chp_name;
        public String chp_name
        {
            get { return _chp_name; }
            set
            {
                _chp_name = value;
                if (clsCMD.Parameters.Contains("@chp_name"))
                { clsCMD.Parameters["@chp_name"].Value = value; }
                else { clsCMD.Parameters.Add("@chp_name", SqlDbType.NVarChar, 1024).Value = value; }
            }
        }
        private String _chp_short;
        public String chp_short
        {
            get { return _chp_short; }
            set
            {
                _chp_short = value;
                if (clsCMD.Parameters.Contains("@chp_short"))
                { clsCMD.Parameters["@chp_short"].Value = value; }
                else { clsCMD.Parameters.Add("@chp_short", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _chp_description;
        public String chp_description
        {
            get { return _chp_description; }
            set
            {
                _chp_description = value;
                if (clsCMD.Parameters.Contains("@chp_description"))
                { clsCMD.Parameters["@chp_description"].Size = value.Length; clsCMD.Parameters["@chp_description"].Value = value; }
                else { clsCMD.Parameters.Add("@chp_description", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Guid _chp_Userid;
        public Guid chp_Userid
        {
            get { return _chp_Userid; }
            set
            {
                _chp_Userid = value;
                if (clsCMD.Parameters.Contains("@chp_Userid"))
                { clsCMD.Parameters["@chp_Userid"].Value = value; }
                else { clsCMD.Parameters.Add("@chp_Userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _chp_OwnerId;
        public Guid chp_OwnerId
        {   
            get { return _chp_OwnerId; }
            set
            {
                _chp_OwnerId = value;
                if (clsCMD.Parameters.Contains("@chp_OwnerId"))
                { clsCMD.Parameters["@chp_OwnerId"].Value = value; }
                else { clsCMD.Parameters.Add("@chp_OwnerId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Boolean _chp_sts;
        public Boolean chp_sts
        {
            get { return _chp_sts; }
            set
            {
                _chp_sts = value;
                if (clsCMD.Parameters.Contains("@chp_sts"))
                { clsCMD.Parameters["@chp_sts"].Value = value; }
                else { clsCMD.Parameters.Add("@chp_sts", SqlDbType.Bit).Value = value; }
            }
        }
        private Int64 _chp_version;
        public Int64 chp_version
        {
            get { return _chp_version; }
            set
            {
                _chp_version = value;
                if (clsCMD.Parameters.Contains("@chp_version"))
                { clsCMD.Parameters["@chp_version"].Value = value; }
                else { clsCMD.Parameters.Add("@chp_version", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                chp_id = Convert.ToInt64(dr["chp_id"]);
                chp_code = new Guid(dr["chp_code"].ToString());
                chp_name = Convert.ToString(dr["chp_name"]);
                chp_short = Convert.ToString(dr["chp_short"]);
                chp_description = Convert.ToString(dr["chp_description"]);
                chp_Userid = new Guid(dr["chp_Userid"].ToString());
                chp_OwnerId = new Guid(dr["chp_OwnerId"].ToString());
                chp_version = Convert.ToInt64(dr["chp_version"]);
                chp_sts = Convert.ToBoolean(dr["chp_sts"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            chp_id = Convert.ToInt64("0");
            chp_code = new Guid();
            chp_name = Convert.ToString("NA");
            chp_short = Convert.ToString("NA");
            chp_description = Convert.ToString("NA");
            chp_Userid = new Guid();
            chp_OwnerId = new Guid();
            chp_version = 0;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.chp_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "chaptersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }
        public Boolean Update(Int64 chp_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.chp_id = chp_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "chaptersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }
        public Boolean DeleteByPKID(Int64 chp_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.chp_id = chp_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "chaptersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }
        public Boolean ChangeStatus(Int64 chp_id, Boolean status,Int64 ver)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.chp_id = chp_id;
            this.chp_sts = status;
            this.chp_version = ver;
            this.AddEditDeleteFlag = "CHNGESTS";
            try
            {
                clsCMD.CommandText = "chaptersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "chaptersGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByMember(Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "chaptersGetData";
                this.chp_Userid = MemberId;
                SetGetSPFlag = "ALLBYMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByUser(Guid UserId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "chaptersGetData";
                this.chp_Userid = UserId;
                SetGetSPFlag = "ALLBYUSER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllNotMapped()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "chaptersGetData";
                SetGetSPFlag = "NOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllNotMappedByMember(Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "chaptersGetData";
                this.chp_Userid = MemberId;
                SetGetSPFlag = "NOTMAPPEDBYMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetByPrimaryKeyID(Int64 intPKID)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.chp_id = intPKID;
                clsCMD.CommandText = "chaptersGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsChapters_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsChapters_PropertiesList> lstResult = new List<clsChapters_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].chp_id = Convert.ToInt64(dr["chp_id"]);
                lstResult[i].chp_name = Convert.ToString(dr["chp_name"]);
                lstResult[i].chp_short = Convert.ToString(dr["chp_short"]);
                lstResult[i].chp_description = Convert.ToString(dr["chp_description"]);
                lstResult[i].chp_code = new Guid(dr["chp_code"].ToString());
                lstResult[i].chp_Userid = new Guid(dr["chp_Userid"].ToString());
                lstResult[i].chp_OwnerId = new Guid(dr["chp_OwnerId"].ToString());
                lstResult[i].chp_sts = Convert.ToBoolean(dr["chp_sts"]);
                lstResult[i].chp_version = Convert.ToInt64(dr["chp_version"]);
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 chp_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.chp_id = chp_id;
                clsCMD.CommandText = "chaptersGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByCodeInProperties(Guid chpcode)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.chp_code = chpcode;
                clsCMD.CommandText = "chaptersGetData";
                SetGetSPFlag = "BYCODE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }
        public Boolean CheckDuplicateRow(Guid chp_code)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.chp_code = chp_code;
                clsCMD.CommandText = "chaptersGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Int64 GetChapterVersion(Guid Code)
        {
            OpenConnection();
            Int64 Version = 0;
            try
            {
                this.chp_code = Code;
                clsCMD.CommandText = "chaptersGetData";
                SetGetSPFlag = "FINDVER";
                Version = Convert.ToInt64(clsCMD.ExecuteScalar());

            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                Version = -1;
            }
            CloseConnection();
            return Version;
        }
        #endregion
    }

    public class clsChapters_PropertiesList
    {
        public Int64 chp_id { get; set; }
        public Guid chp_code { get; set; }
        public String chp_name { get; set; }
        public String chp_short { get; set; }
        public String chp_description { get; set; }
        public Guid chp_Userid { get; set; }
        public Guid chp_OwnerId { get; set; }
        public Boolean chp_sts { get; set; }
        public Int64 chp_version { get; set; }
    }
}