﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class shpweb_LifeProgram_Workbook : System.Web.UI.Page
{
    clsDAL.clsWorkBook clsWB = new clsDAL.clsWorkBook();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (hidden.Value != "")
        {
            Session["chapterid"] = hidden.Value;
            Response.Redirect("~/LifeProgram/Chapters.aspx");
            return;
        }

        if (!IsPostBack)
        {
            _GridFill();
        }
    }

    void _GridFill()
    {
        int intwbid = Convert.ToInt32(Session["workbookid"]);
        if (intwbid == 0)
        {
            Response.Redirect("~/Client/DashBoard.aspx");
            return;
        }
        rptWorkBook.DataSource = clsWB.GetAllRecordByPKID(intwbid);
        rptWorkBook.DataBind();
    }
}