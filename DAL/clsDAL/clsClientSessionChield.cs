﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsClientSessionChield
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsClientSessionChield()
        {
            clsCMD.Parameters.Clear();
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsClientSessionChield()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _csc_id; }
            set
            {
                _csc_id = value;
                if (clsCMD.Parameters.Contains("@csc_id"))
                { clsCMD.Parameters["@csc_id"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _csc_id;
        public Int64 csc_id
        {
            get { return _csc_id; }
            set
            {
                _csc_id = value;
                if (clsCMD.Parameters.Contains("@csc_id"))
                { clsCMD.Parameters["@csc_id"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _csc_ssnid;
        public Int64 csc_ssnid
        {
            get { return _csc_ssnid; }
            set
            {
                _csc_ssnid = value;
                if (clsCMD.Parameters.Contains("@csc_ssnid"))
                { clsCMD.Parameters["@csc_ssnid"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_ssnid", SqlDbType.BigInt).Value = value; }
            }
        }
        private DateTime _csc_date;
        public DateTime csc_date
        {
            get { return _csc_date; }
            set
            {
                _csc_date = value;
                if (clsCMD.Parameters.Contains("@csc_date"))
                { clsCMD.Parameters["@csc_date"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_date", SqlDbType.DateTime).Value = value; }
            }
        }
        private Guid _csc_clientid;
        public Guid csc_clientid
        {
            get { return _csc_clientid; }
            set
            {
                _csc_clientid = value;
                if (clsCMD.Parameters.Contains("@csc_clientid"))
                { clsCMD.Parameters["@csc_clientid"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_clientid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _csc_itemid;
        public Int64 csc_itemid
        {
            get { return _csc_itemid; }
            set
            {
                _csc_itemid = value;
                if (clsCMD.Parameters.Contains("@csc_itemid"))
                { clsCMD.Parameters["@csc_itemid"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_itemid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _csc_fslid;
        public Int64 csc_fslid
        {
            get { return _csc_fslid; }
            set
            {
                _csc_fslid = value;
                if (clsCMD.Parameters.Contains("@csc_fslid"))
                { clsCMD.Parameters["@csc_fslid"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_fslid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _csc_proid;
        public Int64 csc_proid
        {
            get { return _csc_proid; }
            set
            {
                _csc_proid = value;
                if (clsCMD.Parameters.Contains("@csc_proid"))
                { clsCMD.Parameters["@csc_proid"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_proid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Double _csc_itempurchaseamt;
        public Double csc_itempurchaseamt
        {
            get { return _csc_itempurchaseamt; }
            set
            {
                _csc_itempurchaseamt = value;
                if (clsCMD.Parameters.Contains("@csc_itempurchaseamt"))
                { clsCMD.Parameters["@csc_itempurchaseamt"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_itempurchaseamt", SqlDbType.Money).Value = value; }
            }
        }
        private String _csc_payer;
        public String csc_payer
        {
            get { return _csc_payer; }
            set
            {
                _csc_payer = value;
                if (clsCMD.Parameters.Contains("@csc_payer"))
                { clsCMD.Parameters["@csc_payer"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_payer", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _csc_totalHours;
        public Int64 csc_totalHours
        {
            get { return _csc_totalHours; }
            set
            {
                _csc_totalHours = value;
                if (clsCMD.Parameters.Contains("@csc_totalHours"))
                { clsCMD.Parameters["@csc_totalHours"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_totalHours", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _csc_clientnumber;
        public String csc_clientnumber
        {
            get { return _csc_clientnumber; }
            set
            {
                _csc_clientnumber = value;
                if (clsCMD.Parameters.Contains("@csc_clientnumber"))
                { clsCMD.Parameters["@csc_clientnumber"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_clientnumber", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Double _csc_copaydedrecd;
        public Double csc_copaydedrecd
        {
            get { return _csc_copaydedrecd; }
            set
            {
                _csc_copaydedrecd = value;
                if (clsCMD.Parameters.Contains("@csc_copaydedrecd"))
                { clsCMD.Parameters["@csc_copaydedrecd"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_copaydedrecd", SqlDbType.Money).Value = value; }
            }
        }
        private DateTime _csc_timein;
        public DateTime csc_timein
        {
            get { return _csc_timein; }
            set
            {
                _csc_timein = value;
                if (clsCMD.Parameters.Contains("@csc_timein"))
                { clsCMD.Parameters["@csc_timein"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_timein", SqlDbType.DateTime).Value = value; }
            }
        }
        private DateTime _csc_timeout;
        public DateTime csc_timeout
        {
            get { return _csc_timeout; }
            set
            {
                _csc_timeout = value;
                if (clsCMD.Parameters.Contains("@csc_timeout"))
                { clsCMD.Parameters["@csc_timeout"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_timeout", SqlDbType.DateTime).Value = value; }
            }
        }
        private Double _csc_clntpayrecd;
        public Double csc_clntpayrecd
        {
            get { return _csc_clntpayrecd; }
            set
            {
                _csc_clntpayrecd = value;
                if (clsCMD.Parameters.Contains("@csc_clntpayrecd"))
                { clsCMD.Parameters["@csc_clntpayrecd"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_clntpayrecd", SqlDbType.Money).Value = value; }
            }
        }
        private Double _csc_billettoclient;
        public Double csc_billettoclient
        {
            get { return _csc_billettoclient; }
            set
            {
                _csc_billettoclient = value;
                if (clsCMD.Parameters.Contains("@csc_billettoclient"))
                { clsCMD.Parameters["@csc_billettoclient"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_billettoclient", SqlDbType.Money).Value = value; }
            }
        }
        private Double _csc_billedtoinsurance;
        public Double csc_billedtoinsurance
        {
            get { return _csc_billedtoinsurance; }
            set
            {
                _csc_billedtoinsurance = value;
                if (clsCMD.Parameters.Contains("@csc_billedtoinsurance"))
                { clsCMD.Parameters["@csc_billedtoinsurance"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_billedtoinsurance", SqlDbType.Money).Value = value; }
            }
        }
        private String _csc_location;
        public String csc_location
        {
            get { return _csc_location; }
            set
            {
                _csc_location = value;
                if (clsCMD.Parameters.Contains("@csc_location"))
                { clsCMD.Parameters["@csc_location"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_location", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private DateTime _csc_insdatesubmit;
        public DateTime csc_insdatesubmit
        {
            get { return _csc_insdatesubmit; }
            set
            {
                _csc_insdatesubmit = value;
                if (clsCMD.Parameters.Contains("@csc_insdatesubmit"))
                { clsCMD.Parameters["@csc_insdatesubmit"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_insdatesubmit", SqlDbType.DateTime).Value = value; }
            }
        }
        private String _csc_insclmnumber;
        public String csc_insclmnumber
        {
            get { return _csc_insclmnumber; }
            set
            {
                _csc_insclmnumber = value;
                if (clsCMD.Parameters.Contains("@csc_insclmnumber"))
                { clsCMD.Parameters["@csc_insclmnumber"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_insclmnumber", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _csc_insclmnote;
        public String csc_insclmnote
        {
            get { return _csc_insclmnote; }
            set
            {
                _csc_insclmnote = value;
                if (clsCMD.Parameters.Contains("@csc_insclmnote"))
                { clsCMD.Parameters["@csc_insclmnote"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_insclmnote", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Double _csc_recdfrominsu;
        public Double csc_recdfrominsu
        {
            get { return _csc_recdfrominsu; }
            set
            {
                _csc_recdfrominsu = value;
                if (clsCMD.Parameters.Contains("@csc_recdfrominsu"))
                { clsCMD.Parameters["@csc_recdfrominsu"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_recdfrominsu", SqlDbType.Money).Value = value; }
            }
        }
        private DateTime _csc_datenotesubmit;
        public DateTime csc_datenotesubmit
        {
            get { return _csc_datenotesubmit; }
            set
            {
                _csc_datenotesubmit = value;
                if (clsCMD.Parameters.Contains("@csc_datenotesubmit"))
                { clsCMD.Parameters["@csc_datenotesubmit"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_datenotesubmit", SqlDbType.DateTime).Value = value; }
            }
        }
        private Double _csc_totalreceived;
        public Double csc_totalreceived
        {
            get { return _csc_totalreceived; }
            set
            {
                _csc_totalreceived = value;
                if (clsCMD.Parameters.Contains("@csc_totalreceived"))
                { clsCMD.Parameters["@csc_totalreceived"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_totalreceived", SqlDbType.Money).Value = value; }
            }
        }
        private String _csc_todaygoal;
        public String csc_todaygoal
        {
            get { return _csc_todaygoal; }
            set
            {
                _csc_todaygoal = value;
                if (clsCMD.Parameters.Contains("@csc_todaygoal"))
                { clsCMD.Parameters["@csc_todaygoal"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_todaygoal", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Double _csc_submitcollection;
        public Double csc_submitcollection
        {
            get { return _csc_submitcollection; }
            set
            {
                _csc_submitcollection = value;
                if (clsCMD.Parameters.Contains("@csc_submitcollection"))
                { clsCMD.Parameters["@csc_submitcollection"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_submitcollection", SqlDbType.Money).Value = value; }
            }
        }
        private Int64 _csc_show;
        public Int64 csc_show
        {
            get { return _csc_show; }
            set
            {
                _csc_show = value;
                if (clsCMD.Parameters.Contains("@csc_show"))
                { clsCMD.Parameters["@csc_show"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_show", SqlDbType.BigInt).Value = value; }
            }
        }
        private Double _csc_recdfrmcollection;
        public Double csc_recdfrmcollection
        {
            get { return _csc_recdfrmcollection; }
            set
            {
                _csc_recdfrmcollection = value;
                if (clsCMD.Parameters.Contains("@csc_recdfrmcollection"))
                { clsCMD.Parameters["@csc_recdfrmcollection"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_recdfrmcollection", SqlDbType.Money).Value = value; }
            }
        }
        private DateTime _csc_nextappointment;
        public DateTime csc_nextappointment
        {
            get { return _csc_nextappointment; }
            set
            {
                _csc_nextappointment = value;
                if (clsCMD.Parameters.Contains("@csc_nextappointment"))
                { clsCMD.Parameters["@csc_nextappointment"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_nextappointment", SqlDbType.DateTime).Value = value; }
            }
        }
        private Double _csc_adjustwrite;
        public Double csc_adjustwrite
        {
            get { return _csc_adjustwrite; }
            set
            {
                _csc_adjustwrite = value;
                if (clsCMD.Parameters.Contains("@csc_adjustwrite"))
                { clsCMD.Parameters["@csc_adjustwrite"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_adjustwrite", SqlDbType.Money).Value = value; }
            }
        }
        private String _csc_sessiontype;
        public String csc_sessiontype
        {
            get { return _csc_sessiontype; }
            set
            {
                _csc_sessiontype = value;
                if (clsCMD.Parameters.Contains("@csc_sessiontype"))
                { clsCMD.Parameters["@csc_sessiontype"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_sessiontype", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Double _csc_totalcharged;
        public Double csc_totalcharged
        {
            get { return _csc_totalcharged; }
            set
            {
                _csc_totalcharged = value;
                if (clsCMD.Parameters.Contains("@csc_totalcharged"))
                { clsCMD.Parameters["@csc_totalcharged"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_totalcharged", SqlDbType.Money).Value = value; }
            }
        }
        private Boolean _csc_noteenter;
        public Boolean csc_noteenter
        {
            get { return _csc_noteenter; }
            set
            {
                _csc_noteenter = value;
                if (clsCMD.Parameters.Contains("@csc_noteenter"))
                { clsCMD.Parameters["@csc_noteenter"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_noteenter", SqlDbType.Bit).Value = value; }
            }
        }
        private Double _csc_totalpaid;
        public Double csc_totalpaid
        {
            get { return _csc_totalpaid; }
            set
            {
                _csc_totalpaid = value;
                if (clsCMD.Parameters.Contains("@csc_totalpaid"))
                { clsCMD.Parameters["@csc_totalpaid"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_totalpaid", SqlDbType.Money).Value = value; }
            }
        }
        private Boolean _csc_placedinvoce;
        public Boolean csc_placedinvoce
        {
            get { return _csc_placedinvoce; }
            set
            {
                _csc_placedinvoce = value;
                if (clsCMD.Parameters.Contains("@csc_placedinvoce"))
                { clsCMD.Parameters["@csc_placedinvoce"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_placedinvoce", SqlDbType.Bit).Value = value; }
            }
        }
        private Double _csc_balancedue;
        public Double csc_balancedue
        {
            get { return _csc_balancedue; }
            set
            {
                _csc_balancedue = value;
                if (clsCMD.Parameters.Contains("@csc_balancedue"))
                { clsCMD.Parameters["@csc_balancedue"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_balancedue", SqlDbType.Money).Value = value; }
            }
        }
        private Double _csc_cofacilitionfee;
        public Double csc_cofacilitionfee
        {
            get { return _csc_cofacilitionfee; }
            set
            {
                _csc_cofacilitionfee = value;
                if (clsCMD.Parameters.Contains("@csc_cofacilitionfee"))
                { clsCMD.Parameters["@csc_cofacilitionfee"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_cofacilitionfee", SqlDbType.Money).Value = value; }
            }
        }
        private Double _csc_therapist;
        public Double csc_therapist
        {
            get { return _csc_therapist; }
            set
            {
                _csc_therapist = value;
                if (clsCMD.Parameters.Contains("@csc_therapist"))
                { clsCMD.Parameters["@csc_therapist"].Value = value; }
                else { clsCMD.Parameters.Add("@csc_therapist", SqlDbType.Money).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                csc_id = Convert.ToInt64(dr["csc_id"]);
                csc_ssnid = Convert.ToInt64(dr["csc_ssnid"]);
                csc_date = Convert.ToDateTime(dr["csc_date"]);
                csc_clientid = new Guid(dr["csc_clientid"].ToString());
                csc_itemid = Convert.ToInt64(dr["csc_itemid"]);
                csc_fslid = Convert.ToInt64(dr["csc_fslid"]);
                csc_proid = Convert.ToInt64(dr["csc_proid"]);
                csc_itempurchaseamt = Convert.ToDouble(dr["csc_itempurchaseamt"]);
                csc_payer = Convert.ToString(dr["csc_payer"]);
                csc_totalHours = Convert.ToInt64(dr["csc_totalHours"]);
                csc_clientnumber = Convert.ToString(dr["csc_clientnumber"]);
                csc_copaydedrecd = Convert.ToDouble(dr["csc_copaydedrecd"]);
                csc_timein = Convert.ToDateTime(dr["csc_timein"]);
                csc_timeout = Convert.ToDateTime(dr["csc_timeout"]);
                csc_clntpayrecd = Convert.ToDouble(dr["csc_clntpayrecd"]);
                csc_billettoclient = Convert.ToDouble(dr["csc_billettoclient"]);
                csc_billedtoinsurance = Convert.ToDouble(dr["csc_billedtoinsurance"]);
                csc_location = Convert.ToString(dr["csc_location"]);
                csc_insdatesubmit = Convert.ToDateTime(dr["csc_insdatesubmit"]);
                csc_insclmnumber = Convert.ToString(dr["csc_insclmnumber"]);
                csc_insclmnote = Convert.ToString(dr["csc_insclmnote"]);
                csc_recdfrominsu = Convert.ToDouble(dr["csc_recdfrominsu"]);
                csc_datenotesubmit = Convert.ToDateTime(dr["csc_datenotesubmit"]);
                csc_totalreceived = Convert.ToDouble(dr["csc_totalreceived"]);
                csc_todaygoal = Convert.ToString(dr["csc_todaygoal"]);
                csc_submitcollection = Convert.ToDouble(dr["csc_submitcollection"]);
                csc_show = Convert.ToInt64(dr["csc_show"]);
                csc_recdfrmcollection = Convert.ToDouble(dr["csc_recdfrmcollection"]);
                csc_nextappointment = Convert.ToDateTime(dr["csc_nextappointment"]);
                csc_adjustwrite = Convert.ToDouble(dr["csc_adjustwrite"]);
                csc_sessiontype = Convert.ToString(dr["csc_sessiontype"]);
                csc_totalcharged = Convert.ToDouble(dr["csc_totalcharged"]);
                csc_noteenter = Convert.ToBoolean(dr["csc_noteenter"]);
                csc_totalpaid = Convert.ToDouble(dr["csc_totalpaid"]);
                csc_placedinvoce = Convert.ToBoolean(dr["csc_placedinvoce"]);
                csc_balancedue = Convert.ToDouble(dr["csc_balancedue"]);
                csc_cofacilitionfee = Convert.ToDouble(dr["csc_cofacilitionfee"]);
                csc_therapist = Convert.ToDouble(dr["csc_therapist"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            csc_id = Convert.ToInt64("0");
            csc_ssnid = Convert.ToInt64("0");
            csc_date = Convert.ToDateTime(DateTime.Now);
            csc_clientid = new Guid();
            csc_itemid = Convert.ToInt64("0");
            csc_fslid = Convert.ToInt64("0");
            csc_proid = Convert.ToInt64("0");
            csc_itempurchaseamt = Convert.ToDouble("0");
            csc_payer = Convert.ToString("NA");
            csc_totalHours = Convert.ToInt64("0");
            csc_clientnumber = Convert.ToString("NA");
            csc_copaydedrecd = Convert.ToDouble("0");
            csc_timein = Convert.ToDateTime(DateTime.Now);
            csc_timeout = Convert.ToDateTime(DateTime.Now);
            csc_clntpayrecd = Convert.ToDouble("0");
            csc_billettoclient = Convert.ToDouble("0");
            csc_billedtoinsurance = Convert.ToDouble("0");
            csc_location = Convert.ToString("NA");
            csc_insdatesubmit = Convert.ToDateTime(DateTime.Now);
            csc_insclmnumber = Convert.ToString("NA");
            csc_insclmnote = Convert.ToString("NA");
            csc_recdfrominsu = Convert.ToDouble("0");
            csc_datenotesubmit = Convert.ToDateTime(DateTime.Now);
            csc_totalreceived = Convert.ToDouble("0");
            csc_todaygoal = Convert.ToString("NA");
            csc_submitcollection = Convert.ToDouble("0");
            csc_show = Convert.ToInt64("0");
            csc_recdfrmcollection = Convert.ToDouble("0");
            csc_nextappointment = Convert.ToDateTime(DateTime.Now);
            csc_adjustwrite = Convert.ToDouble("0");
            csc_sessiontype = Convert.ToString("NA");
            csc_totalcharged = Convert.ToDouble("0");
            csc_noteenter = Convert.ToBoolean(false);
            csc_totalpaid = Convert.ToDouble("0");
            csc_placedinvoce = Convert.ToBoolean(false);
            csc_balancedue = Convert.ToDouble("0");
            csc_cofacilitionfee = Convert.ToDouble("0");
            csc_therapist = Convert.ToDouble("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.csc_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "ClientSessionChieldAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 csc_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.csc_id = csc_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "ClientSessionChieldAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 csc_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.csc_id = csc_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "ClientSessionChieldAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "ClientSessionChieldGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsClientSessionChield_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsClientSessionChield_PropertiesList> lstResult = new List<clsClientSessionChield_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].csc_id = Convert.ToInt64(dr["csc_id"]);
                lstResult[i].csc_ssnid = Convert.ToInt64(dr["csc_ssnid"]);
                lstResult[i].csc_date = Convert.ToDateTime(dr["csc_date"]);
                lstResult[i].csc_clientid = new Guid(dr["csc_clientid"].ToString());
                lstResult[i].csc_itemid = Convert.ToInt64(dr["csc_itemid"]);
                lstResult[i].csc_fslid = Convert.ToInt64(dr["csc_fslid"]);
                lstResult[i].csc_proid = Convert.ToInt64(dr["csc_proid"]);
                lstResult[i].csc_itempurchaseamt = Convert.ToDouble(dr["csc_itempurchaseamt"]);
                lstResult[i].csc_payer = Convert.ToString(dr["csc_payer"]);
                lstResult[i].csc_totalHours = Convert.ToInt64(dr["csc_totalHours"]);
                lstResult[i].csc_clientnumber = Convert.ToString(dr["csc_clientnumber"]);
                lstResult[i].csc_copaydedrecd = Convert.ToDouble(dr["csc_copaydedrecd"]);
                lstResult[i].csc_timein = Convert.ToDateTime(dr["csc_timein"]);
                lstResult[i].csc_timeout = Convert.ToDateTime(dr["csc_timeout"]);
                lstResult[i].csc_clntpayrecd = Convert.ToDouble(dr["csc_clntpayrecd"]);
                lstResult[i].csc_billettoclient = Convert.ToDouble(dr["csc_billettoclient"]);
                lstResult[i].csc_billedtoinsurance = Convert.ToDouble(dr["csc_billedtoinsurance"]);
                lstResult[i].csc_location = Convert.ToString(dr["csc_location"]);
                lstResult[i].csc_insdatesubmit = Convert.ToDateTime(dr["csc_insdatesubmit"]);
                lstResult[i].csc_insclmnumber = Convert.ToString(dr["csc_insclmnumber"]);
                lstResult[i].csc_insclmnote = Convert.ToString(dr["csc_insclmnote"]);
                lstResult[i].csc_recdfrominsu = Convert.ToDouble(dr["csc_recdfrominsu"]);
                lstResult[i].csc_datenotesubmit = Convert.ToDateTime(dr["csc_datenotesubmit"]);
                lstResult[i].csc_totalreceived = Convert.ToDouble(dr["csc_totalreceived"]);
                lstResult[i].csc_todaygoal = Convert.ToString(dr["csc_todaygoal"]);
                lstResult[i].csc_submitcollection = Convert.ToDouble(dr["csc_submitcollection"]);
                lstResult[i].csc_show = Convert.ToInt64(dr["csc_show"]);
                lstResult[i].csc_recdfrmcollection = Convert.ToDouble(dr["csc_recdfrmcollection"]);
                lstResult[i].csc_nextappointment = Convert.ToDateTime(dr["csc_nextappointment"]);
                lstResult[i].csc_adjustwrite = Convert.ToDouble(dr["csc_adjustwrite"]);
                lstResult[i].csc_sessiontype = Convert.ToString(dr["csc_sessiontype"]);
                lstResult[i].csc_totalcharged = Convert.ToDouble(dr["csc_totalcharged"]);
                lstResult[i].csc_noteenter = Convert.ToBoolean(dr["csc_noteenter"]);
                lstResult[i].csc_totalpaid = Convert.ToDouble(dr["csc_totalpaid"]);
                lstResult[i].csc_placedinvoce = Convert.ToBoolean(dr["csc_placedinvoce"]);
                lstResult[i].csc_balancedue = Convert.ToDouble(dr["csc_balancedue"]);
                lstResult[i].csc_cofacilitionfee = Convert.ToDouble(dr["csc_cofacilitionfee"]);
                lstResult[i].csc_therapist = Convert.ToDouble(dr["csc_therapist"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 csc_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.csc_id = csc_id;
                clsCMD.CommandText = "ClientSessionChieldGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 csc_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.csc_id = csc_id;
                clsCMD.CommandText = "ClientSessionChieldGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion
    }

    public class clsClientSessionChield_PropertiesList
    {
        public Int64 csc_id { get; set; }
        public Int64 csc_ssnid { get; set; }
        public DateTime csc_date { get; set; }
        public Guid csc_clientid { get; set; }
        public Int64 csc_itemid { get; set; }
        public Int64 csc_fslid { get; set; }
        public Int64 csc_proid { get; set; }
        public Double csc_itempurchaseamt { get; set; }
        public String csc_payer { get; set; }
        public Int64 csc_totalHours { get; set; }
        public String csc_clientnumber { get; set; }
        public Double csc_copaydedrecd { get; set; }
        public DateTime csc_timein { get; set; }
        public DateTime csc_timeout { get; set; }
        public Double csc_clntpayrecd { get; set; }
        public Double csc_billettoclient { get; set; }
        public Double csc_billedtoinsurance { get; set; }
        public String csc_location { get; set; }
        public DateTime csc_insdatesubmit { get; set; }
        public String csc_insclmnumber { get; set; }
        public String csc_insclmnote { get; set; }
        public Double csc_recdfrominsu { get; set; }
        public DateTime csc_datenotesubmit { get; set; }
        public Double csc_totalreceived { get; set; }
        public String csc_todaygoal { get; set; }
        public Double csc_submitcollection { get; set; }
        public Int64 csc_show { get; set; }
        public Double csc_recdfrmcollection { get; set; }
        public DateTime csc_nextappointment { get; set; }
        public Double csc_adjustwrite { get; set; }
        public String csc_sessiontype { get; set; }
        public Double csc_totalcharged { get; set; }
        public Boolean csc_noteenter { get; set; }
        public Double csc_totalpaid { get; set; }
        public Boolean csc_placedinvoce { get; set; }
        public Double csc_balancedue { get; set; }
        public Double csc_cofacilitionfee { get; set; }
        public Double csc_therapist { get; set; }
    }
}