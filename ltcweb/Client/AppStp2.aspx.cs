﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Client_AppStp2 : System.Web.UI.Page
{
    clsAppointmentMaster clsAPT = new clsAppointmentMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["appoinment"] != null)
        {
            clsAPT = (clsAppointmentMaster)Session["appoinment"];
        }
    }

    protected void lnkView_Click(object sender, EventArgs e)
    {
        string fln=clsAPT.apt_courseid.ToString() + clsAPT.apt_clientid.ToString() + ".pdf";

        string fp = ConfigurationManager.AppSettings["SizzUrl"].ToString() + "/Document/"+fln;
        WebClient client = new WebClient();
        Byte[] buffer = client.DownloadData(fp);
        if (buffer != null)
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-length", buffer.Length.ToString());
            Response.AddHeader("content-disposition", "attachment;filename="+fln);
            Response.Flush();
            Response.BinaryWrite(buffer);
            Response.End();
        }
    }
    protected void cmdAddPontment_Click(object sender, EventArgs e)
    {
        // code for insert appoinment and redirect to payment;
        Boolean blnRsltApt = clsAPT.Save();
        if (blnRsltApt == true)
        {
            Session["appoinmentId"] = clsAPT.apt_code;
            Response.Redirect("~/Client/Payment.aspx");
        }
    }
}