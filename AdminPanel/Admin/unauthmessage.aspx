﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="unauthmessage.aspx.cs" Inherits="Unauthorisedaccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12" runat="server" id="unauthoriseaccess" visible="false">
            <div class="box">
                <div class="box-title">
                    <h3><i class="glyphicon-settings"></i>UnAuthorised Access</h3>
                </div>
                <div class="box-content">
                    <asp:Label ID="Label1" runat="server" Font-Bold="true" ForeColor="Red" style="text-align:center">
                        you have not sub-scribed for this module
                        <br />
                        <br />
                        to get access of this please subscribe it first .
                    </asp:Label>
                </div>
            </div>
        </div>

        <div class="span12" runat="server" id="invalidpage" visible="false">
            <div class="box">
                <div class="box-title">
                    <h3><i class="glyphicon-settings"></i>Invalid Page Request</h3>
                </div>
                <div class="box-content">
                    <asp:Label ID="Label2" runat="server" Font-Bold="true" ForeColor="Red" style="text-align:center">
                        Opps..!
                        <br />
                        <br />
                        the page you are requesting it seems that is not exist.
                    </asp:Label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

