﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsChildrenDetail
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsChildrenDetail()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsChildrenDetail()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _chd_id; }
            set
            {
                _chd_id = value;
                if (clsCMD.Parameters.Contains("@chd_id"))
                { clsCMD.Parameters["@chd_id"].Value = value; }
                else { clsCMD.Parameters.Add("@chd_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _chd_id;
        public Int64 chd_id
        {
            get { return _chd_id; }
            set
            {
                _chd_id = value;
                if (clsCMD.Parameters.Contains("@chd_id"))
                { clsCMD.Parameters["@chd_id"].Value = value; }
                else { clsCMD.Parameters.Add("@chd_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _chd_userid;
        public Guid chd_userid
        {
            get { return _chd_userid; }
            set
            {
                _chd_userid = value;
                if (clsCMD.Parameters.Contains("@chd_userid"))
                { clsCMD.Parameters["@chd_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@chd_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _chd_name;
        public String chd_name
        {
            get { return _chd_name; }
            set
            {
                _chd_name = value;
                if (clsCMD.Parameters.Contains("@chd_name"))
                { clsCMD.Parameters["@chd_name"].Value = value; }
                else { clsCMD.Parameters.Add("@chd_name", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _chd_age;
        public String chd_age
        {
            get { return _chd_age; }
            set
            {
                _chd_age = value;
                if (clsCMD.Parameters.Contains("@chd_age"))
                { clsCMD.Parameters["@chd_age"].Value = value; }
                else { clsCMD.Parameters.Add("@chd_age", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private DateTime _chd_BirthDate;
        public DateTime chd_BirthDate
        {
            get { return _chd_BirthDate; }
            set
            {
                _chd_BirthDate = value;
                if (clsCMD.Parameters.Contains("@chd_BirthDate"))
                { clsCMD.Parameters["@chd_BirthDate"].Value = value; }
                else { clsCMD.Parameters.Add("@chd_BirthDate", SqlDbType.DateTime).Value = value; }
            }
        }
        private String _chd_gender;
        public String chd_gender
        {
            get { return _chd_gender; }
            set
            {
                _chd_gender = value;
                if (clsCMD.Parameters.Contains("@chd_gender"))
                { clsCMD.Parameters["@chd_gender"].Value = value; }
                else { clsCMD.Parameters.Add("@chd_gender", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Boolean _chd_liveparent;
        public Boolean chd_liveparent
        {
            get { return _chd_liveparent; }
            set
            {
                _chd_liveparent = value;
                if (clsCMD.Parameters.Contains("@chd_liveparent"))
                { clsCMD.Parameters["@chd_liveparent"].Value = value; }
                else { clsCMD.Parameters.Add("@chd_liveparent", SqlDbType.Bit).Value = value; }
            }
        }
        private String _chd_parentname;
        public String chd_parentname
        {
            get { return _chd_parentname; }
            set
            {
                _chd_parentname = value;
                if (clsCMD.Parameters.Contains("@chd_parentname"))
                { clsCMD.Parameters["@chd_parentname"].Value = value; }
                else { clsCMD.Parameters.Add("@chd_parentname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _chd_residence;
        public String chd_residence
        {
            get { return _chd_residence; }
            set
            {
                _chd_residence = value;
                if (clsCMD.Parameters.Contains("@chd_residence"))
                { clsCMD.Parameters["@chd_residence"].Size = value.Length; clsCMD.Parameters["@chd_residence"].Value = value; }
                else { clsCMD.Parameters.Add("@chd_residence", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                chd_id = Convert.ToInt64(dr["chd_id"]);
                chd_userid = new Guid(dr["chd_userid"].ToString());
                chd_name = Convert.ToString(dr["chd_name"]);
                chd_age = Convert.ToString(dr["chd_age"]);
                chd_BirthDate = Convert.ToDateTime(dr["chd_BirthDate"]);
                chd_gender = Convert.ToString(dr["chd_gender"]);
                chd_liveparent = Convert.ToBoolean(dr["chd_liveparent"]);
                chd_parentname = Convert.ToString(dr["chd_parentname"]);
                chd_residence = Convert.ToString(dr["chd_residence"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            chd_id = Convert.ToInt64("0");
            chd_userid = new Guid();
            chd_name = Convert.ToString("NA");
            chd_age = Convert.ToString("NA");
            chd_BirthDate = Convert.ToDateTime(DateTime.Now);
            chd_gender = Convert.ToString("NA");
            chd_liveparent = Convert.ToBoolean(false);
            chd_parentname = Convert.ToString("NA");
            chd_residence = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; this.clsCMD.Connection = clsCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.chd_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_childrenAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
                CloseConnection();
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 chd_id)
        {
            Boolean blnResult = false;
            this.chd_id = chd_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_childrenAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
                CloseConnection();
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 chd_id)
        {
            Boolean blnResult = false;
            this.chd_id = chd_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_childrenAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
                CloseConnection();
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_childrenGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
                CloseConnection();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordUserID(Guid Userid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.chd_userid = Userid;
                clsCMD.CommandText = "sp_childrenGetData";
                SetGetSPFlag = "BYUSERID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
                CloseConnection();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsChildrenDetail_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsChildrenDetail_PropertiesList> lstResult = new List<clsChildrenDetail_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].chd_id = Convert.ToInt64(dr["chd_id"]);
                lstResult[i].chd_userid = new Guid(dr["chd_userid"].ToString());
                lstResult[i].chd_name = Convert.ToString(dr["chd_name"]);
                lstResult[i].chd_age = Convert.ToString(dr["chd_age"]);
                lstResult[i].chd_BirthDate = Convert.ToDateTime(dr["chd_BirthDate"]);
                lstResult[i].chd_gender = Convert.ToString(dr["chd_gender"]);
                lstResult[i].chd_liveparent = Convert.ToBoolean(dr["chd_liveparent"]);
                lstResult[i].chd_parentname = Convert.ToString(dr["chd_parentname"]);
                lstResult[i].chd_residence = Convert.ToString(dr["chd_residence"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 chd_id)
        {
            Boolean blnResult = false;
            try
            {
                this.chd_id = chd_id;
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_childrenGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
                CloseConnection();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 chd_id)
        {
            Boolean blnResult = false;
            try
            {
                this.chd_id = chd_id;
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_childrenGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
                CloseConnection();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }

    public class clsChildrenDetail_PropertiesList
    {
        public Int64 chd_id { get; set; }
        public Guid chd_userid { get; set; }
        public String chd_name { get; set; }
        public String chd_age { get; set; }
        public DateTime chd_BirthDate { get; set; }
        public String chd_gender { get; set; }
        public Boolean chd_liveparent { get; set; }
        public String chd_parentname { get; set; }
        public String chd_residence { get; set; }
    }
}