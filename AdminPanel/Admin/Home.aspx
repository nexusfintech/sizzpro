﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Admin_Home" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeftBar" runat="server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Quick Links</span></a>
        </div>
        <ul class="subnav-menu">
            <li class='dropdown'>
                <a href="#" data-toggle="dropdown">Memberships</a>
                <ul class="dropdown-menu">
                    <% if (HttpContext.Current.User.IsInRole("administrator"))
                       { %>
                    <li>
                        <a href="../Users/MemberRegistration.aspx">Create New <%= MySession.Current.MemberAlias %></a>
                    </li>
                    <%} %>
                    <% if (HttpContext.Current.User.IsInRole("administrator") || HttpContext.Current.User.IsInRole("member"))
                       { %>
                    <li>
                        <a href="../Users/UserRegistration.aspx">Create New <%= MySession.Current.UserAlias %></a>
                    </li>
                    <%} %>
                    <li>
                        <a href="../Users/ClientRegistration.aspx">Create New <%= MySession.Current.ClientAlias %></a>
                    </li>
                </ul>
            </li>
            <%if (HttpContext.Current.User.IsInRole("administrator") || HttpContext.Current.User.IsInRole("member") || HttpContext.Current.User.IsInRole("user"))
              { %>
            <li class='dropdown'>
                <a href="SessionManagement/ViewClientSession.aspx">Client Sessions</a>
            </li>
            <%} %>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <% if (HttpContext.Current.User.IsInRole("administrator"))
       { %>
    <%--<div class="row-fluid">

        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="icon-money"></i>
                    Sales
            </h3>
            </div>
            <div class="span12">
                <ul class="tiles">
                    <li class="green long">
                        <a href="#"><span>$</span><span class='name'>Day</span></a>
                    </li>
                    <li class="red  long">
                        <a href="#"><span>$</span><span class='name'>Month</span></a>
                    </li>
                    <li class="blue long">
                        <a href="#"><span class='count'>$</span><span class='name'>Year</span></a>
                    </li>
                    <li class="magenta long">
                        <a href="#"><span class='count'>$</span><span class='name'>Date</span></a>
                    </li>
                    <li class="pink long">
                        <a href="#"><span class='count'>$</span><span class='name'>By date</span></a>
                    </li>

                </ul>
            </div>
        </div>
    </div>--%>

    <div class="row-fluid">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="glyphicon-user"></i>
                    Users
            </h3>
            </div>
            <div class="span12">
                <ul class="tiles">
                    <li class="green long">
                        <a href="../Users/AdminManagement.aspx"><span><i class="icon-user"></i>&nbsp;<%= MySession.Current.noOfAdmin %></span><span class='name'>ADMINISTRATOR</span></a>
                    </li>
                    <li class="red  long">
                        <a href="../Users/MemberManagement.aspx"><span><i class="icon-user"></i>&nbsp;<%=MySession.Current.noOfmember %></span><span class='name'><%= MySession.Current.MemberAlias.ToUpper() %></span></a>
                    </li>
                    <li class="blue long">
                        <a href="../Users/UserManagement.aspx"><span class='count'><i class="icon-user"></i>&nbsp;<%=MySession.Current.noOfuser %></span><span class='name'><%= MySession.Current.UserAlias.ToUpper() %></span></a>
                    </li>
                    <li class="magenta long">
                        <a href="../Users/UserClientManage.aspx"><span class='count'><i class="icon-user"></i>&nbsp;<%=MySession.Current.noOfclient %></span><span class='name'><%= MySession.Current.ClientAlias.ToUpper() %></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row-fluid">

        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="icon-list-alt"></i>
                    Subscription
            </h3>
            </div>
            <div class="span12">
                <ul class="tiles">
                    <li class="green long">
                        <a href="../Subscription/ModuleMaster.aspx"><span><i class="icon-table"></i></span><span class='name'>MODULES</span></a>
                    </li>
                     <li class="blue long">
                        <a href="../Subscription/ServicesMaster.aspx"><span><i class="icon-table"></i></span><span class='name'>PACKAGES</span></a>
                    </li>
                     <li class="magenta long">
                        <a href="../Subscription/"><span><i class="icon-table"></i></span><span class='name'>USER SUBSCRIPTIONS</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row-fluid">
    </div>
    <%} %>
    <% if (HttpContext.Current.User.IsInRole("member"))
       { %>


    <div class="row-fluid">
        <div class="span6">
            <div class="box">

                <div class="span6">
                    <div class="box-title">
                        <h3>
                            <i class="glyphicon-calendar"></i>Appointments</h3>
                    </div>
                    <ul class="tiles">
                        <li class="green long">
                            <a href="#"><span><i class="icon-calendar"></i></span><span class='name'><i class="icon-circle-arrow-left"></i>&nbsp;Previous</span></a>
                        </li>
                        <li class="red long">
                            <a href="#"><span><i class="icon-calendar"></i></span><span class='name'><i class="icon-circle"></i>&nbsp;Today's</span></a>
                        </li>
                        <li class="blue long">
                            <a href="#"><span><i class="icon-calendar"></i></span><span class='name'>Next&nbsp;<i class="icon-circle-arrow-right"></i></span></a>
                        </li>
                        <li class="magenta long">
                            <a href="#"><span><i class="icon-calendar-empty"></i></span><span class='name'>Total</span></a>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="box">
                <div class="span6">
                    <div class="box-title">
                        <h3>
                            <i class="glyphicon-user"></i>Users</h3>
                    </div>
                    <ul class="tiles">
                        <li class="green long">
                            <a href="#"><span><i class="icon-user"></i>&nbsp;</span><span class='name'>Active <%= MySession.Current.UserAlias %></span></a>
                        </li>
                        <li class="red long">
                            <a href="#"><span><i class="icon-user"></i>&nbsp;</span><span class='name'>Inactive <%= MySession.Current.UserAlias %></span></a>
                        </li>
                        <li class="blue long">
                            <a href="#"><span class='count'><i class="icon-user"></i>&nbsp;</span><span class='name'>New <%= MySession.Current.UserAlias %></span></a>
                        </li>
                        <li class="magenta long">
                            <a href="#"><span><i class="icon-user"></i>&nbsp;</span><span class='name'>Total <%= MySession.Current.UserAlias %></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-calendar"></i>My calendar</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="calendar fc">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <%} %>

    <% if (HttpContext.Current.User.IsInRole("user"))
       {%>
    <div class="row-fluid">
        <div class="box">
            <div class="span12">
                <div class="box-title">
                    <h3>
                        <i class="glyphicon-user"></i>
                        <%= MySession.Current.ClientAlias %>
            </h3>
                </div>
                <ul class="tiles">

                    <li class="green long">
                        <a href="#"><span><i class="icon-user"></i>&nbsp;</span><span class='name'>Active <%= MySession.Current.ClientAlias %></span></a>
                    </li>
                    <li class="red long">
                        <a href="#"><span><i class="icon-user"></i>&nbsp;</span><span class='name'>Inactive <%= MySession.Current.ClientAlias %></span></a>
                    </li>
                    <li class="blue long">
                        <a href="#"><span class='count'><i class="icon-user"></i>&nbsp;</span><span class='name'>New <%= MySession.Current.ClientAlias%></span></a>
                    </li>
                    <li class="magenta long">
                        <a href="#"><span><i class="icon-user"></i>&nbsp;</span><span class='name'>Total <%= MySession.Current.ClientAlias %></span></a>
                    </li>
                    <li class="pink long">
                        <a href="#"><span><i class="icon-user"></i>&nbsp;</span><span class='name'>Shared <%= MySession.Current.ClientAlias %></span></a>
                    </li>

                </ul>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="box">

            <div class="span12">
                <div class="box-title">
                    <h3>
                        <i class="glyphicon-calendar"></i>
                        Appointments
            </h3>
                </div>
                <ul class="tiles">
                    <li class="green long">
                        <a href="#"><span><i class="icon-calendar"></i></span><span class='name'><i class="icon-circle-arrow-left"></i>&nbsp;Previous</span></a>
                    </li>
                    <li class="red  long">
                        <a href="#"><span><i class="icon-calendar"></i></span><span class='name'><i class="icon-circle"></i>&nbsp;Today's</span></a>
                    </li>
                    <li class="blue long">
                        <a href="#"><span><i class="icon-calendar"></i></span><span class='name'>Next&nbsp;<i class="icon-circle-arrow-right"></i></span></a>
                    </li>
                    <li class="magenta long">
                        <a href="#"><span><i class="icon-calendar"></i></span><span class='name'>Total</span></a>
                    </li>
                    <li class="pink long">
                        <a href="#"><span><i class="icon-calendar"></i></span><span class='name'>Cancelled</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="box">

            <div class="span12">
                <div class="box-title">
                    <h3>
                        <i class="icon-tasks"></i>
                        Sessions
            </h3>
                </div>
                <ul class="tiles">
                    <li class="green long">
                        <a href="#"><span><i class="icon-tasks"></i></span><span class='name'><i class="icon-caret-left"></i>&nbsp;Completed</span></a>
                    </li>
                    <li class="red  long">
                        <a href="#"><span><i class="icon-tasks"></i></span><span class='name'><i class="icon-circle"></i>&nbsp;Running</span></a>
                    </li>
                    <li class="blue long">
                        <a href="#"><span><i class="icon-tasks"></i></span><span class='name'>Upcoming&nbsp;<i class="icon-caret-right"></i></span></a>
                    </li>
                    <li class="magenta long">
                        <a href="#"><span><i class="icon-tasks"></i></span><span class='name'>Total</span></a>
                    </li>
                    <li class="pink long">
                        <a href="#"><span><i class="icon-tasks"></i></span><span class='name'>Pending / On hold</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>



    <%} %>
</asp:Content>

