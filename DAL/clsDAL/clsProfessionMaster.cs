﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsProfessionMaster
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsProfessionMaster()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsProfessionMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _prf_id; }
            set
            {
                _prf_id = value;
                if (clsCMD.Parameters.Contains("@prf_id"))
                { clsCMD.Parameters["@prf_id"].Value = value; }
                else { clsCMD.Parameters.Add("@prf_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _prf_id;
        public Int64 prf_id
        {
            get { return _prf_id; }
            set
            {
                _prf_id = value;
                if (clsCMD.Parameters.Contains("@prf_id"))
                { clsCMD.Parameters["@prf_id"].Value = value; }
                else { clsCMD.Parameters.Add("@prf_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _prf_name;
        public String prf_name
        {
            get { return _prf_name; }
            set
            {
                _prf_name = value;
                if (clsCMD.Parameters.Contains("@prf_name"))
                { clsCMD.Parameters["@prf_name"].Value = value; }
                else { clsCMD.Parameters.Add("@prf_name", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prf_memberalis;
        public String prf_memberalis
        {
            get { return _prf_memberalis; }
            set
            {
                _prf_memberalis = value;
                if (clsCMD.Parameters.Contains("@prf_memberalis"))
                { clsCMD.Parameters["@prf_memberalis"].Value = value; }
                else { clsCMD.Parameters.Add("@prf_memberalis", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prf_useralis;
        public String prf_useralis
        {
            get { return _prf_useralis; }
            set
            {
                _prf_useralis = value;
                if (clsCMD.Parameters.Contains("@prf_useralis"))
                { clsCMD.Parameters["@prf_useralis"].Value = value; }
                else { clsCMD.Parameters.Add("@prf_useralis", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prf_clientalis;
        public String prf_clientalis
        {
            get { return _prf_clientalis; }
            set
            {
                _prf_clientalis = value;
                if (clsCMD.Parameters.Contains("@prf_clientalis"))
                { clsCMD.Parameters["@prf_clientalis"].Value = value; }
                else { clsCMD.Parameters.Add("@prf_clientalis", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                prf_id = Convert.ToInt64(dr["prf_id"]);
                prf_name = Convert.ToString(dr["prf_name"]);
                prf_memberalis = Convert.ToString(dr["prf_memberalis"]);
                prf_useralis = Convert.ToString(dr["prf_useralis"]);
                prf_clientalis = Convert.ToString(dr["prf_clientalis"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            prf_id = Convert.ToInt64("0");
            prf_name = Convert.ToString("NA");
            prf_memberalis = Convert.ToString("NA");
            prf_useralis = Convert.ToString("NA");
            prf_clientalis = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.prf_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "ProfessionMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 prf_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.prf_id = prf_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "ProfessionMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 prf_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.prf_id = prf_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "ProfessionMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "ProfessionMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsProfessionMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsProfessionMaster_PropertiesList> lstResult = new List<clsProfessionMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].prf_id = Convert.ToInt64(dr["prf_id"]);
                lstResult[i].prf_name = Convert.ToString(dr["prf_name"]);
                lstResult[i].prf_memberalis = Convert.ToString(dr["prf_memberalis"]);
                lstResult[i].prf_useralis = Convert.ToString(dr["prf_useralis"]);
                lstResult[i].prf_clientalis = Convert.ToString(dr["prf_clientalis"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 prf_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.prf_id = prf_id;
                clsCMD.CommandText = "ProfessionMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 prf_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.prf_id = prf_id;
                clsCMD.CommandText = "ProfessionMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion
    }

    public class clsProfessionMaster_PropertiesList
    {
        public Int64 prf_id { get; set; }
        public String prf_name { get; set; }
        public String prf_memberalis { get; set; }
        public String prf_useralis { get; set; }
        public String prf_clientalis { get; set; }
    }
}