﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Appointment.aspx.cs" Inherits="Appointment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <style type="text/css">
        .list .itemsOne {
            display: block;
            padding: 8px;
            border: 1px solid #368EE0;
            text-align: center;
            margin-bottom: 2px;
        }

        .list .itemstwo {
            display: block;
            padding: 2px;
            border: 1px solid #368EE0;
            text-align: center;
            margin-bottom: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-time"></i>&nbsp;Appointment</h2>
            <h5>
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label></h5>
            <asp:Label ID="lblICmsg" runat="server" Visible="false" ForeColor="Red" Font-Bold="true">Your Informed Consent is not created. To take our services please complete the Informed consent from <a href="informedConsentDetails.aspx">here</a></asp:Label>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <%--<iframe src="http://lifestyletherapy.appointy.com/?isGadget=1" height="555px" scrolling="auto" frameborder="0" allowtransparency="true"></iframe>--%>
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-calendar"></i>
                        Take Appointment
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-column form-bordered'>
                        <div class="control-group">
                            <label for="ddltherapist" class="control-label">Select therapist</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddltherapist" runat="server" AutoPostBack="true" CssClass="select2-me input-xlarge" OnSelectedIndexChanged="ddltherapist_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtAppntDate" class="control-label">Services</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlApntCourse" runat="server" CssClass="select2-me input-xlarge">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtTitle" class="control-label">Subject</label>
                            <div class="controls">
                                <asp:TextBox ID="txtTitle" runat="server" placeholder="Subject" MaxLength="50" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtDescription" class="control-label">Description</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDescription" runat="server" placeholder="Description" Width="99%" TextMode="MultiLine" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtAppntDate" class="control-label">Date</label>
                            <div class="controls">
                                <asp:TextBox ID="txtAppntDate" runat="server" placeholder="mm/dd/yyyy" CssClass="datepick"></asp:TextBox>
                                <asp:Button ID="cmdGetAvailability" runat="server" CssClass="btn btn-primary" Text="Show Available Time" OnClick="cmdGetAvailability_Click" />
                                <span class="help-block">Please enter date in <b>MM/DD/YYYY</b> format</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="lstTime" class="control-label">Available Time</label>
                            <div class="controls">
                                <asp:ListBox ID="lstTime" runat="server" Height="150px" CssClass="list"></asp:ListBox>
                                <asp:Label ID="lblmesg" runat="server" Visible="false" ForeColor="Red" Font-Bold="True">To take an appointment you have to <a href="../Login.aspx" style="font:700 15px verdana">Login</a> first.</asp:Label>
                            </div>

                        </div>
                        <div class="form-actions">
                            <asp:Button ID="cmdAddPontment" runat="server" CssClass="btn btn-primary" Text="Take Appointment" OnClick="cmdAddPontment_Click" />
                            <asp:Label ID="lblICmsg1" runat="server" Visible="false" ForeColor="Red" Font-Bold="true">Your Informed Consent is not created. To take our services please complete the Informed consent from <a href="informedConsentDetails.aspx">here</a></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <h2><strong>Intake Forms</strong></h2>
            <p><b>Please complete our intake assessment application prior to your first session.</b> You can download our application below. You can email to <a href="mailto:admin@LifestyleTherapyCoach.com">admin@LifestyleTherapyCoach.com</a> or print and bring in with you.</p>

            <p>
                <strong>Inform Consent</strong>
                <br />
                <i class="glyphicon-download"></i>&nbsp;<a href="../IntakeFormsIC/INTAKE_FORM_IC.doc">DOC</a> (141KB)|<a href="../IntakeFormsIC/INTAKE_FORM_IC.docx">DOCX</a> (58KB) |<a href="../IntakeFormsIC/INTAKE_FORM_IC.pdf">PDF</a> (455KB)
            </p>

            <p>
                <strong>Life Assessment</strong>
                <br />
                <i class="glyphicon-download"></i>&nbsp;<a href="../IntakeFormsLA/INTAKE_FORM_LA.doc">DOC</a> (441KB)|<a href="../IntakeFormsLA/INTAKE_FORM_LA.docx">DOCX</a> (144KB) |<a href="../IntakeFormsLA/INTAKE_FORM_LA.pdf">PDF</a> (575KB)
            </p>

            <h2><a name="forms" id="forms"></a><strong>Referral Forms</strong></h2>
            <p>Release of Information - <a href="/downloads/Release_of_Information_Update.pdf" target="_blank">PDF</a> <a href="/downloads/Release_of_Information_Update.doc" target="_blank">DOC</a></p>
            <p>Physician Referral - <a href="/downloads/PhysicianReferralForm.pdf" target="_blank">PDF</a> <a href="/downloads/PhysicianReferralForm.doc" target="_blank">DOC</a></p>
            <p><a href="schedule.html" class="wrapper">Referral Scheduling</a></p>
            <p><a href="http://lifestyletherapy.appointy.com/staff/login.aspx" target="_blank">Admin Login</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

