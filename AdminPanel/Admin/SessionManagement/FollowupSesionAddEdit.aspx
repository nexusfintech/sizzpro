﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="FollowupSesionAddEdit.aspx.cs" Inherits="Admin_SessionManagement_FollowupSesionAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .item {
            display: block;
            padding: 5px;
            border: 1px solid black;
            background-color: aliceblue;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="glyphicon-retweet"></i>
                    <asp:Button ID="cmdSsn" runat="server" CssClass="btn btn-primary" Text="Back To Session" OnClick="cmdSsn_Click" />
                </a>
            </li>
            <li>
                <a>
                    <i class="icon-check"></i>
                    <asp:Button ID="cmdclaim" runat="server" CssClass="btn btn-primary" Text="Claim Managment" OnClick="cmdclaim_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-edit"></i>Client Follow Up Session
                </h3>
            </div>
            <div class="box-content">
                <%--<--Start General Box-->--%>
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-edit"></i>General</h3>
                    </div>
                    <div class="box-content nopadding">
                        <div class="form-horizontal form-column form-bordered">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Date</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtDate" runat="server" CssClass="input-large datepick"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtNextApnt" class="control-label">Next Appointment</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtNextApnt" runat="server" CssClass="input-large datepick"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtOutTime" class="control-label">In Time</label>
                                        <div class="controls">
                                            <div class="bootstrap-timepicker">
                                                <asp:TextBox ID="txtIntime" runat="server" CssClass="input-small timepick"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtOutTime" class="control-label">Out Time</label>
                                        <div class="controls">
                                            <div class="bootstrap-timepicker">
                                                <asp:TextBox ID="txtOutTime" runat="server" CssClass="input-small timepick" onblur="calculatehrs"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtTotalPay" class="control-label">Total Hours</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtTotalHrs" runat="server" CssClass="input-medium" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6">
                                    <div class="control-group">
                                        <label for="ddlpos" class="control-label">Place Of Service</label>
                                        <div class="controls">
                                            <UC:PosMaster ID="ddlPos" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="ddlpayer" class="control-label">Payer</label>
                                        <div class="controls">
                                            <UC:InscCMaster ID="ddlPayer" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="ddlFacilities" class="control-label">Facilities</label>
                                        <div class="controls">
                                            <UC:FacalityMaster ID="ddlFclty" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="ddlProvider" class="control-label">Provider</label>
                                        <div class="controls">
                                            <UC:ProviderMaster ID="ddlProvider" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtShow" class="control-label">Show</label>
                                        <div class="controls">
                                            <UC:StaticMaster ID="ddlShow" runat="server" DisplayFlag="SHOW" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<--Star Cash Detail-->--%>
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-edit"></i>Cash Note</h3>
                    </div>
                    <div class="box-content">
                        <div class="span12">
                            <div class="form-horizontal form-column form-bordered">
                                <div class="row-fluid">
                                    <asp:HiddenField ID="fcn_id" runat="server" Value="0" />
                                    <div class="box box-bordered box-condensed box-small">
                                        <div class="box-title">
                                            <h3><i class="icon-asterisk"></i>Subjective</h3>
                                        </div>
                                        <div class="box-content nopadding">
                                            <div class="control-group">
                                                <label for="txtreason" class="control-label">Reason For Visit</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtVisitReason" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box box-bordered box-condensed box-small">
                                        <div class="box-title">
                                            <h3><i class="icon-asterisk"></i>Objective</h3>
                                        </div>
                                        <div class="box-content nopadding">
                                            <div class="control-group">
                                                <label for="txtbehaviour" class="control-label">Behavior</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtbehaviour" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="txtreasponse" class="control-label">Response</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtreasponse" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="txtbehaviour" class="control-label">Who is Present</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtwhopresent" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box box-bordered box-condensed box-small">
                                        <div class="box-title">
                                            <h3><i class="icon-asterisk"></i>Assessment</h3>
                                        </div>
                                        <div class="box-content nopadding">
                                            <div class="row-fluid">
                                                <asp:HiddenField runat="server" ID="TotalChrg" Value="0" />
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <table class="table table-hover table-nomargin table-condensed table-bordered" style="width: 100%;">
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="3">** Diagnosis Code **</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Select Pointer</strong>
                                                                        <br />
                                                                        <asp:DropDownList ID="ddlpointer" runat="server" CssClass="select2-me input-small">
                                                                            <asp:ListItem Value="a1">A(1)</asp:ListItem>
                                                                            <asp:ListItem Value="b2">B(2)</asp:ListItem>
                                                                            <asp:ListItem Value="c3">C(3)</asp:ListItem>
                                                                            <asp:ListItem Value="d4">D(4)</asp:ListItem>
                                                                            <asp:ListItem Value="e5">E(5)</asp:ListItem>
                                                                            <asp:ListItem Value="f6">F(6)</asp:ListItem>
                                                                            <asp:ListItem Value="g7">G(7)</asp:ListItem>
                                                                            <asp:ListItem Value="h8">H(8)</asp:ListItem>
                                                                            <asp:ListItem Value="i9">I(9)</asp:ListItem>
                                                                            <asp:ListItem Value="j10">J(10)</asp:ListItem>
                                                                            <asp:ListItem Value="k11">K(11)</asp:ListItem>
                                                                            <asp:ListItem Value="l12">L(12)</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Select Code</strong>
                                                                        <br />
                                                                        <UC:DignosisCodeMaster ID="ddldgcode" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lnkAddDcode" CssClass="btn btn-primary" runat="server" OnClick="lnkAddDcode_Click"><i class="icon-plus-sign"></i> Add</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <asp:Repeater ID="rptDignosisCode" runat="server" OnItemCommand="rptDignosisCode_ItemCommand" >
                                                                            <HeaderTemplate>
                                                                                <table class="table table-hover table-nomargin table-condensed table-bordered" style="width: 100%;">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Pointer</th>
                                                                                            <th>Dignosis Code</th>
                                                                                            <th>Description</th>
                                                                                            <th></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td style="text-align: center"><%# Eval("fd_pointer") %></td>
                                                                                    <td style="text-align: center"><%# Eval("fd_dignosiscode") %></td>
                                                                                    <td style="text-align: center"><%# Eval("dcm_description") %></td>
                                                                                    <td style="text-align: center">
                                                                                        <asp:LinkButton ID="cmdDCdlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "fd_id") %>' rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i> </asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </tbody>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="span12">
                                                        <table class="table table-hover table-nomargin table-condensed table-bordered" style="width: 100%;">
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="4">** Diagnosis Procedure **</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <strong>CPT Code</strong>
                                                                        <br />
                                                                        <UC:CptMaster ID="ddlCptCode" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <strong>Mod A</strong>
                                                                        <br />
                                                                        <asp:TextBox ID="txtmoda" runat="server" CssClass="input-small"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Mod C</strong>
                                                                        <br />
                                                                        <asp:TextBox ID="txtmodc" runat="server" CssClass="input-small"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Dignosis Code</strong>
                                                                        <br />
                                                                        <asp:TextBox ID="txtdignosis" runat="server" CssClass="input-small"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Line Charge</strong>
                                                                        <br />
                                                                        <asp:TextBox ID="txtCharge" runat="server" CssClass="input-medium"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Mod B</strong>
                                                                        <br />
                                                                        <asp:TextBox ID="txtmodb" runat="server" CssClass="input-small"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <strong>Mod D</strong>
                                                                        <br />
                                                                        <asp:TextBox ID="txtmodd" runat="server" CssClass="input-small"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="cmdSaveandNew" CssClass="btn btn-primary" runat="server" OnClick="cmdSaveandNew_Click" CommandArgument="SAVEANDNEW"><i class="icon-plus-sign"></i> Add</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <asp:Repeater ID="rptDgnProcedure" runat="server" OnItemCommand="rptDgnProcedure_ItemCommand" OnItemDataBound="rptDgnProcedure_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table class="table table-hover table-nomargin table-condensed table-bordered" style="width: 100%;">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>CPT Code</th>
                                                                                            <th>Description</th>
                                                                                            <th>Pointer</th>
                                                                                            <th>Charge</th>
                                                                                            <th>Mod A</th>
                                                                                            <th>Mod B</th>
                                                                                            <th>Mod C</th>
                                                                                            <th>Mod D</th>
                                                                                            <th></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td style="text-align: center"><%# Eval("sdp_cptcode") %></td>
                                                                                    <td style="text-align: center"><%# Eval("cd_description") %></td>
                                                                                    <td style="text-align: center"><%# Eval("sdp_dignosiscode") %></td>
                                                                                    <td style="text-align: center"><%# String.Format("{0:C}",Eval("sdp_linecharge")) %></td>
                                                                                    <td style="text-align: center"><%# Eval("sdp_moda") %></td>
                                                                                    <td style="text-align: center"><%# Eval("sdp_modb") %></td>
                                                                                    <td style="text-align: center"><%# Eval("sdp_modc") %></td>
                                                                                    <td style="text-align: center"><%# Eval("sdp_modd") %></td>
                                                                                    <td style="text-align: center">
                                                                                        <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "sdp_id") %>'><i class="icon-edit-sign"></i> &nbsp;Edit</asp:LinkButton>
                                                                                        <asp:LinkButton ID="cmdDlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "sdp_id") %>' rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i> </asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </tbody>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="txtreason" class="control-label">Interventation</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtintervatation" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box box-bordered box-condensed box-small">
                                        <div class="box-title">
                                            <h3><i class="icon-asterisk"></i>Planning</h3>
                                        </div>
                                        <div class="box-content nopadding">
                                            <div class="span6">
                                                <div class="control-group">
                                                    <label for="txttreatmentplan" class="control-label">Treatment Plan/ Goal</label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txttreatmentplan" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="txtprogress" class="control-label">Progress toward Goal</label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtprogress" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span6">
                                                <div class="control-group">
                                                    <label for="txtnewissue" class="control-label">New Clinic Issue</label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtnewissue" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="txtnextvisitplan" class="control-label">Next Visit Plan</label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtnextvisitplan" runat="server" CssClass="input-block-level" TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<--Start Billing Detail -->--%>
                    <div class="box box-bordered box-color">
                        <div class="box-title">
                            <h3><i class="icon-edit"></i>Billing Detail</h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class="form-horizontal form-column form-bordered">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="control-group">
                                            <label for="txtTotalCharged" class="control-label">Total Charged</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtTotalCharged" runat="server" CssClass="input-medium"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="txtClientPaymentReceived" class="control-label">Client Payment</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtClientPayment" runat="server" CssClass="input-medium"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="txtcofacilitesFee" class="control-label">Insurance Payment</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtInsurancepayment" runat="server" CssClass="input-medium"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="control-group">
                                            <label for="txtBilledClient" class="control-label">Adjustment / Writeoff</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtAdjustmnt" runat="server" CssClass="input-medium" onblur="javascrip:calculatevalues()"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="txtTotalPaid" class="control-label">Total Recieved</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtTotalRcved" runat="server" CssClass="input-medium"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="txtBalanceDue" class="control-label">Balance Due</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtBalanceDue" runat="server" CssClass="input-medium"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="form-actions">
                    <asp:LinkButton ID="cmdSave" CssClass="btn btn-primary" runat="server" CommandArgument="SAVEANDNEXT" OnClick="cmdSave_Click"><i class="glyphicon-circle_arrow_right"></i> Save</asp:LinkButton>
                    <asp:LinkButton ID="cmdCancel" CssClass="btn" runat="server" OnClick="cmdCancel_Click"><i class="icon-remove-circle"></i> Cancel</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $("#<%=txtIntime.ClientID %>").timepicker({
            showMeridian: false,
        });
        $("#<%=txtOutTime.ClientID %>").timepicker({
            showMeridian: false,
        });
        function calculatehrs() {
            p = "1/1/1970 ";
            var timein = $("#<%=txtIntime.ClientID %>").val();
            var timeout = $("#<%=txtOutTime.ClientID %>").val();

            var totalHrs = new Date(new Date(p + timeout) - new Date(p + timein)).toUTCString().split(" ")[4];
            $("#<%=txtTotalHrs.ClientID %>").val(totalHrs);
        }

        function calculatevalues() {
            var totalcharge = $("#<%=txtTotalCharged.ClientID %>").val();
            var clientpayment = $("#<%=txtClientPayment.ClientID %>").val();
            var insuranceoayment = $("#<%=txtInsurancepayment.ClientID %>").val();
            var adjustment = $("#<%=txtAdjustmnt.ClientID %>").val();

            var totalrecieved = (parseInt(clientpayment) + parseInt(insuranceoayment)) - parseInt(adjustment);
            //var balancedue = parseInt(totalcharge) - (parseInt(clientpayment) + parseInt(insuranceoayment) + parseInt(adjustment));
            var balancedue = parseInt(totalcharge) - parseInt(totalrecieved)
            $("#<%=txtTotalRcved.ClientID %>").val(totalrecieved);
            $("#<%=txtBalanceDue.ClientID %>").val(balancedue);
        }
    </script>
</asp:Content>

