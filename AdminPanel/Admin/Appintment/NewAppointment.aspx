﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NewAppointment.aspx.cs" Inherits="Admin_Appintment_NewAppointment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus"></i>
                    <asp:Button ID="cmdMyAppoinment" runat="server" CssClass="btn" Text="My Appointment" OnClick="cmdMyAppoinment_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <%--<iframe src="http://lifestyletherapy.appointy.com/?isGadget=1" height="555px" scrolling="auto" frameborder="0" allowtransparency="true"></iframe>--%>
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-calendar"></i>
                        Take Appointment
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-column form-bordered'>
                         <% if (HttpContext.Current.User.IsInRole("member") || HttpContext.Current.User.IsInRole("administrator"))
                               { %>
                        <div class="control-group">
                            <label for="ddltherapist" class="control-label">Select therapist</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddltherapist" runat="server" AutoPostBack="true" CssClass="select2-me input-xlarge" OnSelectedIndexChanged="ddltherapist_SelectedIndexChanged" ></asp:DropDownList>
                            </div>
                        </div>
                         <%} %>
                        <div class="control-group">
                            <label for="txtAppntDate" class="control-label">Services</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlApntCourse" runat="server" CssClass="select2-me input-xlarge">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="ddlClient" class="control-label">Select Client</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlClient" runat="server" CssClass="select2-me input-xlarge" ></asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtTitle" class="control-label">Subject</label>
                            <div class="controls">
                                <asp:TextBox ID="txtTitle" runat="server" placeholder="Subject" MaxLength="50" CssClass="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtDescription" class="control-label">Description</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDescription" runat="server" placeholder="Description" Width="99%" TextMode="MultiLine"  CssClass="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtAppntDate" class="control-label">Date</label>
                            <div class="controls">
                                <asp:TextBox ID="txtAppntDate" runat="server" placeholder="mm/dd/yyyy" CssClass="datepick"></asp:TextBox>
                                <asp:Button ID="cmdGetAvailability" runat="server" CssClass="btn btn-primary" Text="Show Available Time" OnClick="cmdGetAvailability_Click"  />
                                <span class="help-block">Please enter date in <b>MM/DD/YYYY</b> format</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="lstTime" class="control-label">Available Time</label>
                            <div class="controls">
                                <asp:ListBox ID="lstTime" runat="server" Height="150px" CssClass="list"></asp:ListBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:Button ID="cmdAddPontment" runat="server" CssClass="btn btn-primary" Text="Take Appointment" OnClick="cmdAddPontment_Click"  />
                            <br />
                            <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
