﻿<%@ Page Title="Symptoms" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientMentalHealth.aspx.cs" Inherits="Clients_Assessment_ClientSymptoms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <asp:HiddenField ID="AssesmentStep" runat="server" Value="16" />
     <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-github-alt"></i>
                Client's Mental Health Information
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:Symptoms ID="ucSymptoms" runat="server" />
        </div>
    </div>
     <div class="row-fluid">
        <hr />
        <div class="span12">
            <asp:Button ID="cmdNext" runat="server" Text="Next Psychological >>" CssClass="btn btn-primary" OnClick="cmdNext_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>

