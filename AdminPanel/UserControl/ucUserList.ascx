﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUserList.ascx.cs" Inherits="UserControl_ucUserList" %>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-color box-bordered">
            <div class="box-title">
                <h3>
                    <i class="icon-table"></i>
                    <asp:Label ID="lblBoxHeader" runat="server" Text=""></asp:Label>
                </h3>
            </div>
            <div class="box-content nopadding">
                <asp:Label ID="lblMsg" runat="server" Text="Label" ForeColor="red" Font-Bold="true" Visible="false"></asp:Label>
                <asp:Repeater ID="dtrUserDisplay" runat="server" OnItemDataBound="dtrUserDisplay_ItemDataBound">
                    <HeaderTemplate>
                        <table class="table table-hover table-nomargin dataTable table-bordered">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Email Address</th>
                                    <th>Is Approved</th>
                                    <th>Locked Out</th>
                                    <th>Create Date</th>
                                    <th>Last Login</th>
                                    <th>Is Online</th>
                                    <th>&nbsp;</th>
                                    <%--<th>Profile</th>--%>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# Eval("UserName") %></td>
                            <td><%# Eval("Email") %></td>
                            <td><%# Eval("IsApproved") %></td>
                            <td><%# Eval("IsLockedOut") %></td>
                            <td><%# Eval("CreationDate") %></td>
                            <td><%# Eval("LastLoginDate") %></td>
                            <td><%# Eval("IsOnline") %></td>
                             <td style="text-align: center;">
                                    <asp:Button ID="cmdDelete" runat="server" CssClass="btn btn-info" CommandName="DEACTIVE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProviderUserKey") %>' OnClick="cmdDelete_Click"  />
                             </td>
                            <%--<td>
                                    <a class="clientmanagelink" id="UserLink" clientid='<%# Eval("username") %>' href="#">Manage</a>
                                </td>--%>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                            </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
