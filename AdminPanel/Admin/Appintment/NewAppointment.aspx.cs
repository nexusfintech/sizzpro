﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Globalization;
using clsDAL;
using System.Data;
using System.Text.RegularExpressions;

public partial class Admin_Appintment_NewAppointment : System.Web.UI.Page
{
    clsAppointmentCourse clsCRS = new clsAppointmentCourse();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsCourseMapping clsCRM = new clsCourseMapping();
    clsAppointmentMaster clsAPNT = new clsAppointmentMaster();
    clsSettings clsSTG = new clsSettings();
    clsEmailAccounts clsMailAccount = new clsEmailAccounts();
    clsEmailTemplate clsMailTemplate = new clsEmailTemplate();
    clsSendMail clsMailSend = new clsSendMail();

    private void BindCombo()
    {
        if (User.IsInRole("administrator"))
        {
            ddltherapist.Items.Clear();
            ddltherapist.SelectedIndex = -1;
            ddltherapist.SelectedValue = null;
            ddltherapist.ClearSelection();
            ddltherapist.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddltherapist.Items.Add(lst);
            ddltherapist.DataValueField = "UserId";
            ddltherapist.DataTextField = "UserName";
            DataTable dtResult = clsCPM.GetAllComboByRole("User");
            ddltherapist.DataSource = dtResult;
            ddltherapist.DataBind();
        }
        else
        {
            ddltherapist.Items.Clear();
            ddltherapist.SelectedIndex = -1;
            ddltherapist.SelectedValue = null;
            ddltherapist.ClearSelection();
            ddltherapist.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddltherapist.Items.Add(lst);
            ddltherapist.DataValueField = "UserId";
            ddltherapist.DataTextField = "UserName";
            DataTable dtResult = clsCPM.GetAllComboByRoleandParent("User", (Guid)Membership.GetUser().ProviderUserKey);
            ddltherapist.DataSource = dtResult;
            ddltherapist.DataBind();
        }
    }

    private Boolean ValidateDate(string strValue)
    {
        Match vldDate = Regex.Match(strValue, "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$");
        return vldDate.Success;
    }

    private string ValidatePage()
    {
        string strResult = "";
        if (User.IsInRole("member"))
        {
            if (ddltherapist.SelectedItem.Value == "0")
            { strResult += "Select First Therapist <br/>"; }
        }
        if (ddlApntCourse.SelectedItem.Value == "0")
        { strResult += "Select Course <br/>"; }
        if (txtTitle.Text.Trim() == "")
        { strResult += "Please Input Titel <br/>"; }
        if (txtDescription.Text.Trim() == "")
        { strResult += "Please Input Description <br/>"; }
        if (!ValidateDate(txtAppntDate.Text.Trim()))
        { strResult += "Input proper appointment date"; }
        else
        {
            DateTime dtCurDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime dtApntDate;
            if (!(DateTime.TryParseExact(txtAppntDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtApntDate)))
            {
                strResult += "Invalid Appointment Date format please enter in mm/dd/yyyy";
            }
            else
            {
                if (dtApntDate < dtCurDate)
                {
                    strResult += "Previous days appointment date is not valid..!!";
                }
            }
        }
        return strResult;
    }

    private void BindCourseCombo()
    {
        Guid guUsrID = new Guid();
        DataTable dtGetData = new DataTable();
        if (User.IsInRole("member") || User.IsInRole("administrator"))
        {
            guUsrID = new Guid(ddltherapist.SelectedItem.Value.ToString());
            dtGetData = clsCRM.GetUserWiseCombo(guUsrID);
        }
        else
        {
            guUsrID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            dtGetData = clsCRM.GetUserWiseCombo(guUsrID);
        }
        //DataTable dtGetData = clsCRM.GetUserWiseCombo(guUsrID);
        if (dtGetData.Rows.Count == 0)
        {
            ddlApntCourse.Items.Clear();
            ddlApntCourse.SelectedIndex = -1;
            ddlApntCourse.SelectedValue = null;
            ddlApntCourse.ClearSelection();
            ddlApntCourse.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-No Service Found->>";
            lst.Selected = true;
            ddlApntCourse.Items.Add(lst);
        }
        else
        {
            ddlApntCourse.Items.Clear();
            ddlApntCourse.SelectedIndex = -1;
            ddlApntCourse.SelectedValue = null;
            ddlApntCourse.ClearSelection();
            ddlApntCourse.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddlApntCourse.Items.Add(lst);
            ddlApntCourse.DataValueField = "crs_id";
            ddlApntCourse.DataTextField = "crs_coursename";
            ddlApntCourse.DataSource = dtGetData;
            ddlApntCourse.DataBind();
        }
    }
    private void Bindclient()
    {
        DataTable dtGetData = clsCPM.GetAllComboByRole("Client");
        if (dtGetData.Rows.Count == 0)
        {
            ddlClient.Items.Clear();
            ddlClient.SelectedIndex = -1;
            ddlClient.SelectedValue = null;
            ddlClient.ClearSelection();
            ddlClient.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-No Client Found->>";
            lst.Selected = true;
            ddlClient.Items.Add(lst);
        }
        else
        {
            ddlClient.Items.Clear();
            ddlClient.SelectedIndex = -1;
            ddlClient.SelectedValue = null;
            ddlClient.ClearSelection();
            ddlClient.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select Client->>";
            lst.Selected = true;
            ddlClient.Items.Add(lst);
            ddlClient.DataValueField = "UserId";
            ddlClient.DataTextField = "UserName";
            ddlClient.DataSource = dtGetData;
            ddlClient.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (User.IsInRole("member") || User.IsInRole("administrator"))
            {
                BindCombo();
            }
            else
            {
                BindCourseCombo();
                Bindclient();
            }
            string strDate = DateTime.Now.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            DateTime dtsysdate;
            if (DateTime.TryParseExact(strDate, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out dtsysdate))
            {
                txtAppntDate.Text = dtsysdate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
        }
    }

    protected void cmdGetAvailability_Click(object sender, EventArgs e)
    {
        string strResult = ValidatePage();
        if (strResult != "")
        {
            lblMessage.Text = strResult; return;
        }
        lblMessage.Text = "";
        lstTime.Items.Clear();
        Guid guUserid;
        if (User.IsInRole("member") || User.IsInRole("administrator"))
        {
            guUserid = new Guid(ddltherapist.SelectedItem.Value);
            clsAPNT.apt_userid = guUserid;
        }
        else
        {
            guUserid = (Guid)Membership.GetUser().ProviderUserKey;
            clsAPNT.apt_userid = guUserid;
        }

        Int64 intCRSID = Convert.ToInt64(ddlApntCourse.SelectedItem.Value);
        Boolean blnResult = clsCRS.GetRecordByIDInProperties(intCRSID);
        if (blnResult == true)
        {
            Int64 intInterval = clsCRS.crs_interval;
            //Int64 intProgTime = clsCRS.crs_minutes;
            DateTime dtCurDate;
            if (DateTime.TryParseExact(txtAppntDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtCurDate))
            {
                Boolean blnWH = clsSTG.GetRecordByFlagUserInProperties("WHSTARTTIME", guUserid);
                DateTime dtWHStart = clsSTG.stg_datetimevalue;
                clsSTG.GetRecordByFlagUserInProperties("WHENDTIME", guUserid);
                DateTime dtWHEnd = clsSTG.stg_datetimevalue;
                if (blnWH == true)
                {
                    DateTime dtstartTime;
                    //DateTime today = DateTime.Now;
                    //if (dtCurDate.Date == today.Date)
                    //{
                    //     dtstartTime = new DateTime(dtCurDate.Year, dtCurDate.Month, dtCurDate.Day, today.Hour, today.Minute, today.Second);
                    //}
                    //else
                    //{
                    dtstartTime = new DateTime(dtCurDate.Year, dtCurDate.Month, dtCurDate.Day, dtWHStart.Hour, dtWHStart.Minute, dtWHStart.Second);
                    //}
                    DateTime dtEndTime = new DateTime(dtCurDate.Year, dtCurDate.Month, dtCurDate.Day, dtWHEnd.Hour, dtWHEnd.Minute, dtWHEnd.Second);
                    while (dtstartTime <= dtEndTime.AddMinutes(intInterval * -1))
                    {
                        clsAPNT.apt_apointmentdate = dtCurDate;
                        //dtstartTime = dtstartTime.AddMinutes(intInterval);
                        clsAPNT.apt_startdatetime = dtstartTime;
                        DateTime dtToTime = new DateTime();
                        dtToTime = dtstartTime;
                        dtToTime = dtToTime.AddMinutes(Convert.ToDouble(intInterval));
                        clsAPNT.apt_enddatetime = dtToTime;
                        DataTable dtResult = clsAPNT.GetTimeWiseAppointment();
                        if (dtResult.Rows.Count == 0)
                        {
                            DateTime dtnw = DateTime.Now;
                            if (dtstartTime > dtnw)
                            {
                                ListItem lst = new ListItem();
                                lst.Value = dtstartTime.ToString("MM/dd/yyyy hh:mm:ss tt");
                                lst.Text = dtstartTime.ToString("HH:mm:ss");
                                lst.Attributes.Add("class", "itemstwo");
                                lstTime.Items.Add(lst);
                            }
                        }
                        dtstartTime = dtstartTime.AddMinutes(5);
                    }
                }
                else { lblMessage.Text = "Working Hourse not define please contact to website administrator"; }
            }
            else { lblMessage.Text = "Invalid Appointment Date format please input in mm/dd/yyyy"; }
        }
    }

    protected void cmdAddPontment_Click(object sender, EventArgs e)
    {
        if (lstTime.Items.Count > 0)
        {
            DateTime dtApntDate;
            if (DateTime.TryParseExact(txtAppntDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtApntDate))
            {
                clsCRS.GetRecordByIDInProperties(Convert.ToInt64(ddlApntCourse.SelectedItem.Value));
                clsAPNT.apt_apointmentdate = dtApntDate;
                clsAPNT.apt_clientid = new Guid(ddlClient.SelectedItem.Value);
                clsAPNT.apt_code = DateTime.Now.Ticks.ToString();
                clsAPNT.apt_courseid = Convert.ToInt64(ddlApntCourse.SelectedItem.Value);
                clsAPNT.apt_description = "Title : " + txtTitle.Text.Trim() + Environment.NewLine + txtDescription.Text;
                clsAPNT.apt_title = ddlClient.SelectedItem.Text;
                if (User.IsInRole("member") || User.IsInRole("administrator"))
                { 
                    clsAPNT.apt_userid = new Guid(ddltherapist.SelectedItem.Value); 
                }
                else 
                { 
                    clsAPNT.apt_userid = new Guid(Membership.GetUser().ProviderUserKey.ToString()); 
                }
                DateTime dtStartDate = DateTime.Parse(lstTime.SelectedItem.Value.ToString(), CultureInfo.InvariantCulture, DateTimeStyles.None);
                clsAPNT.apt_startdatetime = dtStartDate;
                DateTime dtEndDate = dtStartDate.AddMinutes(clsCRS.crs_interval);
                clsAPNT.apt_enddatetime = dtEndDate;
                clsAPNT.apt_iscompleted = true;
                Boolean blnSave = clsAPNT.Save();
                if (blnSave == true)
                {
                    Boolean blnIsMaped = clsCPM.CheckForMapping(clsAPNT.apt_userid,clsAPNT.apt_clientid);
                    if(!blnIsMaped)
                    {
                        clsCPM.cpm_parentid = clsAPNT.apt_userid;
                        clsCPM.cpm_userid = clsAPNT.apt_clientid;
                        Boolean bltSave = clsCPM.Save();
                    }
                    Boolean blnTemplate = clsMailTemplate.GetRecordByNameInProperties("AppoinmentCreate");
                    if (blnTemplate == true)
                    {
                        Boolean blnAccountBind = clsMailSend.BindEmailAccount("azure_3b588217fd94a16e4729eeb6efd200b4@azure.com");
                        if (blnAccountBind == true)
                        {
                            clsMailSend.ToMailAddress = Membership.GetUser(clsAPNT.apt_clientid).Email;
                            clsMailSend.Subject = clsMailTemplate.emt_templatesubject;
                            if (clsMailTemplate.emt_ccmail.Trim() != "") 
                            {
                                clsMailSend.CCMailAddress = clsMailTemplate.emt_ccmail.Trim(); 
                            }
                            if (clsMailTemplate.emt_bccmail.Trim() != "") 
                            {
                                clsMailSend.BCCMailAddress = clsMailTemplate.emt_bccmail.Trim(); 
                            }
                            String strHtmlBody = clsMailTemplate.emt_templatebody.Trim();
                            strHtmlBody = strHtmlBody.Replace("{username}", Membership.GetUser(clsAPNT.apt_clientid).UserName);
                            strHtmlBody = strHtmlBody.Replace("{appoinmentuser}", Membership.GetUser(clsAPNT.apt_userid).UserName);
                            strHtmlBody = strHtmlBody.Replace("{appoinmentcourse}", ddlApntCourse.SelectedItem.Text);
                            strHtmlBody = strHtmlBody.Replace("{appoinmentdate}", clsAPNT.apt_apointmentdate.ToString("MM/dd/yyyy"));
                            strHtmlBody = strHtmlBody.Replace("{appoinmenttime}", clsAPNT.apt_startdatetime.TimeOfDay.ToString());
                            clsMailSend.HtmlBody = strHtmlBody;
                            clsMailSend.Priority = System.Net.Mail.MailPriority.Normal;

                            Boolean blnSendMail = clsMailSend.SendMail();
                            if (blnSendMail == true)
                            {
                                lblMessage.Text = "Appointment Save Successfully";
                                clearefields();
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Error in Mail Account's Setting Bind To Mail Sender Engine";
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Appoinmtnt Create Successfully. but error in Mail Sending.. please resend manualy";
                    }
                }
            }
            else { lblMessage.Text = "Invalid appointment date format please input in mm/dd/yyyy"; }
        }
        else { lblMessage.Text = "No Any Available Time"; }
    }

    protected void ddltherapist_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCourseCombo();
        Bindclient();
    }

    protected void cmdMyAppoinment_Click(object sender, EventArgs e)
    {
        Response.Redirect("MyAppointment.aspx");
    }

    public void clearefields()
    {
        txtAppntDate.Text = string.Empty;
        txtDescription.Text = string.Empty;
        txtTitle.Text = string.Empty;
        ddlApntCourse.SelectedIndex = 0;
        if(Page.User.IsInRole("bember") || Page.User.IsInRole("administrator"))
        {
            ddltherapist.SelectedIndex = 0;
        }
        Bindclient();
        lstTime.Items.Clear();
    }
}