﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class Users_UserManage : System.Web.UI.Page
{
    clsEmailAccounts clsEmailAct = new clsEmailAccounts();
    clsEmailTemplate clsEmlTemplate = new clsEmailTemplate();
    clsSendMail clsMailSend = new clsSendMail();
    clsProfileMaster clsPRM = new clsProfileMaster();
    clsUsers clsUser = new clsUsers();

    string strMemberkey;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            strMemberkey = Request.QueryString["memberkey"].ToString();
            lnkProfile.NavigateUrl = "~/Users/UserProfileManagement.aspx?memberkey=" + MySession.Current.UserGuID;
            MembershipUser mu;
            mu = Membership.GetUser(MySession.Current.UserLoginName);
            txtEmail.Text = mu.Email;
        }
    }

    protected void cmdChnagePass_Click(object sender, EventArgs e)
    {
        if (txtOldPassword.Text == "")
        { return; }
        if (txtNewPassword.Text == "")
        { return; }
        if (txtConformPAssword.Text == "")
        { return; }
        try
        {
            if (txtNewPassword.Text == txtConformPAssword.Text)
            {
                MembershipUser mu = Membership.GetUser(MySession.Current.UserLoginName);
                Boolean blnResult = mu.ChangePassword(txtOldPassword.Text, txtNewPassword.Text);
                if (blnResult == true)
                {
                    lblMessage.Text = "Password Changed Successfully";
                     Boolean blnRsltTmplt = clsEmlTemplate.GetRecordByNameInProperties("PasswordChanged");
                     if (blnRsltTmplt == true)
                     {
                         Boolean blnMailAccount = clsEmailAct.GetRecordByIDInProperties(clsEmlTemplate.emt_mailaccountid);
                         if (blnMailAccount == true)
                         {
                             Boolean blnAccountBind = clsMailSend.BindEmailAccount(clsEmailAct.ema_emailaddress);
                            if (blnAccountBind == true)
                            {
                                clsMailSend.ToMailAddress = txtEmail.Text.Trim();
                                clsMailSend.Subject = clsEmlTemplate.emt_templatesubject;
                                if (clsEmlTemplate.emt_ccmail.Trim() != "") { clsMailSend.CCMailAddress = clsEmlTemplate.emt_ccmail.Trim(); }
                                if (clsEmlTemplate.emt_bccmail.Trim() != "") { clsMailSend.BCCMailAddress = clsEmlTemplate.emt_bccmail.Trim(); }
                                String strHtmlBody = clsEmlTemplate.emt_templatebody.Trim();
                                strHtmlBody = strHtmlBody.Replace("{username}", mu.UserName);
                                clsMailSend.HtmlBody = strHtmlBody;
                                clsMailSend.Priority = System.Net.Mail.MailPriority.Normal;
                                Boolean blnSendMail = clsMailSend.SendMail();
                                if (blnSendMail == true)
                                {
                                    Response.Redirect("~/Default.aspx");
                                }
                                else
                                {
                                    lblEmailChange.Text = "your email changed successfully but unable to send email ";
                                }
                            }
                             else
                            {
                                lblMessage.Text = "your Password changed but unable to bind mail account to sending mail ";
                            }
                         }
                         else
                         {
                            lblMessage.Text = "your Password changed but unable to find mail account to sending mail ";
                         }
                     }
                     else
                     {
                         lblMessage.Text = "your Password changed but ,unable to find email template for sending mail " + clsEmlTemplate.GetSetErrorMessage;
                     }
                }
                else { lblMessage.Text = "Password Not Changed"; }
            }
            else { lblMessage.Text = "Confirm Password Not Matched"; }
        }
        catch (Exception ex)
        { lblMessage.Text = ex.Message; }
    }

    protected void cmdChangeEmail_Click(object sender, EventArgs e)
    {
        if (txtEmail.Text != string.Empty)
        {
            MembershipUser mu;
            mu = Membership.GetUser(MySession.Current.UserLoginName);
            mu.Email = txtEmail.Text.Trim();
            try
            {
                Membership.UpdateUser(mu);
            }
            catch (Exception ex)
            {
                lblEmailChange.Text = ex.Message;
                return;
            }
            Boolean blnPrfl = clsPRM.GetRecordByMemberKeyInProperties(mu.ProviderUserKey.ToString());
            if (blnPrfl == true)
            {
                clsPRM.prm_emailid = txtEmail.Text.ToString();
                Boolean blnupdtprfl = clsPRM.Update(clsPRM.prm_id);
                if (blnupdtprfl == true)
                {
                    Boolean blnResult = clsEmlTemplate.GetRecordByNameInProperties("EmailChanged");
                    if (blnResult == true)
                    {
                        Boolean blnMailAccount = clsEmailAct.GetRecordByIDInProperties(clsEmlTemplate.emt_mailaccountid);
                        if (blnMailAccount == true)
                        {
                            Boolean blnAccountBind = clsMailSend.BindEmailAccount(clsEmailAct.ema_emailaddress);
                            if (blnAccountBind == true)
                            {
                                clsMailSend.ToMailAddress = txtEmail.Text.Trim();
                                clsMailSend.Subject = clsEmlTemplate.emt_templatesubject;
                                if (clsEmlTemplate.emt_ccmail.Trim() != "") { clsMailSend.CCMailAddress = clsEmlTemplate.emt_ccmail.Trim(); }
                                if (clsEmlTemplate.emt_bccmail.Trim() != "") { clsMailSend.BCCMailAddress = clsEmlTemplate.emt_bccmail.Trim(); }
                                String strHtmlBody = clsEmlTemplate.emt_templatebody.Trim();
                                strHtmlBody = strHtmlBody.Replace("{username}", mu.UserName);
                                clsMailSend.HtmlBody = strHtmlBody;
                                clsMailSend.Priority = System.Net.Mail.MailPriority.Normal;
                                Boolean blnSendMail = clsMailSend.SendMail();
                                if (blnSendMail == true)
                                {
                                    lblEmailChange.Text = "Your email changed successfully . Now you will get all updates on your new comfigured email.";
                                }
                                else
                                {
                                    lblEmailChange.Text = "your email changed successfully but unable to send email ";
                                }
                            }
                            else
                            {
                                lblEmailChange.Text = "your email changed but unable to bind mail account to sending mail ";
                            }
                        }
                        else
                        {
                            lblEmailChange.Text = "your email changed but unable to get mail account to sending mail " + clsEmailAct.GetSetErrorMessage;
                        }
                    }
                    else
                    {
                        lblEmailChange.Text = "your email changed but ,unable to find email template for sending mail " + clsEmlTemplate.GetSetErrorMessage;
                    }
                }
                else
                {
                    lblEmailChange.Text = "unable to update your profile email . please change it manually";
                }
            }
            else
            {
                lblEmailChange.Text = "Unable to find Profile " + clsPRM.GetSetErrorMessage;
            }
        }
    }

    protected void btnChangUserName_Click(object sender, EventArgs e)
    {
        if (txtUserName.Text != string.Empty)
        {
            MembershipUser mu;
            mu = Membership.GetUser(MySession.Current.UserLoginName);
            MembershipUser nu = Membership.GetUser(txtUserName.Text.ToString());
            if (nu == null)
            {
                Boolean BlnRslt = clsUser.GetRecordByIDInProperties((Guid)mu.ProviderUserKey);
                if (BlnRslt == true)
                {
                    clsUser.UserName = txtUserName.Text.Trim();
                    Boolean blnUpdtRslt = clsUser.Update(clsUser.UserId);
                    if (blnUpdtRslt == true)
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                    else
                    {
                        lblUserName.Text = "Sorry Error occured during Update " + clsUser.GetSetErrorMessage;
                    }
                }
                else
                {
                    lblUserName.Text = "Sorry No profile found " + clsUser.GetSetErrorMessage;
                }
            }
            else
            {
                lblUserName.Text = "Sorry this username is already exist please choose some other usrename";
                return;
            }
        }
        else
        {
            lblUserName.Text = "please enter your new username";
        }
    }
}