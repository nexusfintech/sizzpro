﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAssessmentLinks.ascx.cs" Inherits="UserControl_Assessment_ucAssessmentLinks" %>
<div class="subnav">
    <h4 class="subnav-title">
        <asp:Label ID="lblName" runat="server"></asp:Label>
    </h4>
    <div class="subnav-title">
        <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Assessment Step</span></a>
    </div>
    <ul class="subnav-menu">
        <li>
            <asp:HyperLink ID="lnkIntro" runat="server"><i class="icon-user"></i> Intro</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkProfile" runat="server"><i class="icon-user"></i> You & Profile</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkFamily" runat="server"><i class="icon-user"></i> Family & Origion</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkspouses" runat="server"><i class="icon-user"></i> Spouses & Signicant</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkSiblings" runat="server"><i class="icon-user"></i> Siblings</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkChildren" runat="server"><i class="icon-user"></i> Childrens</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkInsurance" runat="server"><i class="icon-ambulance"></i> Insurance</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkMedical" runat="server"><i class="icon-stethoscope"></i> Medical</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnksymptoms" runat="server"><i class="icon-github-alt"></i> Symptoms</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkpsychological" runat="server"><i class="icon-heart"></i> Psychological</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkaddiction" runat="server"><i class="glyphicon-smoking"></i> Addiction</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkAssFamily" runat="server"><i class="icon-group"></i> Family</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkproblem" runat="server"><i class="icon-remove-sign"></i> Problem</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkreadiness" runat="server"><i class="glyphicon-eye_open"></i> Readiness</asp:HyperLink>
        </li>
    </ul>
</div>
