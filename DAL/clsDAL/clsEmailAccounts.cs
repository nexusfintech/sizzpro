﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsEmailAccounts
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsEmailAccounts()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsEmailAccounts()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _ema_id; }
            set
            {
                _ema_id = value;
                if (clsCMD.Parameters.Contains("@ema_id"))
                { clsCMD.Parameters["@ema_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _ema_id;
        public Int64 ema_id
        {
            get { return _ema_id; }
            set
            {
                _ema_id = value;
                if (clsCMD.Parameters.Contains("@ema_id"))
                { clsCMD.Parameters["@ema_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _ema_accountname;
        public String ema_accountname
        {
            get { return _ema_accountname; }
            set
            {
                _ema_accountname = value;
                if (clsCMD.Parameters.Contains("@ema_accountname"))
                { clsCMD.Parameters["@ema_accountname"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_accountname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ema_emailaddress;
        public String ema_emailaddress
        {
            get { return _ema_emailaddress; }
            set
            {
                _ema_emailaddress = value;
                if (clsCMD.Parameters.Contains("@ema_emailaddress"))
                { clsCMD.Parameters["@ema_emailaddress"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_emailaddress", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ema_emailpassword;
        public String ema_emailpassword
        {
            get { return _ema_emailpassword; }
            set
            {
                _ema_emailpassword = value;
                if (clsCMD.Parameters.Contains("@ema_emailpassword"))
                { clsCMD.Parameters["@ema_emailpassword"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_emailpassword", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ema_smtpport;
        public String ema_smtpport
        {
            get { return _ema_smtpport; }
            set
            {
                _ema_smtpport = value;
                if (clsCMD.Parameters.Contains("@ema_smtpport"))
                { clsCMD.Parameters["@ema_smtpport"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_smtpport", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ema_smtpserver;
        public String ema_smtpserver
        {
            get { return _ema_smtpserver; }
            set
            {
                _ema_smtpserver = value;
                if (clsCMD.Parameters.Contains("@ema_smtpserver"))
                { clsCMD.Parameters["@ema_smtpserver"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_smtpserver", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ema_priority;
        public String ema_priority
        {
            get { return _ema_priority; }
            set
            {
                _ema_priority = value;
                if (clsCMD.Parameters.Contains("@ema_priority"))
                { clsCMD.Parameters["@ema_priority"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_priority", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ema_subjectencoding;
        public String ema_subjectencoding
        {
            get { return _ema_subjectencoding; }
            set
            {
                _ema_subjectencoding = value;
                if (clsCMD.Parameters.Contains("@ema_subjectencoding"))
                { clsCMD.Parameters["@ema_subjectencoding"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_subjectencoding", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _ema_bodyencoding;
        public String ema_bodyencoding
        {
            get { return _ema_bodyencoding; }
            set
            {
                _ema_bodyencoding = value;
                if (clsCMD.Parameters.Contains("@ema_bodyencoding"))
                { clsCMD.Parameters["@ema_bodyencoding"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_bodyencoding", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Boolean _ema_enablessl;
        public Boolean ema_enablessl
        {
            get { return _ema_enablessl; }
            set
            {
                _ema_enablessl = value;
                if (clsCMD.Parameters.Contains("@ema_enablessl"))
                { clsCMD.Parameters["@ema_enablessl"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_enablessl", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _ema_isbodyhtml;
        public Boolean ema_isbodyhtml
        {
            get { return _ema_isbodyhtml; }
            set
            {
                _ema_isbodyhtml = value;
                if (clsCMD.Parameters.Contains("@ema_isbodyhtml"))
                { clsCMD.Parameters["@ema_isbodyhtml"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_isbodyhtml", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _ema_accountverify;
        public Boolean ema_accountverify
        {
            get { return _ema_accountverify; }
            set
            {
                _ema_accountverify = value;
                if (clsCMD.Parameters.Contains("@ema_accountverify"))
                { clsCMD.Parameters["@ema_accountverify"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_accountverify", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _ema_status;
        public Boolean ema_status
        {
            get { return _ema_status; }
            set
            {
                _ema_status = value;
                if (clsCMD.Parameters.Contains("@ema_status"))
                { clsCMD.Parameters["@ema_status"].Value = value; }
                else { clsCMD.Parameters.Add("@ema_status", SqlDbType.Bit).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                ema_id = Convert.ToInt64(dr["ema_id"]);
                ema_accountname = Convert.ToString(dr["ema_accountname"]);
                ema_emailaddress = Convert.ToString(dr["ema_emailaddress"]);
                ema_emailpassword = Convert.ToString(dr["ema_emailpassword"]);
                ema_smtpport = Convert.ToString(dr["ema_smtpport"]);
                ema_smtpserver = Convert.ToString(dr["ema_smtpserver"]);
                ema_priority = Convert.ToString(dr["ema_priority"]);
                ema_subjectencoding = Convert.ToString(dr["ema_subjectencoding"]);
                ema_bodyencoding = Convert.ToString(dr["ema_bodyencoding"]);
                ema_enablessl = Convert.ToBoolean(dr["ema_enablessl"]);
                ema_isbodyhtml = Convert.ToBoolean(dr["ema_isbodyhtml"]);
                ema_accountverify = Convert.ToBoolean(dr["ema_accountverify"]);
                ema_status = Convert.ToBoolean(dr["ema_status"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            ema_id = Convert.ToInt64("0");
            ema_accountname = Convert.ToString("NA");
            ema_emailaddress = Convert.ToString("NA");
            ema_emailpassword = Convert.ToString("NA");
            ema_smtpport = Convert.ToString("NA");
            ema_smtpserver = Convert.ToString("NA");
            ema_priority = Convert.ToString("NA");
            ema_subjectencoding = Convert.ToString("NA");
            ema_bodyencoding = Convert.ToString("NA");
            ema_enablessl = Convert.ToBoolean(false);
            ema_isbodyhtml = Convert.ToBoolean(false);
            ema_accountverify = Convert.ToBoolean(false);
            ema_status = Convert.ToBoolean(false);
        }
        #endregion

        #region Declare Add/Edit/Delete Function
        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.ema_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailaccountsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();

            return blnResult;
        }

        public Boolean Update(Int64 ema_id)
        {
            Boolean blnResult = false;
            this.ema_id = ema_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailaccountsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 ema_id)
        {
            Boolean blnResult = false;
            this.ema_id = ema_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailaccountsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailaccountsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsEmailAccounts_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsEmailAccounts_PropertiesList> lstResult = new List<clsEmailAccounts_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].ema_id = Convert.ToInt64(dr["ema_id"]);
                lstResult[i].ema_accountname = Convert.ToString(dr["ema_accountname"]);
                lstResult[i].ema_emailaddress = Convert.ToString(dr["ema_emailaddress"]);
                lstResult[i].ema_emailpassword = Convert.ToString(dr["ema_emailpassword"]);
                lstResult[i].ema_smtpport = Convert.ToString(dr["ema_smtpport"]);
                lstResult[i].ema_smtpserver = Convert.ToString(dr["ema_smtpserver"]);
                lstResult[i].ema_priority = Convert.ToString(dr["ema_priority"]);
                lstResult[i].ema_subjectencoding = Convert.ToString(dr["ema_subjectencoding"]);
                lstResult[i].ema_bodyencoding = Convert.ToString(dr["ema_bodyencoding"]);
                lstResult[i].ema_enablessl = Convert.ToBoolean(dr["ema_enablessl"]);
                lstResult[i].ema_isbodyhtml = Convert.ToBoolean(dr["ema_isbodyhtml"]);
                lstResult[i].ema_accountverify = Convert.ToBoolean(dr["ema_accountverify"]);
                lstResult[i].ema_status = Convert.ToBoolean(dr["ema_status"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 ema_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.ema_id = ema_id;
                clsCMD.CommandText = "sp_emailaccountsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByAccountInProperties(String strEmailAccount)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.ema_emailaddress = strEmailAccount;
                clsCMD.CommandText = "sp_emailaccountsGetData";
                SetGetSPFlag = "BYACCOUNT";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 ema_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.ema_id = ema_id;
                clsCMD.CommandText = "sp_emailaccountsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }
    public class clsEmailAccounts_PropertiesList
    {
        public Int64 ema_id { get; set; }
        public String ema_accountname { get; set; }
        public String ema_emailaddress { get; set; }
        public String ema_emailpassword { get; set; }
        public String ema_smtpport { get; set; }
        public String ema_smtpserver { get; set; }
        public String ema_priority { get; set; }
        public String ema_subjectencoding { get; set; }
        public String ema_bodyencoding { get; set; }
        public Boolean ema_enablessl { get; set; }
        public Boolean ema_isbodyhtml { get; set; }
        public Boolean ema_accountverify { get; set; }
        public Boolean ema_status { get; set; }
    }
}