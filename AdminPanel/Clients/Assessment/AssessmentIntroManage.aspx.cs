﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Clients_Assessment_AssessmentIntroManage : System.Web.UI.Page
{
    string strMemberkey = "";
    Int64 intUserid;

    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["form_memberkey"] != null)
        {
            strMemberkey = Session["form_memberkey"].ToString();
            intUserid = Convert.ToInt64(Session["form_userid"]);
            ucIntro.Memberkey = strMemberkey;
        }
        else { Response.Redirect("~/Users/UserClientManage.aspx"); }
    }
    protected void cmdReschedule_Click(object sender, EventArgs e)
    {
        Boolean blnresult = clsASR.DeleteByStepID(1, new Guid(strMemberkey));
        if (blnresult == true)
        {
            //lblMessage.Text = clsASR.GetSetErrorMessage;
        }
        else
        {
           // lblMessage.Text = "Error in Operation..." + clsASR.GetSetErrorMessage;
        }
    }
    protected void cmdNext_Click(object sender, EventArgs e)
    {

    }
}