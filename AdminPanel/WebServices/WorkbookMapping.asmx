﻿<%@ WebService Language="C#" Class="WorkbookMapping" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Runtime.Serialization.Json;
using System.Web.Script.Services;
using clsDAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WorkbookMapping : System.Web.Services.WebService
{
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string PTWMapping(string strProgID, string strworkbooks)
    {
        string strResult = "";
        clsProgramToWorkbook clsPTW = new clsProgramToWorkbook();
        Int64 intPRGID = Convert.ToInt64(strProgID);
        string[] strSplited = strworkbooks.Split(',');
        Boolean blnResult = clsPTW.DeleteByProgramID(intPRGID);
        Int64 intPosition = 1;

        if (strSplited.Length > 0)
        {
            foreach (string item in strSplited)
            {
                Int64 intWorkBookID = Convert.ToInt64(item);
                clsPTW.ptw_position = intPosition;
                clsPTW.ptw_programid = intPRGID;
                clsPTW.ptw_workbookid = intWorkBookID;
                clsPTW.Save();
                intPosition++;
            }
            strResult = "Mapping Save Successfully.";
        }
        else
        {
            strResult = "No Workbook Selected For Mapping.";
        }
        return strResult;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string WTCMapping(string strWRBID, string strChapters)
    {
        string strResult = "";
        clsWorkbookToChapters clsWTC = new clsWorkbookToChapters();
        Int64 intWRBID = Convert.ToInt64(strWRBID);
        string[] strSplited = strChapters.Split(',');
        Boolean blnResult = clsWTC.DeleteByWorkbookID(intWRBID);
        Int64 intPosition = 1;

        if (strSplited.Length > 0)
        {
            foreach (string item in strSplited)
            {
                Int64 intChapterID = Convert.ToInt64(item);
                clsWTC.wtc_position = intPosition;
                clsWTC.wtc_workbookid = intWRBID;
                clsWTC.wtc_chapterid = intChapterID;
                clsWTC.Save();
                intPosition++;
            }
            strResult = "Mapping Save Successfully.";
        }
        else
        {
            strResult = "No Chapter Selected For Mapping.";
        }
        return strResult;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CTLMapping(string strCHPID, string strLessons)
    {
        string strResult = "";
        clsChapterToLession clsCTL = new clsChapterToLession();
        Int64 intCHPID = Convert.ToInt64(strCHPID);
        string[] strSplited = strLessons.Split(',');
        Boolean blnResult = clsCTL.DeleteByChpid(intCHPID);
        Int64 intPosition = 1;

        //if (strSplited.Length > 0)
        //{
            foreach (string item in strSplited)
            {
                Int64 intlessonid = Convert.ToInt64(item);
                clsCTL.ctl_position = intPosition;
                clsCTL.ctl_chapterid = intCHPID;
                clsCTL.ctl_lessionid = intlessonid;
                clsCTL.Save();
                intPosition++;
            }
            strResult = "Mapping Save Successfully.";
        //}
        //else
        //{
            strResult = "No Lessons Selected For Mapping.";
        //}
            strResult = clsCTL.GetSetErrorMessage;
        return strResult;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string LTAMapping(string strLSNID, string strAssignments)
    {
        string strResult = "";
        clsLessionToAssignment clsLTA = new clsLessionToAssignment();
        Int64 intLSNID = Convert.ToInt64(strLSNID);
        string[] strSplited = strAssignments.Split(',');
        Boolean blnResult = clsLTA.DeleteByLessonID(intLSNID);
        Int64 intPosition = 1;

        if (strSplited.Length > 0)
        {
            foreach (string item in strSplited)
            {
                Int64 intAssignmentid = Convert.ToInt64(item);
                clsLTA.lta_position = intPosition;
                clsLTA.lta_lessionid = intLSNID;
                clsLTA.lta_assignmentid = intAssignmentid;
                clsLTA.Save();
                intPosition++;
            }
            strResult = "Mapping Save Successfully.";
        }
        else
        {
            strResult = "No Assignment Selected For Mapping.";
        }
        return strResult;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string PAUMapping(string strUserID, string strPrograms)
    {
        string strResult = "";
        clsProgramAssignUser clsPAU = new clsProgramAssignUser();
        Guid guUser_id = new Guid(strUserID);
        string[] strSplited = strPrograms.Split(',');
        Boolean blnResult = clsPAU.DeleteUseridPKID(guUser_id);
        if (strSplited.Length > 0)
        {
            foreach (string item in strSplited)
            {
                Int32 intProgramID = Convert.ToInt32(item);
                clsPAU.pau_userid = guUser_id;
                clsPAU.pau_programid = intProgramID;
                clsPAU.Save();
            }
            strResult = "Mapping Save Successfully.";
        }
        else
        {
            strResult = "No Program Selected For Mapping.";
        }
        return strResult;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CourseMapping(string strUserID, string strCourse)
    {
        string strResult = "";
        clsCourseMapping clsCRM = new clsCourseMapping();
        Guid guUser_id = new Guid(strUserID);
        string[] strSplited = strCourse.Split(',');
        Boolean blnResult = clsCRM.DeleteByUserID(guUser_id);
        if (strSplited.Length > 0)
        {
            foreach (string item in strSplited)
            {
                Int64 intCourseID = Convert.ToInt32(item);
                clsCRM.crm_userid = guUser_id;
                clsCRM.crm_courseid = intCourseID;
                clsCRM.Save();
                strResult += "Save " + clsCRM.GetSetErrorMessage;
            }
            strResult += "Mapping Save Successfully.";
        }
        else
        {
            strResult = "No Program Selected For Mapping.";
        }
        return strResult;
    }            
}