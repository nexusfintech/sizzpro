﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PhysicalSymptoms.ascx.cs" Inherits="UserControl_Assessment_PhysicalSymptoms" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-heart"></i>
                    Physical Symptoms
                </h3>
            </div>
            <div class="box-content ">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="span12">
                        <label>Reason for Visit</label>
                        <asp:TextBox ID="txtreasonforvisit" runat="server" TextMode="SingleLine" Width="60%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>When did your symptoms appear?</label>
                        <asp:TextBox ID="txtsymptomsapper" runat="server" TextMode="SingleLine" Width="60%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <div class="span6">
                            <label>Is this condition getting progressively worse?</label>
                        </div>
                        <div class="span6">
                            <asp:CheckBoxList ID="chkconditionworse" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Unknown"></asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                    </div>
                    <div class="span12">
                        <label>How often do you have this pain?</label>
                        <asp:TextBox ID="howpain" runat="server" TextMode="SingleLine" Width="60%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Is it constant or does it come and go? </label>
                        <asp:TextBox ID="txtcomego" runat="server" TextMode="SingleLine" Width="60%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Any particular time when you feel this pain? </label>
                        <asp:TextBox ID="txttimefeelPain" runat="server" TextMode="SingleLine" Width="60%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Does it interfere with your</label>
                        <asp:CheckBoxList ID="chkInterfere" runat="server" RepeatDirection="Horizontal" Width="60%">
                            <asp:ListItem Value="1" Text="Work"></asp:ListItem>
                            <asp:ListItem Value="2" Text="School"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Sleep"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Daily Routine"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Recreation"></asp:ListItem>
                        </asp:CheckBoxList>
                    </div>

                    <div class="span12">
                        <label>What treatment have you already received for this condition?</label>
                        <asp:CheckBoxList ID="chktreatmentforcondi" runat="server" RepeatDirection="Horizontal" Width="60%">
                            <asp:ListItem Value="1" Text="Medication"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Surgery"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Physical Therapy"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Chiropractic"></asp:ListItem>
                            <asp:ListItem Value="5" Text="None" Selected="True"></asp:ListItem>
                        </asp:CheckBoxList>
                    </div>

                    <div class="span12">
                        <label>Names and address of other doctors who have treated your condition</label>
                        <asp:TextBox ID="txtTreatedDoctors" runat="server" TextMode="MultiLine" Width="60%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Activities or movements that are painful to perform</label>
                        <asp:CheckBoxList ID="chkmovmentpain" runat="server" RepeatDirection="Horizontal" Width="60%">
                            <asp:ListItem Value="1" Text="Sitting"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Standing"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Walking"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Bending"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Lying Down"></asp:ListItem>
                            <asp:ListItem Value="6" Text="None" Selected="True"></asp:ListItem>
                        </asp:CheckBoxList>
                    </div>

                    <div class="span12">
                        <label>Other</label>
                        <asp:TextBox ID="txtPhySymOther" runat="server" TextMode="MultiLine" Width="60%"></asp:TextBox>
                    </div>

                    <div class="span12">
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

