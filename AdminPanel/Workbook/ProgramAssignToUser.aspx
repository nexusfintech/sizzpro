<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProgramAssignToUser.aspx.cs" Inherits="Workbook_ProgramAssignToUser" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="../js/jquery.sortable.min.js"></script>
    <script>
        $(function () {
            $('.sortable').sortable();
            $('.handles').sortable({
                handle: 'span'
            });
            $('.connected').sortable({
                connectWith: '.connected'
            });
            $('.exclude').sortable({
                items: ':not(.disabled)'
            });
        });
	</script>
    <link href="../css/Sortable.css" rel="stylesheet" />
    <script>
        $(function () {
            $("#sortable1, #sortable2").sortable({
                connectWith: ".connectedSortable"
            }).disableSelection();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkCreateNew" runat="server" NavigateUrl="~/Workbook/ProgramMaster.aspx"><i class="icon-retweet"></i>&nbsp;Programs</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="glyphicon-vector_path_circle"></i>
                Program Assign To User
            </h3>
        </div>
        <div class="box-content">
            <div class="row-fluid">
                <div class="span12">
                    <div class="form-horizontal">
                        <% if (User.IsInRole("administrator"))
                           { %>
                        <div class="control-group">
                            <label for="textfield" class="control-label"><%= MySession.Current.MemberAlias %></label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectMember" AutoPostBack="true" runat="server"
                                    OnSelectedIndexChanged="ddlSelectMember_SelectedIndexChanged" CssClass="select2-me input-xlarge">
                                </asp:DropDownList>
                                <span class="help-block">Select Member to load programm below</span>
                            </div>
                        </div>
                        <%} %>
                        <div class="control-group">
                            <label for="ddlUSer" class="control-label">Select User</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlUSer" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                &nbsp;&nbsp;&nbsp;
                                <asp:Button ID="cmdSelect" runat="server" Text="Select" CssClass="btn btn-primary" OnClick="cmdSelect_Click" />
                                <span class="help-block">First select program after then click on select button. then u can map Program to User.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="box box-color box-bordered">
                        <div class="box-title">
                            <h3>
                                <i class="icon-table"></i>
                                <asp:Label ID="lblBoxHeader" runat="server" Text="Selected Program"></asp:Label>
                            </h3>
                        </div>
                        <div class="box-content nopadding">
                            <asp:Panel ID="pnlMapping" runat="server" Visible="false">
                                <table class="table table-nomargin table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="50%" style="text-align: center;">Select Workbook</th>
                                            <th width="50%" style="text-align: center;">Selected/Mapped Workbook</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align: center; vertical-align: top;" width="50%">
                                                <asp:Repeater ID="RptrLeft" runat="server">
                                                    <HeaderTemplate>
                                                        <ul class="connected list">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <li id="<%#Eval("prg_id") %>"><%#Eval("prg_name") %></li>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </ul>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                            <td style="text-align: center; vertical-align: top;" width="50%">
                                                <asp:Repeater ID="RptrRight" runat="server">
                                                    <HeaderTemplate>
                                                        <ul class="connected list no2" id="rghtlist">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <li id='<%#Eval("prg_id") %>'><%#Eval("prg_name") %></li>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </ul>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div id="dvsavemsg" style="display: none;">
                                                    <img src="../img/ajax-loader.gif" />
                                                    <strong style="color: red;">Mapping in Progress Please Wait</strong>
                                                </div>
                                                <div id="dvmsg" style="display: none;">
                                                    <strong style="color: blue;">Save Successfully.</strong>
                                                </div>
                                                <div style="text-align: right;">
                                                    <input id="cmdmap" type="button" value="Save Mapping" class="btn btn-primary" onclick="savemapping()" />
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdUserid" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        function savemapping() {
           // $("#dvsavemsg").show();
           // $("#dvmsg").hide();
            var arrprg = new Array();
            var listbox = document.getElementById('rghtlist');
            if (listbox.children.length > 0)
            {
                for (var count = 0; count < listbox.children.length; count++)
                {
                    arrprg[count] = listbox.children[count].id;
                }
            }
            var varuserid = '<%= hdUserid.Value %>';
            $.ajax({
                type: "POST",
                url: "../WebServices/WorkbookMapping.asmx/PAUMapping",
                data: "{strUserID:'" + varuserid + "',strPrograms:'" + arrprg + "'}",
                contentType: "application/json;",
                dataType: "json",
                success: function (response) {
                    $("#dvmsg").show();
                    alert("Save Successfully.");
                },
                error: function (errdata) {
                    alert(errdata);
                },
                failure: function (msg) {
                    alert(msg);
                }
            });
            $("#dvsavemsg").hide();
        }
    </script>
</asp:Content>

