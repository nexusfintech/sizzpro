﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Workbook_ProgrammSharing : System.Web.UI.Page
{
    clsUserspermission clsUPM = new clsUserspermission();
    clsPrograms clsprg = new clsPrograms();
    clsobjectsharing clsOBS = new clsobjectsharing();
    private Guid UserId { get; set; }
    private Boolean blnGetResult { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (MySession.Current.EditID != 0)
        {
            blnGetResult = clsprg.GetRecordByIDInProperties(MySession.Current.EditID);
            if (!IsPostBack)
            {
                if (blnGetResult)
                {
                    this.UserId = clsprg.prg_OwnerId;
                    blnGetResult = clsprg.GetRecordByIDInProperties(MySession.Current.EditID);
                    BindAdmin(MySession.Current.EditID);
                    BindData();
                }
                else
                {
                    Response.Redirect("~/Workbook/ProgramMaster.aspx");
                }
            }
        }
        else
        {
            Response.Redirect("~/Workbook/ProgramMaster.aspx");
        }
    }
    public void BindAdmin(Int64 WrkId)
    {
        clsUPM.upm_userid = UserId;
        clsUPM.SetGetSPFlag = "USERLISTFORPRG";
        DataTable dtSrc = clsUPM.GetAdminList(WrkId);
        if (dtSrc.Rows.Count > 0)
        {
            ddladmins.Items.Clear();
            ddladmins.SelectedIndex = -1;
            ddladmins.SelectedValue = null;
            ddladmins.ClearSelection();
            ddladmins.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddladmins.Items.Add(lst);
            ddladmins.DataValueField = "Userid";
            ddladmins.DataTextField = "UserName";
            ddladmins.DataSource = dtSrc;
            ddladmins.DataBind();
        }
        else
        {
            ddladmins.Items.Clear();
            ddladmins.SelectedIndex = -1;
            ddladmins.SelectedValue = null;
            ddladmins.ClearSelection();
            ddladmins.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<- No Record Found ->>";
            lst.Selected = true;
            ddladmins.Items.Add(lst);
        }
    }

    public void BindData()
    {
        clsOBS.shr_ownerid = UserId;
        clsOBS.shr_sharedobjid = MySession.Current.EditID;
        clsOBS.SetGetSPFlag = "PROGRAMM";
        DataTable dtRslt = clsOBS.GetRecords();
        dtSharing.DataSource = dtRslt;
        dtSharing.DataBind();
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        if (ddladmins.SelectedIndex != 0)
        {
            clsOBS.shr_sharedwithid = new Guid(ddladmins.SelectedValue.ToString());
            shareprogramm(MySession.Current.EditID);
        }
        else
        {
            lblmessage.Text = "Please Select Any User";
        }
    }

    public void shareprogramm(Int64 PrgId)
    {
        Boolean blnWRslt = blnGetResult;
        if (blnWRslt)
        {

            List<Int64> WrbList = new clsProgramToWorkbook().GetWrkBookByPrgID(PrgId,clsprg.prg_UserId);
            foreach(Int64 wrb in WrbList)
            {
                List<Int64> ChpList = new clsWorkbookToChapters().GetChpByWorkbookID(wrb, clsprg.prg_UserId);
                foreach (Int64 chp in ChpList)
                {
                    List<Int64> LsnList = new clsChapterToLession().GetLsnByChapterID(chp, clsprg.prg_UserId);
                    foreach (Int64 lsn in LsnList)
                    {
                        List<Int64> AsnList = new clsLessionToAssignment().GetAsnByLessonID(lsn, clsprg.prg_UserId);
                        foreach (Int64 asn in AsnList)
                        {
                            clsOBS.shr_object = "ASSIGNMENT";
                            clsOBS.shr_sharedobjid = asn;
                            clsOBS.shr_ownerid = clsprg.prg_UserId;
                            clsOBS.Save();
                        }
                        clsOBS.shr_object = "LESSON";
                        clsOBS.shr_sharedobjid = lsn;
                        clsOBS.shr_ownerid = clsprg.prg_UserId;
                        clsOBS.Save();
                    }
                    clsOBS.shr_object = "CHAPTER";
                    clsOBS.shr_sharedobjid = chp;
                    clsOBS.shr_ownerid = clsprg.prg_UserId;
                    clsOBS.Save();
                }
                clsOBS.shr_object = "WORKBOOK";
                clsOBS.shr_sharedobjid = wrb;
                clsOBS.shr_ownerid = clsprg.prg_UserId;
                clsOBS.Save();
            }
            clsOBS.shr_object = "PROGRAMM";
            clsOBS.shr_sharedobjid = clsprg.prg_id;
            clsOBS.shr_ownerid = clsprg.prg_UserId;
            clsOBS.Save();
            Response.Redirect("~/Workbook/ProgramMaster.aspx");
        }
        else
        {
            Response.Redirect("~/Workbook/ProgramMaster.aspx");
        }
    }
}