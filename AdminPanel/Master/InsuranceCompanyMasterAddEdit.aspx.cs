﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Globalization;

public partial class Master_InsuranceCompanyAddEdit : System.Web.UI.Page
{
    clsInsuranceCompanyMaster clsICM = new clsInsuranceCompanyMaster();
    clsPayerMaster clspm = new clsPayerMaster();
    clsInsuranceTypeMaster clsITM = new clsInsuranceTypeMaster();
    clsZipMaster clszip = new clsZipMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindmasters();
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                loaddata();
            }
        }
    }

    private void bindmasters()
    {
        ddlState.Items.Clear();
        ddlState.SelectedIndex = -1;
        ddlState.SelectedValue = null;
        ddlState.ClearSelection();
        ddlState.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<< -- Pleasse Select -- >>";
        lst.Selected = true;
        //ddlState.Items.Add(lst);
        ddlState.DataSource = clszip.GetAllByMasterFlag("STATE");
        ddlState.DataTextField = "zip_name";
        ddlState.DataValueField = "zip_id";
        ddlState.DataBind();

        // bind health paln
        ddlHpa.Items.Clear();
        ddlHpa.SelectedIndex = -1;
        ddlHpa.SelectedValue = null;
        ddlHpa.ClearSelection();
        ddlHpa.AppendDataBoundItems = true;
        ddlHpa.Items.Add(lst);
        ddlHpa.DataSource = clspm.GetAllRecord() ;
        ddlHpa.DataTextField = "pm_PayerName";
        ddlHpa.DataValueField = "pm_id";
        ddlHpa.DataBind();

        // bind insurance type
        ddlInscType.Items.Clear();
        ddlInscType.SelectedIndex = -1;
        ddlInscType.SelectedValue = null;
        ddlInscType.ClearSelection();
        ddlInscType.AppendDataBoundItems = true;
        ddlInscType.Items.Add(lst);
        ddlInscType.DataSource = clsITM.GetAllRecord();
        ddlInscType.DataTextField = "itm_insurance";
        ddlInscType.DataValueField = "itm_id";
        ddlInscType.DataBind();
    }
    private void loaddata()
    {
        Boolean blnRslt = clsICM.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {
            txtcompanyname.Text = clsICM.insc_name;
            txtadress.Text = clsICM.insc_address;
            txtcity.Text = clsICM.insc_city;
            txtpin.Text = clsICM.insc_pincode;
            txtphone.Text = clsICM.insc_phone;
            txtpayorid.Text = clsICM.insc_payorid;
            txtfax.Text = clsICM.insc_fax;
            txtdflticdt.Text = clsICM.insc_defaulttoicd10dos.ToString("MM/dd/yyyy");

            ListItem lstst = ddlState.Items.FindByValue(clsICM.insc_state.ToString());
            if (lstst != null)
            {
                ddlState.SelectedIndex = ddlState.Items.IndexOf(lstst);
            }
            ListItem lstit = ddlInscType.Items.FindByValue(clsICM.insc_type.ToString());
            if (lstit != null)
            {
                ddlInscType.SelectedIndex = ddlInscType.Items.IndexOf(lstit);
            }
            ListItem lstep = ddlHpa.Items.FindByValue(clsICM.insc_healthplaneligbility.ToString());
            if (lstep != null)
            {
                ddlHpa.SelectedIndex = ddlHpa.Items.IndexOf(lstep);
            }
            ListItem lstbt = ddlBlngTyp.Items.FindByValue(clsICM.insc_billingtype.ToString());
            if (lstbt != null)
            {
                ddlBlngTyp.SelectedIndex = ddlBlngTyp.Items.IndexOf(lstbt);
            }
        }
        else
        {
            MySession.Current.MESSAGE = clsICM.GetSetErrorMessage;
            Response.Redirect("~/Master/InsauraceCompanyMaster.aspx");
        }
    }
    private string validatepage()
    {
        string result = string.Empty;
        DateTime dtBirthDate;

        if (txtcompanyname.Text == string.Empty)
        {
            return result = "Company Name Required";
        }
        else if (txtpayorid.Text == string.Empty)
        {
            return result = "Company PayorId Required";
        }
        else if (!(DateTime.TryParseExact(txtdflticdt.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtBirthDate)))
        {
            return result = "Please Enter Date in mm/DD/yyyy Formate";
        }
        else
        {
            return result;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string vldtreslt = validatepage();
        if (vldtreslt != string.Empty)
        {
            lblmessage.Text = vldtreslt;
            msgdiv.Visible = true;
            return;
        }

        clsICM.insc_name = txtcompanyname.Text;
        clsICM.insc_payorid = txtpayorid.Text;
        clsICM.insc_address = txtadress.Text;
        clsICM.insc_city = txtcity.Text;
        clsICM.insc_state = Convert.ToInt64(ddlState.SelectedValue);
        clsICM.insc_pincode = txtpin.Text;
        clsICM.insc_phone = txtphone.Text;
        clsICM.insc_type = Convert.ToInt64(ddlInscType.SelectedValue);
        clsICM.insc_billingtype = ddlBlngTyp.SelectedValue.ToString();
        clsICM.insc_healthplaneligbility=Convert.ToInt64(ddlHpa.SelectedValue);
        clsICM.insc_defaulttoicd10dos = Convert.ToDateTime(txtdflticdt.Text);


        Boolean blnresult;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnresult = clsICM.Save();
        }
        else
        {
            blnresult = clsICM.Update(MySession.Current.EditID);
        }
        if (blnresult)
        {
            MySession.Current.MESSAGE = "Recored Saved Successfully";
            Response.Redirect("~/Master/InsauraceCompanyMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsICM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/InsauraceCompanyMaster.aspx");
    }
}