﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Users_MemberManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ucUserList.IsAct = true;
            loaddata();
        }
    }
    protected void cmdShowAll_Click(object sender, EventArgs e)
    {
        ucUserList.IsAct = true;
        loaddata();
    }
    protected void cmdInactive_Click(object sender, EventArgs e)
    {
        ucUserList.IsAct = false;
        loaddata();
    }

    private void loaddata()
    {
        ucUserList.Rolename = "Member";
        ucUserList.RefreshGridData();
        lnkCreateNew.NavigateUrl = "~/Users/MemberRegistration.aspx";
    }
}