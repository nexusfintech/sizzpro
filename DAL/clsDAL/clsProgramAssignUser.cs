﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsProgramAssignUser
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsProgramAssignUser()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsProgramAssignUser()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _pau_id; }
            set
            {
                _pau_id = value;
                if (clsCMD.Parameters.Contains("@pau_id"))
                { clsCMD.Parameters["@pau_id"].Value = value; }
                else { clsCMD.Parameters.Add("@pau_id", SqlDbType.BigInt).Value = value; }
            }
        }

        private Guid _ByUser = new Guid();
        public Guid ByUser
        {
            get { return _ByUser; }
            set
            {
                _ByUser = value;
                if (clsCMD.Parameters.Contains("@User"))
                { clsCMD.Parameters["@User"].Value = value; }
                else { clsCMD.Parameters.Add("@User", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _pau_id;
        public Int64 pau_id
        {
            get { return _pau_id; }
            set
            {
                _pau_id = value;
                if (clsCMD.Parameters.Contains("@pau_id"))
                { clsCMD.Parameters["@pau_id"].Value = value; }
                else { clsCMD.Parameters.Add("@pau_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int32 _pau_programid;
        public Int32 pau_programid
        {
            get { return _pau_programid; }
            set
            {
                _pau_programid = value;
                if (clsCMD.Parameters.Contains("@pau_programid"))
                { clsCMD.Parameters["@pau_programid"].Value = value; }
                else { clsCMD.Parameters.Add("@pau_programid", SqlDbType.Int).Value = value; }
            }
        }
        private DateTime _pau_datetime;
        public DateTime pau_datetime
        {
            get { return _pau_datetime; }
            set
            {
                _pau_datetime = value;
                if (clsCMD.Parameters.Contains("@pau_datetime"))
                { clsCMD.Parameters["@pau_datetime"].Value = value; }
                else { clsCMD.Parameters.Add("@pau_datetime", SqlDbType.DateTime).Value = value; }
            }
        }
        private Guid _pau_userid;
        public Guid pau_userid
        {
            get { return _pau_userid; }
            set
            {
                _pau_userid = value;
                if (clsCMD.Parameters.Contains("@pau_userid"))
                { clsCMD.Parameters["@pau_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@pau_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                pau_id = Convert.ToInt64(dr["pau_id"]);
                pau_programid = Convert.ToInt32(dr["pau_programid"]);
                pau_datetime = Convert.ToDateTime(dr["pau_datetime"]);
                pau_userid = new Guid(dr["pau_userid"].ToString());
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            pau_id = Convert.ToInt64("0");
            pau_programid = Convert.ToInt32("0");
            pau_datetime = Convert.ToDateTime(DateTime.Now);
            pau_userid = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.pau_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "programassigntouserAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 pau_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.pau_id = pau_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "programassigntouserAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 pau_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.pau_id = pau_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "programassigntouserAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteUseridPKID(Guid guidUserID)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.pau_userid = guidUserID;
            this.AddEditDeleteFlag = "DELETEBYUSERID";
            try
            {
                clsCMD.CommandText = "programassigntouserAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                clsCMD.CommandText = "programassigntouserGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetWorkBookByUser()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                clsCMD.CommandText = "programassigntouserGetData";
                SetGetSPFlag = "GETUSERWORKBOOK";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetProgrammClient(Guid User)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = User;
                clsCMD.CommandText = "programassigntouserGetData";
                SetGetSPFlag = "GETCLIENTRPROGRAMM";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllProgrammClient()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "programassigntouserGetData";
                SetGetSPFlag = "GETALLCLIENTRPROGRAMM";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetProgramByByUser(Guid guUserid,Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = MemberId;
                this.pau_userid = guUserid;
                clsCMD.CommandText = "programassigntouserGetData";
                SetGetSPFlag = "GETPRGBYUSER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetProgramNotMappedByByUser(Guid guUserid,Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = MemberId;
                this.pau_userid = guUserid;
                clsCMD.CommandText = "programassigntouserGetData";
                SetGetSPFlag = "BYWPRGIDNOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsProgramAssignUser_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsProgramAssignUser_PropertiesList> lstResult = new List<clsProgramAssignUser_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].pau_id = Convert.ToInt64(dr["pau_id"]);
                lstResult[i].pau_programid = Convert.ToInt32(dr["pau_programid"]);
                lstResult[i].pau_datetime = Convert.ToDateTime(dr["pau_datetime"]);
                lstResult[i].pau_userid = new Guid(dr["pau_userid"].ToString());

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 pau_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.pau_id = pau_id;
                clsCMD.CommandText = "programassigntouserGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 pau_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.pau_id = pau_id;
                clsCMD.CommandText = "programassigntouserGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }

    public class clsProgramAssignUser_PropertiesList
    {
        public Int64 pau_id { get; set; }
        public Int32 pau_programid { get; set; }
        public DateTime pau_datetime { get; set; }
        public Guid pau_userid { get; set; }
    }
}