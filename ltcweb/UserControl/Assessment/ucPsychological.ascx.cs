﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class UserControl_Assessment_ucPsychological : System.Web.UI.UserControl
{
    clsInitialAssessment clsASS = new clsInitialAssessment();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();
    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    private void EditEntry()
    {
        Boolean blnEdit = false;
        Guid objKey = new Guid(strMemberkey);
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, rdosufferdpsychological.ClientID);
        if (blnEdit == true)
        {
            ListItem li = rdosufferdpsychological.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                rdosufferdpsychological.SelectedIndex = rdosufferdpsychological.Items.IndexOf(li);
            }
        }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, rdosuicide.ClientID);
        if (blnEdit == true)
        {
            ListItem li = rdosuicide.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                rdosuicide.SelectedIndex = rdosuicide.Items.IndexOf(li);
            }
        }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txthurtingsomeone.ClientID);
        if (blnEdit == true)
        {txthurtingsomeone.Text = clsASS.ini_valuetext;}
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtmindworking.ClientID);
        if (blnEdit == true)
        { txtmindworking.Text = clsASS.ini_valuetext; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtsucideappempts.ClientID);
        if (blnEdit == true)
        { txtsucideappempts.Text = clsASS.ini_valuetext; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtsuffredpsychological.ClientID);
        if (blnEdit == true)
        { txtsuffredpsychological.Text = clsASS.ini_valuetext; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtvoicedescribe.ClientID);
        if (blnEdit == true)
        { txtvoicedescribe.Text = clsASS.ini_valuetext; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            EditEntry();
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Boolean blnSave = false;
            Guid objKey = new Guid(strMemberkey);
            clsASS.ini_userid = objKey;
            if (txthurtingsomeone.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txthurtingsomeone.Text.Trim();
                clsASS.ini_valueflag = txthurtingsomeone.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtmindworking.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtmindworking.Text.Trim();
                clsASS.ini_valueflag = txtmindworking.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtsucideappempts.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtsucideappempts.Text.Trim();
                clsASS.ini_valueflag = txtsucideappempts.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtsuffredpsychological.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtsuffredpsychological.Text.Trim();
                clsASS.ini_valueflag = txtsuffredpsychological.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtvoicedescribe.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtvoicedescribe.Text.Trim();
                clsASS.ini_valueflag = txtvoicedescribe.ClientID;
                blnSave = clsASS.Save();
            }
            ListItem li = rdosufferdpsychological.SelectedItem;
            if (li != null)
            {
                clsASS.ini_valueflag = rdosufferdpsychological.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(rdosufferdpsychological.SelectedValue);
                blnSave = clsASS.Save();
            }
            ListItem li1 = rdosuicide.SelectedItem;
            if (li1 != null)
            {
                clsASS.ini_valueflag = rdosuicide.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(rdosuicide.SelectedValue);
                blnSave = clsASS.Save();
            }

            clsASR.storcrd_stepid = 17;
            clsASR.stprcrd_userid = objKey;
            Boolean stprslt = clsASR.Save();
            if (stprslt == true)
            {
                Response.Redirect("~/Client/Assessments/ClientAddiction.aspx");
            }
            lblMessage.Text = "Save Successfully";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error in save process.. of " + clsASS.ini_valueflag + " System Message : " + ex.Message;
        }
    }
}