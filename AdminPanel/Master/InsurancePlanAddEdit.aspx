﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="InsurancePlanAddEdit.aspx.cs" Inherits="Master_InsurancePlanAddEdit" %>    
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .item 
        {
            display:block;
            padding:5px;
            border:1px solid black;
            background-color:aliceblue;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkmaster" runat="server" NavigateUrl="~/Master/InsurancePlanMaster.aspx"><i class="icon-retweet"></i>&nbsp;InsurancePlan Master</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </div>
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Insurance Plan Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <div class="control-group">
                            <label for="textfield" class="control-label">Select Insurance Company</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlInscCmpny" runat="server" CssClass="select2-me input-large"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtcode" class="control-label">Plan GroupNumber</label>
                            <div class="controls">
                                <asp:TextBox ID="txtgrpnumber" runat="server" CssClass="input-large" data-rule-required="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtcode" class="control-label">Plan Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtname" runat="server" CssClass="input-large" data-rule-required="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtdescription" class="control-label">Deductible</label>
                            <div class="controls">
                                <asp:TextBox ID="txtdeductible" runat="server" data-rule-number="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtcharge" class="control-label">Co-Pay</label>
                            <div class="controls">
                                <asp:TextBox ID="txtcopay" runat="server" CssClass="input-large" data-rule-number="true"></asp:TextBox>
                            </div>
                        </div>
                          <div class="control-group">
                            <label for="txtcharge" class="control-label">Co-Insurance</label>
                            <div class="controls">
                                <asp:TextBox ID="txtcoinsurance" runat="server" CssClass="input-large" data-rule-number="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtmodb" class="control-label">Cover MHO</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlmho" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtmodb" class="control-label">Cover Chiropractor</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlchi" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                          <div class="control-group">
                            <label for="txtmodb" class="control-label">Cover Dietition</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddldit" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                         <div class="control-group">
                            <label for="textfield" class="control-label">Select CPT Code</label>
                            <div class="controls">
                               <%-- <asp:DropDownList ID="ddlcptcodes" runat="server" CssClass="select2-me input-large">
                                </asp:DropDownList>--%>
                                <asp:ListBox ID="ddlcptcodes" runat="server" SelectionMode="Multiple" CssClass="input-large" Width="500"></asp:ListBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" runat="server" CssClass="btn btn-primary" OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                            <asp:LinkButton ID="cmdCancel" runat="server" CssClass="btn" OnClick="cmdCancel_Click" ><i class="icon-remove"></i> Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>

