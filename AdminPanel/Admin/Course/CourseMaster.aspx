﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CourseMaster.aspx.cs" Inherits="Admin_Course_CourseMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkMappingtoWB" runat="server" NavigateUrl="~/Admin/Course/CoursetoUserMapping.aspx"><i class="glyphicon-book_open"></i>&nbsp;Mapping to User</asp:HyperLink>
            </li>
            <li>
                <a><i class="icon-plus"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn btn-primary" Text="Create New Service" OnClick="cmdCreateNew_Click" /></a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-bordered box-color">
        <div class="box-title">
            <h3>
                <i class="glyphicon-projector"></i>
                Appointment Service Management
            </h3>
        </div>
        <div class="box-content nopadding">
            <asp:Repeater ID="dtAssignment" runat="server" OnItemCommand="dtAssignment_ItemCommand">
                <HeaderTemplate>
                    <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="width: 50%;">Service Name</th>
                                <th style="width: 10%;">Group Name</th>
                                <th style="width: 10%;">Duration (Min)</th>
                                <th style="text-align: center; width: 10%;">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("crs_coursename") %></td>
                        <td><%# Eval("crs_groupname") %></td>
                        <td><%# Eval("crs_interval") %></td>
                        <td style="text-align:center">
                            <asp:Button ID="cmdEdit" runat="server" Text="Edit" CssClass="btn btn-green" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "crs_id") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

