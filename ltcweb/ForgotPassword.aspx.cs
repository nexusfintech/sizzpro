﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class ForgotPassword : System.Web.UI.Page
{
    clsProfileMaster clsProfile = new clsProfileMaster();
    string strFlag; string strTokenID;

    protected void Page_Load(object sender, EventArgs e)
    {
        strFlag = Page.Request.QueryString["action"].ToString();

        if (strFlag.ToLower() == "reset")
        {
            dvPasswordReset.Visible = true;
        }
        if (strFlag.ToLower() == "pschange")
        {
            if (Page.Request.QueryString["tokenid"] != null)
            {
                strTokenID = Page.Request.QueryString["tokenid"].ToString();
            }
            else { lblpagemessage.Text = "Please provide token id"; return; }
            Boolean blnResult = clsProfile.GetRecordByTokenIDInProperties(strTokenID);
            if (blnResult == true)
            {
                DateTime startDate = DateTime.Now;
                DateTime endDate = Convert.ToDateTime(clsProfile.prm_tokendatetime);
                TimeSpan tmspan = startDate - endDate;
                if (tmspan.Days > 1)
                {
                    lblpagemessage.Text = "Your requested link is expire please request for new otherwise contact administrator.";
                    return;
                }
                else { dvPasswordChange.Visible = true; }
            }
            else { lblpagemessage.Text = "Profile not found..!!"; return; }
        }
    }

    protected void cmdGetUsername_Click(object sender, EventArgs e)
    {
        if (txtUsername.Text.Trim() != "")
        {
            MembershipUser checkuser = Membership.GetUser(txtUsername.Text.Trim());
            if (checkuser != null)
            {
                if (checkuser.UserName != txtUsername.Text.Trim())
                {
                    lblGetPassword.Text = "Invalid username please try again.";
                    return;
                }
                if (checkuser.IsLockedOut == true)
                {
                    checkuser.UnlockUser();
                }
                Boolean blnResult = clsGetTemplate.GetTemplate("ForgotPassword");
                if (blnResult == true)
                {
                    string strGUID = Guid.NewGuid().ToString();
                    Boolean blnguid = clsProfile.UpdateTokenID(checkuser.ProviderUserKey.ToString(), strGUID);
                    if (blnguid == true)
                    {
                        string strhtml = clsGetTemplate.HtmlTemplate;
                        String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                        String strhost = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                        string strlink = strhost + "ForgotPassword.aspx?action=pschange&tokenid=" + strGUID;
                        strhtml = strhtml.Replace("{resetlink}", strlink).Replace("{username}", checkuser.UserName).Replace("{fullname}", checkuser.UserName);
                        clsGetTemplate.HtmlTemplate = strhtml;
                        Boolean blnSendMail = clsGetTemplate.SendMail(checkuser.Email);
                        if (blnSendMail == true)
                        { lblGetPassword.Text = "Email sended to your registred email account Please check your mail account inbox"; }
                        else { lblGetPassword.Text = "Email not sended successfully."; }
                    }
                    else { lblGetPassword.Text = "Profile Not found...!!"; }
                }
            }
            else { lblGetPassword.Text = "Please Enter Username"; }
        }
    }

    protected void cmdChangePassword_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtNewPassword.Text == txtConformPassword.Text)
            {
                Boolean blnProfile = clsProfile.GetRecordByTokenIDInProperties(strTokenID);
                if (blnProfile == true)
                {
                    Guid objProviderkey = new Guid(clsProfile.prm_userId);
                    MembershipUser mu = Membership.GetUser(objProviderkey);
                    string strResetedPassword = mu.ResetPassword("nexus@admin");
                    Boolean blnResult = mu.ChangePassword(strResetedPassword, txtNewPassword.Text);
                    if (blnResult == true)
                    {
                        lblChangepass.Text = "Password Changed Successfully.";
                        Boolean blnMail = clsGetTemplate.GetTemplate("PasswordChanged");
                        if (blnMail == true)
                        {
                            string userEmail = mu.Email;
                            string strhtml = clsGetTemplate.HtmlTemplate;
                            clsGetTemplate.HtmlTemplate = strhtml.Replace("{username}", mu.UserName);
                            Boolean blnSendmail = clsGetTemplate.SendMail(userEmail);
                        }
                        else { lblChangepass.Text = clsGetTemplate.HtmlTemplate; }
                    }
                    else { lblChangepass.Text = "Password Not Changed."; }
                }
                else { lblpagemessage.Text = "Profile not found..!!"; }
            }
            else { lblChangepass.Text = "Conform Password Not Match.."; }
        }
        catch (Exception ex)
        { lblChangepass.Text = ex.Message; }
    }

}