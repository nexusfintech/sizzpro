﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="CPTCodeAddEdit.aspx.cs" Inherits="Master_CPTCodeAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-retweet"></i>
                    <asp:Button ID="cmdBack" runat="server" CssClass="btn btn-primary" Text="CPT Code Master" OnClick="cmdBack_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="row-fluid">
        <div class="span12">
            <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </div>
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>CPT Code Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                       <%-- <% if (User.IsInRole("administrator"))
                           { %>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Creater</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectMember" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                <span class="help-block">Select Creater for whom Workbook Is created</span>
                            </div>
                        </div>
                        <%} %>--%>
                        <div class="control-group">
                            <label for="txtcode" class="control-label">CPT Code</label>
                            <div class="controls">
                                <asp:TextBox ID="txtcode" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtdescription" class="control-label">Description</label>
                            <div class="controls">
                                <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtcharge" class="control-label">Charge</label>
                            <div class="controls">
                                <asp:TextBox ID="txtcharge" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                         <div class="control-group">
                            <label for="txtpos" class="control-label">POS</label>
                            <div class="controls">
                                <asp:DropDownList ID="drpPOS" runat="server" CssClass="select2-me input-large">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" runat="server" CssClass="btn btn-primary" OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                            <asp:LinkButton ID="cmdCancel" runat="server" CssClass="btn" OnClick="cmdCancel_Click" ><i class="icon-remove"></i> Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>

