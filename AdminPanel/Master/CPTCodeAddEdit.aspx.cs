﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class Master_CPTCodeAddEdit : System.Web.UI.Page
{
    clsCPTCodeMaster clsCCM = new clsCPTCodeMaster();
    clsPlaceofServicesMaster clsPOS = new clsPlaceofServicesMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindpos();
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                loaddata();
            }
        }
    }
    private void loaddata()
    {
        Boolean blnRslt = clsCCM.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {
            txtcode.Text = clsCCM.cd_code;
            txtcharge.Text = clsCCM.cd_charge.ToString();
            txtdescription.Text = clsCCM.cd_description;
            ListItem lstpos = drpPOS.Items.FindByValue(clsCCM.cd_pos.ToString());
            if (lstpos != null)
            {
                drpPOS.SelectedIndex = drpPOS.Items.IndexOf(lstpos);
            }
        }
        else
        {
            MySession.Current.MESSAGE = clsCCM.GetSetErrorMessage;
            Response.Redirect("~/Master/FacilityMaster.aspx");
        }
    }

    private void bindpos()
    {
        drpPOS.Items.Clear();
        drpPOS.SelectedIndex = -1;
        drpPOS.SelectedValue = null;
        drpPOS.ClearSelection();
        drpPOS.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<< -- Pleasse Select -- >>";
        lst.Selected = true;
        drpPOS.Items.Add(lst);
        drpPOS.DataSource = clsPOS.GetAllRecord();
        drpPOS.DataTextField = "pos_place";
        drpPOS.DataValueField = "pos_code";
        drpPOS.DataBind();
    }
    private string validatepage()
    {
        string result = string.Empty;
        if (txtcode.Text == string.Empty)
        {
            result = "CPT Code Required";
            return result;
        }
        else if (txtdescription.Text == string.Empty)
        {
            result = "Code Description Required";
            return result;
        }
        else if (txtcharge.Text == string.Empty)
        {
            result = "Code Charge Required";
            return result;
        }
        else
        {
            return result;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string vldtreslt = validatepage();
        if (vldtreslt != string.Empty)
        {
            lblmessage.Text = vldtreslt;
            msgdiv.Visible = true;
            return;
        }
        clsCCM.cd_userid =(Guid) Membership.GetUser().ProviderUserKey;
        clsCCM.cd_code = txtcode.Text;
        clsCCM.cd_description = txtdescription.Text;
        clsCCM.cd_charge = Convert.ToDouble(txtcharge.Text);
        clsCCM.cd_pos = Convert.ToInt64(drpPOS.SelectedValue);
        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnRslt = clsCCM.Save();
        }
        else
        {
            blnRslt = clsCCM.Update(MySession.Current.EditID);
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/CPTCodeMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsCCM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/CPTCodeMaster.aspx");
    }
    protected void cmdBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/CPTCodeMaster.aspx");
    }
}