﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PreviousAppointment.aspx.cs" Inherits="Client_PreviousAppointment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
     <div class="row-fluid">
        <div class="span12">
            <h3><i class="glyphicon-calendar"></i>&nbsp;Previous Appointments</h3>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-calendar"></i>
                        My Appointments
                    </h3>
                    <div class="actions">
                        <a href="Dashboard.aspx">Dashboard</a>
                        <a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
                    </div>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-column form-bordered'>
                        <asp:Repeater ID="rptAppontment" runat="server" >
                            <HeaderTemplate>
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <th>Course</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>From Time</th>
                                        <th>To Time</th>
                                        <th>Therapist</th>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("crs_coursename") %> </td>
                                    <td><%# Eval("apt_title") %> </td>
                                    <td><%# Eval("apt_description") %> </td>
                                    <td><%# Eval("apt_startdatetime") %> </td>
                                    <td><%# Eval("apt_enddatetime") %> </td>
                                    <td><%# Eval("prm_firstname") %>&nbsp;<%# Eval("prm_lastname") %></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>

