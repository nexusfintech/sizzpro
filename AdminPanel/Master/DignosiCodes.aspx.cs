﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;

public partial class Master_DignosiCodes : System.Web.UI.Page
{
    clsDiagnosisCodeMaster clsDCM = new clsDiagnosisCodeMaster();
    clsAdminDignosiscodeMapping clsADM = new clsAdminDignosiscodeMapping();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptdgCode.DataSource = clsADM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        rptdgCode.DataBind();
    }
    private void loadcodes()
    {
        clsADM.ucm_userid =(Guid) Membership.GetUser().ProviderUserKey;
        DataTable dtsrs = clsADM.GetAllRecordForDrop();
        dtsrs.Columns.Add("DignosisCode", typeof(string), "dcm_code + ' - '  + dcm_description +' - '+dcm_hcc +' - '+dcm_ispayable");

        ddlCodes.Items.Clear();
        ddlCodes.SelectedIndex = -1;
        ddlCodes.SelectedValue = null;
        ddlCodes.ClearSelection();
        ddlCodes.AppendDataBoundItems = true;
        ddlCodes.DataValueField = "dcm_id";
        ddlCodes.DataTextField = "DignosisCode";
        ddlCodes.DataSource = dtsrs;
        ddlCodes.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        loadcodes();
        adddc.Visible = true;
    }
    protected void rptdgCode_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;
            Boolean ispybl = Convert.ToBoolean(DataBinder.Eval(DI, "dcm_ispayable"));
            Boolean hcc = Convert.ToBoolean(DataBinder.Eval(DI, "dcm_hcc"));

            if (ispybl)
            {
                Label lbl = (Label)e.Item.FindControl("lpt");
                lbl.Visible = true;
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lpf");
                lbl.Visible = true;
            }
            if (hcc)
            {
                Label lbl = (Label)e.Item.FindControl("lht");
                lbl.Visible = true;
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lhf");
                lbl.Visible = true;
            }
        }
    }
    protected void rptdgCode_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if(e.CommandName == "DELETE")
        {
            Boolean delreslt = clsADM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (delreslt)
            {
                MySession.Current.MESSAGE = "Code Deleted Successfully";
                Response.Redirect("~/Master/DignosiCodes.aspx");
            }
            else
            {
                lblmessage.Text = clsADM.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        if(ddlCodes.SelectedIndex== -1)
        {
            lblmessage.Text = "Please Select DignosisCode";
            msgdiv.Visible = true;
            return;
        }
        clsADM.ucm_codeid = Convert.ToInt64(ddlCodes.SelectedValue);
        clsADM.ucm_userid = (Guid)Membership.GetUser().ProviderUserKey;
        Boolean blnRslt = clsADM.Save();
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Code Added Successfully";
            Response.Redirect("~/Master/DignosiCodes.aspx");
        }
    }
}