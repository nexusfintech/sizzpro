﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
public partial class UserControl_Assessment_ucAccidentInfo : System.Web.UI.UserControl
{
    clsInitialAssessment clsASS = new clsInitialAssessment();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();
    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            EditEntry();
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Boolean blnSave = false;
            Guid objKey = new Guid(strMemberkey);
            clsASS.ini_userid = objKey;
            ListItem li = chkaccContact.SelectedItem;
            if (txAccidnttDesc.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txAccidnttDesc.Text.Trim();
                clsASS.ini_valueflag = txAccidnttDesc.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtAttorney.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtAttorney.Text.Trim();
                clsASS.ini_valueflag = txtAttorney.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtAcciDate.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtAcciDate.Text.Trim();
                clsASS.ini_valueflag = txtAcciDate.ClientID;
                blnSave = clsASS.Save();
            }
            if (li != null)
            {
                clsASS.ini_valueflag = chkaccContact.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(chkaccContact.SelectedValue);
                blnSave = clsASS.Save();
            }
            ListItem li1 = chkAccidenteType.SelectedItem;
            if (li1 != null)
            {
                clsASS.ini_valueflag = chkAccidenteType.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(chkAccidenteType.SelectedValue);
                blnSave = clsASS.Save();
            }
            ListItem li2 = chkduetoAccident.SelectedItem;
            if (li1 != null)
            {
                clsASS.ini_valueflag = chkduetoAccident.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(chkduetoAccident.SelectedValue);
                blnSave = clsASS.Save();
            }

            clsASR.storcrd_stepid = 14;
            clsASR.stprcrd_userid = objKey;
            Boolean stprslt = clsASR.Save();
            if (stprslt == true)
            {
                Response.Redirect("~/Client/Assessments/ClientMedical.aspx");
            }
            lblMessage.Text = "Save Successfully";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error in save process.. of " + clsASS.ini_valueflag + " System Message : " + ex.Message;
        }
    }

    private void EditEntry()
    {
        Boolean blnEdit = false;
        Guid objKey = new Guid(strMemberkey);
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkaccContact.ClientID);
        if (blnEdit == true)
        {
            ListItem li = chkaccContact.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                chkaccContact.SelectedIndex = chkaccContact.Items.IndexOf(li);
            }
        }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkAccidenteType.ClientID);
        if (blnEdit == true)
        {
            ListItem li = chkAccidenteType.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                chkAccidenteType.SelectedIndex = chkAccidenteType.Items.IndexOf(li);
            }
        }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkduetoAccident.ClientID);
        if (blnEdit == true)
        {
            ListItem li = chkduetoAccident.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                chkduetoAccident.SelectedIndex = chkduetoAccident.Items.IndexOf(li);
            }
        }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txAccidnttDesc.ClientID);
        if (blnEdit == true)
        { txAccidnttDesc.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtAttorney.ClientID);
        if (blnEdit == true)
        { txtAttorney.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtAcciDate.ClientID);
        if (blnEdit == true)
        { txtAcciDate.Text = clsASS.ini_valuetext; }
    }
}