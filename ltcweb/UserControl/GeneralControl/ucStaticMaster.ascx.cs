﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class UserControl_GeneralControl_ucStaticMaster : System.Web.UI.UserControl
{
    clsStaticMaster clsStatic = new clsStaticMaster();
    private string strDisp = "";
    private Int64 intId = 0;
    private string strName = "";

    public string DisplayFlag
    {
        get { return strDisp; }
        set { strDisp = value; }
    }

    public Int64 SelectedValue
    {
        get { intId = Convert.ToInt64(ddlStatic.SelectedValue); return intId; }
        set
        {
            intId = value;
            if (ddlStatic.Items.Count == 0)
            { Bind(); }
            ListItem liStatic = ddlStatic.Items.FindByValue(intId.ToString());
            if (liStatic != null)
            {
                ddlStatic.SelectedIndex = ddlStatic.Items.IndexOf(liStatic);
            }
        }
    }

    public string SelectedName
    {
        get { strName = ddlStatic.SelectedItem.Text; return strName; }
        set
        {
            strName = value;
            ListItem liStatic = ddlStatic.Items.FindByText(strName.ToString());
            if (liStatic != null)
            {
                ddlStatic.SelectedIndex = ddlStatic.Items.IndexOf(liStatic);
            }
        }
    }

    public int Width
    {
        set { ddlStatic.Width = Unit.Pixel(value); }
    }

    private void Bind()
    {
        ddlStatic.Items.Clear();
        ddlStatic.SelectedIndex = -1;
        ddlStatic.SelectedValue = null;
        ddlStatic.ClearSelection();
        ddlStatic.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlStatic.Items.Add(lst);
        ddlStatic.DataSource = clsStatic.GetFlagWise(strDisp);
        ddlStatic.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}