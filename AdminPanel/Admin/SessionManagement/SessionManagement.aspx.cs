﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Admin_SessionManagement_SessionManagement : System.Web.UI.Page
{
    clsProfileMaster clsPRO = new clsProfileMaster();
    clsClientSession clsSSN = new clsClientSession();

    private void BindProfileDetail()
    {
        if (clsPRO.GetRecordByMemberKeyInProperties(Session["sessionuserid"].ToString()))
        {
            lblClientName.Text = clsPRO.prm_firstname + " " + clsPRO.prm_lastname;
        }
        else { lblClientName.Text = "Not Found..!!"; }
    }
    private void BindGridData()
    {
        clsSSN.ssn_clientid = new Guid(Session["sessionuserid"].ToString());
        rptSession.DataSource = clsSSN.GetAllRecordClientWise();
        rptSession.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["sessionuserid"] != null)
        {
            if (!Page.IsPostBack)
            {
                BindProfileDetail();
                BindGridData();
            }
        }
        else { Response.Redirect("~/Users/UserClientManage.aspx"); }

    }

    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Admin/SessionManagement/SessionAddEdit.aspx");
    }
    protected void rptSession_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            Int64 intEditID = Convert.ToInt64(e.CommandArgument);
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = intEditID;
            Response.Redirect("~/Admin/SessionManagement/SessionAddEdit.aspx");
        }
    }
}