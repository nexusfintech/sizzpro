﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsWorkbookToChapters
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsWorkbookToChapters()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsWorkbookToChapters()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _wtc_id; }
            set
            {
                _wtc_id = value;
                if (clsCMD.Parameters.Contains("@wtc_id"))
                { clsCMD.Parameters["@wtc_id"].Value = value; }
                else { clsCMD.Parameters.Add("@wtc_id", SqlDbType.BigInt).Value = value; }
            }
        }

        private Guid _ByUser = new Guid();
        public Guid ByUser
        {
            get { return _ByUser; }
            set
            {
                _ByUser = value;
                if (clsCMD.Parameters.Contains("@User"))
                { clsCMD.Parameters["@User"].Value = value; }
                else { clsCMD.Parameters.Add("@User", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _wtc_id;
        public Int64 wtc_id
        {
            get { return _wtc_id; }
            set
            {
                _wtc_id = value;
                if (clsCMD.Parameters.Contains("@wtc_id"))
                { clsCMD.Parameters["@wtc_id"].Value = value; }
                else { clsCMD.Parameters.Add("@wtc_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _wtc_workbookid;
        public Int64 wtc_workbookid
        {
            get { return _wtc_workbookid; }
            set
            {
                _wtc_workbookid = value;
                if (clsCMD.Parameters.Contains("@wtc_workbookid"))
                { clsCMD.Parameters["@wtc_workbookid"].Value = value; }
                else { clsCMD.Parameters.Add("@wtc_workbookid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _wtc_chapterid;
        public Int64 wtc_chapterid
        {
            get { return _wtc_chapterid; }
            set
            {
                _wtc_chapterid = value;
                if (clsCMD.Parameters.Contains("@wtc_chapterid"))
                { clsCMD.Parameters["@wtc_chapterid"].Value = value; }
                else { clsCMD.Parameters.Add("@wtc_chapterid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _wtc_position;
        public Int64 wtc_position
        {
            get { return _wtc_position; }
            set
            {
                _wtc_position = value;
                if (clsCMD.Parameters.Contains("@wtc_position"))
                { clsCMD.Parameters["@wtc_position"].Value = value; }
                else { clsCMD.Parameters.Add("@wtc_position", SqlDbType.BigInt).Value = value; }
            }
        }

        private Guid _wtc_userid;
        public Guid wtc_userid
        {
            get { return _wtc_userid; }
            set
            {
                _wtc_userid = value;
                if (clsCMD.Parameters.Contains("@wtc_userid"))
                { clsCMD.Parameters["@wtc_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@wtc_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                wtc_id = Convert.ToInt64(dr["wtc_id"]);
                wtc_workbookid = Convert.ToInt64(dr["wtc_workbookid"]);
                wtc_chapterid = Convert.ToInt64(dr["wtc_chapterid"]);
                wtc_position = Convert.ToInt64(dr["wtc_position"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            wtc_id = Convert.ToInt64("0");
            wtc_workbookid = Convert.ToInt64("0");
            wtc_chapterid = Convert.ToInt64("0");
            wtc_position = Convert.ToInt64("0");
            wtc_userid = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wtc_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "workbooktochaptersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 wtc_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wtc_id = wtc_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "workbooktochaptersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 wtc_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wtc_id = wtc_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "workbooktochaptersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            OpenConnection();
            return blnResult;
        }

        public Boolean DeleteByWorkbookID(Int64 intWrbID)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wtc_workbookid = intWrbID;
            this.AddEditDeleteFlag = "DELETEWRBID";
            try
            {
                clsCMD.CommandText = "workbooktochaptersAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                clsCMD.CommandText = "workbooktochaptersGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetChapterWorkbookWise(Int64 intWorkbookID)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                this.wtc_workbookid = intWorkbookID;
                clsCMD.CommandText = "workbooktochaptersGetData";
                SetGetSPFlag = "GETCHAPTERWBWISE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllByWorkbookID(Int64 intWrbID,Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.wtc_workbookid = intWrbID;
                this.ByUser = MemberId;
                clsCMD.CommandText = "workbooktochaptersGetData";
                SetGetSPFlag = "BYWRBID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<Int64> GetChpByWorkbookID(Int64 intWrbID, Guid MemberId)
        {

            OpenConnection();
            List<Int64> chapters = new List<Int64>();
            try
            {
                this.wtc_workbookid = intWrbID;
                this.ByUser = MemberId;
                clsCMD.CommandText = "workbooktochaptersGetData";
                SetGetSPFlag = "BYWRBID";
                SqlDataReader SDR = clsCMD.ExecuteReader();
                while(SDR.Read())
                {
                    Int64 chp = Convert.ToInt64(SDR["chp_id"]);
                    chapters.Add(chp);
                }
                SDR.Close();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                chapters = null;
            }
            CloseConnection();
            return chapters;
        }

        public DataTable GetAllByWorkbookIDNotMapped(Int64 intWrbID,Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.wtc_workbookid= intWrbID;
                this.ByUser = MemberId;
                clsCMD.CommandText = "workbooktochaptersGetData";
                SetGetSPFlag = "BYWRBIDNOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public Int64 GetPreviousChapterStatus(Int64 intCURPosID,Int64 intWRKID,Guid usrUserID)
        {
            OpenConnection();
            Int64 intResult = 0;
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                this.wtc_position = intCURPosID;
                this.wtc_workbookid = intWRKID;
                this.wtc_userid = usrUserID;
                clsCMD.CommandText = "workbooktochaptersGetData";
                SetGetSPFlag = "GETPRVCHPSTS";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                {
                    DataRow dr = dtResult.Rows[0];
                    intResult = Convert.ToInt64(dr["chp_status"]);
                }
                else { intResult = 0; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return intResult;
        }

        public List<clsWorkbookToChapters_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsWorkbookToChapters_PropertiesList> lstResult = new List<clsWorkbookToChapters_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].wtc_id = Convert.ToInt64(dr["wtc_id"]);
                lstResult[i].wtc_workbookid = Convert.ToInt64(dr["wtc_workbookid"]);
                lstResult[i].wtc_chapterid = Convert.ToInt64(dr["wtc_chapterid"]);
                lstResult[i].wtc_position = Convert.ToInt64(dr["wtc_position"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 wtc_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.wtc_id = wtc_id;
                clsCMD.CommandText = "workbooktochaptersGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 wtc_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.wtc_id = wtc_id;
                clsCMD.CommandText = "workbooktochaptersGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public DataTable GetWorkbookChapter(Int64 wrkid)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.wtc_workbookid = wrkid;
                this.ByUser = new Guid();
                clsCMD.CommandText = "workbooktochaptersGetData";
                SetGetSPFlag = "GETWRKCHP";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        #endregion
    }

    public class clsWorkbookToChapters_PropertiesList
    {
        public Int64 wtc_id { get; set; }
        public Int64 wtc_workbookid { get; set; }
        public Int64 wtc_chapterid { get; set; }
        public Int64 wtc_position { get; set; }
    }
}