﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsOrganizationalProfile
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsOrganizationalProfile()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsOrganizationalProfile()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _op_id; }
            set
            {
                _op_id = value;
                if (clsCMD.Parameters.Contains("@op_id"))
                { clsCMD.Parameters["@op_id"].Value = value; }
                else { clsCMD.Parameters.Add("@op_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _op_id;
        public Int64 op_id
        {
            get { return _op_id; }
            set
            {
                _op_id = value;
                if (clsCMD.Parameters.Contains("@op_id"))
                { clsCMD.Parameters["@op_id"].Value = value; }
                else { clsCMD.Parameters.Add("@op_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _op_userid;
        public Guid op_userid
        {
            get { return _op_userid; }
            set
            {
                _op_userid = value;
                if (clsCMD.Parameters.Contains("@op_userid"))
                { clsCMD.Parameters["@op_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@op_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _op_organizationname;
        public String op_organizationname
        {
            get { return _op_organizationname; }
            set
            {
                _op_organizationname = value;
                if (clsCMD.Parameters.Contains("@op_organizationname"))
                { clsCMD.Parameters["@op_organizationname"].Value = value; }
                else { clsCMD.Parameters.Add("@op_organizationname", SqlDbType.NVarChar, 150).Value = value; }
            }
        }
        private String _op_address;
        public String op_address
        {
            get { return _op_address; }
            set
            {
                _op_address = value;
                if (clsCMD.Parameters.Contains("@op_address"))
                { clsCMD.Parameters["@op_address"].Value = value; }
                else { clsCMD.Parameters.Add("@op_address", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _op_officephone;
        public String op_officephone
        {
            get { return _op_officephone; }
            set
            {
                _op_officephone = value;
                if (clsCMD.Parameters.Contains("@op_officephone"))
                { clsCMD.Parameters["@op_officephone"].Value = value; }
                else { clsCMD.Parameters.Add("@op_officephone", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _op_fax;
        public String op_fax
        {
            get { return _op_fax; }
            set
            {
                _op_fax = value;
                if (clsCMD.Parameters.Contains("@op_fax"))
                { clsCMD.Parameters["@op_fax"].Value = value; }
                else { clsCMD.Parameters.Add("@op_fax", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _op_taxid;
        public String op_taxid
        {
            get { return _op_taxid; }
            set
            {
                _op_taxid = value;
                if (clsCMD.Parameters.Contains("@op_taxid"))
                { clsCMD.Parameters["@op_taxid"].Value = value; }
                else { clsCMD.Parameters.Add("@op_taxid", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _op_legalname;
        public String op_legalname
        {
            get { return _op_legalname; }
            set
            {
                _op_legalname = value;
                if (clsCMD.Parameters.Contains("@op_legalname"))
                { clsCMD.Parameters["@op_legalname"].Value = value; }
                else { clsCMD.Parameters.Add("@op_legalname", SqlDbType.NVarChar, 200).Value = value; }
            }
        }
        private String _op_legaladdress;
        public String op_legaladdress
        {
            get { return _op_legaladdress; }
            set
            {
                _op_legaladdress = value;
                if (clsCMD.Parameters.Contains("@op_legaladdress"))
                { clsCMD.Parameters["@op_legaladdress"].Value = value; }
                else { clsCMD.Parameters.Add("@op_legaladdress", SqlDbType.NVarChar).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                op_id = Convert.ToInt64(dr["op_id"]);
                op_userid = new Guid(dr["op_userid"].ToString());
                op_organizationname = Convert.ToString(dr["op_organizationname"]);
                op_address = Convert.ToString(dr["op_address"]);
                op_officephone = Convert.ToString(dr["op_officephone"]);
                op_fax = Convert.ToString(dr["op_fax"]);
                op_taxid = Convert.ToString(dr["op_taxid"]);
                op_legalname = Convert.ToString(dr["op_legalname"]);
                op_legaladdress = Convert.ToString(dr["op_legaladdress"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            op_id = Convert.ToInt64("0");
            op_userid = new Guid();
            op_organizationname = Convert.ToString("NA");
            op_address = Convert.ToString("NA");
            op_officephone = Convert.ToString("NA");
            op_fax = Convert.ToString("NA");
            op_taxid = Convert.ToString("NA");
            op_legalname = Convert.ToString("NA");
            op_legaladdress = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.op_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_OrganizationalProfileAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 op_id)
        {
            Boolean blnResult = false;
            this.op_id = op_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_OrganizationalProfileAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 op_id)
        {
            Boolean blnResult = false;
            this.op_id = op_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_OrganizationalProfileAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_OrganizationalProfileGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsOrganizationalProfile_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsOrganizationalProfile_PropertiesList> lstResult = new List<clsOrganizationalProfile_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].op_id = Convert.ToInt64(dr["op_id"]);
                lstResult[i].op_userid = new Guid(dr["op_userid"].ToString());
                lstResult[i].op_organizationname = Convert.ToString(dr["op_organizationname"]);
                lstResult[i].op_address = Convert.ToString(dr["op_address"]);
                lstResult[i].op_officephone = Convert.ToString(dr["op_officephone"]);
                lstResult[i].op_fax = Convert.ToString(dr["op_fax"]);
                lstResult[i].op_taxid = Convert.ToString(dr["op_taxid"]);
                lstResult[i].op_legalname = Convert.ToString(dr["op_legalname"]);
                lstResult[i].op_legaladdress = Convert.ToString(dr["op_legaladdress"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 op_id)
        {
            Boolean blnResult = false;
            try
            {
                this.op_id = op_id;
                clsCMD.CommandText = "sp_OrganizationalProfileGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean GetRecordByMemberKey(Guid memberId)
        {
            Boolean blnResult = false;
            try
            {
                this.op_userid= memberId;
                clsCMD.CommandText = "sp_OrganizationalProfileGetData";
                SetGetSPFlag = "BYMEMKEY";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 op_id)
        {
            Boolean blnResult = false;
            try
            {
                this.op_id = op_id;
                clsCMD.CommandText = "sp_OrganizationalProfileGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsOrganizationalProfile_PropertiesList
    {
        public Int64 op_id { get; set; }
        public Guid op_userid { get; set; }
        public String op_organizationname { get; set; }
        public String op_address { get; set; }
        public String op_officephone { get; set; }
        public String op_fax { get; set; }
        public String op_taxid { get; set; }
        public String op_legalname { get; set; }
        public String op_legaladdress { get; set; }
    }
}