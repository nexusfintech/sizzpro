﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class UserControl_Assessment_ucAssessmentAddiction : System.Web.UI.UserControl
{
    clsInitialAssessment clsASS = new clsInitialAssessment();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();

    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    private void EditEntry()
    {
        Boolean blnEdit = false;
        Guid objKey = new Guid(strMemberkey);
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, rdoCurrentafair.ClientID);
        if (blnEdit == true)
        {
            ListItem li = rdoCurrentafair.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                rdoCurrentafair.SelectedIndex = rdoCurrentafair.Items.IndexOf(li);
            }
        }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtotheraddiction.ClientID);
        if (blnEdit == true)
        { txtotheraddiction.Text = clsASS.ini_valuetext; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtsexsualdescribe.ClientID);
        if (blnEdit == true)
        { txtsexsualdescribe.Text = clsASS.ini_valuetext; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtwhatalcohol.ClientID);
        if (blnEdit == true)
        { txtwhatalcohol.Text = clsASS.ini_valuetext; }

        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkhomosex.ClientID);
        if (blnEdit == true)
        { chkhomosex.Checked = clsASS.ini_valuebit; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkothersex.ClientID);
        if (blnEdit == true)
        { chkothersex.Checked = clsASS.ini_valuebit; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkpornography.ClientID);
        if (blnEdit == true)
        { chkpornography.Checked = clsASS.ini_valuebit; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            SetValue();
            EditEntry();
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Boolean blnSave = false;
            Guid objKey = new Guid(strMemberkey);
            clsASS.ini_userid = objKey;
            if (txtotheraddiction.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtotheraddiction.Text.Trim();
                clsASS.ini_valueflag = txtotheraddiction.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtsexsualdescribe.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtsexsualdescribe.Text.Trim();
                clsASS.ini_valueflag = txtsexsualdescribe.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtwhatalcohol.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtwhatalcohol.Text.Trim();
                clsASS.ini_valueflag = txtwhatalcohol.ClientID;
                blnSave = clsASS.Save();
            }
            ListItem li = rdoCurrentafair.SelectedItem;
            if (li != null)
            {
                clsASS.ini_valueflag = rdoCurrentafair.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(rdoCurrentafair.SelectedValue);
                blnSave = clsASS.Save();
            }
            clsASS.ini_valuebit = chkhomosex.Checked;
            clsASS.ini_valueflag = chkhomosex.ClientID;
            blnSave = clsASS.Save();
            clsASS.ini_valuebit = chkothersex.Checked;
            clsASS.ini_valueflag = chkothersex.ClientID;
            blnSave = clsASS.Save();
            clsASS.ini_valuebit = chkpornography.Checked;
            clsASS.ini_valueflag = chkpornography.ClientID;
            blnSave = clsASS.Save();

            clsASR.storcrd_stepid = 18;
            clsASR.stprcrd_userid = objKey;
            Boolean stprslt = clsASR.Save();
            if (stprslt == true)
            {
                Response.Redirect("~/Client/Assessments/ClientProblem.aspx");
            }
            lblMessage.Text = "Save Successfully";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error in save process.. of " + clsASS.ini_valueflag + " System Message : " + ex.Message;
        }
    }
}