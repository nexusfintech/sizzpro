﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;


public partial class UserControl_GeneralControl_ucBillingProviderMaster : System.Web.UI.UserControl
{
    clsBillingproviderMaster clsBPM = new clsBillingproviderMaster();
    private Int64 intId = 0;
    private string strName = string.Empty;

    public Int64 SelectedValue
    {
        get { intId = Convert.ToInt64(ddlBillingProvider.SelectedValue); return intId; }
        set { intId = value; }
    }

    public string SelectedName
    {
        get { strName = ddlBillingProvider.SelectedItem.Text; return strName; }
        set { strName = value; }
    }

    public int Width
    {
        set { ddlBillingProvider.Width = Unit.Pixel(value); }
    }

    public void Bind()
    {
        DataTable dt = clsBPM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        ddlBillingProvider.DataTextField = "bpm_name";
        ddlBillingProvider.DataValueField = "bpm_id";
        ddlBillingProvider.DataSource = dt;
        ddlBillingProvider.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}