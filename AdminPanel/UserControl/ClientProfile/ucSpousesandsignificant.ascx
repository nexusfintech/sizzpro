﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSpousesandsignificant.ascx.cs" Inherits="UserControl_ClientProfile_ucSpousesandsignificant" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-user"></i>
                    Spouse & Significant
                </h3>
            </div>
            <div class="box-content nopadding">
                <div class='form-horizontal form-column form-bordered '>
                    <div class="span6">
                        <div class="control-group">
                            <label for="txtFirstName" class="control-label">First Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtFirstName" runat="server" placeholder="Firstname" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtMiddlename" class="control-label">Middle Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtMiddlename" runat="server" placeholder="Middlename" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtlastname" class="control-label">Last Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtlastname" runat="server" placeholder="Last Name" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtAge" class="control-label">Birth Date</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDOB" runat="server" data-rule-required="true" CssClass="input-medium datepick"></asp:TextBox>
                                <span class="help-block">Enter Birthdate in MM/DD/YYYY format.</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtStartDate" class="control-label">Start Date</label>
                            <div class="controls">
                                <asp:TextBox ID="txtStartDate" data-rule-required="true" runat="server" CssClass="input-medium datepick"></asp:TextBox>
                                <span class="help-block">Enter Date in MM/DD/YYYY format.</span>
                            </div>
                        </div>

                    </div>
                    <div class="span6">
                        <div class="control-group">
                            <label for="chklevewithclient" class="control-label">Gender</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlGender" runat="server">
                                    <asp:ListItem Text="Male"></asp:ListItem>
                                    <asp:ListItem Text="Female"></asp:ListItem>
                                    <asp:ListItem Text="TransGender"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="password" class="control-label">Relationship</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlRelationship" runat="server" class="complexify-me">
                                    <asp:ListItem Text="Spouse" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Significant"></asp:ListItem>
                                    <asp:ListItem Text="Other"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtlastname" class="control-label"></label>
                            <div class="controls">
                                <div class="span12 check-line">
                                    <asp:CheckBox ID="chkLive" runat="server" CssClass='icheck-me' data-skin="square" data-color="blue" />
                                    <label class='inline'>Lives With Client..?</label>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtAge" class="control-label"></label>
                            <div class="controls">
                                <br />
                                <br />
                                <br />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="txtAge" class="control-label">End Date</label>
                            <div class="controls">
                                <asp:TextBox ID="txtEndDate" runat="server" CssClass="input-medium datepick"></asp:TextBox>
                                <span class="help-block">Enter Date in MM/DD/YYYY format.</span>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row-fluid">
    <div class="span12">
        <asp:Repeater ID="dtSpouses" runat="server" OnItemCommand="dtSpouses_ItemCommand">
            <HeaderTemplate>
                <table class="table table-hover table-nomargin dataTable table-bordered">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th>Birth Date</th>
                            <th>Gender</th>
                            <th>Live With Client</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("sas_firstname") %></td>
                    <td><%# Eval("sas_middlename") %></td>
                    <td><%# Eval("sas_lastname") %></td>
                    <td><%# Eval("sas_birthdate","{0:MM/dd/yyyy}") %></td>
                    <td><%# Eval("sas_gender") %></td>
                    <td><%# Eval("sas_livewithclient") %></td>
                    <td><%# Eval("sas_startdate","{0:MM/dd/yyyy}") %></td>
                    <td><%# Eval("sas_enddate","{0:MM/dd/yyyy}") %></td>
                    <td><%# Eval("sas_description") %></td>
                    <td>
                        <i class="icon-edit">
                            <asp:Button ID="cmdEdit" runat="server" Text="Edit" CssClass="btn cancel" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "sas_id") %>' />
                        </i>
                    </td>
                    <td>
                        <i class="icon-remove">
                            <asp:Button ID="cmdDelete" runat="server" Text="Delete" CssClass="btn cancel" CommandName="delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "sas_id") %>' />
                        </i>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
