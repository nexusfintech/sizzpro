﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Philosophy.aspx.cs" Inherits="Philosophy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="glyphicon-candle"></i>  Our Philosophy</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <p>We believe everyone deserves to live the most  fulfilling life possible.&nbsp; Unfortunately,  life transitions can create difficulties which prevent individuals and families  from experiencing this kind of fullness and joy.&nbsp; Through our counseling program, we help those  who are suffering identify the cause and begin on the road to healing.&nbsp; We provide compassionate support in helping  our clients pinpoint and utilize the tools needed to weather life&rsquo;s storms, be  they larger or small.</p>
            <p>It is our purpose to provide the  best counseling experience possible from a compassionate, non-judgmental, and  unbiased standpoint.&nbsp; We believe we are  called to use our education, talents and abilities to help support and maintain  strong individuals, marriages and families.</p>
            <p>Our philosophy is to listen to our clients and respond with empathy and wisdom. Our desire is to help you make decisions that lead you to greater productivity and happiness in life. We want you to feel responsible for your life and see us as skilled listeners there to help you get where you want to be. We seek to ensure that our counsel rests on timeless Biblical principles. Our goal is that from the time you spend with us you will grow in knowledge and understanding. Our desire is not for you to become dependent upon us but dependent on the Lord for your sustenance and strength, wisdom and understanding, and motivation and change.  </p>
            <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

