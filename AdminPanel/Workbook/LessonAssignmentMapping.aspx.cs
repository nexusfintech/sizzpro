﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;

public partial class Workbook_LessonAssignmentMapping : System.Web.UI.Page
{
    clsLessions clsLSN = new clsLessions();
    clsLessionToAssignment clsLTA = new clsLessionToAssignment();
    clsClientParentMapping clsCPM = new clsClientParentMapping();

    private void BindCombo(Guid UserId)
    {
        DataTable dtRslt = clsLSN.GetAllRecordByMember(UserId);
        ddlLesson.Items.Clear();
        ddlLesson.SelectedIndex = -1;
        ddlLesson.SelectedValue = null;
        ddlLesson.ClearSelection();
        ddlLesson.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlLesson.Items.Add(lst);
        dtRslt.Columns.Add("Lesson", typeof(string), "lsn_version + ' - '  + lsn_name");
        ddlLesson.DataValueField = "lsn_id";
        ddlLesson.DataTextField = "Lesson";
        ddlLesson.DataSource = dtRslt;
        ddlLesson.DataBind();
    }

    private void BindListView(Int64 intlsnid, Guid Userid)
    {
        DataTable dtleft = clsLTA.GetAllByLessonIDNotMapped(intlsnid, Userid);
        DataTable dtright = clsLTA.GetAllByLessonID(intlsnid, Userid);

        RptrLeft.DataSource = dtleft;
        RptrLeft.DataBind();

        RptrRight.DataSource = dtright;
        RptrRight.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (User.IsInRole("administrator"))
            {
                BindMemberCombo();
            }
            else
            {
                BindCombo((Guid)Membership.GetUser().ProviderUserKey);
            }
        }
    }

    protected void cmdSelect_Click(object sender, EventArgs e)
    {
        Session["lsnid"] = ddlLesson.SelectedValue.ToString();
        Int64 intlsn = Convert.ToInt64(Session["lsnid"]);
        hdLessonid.Value = ddlLesson.SelectedValue.ToString(); 
        lblBoxHeader.Text = "( " + ddlLesson.SelectedItem.Text + " ) Lesson Selected";
        pnlMapping.Visible = true;
        if (User.IsInRole("administrator"))
        {
            BindListView(intlsn, new Guid(ddlSelectMember.SelectedValue.ToString()));
        }
        else
        {
            BindListView(intlsn, (Guid)Membership.GetUser().ProviderUserKey);
        }
    }

    private void BindMemberCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRole("Member");
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }
    protected void ddlSelectMember_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCombo(new Guid(ddlSelectMember.SelectedValue.ToString()));
    }
}