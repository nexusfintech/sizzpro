﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;
using System.IO;

public partial class Agreements_AgreementMapping : System.Web.UI.Page
{
    clsAgreementServiceMapping clsASM = new clsAgreementServiceMapping();

    public long AgreementId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (MySession.Current.EditID == 0)
            {
                Response.Redirect("~/Agreements/AgreementMaster.aspx");
            }
            else
            {
                AgreementId = MySession.Current.EditID;
                loadmapping();
            }
        }
    }

    private void loadmapping()
    {
        DataTable dtResult = clsASM.GetRecordByAgreement(AgreementId);
        dtAgreementMping.DataSource = dtResult;
        dtAgreementMping.DataBind();
    }

    protected void CmdMappingRmv_Click(object sender, EventArgs e)
    {
        LinkButton btnRemove = (LinkButton)sender;
        Boolean blnreslt = clsASM.DeleteByPKID(Convert.ToInt64(btnRemove.CommandArgument));
        Response.Redirect("~/Agreements/AgreementMaster.aspx");
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Agreements/AddEditAgreement.aspx");
    }
}