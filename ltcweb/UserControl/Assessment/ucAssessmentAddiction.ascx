﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAssessmentAddiction.ascx.cs" Inherits="UserControl_Assessment_ucAssessmentAddiction" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="glyphicon-smoking"></i>
                    Addiction
                </h3>
            </div>
            <div class="box-content">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="span12">
                        <label>What is your history of alcohol/tobacco/chemical use?</label>
                        <asp:TextBox ID="txtwhatalcohol" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <div class="span6">
                            <label>Have you had or are you currently having an affair?</label>
                        </div>
                        <div class="span6">
                            <asp:RadioButtonList ID="rdoCurrentafair" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="2" Text="No"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span6">
                            <label>Do you have current or past involvement in</label>
                        </div>
                        <div class="span6">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkpornography" runat="server" Text="Pornography"></asp:CheckBox></td>
                                    <td>
                                        <asp:CheckBox ID="chkhomosex" runat="server" Text="Homosexuality"></asp:CheckBox></td>
                                    <td>
                                        <asp:CheckBox ID="chkothersex" runat="server" Text="Other deviant sexual behaviour?"></asp:CheckBox></td>
                                </tr>
                            </table>
                        </div>
                        <div class="span12">
                            <label>Describe</label>
                            <asp:TextBox ID="txtsexsualdescribe" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                        </div>
                        <div class="span12">
                            <label>Is there any other addiction(s) that you struggle with? </label>
                            <asp:TextBox ID="txtotheraddiction" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
