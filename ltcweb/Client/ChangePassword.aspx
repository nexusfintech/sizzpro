﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-key"></i>Password Change</h3>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-bordered'>
                        <div class="control-group">
                            <label for="password" class="control-label">Old Password</label>
                            <div class="controls">
                                <asp:TextBox ID="txtOldPassword" runat="server" placeholder="Old password" class="complexify-me input-xlarge" TextMode="Password"></asp:TextBox>
                                <span class="help-block">Password Strength
                                            <div class="progress progress-info">
                                                <div class="bar bar-red" style="width: 0%"></div>
                                            </div>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="password" class="control-label">New Password</label>
                            <div class="controls">
                                <asp:TextBox ID="txtNewPassword" runat="server" placeholder="New password" class="complexify-me input-xlarge" TextMode="Password"></asp:TextBox>
                                <span class="help-block">Password Strength
                                            <div class="progress progress-info">
                                                <div class="bar bar-red" style="width: 0%"></div>
                                            </div>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="password" class="control-label">Conform Password</label>
                            <div class="controls">
                                <asp:TextBox ID="txtConformPAssword" runat="server" placeholder="Confirm new password" class="complexify-me input-xlarge" TextMode="Password"></asp:TextBox>
                                <span class="help-block">Password Strength
                                            <div class="progress progress-info">
                                                <div class="bar bar-red" style="width: 0%"></div>
                                            </div>
                                </span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:Button ID="cmdChnagePass" runat="server" CssClass="btn btn-primary" Text="Change Password" OnClick="cmdChnagePass_Click" />
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

