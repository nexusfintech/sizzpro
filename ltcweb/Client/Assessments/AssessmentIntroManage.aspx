﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AssessmentIntroManage.aspx.cs" Inherits="Clients_Assessment_AssessmentIntroManage" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpMainContent" runat="Server">
    <asp:HiddenField ID="AssesmentStep" runat="server" Value="1" />
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="glyphicon-notes"></i>
                Intro Content
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:ManageIntroContent ID="ucIntro" runat="server" />
        </div>
    </div>
    <div class="row-fluid">
        <hr />
        <div class="span12">
            <asp:Button ID="cmdNext" runat="server" Text="Next  You & Profile >>" CssClass="btn btn-primary" OnClick="cmdNext_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

