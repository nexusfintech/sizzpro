﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RegistrationSuccessfull.aspx.cs" Inherits="RegistrationSuccessfull" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="glyphicon-ok_2"></i>&nbsp;Registration Successful.</h2>
        </div>
    </div>
    <hr />
    <h3>Congratulations, Your event is registered successfully.</h3>
    <h5>Please note that your place can only be confirmed once has received your detail on Registered email</h5>
    <div class="row-fluid">
        <div class="span12">
            <div class="span6">
                <div class="box">
                    <div class="box-title">
                        <h3>
                            <i class="icon-user"></i>
                            User Registration Detail
								</h3>
                    </div>
                    <div class="box-content nopadding">
                        <table class="table table-hover table-nomargin table-condensed">
                            <thead>
                                <tr>
                                    <th>Particular</th>
                                    <th>value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>First Name</td>
                                    <td><%# Session["reg_firstname"].ToString() %></td>
                                </tr>
                                <tr>
                                    <td>Middle Name</td>
                                    <td><%# Session["reg_middlename"].ToString() %></td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td><%# Session["reg_lastname"].ToString() %></td>
                                </tr>
                                <tr>
                                    <td>Registred Email</td>
                                    <td><%# Session["reg_email"].ToString() %></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <p><b>Note : </b>if you not getting account activation mail so please click on below  button </p>
            <div class="form-actions">
                <asp:Button ID="cmdResend" runat="server" CssClass="btn btn-primary" Text="Resend Account Activation Mail" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

