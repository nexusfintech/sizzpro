﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
using System.Text;
using System.IO;
using System.Globalization;
using System.Data;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net;
using System.Configuration;

public partial class informedConsentDetails : System.Web.UI.Page
{
    clsClientInformconcent clsCIC = new clsClientInformconcent();
    clsProfileMaster clsPro = new clsProfileMaster();
    clsICdetails_PropertiesList icP = new clsICdetails_PropertiesList();
    clsICdetails icPathDetail = new clsICdetails();
    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strClientKey;
    string strAdminKey;

    public string Memberkey
    {
        get { return strClientKey; }
        set { strClientKey = value; }
    }

    public string AdminKey
    {
        get { return strAdminKey; }
        set { strAdminKey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strClientKey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        //string uname = HttpContext.Current.User.Identity.Name.ToString();
        //MembershipUser mu = Membership.GetUser(uname);
        intUserID = Convert.ToInt64(Session["form_userid"]);
            if (Request.QueryString["ui"] != null)
            {
                strClientKey = Request.QueryString["ui"].ToString();
            }
            else
            {
                Session["msg_boxheader"] = "Interenal Error";
                Session["msg_message"] = "Opps ..! Some internal server error occured";
                Response.Redirect("Messages.aspx");
            }
            //EditEntry();
    }

    private void EditEntry()
    {
        bool blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "nameoncreditcard");
        if (blnEdit == true)
        {
            nameoncreditcard.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "cardnumber");
        if (blnEdit == true)
        {
            cardnumber.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "creditcardexpirationdate");
        if (blnEdit == true)
        {
            creditcardexpirationdate.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "creditcardsecnumber");
        if (blnEdit == true)
        {
            creditcardsecnumber.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "authorisationphysian");
        if (blnEdit == true)
        {
            authorisationphysian.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "authorizationaddress");
        if (blnEdit == true)
        {
            authorizationaddress.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "authorizationcity");
        if (blnEdit == true)
        {
            authorizationcity.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "authorizationstate");
        if (blnEdit == true)
        {
            authorizationstate.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "authorizationzip");
        if (blnEdit == true)
        {
            authorizationzip.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "authorizationphn");
        if (blnEdit == true)
        {
            authorizationphn.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "authorizationfax");
        if (blnEdit == true)
        {
            authorizationfax.Text = clsCIC.cic_value;
        }
        blnEdit = clsCIC.GetRecordByIDInProperties(new Guid(strClientKey), "authorizationend");
        if (blnEdit == true)
        {
            authorizationend.Text = clsCIC.cic_value;
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Boolean blnSave = false;
        Guid objKey = new Guid(strClientKey);
        clsCIC.cic_clientid = objKey;
        foreach (Control control in InformConcentData.Controls)
        {
            if (control.GetType() == typeof(TextBox))
            {
                clsCIC.cic_valueflag = control.ID.ToString();
                TextBox txtbx = control as TextBox;
                clsCIC.cic_value = txtbx.Text.ToString();
                blnSave = clsCIC.Save();
                clsCIC.cic_value = string.Empty;
            }
            if (control.GetType() == typeof(DropDownList))
            {
                clsCIC.cic_valueflag = control.ID.ToString();
                DropDownList drpdwn = control as DropDownList;
                clsCIC.cic_value = drpdwn.SelectedItem.Text.ToString();
                blnSave = clsCIC.Save();
                clsCIC.cic_value = string.Empty;
            }
        }
        foreach (System.Web.UI.WebControls.ListItem itms in corspondenceby.Items)
        {
            clsCIC.cic_valueflag = "corspondenceby";
            if (itms.Selected)
            {
                clsCIC.cic_value = clsCIC.cic_value+"@"+itms.Text;
                clsCIC.cic_value=clsCIC.cic_value.TrimStart('@');
            }
            blnSave = clsCIC.Save();
        }
        clsCIC.cic_valueflag = "sts";
        clsCIC.cic_value = true.ToString();
        if (blnSave == true)
        {
            lblMessage.Text = "Client created successfully.";
            Session["msg_boxheader"] ="Registration complete";
            Session["msg_message"] = "Your account created successfully. To activate your account please visit activation link to your registered email.";
            Response.Redirect("Messages.aspx");
        }
    }

    private void createpdfrequest()
    {
        strAdminKey = Membership.GetUser(ConfigurationManager.AppSettings["memberuserid"].ToString()).ProviderUserKey.ToString();
        string url = ConfigurationManager.AppSettings["SizzUrl"].ToString() + "/InformConcent.aspx?mode=create&m=" + strAdminKey + "&c=" + strClientKey;
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.Method = "POST";
        request.ContentLength = 0;
        var response = (HttpWebResponse)request.GetResponse();
        int statuscode = Convert.ToInt32(response.StatusCode);
        if (statuscode == 1)
        {
            Session["msg_boxheader"] = "Success";
            Session["msg_message"] = "Hello, " + Membership.GetUser().UserName+" Congratulation <br/> Your Inform concent Completed Successfully <br/> Now Click <a href='Client/Appointment.aspx'>Here</a> to make appoinment with one of our Therpist";
            Response.Redirect("~/Messages.aspx");
        }
        if (statuscode == 2)
        {
            Session["msg_boxheader"] = "Error";
            Session["msg_message"] = "Hello "+ Membership.GetUser().UserName+" Sorry ..! <br/> Server is not able to save your file data to database . pleasee try again <br/> If you are continue to getting this error then contact administrator of this site .";
            Response.Redirect("~/Messages.aspx");
        }
        if (statuscode == 3)
        {
            Session["msg_boxheader"] = "Error";
            Session["msg_message"] = "Hello " + Membership.GetUser().UserName + "Sorry ..! <br/> Server is not able to cretae your Inform Concent File . pleasee try again <br/> If you are continue to getting this error then contact administrator of this site .";
            Response.Redirect("~/Messages.aspx");
        }
    }
}