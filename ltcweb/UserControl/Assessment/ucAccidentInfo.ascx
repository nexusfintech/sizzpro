﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAccidentInfo.ascx.cs" Inherits="UserControl_Assessment_ucAccidentInfo" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-ambulance"></i>
                    Accident Information
                </h3>
            </div>
            <div class="box-content ">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="span12">
                        <label>Is condition due to an accident?</label>
                        <asp:CheckBoxList ID="chkduetoAccident" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="2" Text="No"></asp:ListItem>
                        </asp:CheckBoxList>
                    </div>
                    <div class="span12">
                        <label for="txtAcciDate" class="control-label right">Accident Date</label>

                        <asp:TextBox ID="txtAcciDate" runat="server" data-rule-required="true" CssClass="input-small datepick"></asp:TextBox>

                    </div>

                    <div class="span12">
                        <label>Type of Accident</label>
                        <asp:CheckBoxList ID="chkAccidenteType" runat="server" RepeatDirection="Horizontal" Width="40%">
                            <asp:ListItem Value="1" Text="Auto"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Work"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Home"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Other"></asp:ListItem>
                        </asp:CheckBoxList>
                    </div>

                    <div class="span12">
                        <label>To whom have you made a report of your accident??</label>
                        <asp:CheckBoxList ID="chkaccContact" runat="server" RepeatDirection="Horizontal" Width="40%">
                            <asp:ListItem Value="1" Text="insurance"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Employer"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Worker Company"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Other"></asp:ListItem>
                        </asp:CheckBoxList>
                    </div>

                    <div class="span12">
                        <label>Attorney Name (if applicable)</label>
                        <asp:TextBox ID="txtAttorney" runat="server" TextMode="SingleLine" Width="60%"></asp:TextBox>
                    </div>

                    <div class="span12">
                        <label>Description of the accident</label>
                        <asp:TextBox ID="txAccidnttDesc" runat="server" TextMode="MultiLine" Width="60%"></asp:TextBox>
                    </div>

                    <div class="span12">
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

