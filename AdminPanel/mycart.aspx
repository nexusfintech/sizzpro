﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSite.master" AutoEventWireup="true" CodeFile="mycart.aspx.cs" Inherits="mycart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid" style="border: 1px solid #0072D6;">
        <div class="span12">
            <%--<asp:Literal ID="ltrPageBody" runat="server"></asp:Literal>--%>
            <h4 style="text-align: center">My Cart</h4>
            <hr />
            <asp:Repeater ID="rptcart" runat="server">
                <HeaderTemplate>
                    <table class="table table-nomargin">
                        <thead>
                            <tr>
                                <th style="text-align: left">Id</th>
                                <th style="text-align: left">Service</th>
                                <th style="text-align: left">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("id") %></td>
                        <td><%# Eval("service") %></td>
                        <td><%# Eval("price") %></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                        </table>
                </FooterTemplate>
            </asp:Repeater>
            <div class="span12">
                <div class="span8"></div>
                <div class="span4">
                    <table class="table">
                        <tr>
                            <td>
                                <h4>Total : 
                                                <asp:Label ID="lbltotal" runat="server"></asp:Label></h4>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btn_payment" runat="server" CssClass="btn btn-primary" OnClick="btn_payment_Click" Text="Procede To Payment" />
                                <%--<a href="Checkout.aspx" class="btn">Back</a>--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <%--<div class="span12" style="margin: auto; padding: 15px;">
                            <div class="span6">
                                <a href="Default.aspx" class="btn btn-block btn-primary">
                                    <h6>Home</h6>
                                </a>
                            </div>
                            <div class="span6">
                                <a href="Default.aspx" class="btn btn-block btn-green">
                                    <h6>Go To Cart</h6>
                                </a>
                            </div>
                        </div>--%>
        </div>
    </div>
</asp:Content>

