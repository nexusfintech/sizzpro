﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace clsDAL.Conn
{
    public class clsConnection
    {
        public SqlConnection SqlDBCon = new SqlConnection();
        public String strGeneratedError = "";

        public Boolean GetConnection()
        {
            Boolean blnResult = false;
            try
            {
                //SqlDBCon.ConnectionString = @"Data Source=ADMIN\SQLEXPRESS;Initial Catalog=dbshp;Integrated Security=False;User ID=sa;Password=12345";
                //SqlDBCon.ConnectionString = @"Data Source=ADMIN\SQLEXPRESS;Initial Catalog=dbshp;Integrated Security=False;User ID=sa;Password=12345;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";

                SqlDBCon.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ToString();
                if (SqlDBCon.State == ConnectionState.Closed)
                {
                    SqlDBCon.Open();
                    blnResult = true;
                }
            }
            catch (Exception ex)
            {
                blnResult = false;
                strGeneratedError = ex.Message;
            }
            return blnResult;
        }
    }
}
