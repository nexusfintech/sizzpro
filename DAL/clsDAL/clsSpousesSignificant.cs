﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsSpousesSignificant
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsSpousesSignificant()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsSpousesSignificant()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _sas_id; }
            set
            {
                _sas_id = value;
                if (clsCMD.Parameters.Contains("@sas_id"))
                { clsCMD.Parameters["@sas_id"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _sas_id;
        public Int64 sas_id
        {
            get { return _sas_id; }
            set
            {
                _sas_id = value;
                if (clsCMD.Parameters.Contains("@sas_id"))
                { clsCMD.Parameters["@sas_id"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _sas_userid;
        public Guid sas_userid
        {
            get { return _sas_userid; }
            set
            {
                _sas_userid = value;
                if (clsCMD.Parameters.Contains("@sas_userid"))
                { clsCMD.Parameters["@sas_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _sas_firstname;
        public String sas_firstname
        {
            get { return _sas_firstname; }
            set
            {
                _sas_firstname = value;
                if (clsCMD.Parameters.Contains("@sas_firstname"))
                { clsCMD.Parameters["@sas_firstname"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_firstname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _sas_middlename;
        public String sas_middlename
        {
            get { return _sas_middlename; }
            set
            {
                _sas_middlename = value;
                if (clsCMD.Parameters.Contains("@sas_middlename"))
                { clsCMD.Parameters["@sas_middlename"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_middlename", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _sas_lastname;
        public String sas_lastname
        {
            get { return _sas_lastname; }
            set
            {
                _sas_lastname = value;
                if (clsCMD.Parameters.Contains("@sas_lastname"))
                { clsCMD.Parameters["@sas_lastname"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_lastname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private DateTime _sas_birthdate;
        public DateTime sas_birthdate
        {
            get { return _sas_birthdate; }
            set
            {
                _sas_birthdate = value;
                if (clsCMD.Parameters.Contains("@sas_birthdate"))
                { clsCMD.Parameters["@sas_birthdate"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_birthdate", SqlDbType.DateTime).Value = value; }
            }
        }
        private String _sas_gender;
        public String sas_gender
        {
            get { return _sas_gender; }
            set
            {
                _sas_gender = value;
                if (clsCMD.Parameters.Contains("@sas_gender"))
                { clsCMD.Parameters["@sas_gender"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_gender", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _sas_description;
        public String sas_description
        {
            get { return _sas_description; }
            set
            {
                _sas_description = value;
                if (clsCMD.Parameters.Contains("@sas_description"))
                { clsCMD.Parameters["@sas_description"].Size = value.Length; clsCMD.Parameters["@sas_description"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_description", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Boolean _sas_livewithclient;
        public Boolean sas_livewithclient
        {
            get { return _sas_livewithclient; }
            set
            {
                _sas_livewithclient = value;
                if (clsCMD.Parameters.Contains("@sas_livewithclient"))
                { clsCMD.Parameters["@sas_livewithclient"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_livewithclient", SqlDbType.Bit).Value = value; }
            }
        }
        private DateTime? _sas_startdate;
        public DateTime? sas_startdate
        {
            get { return _sas_startdate; }
            set
            {
                _sas_startdate = value;
                if (clsCMD.Parameters.Contains("@sas_startdate"))
                { if (value == null) { clsCMD.Parameters["@sas_startdate"].Value = DBNull.Value; } else { clsCMD.Parameters["@sas_startdate"].Value = value; } }
                else { if (value == null) { clsCMD.Parameters.Add("@sas_startdate", SqlDbType.DateTime).Value = DBNull.Value; } else { clsCMD.Parameters.Add("@sas_startdate", SqlDbType.DateTime).Value = value; } }
            }
        }
        private DateTime? _sas_enddate;
        public DateTime? sas_enddate
        {
            get { return _sas_enddate; }
            set
            {
                _sas_enddate = value;
                if (clsCMD.Parameters.Contains("@sas_enddate"))
                { if (value == null) { clsCMD.Parameters["@sas_enddate"].Value = DBNull.Value; } else { clsCMD.Parameters["@sas_enddate"].Value = value; } }
                else { if (value == null) { clsCMD.Parameters.Add("@sas_enddate", SqlDbType.DateTime).Value = DBNull.Value; } else { clsCMD.Parameters.Add("@sas_enddate", SqlDbType.DateTime).Value = value; } }
            }
        }
        private String _sas_relationship;
        public String sas_relationship
        {
            get { return _sas_relationship; }
            set
            {
                _sas_relationship = value;
                if (clsCMD.Parameters.Contains("@sas_relationship"))
                { clsCMD.Parameters["@sas_relationship"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_relationship", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _sas_relationflag;
        public String sas_relationflag
        {
            get { return _sas_relationflag; }
            set
            {
                _sas_relationflag = value;
                if (clsCMD.Parameters.Contains("@sas_relationflag"))
                { clsCMD.Parameters["@sas_relationflag"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_relationflag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _sas_parentid;
        public Int64 sas_parentid
        {
            get { return _sas_parentid; }
            set
            {
                _sas_parentid = value;
                if (clsCMD.Parameters.Contains("@sas_parentid"))
                { clsCMD.Parameters["@sas_parentid"].Value = value; }
                else { clsCMD.Parameters.Add("@sas_parentid", SqlDbType.BigInt).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                sas_id = Convert.ToInt64(dr["sas_id"]);
                sas_userid = new Guid(dr["sas_userid"].ToString());
                sas_firstname = Convert.ToString(dr["sas_firstname"]);
                sas_middlename = Convert.ToString(dr["sas_middlename"]);
                sas_lastname = Convert.ToString(dr["sas_lastname"]);
                sas_birthdate = Convert.ToDateTime(dr["sas_birthdate"]);
                sas_gender = Convert.ToString(dr["sas_gender"]);
                sas_description = Convert.ToString(dr["sas_description"]);
                sas_livewithclient = Convert.ToBoolean(dr["sas_livewithclient"]);
                sas_startdate = Convert.ToDateTime(dr["sas_startdate"]);
                sas_enddate = Convert.ToDateTime(dr["sas_enddate"]);
                sas_relationship = Convert.ToString(dr["sas_relationship"]);
                sas_relationflag = Convert.ToString(dr["sas_relationflag"]);
                sas_parentid = Convert.ToInt64(dr["sas_parentid"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            sas_id = Convert.ToInt64("0");
            sas_userid = new Guid();
            sas_firstname = Convert.ToString("NA");
            sas_middlename = Convert.ToString("NA");
            sas_lastname = Convert.ToString("NA");
            sas_birthdate = Convert.ToDateTime(DateTime.Now);
            sas_gender = Convert.ToString("NA");
            sas_description = Convert.ToString("NA");
            sas_livewithclient = Convert.ToBoolean(false);
            //sas_startdate = Convert.ToDateTime(DateTime.Now);
            //sas_enddate = Convert.ToDateTime(DateTime.Now);
            sas_relationship = Convert.ToString("NA");
            sas_relationflag = Convert.ToString("NA");
            sas_parentid = Convert.ToInt64("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function
        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.sas_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_spousesandsignificantAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 sas_id)
        {
            Boolean blnResult = false;
            this.sas_id = sas_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_spousesandsignificantAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 sas_id)
        {
            Boolean blnResult = false;
            this.sas_id = sas_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_spousesandsignificantAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_spousesandsignificantGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllByUserID(Guid UserID)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.sas_userid = UserID;
                clsCMD.CommandText = "sp_spousesandsignificantGetData";
                SetGetSPFlag = "BYUSERID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsSpousesSignificant_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsSpousesSignificant_PropertiesList> lstResult = new List<clsSpousesSignificant_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].sas_id = Convert.ToInt64(dr["sas_id"]);
                lstResult[i].sas_userid = new Guid(dr["sas_userid"].ToString());
                lstResult[i].sas_firstname = Convert.ToString(dr["sas_firstname"]);
                lstResult[i].sas_middlename = Convert.ToString(dr["sas_middlename"]);
                lstResult[i].sas_lastname = Convert.ToString(dr["sas_lastname"]);
                lstResult[i].sas_birthdate = Convert.ToDateTime(dr["sas_birthdate"]);
                lstResult[i].sas_gender = Convert.ToString(dr["sas_gender"]);
                lstResult[i].sas_description = Convert.ToString(dr["sas_description"]);
                lstResult[i].sas_livewithclient = Convert.ToBoolean(dr["sas_livewithclient"]);
                lstResult[i].sas_startdate = Convert.ToDateTime(dr["sas_startdate"]);
                lstResult[i].sas_enddate = Convert.ToDateTime(dr["sas_enddate"]);
                lstResult[i].sas_relationship = Convert.ToString(dr["sas_relationship"]);
                lstResult[i].sas_relationflag = Convert.ToString(dr["sas_relationflag"]);
                lstResult[i].sas_parentid = Convert.ToInt64(dr["sas_parentid"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 sas_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.sas_id = sas_id;
                clsCMD.CommandText = "sp_spousesandsignificantGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 sas_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.sas_id = sas_id;
                clsCMD.CommandText = "sp_spousesandsignificantGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }
    public class clsSpousesSignificant_PropertiesList
    {
        public Int64 sas_id { get; set; }
        public Guid sas_userid { get; set; }
        public String sas_firstname { get; set; }
        public String sas_middlename { get; set; }
        public String sas_lastname { get; set; }
        public DateTime sas_birthdate { get; set; }
        public String sas_gender { get; set; }
        public String sas_description { get; set; }
        public Boolean sas_livewithclient { get; set; }
        public DateTime sas_startdate { get; set; }
        public DateTime sas_enddate { get; set; }
        public String sas_relationship { get; set; }
        public String sas_relationflag { get; set; }
        public Int64 sas_parentid { get; set; }
    }
}