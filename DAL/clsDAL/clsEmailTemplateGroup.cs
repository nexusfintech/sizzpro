﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsEmailTemplateGroup
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsEmailTemplateGroup()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsEmailTemplateGroup()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _mtg_id; }
            set
            {
                _mtg_id = value;
                if (clsCMD.Parameters.Contains("@mtg_id"))
                { clsCMD.Parameters["@mtg_id"].Value = value; }
                else { clsCMD.Parameters.Add("@mtg_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _mtg_id;
        public Int64 mtg_id
        {
            get { return _mtg_id; }
            set
            {
                _mtg_id = value;
                if (clsCMD.Parameters.Contains("@mtg_id"))
                { clsCMD.Parameters["@mtg_id"].Value = value; }
                else { clsCMD.Parameters.Add("@mtg_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _mtg_groupname;
        public String mtg_groupname
        {
            get { return _mtg_groupname; }
            set
            {
                _mtg_groupname = value;
                if (clsCMD.Parameters.Contains("@mtg_groupname"))
                { clsCMD.Parameters["@mtg_groupname"].Value = value; }
                else { clsCMD.Parameters.Add("@mtg_groupname", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                mtg_id = Convert.ToInt64(dr["mtg_id"]);
                mtg_groupname = Convert.ToString(dr["mtg_groupname"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            mtg_id = Convert.ToInt64("0");
            mtg_groupname = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function
        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.mtg_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailtemplategroupAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 mtg_id)
        {
            Boolean blnResult = false;
            this.mtg_id = mtg_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailtemplategroupAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 mtg_id)
        {
            Boolean blnResult = false;
            this.mtg_id = mtg_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailtemplategroupAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_emailtemplategroupGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsEmailTemplateGroup_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsEmailTemplateGroup_PropertiesList> lstResult = new List<clsEmailTemplateGroup_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].mtg_id = Convert.ToInt64(dr["mtg_id"]);
                lstResult[i].mtg_groupname = Convert.ToString(dr["mtg_groupname"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 mtg_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.mtg_id = mtg_id;
                clsCMD.CommandText = "sp_emailtemplategroupGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByNameInProperties(String strName)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.mtg_groupname = strName;
                clsCMD.CommandText = "sp_emailtemplategroupGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(String mtg_groupname)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.mtg_groupname = mtg_groupname;
                clsCMD.CommandText = "sp_emailtemplategroupGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion

    }
    public class clsEmailTemplateGroup_PropertiesList
    {
        public Int64 mtg_id { get; set; }
        public String mtg_groupname { get; set; }
    }
}