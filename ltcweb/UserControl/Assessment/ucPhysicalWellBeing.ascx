﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucPhysicalWellBeing.ascx.cs" Inherits="UserControl_Assessment_ucPhysicalWellBeing" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-github-alt"></i>
                    Physical WellBeing
                </h3>
            </div>
            <div class="box-content nopadding">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlaidsHiv" class="control-label">AIDS/HIV</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlaidsHiv" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlDiabetes" class="control-label">Diabetes</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlDiabetes" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlLiverDisease" class="control-label">Liver Disease</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlLiverDisease" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlRheumaticFever" class="control-label">Rheumatic Fever</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlRheumaticFever" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAlcoholism" class="control-label">Alcoholism</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAlcoholism" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlEmphysema" class="control-label">Emphysema</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlEmphysema" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlMeasles" class="control-label">Measles</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlMeasles" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlScarletFever" class="control-label">Scarlet Fever</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlScarletFever" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAllergyShots" class="control-label">Allergy Shots</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAllergyShots" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlEpilepsy" class="control-label">Epilepsy</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlEpilepsy" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlMigraineHeadaches" class="control-label">Migraine Headaches</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlMigraineHeadaches" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlSTD" class="control-label">STD</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlSTD" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAnemia" class="control-label">Anemia</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAnemia" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlFractures" class="control-label">Fractures</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlFractures" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlMiscarriage" class="control-label">Miscarriage</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlMiscarriage" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlStroke" class="control-label">Stroke</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStroke" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAnorexia" class="control-label">Anorexia</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAnorexia" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlGlaucoma" class="control-label">Glaucoma</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlGlaucoma" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlSuicideAttempt" class="control-label">Suicide Attempt</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlSuicideAttempt" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAppendicitis" class="control-label">Appendicitis</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAppendicitis" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlGoiter" class="control-label">Goiter</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlGoiter" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlMultipleSclerosis" class="control-label">Multiple Sclerosis</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlMultipleSclerosis" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlThyroidProblems" class="control-label">Thyroid Problems</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlThyroidProblems" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlFear" class="control-label">Fear</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlFear" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlArthritis" class="control-label">Arthritis</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlArthritis" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlGonorrhea" class="control-label">Gonorrhea</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlGonorrhea" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlTonsillitis" class="control-label">Tonsillitis</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlTonsillitis" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlAsthma" class="control-label">Asthma</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAsthma" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlGout" class="control-label">Gout</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlGout" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlOsteoporosis" class="control-label">Osteoporosis</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlOsteoporosis" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlTuberculosis" class="control-label">Tuberculosis</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlTuberculosis" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlBleedingDisorders" class="control-label">Bleeding Disorders</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlBleedingDisorders" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlHeartDisease" class="control-label">Heart Disease</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlHeartDisease" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlPacemaker" class="control-label">Pacemaker</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlPacemaker" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlTumorsGrowths" class="control-label">Tumors/Growths</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlTumorsGrowths" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlBreastLump" class="control-label">Breast Lump</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlBreastLump" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlHeadaches" class="control-label">Headaches</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlHeadaches" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlHepatitis" class="control-label">Hepatitis</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlHepatitis" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlParkinsonDisease" class="control-label">Parkinson’s Disease</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlParkinsonDisease" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlTyphoidFever" class="control-label">Typhoid Fever</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlTyphoidFever" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlNervousness" class="control-label">Nervousness</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlNervousness" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlBronchitis" class="control-label">Bronchitis</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlBronchitis" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlHernia" class="control-label">Hernia</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlHernia" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlPinchedNerve" class="control-label">Pinched Nerve</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlPinchedNerve" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlUlcers" class="control-label">Ulcers</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlUlcers" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlBulimia" class="control-label">Bulimia</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlBulimia" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlHerniatedDisk" class="control-label">Herniated Disk</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlHerniatedDisk" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlPneumonia" class="control-label">Pneumonia</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlPneumonia" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlVaginalInfections" class="control-label">Vaginal Infections</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlVaginalInfections" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlCancer" class="control-label">Cancer</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlCancer" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlHerpes" class="control-label">Herpes</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlHerpes" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlSmoking" class="control-label">Smoking</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlSmoking" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlPolio" class="control-label">Polio</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlPolio" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlWhoopingCough" class="control-label">Whooping Cough</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlWhoopingCough" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlCataracts" class="control-label">Cataracts</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlCataracts" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlHighBloodPressures" class="control-label">High Blood Pressures</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlHighBloodPressures" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlProstateProblem" class="control-label">Prostate Problem</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlProstateProblem" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlRheumatoidArthritis" class="control-label">Rheumatoid Arthritis</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlRheumatoidArthritis" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlChemicalDependency" class="control-label">Chemical Dependency</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlChemicalDependency" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlHighCholesterol" class="control-label">High Cholesterol</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlHighCholesterol" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlProsthesis" class="control-label">Prosthesis</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlProsthesis" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlChickenPox" class="control-label">Chicken Pox</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlChickenPox" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlBaddreams" class="control-label">Bad dreams</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlBaddreams" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlKidneyDisease" class="control-label">KidneyDisease</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlKidneyDisease" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlPsychiatricCare" class="control-label">Psychiatric Care</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlPsychiatricCare" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlCholesterol" class="control-label">Cholesterol</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlCholesterol" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Low"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Normal"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="High"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlBloodSugarLevels" class="control-label">Sugar Level</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlBloodSugarLevels" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Low"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Normal"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="High"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlExercise" class="control-label">Exercise</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlExercise" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Moderate"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Daily"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Heavy"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlWorkActivity" class="control-label">Work Activity</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlWorkActivity" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Sitting"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Standing"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Light Labour"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="Heavy Labour"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlSelfesteemconfidence" class="control-label">Self-esteem/confidence</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlSelfesteemconfidence" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlDomesticViolence" class="control-label">Domestic Violence</label>
                                <div class="controls">
                                    <asp:DropDownList ID="DropDownList70" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="ddlBadIllness" class="control-label">Bad Illness</label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlBadIllness" runat="server" Width="190px">
                                        <asp:ListItem Value="1" Text="NA"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Previous"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Current"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Previous/Current"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                    </div>--%>
                    <div class="span12">
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
