﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsProgramToWorkbook
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }
        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }
        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsProgramToWorkbook()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsProgramToWorkbook()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _ptw_id; }
            set
            {
                _ptw_id = value;
                if (clsCMD.Parameters.Contains("@ptw_id"))
                { clsCMD.Parameters["@ptw_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ptw_id", SqlDbType.BigInt).Value = value; }
            }
        }

        private Guid _ByUser = new Guid();
        public Guid ByUser
        {
            get { return _ByUser; }
            set
            {
                _ByUser = value;
                if (clsCMD.Parameters.Contains("@User"))
                { clsCMD.Parameters["@User"].Value = value; }
                else { clsCMD.Parameters.Add("@User", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _ptw_id;
        public Int64 ptw_id
        {
            get { return _ptw_id; }
            set
            {
                _ptw_id = value;
                if (clsCMD.Parameters.Contains("@ptw_id"))
                { clsCMD.Parameters["@ptw_id"].Value = value; }
                else { clsCMD.Parameters.Add("@ptw_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _ptw_programid;
        public Int64 ptw_programid
        {
            get { return _ptw_programid; }
            set
            {
                _ptw_programid = value;
                if (clsCMD.Parameters.Contains("@ptw_programid"))
                { clsCMD.Parameters["@ptw_programid"].Value = value; }
                else { clsCMD.Parameters.Add("@ptw_programid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _ptw_workbookid;
        public Int64 ptw_workbookid
        {
            get { return _ptw_workbookid; }
            set
            {
                _ptw_workbookid = value;
                if (clsCMD.Parameters.Contains("@ptw_workbookid"))
                { clsCMD.Parameters["@ptw_workbookid"].Value = value; }
                else { clsCMD.Parameters.Add("@ptw_workbookid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _ptw_position;
        public Int64 ptw_position
        {
            get { return _ptw_position; }
            set
            {
                _ptw_position = value;
                if (clsCMD.Parameters.Contains("@ptw_position"))
                { clsCMD.Parameters["@ptw_position"].Value = value; }
                else { clsCMD.Parameters.Add("@ptw_position", SqlDbType.BigInt).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                ptw_id = Convert.ToInt64(dr["ptw_id"]);
                ptw_programid = Convert.ToInt64(dr["ptw_programid"]);
                ptw_workbookid = Convert.ToInt64(dr["ptw_workbookid"]);
                ptw_position = Convert.ToInt64(dr["ptw_position"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            ptw_id = Convert.ToInt64("0");
            ptw_programid = Convert.ToInt64("0");
            ptw_workbookid = Convert.ToInt64("0");
            ptw_position = Convert.ToInt64("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ptw_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "programtoworkbookAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 ptw_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ptw_id = ptw_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "programtoworkbookAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 ptw_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ptw_id = ptw_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "programtoworkbookAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByProgramID(Int64 intPrgID)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.ptw_programid = intPrgID; ;
            this.AddEditDeleteFlag = "DELETEBYPRGID";
            try
            {
                clsCMD.CommandText = "programtoworkbookAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ByUser = new Guid();
                clsCMD.CommandText = "programtoworkbookGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetProgrammWorkbook(Int64 prgid)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ptw_programid = prgid;
                this.ByUser = new Guid();
                clsCMD.CommandText = "programtoworkbookGetData";
                SetGetSPFlag = "GETPRGWRKBK";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        public DataTable GetAllByProgramID(Int64 intPRGID, Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ptw_programid = intPRGID;
                clsCMD.CommandText = "programtoworkbookGetData";
                this.ByUser = MemberId;
                SetGetSPFlag = "BYPRGID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        public DataTable GetAllByProgramIDNotMapped(Int64 intPRGID,Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.ptw_programid = intPRGID;
                clsCMD.CommandText = "programtoworkbookGetData";
                this.ByUser = MemberId;
                SetGetSPFlag = "BYPRGIDNOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        public List<clsProgramToWorkbook_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsProgramToWorkbook_PropertiesList> lstResult = new List<clsProgramToWorkbook_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].ptw_id = Convert.ToInt64(dr["ptw_id"]);
                lstResult[i].ptw_programid = Convert.ToInt64(dr["ptw_programid"]);
                lstResult[i].ptw_workbookid = Convert.ToInt64(dr["ptw_workbookid"]);
                lstResult[i].ptw_position = Convert.ToInt64(dr["ptw_position"]);

                i++;
            }
            return lstResult;
        }
        public Boolean GetRecordByIDInProperties(Int64 ptw_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.ptw_id = ptw_id;
                clsCMD.CommandText = "programtoworkbookGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }
        public Boolean CheckDuplicateRow(Int64 ptw_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.ByUser = new Guid();
                this.ptw_id = ptw_id;
                clsCMD.CommandText = "programtoworkbookGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }
        public List<Int64> GetWrkBookByPrgID(Int64 intpegID, Guid MemberId)
        {

            OpenConnection();
            List<Int64> chapters = new List<Int64>();
            try
            {
                this.ptw_programid = intpegID;
                this.ByUser = MemberId;
                clsCMD.CommandText = "programtoworkbookGetData";
                SetGetSPFlag = "BYPRGID";
                SqlDataReader SDR = clsCMD.ExecuteReader();
                while (SDR.Read())
                {
                    Int64 chp = Convert.ToInt64(SDR["wrb_id"]);
                    chapters.Add(chp);
                }
                SDR.Close();
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                chapters = null;
            }
            CloseConnection();
            return chapters;
        }
        #endregion
    }
    public class clsProgramToWorkbook_PropertiesList
    {
        public Int64 ptw_id { get; set; }
        public Int64 ptw_programid { get; set; }
        public Int64 ptw_workbookid { get; set; }
        public Int64 ptw_position { get; set; }
    }
}