﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Chapters.aspx.cs" Inherits="shpweb_LifeProgram_Chapters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <style type="text/css">
        .alternate
        {
            background-color: #EAEAEA;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h5>You are in, <a href="../Client/Dashboard.aspx">Home</a>  &gt;&gt; 
                        <a href="Workbook.aspx">Workbook</a> &gt;&gt;<a href="Chapters.aspx"> Chapters</a>
            </h5>
            <hr />
            <asp:Repeater ID="rptWorkbook" runat="server">
                <ItemTemplate>
                    <div class="row-fluid">
                        <div class="span12">
                            <h2><i class="icon-book"></i>&nbsp;<%#Eval("wrb_name") %></h2>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <%#Eval("wrb_description") %>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <hr />
            <asp:Repeater ID="rptChapters" runat="server">
                <HeaderTemplate>
                    <table class="table table-hover table-nomargin table-colored-header table-bordered table-condensed">
                        <thead>
                            <th style="width: 5%;">Sr. No.</th>
                            <th style="width: 75%;">Chapter's Name</th>
                            <th style="text-align: center; width: 10%;">Status</th>
                            <th style="text-align: center; width: 10%;">Lessons</th>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>">
                        <td style="text-align: center;"><%# Container.ItemIndex +1 %></td>
                        <td><%#Eval("chp_name") %></td>
                        <td style="text-align: center;">
                            <img src="../img/<%# GetImageName(DataBinder.Eval(Container.DataItem, "chp_status").ToString(),DataBinder.Eval(Container.DataItem, "wtc_position").ToString()) %>" style="height: 30px; width: 30px;" />
                        </td>
                        <td style="text-align: center;">
                            <a href="#" id="<%#Eval("chp_id") %>" name='<%#GetLinkStatus(Eval("chp_status").ToString(), Eval("wtc_position").ToString()) %>'>
                                <img src="../img/<%# GetLinkImageName(DataBinder.Eval(Container.DataItem, "chp_status").ToString(),DataBinder.Eval(Container.DataItem, "wtc_position").ToString()) %>" style="height: 30px; width: 30px;" />
                            </a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <%--<asp:Repeater ID="RT" runat="server">
                <HeaderTemplate>
                    <table style="margin: auto; width: 790px; float: left" border="0">
                        <thead>
                            <tr style="text-align: center; font-weight: bold; background-color: #296a9d; color: #FFFFFF;">
                                <th style="width: 50px; height: 30px;">No.</th>
                                <th style="text-align: left; padding-left: 5px; width: 600px;">Chapter Title</th>
                            </tr>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr style="text-align: center;">
                        <td style="height: 30px" class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>"><%# Container.ItemIndex +1 %></td>
                        <td style="text-align: left; padding-left: 5px;" class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>"><%#Eval("chp_name") %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:Repeater ID="RTLink" runat="server">
                <HeaderTemplate>
                    <table style="margin: auto; width: 350px; float: left" border="0">
                        <thead>
                            <tr style="text-align: center; font-weight: bold; background-color: #296a9d; color: #FFFFFF;">
                                <th style="width: 80px; height: 30px">Status</th>
                                <th style="width: 75px;">Lessons</th>
                            </tr>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr style="text-align: center;">
                        <td class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>">
                            <asp:Image ID="imgsts" runat="server" ImageUrl='<%#Eval("urlsts") %>' Style="height: 30px; width: 30px;" />
                        </td>

                        <td class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate" %>">
                            <a href='<%#Eval("href") %>' id="<%#Eval("ID") %>" name="view">
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%#Eval("urlgo") %>' Style="height: 30px; width: 30px;" />
                            </a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>--%>
        </div>
    </div>

    <input type="hidden" id="hidden" name="hidden" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("[name='view']").click(function () {
                var id = this.id;
                $("#<%= hidden.ClientID %>").val(id);
                $("#form1").submit();
                return false;
            });
        });
    </script>
</asp:Content>
