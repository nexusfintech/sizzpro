﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Payment.aspx.cs" Inherits="Payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="glyphicon-user_add"></i>&nbsp;Payment</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12" align="center">
            <h5>For first time appoinment you have to pay us <span style="color: red">10$</span> for appoinment fee
            </h5>
            <br />
            <p>
               <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DXT36YQ3D8MDW&return=http://lifestyletherapycoach.azurewebsites.net/client/Payment.aspx?f=success&cancel_return=http://lifestyletherapycoach.azurewebsites.net/client/Payment.aspx?f=cancle" target="_self" >
                    <img src="../img/images.jpg" border="0" alt="paypal - the safer, easier way to pay online!" />
                </a>

                 <%--<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DXT36YQ3D8MDW&return=http://localhost:9874/ltcweb/client/Payment.aspx?f=success&cancel_return=http://localhost:9874/ltcweb/client/Payment.aspx?f=cancle" target="_self" >
                    <img src="../img/images.jpg" border="0" alt="paypal - the safer, easier way to pay online!" />
                </a>--%>
            </p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
