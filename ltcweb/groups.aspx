﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="groups.aspx.cs" Inherits="groups" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="glyphicon-group"></i>THERAPY GROUPS</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <h4>Life Recovery Program</h4>
            <p> Our Life Recovery Program provides a wonderful opportunity for individuals whose childhood development was interrupted &nbsp;by trauma, neglect, abuse, abandonment, or emotional repression. If  you were shuffled around as a child, grew up with an alcoholic parent or simply was not taken care of emotionally, you may be suffering with depression, anxiety, relationship difficulties, or addictions as an adult. Individuals who have suffered in these ways tend to feel extremely insecure and are unable to form stable and satisfying relationships in adulthood. If you answer yes to most of the questions to these few questions you probably would be classified as an adult child from a dysfunctional home. This of course is not your fault, but it is something only you can do something about.</p>
            <p><strong>AM I AN ADULT CHILD?</strong></p>
            <p><a href="http://sizzlinghotmarriage.com/blog/?page_id=117" target="_blank">Take the test now to find out!</a></p>
            <p>If you answered &quot;yes&quot; to one ore more  of the questions above, we encourage you to consider signing up for our program.</p>
            <p>Our Life Recovery Program is a great opportunity for healing and personal growth.&nbsp;&nbsp;An opportunity to recover the fulfilling life and satisfying relationships you've always wanted. Life Recovery Group is a support group on steroids. The steroids being therapy. You'll laugh, you'll cry, you'll learn, you'll grow. This program includes individual, group, and family therapy. Seminars and retreats are held periodically.</p>
            <p>The Life Recovery Program centers around The Twelve Steps. In addition to attending weekly groups you will work on the steps in your online workbook. The curriculum is Bible based but not preachy. It is introspective causing you to look deep within yourself to make the change you desire. We encourage our participants to be committed to this process for at least a year. You didn't get where you are overnight and you are not going to just change all the sudden. You will start feeling better and doing better - but we warn our participants that doesn't mean you need to quit working your program.</p>
            <p>For more information read our brochure <a href="/LifeRecoveryProgram/Life_Recovery_Program_Brochure.pdf" target="_blank">Life Recovery Program Brochure</a> or give us a call.</p>
            <p>Learn more at <strong><a href="lifeprogram/">RecoveringMyLife.com</a></strong></p>
            <p><strong>Current Groups</strong></p>
            <p>Codependent Women's Teleconference - Tuesdays from 6-7 PM CST </p>
            <p><strong><a href="sexualabusetherapy.aspx">UPCOMING SEMINARS</a></strong></p>
            <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

