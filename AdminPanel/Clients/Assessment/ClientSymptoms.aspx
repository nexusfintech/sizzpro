﻿<%@ Page Title="Symptoms" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClientSymptoms.aspx.cs" Inherits="Clients_Assessment_ClientSymptoms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
     <UC:AssessmentStepLinks ID="ucasslinks" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-github-alt"></i>
                Client's Symptoms Information
            </h3>
        </div>
        <div class="box-content nopadding">
            <%--<UC:ClientSymptoms ID="ucSymptoms" runat="server" />--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>

