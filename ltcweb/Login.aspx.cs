﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Login : System.Web.UI.Page
{
    clsProfileMaster clsProfile = new clsProfileMaster();
    clsUsers clsUSR = new clsUsers();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void cmdLogin_Click(object sender, EventArgs e)
    {
        if (txtUsername.Text.Trim() == "" || txtPassword.Text.Trim() == "")
        {
            lblMessage_page.Text = "Please enter username or password";
            return;
        }

        string[] usersInRole = Roles.GetRolesForUser(txtUsername.Text.Trim());
        if (usersInRole.Length > 0)
        {
            if (usersInRole[0] == "Administrator" || usersInRole[0] == "Member" || usersInRole[0] == "User")
            {
                lblMessage_page.Text = "Only client can login from here..";
                return;
            }
            if (Membership.ValidateUser(txtUsername.Text.Trim(), txtPassword.Text))
            {
                MembershipUser mu = Membership.GetUser(txtUsername.Text.Trim());
                if (mu != null)
                {
                    Guid userguid = (Guid)mu.ProviderUserKey;
                    Session["form_memberkey"] = userguid.ToString();
                    //MySession.Current.UserGuID = userguid;
                    //MySession.Current.UserLoginName = txtUsername.Text.Trim();
                    //string[] roleNames = Roles.GetRolesForUser(txtUsername.Text.Trim());
                    //MySession.Current.UserRole = roleNames[0];
                    clsUSR.GetRecordByIDInProperties(userguid);
                    //MySession.Current.UserLoginID = clsUSR.User_Id;
                    Session["form_userid"] = clsUSR.User_Id;
                    Boolean blnProfile = clsProfile.GetRecordByMemberKeyInProperties(userguid.ToString());
                    if (blnProfile == true)
                    {
                        Session["userloginname"] = clsProfile.prm_firstname + " " + clsProfile.prm_lastname;
                        if (clsProfile.prm_emailverify == false)
                        {
                            Session["msg_header"] = "Email Account Verification Not Complete";
                            Session["msg_boxheader"] = "Hello, " + txtUsername.Text;
                            Session["msg_message"] = "Your email account not verify please check mail inbox you will get verification mail in that have verification link. you have to verify by that link";
                            FormsAuthentication.SignOut(); Response.Redirect("~/Messages.aspx");
                        }
                    }
                    else
                    {
                        Session["msg_header"] = "Email Account Verification Not Complete or Profile Currpt";
                        Session["msg_boxheader"] = "Hello, " + txtUsername.Text;
                        Session["msg_message"] = "Your email account not verify please check mail inbox you will get verification mail in that have verification link. you have to verify by that link";
                        Response.Redirect("~/Messages.aspx");
                    }
                }
                else
                {
                    Session["msg_header"] = "Membership Authication Failed";
                    Session["msg_boxheader"] = "Hello, " + txtUsername.Text;
                    Session["msg_message"] = "Membership verification authetication process is not completed.";
                    Response.Redirect("~/Messages.aspx");
                }

                // Success, create non-persistent authentication cookie.
                FormsAuthentication.SetAuthCookie(
                        this.txtUsername.Text.Trim(), false);
                FormsAuthenticationTicket ticket1 =
                   new FormsAuthenticationTicket(
                        1,                                   // version
                        this.txtUsername.Text.Trim(),   // get username  from the form
                        DateTime.Now,                        // issue time is now
                        DateTime.Now.AddMinutes(10),         // expires in 10 minutes
                        false,      // cookie is not persistent
                        usersInRole[0] // role assignment is stored
                    // in userData
                        );
                HttpCookie cookie1 = new HttpCookie(
                  FormsAuthentication.FormsCookieName,
                  FormsAuthentication.Encrypt(ticket1));
                Response.Cookies.Add(cookie1);

                // 4. Do the redirect. 
                String returnUrl1;
                // the login is successful
                if (Request.QueryString["ReturnUrl"] == null)
                {
                    Response.Redirect("~/Client/Dashboard.aspx");
                }
                //login not unsuccessful 
                else
                {
                    returnUrl1 = Request.QueryString["ReturnUrl"];
                    Response.Redirect(returnUrl1);
                }
            }
            else
            {
                lblMessage_page.Visible = true;
                lblMessage_page.Text = "Invalid Username or Password";
                MembershipUser mu = Membership.GetUser(txtUsername.Text.Trim());
                if (mu != null)
                {
                    if (mu.IsLockedOut)
                    {
                        lblMessage_page.Text = "User is locked. Please contact to administrator";
                    }
                }
            }
        }
        else
        {
            lblMessage_page.Visible = true;
            lblMessage_page.Text = "Invalid Username or Password";
        }
    }
}