﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsWorkbookAnswer
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsWorkbookAnswer()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsWorkbookAnswer()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _wns_id; }
            set
            {
                _wns_id = value;
                if (clsCMD.Parameters.Contains("@wns_id"))
                { clsCMD.Parameters["@wns_id"].Value = value; }
                else { clsCMD.Parameters.Add("@wns_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _wns_id;
        public Int64 wns_id
        {
            get { return _wns_id; }
            set
            {
                _wns_id = value;
                if (clsCMD.Parameters.Contains("@wns_id"))
                { clsCMD.Parameters["@wns_id"].Value = value; }
                else { clsCMD.Parameters.Add("@wns_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _wns_assignmentid;
        public Int64 wns_assignmentid
        {
            get { return _wns_assignmentid; }
            set
            {
                _wns_assignmentid = value;
                if (clsCMD.Parameters.Contains("@wns_assignmentid"))
                { clsCMD.Parameters["@wns_assignmentid"].Value = value; }
                else { clsCMD.Parameters.Add("@wns_assignmentid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _wns_userid;
        public Guid wns_userid
        {
            get { return _wns_userid; }
            set
            {
                _wns_userid = value;
                if (clsCMD.Parameters.Contains("@wns_userid"))
                { clsCMD.Parameters["@wns_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@wns_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _wns_answer;
        public String wns_answer
        {
            get { return _wns_answer; }
            set
            {
                _wns_answer = value;
                if (clsCMD.Parameters.Contains("@wns_answer"))
                { clsCMD.Parameters["@wns_answer"].Size = value.Length; clsCMD.Parameters["@wns_answer"].Value = value; }
                else { clsCMD.Parameters.Add("@wns_answer", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private DateTime _wns_answerdatetime;
        public DateTime wns_answerdatetime
        {
            get { return _wns_answerdatetime; }
            set
            {
                _wns_answerdatetime = value;
                if (clsCMD.Parameters.Contains("@wns_answerdatetime"))
                { clsCMD.Parameters["@wns_answerdatetime"].Value = value; }
                else { clsCMD.Parameters.Add("@wns_answerdatetime", SqlDbType.DateTime).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                wns_id = Convert.ToInt64(dr["wns_id"]);
                wns_assignmentid = Convert.ToInt64(dr["wns_assignmentid"]);
                wns_userid = new Guid(dr["wns_userid"].ToString());
                wns_answer = Convert.ToString(dr["wns_answer"]);
                wns_answerdatetime = Convert.ToDateTime(dr["wns_answerdatetime"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            wns_id = Convert.ToInt64("0");
            wns_assignmentid = Convert.ToInt64("0");
            wns_userid = new Guid();
            wns_answer = Convert.ToString("NA");
            wns_answerdatetime = Convert.ToDateTime(DateTime.Now);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wns_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "workbookanswerAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 wns_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wns_id = wns_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "workbookanswerAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 wns_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.wns_id = wns_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "workbookanswerAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            OpenConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "workbookanswerGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsWorkbookAnswer_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsWorkbookAnswer_PropertiesList> lstResult = new List<clsWorkbookAnswer_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].wns_id = Convert.ToInt64(dr["wns_id"]);
                lstResult[i].wns_assignmentid = Convert.ToInt64(dr["wns_assignmentid"]);
                lstResult[i].wns_userid = new Guid(dr["wns_userid"].ToString());
                lstResult[i].wns_answer = Convert.ToString(dr["wns_answer"]);
                lstResult[i].wns_answerdatetime = Convert.ToDateTime(dr["wns_answerdatetime"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 wns_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.wns_id = wns_id;
                clsCMD.CommandText = "workbookanswerGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean ReadAnswer(Int64 asnid,Guid Userid)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.wns_assignmentid = asnid;
                this.wns_userid = Userid;
                clsCMD.CommandText = "workbookanswerGetData";
                SetGetSPFlag = "READANS";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 wns_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.wns_id = wns_id;
                clsCMD.CommandText = "workbookanswerGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        #endregion
    }

    public class clsWorkbookAnswer_PropertiesList
    {
        public Int64 wns_id { get; set; }
        public Int64 wns_assignmentid { get; set; }
        public Guid wns_userid { get; set; }
        public String wns_answer { get; set; }
        public DateTime wns_answerdatetime { get; set; }
    }
}