﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Users_UserManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (SelectedClient.Value != "")
            {
                Session["selid"] = SelectedClient.Value;
                Response.Redirect("~/Users/UserProfileManagement.aspx?memberkey=" + Session["selid"].ToString());
            }
            ucUserList.IsAct = true;
            loaddata();
        }
    }

    private void loaddata()
    {
        ucUserList.Rolename = "User";
        ucUserList.RefreshGridData();
        lnkCreateNew.NavigateUrl = "~/Users/UserRegistration.aspx"; 
    }
    protected void cmdShowAll_Click(object sender, EventArgs e)
    {
        ucUserList.IsAct = true;
        loaddata();
    }
    protected void cmdInactive_Click(object sender, EventArgs e)
    {
        ucUserList.IsAct = false;
        loaddata();
    }
}