﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Workbook_ProgramMaster : System.Web.UI.Page
{
    clsPrograms clsPRG = new clsPrograms();
    clsobjectsharing clsOBS = new clsobjectsharing();

    private void BindGrid()
    {
        if (User.IsInRole("administrator"))
        {
            dtProgram.DataSource = clsPRG.GetAllRecord();
        }
        else
        {
            dtProgram.DataSource = clsPRG.GetAllRecordByUser((Guid)Membership.GetUser().ProviderUserKey);
        }
        dtProgram.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGrid();
            if (MySession.Current.MESSAGE != string.Empty)
            {
                lblmessage.Text = MySession.Current.MESSAGE;
                msgdiv.Visible = true;
                MySession.Current.MESSAGE = string.Empty;
            }
        }
    }
    protected void dtProgram_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = e.CommandName;
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Workbook/ProgramAddEdit.aspx");
        }
        if (e.CommandName == "SHARE")
        {
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Workbook/ProgrammSharing.aspx");
        }
        if (e.CommandName == "FREEZ")
        {
            Boolean BlnGetrslt = clsPRG.GetRecordByIDInProperties(Convert.ToInt64(e.CommandArgument));
            Int64 Version = clsPRG.GetProgrammVersion(clsPRG.prg_code);
            Boolean BlnRslt = clsPRG.ChangeStatus(Convert.ToInt64(e.CommandArgument), true, Version + 1);
            if (BlnRslt)
            {
                Response.Redirect("~/Workbook/ProgramMaster.aspx");
            }
            else
            {
                lblmessage.Text = "Error Occured During Freez Your Workbook" + clsPRG.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
        if (e.CommandName == "NEWVER")
        {
            Boolean BlnRslt = clsPRG.GetRecordByIDInProperties(Convert.ToInt64(e.CommandArgument));
            if (BlnRslt)
            {
                clsPRG.prg_sts = false;
                Boolean blnSvRsl = clsPRG.Save();
                if (blnSvRsl == true)
                {
                    clsPRG.GetRecordByCodeInProperties(clsPRG.prg_code);
                    clsOBS.shr_sharedobjid = clsPRG.prg_id;
                    clsOBS.shr_object = "PROGRAMM";
                    clsOBS.shr_ownerid = clsPRG.prg_UserId;
                    clsOBS.shr_sharedwithid = clsPRG.prg_UserId;
                    blnSvRsl = clsOBS.Save();
                    Response.Redirect("~/Workbook/ProgramMaster.aspx");
                }
                else
                {
                    lblmessage.Text = "Error Occured During Creatig New Copy" + clsPRG.GetSetErrorMessage;
                    msgdiv.Visible = true;
                }
            }
            else
            {
                lblmessage.Text = "Error Occured During Featchng Your Workbook" + clsPRG.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }

    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Workbook/ProgramAddEdit.aspx");
    }

    protected void cmdShowAll_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void cmdMyRec_Click(object sender, EventArgs e)
    {

    }
    protected void dtProgram_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;
            Guid UserId = new Guid(DataBinder.Eval(DI, "prg_UserId").ToString());
            if (UserId == (Guid)Membership.GetUser().ProviderUserKey || User.IsInRole("administrator"))
            {
                Boolean Isact = Convert.ToBoolean(DataBinder.Eval(DI, "prg_sts"));
                if (Isact)
                {
                    LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdFreez");
                    lnkf.Enabled = false;
                    lnkf.CssClass = "btn btn-success";
                }
                else
                {
                    LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdFreez");
                    lnkf.Enabled = true;
                }
                var dv = e.Item.FindControl("action");
                dv.Visible = true;
            }
            else
            {
                var dv = e.Item.FindControl("share");
                dv.Visible = true;
                var c = e.Item.FindControl("dvf");
                c.Visible = true;
            }
        }
    }
    protected void cmdPrgClnt_Click(object sender, EventArgs e)
    {
        MySession.Current.ObjOwnerID = (Guid)Membership.GetUser().ProviderUserKey;
        Response.Redirect("~/Workbook/ProgrammClient.aspx");
    }
}