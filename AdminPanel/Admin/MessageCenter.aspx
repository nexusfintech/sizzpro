﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="MessageCenter.aspx.cs" Inherits="Users_MessageCenter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-header">
        <div class="pull-left">
            <h3><i class="icon-envelope-alt"></i>&nbsp;Messages &amp; Chat</h3>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span4">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3><i class="icon-envelope-alt"></i>Inbox</h3>
                    <div class="actions">
                        <%--<a href="../Users/MessageCenter.aspx">Go to Message Center</a>--%>
                        <a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
                    </div>
                </div>
                <div class="nopadding box-content">
                    <div class=" scrollable" data-height="530" data-visible="true" data-start="bottom">
                        <asp:Repeater ID="rptMesg" runat="server">
                            <HeaderTemplate>
                                <ul class="messages">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li class="left">
                                    <div class="image">
                                        <img src="../img/demo/user1.jpg" alt="">
                                    </div>
                                    <div class="message">
                                        <span class="caret"></span>
                                        <span class="name"><%# Eval("Sender") %></span>
                                        <p><%# Eval("Mtext") %></p>
                                        <span class="time"><%# Eval("MsgTime") %></span>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>

                    <%--<div class="modal-footer">
                        <a href="MessageCenter.aspx" class="btn-primary btn">Go to Message Center &nbsp;<i class="icon-circle-arrow-right"></i></a>
                    </div>--%>
                </div>
            </div>
        </div>

        <div class="span4">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3><i class="icon-book"></i>Contacts</h3>
                    <div class="actions">
                        <%--<a href="../Users/MessageCenter.aspx">Go to Message Center</a>
                        <a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
                        <a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>--%>
                    </div>
                </div>
                <div class="box-content nopadding scrollable" data-height="530" data-visible="true" data-start="bottom">
                    <asp:Repeater ID="rptContacts" runat="server">
                        <HeaderTemplate>
                            <ul class="tasklist">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
                                <div class="check">
                                    <%--<asp:CheckBox ID="CheckBox1" runat="server" class='icheck-me' data-skin="square" data-color="green" value='<%#Eval("prm_userId") %>' />--%>
                                    <input type="checkbox" runat="server" id="chkDisplayTitle" class='icheck-me' data-skin="square" data-color="blue" value='<%#Eval("prm_userId") %>'>
                                </div>
                                <span class="task"><%# Eval("ContactName") %> &nbsp;&nbsp;(<%# Eval("URole") %>)
                                </span>
                            </li>
                        </ItemTemplate>

                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>

        <div class="span4">
            <div class="box box-color box-bordered lime">
                <div class="box-title">
                    <h3><i class="icon-envelope-alt"></i>Sent box</h3>
                    <div class="actions">
                    </div>
                </div>
                <div class="box-content nopadding scrollable" data-visible="true" data-height="300" data-start="bottom">
                    <asp:Repeater ID="rptSentMesg" runat="server">
                        <HeaderTemplate>
                            <ul class="messages">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li class="right">

                                <div class="image">
                                    <img src="../img/demo/user2.jpg" alt="">
                                </div>
                                <div class="message">
                                    <span class="caret"></span>
                                    <p><%# Eval("Mtext") %></p>
                                    <span class="time"><%# Eval("Mdatetime") %></span>
                                </div>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>

        <div class="span4">
            <div class="box box-color box-bordered orange">
                <div class="box-title">
                    <h3><i class="icon-envelope"></i>New Message</h3>
                    <div class="actions">
                    </div>
                </div>
                <div class="box-content nopadding scrollable" data-visible="true" data-start="bottom">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <div style="padding: 10px 10px 0px 10px">
                                <asp:TextBox ID="txtMsg" runat="server" CssClass="input-block-level" placeholder="Write message here..." TextMode="MultiLine" Width="827px"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <asp:Button ID="cmdSend" runat="server" CssClass="btn btn-primary" Text="Send" OnClick="cmdSend_Click" />
                        <asp:Label ID="lblMsg" runat="server" Visible="false" Font-Bold="true"></asp:Label>
                    </div>
                </div>
            </div>
        </div>


    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

