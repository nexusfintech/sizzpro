﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsAssignments
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsAssignments()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsAssignments()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _asn_id; }
            set
            {
                _asn_id = value;
                if (clsCMD.Parameters.Contains("@asn_id"))
                { clsCMD.Parameters["@asn_id"].Value = value; }
                else { clsCMD.Parameters.Add("@asn_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _asn_id;
        public Int64 asn_id
        {
            get { return _asn_id; }
            set
            {
                _asn_id = value;
                if (clsCMD.Parameters.Contains("@asn_id"))
                { clsCMD.Parameters["@asn_id"].Value = value; }
                else { clsCMD.Parameters.Add("@asn_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _asn_code;
        public Guid asn_code
        {
            get { return _asn_code; }
            set
            {
                _asn_code = value;
                if (clsCMD.Parameters.Contains("@asn_code"))
                { clsCMD.Parameters["@asn_code"].Value = value; }
                else { clsCMD.Parameters.Add("@asn_code", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _asn_name;
        public String asn_name
        {
            get { return _asn_name; }
            set
            {
                _asn_name = value;
                if (clsCMD.Parameters.Contains("@asn_name"))
                { clsCMD.Parameters["@asn_name"].Value = value; }
                else { clsCMD.Parameters.Add("@asn_name", SqlDbType.NVarChar, 1024).Value = value; }
            }
        }
        private String _asn_short;
        public String asn_short
        {
            get { return _asn_short; }
            set
            {
                _asn_short = value;
                if (clsCMD.Parameters.Contains("@asn_short"))
                { clsCMD.Parameters["@asn_short"].Value = value; }
                else { clsCMD.Parameters.Add("@asn_short", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _asn_description;
        public String asn_description
        {
            get { return _asn_description; }
            set
            {
                _asn_description = value;
                if (clsCMD.Parameters.Contains("@asn_description"))
                { 
                    clsCMD.Parameters["@asn_description"].Size = value.Length; 
                    clsCMD.Parameters["@asn_description"].Value = value; 
                }
                else 
                { 
                    clsCMD.Parameters.Add("@asn_description", SqlDbType.NVarChar, value.Length).Value = value; 
                }
            }
        }
        private Guid _asn_UserId;
        public Guid asn_UserId
        {
            get { return _asn_UserId; }
            set
            {
                _asn_UserId = value;
                if (clsCMD.Parameters.Contains("@asn_UserId"))
                { clsCMD.Parameters["@asn_UserId"].Value = value; }
                else { clsCMD.Parameters.Add("@asn_UserId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _asn_Owner;
        public Guid asn_Owner
        {
            get { return _asn_Owner; }
            set
            {
                _asn_UserId = value;
                if (clsCMD.Parameters.Contains("@asn_Owner"))
                { clsCMD.Parameters["@asn_Owner"].Value = value; }
                else { clsCMD.Parameters.Add("@asn_Owner", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        private Boolean _asn_sts;
        public Boolean asn_sts
        {
            get { return _asn_sts; }
            set
            {
                _asn_sts = value;
                if (clsCMD.Parameters.Contains("@asn_sts"))
                { clsCMD.Parameters["@asn_sts"].Value = value; }
                else { clsCMD.Parameters.Add("@asn_sts", SqlDbType.Bit).Value = value; }
            }
        }
        private Int64 _asn_version;
        public Int64 asn_version
        {
            get { return _asn_version; }
            set
            {
                _asn_version = value;
                if (clsCMD.Parameters.Contains("@asn_version"))
                { clsCMD.Parameters["@asn_version"].Value = value; }
                else { clsCMD.Parameters.Add("@asn_version", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                asn_id = Convert.ToInt64(dr["asn_id"]);
                asn_code = new Guid(dr["asn_code"].ToString());
                asn_name = Convert.ToString(dr["asn_name"]);
                asn_short = Convert.ToString(dr["asn_short"]);
                asn_description = Convert.ToString(dr["asn_description"]);
                asn_UserId = new Guid(dr["asn_UserId"].ToString());
                asn_Owner = new Guid(dr["asn_Owner"].ToString());
                asn_sts = Convert.ToBoolean(dr["asn_sts"]);
                asn_version = Convert.ToInt64(dr["asn_version"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            asn_id = Convert.ToInt64("0");
            asn_code = new Guid();
            asn_name = Convert.ToString("NA");
            asn_short = Convert.ToString("NA");
            asn_description = Convert.ToString("NA");
            asn_UserId = new Guid();
            asn_Owner = new Guid();
            asn_sts = false;
            asn_version = 0;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.asn_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "assignmentsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 asn_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.asn_id = asn_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "assignmentsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 asn_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.asn_id = asn_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "assignmentsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean ChangeStatus(Int64 asmid, Boolean status,Int64 ver)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.asn_id = asmid;
            this.asn_sts = status;
            this.asn_version = ver;
            this.AddEditDeleteFlag = "CHNGESTS";
            try
            {
                clsCMD.CommandText = "assignmentsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "assignmentsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByMember(Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "assignmentsGetData";
                this.asn_UserId = MemberId;
                SetGetSPFlag = "ALLBYMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByUser(Guid UserId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "assignmentsGetData";
                this.asn_UserId = UserId;
                SetGetSPFlag = "ALLBYUSER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        public DataTable GetAllNotMapped()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "assignmentsGetData";
                SetGetSPFlag = "NOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        public DataTable GetAllNotMappedByMember(Guid MemberId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "assignmentsGetData";
                this.asn_UserId = MemberId;
                SetGetSPFlag = "NOTMAPPEDBYMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        public List<clsAssignments_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsAssignments_PropertiesList> lstResult = new List<clsAssignments_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].asn_id = Convert.ToInt64(dr["asn_id"]);
                lstResult[i].asn_name = Convert.ToString(dr["asn_name"]);
                lstResult[i].asn_short = Convert.ToString(dr["asn_short"]);
                lstResult[i].asn_description = Convert.ToString(dr["asn_description"]);
                lstResult[i].asn_code = new Guid(dr["asn_code"].ToString());
                lstResult[i].asn_Owner = new Guid(dr["asn_Owner"].ToString());
                lstResult[i].asn_UserId = new Guid(dr["asn_UserId"].ToString());
                lstResult[i].asn_sts = Convert.ToBoolean(dr["asn_sts"]);
                lstResult[i].asn_version = Convert.ToInt64(dr["asn_version"]);
                i++;
            }
            return lstResult;
        }
        public Boolean GetRecordByIDInProperties(Int64 asn_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.asn_id = asn_id;
                clsCMD.CommandText = "assignmentsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }
        public Boolean CheckDuplicateRow(Guid asn_code)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.asn_code = asn_code;
                clsCMD.CommandText = "assignmentsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }
        public Boolean GetRecordByCodeInProperties(Guid Code)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.asn_code = Code;
                clsCMD.CommandText = "assignmentsGetData";
                SetGetSPFlag = "BYCODE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }
        public Int64 GetAssignmentVersion(Guid Code)
        {
            OpenConnection();
            Int64 Version = 0;
            try
            {
                this.asn_code = Code;
                clsCMD.CommandText = "assignmentsGetData";
                SetGetSPFlag = "FINDVER";
                Version = Convert.ToInt64(clsCMD.ExecuteScalar());
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                Version = -1;
            }
            CloseConnection();
            return Version;
        }
        #endregion
    }

    public class clsAssignments_PropertiesList
    {
        public Int64 asn_id { get; set; }
        public Guid asn_code { get; set; }
        public String asn_name { get; set; }
        public String asn_short { get; set; }
        public String asn_description { get; set; }
        public Guid asn_UserId { get; set; }
        public Guid asn_Owner { get; set; }
        public Boolean asn_sts { get; set; }
        public Int64 asn_version { get; set; }
    }
}