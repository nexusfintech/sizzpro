﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;

public partial class Master_FacilityAddEdit : System.Web.UI.Page
{
    clsFacilities clsFCL = new clsFacilities();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            //bindadmin();
            if(MySession.Current.AddEditFlag=="EDIT")
            {
                loaddata();
            }
        }
    }

    //private void bindadmin()
    //{
    //    ddlSelectMember.Items.Clear();
    //    ddlSelectMember.SelectedIndex = -1;
    //    ddlSelectMember.SelectedValue = null;
    //    ddlSelectMember.ClearSelection();
    //    ddlSelectMember.AppendDataBoundItems = true;
    //    ListItem lst = new ListItem();
    //    lst.Value = "0";
    //    lst.Text = "<<-Please Select->>";
    //    lst.Selected = true;
    //    ddlSelectMember.Items.Add(lst);
    //    ddlSelectMember.DataValueField = "Userid";
    //    ddlSelectMember.DataTextField = "UserName";
    //    DataTable dtResult = clsCPM.GetAllCombo();
    //    ddlSelectMember.DataSource = dtResult;
    //    ddlSelectMember.DataBind();
    //}
    private void loaddata()
    {
        Boolean blnRslt = clsFCL.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {
            txtFcltyname.Text = clsFCL.fsl_name;
            txtFacltyadd.Text = clsFCL.fsl_address;
            txtFcltycity.Text = clsFCL.fsl_city;
            txtFcltystate.Text = clsFCL.fsl_state;
            txtFcltyzip.Text = clsFCL.fsl_zip;
            txtFcltyphn.Text = clsFCL.fsl_phone;
            txtFcltyfax.Text = clsFCL.fsl_fax;
        }
        else
        {
            MySession.Current.MESSAGE = clsFCL.GetSetErrorMessage;
            Response.Redirect("~/Master/FacilityMaster.aspx");
        }
    }
    private string validatepage()
    {
        string result = string.Empty;
        if(txtFcltyname.Text == string.Empty)
        {
            result = "Facility Name is Required";
            return result;   
        }
        else if(txtFacltyadd.Text == string.Empty)
        {
             result = "Facility Address is Required";
            return result; 
        }
        else if (txtFcltycity.Text == string.Empty)
        {
            result = "Facility City is Required";
            return result;
        }
        else if (txtFcltystate.Text == string.Empty)
        {
            result = "Facility State is Required";
            return result;
        }
        else if (txtFcltyzip.Text == string.Empty)
        {
            result = "Facility Zip is Required";
            return result;
        }
        else if (txtFcltyphn.Text == string.Empty)
        {
            result = "Facility Phone is Required";
            return result;
        }
        else
        {
            return result;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string validationresult=validatepage();
        if(validationresult != string.Empty)
        {
            lblmessage.Text = validationresult;
            msgdiv.Visible = true;
            return;
        }

        clsFCL.fsl_userid = (Guid) Membership.GetUser().ProviderUserKey;
        clsFCL.fsl_name = txtFcltyname.Text;
        clsFCL.fsl_address = txtFacltyadd.Text;
        clsFCL.fsl_city = txtFcltycity.Text;
        clsFCL.fsl_state = txtFcltystate.Text;
        clsFCL.fsl_zip = txtFcltyzip.Text;
        clsFCL.fsl_phone = txtFcltyphn.Text;
        clsFCL.fsl_fax = txtFcltyfax.Text;

        Boolean blnresult;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnresult = clsFCL.Save();
        }
        else
        {
            blnresult = clsFCL.Update(MySession.Current.EditID);
        }
        if (blnresult)
        {
            MySession.Current.MESSAGE = "Data Saved Successfully";
            Response.Redirect("~/Master/FacilityMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsFCL.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/FacilityMaster.aspx");
    }
}