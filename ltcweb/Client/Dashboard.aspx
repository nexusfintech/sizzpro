﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Client_Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h3><i class="glyphicon-dashboard"></i>&nbsp;Dashboard</h3>
        </div>
    </div>
    <asp:Label ID="lblICmsg" runat="server" Visible="false" ForeColor="Red" Font-Bold="true">Your Informed Consent is not completed. To take our services please complete the Informed consent from <a href="informedConsentDetails.aspx">here</a></asp:Label>
    <hr />

    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="glyphicon-book_open"></i>
                        Workbook
                    </h3>
                    <div class="actions">
                        <a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
                    </div>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-column form-bordered'>
                        <asp:Repeater ID="rptWorkbookDispaly" runat="server">
                            <ItemTemplate>
                                <div style="text-align: center; vertical-align: middle; margin: 5px; float: left;">
                                    <img src='<%= Page.ResolveClientUrl("~/img/workbook.png") %>' style="width: 150px; height: 150px;">
                                    <br />
                                    <a href="Workbook.aspx" id='<%#Eval("wrb_id")%>' name="view">
                                        <%#Eval("wrb_name") %>
                                    </a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-calendar"></i>
                        My Appointments
                    </h3>
                    <div class="actions">
                        <a href="PreviousAppointment.aspx">Previous Appointments</a>
                        <a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
                    </div>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-column form-bordered'>
                        <asp:Repeater ID="rptAppontment" runat="server" OnItemCommand="rptAppontment_ItemCommand">
                            <HeaderTemplate>
                                <table class="table table-hover table-nomargin table-bordered">
                                    <thead>
                                        <th>Course</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>From Time</th>
                                        <th>To Time</th>
                                        <th>Therapist</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("crs_coursename") %> </td>
                                    <td><%# Eval("apt_title") %> </td>
                                    <td><%# Eval("apt_description") %> </td>
                                    <td><%# Eval("apt_startdatetime") %> </td>
                                    <td><%# Eval("apt_enddatetime") %> </td>
                                    <td><%# Eval("prm_firstname") %>&nbsp;<%# Eval("prm_lastname") %></td>
                                    <td>
                                        <asp:Button ID="cmdEdit" runat="server" Text="Delete" CssClass="btn cancel btn-primary" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "apt_id") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="glyphicon-file"></i>Documents</h3>
                </div>
                <div class="box-content nopadding">
                    <div style="text-align: center; vertical-align: middle; margin: 5px; float: left;">
                        <asp:LinkButton ID="lnkBtnDonwload" runat="server" Visible="false" OnClick="lnkBtnDonwload_Click">
                            <img src='<%= Page.ResolveClientUrl("~/img/pdf.png") %>' style="width: 100px; height: 100px;">
                            <br />
                            Informed Consent
                        </asp:LinkButton>
                    </div>
                    <asp:Label runat="server" ID="lblIcFile" ForeColor="Red" Font-Bold="true"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <input type="hidden" id="hidden" name="hidden" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("[name='view']").click(function () {
                var id = this.id;
                $("#<%= hidden.ClientID %>").val(id);
                $("#form1").submit();
                return false;
            });
        });
    </script>
</asp:Content>

