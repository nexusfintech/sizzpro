﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="about-joe.aspx.cs" Inherits="about_joe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2>About Joseph L. Follette</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <p>
                <img src="img/therapist1.png" alt="Joseph Follette, Jr." hspace="5" vspace="5" align="left" style="margin-left: 5px; margin-right: 10px;" />
                Joseph L. Follette, Jr. , author of forthcoming book &quot;Return to Love and Get the Sizzle Back in Your Marriage&quot; and owner of Lifestyle Therapy and Coaching located in Huntsville, AL. He is a Licensed Marriage &amp; Family Therapist and Ordained Seventh-day Adventist minister. He is a clinical member of the American Association for Marriage and Family Therapy. He has over 25 years of experience meeting human needs as a church pastor, residence hall director, professor and counselor. He was commissioned as a Chaplain Candidate  in the US Army. As a skilled listener he has helped hundreds clarify their challenges and reach their goals.  Joe is a positive upbeat individual with a warm and friendly personality. He enjoys laughter and loves to see the spark return in his clients. He specializes in helping married couples become a Sizzling Hot Marriage.
            </p>
            <p>Joseph L. Follette, Jr. is a positive upbeat individual with a warm and friendly personality that makes even the most guarded individual in therapy feel comfortable immediately. Joe's has been able to help hundreds of individuals, couples and families work through their challenges and get where they want to be in their life.  Joe has the expertise to help people sort through the issues of life.  He specializes in helping couple's reignite the flame in their marriage. See <a href="http://www.sizzlinghotmarriage.com" target="_blank">Sizzling Hot Marriage</a> for more information. He has helped many recover from the ravages of divorce. Many dating couples have been able to work through their issues to enjoy a great marriage. There are dozens of drug addicts who turned their back on drugs for good. And parents who can now sleep with more confidence in their parenting skills. Joe's gifts have made the way for many to experience a better life.</p>
            <p>The Lifestyle Coaching Network is an opportunity for Joe to extend his healing touch to thousands across the globe. As an ABEMFT Approved Supervisor, Joe coaches new therapist into becoming effective therapists. He is developing a network of therapist who desire to be second to none in helping people change their perspective and improve their lives.</p>
            <p>Joe offers his years of experience working with couples condensed into his Sizzling Hot that make it easy for couples to learn and practice the principles of success for marriage. In this day and age everyone is aware of how divorce has tarnished marriage. Follette has determined to strengthen marriage by sharing these timeless principles he finds himself going over couple after couple. These principles include effective communication, anger management, financial management, and living a healthier lifestyle. Joe is also a gifted speaker offering seminars, sermons and motivational speaking wherever invited. He keeps his audiences on the edge of their seats as he shares insights that really make a difference.</p>
            <p>Joe is currently pursuing a PhD in Marriage and Family Therapy from Amridge University. He received his MFT counseling degree from University of Southern Mississippi and his pastoral degree from Andrews University. He completed his undergraduate studies at Oakwood University. Joe is a committed family man married with 5 children. He enjoys reading, computers and his dogs. He is actively engaged in community service. But most of all Joe is thankful for God's love, grace and leadership in his life. [<a href="/about-joe-vitae.html">Curriculum Vitae</a>]</p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="span3">
                <a href="http://www.theravive.com">
                    <img usemap="#theravive-counseling" style="border: none;" src="http://www.theravive.com/siteimages/v2/theravive-therapy-seal.jpg" alt="Theravive Counselor">
                </a>
                <map name="theravive-counseling">
                    <area title="Theravive Profile: Joseph Follette, M.Div., M.S., LMFT" shape="rect" href="http://www.theravive.com/therapists/joseph-follette.aspx" coords="0,-2,150,45">
                    <area title="Family Therapy" shape="rect" href="http://www.theravive.com" coords="0,47,170,58">
                    <area title="Counseling Huntsville AL" shape="rect" href="http://www.theravive.com/cities/al/counseling-huntsville.aspx" coords="150,0,170,47">
                </map>
            </div>
            <div class="span3">
                <p>
                    <img width="146" height="69" border="0" usemap="#verifiedmap" alt="verified by Psychology Today" src="http://Therapists.PsychologyToday.com/rms/external_verification.php?profid=104421">
                    <map id="verifiedmap" name="verifiedmap">
                        <area onclick="window.open( 'http://Therapists.PsychologyToday.com/rms/verification_personal.php?id=104421', 'window_a', 'width=600,height=400,resizable=yes,scrollbars=no,' + 'toolbar=no,location=no,status=no,menubar=no' );return false;" target="_blank" href="http://Therapists.PsychologyToday.com/rms/verification_personal.php?id=104421" alt="verified by Psychology Today" coords="0,0,146,38" shape="rect">
                        <area target="_blank" href="http://Therapists.PsychologyToday.com/rms/104421" alt="Directory" coords="0,38,146,69" shape="rect">
                    </map>
                </p>
            </div>
            <div class="span3">
                <a href="http://www.aamft.org/cgi-shl/TWServer.exe?Run:LOCATEUS_2:TradeWinds_KEY=4859">
                    <img width="79" height="74" alt="AAMFT" src="images/logos/Other Organizations/AAMFT-Clinical-Member-Logo-trimmed.gif">
                </a>
            </div>
            <div class="span3">
                <a href="http://www.amhc.state.al.us/ViewLicensee.aspx?LicenseNum=214">
                    <img width="192" height="70" alt="ABEMFT" src="images/logos/ABEMFTLogo.gif">
                </a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

