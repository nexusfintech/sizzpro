﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
using System.Data;
public partial class Admin_ClaimManagment_ClaimAddEdit : System.Web.UI.Page
{
    private Guid clientid;
    private Guid sessionid = new Guid();
    clsClaimStatusMaster clsCSM = new clsClaimStatusMaster();
    clsClientFollowUpSession clsCFS = new clsClientFollowUpSession();
    clsClientClaim clsCC = new clsClientClaim();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["sessionuserid"] == null)
        {
            Response.Redirect("~/Users/UserClientManage.aspx");
        }
        else
        {
            clientid = new Guid(Session["sessionuserid"].ToString());
        }

        if (Session["sessionid"] != null)
        {
            sessionid = new Guid(Session["sessionid"].ToString());
        }
        if (!IsPostBack)
        {
            bindcontrols();
            if (sessionid != new Guid())
            {
                ListItem ssn = ddlSession.Items.FindByValue(sessionid.ToString());
                if (ssn != null)
                {
                    ddlSession.SelectedIndex = ddlSession.Items.IndexOf(ssn);
                }
                ListItem clmsts = ddlClmStatus.Items.FindByText("Claim Created");
                if (clmsts != null)
                {
                    ddlClmStatus.SelectedIndex = ddlClmStatus.Items.IndexOf(clmsts);
                }
                DataTable dtssn = clsCFS.GetAllRecordBySession(sessionid);
                if (dtssn != null)
                {
                    Convert.ToDateTime(clsCFS.cfs_intime.ToString()).TimeOfDay.ToString();
                    ddlinscommaster.SelectedName = dtssn.Rows[0]["insc_name"].ToString();
                    txtIntime.Text = Convert.ToDateTime(dtssn.Rows[0]["cfs_intime"].ToString()).TimeOfDay.ToString();
                    txtOutTime.Text = Convert.ToDateTime(dtssn.Rows[0]["cfs_outtime"].ToString()).TimeOfDay.ToString();
                    txtTotalCharge.Text = dtssn.Rows[0]["fsb_totalrecieved"].ToString();
                    clientid = new Guid(dtssn.Rows[0]["cfs_userid"].ToString());
                }
            }
        }
        if(MySession.Current.AddEditFlag == "EDIT")
        {
            EditEntry();
        }
    }

    private void EditEntry()
    {
        //bindcontrols();
        Boolean blnRslt = clsCC.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {
            ListItem ssn = ddlSession.Items.FindByValue(clsCC.cm_sesssionid.ToString());
            if (ssn != null)
            {
                ddlSession.SelectedIndex = ddlSession.Items.IndexOf(ssn);
            }
            ListItem clmsts = ddlClmStatus.Items.FindByValue(clsCC.cm_status.ToString());
            if (clmsts != null)
            {
                ddlClmStatus.SelectedIndex = ddlClmStatus.Items.IndexOf(clmsts);
            }
            txtcreateddate.Text = clsCC.cm_datecreated.ToString("MM/dd/yyyy");
            txtextranote.Text = clsCC.cm_claimnote;
            txtIntime.Text = clsCC.cm_timein.TimeOfDay.ToString();
            txtOutTime.Text = clsCC.cm_timeout.TimeOfDay.ToString();
            txtTotalCharge.Text=clsCC.cm_totalcharge.ToString();

            if(clsCC.cm_isemergency)
            {
                chkemergency.Checked = true;    
            }
            if(clsCC.cm_issecondry)
            {
                chksecondry.Checked = true;
            }
        }
    }
    private void bindcontrols()
    {
        ddlClmStatus.DataSource = clsCSM.GetAllRecord();
        ddlClmStatus.DataTextField = "clmsts_status";
        ddlClmStatus.DataValueField = "clmsts_id";
        ddlClmStatus.DataBind();

        DataTable dtsrc = clsCFS.GetAllRecordByClient(clientid);
        ddlSession.DataSource = dtsrc;
        ddlSession.DataTextField = "cfs_sessionid";
        ddlSession.DataValueField = "cfs_sessionid";
        ddlSession.DataBind();

    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        clsCC.cm_claimnote = txtextranote.Text;
        clsCC.cm_datecreated = Convert.ToDateTime(txtcreateddate.Text);
        clsCC.cm_FormType = ddlFormType.SelectedValue;
        clsCC.cm_insurancebilling = ddlbillingtype.SelectedValue;
        clsCC.cm_insurancecompany = ddlinscommaster.SelectedValue;
        if (chkemergency.Checked)
        {
            clsCC.cm_isemergency = true;
        }
        if (chksecondry.Checked)
        {
            clsCC.cm_issecondry = true;
        }
        clsCC.cm_sesssionid = new Guid(ddlSession.SelectedValue);
        clsCC.cm_status = Convert.ToInt64(ddlClmStatus.SelectedValue);

        var dt = DateTime.Now.Date; // time is zero by default
        string it = txtIntime.Text;
        var tm = TimeSpan.Parse(it);
        DateTime dtInTime = dt + tm;
        clsCFS.cfs_intime = dtInTime;
        string ot = txtOutTime.Text;
        tm = TimeSpan.Parse(ot);
        DateTime dtOutTime = dt + tm;
        clsCFS.cfs_outtime = dtOutTime;

        clsCC.cm_timein = dtInTime;
        clsCC.cm_timeout = dtOutTime;
        clsCC.cm_pataintid = clientid;
        if (txtsubmiteddate.Text != string.Empty)
        {
            clsCC.cm_submiteddate = Convert.ToDateTime(txtsubmiteddate.Text);
        }
        clsCC.cm_totalcharge = Convert.ToDouble(txtTotalCharge.Text);
        Boolean blnSvRslt;
        if (MySession.Current.AddEditFlag == "EDIT")
        {
            blnSvRslt = clsCC.Update(MySession.Current.EditID);
            MySession.Current.MESSAGE = "Claim Updated Successfully";
        }
        else
        {
            blnSvRslt = clsCC.Save();
            MySession.Current.MESSAGE = "Claim Created Successfully";
        }
        if (blnSvRslt)
        {
            Response.Redirect("~/Admin/ClaimManagment/ClaimMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsCC.GetSetErrorMessage;
            msgdiv.Visible = true;
            return;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/ClaimManagment/ClaimMaster.aspx");
    }
    protected void cmdClmMstr_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/ClaimManagment/ClaimMaster.aspx");
    }
    protected void cmdView_Click(object sender, EventArgs e)
    {
        Session["sessionid"] = new Guid(ddlSession.SelectedValue);
        Response.Redirect("~/Admin/SessionManagement/FollowupSesionView.aspx?tag=claim");
    }
}