﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Workbook_LessonMaster : System.Web.UI.Page
{
    clsLessions clsLSN = new clsLessions();
    clsobjectsharing clsOBS = new clsobjectsharing();

    private void BindGrid()
    {
        if (User.IsInRole("administrator"))
        {
            dtLesson.DataSource = clsLSN.GetAllRecord();
        }
        else
        {
            dtLesson.DataSource = clsLSN.GetAllRecordByUser((Guid)Membership.GetUser().ProviderUserKey);
        }
        dtLesson.DataBind();
    }

    private void BindGridNotMapped()
    {
        dtLesson.DataSource = clsLSN.GetAllNotMapped();
        dtLesson.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGrid();
            if (MySession.Current.MESSAGE != string.Empty)
            {
                lblmessage.Text = MySession.Current.MESSAGE;
                msgdiv.Visible = true;
                MySession.Current.MESSAGE = string.Empty;
            }
        }
    }

    protected void dtLesson_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = e.CommandName;
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Workbook/LessonAddEdit.aspx");
        }
        if (e.CommandName == "SHARE")
        {
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Workbook/LessonSharing.aspx");
        }
        if (e.CommandName == "FREEZ")
        {
            Boolean BlnGetrslt = clsLSN.GetRecordByIDInProperties(Convert.ToInt64(e.CommandArgument));
            Int64 Version = clsLSN.GetLessonVersion(clsLSN.lsn_code);
            Boolean BlnRslt = clsLSN.ChangeStatus(Convert.ToInt64(e.CommandArgument), true,Version+1);
            if (BlnRslt)
            {
                Response.Redirect("~/Workbook/LessonMaster.aspx");
            }
            else
            {
                lblmessage.Text = "Error Occured During Freez Your Workbook" + clsLSN.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
        if (e.CommandName == "NEWVER")
        {
            Boolean BlnRslt = clsLSN.GetRecordByIDInProperties(Convert.ToInt64(e.CommandArgument));
            if (BlnRslt)
            {
                clsLSN.lsn_sts = false;
                Boolean blnSvRsl = clsLSN.Save();
                if (blnSvRsl == true)
                {
                    clsLSN.GetRecordByCodeInProperties(clsLSN.lsn_code);
                    clsOBS.shr_sharedobjid = clsLSN.lsn_id;
                    clsOBS.shr_object = "LESSON";
                    clsOBS.shr_ownerid = clsLSN.lsn_UserId;
                    clsOBS.shr_sharedwithid = clsLSN.lsn_UserId;
                    blnSvRsl = clsOBS.Save();
                    Response.Redirect("~/Workbook/LessonMaster.aspx");
                }
                else
                {
                    lblmessage.Text = "Error Occured During Creatig New Copy" + clsLSN.GetSetErrorMessage;
                    msgdiv.Visible = true;
                }
            }
            else
            {
                lblmessage.Text = "Error Occured During Featchng Your Lesson" + clsLSN.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }

    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Workbook/LessonAddEdit.aspx");
    }

    protected void cmdHideMapped_Click(object sender, EventArgs e)
    {
        BindGridNotMapped();
    }
    protected void cmdShowAll_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void cmdMyRec_Click(object sender, EventArgs e)
    {

    }
    protected void dtLesson_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;
            Guid UserId = new Guid(DataBinder.Eval(DI, "lsn_UserId").ToString());
            if (UserId == (Guid)Membership.GetUser().ProviderUserKey || User.IsInRole("administrator"))
            {
                Boolean Isact = Convert.ToBoolean(DataBinder.Eval(DI, "lsn_sts"));
                if (Isact)
                {
                    LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdFreez");
                    lnkf.Enabled = false;
                    lnkf.CssClass = "btn btn-success";

                    //LinkButton lnks = (LinkButton)e.Item.FindControl("cmdShare");
                    //lnks.Enabled = true;
                }
                else
                {
                    LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdFreez");
                    lnkf.Enabled = true;

                    //LinkButton lnks = (LinkButton)e.Item.FindControl("cmdShare");
                    //lnks.Enabled = false;
                    //lnks.CssClass = "btn btn-success";
                }
                var dv = e.Item.FindControl("action");
                dv.Visible = true;
            }
            else
            {
                var dv = e.Item.FindControl("share");
                dv.Visible = true;
                var c = e.Item.FindControl("dvf");
                c.Visible = true;
            }
        }
    }
}