﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class UserControl_ucUserList : System.Web.UI.UserControl
{
    private Boolean _IsAct;
    public Boolean IsAct
    {
        get { return _IsAct; }
        set { _IsAct = value; }
    }

    private string strRoleName;

    public string Rolename
    {
        get { return strRoleName; }
        set { strRoleName = value; }
    }
    clsClientParentMapping clsCPM = new clsClientParentMapping();

    private void BindGrid()
    {
        //string[] usersInRole = Roles.GetUsersInRole(strRoleName);
        MembershipUserCollection UserResult = new MembershipUserCollection();
        Guid usrId = new Guid();
        List<Guid> usersInRole;
        if (Page.User.IsInRole("member"))
        {
            usersInRole = clsCPM.GetAllUserByRoleandParent(strRoleName, (Guid)Membership.GetUser().ProviderUserKey, IsAct);
        }
        else
        {
            usersInRole = clsCPM.GetAllUserByRole(strRoleName, IsAct);
        }

        foreach (var userid in usersInRole)
        {
            MembershipUser usr = Membership.GetUser(userid);
            UserResult.Add(usr);
        }
        dtrUserDisplay.DataSource = UserResult;
        dtrUserDisplay.DataBind();
    }

    public void RefreshGridData()
    {
        BindGrid();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblBoxHeader.Text = Rolename + " List";
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        Button dltButton = (Button)sender;
        bool blnResult1 = false;
        MembershipUser muser = Membership.GetUser(new Guid(dltButton.CommandArgument.ToString()));

        if (Roles.IsUserInRole(muser.UserName, "member"))
        {
            if (dltButton.Text == "DeActive")
            {
                muser.IsApproved = false;
                //List<Guid> stafflist = clsCPM.GetAllUserByRoleandParent("User", (Guid)muser.ProviderUserKey, true);
                //foreach (Guid staff in stafflist)
                //{
                //    MembershipUser stfusr = Membership.GetUser(staff);
                //    stfusr.IsApproved = false;
                //    Membership.UpdateUser(stfusr);
                //    clsCPM.UpdateStatus((Guid)stfusr.ProviderUserKey, false);
                //}
                Membership.UpdateUser(muser);
                blnResult1 = clsCPM.UpdateStatus((Guid)muser.ProviderUserKey, false);
            }
            else if (dltButton.Text == "Active")
            {
                muser.IsApproved = true;
                //List<Guid> stafflist = clsCPM.GetAllUserByRoleandParent("User", (Guid)muser.ProviderUserKey, false);
                //foreach (Guid staff in stafflist)
                //{
                //    MembershipUser stfusr = Membership.GetUser(staff);
                //    stfusr.IsApproved = true;
                //    Membership.UpdateUser(stfusr);
                //    clsCPM.UpdateStatus((Guid)stfusr.ProviderUserKey, true);
                //}
                Membership.UpdateUser(muser);
                blnResult1 = clsCPM.UpdateStatus((Guid)muser.ProviderUserKey, true);
            }
        }
        else
        {
            MembershipUser lginusr = Membership.GetUser();
            if (Roles.IsUserInRole(lginusr.UserName, "administrator"))
            {
                if (dltButton.Text == "DeActive")
                {
                    muser.IsApproved = false;
                    Membership.UpdateUser(muser);
                    blnResult1 = clsCPM.UpdateStatus((Guid)muser.ProviderUserKey, false);
                }
                else if (dltButton.Text == "Active")
                {
                    muser.IsApproved = true;
                    Membership.UpdateUser(muser);
                    blnResult1 = clsCPM.UpdateStatus((Guid)muser.ProviderUserKey, true);
                }
            }
            else if (Roles.IsUserInRole(lginusr.UserName, "member"))
            {
                if (dltButton.Text == "DeActive")
                {
                    //muser.IsApproved = false;
                    //Membership.UpdateUser(muser);
                    blnResult1 = clsCPM.UpdateUserStatus((Guid)muser.ProviderUserKey,(Guid)lginusr.ProviderUserKey, false);
                }
                else if (dltButton.Text == "Active")
                {
                    //muser.IsApproved = true;
                    //Membership.UpdateUser(muser);
                    blnResult1 = clsCPM.UpdateUserStatus((Guid)muser.ProviderUserKey,(Guid)lginusr.ProviderUserKey, true);
                }
            }
        }
        if (blnResult1 == true)
        {
            if (Roles.IsUserInRole(muser.UserName, "User"))
            {
                Response.Redirect("~/Users/UserManagement.aspx");
            }
            else
            {
                Response.Redirect("~/Users/MemberManagement.aspx");
            }
        }
    }

    protected void dtrUserDisplay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Button BtnDlt = (Button)e.Item.FindControl("cmdDelete");
            object DI = e.Item.DataItem;
            Boolean Isact = Convert.ToBoolean(DataBinder.Eval(DI, "IsApproved"));
            if (Isact)
            {
                BtnDlt.Text = "DeActive";
            }
            else
            {
                BtnDlt.Text = "Active";
            }
            string username = DataBinder.Eval(DI, "Username").ToString();
            if(username == Membership.GetUser().UserName)
            {
                BtnDlt.Enabled = false;
            }
        }
    }
}