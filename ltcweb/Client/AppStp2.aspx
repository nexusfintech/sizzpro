﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AppStp2.aspx.cs" Inherits="Client_AppStp2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="glyphicon-file"></i>Documents</h3>
                </div>
                <div class="box-content nopadding">
                    <h5 style="text-align: center">
                        Click <asp:LinkButton runat="server" ID="lnkView"  OnClick="lnkView_Click">HERE</asp:LinkButton> to view your InformConcent.
                        <br />
                        <br />
                        <asp:CheckBox ID="chk" runat="server" />
                        <strong style="color: red">By checking this you are accept that you have read and understood the terms and conditions regarding to this appoinment , which are listed above.
                        </strong>
                        <div class="form-actions">
                            <asp:Button ID="cmdAddPontment" runat="server" CssClass="btn btn-primary" Text="Make Payment & Complate Appointment" OnClick="cmdAddPontment_Click"/>
                        </div>
                    </h5>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

