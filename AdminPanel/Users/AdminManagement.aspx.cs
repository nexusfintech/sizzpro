﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Users_AdminManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ucUserList.IsAct = true;
        loaddata();
    }
    private void loaddata( )
    {
        ucUserList.Rolename = "Administrator";
        ucUserList.RefreshGridData();
        lnkCreateNew.NavigateUrl = "~/Users/AdminRegistration.aspx"; 
    }
    protected void cmdShowAll_Click(object sender, EventArgs e)
    {
        ucUserList.IsAct = true;
        loaddata();
    }
    protected void cmdInactive_Click(object sender, EventArgs e)
    {
        ucUserList.IsAct = false;
        loaddata();
    }
}