﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ChildrenManagement.aspx.cs" Inherits="Users_ChildrenManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <asp:HiddenField ID="AssesmentStep" runat="server" Value="8" />
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-group"></i>
                Client's Children Management
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:ChildrenDetail ID="ucChildren" runat="server" />
        </div>
    </div>
      <div class="row-fluid">
        <hr />
        <div class="span12">
            <asp:Button ID="cmdNext" runat="server" Text="Next Siblings >>" CssClass="btn btn-primary" OnClick="cmdNext_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>

