﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="WorkbookSharing.aspx.cs" Inherits="Workbook_WorkbookSharing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkMappingtoWB" runat="server" NavigateUrl="~/Workbook/WorkbookMaster.aspx"><i class="glyphicon-retweet_2"></i>&nbsp;Work Book</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="glyphicon-retweet"></i>WorkBook Sharing</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <div class="control-group">
                            <label for="txtCode" class="control-label">Select Admin</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddladmins" runat="server" CssClass="select2-me input-large"></asp:DropDownList>
                                <span class="help-block">Select Admin to Share Workbook With </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Paid or Free</label>
                            <div class="controls">
                                <div class="check-demo-col">
                                    <div class="check-line">
                                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="select2-me input-large">
                                            <asp:ListItem>FREE</asp:ListItem>
                                            <asp:ListItem>PAID</asp:ListItem>
                                        </asp:DropDownList>
                                        <span class="help-block">which way you want to share</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="textfield" class="control-label">Locked or Unlocked</label>
                            <div class="controls">
                                <div class="check-demo-col">
                                    <div class="check-line">
                                        <asp:DropDownList ID="DropDownList2" runat="server" CssClass="select2-me input-large">
                                            <asp:ListItem>LOCKED</asp:ListItem>
                                            <asp:ListItem>UNLOCKED</asp:ListItem>
                                        </asp:DropDownList>
                                        <span class="help-block">which way you want to share</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" runat="server" CssClass="btn btn-primary" OnClick="cmdSave_Click"><i class="glyphicon-ok_2"></i> Save</asp:LinkButton>
                            &nbsp;
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
