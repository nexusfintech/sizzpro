﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AgreementServiceMapping.aspx.cs" Inherits="Agreements_AgreementServiceMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkServiceMapping" runat="server" NavigateUrl="~/Agreements/AgreementMaster.aspx"><i class="icon-retweet"></i>&nbsp;&nbsp;Agreement Master</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Agreement Mapping</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <% if (User.IsInRole("administrator"))
                           { %>
                        <div class="control-group">
                            <label for="textfield" class="control-label">UserList</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectMember" runat="server" CssClass="select2-me input-xlarge" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectMember_SelectedIndexChanged"></asp:DropDownList>
                                <span class="help-block">Select User</span>
                            </div>
                        </div>
                        <%} %>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Agreements</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlAgreement" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                <span class="help-block">Select Agreemeent</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Services</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlService" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                <span class="help-block">Select Service</span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save Mapping" OnClick="cmdSave_Click" />
                            <asp:Button ID="cmdCancel" runat="server" CssClass="btn" Text="Cancel" OnClick="cmdCancel_Click" />
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

