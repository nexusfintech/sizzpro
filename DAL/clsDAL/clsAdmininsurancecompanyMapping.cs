﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsAdmininsurancecompanyMapping
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsAdmininsurancecompanyMapping()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsAdmininsurancecompanyMapping()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _aic_id; }
            set
            {
                _aic_id = value;
                if (clsCMD.Parameters.Contains("@aic_id"))
                { clsCMD.Parameters["@aic_id"].Value = value; }
                else { clsCMD.Parameters.Add("@aic_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _aic_id;
        public Int64 aic_id
        {
            get { return _aic_id; }
            set
            {
                _aic_id = value;
                if (clsCMD.Parameters.Contains("@aic_id"))
                { clsCMD.Parameters["@aic_id"].Value = value; }
                else { clsCMD.Parameters.Add("@aic_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _aic_userid;
        public Guid aic_userid
        {
            get { return _aic_userid; }
            set
            {
                _aic_userid = value;
                if (clsCMD.Parameters.Contains("@aic_userid"))
                { clsCMD.Parameters["@aic_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@aic_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _aic_inscid;
        public Int64 aic_inscid
        {
            get { return _aic_inscid; }
            set
            {
                _aic_inscid = value;
                if (clsCMD.Parameters.Contains("@aic_inscid"))
                { clsCMD.Parameters["@aic_inscid"].Value = value; }
                else { clsCMD.Parameters.Add("@aic_inscid", SqlDbType.BigInt).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                aic_id = Convert.ToInt64(dr["aic_id"]);
                aic_userid = new Guid(dr["aic_userid"].ToString());
                aic_inscid = Convert.ToInt64(dr["aic_inscid"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            aic_id = Convert.ToInt64("0");
            aic_userid = new Guid();
            aic_inscid = Convert.ToInt64("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.aic_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_AdmininsurancecompanyMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 aic_id)
        {
            Boolean blnResult = false;
            this.aic_id = aic_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_AdmininsurancecompanyMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 aic_id)
        {
            Boolean blnResult = false;
            this.aic_id = aic_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_AdmininsurancecompanyMappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_AdmininsurancecompanyMappingGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordbyAdmin(Guid adminid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.aic_userid = adminid;
                clsCMD.CommandText = "sp_AdmininsurancecompanyMappingGetData";
                SetGetSPFlag = "ALLBYADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllnotmappedRecordbyAdmin(Guid adminid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.aic_userid = adminid;
                clsCMD.CommandText = "sp_AdmininsurancecompanyMappingGetData";
                SetGetSPFlag = "ALLNOTMAPPEDBYADMIN";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }
        public List<clsAdmininsurancecompanyMapping_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsAdmininsurancecompanyMapping_PropertiesList> lstResult = new List<clsAdmininsurancecompanyMapping_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].aic_id = Convert.ToInt64(dr["aic_id"]);
                lstResult[i].aic_userid = new Guid(dr["aic_userid"].ToString());
                lstResult[i].aic_inscid = Convert.ToInt64(dr["aic_inscid"]);
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 aic_id)
        {
            Boolean blnResult = false;
            try
            {
                this.aic_id = aic_id;
                clsCMD.CommandText = "sp_AdmininsurancecompanyMappingGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 aic_id)
        {
            Boolean blnResult = false;
            try
            {
                this.aic_id = aic_id;
                clsCMD.CommandText = "sp_AdmininsurancecompanyMappingGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsAdmininsurancecompanyMapping_PropertiesList
    {
        public Int64 aic_id { get; set; }
        public Guid aic_userid { get; set; }
        public Int64 aic_inscid { get; set; }
    }
}