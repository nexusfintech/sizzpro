﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsInsuranceCompanyMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsInsuranceCompanyMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsInsuranceCompanyMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _insc_id; }
            set
            {
                _insc_id = value;
                if (clsCMD.Parameters.Contains("@insc_id"))
                { clsCMD.Parameters["@insc_id"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _insc_id;
        public Int64 insc_id
        {
            get { return _insc_id; }
            set
            {
                _insc_id = value;
                if (clsCMD.Parameters.Contains("@insc_id"))
                { clsCMD.Parameters["@insc_id"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _insc_name;
        public String insc_name
        {
            get { return _insc_name; }
            set
            {
                _insc_name = value;
                if (clsCMD.Parameters.Contains("@insc_name"))
                { clsCMD.Parameters["@insc_name"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_name", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _insc_payorid;
        public String insc_payorid
        {
            get { return _insc_payorid; }
            set
            {
                _insc_payorid = value;
                if (clsCMD.Parameters.Contains("@insc_payorid"))
                { clsCMD.Parameters["@insc_payorid"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_payorid", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _insc_address;
        public String insc_address
        {
            get { return _insc_address; }
            set
            {
                _insc_address = value;
                if (clsCMD.Parameters.Contains("@insc_address"))
                { clsCMD.Parameters["@insc_address"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_address", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _insc_city;
        public String insc_city
        {
            get { return _insc_city; }
            set
            {
                _insc_city = value;
                if (clsCMD.Parameters.Contains("@insc_city"))
                { clsCMD.Parameters["@insc_city"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_city", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _insc_state;
        public Int64 insc_state
        {
            get { return _insc_state; }
            set
            {
                _insc_state = value;
                if (clsCMD.Parameters.Contains("@insc_state"))
                { clsCMD.Parameters["@insc_state"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_state", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _insc_pincode;
        public String insc_pincode
        {
            get { return _insc_pincode; }
            set
            {
                _insc_pincode = value;
                if (clsCMD.Parameters.Contains("@insc_pincode"))
                { clsCMD.Parameters["@insc_pincode"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_pincode", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _insc_phone;
        public String insc_phone
        {
            get { return _insc_phone; }
            set
            {
                _insc_phone = value;
                if (clsCMD.Parameters.Contains("@insc_phone"))
                { clsCMD.Parameters["@insc_phone"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_phone", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _insc_fax;
        public String insc_fax
        {
            get { return _insc_fax; }
            set
            {
                _insc_fax = value;
                if (clsCMD.Parameters.Contains("@insc_fax"))
                { clsCMD.Parameters["@insc_fax"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_fax", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _insc_type;
        public Int64 insc_type
        {
            get { return _insc_type; }
            set
            {
                _insc_type = value;
                if (clsCMD.Parameters.Contains("@insc_type"))
                { clsCMD.Parameters["@insc_type"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_type", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _insc_billingtype;
        public String insc_billingtype
        {
            get { return _insc_billingtype; }
            set
            {
                _insc_billingtype = value;
                if (clsCMD.Parameters.Contains("@insc_billingtype"))
                { clsCMD.Parameters["@insc_billingtype"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_billingtype", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _insc_healthplaneligbility;
        public Int64 insc_healthplaneligbility
        {
            get { return _insc_healthplaneligbility; }
            set
            {
                _insc_healthplaneligbility = value;
                if (clsCMD.Parameters.Contains("@insc_healthplaneligbility"))
                { clsCMD.Parameters["@insc_healthplaneligbility"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_healthplaneligbility", SqlDbType.BigInt).Value = value; }
            }
        }
        private DateTime _insc_defaulttoicd10dos;
        public DateTime insc_defaulttoicd10dos
        {
            get { return _insc_defaulttoicd10dos; }
            set
            {
                _insc_defaulttoicd10dos = value;
                if (clsCMD.Parameters.Contains("@insc_defaulttoicd10dos"))
                { clsCMD.Parameters["@insc_defaulttoicd10dos"].Value = value; }
                else { clsCMD.Parameters.Add("@insc_defaulttoicd10dos", SqlDbType.DateTime).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                insc_id = Convert.ToInt64(dr["insc_id"]);
                insc_name = Convert.ToString(dr["insc_name"]);
                insc_payorid = Convert.ToString(dr["insc_payorid"]);
                insc_address = Convert.ToString(dr["insc_address"]);
                insc_city = Convert.ToString(dr["insc_city"]);
                insc_state = Convert.ToInt64(dr["insc_state"]);
                insc_pincode = Convert.ToString(dr["insc_pincode"]);
                insc_phone = Convert.ToString(dr["insc_phone"]);
                insc_fax = Convert.ToString(dr["insc_fax"]);
                insc_type = Convert.ToInt64(dr["insc_type"]);
                insc_billingtype = Convert.ToString(dr["insc_billingtype"]);
                insc_healthplaneligbility = Convert.ToInt64(dr["insc_healthplaneligbility"]);
                insc_defaulttoicd10dos = Convert.ToDateTime(dr["insc_defaulttoicd10dos"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            insc_id = Convert.ToInt64("0");
            insc_name = Convert.ToString("NA");
            insc_payorid = Convert.ToString("NA");
            insc_address = Convert.ToString("NA");
            insc_city = Convert.ToString("NA");
            insc_state = Convert.ToInt64("0");
            insc_pincode = Convert.ToString("NA");
            insc_phone = Convert.ToString("NA");
            insc_fax = Convert.ToString("NA");
            insc_type = Convert.ToInt64("0");
            insc_billingtype = Convert.ToString("NA");
            insc_healthplaneligbility = Convert.ToInt64("0");
            insc_defaulttoicd10dos = Convert.ToDateTime(DateTime.Now);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.insc_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_InsuranceCompanyMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                {
                    clsCMD.CommandType = CommandType.Text;
                    clsCMD.CommandText = "select @@identity as primarykey from tbl_InsuranceCompanyMaster";
                    this.PrimeryKey = Convert.ToInt32(clsCMD.ExecuteScalar());
                    blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; 
                }
                else 
                { 
                    blnResult = false; GetSetErrorMessage = "Error in new entry operation."; 
                }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 insc_id)
        {
            Boolean blnResult = false;
            this.insc_id = insc_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_InsuranceCompanyMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 insc_id)
        {
            Boolean blnResult = false;
            this.insc_id = insc_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_InsuranceCompanyMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_InsuranceCompanyMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsInsuranceCompanyMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsInsuranceCompanyMaster_PropertiesList> lstResult = new List<clsInsuranceCompanyMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].insc_id = Convert.ToInt64(dr["insc_id"]);
                lstResult[i].insc_name = Convert.ToString(dr["insc_name"]);
                lstResult[i].insc_payorid = Convert.ToString(dr["insc_payorid"]);
                lstResult[i].insc_address = Convert.ToString(dr["insc_address"]);
                lstResult[i].insc_city = Convert.ToString(dr["insc_city"]);
                lstResult[i].insc_state = Convert.ToInt64(dr["insc_state"]);
                lstResult[i].insc_pincode = Convert.ToString(dr["insc_pincode"]);
                lstResult[i].insc_phone = Convert.ToString(dr["insc_phone"]);
                lstResult[i].insc_fax = Convert.ToString(dr["insc_fax"]);
                lstResult[i].insc_type = Convert.ToInt64(dr["insc_type"]);
                lstResult[i].insc_billingtype = Convert.ToString(dr["insc_billingtype"]);
                lstResult[i].insc_healthplaneligbility = Convert.ToInt64(dr["insc_healthplaneligbility"]);
                lstResult[i].insc_defaulttoicd10dos = Convert.ToDateTime(dr["insc_defaulttoicd10dos"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 insc_id)
        {
            Boolean blnResult = false;
            try
            {
                this.insc_id = insc_id;
                clsCMD.CommandText = "sp_InsuranceCompanyMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 insc_id)
        {
            Boolean blnResult = false;
            try
            {
                this.insc_id = insc_id;
                clsCMD.CommandText = "sp_InsuranceCompanyMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsInsuranceCompanyMaster_PropertiesList
    {
        public Int64 insc_id { get; set; }
        public String insc_name { get; set; }
        public String insc_payorid { get; set; }
        public String insc_address { get; set; }
        public String insc_city { get; set; }
        public Int64 insc_state { get; set; }
        public String insc_pincode { get; set; }
        public String insc_phone { get; set; }
        public String insc_fax { get; set; }
        public Int64 insc_type { get; set; }
        public String insc_billingtype { get; set; }
        public Int64 insc_healthplaneligbility { get; set; }
        public DateTime insc_defaulttoicd10dos { get; set; }
    }
}