﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="depression.aspx.cs" Inherits="depression" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-question"></i>&nbsp;Depression</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <img src="img/depression.png" />
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h4>DEPRESSION</h4>
            <p>Depression is a mood disorder quite commonly diagnosed in our modern society.  It encompasses both psychological (mind) and physiological (body) issues.  Depression can cause symptoms such as anxiety, thoughts of hopelessness and helplessness, profound sadness, lack of positive self-image, lack of energy and a diminished interest in pleasurable activities.  Those who are depressed can suffer from agitation and irritability, weight loss or weight gain, insomnia, fatigue, inappropriate guilt, feelings of worthlessness, thoughts of death and sometimes thoughts of suicide. </p>
            <h5>How to Recognize Depression</h5>
            <p>The following symptoms may be strong indicators that an individual is experiencing depression:</p>
            <ul type="disc">
                <li>Feeling down and tearfulness over several days and weeks.</li>
                <li>Failing to keep up with current circumstances and responsibilities.</li>
                <li>Irritability, anxiety, tension and moodiness.</li>
                <li>Withdrawal from family and friends.</li>
                <li>Dropping grades or work productivity.</li>
                <li>Change in personality like loss of upbeat attitude.</li>
                <li>Sleeping trouble including getting to sleep and waking early (Insomnia).</li>
                <li>Difficulty feeling hungry,  weight loss and constipation.</li>
                <li>Lack of energy and tiredness/lack of stamina</li>
                <li>Loss of interest in sexual activity</li>
                <li>Difficulty concentrating and forgetfulness</li>
                <li>Feelings of low self-esteem, guilt, helplessness and hopelessness.</li>
                <li>Thoughts of death and making plans to hurt oneself</li>
                <li>Cutting and inflicting pain on oneself</li>
                <li>Hanging around the &quot;bad&quot; crowd.</li>
            </ul>
            <h5>How We Treat Depression</h5>
            <p>There are a myriad of medications available for the treatment of depression, yet this is only a short-term method which fails to help one understand the underlying reasons for this disorder.  Therapy for depression can be helpful in uncovering these causes and assist one in making positive life changes and a reduction in the necessity for medication. </p>
            <p>Thinking positive thoughts about yourself depends largely upon your view of yourself within the context of others. Marriage and Family Therapists believe that our self-concept is determined by the relationships we have with others. That is why we approach individual issues by addressing the family system. Your relationship with your family, friends, community, and God directly impacts your attitude and behavior. If you have been hurt by others either through abuse, neglect or simply tragic circumstances, depression and anxiety are the likely symptoms that may develop. These symptoms indicate you are at a crossroad and you are unable to decide which way to go. At Lifestyle we will help you process your hurts and identify new beliefs that support a healthy and fulfilling life and relationships. We will help you develop the clarity you need to move forward in your life. </p>
            <p>At Lifestyle Therapy Coach, we will work with you from one of several counseling programs that best meets your individual needs.  One such therapy emphasizes the client’s character strengths and virtues.  This positive psychology model helps one to create more upbeat emotions, become involved in more pleasurable activities, and harness and expand upon momentary times of satisfaction in life in order to be more fulfilled.  In this way, even small changes are recognized for the positive effects which they create in the client’s life. Other treatment emphasizes changing one's beliefs and emotions. In this method client's take responsibility for their behaviors and the changes that are required to produce different outcomes.</p>
            <p>We believe that most everyone has the innate desire and ability to become a happier person, but many do not know where to begin.  Our programs will help you tap into your personal resources and gain the confidence and ability to live the fulfilled life that you wish for. </p>
            <h5>Resources</h5>
            <a href="http://www.orlandocvi.com/documents/BeckDepressionInventory1.pdf" target="_blank">Beck  Depression Inventory&copy; Scale</a>
            <br>
            <a href="http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0001941/" target="_blank">Learn  more about Depression</a> from The National Institute of Mental Health<br>
            <a href="http://www.webmd.com/depression/default.htm" target="_blank">Learn more about Depression</a> from WebMD</p>
            <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

