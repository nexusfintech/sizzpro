﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAssessmentMedical.ascx.cs" Inherits="UserControl_Assessment_ucAssessmentMedical" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-stethoscope"></i>
                    Medical
                </h3>
            </div>
            <div class="box-content nopadding">
                <div class='form-horizontal form-column form-bordered'>

                    <div class="control-group">
                        <div class="span12">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtBornPlace" class="control-label">Where were you born..?</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtBornPlace" runat="server" placeholder="Born Place" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtraised" class="control-label">Where were you raised..?</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtraised" runat="server" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="span12">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtHeight" class="control-label">Height</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtHeight" runat="server" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtWaight" class="control-label">Weight</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtWaight" runat="server" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtWaist" class="control-label">Waist</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtWaist" runat="server" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtBodyFat" class="control-label">Percentage Body Fat</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtBodyFat" runat="server" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtBMI" class="control-label">BMI</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtBMI" runat="server" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtResentBP" class="control-label">Resent B. P.</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtResentBP" runat="server" placeholder="Blood Pressure" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtfamilyhistory" class="control-label">Family history of obesity?</label>
                        <div class="controls">
                            <asp:TextBox ID="txtfamilyhistory" runat="server" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtexercise" class="control-label">Do you exercise at least once per week?</label>
                        <div class="controls">
                            <asp:TextBox ID="txtexercise" runat="server" Width="50%" placeholder="Do you exercise at least once per week?" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtphysical" class="control-label">When was your last physical?</label>
                        <div class="controls">
                            <asp:TextBox ID="txtphysical" runat="server" Width="50%" placeholder="When was your last physical?" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtphysical" class="control-label">When was your last physical?</label>
                        <div class="controls">
                            <asp:TextBox ID="TextBox1" runat="server" Width="50%" placeholder="When was your last physical?" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtFamilyphycian" class="control-label">Who are your family physicians?</label>
                        <div class="controls">
                            <asp:TextBox ID="txtFamilyphycian" runat="server" Width="50%" placeholder="Who are your family physicians?" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtpsychiatrist" class="control-label">Name of your psychiatrist? (if you have one)</label>
                        <div class="controls">
                            <asp:TextBox ID="txtpsychiatrist" runat="server" Width="50%" placeholder="Name of your psychiatrist? (if you have one)" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtcurrentmedical" class="control-label">What current medical conditions are you being treated for?</label>
                        <div class="controls">
                            <asp:TextBox ID="txtcurrentmedical" runat="server" Width="50%" placeholder="What current medical conditions are you being treated for?" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtmedicaldosage" class="control-label">What medications (and dosage) are you currently taking?</label>
                        <div class="controls">
                            <asp:TextBox ID="txtmedicaldosage" runat="server" Width="50%" placeholder="What medications (and dosage) are you currently taking?" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtpresctibring" class="control-label">Who is the prescribing physician?</label>
                        <div class="controls">
                            <asp:TextBox ID="txtpresctibring" runat="server" Width="50%" placeholder="Who is the prescribing physician?" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtlastilnesstreat" class="control-label">What illnesses or conditions have you been treated for in the past?</label>
                        <div class="controls">
                            <asp:TextBox ID="txtlastilnesstreat" runat="server" Width="50%" placeholder="What illnesses or conditions have you been treated for in the past?" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtmajorhispitalpurpose" class="control-label">Any major hospitalizations? (What year and purpose)</label>
                        <div class="controls">
                            <asp:TextBox ID="txtmajorhispitalpurpose" runat="server" Width="50%" placeholder="Any major hospitalizations? (What year and purpose)" class="complexify-me"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-actions">
                        <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
