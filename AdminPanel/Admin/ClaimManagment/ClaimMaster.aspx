﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="ClaimMaster.aspx.cs" Inherits="Admin_SessionManagement_ClaimMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
     <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn btn-primary" Text="Add New Claim" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
             <li>
                <a>
                    <i class="glyphicon-retweet"></i>
                    <asp:Button ID="cmdSession" runat="server" CssClass="btn btn-primary" Text="Back to Session" OnClick="cmdSession_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Claim Master
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptClmMaster" runat="server" OnItemCommand="rptClmMaster_ItemCommand">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th width="10%">ClaimNumber</th>
                                        <th>Type</th>
                                        <th>Created Date</th>
                                        <th>From Time</th>
                                        <th>To Time</th>
                                        <th>Total Charge</th>
                                        <th>Insurance Co.</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td width="10%"><%# Eval("cm_id") %></td>
                                <td><%# Eval("cm_FormType") %></td>
                                <td><%# Eval("cm_datecreated","{0:dd/MM/yyyy}") %></td>
                                <td><%# String.Format("{0:HH:mm}",Eval("cm_timein")) %></td>
                                <td><%# String.Format("{0:HH:mm}",Eval("cm_timeout")) %></td>
                                <td><%# String.Format("{0:C}",Eval("cm_totalcharge")) %></td>
                                <td><%# Eval("insc_name") %></td>
                                <td><%# Eval("clmsts_status") %></td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "cm_id") %>' rel="tooltip" data-placement="top" title="EDIT"><i class="icon-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="cmdSubmit" runat="server" CssClass="btn btn-primary" CommandName="EDIT" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "cm_id") %>' rel="tooltip" data-placement="top" title="SUBMIT"><i class="icon-ok"></i></asp:LinkButton>
                                    <asp:LinkButton ID="cmdDlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "cm_id") %>' rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>