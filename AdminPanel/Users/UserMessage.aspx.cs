﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class Users_UserMessage : System.Web.UI.Page
{
    clsProfileMaster clsProfile = new clsProfileMaster();

    private string strMessageFlag;
    private string strtokenid;

    protected void Page_Load(object sender, EventArgs e)
    {
        strMessageFlag = Page.Request.QueryString["message"].ToString();
        strtokenid = Page.Request.QueryString["tokenid"].ToString();
        Boolean blnResult = clsProfile.GetRecordByTokenIDInProperties(strtokenid);
        if (blnResult == true)
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = Convert.ToDateTime(clsProfile.prm_tokendatetime);
            TimeSpan tmspan = startDate - endDate;
            if (tmspan.Days > 1)
            {
                lblpagemessage.Text = "Your requested link is expire please request for new otherwise contact administrator.";
                return;
            }

            if (strMessageFlag == "successfull")
            {
                lblusername.Text = MySession.Current.UserLoginName;
                lblEmailAccount.Text = clsProfile.prm_emailid;
                content_successfull.Visible = true;
            }
            if (strMessageFlag == "activation")
            {
                Boolean blnValidate = clsProfile.EmailVerify(strtokenid, true);
                if (blnValidate == true)
                {
                    lblusernameactivation.Text = clsProfile.prm_emailid;
                    lblusernameactivation1.Text = clsProfile.prm_emailid;
                    lblemailactivation.Text = clsProfile.prm_emailid;
                    content_activation.Visible = true;
                }
                else
                {
                    lblpagemessage.Text = "Email Account Verification Process not Complete Please Try Again..." + clsProfile.GetSetErrorMessage;
                }
            }
            if (strMessageFlag == "activationincomplete")
            {
                lblemailinactive.Text = clsProfile.prm_emailid;
                lblusernameinactive.Text = clsProfile.prm_emailid;
                content_activationfail.Visible = true;
            }
        }
        else { lblpagemessage.Text = "Profile Not Found..!!"; }
    }

    protected void cmdresend_Click(object sender, EventArgs e)
    {
        Boolean blnResult = clsGetTemplate.GetTemplate("ResentActivationLink");
        if (blnResult == true)
        {
            blnResult = clsProfile.GetRecordByTokenIDInProperties(strtokenid);
            if (blnResult == true)
            {
                string strGUID = Guid.NewGuid().ToString();
                MembershipUser msu = Membership.GetUser(clsProfile.prm_userId);
                Boolean blnguid = clsProfile.UpdateTokenID(clsProfile.prm_userId, strGUID);
                if (blnguid == true)
                {
                    string strhtml = clsGetTemplate.HtmlTemplate;
                    String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                    String strhost = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                    string strlink = strhost + "UserMessage.aspx?message=activation&tokenid=" + strGUID;
                    strhtml = strhtml.Replace("{activelink}", strlink).Replace("{username}", msu.UserName);
                    clsGetTemplate.HtmlTemplate = strhtml;
                    Boolean blnSendMail = clsGetTemplate.SendMail(clsProfile.prm_emailid);
                    if (blnSendMail == true)
                    { lbllinkgen.Text = "Email sent to your registred email account Please check your mail account inbox"; }
                    else { lbllinkgen.Text = "Email not sended successfully."; }
                }
                else { lbllinkgen.Text = "Token is not generate please contact to administrator...!!"; }
            }
            else { lbllinkgen.Text = "activation link is generated. and this link is dead please check you mailbox and use fresh link."; }
        }
    }

    protected void cmdExpiredResendLink_Click(object sender, EventArgs e)
    {
        Boolean blnResult = clsGetTemplate.GetTemplate("ResentActivationLink");
        if (blnResult == true)
        {
            blnResult = clsProfile.GetRecordByTokenIDInProperties(strtokenid);
            if (blnResult == true)
            {
                string strGUID = Guid.NewGuid().ToString();
                MembershipUser msu = Membership.GetUser(clsProfile.prm_userId);
                Boolean blnguid = clsProfile.UpdateTokenID(clsProfile.prm_userId, strGUID);
                if (blnguid == true)
                {
                    string strhtml = clsGetTemplate.HtmlTemplate;
                    String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                    String strhost = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                    string strlink = strhost + "UserMessage.aspx?message=activation&tokenid=" + strGUID;
                    strhtml = strhtml.Replace("{activelink}", strlink).Replace("{username}", msu.UserName);
                    clsGetTemplate.HtmlTemplate = strhtml;
                    Boolean blnSendMail = clsGetTemplate.SendMail(clsProfile.prm_emailid);
                    if (blnSendMail == true)
                    { lbllinkgen1.Text = "Email sent to your registred email account Please check your mail account inbox"; }
                    else { lbllinkgen1.Text = "Email not sended successfully."; }
                }
                else { lbllinkgen1.Text = "Token is not generate please contact to administrator...!!"; }
            }
            else { lbllinkgen1.Text = "activation link is generated. and this link is dead please check you mailbox and use fresh link."; }
        }
    }
}