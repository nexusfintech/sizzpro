﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_ReferingProviderAddEdit : System.Web.UI.Page
{
    clsReferringprovidersMaster clsRPM = new clsReferringprovidersMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                loaddata();
            }
        }
    }
    private void loaddata()
    {
        Boolean blnRslt = clsRPM.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {
            txtrpemail.Text = clsRPM.rp_email;
            txtrpAddress.Text = clsRPM.rp_address;
            txtrpcontact.Text = clsRPM.rp_contact;
            txtrpfax.Text = clsRPM.rp_fax;
            txtrpname.Text = clsRPM.rp_name;
        }
        else
        {
            MySession.Current.MESSAGE = clsRPM.GetSetErrorMessage;
            Response.Redirect("~/Master/ReferingProviderMaster.aspx");
        }
    }
    private string validatepage()
    {
        string result = string.Empty;
        if (txtrpname.Text == string.Empty)
        {
            result = "Provider Name Required";
            return result;
        }
        else if (txtrpcontact.Text == string.Empty)
        {
            return result = "Provider Contact Required";
        }
        else
        {
            return result;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string vldtrslt = validatepage();
        if(vldtrslt != string.Empty)
        {
            lblmessage.Text = vldtrslt;
            msgdiv.Visible = true;
        }

        clsRPM.rp_name = txtrpname.Text;
        clsRPM.rp_address = txtrpAddress.Text;
        clsRPM.rp_contact = txtrpcontact.Text;
        clsRPM.rp_fax = txtrpfax.Text;
        clsRPM.rp_email = txtrpemail.Text;
        clsRPM.rp_userid =(Guid) Membership.GetUser().ProviderUserKey;
        Boolean blnRslt;
        if(MySession.Current.AddEditFlag=="ADD")
        {
            blnRslt = clsRPM.Save();
        }
        else
        {
            blnRslt = clsRPM.Update(MySession.Current.EditID);
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/ReferingProviderMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsRPM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/ReferingProviderMaster.aspx");
    }
}