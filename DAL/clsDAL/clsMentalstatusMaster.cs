﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsMentalstatusMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsMentalstatusMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsMentalstatusMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _msm_id; }
            set
            {
                _msm_id = value;
                if (clsCMD.Parameters.Contains("@msm_id"))
                { clsCMD.Parameters["@msm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@msm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _msm_id;
        public Int64 msm_id
        {
            get { return _msm_id; }
            set
            {
                _msm_id = value;
                if (clsCMD.Parameters.Contains("@msm_id"))
                { clsCMD.Parameters["@msm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@msm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _msm_value;
        public Int64 msm_value
        {
            get { return _msm_value; }
            set
            {
                _msm_value = value;
                if (clsCMD.Parameters.Contains("@msm_value"))
                { clsCMD.Parameters["@msm_value"].Value = value; }
                else { clsCMD.Parameters.Add("@msm_value", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _msm_name;
        public String msm_name
        {
            get { return _msm_name; }
            set
            {
                _msm_name = value;
                if (clsCMD.Parameters.Contains("@msm_name"))
                { clsCMD.Parameters["@msm_name"].Value = value; }
                else { clsCMD.Parameters.Add("@msm_name", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private String _msm_flag;
        public String msm_flag
        {
            get { return _msm_flag; }
            set
            {
                _msm_flag = value;
                if (clsCMD.Parameters.Contains("@msm_flag"))
                { clsCMD.Parameters["@msm_flag"].Value = value; }
                else { clsCMD.Parameters.Add("@msm_flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                msm_id = Convert.ToInt64(dr["msm_id"]);
                msm_value = Convert.ToInt64(dr["msm_value"]);
                msm_name = Convert.ToString(dr["msm_name"]);
                msm_flag = Convert.ToString(dr["msm_flag"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            msm_id = Convert.ToInt64("0");
            msm_value = Convert.ToInt64("0");
            msm_name = Convert.ToString("NA");
            msm_flag = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.msm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_MentalstatusMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 msm_id)
        {
            Boolean blnResult = false;
            this.msm_id = msm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_MentalstatusMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 msm_id)
        {
            Boolean blnResult = false;
            this.msm_id = msm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_MentalstatusMasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_MentalstatusMasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByFlag(string flag)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.msm_flag = flag;
                clsCMD.CommandText = "sp_MentalstatusMasterGetData";
                SetGetSPFlag = "ALLBYFLAG";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsMentalstatusMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsMentalstatusMaster_PropertiesList> lstResult = new List<clsMentalstatusMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].msm_id = Convert.ToInt64(dr["msm_id"]);
                lstResult[i].msm_value = Convert.ToInt64(dr["msm_value"]);
                lstResult[i].msm_name = Convert.ToString(dr["msm_name"]);
                lstResult[i].msm_flag = Convert.ToString(dr["msm_flag"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 msm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.msm_id = msm_id;
                clsCMD.CommandText = "sp_MentalstatusMasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 msm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.msm_id = msm_id;
                clsCMD.CommandText = "sp_MentalstatusMasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsMentalstatusMaster_PropertiesList
    {
        public Int64 msm_id { get; set; }
        public Int64 msm_value { get; set; }
        public String msm_name { get; set; }
        public String msm_flag { get; set; }
    }
}