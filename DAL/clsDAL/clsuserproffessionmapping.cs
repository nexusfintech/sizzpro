﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using clsDAL.Conn;
using System.Data.SqlClient;
using System.Data;

namespace clsDAL
{
    public class clsuserproffessionmapping
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsuserproffessionmapping()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsuserproffessionmapping()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _prfm_id; }
            set
            {
                _prfm_id = value;
                if (clsCMD.Parameters.Contains("@prfm_id"))
                { clsCMD.Parameters["@prfm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@prfm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _prfm_id;
        public Int64 prfm_id
        {
            get { return _prfm_id; }
            set
            {
                _prfm_id = value;
                if (clsCMD.Parameters.Contains("@prfm_id"))
                { clsCMD.Parameters["@prfm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@prfm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _prfm_userid;
        public Guid prfm_userid
        {
            get { return _prfm_userid; }
            set
            {
                _prfm_userid = value;
                if (clsCMD.Parameters.Contains("@prfm_userid"))
                { clsCMD.Parameters["@prfm_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@prfm_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _prfm_memberalias;
        public String prfm_memberalias
        {
            get { return _prfm_memberalias; }
            set
            {
                _prfm_memberalias = value;
                if (clsCMD.Parameters.Contains("@prfm_memberalias"))
                { clsCMD.Parameters["@prfm_memberalias"].Value = value; }
                else { clsCMD.Parameters.Add("@prfm_memberalias", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prfm_useralias;
        public String prfm_useralias
        {
            get { return _prfm_useralias; }
            set
            {
                _prfm_useralias = value;
                if (clsCMD.Parameters.Contains("@prfm_useralias"))
                { clsCMD.Parameters["@prfm_useralias"].Value = value; }
                else { clsCMD.Parameters.Add("@prfm_useralias", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _prfm_clientalias;
        public String prfm_clientalias
        {
            get { return _prfm_clientalias; }
            set
            {
                _prfm_clientalias = value;
                if (clsCMD.Parameters.Contains("@prfm_clientalias"))
                { clsCMD.Parameters["@prfm_clientalias"].Value = value; }
                else { clsCMD.Parameters.Add("@prfm_clientalias", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                prfm_id = Convert.ToInt64(dr["prfm_id"]);
                prfm_userid = new Guid(dr["prfm_userid"].ToString());
                prfm_memberalias = Convert.ToString(dr["prfm_memberalias"]);
                prfm_useralias = Convert.ToString(dr["prfm_useralias"]);
                prfm_clientalias = Convert.ToString(dr["prfm_clientalias"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            prfm_id = Convert.ToInt64("0");
            prfm_userid = new Guid();
            prfm_memberalias = Convert.ToString("NA");
            prfm_useralias = Convert.ToString("NA");
            prfm_clientalias = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.prfm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_tbl_userproffessionmappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Guid UserId)
        {
            Boolean blnResult = false;
            this.prfm_userid = UserId;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_tbl_userproffessionmappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 prfm_id)
        {
            Boolean blnResult = false;
            this.prfm_id = prfm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_tbl_userproffessionmappingAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_tbl_userproffessionmappingGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsuserproffessionmapping_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsuserproffessionmapping_PropertiesList> lstResult = new List<clsuserproffessionmapping_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].prfm_id = Convert.ToInt64(dr["prfm_id"]);
                lstResult[i].prfm_userid = new Guid(dr["prfm_userid"].ToString());
                lstResult[i].prfm_memberalias = Convert.ToString(dr["prfm_memberalias"]);
                lstResult[i].prfm_useralias = Convert.ToString(dr["prfm_useralias"]);
                lstResult[i].prfm_clientalias = Convert.ToString(dr["prfm_clientalias"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByUserIdInProperties(Guid prfm_userid)
        {
            Boolean blnResult = false;
            try
            {
                this.prfm_userid = prfm_userid;
                clsCMD.CommandText = "sp_tbl_userproffessionmappingGetData";
                SetGetSPFlag = "BYUSERID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 prfm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.prfm_id = prfm_id;
                clsCMD.CommandText = "sp_tbl_userproffessionmappingGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion
    }
    public class clsuserproffessionmapping_PropertiesList
    {
        public Int64 prfm_id { get; set; }
        public Guid prfm_userid { get; set; }
        public String prfm_memberalias { get; set; }
        public String prfm_useralias { get; set; }
        public String prfm_clientalias { get; set; }
    }
}
