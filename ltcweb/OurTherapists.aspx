﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OurTherapists.aspx.cs" Inherits="OurTherapists" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-group"></i>Our Therapists</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div class="span2">
                <img src="img/therapist1.jpg" />
            </div>
            <div class="span10">
                <h5><a href="about-joe.aspx">Joseph L. Follette, Jr., M.Div., M.S., LMFT, AS - CEO</a></h5>
                <p>Joseph L. Follette, Jr. , owner of Lifestyle Therapy and Coaching and author of forthcoming book, &quot;Return to Love and Get the Sizzle Back in Your Marriage&quot;, is an ordained minister and a Licensed Marriage and Family Therapist with many years experience. Joe is a positive upbeat individual with a warm and friendly personality. He enjoys laughter and loves to see the spark of life return in his clients. He specializes in helping couples achieve <a href="http://www.sizzlinghotmarriage.com" target="_blank">Sizzling Hot Marriages</a>. </p>
                <p>Joe earned a Master’s of Divinity from Andrews University, Master’s Degree in Marriage &amp; Family Therapy from The University of Southern Mississippi and is currently pursuing a PhD in Marriage &amp; Family Therapy from Amridge University.</p>
            </div>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div class="span2">
                <img src="img/therapist4.jpg" />
            </div>
            <div class="span10">
                <h5><a href="#">Pettina King, M.S., LPC, NCC</a></h5>
                <p><strong>Vision:</strong>&nbsp;To provide individual, group, and family counseling, as well as educational presentations, in order to help people live their best life.</p>
                <p><strong>Mission:</strong>&nbsp;Providing competent, knowledgeable service with the client's own goals in mind; to deliver real help in a time of real need.</p>
                <p><strong>Target audience:</strong>&nbsp;Adults, age 18+, with addictions issues, mood disorders, or other life-disrupting issues in Huntsville, AL and the surrounding areas.</p>
                <p><strong>Therapeutic framework</strong></p>
                <p>&#9642;&nbsp;Rational Emotive Behavioral Therapy</p>
                <p>&#9642;&nbsp;Cognitive Behavioral Therapy</p>
                <p>&#9642;&nbsp;Client Centered Therapy</p>
                <p>&#9642;&nbsp;Any combination of the above as indicated</p>
                <p><strong>What makes me different:</strong>&nbsp;My unique value as a counselor is found in the blend of common sense, world experience, and proven therapeutic techniques that hallmarks my approach. I am a Licensed Professional Counselor and a National Certified Counselor.&nbsp; I have a BS in Psychology and a MS in Counseling Psychology.&nbsp; I have worked with individuals from all walks of life, from felony offenders to individuals with adjustment disorders.&nbsp;&nbsp; The majority of my focus has been working with persons who have addiction as well as co-occurring diagnoses. I understand how devastating substance abuse can be, not just to the individual, but to the entire family unit.&nbsp;&nbsp;&nbsp; As the individual works on getting into a more productive framework, it is important that they develop the confidence to rebuild the parts of their life that were destroyed or neglected during the time they were not living as their best selves.&nbsp; I have done case management, and I am still committed to helping my clients not just for the time they are in front of me, but also in a more holistic way.&nbsp; I believe in giving housing referrals, job search leads, and other life building information.&nbsp;</p>
            </div>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div class="span2">
                <img src="img/therapist5.jpg" />
            </div>
            <div class="span10">
                <h5><a href="http://vincentwhitejrdc.com/" target="_blank">Vincent White, Jr. DC</a></h5>
                <p>
                    Dr. Vincent White, Jr., chiropractor for Lifestyle Counseling and Chiropractic Care is a native of
                    Huntsville, Alabama and a graduate of Oakwood University. Dr. White completed his training
                    in 2007 from Sherman College of Chiropractic. Since 2007, he has offered his services in helping
                    to open two chiropractic clinics and has served hundreds of patients, of which, amazing results
                    have been seen!
                </p>
                <p>
                    Currently accepting new patients. Treating conditions common to chiropractic such as back or neck
                    pain, headaches, and arm or leg pain. These conditions are often caused by misalignments of the
                    spine called subluxations. Subluxations cause the nerve openings to be made smaller putting pressure
                    on the spinal nerves causing nerve interference with the transmission of information from the brain,
                    over the spinal cord to all the systems of the body. The resulting altered function disrupts the body’s
                    ability to function at its best. Consequently, when subluxations are corrected the ability of the body to
                    function at its best is restored.
                </p>
            </div>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div class="span2">
                <img src="img/therapist6.jpg" />
            </div>
            <div class="span10">
                <h5><a href="#">Claudia Follette, RD</a></h5>
                <p>Claudia Follette is a Licensed Registered Dietitian with expertise in Medical Nutrition Therapy and Health Coaching. Claudia has over 20 years of experience as a Nutrition Therapists working with patients with renal disease, HIV/AIDS, high cholesterol, diabetes, and high blood pressure. As our Nutrition & Wellness Therapist she will help you reach your health goals and provide treatment for cholesterol and blood pressure management, weight control, blood sugar management, emotional and compulsive eating, metabolic syndrome, healthy digestive system functioning, and family nutrition. </p>
            </div>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div class="span2">
                <img src="img/therapist2.png" />
            </div>
            <div class="span10">
                <h5><a href="#">Alicia Lomack, M.A.</a></h5>
                <p>Alicia Lomack is a counseling intern under the supervision of Joseph Follette, Jr. LMFT and Approved Supervisor at Lifestyle Therapy and Coaching. She has provided family planning counseling for pregnant women as well as post abortion counseling.  Alicia has experience working with elementary age children with behavior problems and partnering with their parents to ensure greater success for their education.  She has worked with individuals who are battling alcoholism and drug abuse.  Alicia is a military wife and mother and has personal experience dealing with deployment issues.  She loves working with older adults and has a passion for helping individuals reach their goals.  She has an energizing personality and loves to focus on the positive. Alicia looks forward to helping you reach your goals. </p>
            </div>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div class="span2">
                <img src="img/therapist3.jpg" />
            </div>
            <div class="span10">
                <h5><a href="aboutcelina.aspx">Celina Villalobos, M.S., N.S.</a></h5>
                <p>Currently under the supervision of Joseph L. Follette Jr. LMFT - Approved Supervisor, Celina is working to become a Licensed Marriage &amp; Family Therapist and a Licensed Professional Counselor. She practiced therapy in Mexico for several years prior to moving to Huntsville and has many years of counseling experience helping individuals, couples and families work through their issues and reach their goals. Celina especially enjoys working with adolescents and adults. Most of her clients say that they experience her as being warm, kind and gentle in her approach to therapy. While Spanish is her first language she speaks English very well as well as some French. She loves helping people and looks forward to helping you work through the challenges you are currently facing.</p>
                <h4>Education</h4>
                <p>Celina has a Master's degree in Clinical and Health Psychology (Barcelona, ES.), a degree in Clinical Neuropsychology (Barcelona, ES.) and will complete another Master's in Family Therapy (Mexico) by December 2013.</p>
                <h4>Languages</h4>
                <p>English, Spanish, and some French.</p>
            </div>
        </div>
    </div>
    <hr />
    <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

