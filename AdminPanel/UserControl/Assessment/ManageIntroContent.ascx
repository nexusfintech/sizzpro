﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageIntroContent.ascx.cs" Inherits="UserControl_Assessment_ManageIntroContent" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="glyphicon-notes"></i>
                    <asp:Label ID="lblBoxHeader" runat="server"></asp:Label>
                </h3>
            </div>
            <div class="box-content">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="modal-body nopadding">
                        <div class="control-group">
                            <asp:TextBox ID="txtContent" runat="server" class='ckeditor' TextMode="MultiLine">
                            </asp:TextBox>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                        <asp:Button ID="cmdReschedule" runat="server" CssClass="btn btn-primary" Text="Re-Assesment" Visible="false" OnClick="cmdReschedule_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
