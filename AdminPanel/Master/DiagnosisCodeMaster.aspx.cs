﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Master_DiagnosisCodeMaster : System.Web.UI.Page
{
    clsDiagnosisCodeMaster clsDCM = new clsDiagnosisCodeMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
        if (MySession.Current.MESSAGE != string.Empty)
        {
            lblmessage.Text = MySession.Current.MESSAGE;
            msgdiv.Visible = true;
            MySession.Current.MESSAGE = string.Empty;
        }
    }
    private void loaddata()
    {
        rptdgCode.DataSource = clsDCM.GetAllRecord();
        rptdgCode.DataBind();
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Master/DiagnosisCodeMasterAddEdit.aspx");
    }
    protected void rptdgCode_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Master/DiagnosisCodeMasterAddEdit.aspx");
        }
        if (e.CommandName == "DELETE")
        {
            Boolean blnrslt = clsDCM.DeleteByPKID(Convert.ToInt64(e.CommandArgument));
            if (blnrslt)
            {
                MySession.Current.MESSAGE = "Record Deleted Successfully";
                Response.Redirect("~/Master/DiagnosisCodeMaster.aspx");
            }
            else
            {
                lblmessage.Text = clsDCM.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
    protected void rptdgCode_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;
            Boolean ispybl = Convert.ToBoolean(DataBinder.Eval(DI, "dcm_ispayable"));
            Boolean hcc = Convert.ToBoolean(DataBinder.Eval(DI, "dcm_hcc"));

            if (ispybl)
            {
                Label lbl = (Label)e.Item.FindControl("lpt");
                lbl.Visible = true;
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lpf");
                lbl.Visible = true;
            }
            if (hcc)
            {
                Label lbl = (Label)e.Item.FindControl("lht");
                lbl.Visible = true;
            }
            else
            {
                Label lbl = (Label)e.Item.FindControl("lhf");
                lbl.Visible = true;
            }
        }
    }
}