﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContactUS.aspx.cs" Inherits="UserControl_ContactUS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-phone"></i>&nbsp;Contact US</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <div class="span8">
                <h4>
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </h4>
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-mail-forward"></i>
                            Contact Detail
                        </h3>
                    </div>
                    <div class="box-content nopadding">
                        <div class='form-horizontal form-column form-bordered'>
                            <div class="control-group">
                                <label for="txtFirstName" class="control-label">Name*</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtName" runat="server" placeholder="Name" data-rule-required="true" class="complexify-me"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="txtEmail" class="control-label">Email Address*</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtEmail" runat="server" placeholder="Email Address" data-rule-required="true" class="complexify-me"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="txtSubject" class="control-label">Subject*</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtSubject" runat="server" placeholder="Subject" data-rule-required="true" class="complexify-me"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="txtMessage" class="control-label">Message*</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtMessage" runat="server" placeholder="Descriptive Message" data-rule-required="true"  class="complexify-me" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-actions">
                                <asp:Button ID="cmdSend" runat="server" CssClass="btn btn-primary" Text="Send" OnClick="cmdSend_Click"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span4">
                <strong>Lifestyle Therapy and Coaching</strong>
                <p><strong>4801 University Square NW</strong> <strong>Suite 13</strong></p>
                <p>
                    <strong>Huntsville, AL 35816<br />
                        <br />
                        Phone: 1-256-850-4426<br />
                        FAX:   1-925-522-4887</strong>
                </p>
                <p><a href="mailto:admin@lifestyletherapycoach.com"><strong>admin@LifestyleTherapyCoach.com</strong></a></p>
                <p>Click here to get started - <a href="GettingStarted.aspx">Getting Started</a></p>
                <p class="headline">Referral Forms</p>
                <p>Release of Information - <a href="/downloads/Release_of_Information_Update.pdf" target="_blank">PDF</a> <a href="/downloads/Release_of_Information_Update.doc" target="_blank">DOC</a></p>
                <p>Physician Referral - <a href="/downloads/PhysicianReferralForm.pdf" target="_blank">PDF</a> <a href="/downloads/PhysicianReferralForm.doc" target="_blank">DOC</a></p>
                <p><a href="Client/Appointment.aspx" class="wrapper">Referral Scheduling</a></p>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

