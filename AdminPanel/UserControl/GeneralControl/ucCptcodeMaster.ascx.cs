﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;

public partial class UserControl_GeneralControl_ucCptcodeMaster : System.Web.UI.UserControl
{
    clsCPTCodeMaster clsCCM = new clsCPTCodeMaster();
    private Int64 intId = 0;
    private Int64 charge = 0;
    private string strName = string.Empty;

    public Int64 SelectedValue
    {
        get { intId = Convert.ToInt64(ddlCpt.SelectedValue); return intId; }
        set 
        { 
            intId = value;
            ListItem itm = ddlCpt.Items.FindByValue(intId.ToString());
            if (itm != null)
            {
                ddlCpt.SelectedIndex = ddlCpt.Items.IndexOf(itm);
            }
        }
    }

    public Int64 CodeCharge
    {
        get 
        { 
            return charge; 
        }
        set
        {
            charge = value;
        }
    }

    public string SelectedName
    {
        get { strName = ddlCpt.SelectedItem.Text; return strName; }
        set { strName = value; }
    }
    public int Width
    {
        set { ddlCpt.Width = Unit.Pixel(value); }
    }
    private void Bind()
    {
        DataTable dt = clsCCM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        dt.Columns.Add("Code", typeof(string), "cd_code + ' - '  + cd_description");
        ddlCpt.DataTextField = "Code";
        ddlCpt.DataValueField = "cd_code";
        ddlCpt.DataSource = dt;
        ddlCpt.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}