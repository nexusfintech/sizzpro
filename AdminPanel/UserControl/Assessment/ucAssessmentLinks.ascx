﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAssessmentLinks.ascx.cs" Inherits="UserControl_Assessment_ucAssessmentLinks" %>
<div class="subnav">
    <h4 class="subnav-title">
        <asp:Label ID="lblName" runat="server"></asp:Label>
    </h4>
    <div class="subnav-title">
        <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Assessment Step</span></a>
    </div>
    <ul class="subnav-menu">
        <li>
            <asp:HyperLink ID="lnkIntro" runat="server"><i class="icon-user"></i> Intro</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkProfile" runat="server"><i class="icon-user"></i> You & Profile</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkFamily" runat="server"><i class="icon-user"></i> Family & Origion</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkspouses" runat="server"><i class="icon-user"></i> Spouses & Signicant</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkChildren" runat="server"><i class="icon-user"></i> Childrens</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkSiblings" runat="server"><i class="icon-user"></i> Siblings</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkFin" runat="server"><i class="icon-money"></i> Financial</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkPersonalH" runat="server"><i class="icon-user"></i> Personal History</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkWellBeing" runat="server"><i class="icon-github-alt"></i> Physical WellBeing</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnksymptoms" runat="server"><i class="icon-github-alt"></i> Physical Symptoms</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkAccident" runat="server"><i class="icon-ambulance"></i> Accident Hisotry</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkMedical" runat="server"><i class="icon-ambulance"></i> Medical History</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkMental" runat="server"><i class="icon-ambulance"></i> Mental Health Hisotry</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkaddiction" runat="server"><i class="glyphicon-smoking"></i> Addiction</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkproblem" runat="server"><i class="icon-remove-sign"></i> Problems</asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="lnkreadiness" runat="server"><i class="glyphicon-eye_open"></i> Readiness</asp:HyperLink>
        </li>
    </ul>
</div>
