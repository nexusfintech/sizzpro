﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FAQ.aspx.cs" Inherits="FAQ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-question-sign"></i>&nbsp;FAQ</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <h4>FREQUENTLY ASKED QUESTIONS</h4>
            <p>&nbsp;</p>
            <p>Getting Started and General Administration</p>
            <ul>
                <li><a href="FAQ.aspx#1">What kinds of counseling do you offer?</a></li>
                <li><a href="FAQ.aspx#2">How do I make an appointment with a counselor?</a></li>
                <li><a href="FAQ.aspx#3">I've never gone to counseling before. What can I expect?</a></li>
                <li><a href="FAQ.aspx#4">Where are you located?</a></li>
            </ul>
            Counseling Sessions
    <ul>
        <li><a href="FAQ.aspx#5">How many sessions do I need?</a></li>
        <li><a href="FAQ.aspx#101">What is your counseling philosophy?</a></li>
        <li><a href="FAQ.aspx#6">What if I need to cancel or reschedule my session?</a></li>
        <li><a href="FAQ.aspx#7">What if I will be late for my session?</a></li>
        <li><a href="FAQ.aspx#8">Do you have a psychiatrist at LTC?</a></li>
    </ul>
            Fees and Payment
    <ul>
        <li><a href="FAQ.aspx#9">What are your fees?</a></li>
        <li><a href="FAQ.aspx#10">Do you take health insurance?</a></li>
        <li><a href="FAQ.aspx#11">Do you accept credit cards?</a></li>
    </ul>
            Couples and Marriage Counseling
    <ul>
        <li><a href="FAQ.aspx#12">What is the Sizzling Hot Marriage Assessment?</a></li>
        <li><a href="FAQ.aspx#13">We are being married by an LTC pastor. Where do I sign up for premarital counseling?</a></li>
    </ul>
            Other
    <ul>
        <li><a href="FAQ.aspx#14">How can I purchase LTC counseling resources?</a></li>
        <li><a href="FAQ.aspx#15">How can I sign up for your seminars and groups?</a></li>
    </ul>
            <br/>
            <strong><a name="1"></a>What kinds of counseling do you offer?</strong>
            <p>We see a wide variety of people for individual, pre-marital, marital and family counseling for most  issues people face. Our specialty is marriage and family relationships.  See&nbsp;<a href="OurServices.aspx">Services</a>&nbsp;for more information. We offer face-to-face therapy as well as telephone and Skype therapy. We also offer group therapy in our <a href="http://www.recoveringmylife.com">Lifestyle Life Recovery</a> and <a href="http://www.sizzlinghotmarriage.com">Sizzling Hot Marriage</a> programs.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><strong><a name="101" id="101"></a>Our Philosophy of Change</strong></p>
            <p>Our philosophy is that the change you desire to achieve in yourself and relationships is beyond eliminating the troubled emotions that may drive you into therapy. Many enter therapy with very minor challenges and only need a few sessions to get some clarity on an issue. However, others enter therapy with very complex issues and often quit therapy before it has the chance to produce all the change needed to develop the skills needed to achieve more permanent change in their personality and relationships. Like chemotherapy which hopes to get all of the cancer, we hope to resolve all of the problem beliefs and behaviors contributing to the trouble that brought you into  treatment in the first place. By taking a more holistic approach our clients can go on to live their lives with the interpersonal power and individual productivity God intended and achieve the improved outcome these types of evidence-based approaches tend to demonstrate. </p>
            <p>&nbsp;</p>
            <p>Our research shows the five things that keep people from staying in treatment long enough to achieve a more permanent level of change are time, money, incompatibility with the therapist, ineffective goals and impatience. So we have created a system to ensure our clients success in therapy.&nbsp;</p>
            <ol>
                <li>We make a year long commitment to our clients to ensure they obtain the permanent change they desire to achieve. </li>
                <li>We listen carefully to our clients to ensure we accurately access their problems and give them confidence that we can help them. </li>
                <li>We identify transformative goals and objectives that our clients feel are appropriate, desireable, and obtainable. </li>
                <li>We offer our services at affordable rates along with convenient payment plans.</li>
                <li>We utilize technology to keep our clients in treatment and moving toward their goals to ensure real and lasting change.</li>
            </ol>
            <p>
                <br/>
                <strong><a name="2"></a>How do I make an appointment with a counselor?</strong>
            </p>
            <p><a href="http://lifestyletherapycoach.com/initials/InformedConsent.asp"></a>You can call our scheduling service at <strong>(256) 850-4426</strong> to schedule an appointment or use our <a href="Client/Appointment.aspx">online scheduler</a> to make your first appointment. Please note we are unable to accept walk-in appointments and cannot guarantee same-day appointments.  Complete our online intake application or download your <a href="Client/Appointment.aspx">intake application</a> here.</p>
            <br/>
            <strong><a name="3"></a>I've never gone to counseling before. What can I expect?</strong>
            <p>Counseling involves beginning a new relationship. It may take a few sessions for your counselor to get to know you and your story before moving into actual therapy. Counseling is also interactive and dynamic, and you should feel free to ask questions to help you connect with your counselor as you progress through the counseling experience together. It is an opportunity for you to share what is on your mind with a third party who will help you understand your problem better and help you achieve the changes that will lead to you feeling better about yourself and your problem.</p>
            <br/>
            <strong><a name="4"></a>Where are you located?</strong>
            <p>We are located at <a href="http://maps.google.com/maps?hl=en&amp;q=4801+university+square+huntsville+al&amp;bav=on.2,or.r_gc.r_pw.&amp;biw=1348&amp;bih=649&amp;um=1&amp;ie=UTF-8&amp;hq=&amp;hnear=0x88626be46839c899:0xa4339b3f1a1c2e4c,4801+University+Square,+Huntsville,+AL+35816&amp;gl=us&amp;ei=7Nv5TZeIJ-rb0QHQ5-SkAw&amp;sa=X&amp;oi=geocode_result&amp;ct=title&amp;resnum=1&amp;ved=0CBYQ8gEwAA">4801 University Square Suite 13</a>. Take University Drive to The Boardwalk. The Boardwalk is between Old Monrovia and Wynn Drive. LaQuinta and Country Inn and Suites is on the corner of The Boardwalk and University Drive. Newks Restaurant is in the shopping center across the street. We are in the last building on the right on The Boardwalk. Suite 13 is the fourth suite from the corner. If you are coming from Wynn Drive, University Square is the street between the post office and HL Cleveland way. If coming from University Drive pass the post office turn left onto University Square. Take a right at the stop sign and curve on around the bend to the left. Turn left into the first driveway on your left. Suite 13 will be to the right. <a href="http://maps.google.com/maps?hl=en&amp;q=4801+University+Square+NW+Suite+13+Huntsville,+AL+35816&amp;bav=on.2,or.r_gc.r_pw.,cf.osb&amp;biw=1366&amp;bih=659&amp;um=1&amp;ie=UTF-8&amp;hq=&amp;hnear=0x88626be46839c899:0xc38314422e9e53d,4801+University+Square+%2313,+Huntsville,+AL+35816&amp;gl=us&amp;ei=cBDTTqX9O5GhtwfPk9WpDQ&amp;sa=X&amp;oi=geocode_result&amp;ct=title&amp;resnum=1&amp;ved=0CBwQ8gEwAA" target="_blank">Click Here to Get Directions from your location</a>.</p>
            <br/>
            <strong><a name="5"></a>How many sessions do I need?</strong>
            <p>Because each case is different, your counselor will ascertain your needs and discuss a treatment plan with you.</p>
            <br/>
            <strong><a name="6"></a>What if I need to cancel or reschedule my session?</strong>
            <p>You must notify your counselor 24 hours before your scheduled appointment time. Failure to do so will result in being charged $75 for the session. Please see our cancellation policy under "Sessions &amp; Fees".</p>
            <br/>
            <strong><a name="7"></a>What if I am running late to my session?</strong>
            <p>Please contact your counselor directly using the number they give you. Leaving a message on the Reception line will not ensure that your counselor is notified. In general, an email reply to admin@lifestyletherapycoach will be received within an hour.</p>
            <br/>
            <strong><a name="8"></a>Do you have a psychiatrist at LTC?</strong>
            <p>No we do not have a staff psychiatrist. We  will refer you to a psychiatrist if needed.</p>
            <br/>
            <strong><a name="9"></a>What are your fees?</strong>
            <p>&nbsp;</p>
            <p>
                <table border="0" cellpadding="0" cellspacing="0" class="auto-style1" style="mso-yfti-tbllook: 1184; mso-padding-alt: 0in 5.4pt 0in 5.4pt; mso-border-insideh: none; mso-border-insidev: none">
                    <tr>
                        <td class="auto-style2" valign="top">
                            <p class="auto-style20">
                                Services<o:p></o:p>
                            </p>
                            <p class="auto-style9">
                                <o:p></o:p>
                            </p>
                        </td>
                        <td valign="top" width="78">
                            <p>
                                &nbsp;
                            </p>
                            <p>
                                <b><span>Rate<o:p></o:p></span></b>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style10" valign="top">
                            <p>
                                <span>Assessment Session (60 minutes)<o:p></o:p></span>
                            </p>
                        </td>
                        <td class="auto-style11" valign="top" width="78">
                            <p class="auto-style15">
                                $220<o:p></o:p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style14" valign="top">
                            <p>
                                <span>Follow-up Session (50 minutes)<o:p></o:p></span>
                            </p>
                        </td>
                        <td valign="top" width="78">
                            <p class="auto-style15">
                                $150<o:p></o:p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style14" valign="top">
                            <p>
                                <span>Extended Session (&lt;30 minutes)<o:p></o:p></span>
                            </p>
                        </td>
                        <td valign="top" width="78">
                            <p class="auto-style15">
                                $75<o:p></o:p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style14" valign="top">
                            <p>
                                <span>Group Session<o:p></o:p></span>
                            </p>
                        </td>
                        <td valign="top" width="78">
                            <p class="auto-style15">
                                $80<o:p></o:p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style12" valign="top">
                            <p>
                                <span>No Show/Late Cancel<o:p></o:p></span>
                            </p>
                        </td>
                        <td class="auto-style13" valign="top" width="78">
                            <p class="auto-style15">
                                $75<o:p></o:p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style14" valign="top">
                            <p>
                                <span>Average Insurance Copayment<o:p></o:p></span>
                            </p>
                        </td>
                        <td valign="top" width="78">
                            <p class="auto-style15">
                                $10 - $50<o:p></o:p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style14" valign="top">
                            <p>
                                <span>Average # of Sessions
                                    <o:p></o:p>
                                </span>
                            </p>
                        </td>
                        <td valign="top" width="78">
                            <p class="auto-style15">
                                8-16<o:p></o:p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style14" valign="top">
                            <p>
                                <span>Return Check Fee<o:p></o:p></span>
                            </p>
                        </td>
                        <td valign="top" width="78">
                            <p class="auto-style15">
                                $32<o:p></o:p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style10" valign="top">
                            <p>
                                <span>Tests and Measures<o:p></o:p></span>
                            </p>
                        </td>
                        <td class="auto-style11" valign="top" width="78">
                            <p class="auto-style15">
                                $199<o:p></o:p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style14" valign="top">
                            <p>
                                <span>Psychosocial Assessment<o:p></o:p></span>
                            </p>
                        </td>
                        <td valign="top" width="78">
                            <p class="auto-style15">
                                $350<o:p></o:p>
                            </p>
                        </td>
                    </tr>
                </table>
            </p>
            <p>&nbsp;</p>
            <p>
                <span>Many of our clients take advantage of our <b>treatment programs</b>. Our treatment programs provide a more comprehensive approach to addressing the complex challenges many of our clients face. Relationship problems often involve significant clinical issues that generally take several months to work through. ie. Substance abuse, affair, depression, anxiety, grief, pornography, sexual abuse, sexual problems, anger management. Our extensive treatment programs ensure that our clients make positive lifestyle changes that last a lifetime. What you focus on you cause to happen. We currently offer the <b><i>Lifestyle Life Recovery</i></b> and <b><i>Sizzling Hot Marriage Maker</i></b> programs.<o:p></o:p></span>
            </p>
            <p>
                &nbsp;<table border="0" cellpadding="0" cellspacing="0" class="auto-style1" style="mso-yfti-tbllook: 1184; mso-padding-alt: 0in 5.4pt 0in 5.4pt; mso-border-insideh: none; mso-border-insidev: none">
                    <tr>
                        <td colspan="2" valign="top">
                            <p>
                                &nbsp;
                            </p>
                            <p>
                                <b><span>Programs</span></b><span><o:p></o:p></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style4" valign="top">
                            <p>
                                <i><span>Program Fee</span></i><span> - 12-Month Therapist Retainer/Concierge with Mobile, Email &amp; Text Support, An Assessment Session, An Assessment Test, Online Workbook, Case Management, and Priority Scheduling<o:p></o:p></span>
                            </p>
                        </td>
                        <td class="auto-style6" valign="top">
                            <p>
                                <span>$499<o:p></o:p></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style4" valign="top">
                            <p>
                                <span>Prepaid Therapy (6 Sessions)<o:p></o:p></span>
                            </p>
                        </td>
                        <td class="auto-style6" valign="top">
                            <p>
                                <span>$599<o:p></o:p></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style4" valign="top">
                            <p>
                                <span>Recovery Group Therapy (12 Sessions)<o:p></o:p></span>
                            </p>
                        </td>
                        <td class="auto-style6" valign="top">
                            <p>
                                <span>$360<o:p></o:p></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style5" valign="top">
                            <p>
                                <span>Minimum Down Payment<o:p></o:p></span>
                            </p>
                        </td>
                        <td class="auto-style7" valign="top">
                            <p>
                                <span>$250<o:p></o:p></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style5" valign="top">
                            <p>
                                <span>Installment Fee (per payment)<o:p></o:p></span>
                            </p>
                        </td>
                        <td class="auto-style7" valign="top">
                            <p>
                                <span>$10<o:p></o:p></span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style8" colspan="2">
                            <p>
                                &nbsp;
                            </p>
                            <p>
                                Insurance may only cover a portion of program fee.<br class="auto-style19" />
                                Insurance payments and copayments may cover therapy fees.<br class="auto-style19" />
                                Clients are responsible for fees unpaid by insurance.<br class="auto-style19" />
                                Other than the $99 monthly installment plan all fees must be paid by the last session.<br class="auto-style19" />
                                We accept cash, checks and credit/debit cards.<o:p><br />
                    1.5% late fees on unpaid balances each month.<o:p><br />
                    We initiate collections after 60 days of non-payment.<o:p><br />
                    Fees can also be paid at <a href="http://payment.lifestyletherapycoach.com" target="_blank">payment.lifestyletherapycoach.com</a>.</o:p></o:p></o:p>
                            </p>
                            <p>
                                <o:p>
                    <o:p>
                    <o:p>We do offer sliding scale fees for clients who can establish need.</o:p></o:p></o:p>
                            </p>
                        </td>
                    </tr>
                </table>
            </p>
            <p>&nbsp; </p>
            <p>&nbsp;</p>
            <p><strong><a name="10"></a>Do you take any health insurance?</strong> </p>
            <p>We accept health insurance on a case by case basis and will file on your behalf.&nbsp; Your insurance company will reimburse you according to your plan. We accept most major companies including Blue Cross Blue Shield, American Behavioral, Aetna, CIGNA, United Healthcare, TRICARE, American Behavioral, Coventry, Administrative Concepts (Oakwood University), Medicare, Mutual of Omaha, Behavioral Health Systems and GEHA. Be sure to confirm with your health plan to ensure coverage. You are responsible for your copay and deductible at time of service and for all fees on your account.</p>
            <br/>
            <strong><a name="11"></a>Do you accept credit cards?</strong>
            <p>Yes. You can pay online at <a href="Resources.aspx">payment.LifestyleTherapyCoach.com</a> and we accept credit card payments in our office. </p>
            <p>
                <br/>
                <strong><a name="12"></a>We are thinking about getting married. Do you offer  premarital counseling?</strong>
            </p>
            <p>Our therapists  provide  premarital counseling. Our <a href="http://www.SizzlingHotMarriage.com" target="_blank">Sizzling Hot Marriage</a> program is for premarried couples as well. We utilize the Prepare-Enrich assessment  to help couples preparing for marriage.</p>
            <br/>
            <strong><a name="13"></a>What is the Sizzling Hot Marriage assessment?</strong>
            <p>The  <a href="http://www.SizzlingHotMarriage.com" target="_blank">Sizzling Hot Marriage</a> program is designed to help couples get the heat back into their relationship.  Couples take the assessment online and  work through the results with a counselor in 10-12 follow up sessions.</p>
            <br/>
            <strong><a name="14"></a>Where do I purchase Lifestyle Therapy &amp; Coaching counseing resources?</strong>
            <p>You may browse our <a href="Resources.aspx">counseling resources</a> here.</p>
            <br/>
            <strong><a name="15"></a>How can I sign up for your seminars and support groups?</strong>
            <p>Please register online or call for our seminars and groups. Please visit our&nbsp;<a href="groups.aspx">groups page</a>&nbsp;for a list of current offerings.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

