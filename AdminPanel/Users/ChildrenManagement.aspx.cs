﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Users_ChildrenManagement : System.Web.UI.Page
{
    string strMemberkey = "";
    Int64 intUserid;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["form_memberkey"] != null)
        {
            strMemberkey = Session["form_memberkey"].ToString();
            intUserid = Convert.ToInt64(Session["form_userid"]);
            ucChildren.Memberkey = strMemberkey;
        }
        else { Response.Redirect("~/Users/UserClientManage.aspx"); }
    }
}