﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AccidentInformation.aspx.cs" Inherits="Client_Assessments_AccidentInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <asp:HiddenField ID="AssesmentStep" runat="server"  Value="14"/>
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-stethoscope"></i>
                Client's Accident Information
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:AccidentInformation ID="uccAccdntInfo" runat="server" />
        </div>
    </div>
    <div class="row-fluid">
        <hr />
        <div class="span12">
            <asp:Button ID="cmdNext" runat="server" Text="Next Medical >>" CssClass="btn btn-primary" OnClick="cmdNext_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

