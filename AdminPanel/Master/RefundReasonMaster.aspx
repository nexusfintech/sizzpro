﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="RefundReasonMaster.aspx.cs" Inherits="Master_RefundReasonMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" Runat="Server">
     <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <%--<ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdCreateNew" runat="server" CssClass="btn" Text="Add New Facility" OnClick="cmdCreateNew_Click" />
                </a>
            </li>
        </ul>--%>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-table"></i>
                        Refaund Reason List
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <asp:Repeater ID="rptPos" runat="server">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>REASON</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("rrm_id") %></td>
                                <td><%# Eval("rrm_reason") %></td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdDlt" runat="server" CssClass="btn btn-inverse" CommandName="DELETE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "rrm_id") %>' OnClick="cmdDlt_Click" rel="tooltip" data-placement="top" title="DELETE"><i class="icon-trash"></i> </asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Reason Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <div class="control-group">
                            <label for="txtCode" class="control-label">Reason For Reafund</label>
                            <div class="controls">
                                <asp:TextBox ID="txtreason" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" CssClass="btn btn-primary" runat="server" OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>