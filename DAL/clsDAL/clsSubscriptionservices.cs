﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clssubscriptionservices
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clssubscriptionservices()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clssubscriptionservices()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _srvc_id; }
            set
            {
                _srvc_id = value;
                if (clsCMD.Parameters.Contains("@srvc_id"))
                { clsCMD.Parameters["@srvc_id"].Value = value; }
                else { clsCMD.Parameters.Add("@srvc_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _UserID ;
        public Guid UserId
        {
            get { return _UserID; }
            set
            {
                _UserID = value;
                if (clsCMD.Parameters.Contains("@UserId"))
                { clsCMD.Parameters["@UserId"].Value = value; }
                else { clsCMD.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _srvc_id;
        public Int64 srvc_id
        {
            get { return _srvc_id; }
            set
            {
                _srvc_id = value;
                if (clsCMD.Parameters.Contains("@srvc_id"))
                { clsCMD.Parameters["@srvc_id"].Value = value; }
                else { clsCMD.Parameters.Add("@srvc_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _srvc_servicename;
        public String srvc_servicename
        {
            get { return _srvc_servicename; }
            set
            {
                _srvc_servicename = value;
                if (clsCMD.Parameters.Contains("@srvc_servicename"))
                { clsCMD.Parameters["@srvc_servicename"].Value = value; }
                else { clsCMD.Parameters.Add("@srvc_servicename", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private Int64 _srvc_periodval;
        public Int64 srvc_periodval
        {
            get { return _srvc_periodval; }
            set
            {
                _srvc_periodval = value;
                if (clsCMD.Parameters.Contains("@srvc_periodval"))
                { clsCMD.Parameters["@srvc_periodval"].Value = value; }
                else { clsCMD.Parameters.Add("@srvc_periodval", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _srvc_periodinterval;
        public String srvc_periodinterval
        {
            get { return _srvc_periodinterval; }
            set
            {
                _srvc_periodinterval = value;
                if (clsCMD.Parameters.Contains("@srvc_periodinterval"))
                { clsCMD.Parameters["@srvc_periodinterval"].Value = value; }
                else { clsCMD.Parameters.Add("@srvc_periodinterval", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Int64 _srvc_fixcharge;
        public Int64 srvc_fixcharge
        {
            get { return _srvc_fixcharge; }
            set
            {
                _srvc_fixcharge = value;
                if (clsCMD.Parameters.Contains("@srvc_fixcharge"))
                { clsCMD.Parameters["@srvc_fixcharge"].Value = value; }
                else { clsCMD.Parameters.Add("@srvc_fixcharge", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _srvc_recuringcharge;
        public Int64 srvc_recuringcharge
        {
            get { return _srvc_recuringcharge; }
            set
            {
                _srvc_recuringcharge = value;
                if (clsCMD.Parameters.Contains("@srvc_recuringcharge"))
                { clsCMD.Parameters["@srvc_recuringcharge"].Value = value; }
                else { clsCMD.Parameters.Add("@srvc_recuringcharge", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _srvc_usagescharge;
        public Int64 srvc_usagescharge
        {
            get { return _srvc_usagescharge; }
            set
            {
                _srvc_usagescharge = value;
                if (clsCMD.Parameters.Contains("@srvc_usagescharge"))
                { clsCMD.Parameters["@srvc_usagescharge"].Value = value; }
                else { clsCMD.Parameters.Add("@srvc_usagescharge", SqlDbType.BigInt).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                srvc_id = Convert.ToInt64(dr["srvc_id"]);
                srvc_servicename = Convert.ToString(dr["srvc_servicename"]);
                srvc_periodval = Convert.ToInt64(dr["srvc_periodval"]);
                srvc_periodinterval = Convert.ToString(dr["srvc_periodinterval"]);
                srvc_fixcharge = Convert.ToInt64(dr["srvc_fixcharge"]);
                srvc_recuringcharge = Convert.ToInt64(dr["srvc_recuringcharge"]);
                srvc_usagescharge = Convert.ToInt64(dr["srvc_usagescharge"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            srvc_id = Convert.ToInt64("0");
            srvc_servicename = Convert.ToString("NA");
            srvc_periodval = Convert.ToInt64("0");
            srvc_periodinterval = Convert.ToString("NA");
            srvc_fixcharge = Convert.ToInt64("0");
            srvc_recuringcharge = Convert.ToInt64("0");
            srvc_usagescharge = Convert.ToInt64("0");
            UserId = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.srvc_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_subscriptionservicesAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 srvc_id)
        {
            Boolean blnResult = false;
            this.srvc_id = srvc_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_subscriptionservicesAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 srvc_id)
        {
            Boolean blnResult = false;
            this.srvc_id = srvc_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_subscriptionservicesAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_subscriptionservicesGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetNotSubscribedPackages(Guid UsrId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.UserId = UsrId;
                clsCMD.CommandText = "sp_subscriptionservicesGetData";
                SetGetSPFlag = "NOTSUBSCRIBED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetServiceModules(string servicename)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.srvc_servicename = servicename;
                clsCMD.CommandText = "sp_subscriptionservicesGetData";
                SetGetSPFlag = "GETMODULES";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clssubscriptionservices_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clssubscriptionservices_PropertiesList> lstResult = new List<clssubscriptionservices_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].srvc_id = Convert.ToInt64(dr["srvc_id"]);
                lstResult[i].srvc_servicename = Convert.ToString(dr["srvc_servicename"]);
                lstResult[i].srvc_periodval = Convert.ToInt64(dr["srvc_periodval"]);
                lstResult[i].srvc_periodinterval = Convert.ToString(dr["srvc_periodinterval"]);
                lstResult[i].srvc_fixcharge = Convert.ToInt64(dr["srvc_fixcharge"]);
                lstResult[i].srvc_recuringcharge = Convert.ToInt64(dr["srvc_recuringcharge"]);
                lstResult[i].srvc_usagescharge = Convert.ToInt64(dr["srvc_usagescharge"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 srvc_id)
        {
            Boolean blnResult = false;
            try
            {
                this.srvc_id = srvc_id;
                clsCMD.CommandText = "sp_subscriptionservicesGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 srvc_id)
        {
            Boolean blnResult = false;
            try
            {
                this.srvc_id = srvc_id;
                clsCMD.CommandText = "sp_subscriptionservicesGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clssubscriptionservices_PropertiesList
    {
        public Int64 srvc_id { get; set; }
        public String srvc_servicename { get; set; }
        public Int64 srvc_periodval { get; set; }
        public String srvc_periodinterval { get; set; }
        public Int64 srvc_fixcharge { get; set; }
        public Int64 srvc_recuringcharge { get; set; }
        public Int64 srvc_usagescharge { get; set; }
    }
}