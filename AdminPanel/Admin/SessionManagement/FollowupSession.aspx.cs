﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using clsDAL;

public partial class Admin_SessionManagement_FollowupSession : System.Web.UI.Page
{
    clsClientFollowUpSession clsCFS = new clsClientFollowUpSession();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["sessionuserid"] != null)
            {
                binddata(new Guid(Session["sessionuserid"].ToString()));
            }
            if (MySession.Current.MESSAGE != string.Empty)
            {
                lblmessage.Text = MySession.Current.MESSAGE;
                msgdiv.Visible = true;
                MySession.Current.MESSAGE = string.Empty;
            }
        }
    }

    private void binddata(Guid clientid)
    {
        rptflwsession.DataSource = clsCFS.GetAllRecordByClient(clientid);
        rptflwsession.DataBind();
    }
    protected void rptflwsession_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = "EDIT";
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Admin/SessionManagement/FollowupSesionAddEdit.aspx");
        }
        if (e.CommandName == "VIEW")
        {
            Session["sessionid"]= new Guid(e.CommandArgument.ToString());
            Response.Redirect("~/Admin/SessionManagement/FollowupSesionView.aspx");
        }
            if (e.CommandName == "CLAIMCREATE")
        {
            Session["sessionid"] = new Guid(e.CommandArgument.ToString());
            Response.Redirect("~/Admin/ClaimManagment/ClaimAddEdit.aspx");
        }
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        Response.Redirect("~/Admin/SessionManagement/FollowupSesionAddEdit.aspx");
    }
    protected void cmdclaim_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/ClaimManagment/ClaimMaster.aspx");
    }
}
