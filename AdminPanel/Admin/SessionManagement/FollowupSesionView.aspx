﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="FollowupSesionView.aspx.cs" Inherits="Admin_SessionManagement_FollowupSesionView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Records</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <a>
                    <i class="icon-plus-sign"></i>
                    <asp:Button ID="cmdReturn" runat="server" CssClass="btn btn-primary" Text="Back to Session" OnClick="cmdReturn_Click" />
                </a>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:View ID="View1" runat="server"></asp:View>
    <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="row-fluid">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-edit"></i>Client Follow Up Session
                </h3>
            </div>
            <div class="box-content">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3><i class="icon-asterisk"></i><small>
                            <asp:Label ID="lblpataintname" runat="server"></asp:Label>'s</small> Session Detail</h3>
                    </div>
                    <div class="box-content nopadding">
                        <div class="form-horizontal form-column form-bordered">
                            <div class="row-fluid">
                                <table class="table table-hover table-nomargin table-bordered" style="width: 100%;">
                                    <tr>
                                        <th style="text-align: left; width: 30%">Pataint Name</th>
                                        <td>
                                            <asp:Label runat="server" ID="lblclientname"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Date</th>
                                        <td>
                                            <asp:Label runat="server" ID="lbldate"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Next Apoinment</th>
                                        <td>
                                            <asp:Label runat="server" ID="lblnextdate"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Location</th>
                                        <td>
                                            <asp:Label runat="server" ID="lbllocation"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Total Charge</th>
                                        <td>
                                            <asp:Label runat="server" ID="lbltotalcharge"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Facility</th>
                                        <td>
                                            <asp:Label runat="server" ID="lblfclty" Font-Bold="true"></asp:Label>
                                            <hr />
                                            <strong>Address :</strong>
                                            <br />
                                            <asp:Label runat="server" ID="lblfcltyaddress"></asp:Label>
                                            <br />
                                            <br />
                                            <strong>Phone :</strong>
                                            <asp:Label runat="server" ID="lblfcltyphone"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Payer</th>
                                        <td>
                                            <asp:Label runat="server" ID="lblinsname" Font-Bold="true"></asp:Label>
                                            <hr />
                                            <strong>Address :</strong>
                                            <br />
                                            <asp:Label runat="server" ID="lblinsaddress"></asp:Label>
                                            <br />
                                            <br />
                                            <strong>Phone :</strong>
                                            <asp:Label runat="server" ID="lblinsphone"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Dignosis Detail</th>
                                        <td>
                                            <asp:Repeater ID="rptDgndtail" runat="server">
                                                <HeaderTemplate>
                                                    <table class="table table-hover table-nomargin table-condensed table-bordered" style="width: 100%;">
                                                        <thead>
                                                            <tr>
                                                                <th>CPT Code</th>
                                                                <th>Description</th>
                                                                <th>Charge</th>
                                                                <th>Mod A</th>
                                                                <th>Mod B</th>
                                                                <th>Mod C</th>
                                                                <th>Mod D</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="text-align: center"><%# Eval("sdp_cptcode") %></td>
                                                        <td style="text-align: center"><%# Eval("cd_description") %></td>
                                                        <td style="text-align: center"><%# String.Format("{0:C}",Eval("sdp_linecharge")) %></td>
                                                        <td style="text-align: center"><%# Eval("sdp_moda") %></td>
                                                        <td style="text-align: center"><%# Eval("sdp_modb") %></td>
                                                        <td style="text-align: center"><%# Eval("sdp_modc") %></td>
                                                        <td style="text-align: center"><%# Eval("sdp_modd") %></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </tbody>
                </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                    <!--Cash Deatil-->
                                    <tr>
                                        <th style="text-align: left; width: 30%">Reason For Visit</th>
                                        <td>
                                            <asp:Literal ID="ltlvisitreson" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Treatment Plan/ Goal</th>
                                        <td>
                                            <asp:Literal ID="ltltreatmentplan" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Behavior</th>
                                        <td>
                                            <asp:Literal ID="ltlbeahviour" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Interventation</th>
                                        <td>
                                            <asp:Literal ID="ltlinterventation" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Response</th>
                                        <td>
                                            <asp:Literal ID="ltlrlresp" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Progress</th>
                                        <td>
                                            <asp:Literal ID="ltlprogress" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">New Clinic Issue</th>
                                        <td>
                                            <asp:Literal ID="ltlnewclinicissue" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <!--Bill Deatil-->
                                    <tr>
                                        <th style="text-align: left; width: 30%">Total Charge</th>
                                        <td>
                                            <asp:Label runat="server" ID="lblttlcharge"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Client Paid</th>
                                        <td>
                                            <asp:Label runat="server" ID="lblclientpaid"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Insurance Paid</th>
                                        <td>
                                            <asp:Label runat="server" ID="lblinsurancepaid"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Adjustment</th>
                                        <td>
                                            <asp:Label runat="server" ID="lbladjustment"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Total Recieved</th>
                                        <td>
                                            <asp:Label runat="server" ID="lbltotalreceved"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left; width: 30%">Total Balance Due</th>
                                        <td>
                                            <asp:Label runat="server" ID="lblbaldue"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="form-actions">
                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-green" runat="server" OnClick="cmdClmSbmt_Click"><i class="glyphicon-print"></i> Print</asp:LinkButton>
                <asp:LinkButton ID="cmdClmSbmt" CssClass="btn btn-primary" runat="server" OnClick="cmdClmSbmt_Click"><i class="glyphicon-ok"></i> Create Claim</asp:LinkButton>
                <asp:LinkButton ID="cmdCancel" CssClass="btn" runat="server" OnClick="cmdReturn_Click"><i class="icon-remove-circle"></i> Back</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
