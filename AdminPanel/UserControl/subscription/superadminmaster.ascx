﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="superadminmaster.ascx.cs" Inherits="UserControl_subscription_adminmaster" %>
 <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
        <button class="close" data-dismiss="alert" type="button">×</button>
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div class="box box-color box-bordered">
        <div class="box-title">
            <h3>
                <i class="glyphicon-projector"></i>
                Subscribtion Master
            </h3>
        </div>
        <div class="box-content nopadding">
            <div class="row-fluid">
                <div class="span12">
                    <asp:Repeater ID="dtSubMaster" runat="server" OnItemCommand="dtSubMaster_ItemCommand" OnItemDataBound="dtSubMaster_ItemDataBound">
                        <HeaderTemplate>
                            <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                                <thead>
                                     <tr>
                                        <th>Package Name</th>
                                        <th>User Name</th>
                                         <th>Subscribed Date</th>
                                        <th>Mode</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("srvc_servicename") %></td>
                                <td><%# Eval("UserName") %></td>
                                <td><%# Eval("us_subscribeddate") %></td>
                                <td style="text-align: center">
                                    <asp:Label ID="dvf" runat="server" Text="Label" Visible="false" CssClass="label label-success"> ACTIVE</asp:Label>
                                    <asp:Label ID="dve" runat="server" Text="Label" Visible="false" CssClass="label label-important">DE-ACTIVE</asp:Label>
                                </td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="cmdShare" runat="server" CssClass="btn btn-primary" CommandName="SHARE" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "us_id") %>'><i class="icon-share"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>