﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsUserspermission
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsUserspermission()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsUserspermission()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _upm_id; }
            set
            {
                _upm_id = value;
                if (clsCMD.Parameters.Contains("@upm_id"))
                { clsCMD.Parameters["@upm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@upm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _upm_id;
        public Int64 upm_id
        {
            get { return _upm_id; }
            set
            {
                _upm_id = value;
                if (clsCMD.Parameters.Contains("@upm_id"))
                { clsCMD.Parameters["@upm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@upm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _upm_userid;
        public Guid upm_userid
        {
            get { return _upm_userid; }
            set
            {
                _upm_userid = value;
                if (clsCMD.Parameters.Contains("@upm_userid"))
                { clsCMD.Parameters["@upm_userid"].Value = value; }
                else { clsCMD.Parameters.Add("@upm_userid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Int64 _upm_objectid;
        public Int64 upm_objectid
        {
            get { return _upm_objectid; }
            set
            {
                _upm_objectid = value;
                if (clsCMD.Parameters.Contains("@upm_objectid"))
                { clsCMD.Parameters["@upm_objectid"].Value = value; }
                else { clsCMD.Parameters.Add("@upm_objectid", SqlDbType.BigInt).Value = value; }
            }
        }
        private Boolean _upm_all;
        public Boolean upm_all
        {
            get { return _upm_all; }
            set
            {
                _upm_all = value;
                if (clsCMD.Parameters.Contains("@upm_all"))
                { clsCMD.Parameters["@upm_all"].Value = value; }
                else { clsCMD.Parameters.Add("@upm_all", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _upm_new;
        public Boolean upm_new
        {
            get { return _upm_new; }
            set
            {
                _upm_new = value;
                if (clsCMD.Parameters.Contains("@upm_new"))
                { clsCMD.Parameters["@upm_new"].Value = value; }
                else { clsCMD.Parameters.Add("@upm_new", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _upm_edit;
        public Boolean upm_edit
        {
            get { return _upm_edit; }
            set
            {
                _upm_edit = value;
                if (clsCMD.Parameters.Contains("@upm_edit"))
                { clsCMD.Parameters["@upm_edit"].Value = value; }
                else { clsCMD.Parameters.Add("@upm_edit", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _upm_delete;
        public Boolean upm_delete
        {
            get { return _upm_delete; }
            set
            {
                _upm_delete = value;
                if (clsCMD.Parameters.Contains("@upm_delete"))
                { clsCMD.Parameters["@upm_delete"].Value = value; }
                else { clsCMD.Parameters.Add("@upm_delete", SqlDbType.Bit).Value = value; }
            }
        }
        private Boolean _upm_view;
        public Boolean upm_view
        {
            get { return _upm_view; }
            set
            {
                _upm_view = value;
                if (clsCMD.Parameters.Contains("@upm_view"))
                { clsCMD.Parameters["@upm_view"].Value = value; }
                else { clsCMD.Parameters.Add("@upm_view", SqlDbType.Bit).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                upm_id = Convert.ToInt64(dr["upm_id"]);
                upm_userid = new Guid(dr["upm_userid"].ToString());
                upm_objectid = Convert.ToInt64(dr["upm_objectid"]);
                upm_all = Convert.ToBoolean(dr["upm_all"]);
                upm_new = Convert.ToBoolean(dr["upm_new"]);
                upm_edit = Convert.ToBoolean(dr["upm_edit"]);
                upm_delete = Convert.ToBoolean(dr["upm_delete"]);
                upm_view = Convert.ToBoolean(dr["upm_view"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            upm_id = Convert.ToInt64("0");
            upm_userid = new Guid();
            upm_objectid = Convert.ToInt64("0");
            upm_all = Convert.ToBoolean(false);
            upm_new = Convert.ToBoolean(false);
            upm_edit = Convert.ToBoolean(false);
            upm_delete = Convert.ToBoolean(false);
            upm_view = Convert.ToBoolean(false);
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.upm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_userspermissionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 upm_id)
        {
            Boolean blnResult = false;
            this.upm_id = upm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_userspermissionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 upm_id)
        {
            Boolean blnResult = false;
            this.upm_id = upm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_userspermissionAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_userspermissionGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordByUserId(Guid Userid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.upm_userid = Userid;
                clsCMD.CommandText = "sp_userspermissionGetData";
                SetGetSPFlag = "BYUSER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsUserspermission_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsUserspermission_PropertiesList> lstResult = new List<clsUserspermission_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].upm_id = Convert.ToInt64(dr["upm_id"]);
                lstResult[i].upm_userid = new Guid(dr["upm_userid"].ToString());
                lstResult[i].upm_objectid = Convert.ToInt64(dr["upm_objectid"]);
                lstResult[i].upm_all = Convert.ToBoolean(dr["upm_all"]);
                lstResult[i].upm_new = Convert.ToBoolean(dr["upm_new"]);
                lstResult[i].upm_edit = Convert.ToBoolean(dr["upm_edit"]);
                lstResult[i].upm_delete = Convert.ToBoolean(dr["upm_delete"]);
                lstResult[i].upm_view = Convert.ToBoolean(dr["upm_view"]);
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 upm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.upm_id = upm_id;
                clsCMD.CommandText = "sp_userspermissionGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 upm_id)
        {
            Boolean blnResult = false;
            try
            {
                this.upm_id = upm_id;
                clsCMD.CommandText = "sp_userspermissionGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckRecords(Guid UserId,Int64 ObjectId)
        {
            Boolean blnResult = false;
            try
            {
                this.upm_userid = UserId;
                this.upm_objectid = ObjectId;
                clsCMD.CommandText = "sp_userspermissionGetData";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public DataTable GetAdminList(Int64 Id)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.upm_objectid = Id;
                clsCMD.CommandText = "sp_userspermissionGetData";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllAdmin()
        {
            DataTable dtResult = new DataTable();
            try
            {
                SetGetSPFlag = "GETALLADMIN";
                clsCMD.CommandText = "sp_userspermissionGetData";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }
        #endregion

    }
    public class clsUserspermission_PropertiesList
    {
        public Int64 upm_id { get; set; }
        public Guid upm_userid { get; set; }
        public Int64 upm_objectid { get; set; }
        public Boolean upm_all { get; set; }
        public Boolean upm_new { get; set; }
        public Boolean upm_edit { get; set; }
        public Boolean upm_delete { get; set; }
        public Boolean upm_view { get; set; }
    }
}