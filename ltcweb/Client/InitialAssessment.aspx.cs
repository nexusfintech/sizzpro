﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Client_InitialAssessment : System.Web.UI.Page
{
    string strMemberkey = "";
    Int64 intUserid;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["stage"] != null)
        {
            MembershipUser mu = Membership.GetUser(User.Identity.Name);
            Session["form_memberkey"] = mu.ProviderUserKey.ToString();
            if (Session["form_memberkey"] != null)
            {
                string strStage = Request.QueryString["stage"].ToString();
                strMemberkey = Session["form_memberkey"].ToString();
                intUserid = Convert.ToInt64(Session["form_userid"]);
                if (strStage == "1")
                { ucManageIntro.Visible = true; }
                if (strStage == "2")
                { ucManageProfile.Visible = true; }
                if (strStage == "3")
                { ucSpouse.Visible = true; }
                if (strStage == "4")
                { ucSibling.Visible = true; }
                if (strStage == "6")
                { ucChildren.Visible = true; }
                if (strStage == "5")
                { ucFinancial.Visible = true; }
                if (strStage == "6")
                { ucMedical.Visible = true; }
                if (strStage == "7")
                { ucMental.Visible = true; }
                if (strStage == "8")
                { ucPsychological.Visible = true; }
                if (strStage == "9")
                { ucAddiction.Visible = true; }
                if (strStage == "10")
                { ucFamily.Visible = true; }
                if (strStage == "11")
                { ucProblem.Visible = true; }
                if (strStage == "12")
                { ucReadiness.Visible = true; }
            }
            else { Response.Redirect("~/Client/Dashboard.aspx"); }
        }
        else { Response.Redirect("~/Client/Dashboard.aspx"); }
    }
}