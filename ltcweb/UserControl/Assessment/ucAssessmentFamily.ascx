﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAssessmentFamily.ascx.cs" Inherits="UserControl_Assessment_AssessmentFamily" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-group"></i>
                    Personal History
                </h3>
            </div>
            <div class="box-content">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="control-group">
                        <label for="txtraised" class="control-label">Check all that applied during your childhood</label>
                        <div class="controls">
                            <div class="span12">
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkHappychildhood" runat="server" Text="Happy childhood" /></td>
                                        <td>
                                            <asp:CheckBox ID="chkEmotionalproblems" runat="server" Text="Emotional problems" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkAlcoholabuse" runat="server" Text="Alcohol abuse" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkFamilyproblems" runat="server" Text="Family problems" /></td>
                                        <td>
                                            <asp:CheckBox ID="chkEatingdisorder" runat="server" Text="Eating disorder" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkDrugabuse" runat="server" Text="Drug abuse" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkSexuallyabused" runat="server" Text="Sexually abused" /></td>
                                        <td>
                                            <asp:CheckBox ID="chkPhysicallyabused" runat="server" Text="Physically abused" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkAcademicschoolproblems" runat="server" Text="Academic/school problems" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkFinancialproblems" runat="server" Text="Financial problems" /></td>
                                        <td>
                                            <asp:CheckBox ID="chkLegaltrouble" runat="server" Text="Legal trouble" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkOtherPleaseexplain" runat="server" Text="Other (Please explain):" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtchildrenotherexplain" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkBehaviorproblems" runat="server" Text="Behavior problems" /></td>
                                        <td>
                                            <asp:CheckBox ID="chkUnhappychildhood" runat="server" Text="Unhappy childhood" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="span12">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtfamilybrother" class="control-label">Brothers</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtfamilybrother" runat="server" placeholder="Brother" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtfamilySisters" class="control-label">Sisters</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtfamilySisters" runat="server" placeholder="Sister" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtBirthOrderBio" class="control-label">BirthOrder(Bio)</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtBirthOrderBio" runat="server" placeholder="BirthOrder(Bio)" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="txtStepSiblings" class="control-label">Step Siblings</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtStepSiblings" runat="server" placeholder="Step sibling" class="complexify-me"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="txtraised" class="control-label">Do you have step-parents?</label>
                        <div class="controls">
                            <table style="width:100%">
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="rdostepperents" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td>
                                        <asp:CheckBox ID="chkfamilyFather" runat="server" Text="Father" /></td>
                                    <td>
                                        <asp:CheckBox ID="chkfamilymother" runat="server" Text="Mother" /></td>
                                    <td>
                                        <asp:CheckBox ID="chkfamilyboth" runat="server" Text="Both" /></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="span12">
                        <label>Describe your biological parents’ relationship</label>
                        <asp:TextBox ID="txtdescbiologiparentrel" runat="server" TextMode="MultiLine" Width="98%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Describe your relationship with your parents</label>
                        <asp:TextBox ID="txtdecrelwithparent" runat="server" TextMode="MultiLine" Width="98%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Give a brief description of your family of origin's story</label>
                        <asp:TextBox ID="txtbrifdescfamorigion" runat="server" TextMode="MultiLine" Width="98%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Describe your friendships and how supportive they are</label>
                        <asp:TextBox ID="txtDescfrindsupport" runat="server" TextMode="MultiLine" Width="98%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>How would you describe your status in the community?</label>
                        <asp:TextBox ID="txtdescstscommunity" runat="server" TextMode="MultiLine" Width="98%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Briefly describe your romantic relationship history</label>
                        <asp:TextBox ID="txtbrifdescromrelhistory" runat="server" TextMode="MultiLine" Width="98%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <label>Please any significant family or personal history that may be important to understanding you better</label>
                        <asp:TextBox ID="txtsigfamperunderbetter" runat="server" TextMode="MultiLine" Width="98%"></asp:TextBox>
                    </div>
                    <div class="span12">
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
