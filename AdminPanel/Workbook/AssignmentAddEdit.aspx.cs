﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Data;
using System.Web.Security;

public partial class Workbook_AssignmentAddEdit : System.Web.UI.Page
{
    clsAssignments clsASN = new clsAssignments();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsMembership mem = new clsMembership();
    clsobjectsharing clsOBS = new clsobjectsharing();

    private void EditEntry()
    {
        Boolean blnResult = clsASN.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnResult == true)
        {
            txtDescription.Text = clsASN.asn_description;
            txtAssignmentName.Text = clsASN.asn_name;
            txtshort.Text = clsASN.asn_short;
            ListItem liInterval = ddlSelectMember.Items.FindByValue(clsASN.asn_UserId.ToString());
            if (liInterval != null)
            {
                ddlSelectMember.SelectedIndex = ddlSelectMember.Items.IndexOf(liInterval);
            }
            asncd.Value = clsASN.asn_code.ToString();
            asnsts.Value = clsASN.asn_sts.ToString();
        }
        else { Response.Redirect("~/Workbook/ChapterMaster.aspx"); }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (MySession.Current.AddEditFlag == "null")
            {
                Response.Redirect("~/Workbook/AssignmentMaster.aspx");
            }
            if (User.IsInRole("administrator"))
            {
                BindCombo();
            }
            if (MySession.Current.AddEditFlag == "EDIT")
            { 
                EditEntry();
            }
        }
    }

    private void BindCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllCombo();
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }
    private string PageValidation()
    {
        string strResult = "";
        if (txtAssignmentName.Text.Trim() == "")
        { strResult += "Please Input Assignment Name <br/>"; }
        if (txtshort.Text.Trim() == "")
        { strResult += "Please Input Assignment short <br/>"; }
        return strResult;
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Boolean blnSave = false;
        string strResult = PageValidation();
        if (strResult != "")
        { lblmessage.Text = strResult; return; }
        clsASN.asn_description = txtDescription.Text;
        clsASN.asn_name = txtAssignmentName.Text;
        clsASN.asn_short = txtshort.Text;
        if (User.IsInRole("administrator"))
        {
            clsASN.asn_UserId = new Guid(ddlSelectMember.SelectedValue.ToString());
        }
        else
        {
            clsASN.asn_UserId = (Guid)Membership.GetUser().ProviderUserKey;
        }
        clsASN.asn_Owner = mem.SetOwner(clsASN.asn_UserId);
        if (MySession.Current.AddEditFlag == "ADD")
        {
            clsASN.asn_code = Guid.NewGuid();
            blnSave = clsASN.Save();
            clsASN.GetRecordByCodeInProperties(clsASN.asn_code);
            clsOBS.shr_sharedobjid = clsASN.asn_id;
            clsOBS.shr_object = "ASSIGNMENT";
            clsOBS.shr_ownerid = clsASN.asn_UserId;
            clsOBS.shr_sharedwithid = clsASN.asn_UserId;
            clsOBS.Save();
        }
        else
        {
            clsASN.asn_sts = Convert.ToBoolean(asnsts.Value);
            clsASN.asn_code=new Guid(asncd.Value);
            blnSave = clsASN.Update(MySession.Current.EditID);
        }
        if (blnSave == true)
        {
            Response.Redirect("~/Workbook/AssignmentMaster.aspx");
        }
        else { lblmessage.Text = "Error in save ..." + clsASN.GetSetErrorMessage; }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Workbook/AssignmentMaster.aspx");
    }
}