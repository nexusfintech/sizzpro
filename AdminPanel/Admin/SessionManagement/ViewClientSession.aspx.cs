﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Admin_SessionManagement_ViewClientSession : System.Web.UI.Page
{
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsClientSession clsSSN = new clsClientSession();

    private void BindCombo()
    {
        ddlClient.DataTextField = "UserName";
        ddlClient.DataValueField = "UserId";
        ddlClient.DataSource = clsCPM.GetAllComboByRole("Client");
        ddlClient.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["sessionfilter"] != null)
        {
            if (Session["sessionfilter"] == "clntwise")
            { pnlClientWise.Visible = true; }
            if (Session["sessionfilter"] == "datewise")
            { pnlDateWise.Visible = true; }
            if (Session["sessionfilter"] == "faciwise")
            { pnlFaciWise.Visible = true; }
            if (Session["sessionfilter"] == "provwise")
            { pnlProvWise.Visible = true; }
            if (Session["sessionfilter"] == "itemwise")
            { pnlItemWise.Visible = true; }
        }
        else { pnlClientWise.Visible = true; }
        if (!Page.IsPostBack)
        {
            BindCombo();
        }

    }

    protected void cmdClntWise_Click(object sender, EventArgs e)
    {
        Session["sessionfilter"] = "clntwise";
        Response.Redirect("~/Admin/SessionManagement/ViewClientSession.aspx");
    }
    protected void cmdDateWise_Click(object sender, EventArgs e)
    {
        Session["sessionfilter"] = "datewise";
        Response.Redirect("~/Admin/SessionManagement/ViewClientSession.aspx");
    }
    protected void cmdItemWise_Click(object sender, EventArgs e)
    {
        Session["sessionfilter"] = "itemwise";
        Response.Redirect("~/Admin/SessionManagement/ViewClientSession.aspx");
    }
    protected void cmdProvWise_Click(object sender, EventArgs e)
    {
        Session["sessionfilter"] = "provwise";
        Response.Redirect("~/Admin/SessionManagement/ViewClientSession.aspx");
    }
    protected void cmdFaciWise_Click(object sender, EventArgs e)
    {
        Session["sessionfilter"] = "faciwise";
        Response.Redirect("~/Admin/SessionManagement/ViewClientSession.aspx");
    }
    protected void cmdClientWisefilter_Click(object sender, EventArgs e)
    {
        if (ddlClient.SelectedIndex == -1)
        {
            lblmessage.Text = "Please Select Client";
            msgdiv.Visible = true;
            return;
        }
        Guid clientid =  new Guid(ddlClient.SelectedItem.Value);
        clsSSN.ssn_clientid = clientid; 
        dtDisplay.DataSource = clsSSN.GetAllRecordClientWise();
        dtDisplay.DataBind();
    }
    protected void cmdDateWiseFilter_Click(object sender, EventArgs e)
    {
        string[] strDate = txtDateRange.Text.Split('-');
        int intvarLength = strDate.Length;
        DateTime dtFromDate;
        if (!(DateTime.TryParseExact(strDate[0], "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtFromDate)))
        {
            lblmessage.Text = "Invalid From Date format please enter in mm/dd/yyyy";
            return;
        }

        DateTime dtToDate;
        if (!(DateTime.TryParseExact(strDate[1], "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtToDate)))
        {
            lblmessage.Text = "Invalid To Date format please enter in mm/dd/yyyy";
            return;
        }

        dtFromDate.AddHours(0);
        dtFromDate.AddMinutes(0);
        dtFromDate.AddSeconds(0);
        dtToDate.AddDays(1);
        dtToDate.AddHours(0);
        dtToDate.AddMinutes(0);
        dtToDate.AddSeconds(0);
        dtDisplay.DataSource = clsSSN.GetAllRecordDateRangeWise(dtFromDate,dtToDate);
        dtDisplay.DataBind();
    }
    protected void cmdItemWiseFilter_Click(object sender, EventArgs e)
    {
        if(ddlItem.SelectedIndex == -1)
        {
            lblmessage.Text = "Please Select Any Item";
            msgdiv.Visible = true;
            return;
        }
        Int64 intprValue = Convert.ToInt64(ddlItem.SelectedItem.Value);
        dtDisplay.DataSource = clsSSN.GetAllRecordItemWise(intprValue);
        dtDisplay.DataBind();
    }
    protected void cmdProvWiseFilter_Click(object sender, EventArgs e)
    {
        if (ddlProvider.SelectedIndex == -1)
        {
            lblmessage.Text = "Please Select Any Provider";
            msgdiv.Visible = true;
            return;
        }
        Int64 intprValue = Convert.ToInt64(ddlProvider.SelectedItem.Value);
        dtDisplay.DataSource = clsSSN.GetAllRecordProviderWise(intprValue);
        dtDisplay.DataBind();
    }
    protected void cmdFaciWiseFilter_Click(object sender, EventArgs e)
    {
        if (ddlFacilities.SelectedIndex == -1)
        {
            lblmessage.Text = "Please Select Any Facality";
            msgdiv.Visible = true;
            return;
        }
        Int64 intprValue = Convert.ToInt64(ddlFacilities.SelectedItem.Value);
        dtDisplay.DataSource = clsSSN.GetAllRecordFacilitiesWise(intprValue);
        dtDisplay.DataBind();
    }
}