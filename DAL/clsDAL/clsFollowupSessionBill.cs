﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsFollowupSessionBill
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsFollowupSessionBill()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsFollowupSessionBill()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _fsb_id; }
            set
            {
                _fsb_id = value;
                if (clsCMD.Parameters.Contains("@fsb_id"))
                { clsCMD.Parameters["@fsb_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fsb_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _fsb_id;
        public Int64 fsb_id
        {
            get { return _fsb_id; }
            set
            {
                _fsb_id = value;
                if (clsCMD.Parameters.Contains("@fsb_id"))
                { clsCMD.Parameters["@fsb_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fsb_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _fsb_sessionid;
        public Guid fsb_sessionid
        {
            get { return _fsb_sessionid; }
            set
            {
                _fsb_sessionid = value;
                if (clsCMD.Parameters.Contains("@fsb_sessionid"))
                { clsCMD.Parameters["@fsb_sessionid"].Value = value; }
                else { clsCMD.Parameters.Add("@fsb_sessionid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Double _fsb_totalcharged;
        public Double fsb_totalcharged
        {
            get { return _fsb_totalcharged; }
            set
            {
                _fsb_totalcharged = value;
                if (clsCMD.Parameters.Contains("@fsb_totalcharged"))
                { clsCMD.Parameters["@fsb_totalcharged"].Value = value; }
                else { clsCMD.Parameters.Add("@fsb_totalcharged", SqlDbType.Money).Value = value; }
            }
        }
        private Double _fsb_clientpayment;
        public Double fsb_clientpayment
        {
            get { return _fsb_clientpayment; }
            set
            {
                _fsb_clientpayment = value;
                if (clsCMD.Parameters.Contains("@fsb_clientpayment"))
                { clsCMD.Parameters["@fsb_clientpayment"].Value = value; }
                else { clsCMD.Parameters.Add("@fsb_clientpayment", SqlDbType.Money).Value = value; }
            }
        }
        private Double _fsb_insurancepayment;
        public Double fsb_insurancepayment
        {
            get { return _fsb_insurancepayment; }
            set
            {
                _fsb_insurancepayment = value;
                if (clsCMD.Parameters.Contains("@fsb_insurancepayment"))
                { clsCMD.Parameters["@fsb_insurancepayment"].Value = value; }
                else { clsCMD.Parameters.Add("@fsb_insurancepayment", SqlDbType.Money).Value = value; }
            }
        }
        private Double _fsb_totalrecieved;
        public Double fsb_totalrecieved
        {
            get { return _fsb_totalrecieved; }
            set
            {
                _fsb_totalrecieved = value;
                if (clsCMD.Parameters.Contains("@fsb_totalrecieved"))
                { clsCMD.Parameters["@fsb_totalrecieved"].Value = value; }
                else { clsCMD.Parameters.Add("@fsb_totalrecieved", SqlDbType.Money).Value = value; }
            }
        }
        private Double _fsb_writeoff;
        public Double fsb_writeoff
        {
            get { return _fsb_writeoff; }
            set
            {
                _fsb_writeoff = value;
                if (clsCMD.Parameters.Contains("@fsb_writeoff"))
                { clsCMD.Parameters["@fsb_writeoff"].Value = value; }
                else { clsCMD.Parameters.Add("@fsb_writeoff", SqlDbType.Money).Value = value; }
            }
        }
        private Double _fsb_balancedue;
        public Double fsb_balancedue
        {
            get { return _fsb_balancedue; }
            set
            {
                _fsb_balancedue = value;
                if (clsCMD.Parameters.Contains("@fsb_balancedue"))
                { clsCMD.Parameters["@fsb_balancedue"].Value = value; }
                else { clsCMD.Parameters.Add("@fsb_balancedue", SqlDbType.Money).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                fsb_id = Convert.ToInt64(dr["fsb_id"]);
                fsb_sessionid = new Guid(dr["fsb_sessionid"].ToString());
                fsb_totalcharged = Convert.ToDouble(dr["fsb_totalcharged"]);
                fsb_clientpayment = Convert.ToDouble(dr["fsb_clientpayment"]);
                fsb_insurancepayment = Convert.ToDouble(dr["fsb_insurancepayment"]);
                fsb_totalrecieved = Convert.ToDouble(dr["fsb_totalrecieved"]);
                fsb_writeoff = Convert.ToDouble(dr["fsb_writeoff"]);
                fsb_balancedue = Convert.ToDouble(dr["fsb_balancedue"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            fsb_id = Convert.ToInt64("0");
            fsb_sessionid = new Guid();
            fsb_totalcharged = Convert.ToDouble("0");
            fsb_clientpayment = Convert.ToDouble("0");
            fsb_insurancepayment = Convert.ToDouble("0");
            fsb_totalrecieved = Convert.ToDouble("0");
            fsb_writeoff = Convert.ToDouble("0");
            fsb_balancedue = Convert.ToDouble("0");
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.fsb_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_FollowupSessionBillAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                {
                    blnResult = true; 
                    GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; 
                }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 fsb_id)
        {
            Boolean blnResult = false;
            this.fsb_id = fsb_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_FollowupSessionBillAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 fsb_id)
        {
            Boolean blnResult = false;
            this.fsb_id = fsb_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_FollowupSessionBillAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_FollowupSessionBillGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsFollowupSessionBill_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsFollowupSessionBill_PropertiesList> lstResult = new List<clsFollowupSessionBill_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].fsb_id = Convert.ToInt64(dr["fsb_id"]);
                lstResult[i].fsb_sessionid = new Guid(dr["fsb_sessionid"].ToString());
                lstResult[i].fsb_totalcharged = Convert.ToDouble(dr["fsb_totalcharged"]);
                lstResult[i].fsb_clientpayment = Convert.ToDouble(dr["fsb_clientpayment"]);
                lstResult[i].fsb_insurancepayment = Convert.ToDouble(dr["fsb_insurancepayment"]);
                lstResult[i].fsb_totalrecieved = Convert.ToDouble(dr["fsb_totalrecieved"]);
                lstResult[i].fsb_writeoff = Convert.ToDouble(dr["fsb_writeoff"]);
                lstResult[i].fsb_balancedue = Convert.ToDouble(dr["fsb_balancedue"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 fsb_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fsb_id = fsb_id;
                clsCMD.CommandText = "sp_FollowupSessionBillGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean GetRecordBySessionId(Guid sessionid)
        {
            Boolean blnResult = false;
            try
            {
                this.fsb_sessionid = sessionid;
                clsCMD.CommandText = "sp_FollowupSessionBillGetData";
                SetGetSPFlag = "BYSSNID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                {
                    blnResult = SetValueToProperties(dtResult.Rows[0]); 
                }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 fsb_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fsb_id = fsb_id;
                clsCMD.CommandText = "sp_FollowupSessionBillGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsFollowupSessionBill_PropertiesList
    {
        public Int64 fsb_id { get; set; }
        public Guid fsb_sessionid { get; set; }
        public Double fsb_totalcharged { get; set; }
        public Double fsb_clientpayment { get; set; }
        public Double fsb_insurancepayment { get; set; }
        public Double fsb_totalrecieved { get; set; }
        public Double fsb_writeoff { get; set; }
        public Double fsb_balancedue { get; set; }
    }
}