﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using clsDAL;

public partial class Subscription_PackageSubscriptionMaster : System.Web.UI.Page
{
    clsuserpackagesubscribtion clsUPS = new clsuserpackagesubscribtion();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            
        }
    }
    protected void dtSubMaster_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;
            Boolean Isact = Convert.ToBoolean(DataBinder.Eval(DI, "us_status"));
            if (Isact)
            {
                var c = e.Item.FindControl("dvf");
                c.Visible = true;
            }
            else
            {
                var c = e.Item.FindControl("dve");
                c.Visible = true;
            }
        }
    }
    //private void Binddata()
    //{ 
    //    DataTable dtRslt=new DataTable();
    //    dtRslt = clsUPS.GetAllRecord();
    //    dtSubMaster.DataSource = dtRslt;
    //    dtSubMaster.DataBind();
    //}
    protected void dtSubMaster_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Subscription/PackageSubscriptionAddEdit.aspx");
    }
}