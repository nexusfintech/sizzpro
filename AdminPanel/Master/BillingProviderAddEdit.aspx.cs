﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
public partial class Master_BillingProviderAddEdit : System.Web.UI.Page
{
    clsBillingproviderMaster clsBPM = new clsBillingproviderMaster();
    clsZipMaster clszip = new clsZipMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindstate();
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                loaddata();
            }
        }
    }
    private void loaddata()
    {
        Boolean blnRslt = clsBPM.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {

            txtname.Text = clsBPM.bpm_name;
            txtaddress.Text = clsBPM.bpm_address;
            txtcity.Text = clsBPM.bpm_city;
            txtpincode.Text = clsBPM.bpm_zip;
            txtphn.Text = clsBPM.bpm_phone;
            txtfax.Text = clsBPM.bpm_fax;
            txtTaxid.Text = clsBPM.bpm_taxid;
            ListItem lstst = ddlState.Items.FindByValue(clsBPM.bpm_state.ToString());
            if (lstst != null)
            {
                ddlState.SelectedIndex = ddlState.Items.IndexOf(lstst);
            }
        }
        else
        {
            MySession.Current.MESSAGE = clsBPM.GetSetErrorMessage;
            Response.Redirect("~/Master/BillingProviderMaster.aspx");
        }
    }
    private void bindstate()
    {
        ddlState.Items.Clear();
        ddlState.SelectedIndex = -1;
        ddlState.SelectedValue = null;
        ddlState.ClearSelection();
        ddlState.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<< -- Pleasse Select -- >>";
        lst.Selected = true;
        ddlState.Items.Add(lst);
        ddlState.DataSource = clszip.GetAllByMasterFlag("STATE");
        ddlState.DataTextField = "zip_name";
        ddlState.DataValueField = "zip_id";
        ddlState.DataBind();
    }
    private string validatepage()
    {
        string result = string.Empty;
        if (txtname.Text == string.Empty)
        {
            result = "Provider Name Required";
            return result;
        }
        else if (txtaddress.Text == string.Empty)
        {
            result = "Provider Address Required";
            return result;
        }
        else if (txtcity.Text == string.Empty)
        {
            result = "Provider City Required";
            return result;
        }
        else if (ddlState.SelectedIndex == 0)
        {
            result = "Provider State Required";
            return result;
        }
        else if (txtpincode.Text == string.Empty)
        {
            result = "Provider PinCode Required";
            return result;
        }
        else
        {
            return result;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string vldtreslt = validatepage();
        if (vldtreslt != string.Empty)
        {
            lblmessage.Text = vldtreslt;
            msgdiv.Visible = true;
            return;
        }

        clsBPM.bpm_name = txtname.Text;
        clsBPM.bpm_taxid = txtTaxid.Text;
        clsBPM.bpm_address = txtaddress.Text;
        clsBPM.bpm_city = txtcity.Text;
        clsBPM.bpm_state = Convert.ToInt64(ddlState.SelectedValue);
        clsBPM.bpm_zip = txtpincode.Text;
        clsBPM.bpm_phone = txtphn.Text;
        clsBPM.bpm_fax = txtfax.Text;
        clsBPM.bpm_userid =(Guid)Membership.GetUser().ProviderUserKey;
        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnRslt = clsBPM.Save();
        }
        else
        {
            blnRslt = clsBPM.Update(MySession.Current.EditID);
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Successfully";
            Response.Redirect("~/Master/BillingProviderMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsBPM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/BillingProviderMaster.aspx");
    }
}