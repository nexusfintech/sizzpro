﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using clsDAL;

public partial class Workbook_ProgrammWorkbook : System.Web.UI.Page
{
    clsProgramToWorkbook clsPTW = new clsProgramToWorkbook();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddata();
        }
    }
    public void binddata()
    {
        if (MySession.Current.EditID != 0)
        {
            dtPrgWk.DataSource=clsPTW.GetProgrammWorkbook(MySession.Current.EditID);
            dtPrgWk.DataBind();
        }
        else
        {
            Response.Redirect("~/Workbook/ProgrammClient.aspx");
        }
    }
    protected void lnkChps_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn=(LinkButton)sender;
        MySession.Current.EditID = Convert.ToInt64(lnkbtn.CommandArgument);
        Response.Redirect("~/Workbook/WorkbookChapter.aspx");
    }
}