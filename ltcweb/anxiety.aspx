﻿<%@ Page Title="anxiety" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="anxiety.aspx.cs" Inherits="anxiety" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-question"></i>&nbsp;Anxiety</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <p>Anxiety is a normal reaction to life’s stressors.  We all experience anxiety as indicated by physiological (physical) symptoms such as “butterflies” in our stomach, increased heart rate, elevated blood pressure, sweating and a general feeling of dread.  Yet, when these physiological symptoms become unmanageable, they can produce headaches, shortness of breath, muscle weakness, nausea, chest pains, a feeling of doom, and outright panic.  This level of anxiety is often referred to as a panic disorder or anxiety disorder.  It can be produced by a number of stimuli ranging from test anxiety to phobias.  It can be chronic (long-term) or (acute) short-term.  Regardless of the cause, anxiety is a problem that can be treated with a variety of therapies and/or medications.</p>
        </div>
        <h5>How do I know if I have an Anxiety Disorder?</h5>
        <p>Anxiety, in its many forms, is most often diagnosed with the use of one of several research based professional assessment tools such as the Beck Anxiety Inventory®, The Minnesota Multiphasic Personality Inventory (MMPI-2)®, or The Hamilton Anxiety Scale (HAM-A)®.</p>
        <p>The Diagnostic and Statistical Manual of Mental Disorders (DSM) identifies eleven basic subtypes of anxiety disorders, most of which are addressed in separate sections. Criteria for generalized anxiety are as follows:</p>
        <p>A. At least 6 months of "excessive anxiety and worry" about a variety of events and situations. Generally, "excessive" can be interpreted as more than would be expected for a particular situation or event. Most people become anxious over certain things, but the intensity of the anxiety typically corresponds to the situation.</p>
        <p>B. There is significant difficulty in controlling the anxiety and worry. If someone has a very difficult struggle to regain control, relax, or cope with the anxiety and worry, then this requirement is met.</p>
        <p>C. The presence for most days over the previous six months of 3 or more (only 1 for children) of the following symptoms:</p>
        <p>1. Feeling wound-up, tense, or restless</p>
        <p>2. Easily becoming fatigued or worn-out</p>
        <p>3. Concentration problems</p>
        <p>4. Irritability</p>
        <p>5. Significant tension in muscles</p>
        <p>6. Difficulty with sleep</p>
        <p>D. The symptoms are not part of another mental disorder.</p>
        <p>E. The symptoms cause "clinically significant distress" or problems functioning in daily life. "Clinically significant" is the part that relies on the perspective of the treatment provider. Some people can have many of the aforementioned symptoms and cope with them well enough to maintain a high level of functioning.</p>
        <p>F. The condition is not due to a substance use/ abuse or medical issue. It should be noted that anxiety can also be an indicator of a more serious disorder including PTSD or Post Traumatic Stress Disorder.  Therefore, it is very important to talk with a mental health professional in order to receive a proper assessment of your condition.</p>
        <h5>How is Anxiety Treated</h5>
        <p>Anxiety can be treated with medication, therapy or a combination of both.  Your choice of treatment should take into consideration other medications you currently use, the results of your professional assessment, and whether you desire a permanent or “quick” fix.  Medication alone can be a very useful tool to combat the symptoms of anxiety for many people.  However, it does not address the source, only the symptoms.  Most medications prescribed for the treatment of anxiety are for short-term usage only.  Therefore, if the issue which causes the anxiety is not treated, the symptoms will most often return once the medication is stopped.  This is why it is important to consider seeking professional assistance from a trained therapist when dealing with anxiety.  He or she will work with you to determine the cause of your anxiety and using your personal resources, help you to overcome the problem.  </p>
        <p>There are numerous therapy methods that research has shown to be ideal for the treatment of anxiety.  Some of these are the Cognitive-Behavioral approach, Rational Emotive Behavior Therapy, Relaxation Therapy, and deep breathing techniques.  Lifestyle analysis and change recommendations may also be included in your treatment plan.  </p>
        <p>For more information on Anxiety, visit <a href="http://www.webmd.com/anxiety-panic/guide/mental-health-anxiety-disorders">http://www.webmd.com/anxiety-panic/guide/mental-health-anxiety-disorders</a>.</p>
        <p>
            Other Resources
          <a href="http://www.nimh.nih.gov/health/topics/anxiety-disorders/index.shtml">National Institute of Mental  Health (NIMH): Anxiety</a>
        </p>
        <p><a href="http://www.adaa.org/">Anxiety Disorders Association of  America</a> </p>
        <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

