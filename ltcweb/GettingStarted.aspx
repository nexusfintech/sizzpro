﻿<%@ Page Title="Getting Started" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GettingStarted.aspx.cs" Inherits="GettingStarted" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-play"></i>&nbsp;Getting Started</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <h4>How to Get Started</h4>
            <p>If you are seeking counseling at Lifestyle Therapy and Coaching, please follow the steps below. </p>
            <ol type="1" start="1">
                <li>
                    <p>Complete our Intake form below.</p>
                    <p><strong><a href="/downloads/INTAKE_FORM.doc" target="_blank">Intake Form - WORD VERSION</a></strong> (Right click | Save link as)</p>
                    <p><strong><a href="/downloads/INTAKE_FORM.pdf" target="_blank">Intake Form - PDF</a></strong> <a href="/downloads/INTAKE_FORM.pdf"><strong>VERSION</strong></a></p>
                </li>
            </ol>
            <ol start="2">
                <li>
                    <div>You can call our scheduling service at <strong>(256) 850-4426</strong> to schedule an appointment or use our <a href="Client/Appointment.aspx">online scheduler</a> to make your first appointment.</div>
                </li>
            </ol>
            <div><strong>N</strong><strong>ote</strong>: If your appointment is in our Huntsville office please come to  the rear of the suite. Please wait in the front or rear lobby for your counselor to meet you at the time of your session.</div>
            <p><strong>Lifestyle Therapy and Coaching</strong></p>
            <p><strong>4801 University Square NW Suite 13</strong></p>
            <p><strong>Huntsville, AL 35816</strong></p>
            <p><strong>FAX: (925) 522-4887&nbsp;</strong></p>
            <p><strong><a href="http://maps.google.com/maps?hl=en&q=4801+University+Square+NW+Suite+13+Huntsville,+AL+35816&bav=on.2,or.r_gc.r_pw.,cf.osb&biw=1366&bih=659&um=1&ie=UTF-8&hq=&hnear=0x88626be46839c899:0xc38314422e9e53d,4801+University+Square+%2313,+Huntsville,+AL+35816&gl=us&ei=cBDTTqX9O5GhtwfPk9WpDQ&sa=X&oi=geocode_result&ct=title&resnum=1&ved=0CBwQ8gEwAA" target="_blank">Map</a></strong></p>
            <strong>
                <br />
                <br />
                Phone: 1-256-850-4426<br>
                FAX:   1-925-522-4887</strong><p><a href="mailto:admin@lifestyletherapycoach.com"><strong>admin@LifestyleTherapyCoach.com</strong></a></p>
            <h4>Referral Forms</h4>
            <p>Release of Information - <a href="/downloads/Release_of_Information_Update.pdf" target="_blank">PDF</a> <a href="/downloads/Release_of_Information_Update.doc" target="_blank">DOC</a></p>
            <p>Physician Referral - <a href="/downloads/PhysicianReferralForm.pdf" target="_blank">PDF</a> <a href="/downloads/PhysicianReferralForm.doc" target="_blank">DOC</a></p>
            <p><a href="Client/Appointment.aspx" class="wrapper">Referral Scheduling</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

