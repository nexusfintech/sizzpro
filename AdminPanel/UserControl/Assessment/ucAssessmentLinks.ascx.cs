﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class UserControl_Assessment_ucAssessmentLinks : System.Web.UI.UserControl
{
    string strMemberkey = "";
    Int64 intUserid;

    protected void Page_Load(object sender, EventArgs e)
    {
        strMemberkey = Session["form_memberkey"].ToString();
        intUserid = Convert.ToInt64(Session["form_userid"]);
        lnkIntro.NavigateUrl = "~/Clients/Assessment/AssessmentIntroManage.aspx";
        lnkProfile.NavigateUrl = "~/Clients/ClientIntialAssessment.aspx";
        lnkFamily.NavigateUrl = "~/Users/UserFamilyOrigion.aspx";
        lnkSiblings.NavigateUrl = "~/Users/UserSibling.aspx";
        lnkspouses.NavigateUrl = "~/Users/Spousesandsignificant.aspx";
        lnkChildren.NavigateUrl = "~/Users/ChildrenManagement.aspx";
        lnkMedical.NavigateUrl = "~/Clients/Assessment/ClientMedical.aspx";
        lnksymptoms.NavigateUrl = "~/Clients/Assessment/PhysicalSymptoms.aspx";
        lnkWellBeing.NavigateUrl = "~/Clients/Assessment/ClientPhysicalWellBeing.aspx";
        lnkMental.NavigateUrl = "~/Clients/Assessment/ClientMentalHealth.aspx";
        lnkaddiction.NavigateUrl = "~/Clients/Assessment/ClientAddiction.aspx";
        lnkPersonalH.NavigateUrl = "~/Clients/Assessment/ClientPersonalHistory.aspx";
        lnkFin.NavigateUrl = "~/Clients/Assessment/ClientFinancial.aspx";
        lnkproblem.NavigateUrl = "~/Clients/Assessment/ClientProblem.aspx";
        lnkreadiness.NavigateUrl = "~/Clients/Assessment/ClientReadiness.aspx";
        lnkAccident.NavigateUrl = "~/Clients/Assessment/AccidentInformation.aspx";
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        lblName.Text = mu.UserName;
    }
}