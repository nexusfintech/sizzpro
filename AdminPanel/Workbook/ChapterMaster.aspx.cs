﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Workbook_ChapterMaster : System.Web.UI.Page
{
    clsChapters clsCHP = new clsChapters();
    clsobjectsharing clsOBS = new clsobjectsharing();

    private void BindGrid()
    {
        if (User.IsInRole("administrator"))
        {
            dtChapter.DataSource = clsCHP.GetAllRecord();
        }
        else
        {
            dtChapter.DataSource = clsCHP.GetAllRecordByUser((Guid)Membership.GetUser().ProviderUserKey);
        }
        dtChapter.DataBind();
    }

    private void BindGridNotMapped()
    {

        if (User.IsInRole("administrator"))
        {
            dtChapter.DataSource = clsCHP.GetAllNotMapped();
        }
        else if (User.IsInRole("member"))
        {
            dtChapter.DataSource = clsCHP.GetAllNotMappedByMember((Guid)Membership.GetUser().ProviderUserKey);
        }
        dtChapter.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGrid();
            if (MySession.Current.MESSAGE != string.Empty)
            {
                lblmessage.Text = MySession.Current.MESSAGE;
                msgdiv.Visible = true;
                MySession.Current.MESSAGE = string.Empty;
            }
        }
    }
    protected void dtChapter_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "EDIT")
        {
            MySession.Current.AddEditFlag = e.CommandName;
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Workbook/ChapterAddEdit.aspx");
        }
        if (e.CommandName == "SHARE")
        {
            MySession.Current.EditID = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/Workbook/ChapterSharing.aspx");
        }
        if (e.CommandName == "FREEZ")
        {
            Boolean BlnGetrslt = clsCHP.GetRecordByIDInProperties(Convert.ToInt64(e.CommandArgument));
            Int64 Version = clsCHP.GetChapterVersion(clsCHP.chp_code);
            Boolean BlnRslt = clsCHP.ChangeStatus(Convert.ToInt64(e.CommandArgument), true,Version+1);
            if (BlnRslt)
            {
                Response.Redirect("~/Workbook/ChapterMaster.aspx");
            }
            else
            {
                lblmessage.Text = "Error Occured During Freez Your Chapter ," + clsCHP.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
        if (e.CommandName == "NEWVER")
        {
            Boolean BlnRslt = clsCHP.GetRecordByIDInProperties(Convert.ToInt64(e.CommandArgument));
            if (BlnRslt)
            {
                clsCHP.chp_sts = false;
                Boolean blnSvRsl = clsCHP.Save();
                if (blnSvRsl == true)
                {
                    clsCHP.GetRecordByCodeInProperties(clsCHP.chp_code);
                    clsOBS.shr_sharedobjid = clsCHP.chp_id;
                    clsOBS.shr_object = "CHAPTER";
                    clsOBS.shr_ownerid = clsCHP.chp_Userid;
                    clsOBS.shr_sharedwithid = clsCHP.chp_Userid;
                    blnSvRsl = clsOBS.Save();
                    Response.Redirect("~/Workbook/ChapterMaster.aspx");
                }
                else
                {
                    lblmessage.Text = "Error Occured During Creatig New Copy ," + clsCHP.GetSetErrorMessage;
                    msgdiv.Visible = true;
                }
            }
            else
            {
                lblmessage.Text = "Error Occured During Featchng Your Workbook ," + clsCHP.GetSetErrorMessage;
                msgdiv.Visible = true;
            }
        }
    }
    protected void cmdCreateNew_Click(object sender, EventArgs e)
    {
        MySession.Current.AddEditFlag = "ADD";
        MySession.Current.EditID = 0;
        Response.Redirect("~/Workbook/ChapterAddEdit.aspx");
    }
    protected void cmdHideMapped_Click(object sender, EventArgs e)
    {
        BindGridNotMapped();
    }
    protected void cmdShowAll_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void cmdMyRec_Click(object sender, EventArgs e)
    {

    }
    protected void dtChapter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object DI = e.Item.DataItem;
            Guid UserId = new Guid(DataBinder.Eval(DI, "chp_UserId").ToString());
            if (UserId == (Guid)Membership.GetUser().ProviderUserKey || User.IsInRole("administrator"))
            {
                Boolean Isact = Convert.ToBoolean(DataBinder.Eval(DI, "chp_sts"));
                if (Isact)
                {
                    LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdFreez");
                    lnkf.Enabled = false;
                    lnkf.CssClass = "btn btn-success";

                    //LinkButton lnks = (LinkButton)e.Item.FindControl("cmdShare");
                    //lnks.Enabled = true;
                }
                else
                {
                    LinkButton lnkf = (LinkButton)e.Item.FindControl("cmdFreez");
                    lnkf.Enabled = true;

                    //LinkButton lnks = (LinkButton)e.Item.FindControl("cmdShare");
                    //lnks.Enabled = false;
                    //lnks.CssClass = "btn btn-success";
                }
                var dv = e.Item.FindControl("action");
                dv.Visible = true;
            }
            else
            {
                var dv = e.Item.FindControl("share");
                dv.Visible = true;
                var c = e.Item.FindControl("dvf");
                c.Visible = true;
            }
        }
    }
}