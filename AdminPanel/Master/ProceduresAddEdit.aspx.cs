﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;

public partial class Master_ProceduresAddEdit : System.Web.UI.Page
{
    clsProceduresMaster clsPM = new clsProceduresMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (MySession.Current.AddEditFlag == "EDIT")
            {
                loaddata();
            }
        }
    }
    private void loaddata()
    {
        Boolean blnRslt = clsPM.GetRecordByIDInProperties(MySession.Current.EditID);
        if (blnRslt)
        {
            txtPrcCd.Text = clsPM.p_code;
            txtProcedure.Text = clsPM.p_procedure;
            txtDescription.Text = clsPM.p_description;
        }
        else
        {
            MySession.Current.MESSAGE = clsPM.GetSetErrorMessage;
            Response.Redirect("~/Master/ProceduresMaster.aspx");
        }
    }
    private string validatepage()
    {
        string result = string.Empty;
        if (txtPrcCd.Text == string.Empty)
        {
            result = "Procedure Code is Required";
            return result;
        }
        else if (txtProcedure.Text == string.Empty)
        {
            result = "Procedure Name is Required";
            return result;
        }
        else
        {
            return result;
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string vldtresult = validatepage();
        if(vldtresult != string.Empty)
        {
            lblmessage.Text = vldtresult;
            msgdiv.Visible = true;
            return;
        }

        clsPM.p_code = txtPrcCd.Text;
        clsPM.p_procedure = txtProcedure.Text;
        clsPM.p_description = txtDescription.Text;
        clsPM.p_userid = (Guid)Membership.GetUser().ProviderUserKey;
        Boolean blnRslt;
        if (MySession.Current.AddEditFlag == "ADD")
        {
            blnRslt = clsPM.Save();
        }
        else
        {
            blnRslt = clsPM.Update(MySession.Current.EditID);
        }
        if (blnRslt)
        {
            MySession.Current.MESSAGE = "Record Saved Succcessfully";
            Response.Redirect("~/Master/ProceduresMaster.aspx");
        }
        else
        {
            lblmessage.Text = clsPM.GetSetErrorMessage;
            msgdiv.Visible = true;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/ProceduresMaster.aspx");
    }
}