﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Forum.aspx.cs" Inherits="LifeProgram_Forum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3>
                        Forum
                    </h3>
                </div>
                <div class="box-content nopadding">
                    <div class='form-horizontal form-column form-bordered' style="padding-bottom: 5px;padding-top:5px;">
                        <div>
                            <div class="span12" padding-top: 5px;">
                                <a id="nabblelink" href="http://recovering-my-life.50909.x6.nabble.com/">Recovering My Life Forum</a>
                                <script src="http://recovering-my-life.50909.x6.nabble.com/embed/f1"></script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" Runat="Server">
</asp:Content>