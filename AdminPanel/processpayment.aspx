﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSite.master" AutoEventWireup="true" CodeFile="processpayment.aspx.cs" Inherits="procedetopay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid" style="border: 1px solid #0072D6;">
        <div class="span12">
            <%--<asp:Literal ID="ltrPageBody" runat="server"></asp:Literal>--%>
            <h4 style="text-align: center">Payment</h4>
            <hr />
            <div class="form-horizontal form-bordered">
                <div class="control-group">
                    <label for="txtCode" class="control-label">CreditCard Number</label>
                    <div class="controls">
                        <asp:TextBox ID="txtccnum" runat="server" CssClass="input-large" MaxLength="16" placeholder="0000000000000000"></asp:TextBox>
                        <span class="help-block">your creditcard number without '-' or space</span>
                    </div>
                </div>
                <%--<div class="control-group">
                    <label for="txtAssignmentName" class="control-label">Card Holder Name</label>
                    <div class="controls">
                        <asp:TextBox ID="txtchname" runat="server" CssClass="input-large" placeholder="holder's name"></asp:TextBox>
                        <span class="help-block">name appeared on creditcard</span>
                    </div>
                </div>--%>
                <div class="control-group">
                    <label for="txtAssignmentName" class="control-label">Expiry Date</label>
                    <div class="controls">
                        <asp:TextBox ID="txtexp" runat="server" CssClass="input-large" MaxLength="4" placeholder="mmyy"></asp:TextBox>
                        <span class="help-block">mm/yy format</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="txtAssignmentName" class="control-label">Card Code</label>
                    <div class="controls">
                        <asp:TextBox ID="txtcardcode" runat="server" CssClass="input-large" MaxLength="3" placeholder="111"></asp:TextBox>
                        <span class="help-block">3 digit code appeare on card</span>
                    </div>
                </div>
                <div class="form-actions">
                    <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Pay Now" OnClick="cmdSave_Click" />
                    <%--<asp:Button ID="cmdCancel" runat="server" CssClass="btn" Text="Return To Cart" OnClick="cmdCancel_Click" />--%>
                    <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

