﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;

public partial class UserControl_Assessment_ucAssessmentMedical : System.Web.UI.UserControl
{
    clsInitialAssessment clsASS = new clsInitialAssessment();
    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    private void EditValue()
    {
        try
        {
            Guid objKey = new Guid(strMemberkey);
            Boolean blnEdit = false;
            clsASS.ini_userid = objKey;

            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtBMI.ClientID);
            clsASS.ini_valuetext = txtBMI.Text;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtBodyFat.ClientID);
            txtBodyFat.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtBornPlace.ClientID);
            txtBornPlace.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtcurrentmedical.ClientID);
            txtcurrentmedical.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtexercise.ClientID);
            txtexercise.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtfamilyhistory.ClientID);
            txtfamilyhistory.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtFamilyphycian.ClientID);
            txtFamilyphycian.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtHeight.ClientID);
            txtHeight.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtlastilnesstreat.ClientID);
            txtlastilnesstreat.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtmajorhispitalpurpose.ClientID);
            txtmajorhispitalpurpose.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtmedicaldosage.ClientID);
            txtmedicaldosage.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtphysical.ClientID);
            txtphysical.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtpresctibring.ClientID);
            txtpresctibring.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtpsychiatrist.ClientID);
            txtpsychiatrist.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtraised.ClientID);
            txtraised.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtResentBP.ClientID);
            txtResentBP.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtWaight.ClientID);
            txtWaight.Text = clsASS.ini_valuetext;


            blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtWaist.ClientID);
            txtWaist.Text = clsASS.ini_valuetext;
            
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error in " + clsASS.ini_valueflag + " saving process ...." + clsASS.GetSetErrorMessage + " " + ex.Message;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            EditValue();
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Guid objKey = new Guid(strMemberkey);
            Boolean blnSave = false;
            SetValue();
            clsASS.ini_userid = objKey;

            clsASS.ini_valueflag = txtBMI.ClientID;
            clsASS.ini_valuetext = txtBMI.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtBodyFat.ClientID;
            clsASS.ini_valuetext = txtBodyFat.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtBornPlace.ClientID;
            clsASS.ini_valuetext = txtBornPlace.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtcurrentmedical.ClientID;
            clsASS.ini_valuetext = txtcurrentmedical.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtexercise.ClientID;
            clsASS.ini_valuetext = txtexercise.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtfamilyhistory.ClientID;
            clsASS.ini_valuetext = txtfamilyhistory.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtFamilyphycian.ClientID;
            clsASS.ini_valuetext = txtFamilyphycian.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtHeight.ClientID;
            clsASS.ini_valuetext = txtHeight.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtlastilnesstreat.ClientID;
            clsASS.ini_valuetext = txtlastilnesstreat.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtmajorhispitalpurpose.ClientID;
            clsASS.ini_valuetext = txtmajorhispitalpurpose.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtmedicaldosage.ClientID;
            clsASS.ini_valuetext = txtmedicaldosage.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtphysical.ClientID;
            clsASS.ini_valuetext = txtphysical.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtpresctibring.ClientID;
            clsASS.ini_valuetext = txtpresctibring.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtpsychiatrist.ClientID;
            clsASS.ini_valuetext = txtpsychiatrist.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtraised.ClientID;
            clsASS.ini_valuetext = txtraised.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtResentBP.ClientID;
            clsASS.ini_valuetext = txtResentBP.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtWaight.ClientID;
            clsASS.ini_valuetext = txtWaight.Text;
            blnSave = clsASS.Save();

            clsASS.ini_valueflag = txtWaist.ClientID;
            clsASS.ini_valuetext = txtWaist.Text;
            blnSave = clsASS.Save();
            lblMessage.Text = "Save Successfully.";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error in " + clsASS.ini_valueflag + " saving process ...." + clsASS.GetSetErrorMessage + " " +ex.Message;
        }
        
        
    }
}