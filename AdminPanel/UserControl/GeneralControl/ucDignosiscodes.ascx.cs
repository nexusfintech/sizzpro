﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;
using System.Web.Security;
using System.Data;

public partial class UserControl_GeneralControl_ucDignosiscodes : System.Web.UI.UserControl
{
    clsAdminDignosiscodeMapping clsADM = new clsAdminDignosiscodeMapping();
    private Int64 intId = 0;
    private string strName = string.Empty;

    public Int64 SelectedValue
    {
        get { intId = Convert.ToInt64(ddlDignosis.SelectedValue); return intId; }
        set { 
            intId = value;
            ListItem itm = ddlDignosis.Items.FindByValue(intId.ToString());
            if (itm != null)
            {
                ddlDignosis.SelectedIndex = ddlDignosis.Items.IndexOf(itm);
            }
        }
    }

    public string SelectedName
    {
        get { strName = ddlDignosis.SelectedItem.Text; return strName; }
        set { strName = value; }
    }

    public int Width
    {
        set { ddlDignosis.Width = Unit.Pixel(value); }
    }

    public void Bind()
    {
        DataTable dt = clsADM.GetAllRecordByAdmin((Guid)Membership.GetUser().ProviderUserKey);
        dt.Columns.Add("DCode", typeof(string), "dcm_code + ' - '  + dcm_description");
        ddlDignosis.DataTextField = "DCode";
        ddlDignosis.DataValueField = "dcm_code";
        ddlDignosis.DataSource = dt;
        ddlDignosis.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
}