﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsFollowupDignosis
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsFollowupDignosis()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsFollowupDignosis()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _fd_id; }
            set
            {
                _fd_id = value;
                if (clsCMD.Parameters.Contains("@fd_id"))
                { clsCMD.Parameters["@fd_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fd_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _fd_id;
        public Int64 fd_id
        {
            get { return _fd_id; }
            set
            {
                _fd_id = value;
                if (clsCMD.Parameters.Contains("@fd_id"))
                { clsCMD.Parameters["@fd_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fd_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Int64 _fd_dignosiscode;
        public Int64 fd_dignosiscode
        {
            get { return _fd_dignosiscode; }
            set
            {
                _fd_dignosiscode = value;
                if (clsCMD.Parameters.Contains("@fd_dignosiscode"))
                { clsCMD.Parameters["@fd_dignosiscode"].Value = value; }
                else { clsCMD.Parameters.Add("@fd_dignosiscode", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _fd_pointer;
        public String fd_pointer
        {
            get { return _fd_pointer; }
            set
            {
                _fd_pointer = value;
                if (clsCMD.Parameters.Contains("@fd_pointer"))
                { clsCMD.Parameters["@fd_pointer"].Value = value; }
                else { clsCMD.Parameters.Add("@fd_pointer", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private Guid _fd_sessionid;
        public Guid fd_sessionid
        {
            get { return _fd_sessionid; }
            set
            {
                _fd_sessionid = value;
                if (clsCMD.Parameters.Contains("@fd_sessionid"))
                { clsCMD.Parameters["@fd_sessionid"].Value = value; }
                else { clsCMD.Parameters.Add("@fd_sessionid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                fd_id = Convert.ToInt64(dr["fd_id"]);
                fd_dignosiscode = Convert.ToInt64(dr["fd_dignosiscode"]);
                fd_pointer = Convert.ToString(dr["fd_pointer"]);
                fd_sessionid = new Guid(dr["fd_sessionid"].ToString());
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            fd_id = Convert.ToInt64("0");
            fd_dignosiscode = Convert.ToInt64("0");
            fd_pointer = Convert.ToString("NA");
            fd_sessionid = new Guid();
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.fd_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_FollowupDignosisAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 fd_id)
        {
            Boolean blnResult = false;
            this.fd_id = fd_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_FollowupDignosisAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 fd_id)
        {
            Boolean blnResult = false;
            this.fd_id = fd_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_FollowupDignosisAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_FollowupDignosisGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public DataTable GetAllRecordBySession(Guid Sessionid)
        {
            DataTable dtResult = new DataTable();
            try
            {
                this.fd_sessionid = Sessionid;
                clsCMD.CommandText = "sp_FollowupDignosisGetData";
                SetGetSPFlag = "BYSESSIONID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<clsFollowupDignosis_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsFollowupDignosis_PropertiesList> lstResult = new List<clsFollowupDignosis_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].fd_id = Convert.ToInt64(dr["fd_id"]);
                lstResult[i].fd_dignosiscode = Convert.ToInt64(dr["fd_dignosiscode"]);
                lstResult[i].fd_pointer = Convert.ToString(dr["fd_pointer"]);
                lstResult[i].fd_sessionid = new Guid(dr["fd_sessionid"].ToString());

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 fd_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fd_id = fd_id;
                clsCMD.CommandText = "sp_FollowupDignosisGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 fd_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fd_id = fd_id;
                clsCMD.CommandText = "sp_FollowupDignosisGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class clsFollowupDignosis_PropertiesList
    {
        public Int64 fd_id { get; set; }
        public Int64 fd_dignosiscode { get; set; }
        public String fd_pointer { get; set; }
        public Guid fd_sessionid { get; set; }
    }
}