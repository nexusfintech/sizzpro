﻿<%@ Page Title="Marriage Consuseling" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="marriage-counseling.aspx.cs" Inherits="marriage_counseling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-question"></i>&nbsp;MARRIAGE COUNSELING</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <img src="img/merragecounsil.jpg" />
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h5>Marriage Counseling</h5>
            <p>
                Many couples face issues in  marriage that lead them to a place where the relationship is questioned.&nbsp; It could be due to natural <a href="http://lifestyletherapycoach.com/family-counselling.html">family  transitions</a> such as <a href="http://lifestyletherapycoach.com/241-counseling/services.html">becoming  new parents</a>, or having children leave &ldquo;the nest&rdquo;.&nbsp; Or, it could happen as a result of  infidelity, <a href="http://lifestyletherapycoach.com/241-counseling/addictions-recovery.html">substance  abuse</a>, <a href="http://lifestyletherapycoach.com/241-counseling/grief-loss.html">loss of  a child</a>, or any number of unfortunate issues.&nbsp; Regardless of the cause, many couples find  themselves struggling with adjustment and changes. &nbsp;If the marriage does not have a strong and  stable foundation, these struggles can lead to irreversible damage, possibly  divorce.
            </p>

            <p>Marriage counseling (also known  as couple&rsquo;s therapy) has been shown to be beneficial as it can help couples  learn to resolve conflicts, communicate their needs, gain a deeper  understanding of their partner, and reconnect in a way to strengthen the  relationship and add joy back into the marriage.</p>
            <p>Children benefit when their  parents seek counseling for marital issues as it reduces the tension in the  home, and creates a more loving environment conducive to positive growth.&nbsp; Children also learn what it&rsquo;s like to be  married by watching their parents interact as a couple.&nbsp; If marital difficulties outweigh marital  happiness, the children will not only suffer over the division, they will gain  a distorted view of what marriage is all about.</p>
            <h5>How do you know when to seek help?</h5>
            <p>
                Although marriage has its ups and  downs, there are a few signs that indicate a more serious issue is at hand and professional  counseling should be considered.&nbsp; Some of  these indicators are:
            </p>
            <ul>
                <li>One or both partners feel a fundamental  dissatisfaction in the relationship.</li>
                <li>Disappointment in the relationship seems to be  more present than absent.</li>
                <li>Partners have stopped doing nice things for one  another</li>
                <li>Frequent arguments that go unresolved.</li>
                <li>Communication is limited and/or ceased being  productive.</li>
                <li>Minor issues tend to take on a life of their own  and cause significant disruption.</li>
                <li>Limited or no sexual activity without an  underlying medical cause.</li>
                <li>Infidelity</li>
                <li>Sexual issues</li>
                <li>Loss of connection between partners</li>
                <li>Violence in the relationship</li>
                <li>Withdrawal by one or both of the partners</li>
            </ul>
            <p>Although statistics indicate that  divorce is common for half the couples who marry for the first time, &ldquo;significant  data exists which support the efficacy of family and couples therapy and that  there is no evidence indicating that couples are harmed when they undergo  treatment&rdquo; (Friedlander,  1997)1.&nbsp;  Furthermore, studies have shown that 75% of couples receiving therapy  for marital distress were better off and reported significant improvements than  those not receiving professional help.</p>
            <h5>What happens in Marriage Counseling?</h5>
            <p>
                A marriage counselor is a professional  trained in couple&rsquo;s therapy.&nbsp; He/she may  use one of several different approaches, depending on what problems the couple  is experiencing.&nbsp; Whatever the therapy,  the goals will be to help the couple learn to use the skills necessary for a  good marriage.&nbsp;
            </p>
            <p>Some couples find it difficult to  begin marriage therapy simply because they feel there is no hope for their  marriage.&nbsp; Others fear expressing their  problems to a stranger.&nbsp; These factors  are taken into consideration by the therapist who will work with the couple to  create a comfortable environment for all involved.</p>
            <p>Strong marriages are the basis of  strong families, which lead to strong communities and benefit all around.&nbsp; Let us help you build a strong foundation for  your marriage.</p>
            <h5>Marriage Resources Online</h5>
            <p>
                <a href="http://www.healthymarriageinfo.org/">National Healthy Marriage Resource  Center</a>
                <br />
                <a href="http://www.focusonthefamily.com/marriage.aspx">Focus on the Family &ndash;  Marriage and Relationships</a><br />
                <a href="http://www.strongbonds.org/skins/strongbonds/home.aspx">U. S. Army&rsquo;s  &ldquo;Strong Bonds&rdquo; Marriage and Family Program</a><br />
            </p>
            <h5>Books</h5>
            <p>
                <a href="http://www.amazon.com/exec/obidos/ASIN/0609805797/thefamilyandm-20">Seven  Principles for Making Marriage Work &ndash; Dr. John Gottman</a><br />
                <a href="http://www.amazon.com/dp/B0032CVAQQ?tag=thefamilyandm-20&camp=15309&creative=331477&linkCode=st1&creativeASIN=B0032CVAQQ&adid=078NJPCTBQW5R5RRXQQ6">The  Five Love Languages &ndash; Gary Chapman</a><br />
                <a href="http://www.amazon.com/dp/0805448853?tag=thefamilyandm-20&camp=15309&creative=331477&linkCode=st1&creativeASIN=0805448853&adid=1655128CE9FYXADPNPY6">The  Love Dare &ndash; Steven Kendrick</a><br />
                <a href="http://www.amazon.com/Sacred-Marriage-What-Designed-Happy/dp/0310242827/ref=sr_1_9?s=books&ie=UTF8&qid=1300897696&sr=1-9">Sacred  Marriage &ndash; Gary Thomas</a><br />
            </p>
            <p align="center"><a href="Client/Appointment.aspx">Click here to set up an appointment with one of our therapists</a></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

