﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;
namespace clsDAL
{
    public class cls_FollowupCashnote
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public cls_FollowupCashnote()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~cls_FollowupCashnote()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion


        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _fcn_id; }
            set
            {
                _fcn_id = value;
                if (clsCMD.Parameters.Contains("@fcn_id"))
                { clsCMD.Parameters["@fcn_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _fcn_id;
        public Int64 fcn_id
        {
            get { return _fcn_id; }
            set
            {
                _fcn_id = value;
                if (clsCMD.Parameters.Contains("@fcn_id"))
                { clsCMD.Parameters["@fcn_id"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _fcn_sessionid;
        public Guid fcn_sessionid
        {
            get { return _fcn_sessionid; }
            set
            {
                _fcn_sessionid = value;
                if (clsCMD.Parameters.Contains("@fcn_sessionid"))
                { clsCMD.Parameters["@fcn_sessionid"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_sessionid", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _fcn_reasonforvisit;
        public String fcn_reasonforvisit
        {
            get { return _fcn_reasonforvisit; }
            set
            {
                _fcn_reasonforvisit = value;
                if (clsCMD.Parameters.Contains("@fcn_reasonforvisit"))
                { clsCMD.Parameters["@fcn_reasonforvisit"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_reasonforvisit", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _fcn_treatmentplan;
        public String fcn_treatmentplan
        {
            get { return _fcn_treatmentplan; }
            set
            {
                _fcn_treatmentplan = value;
                if (clsCMD.Parameters.Contains("@fcn_treatmentplan"))
                { clsCMD.Parameters["@fcn_treatmentplan"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_treatmentplan", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _fcn_behavior;
        public String fcn_behavior
        {
            get { return _fcn_behavior; }
            set
            {
                _fcn_behavior = value;
                if (clsCMD.Parameters.Contains("@fcn_behavior"))
                { clsCMD.Parameters["@fcn_behavior"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_behavior", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _fcn_intervention;
        public String fcn_intervention
        {
            get { return _fcn_intervention; }
            set
            {
                _fcn_intervention = value;
                if (clsCMD.Parameters.Contains("@fcn_intervention"))
                { clsCMD.Parameters["@fcn_intervention"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_intervention", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _fcn_response;
        public String fcn_response
        {
            get { return _fcn_response; }
            set
            {
                _fcn_response = value;
                if (clsCMD.Parameters.Contains("@fcn_response"))
                { clsCMD.Parameters["@fcn_response"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_response", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _fcn_progress;
        public String fcn_progress
        {
            get { return _fcn_progress; }
            set
            {
                _fcn_progress = value;
                if (clsCMD.Parameters.Contains("@fcn_progress"))
                { clsCMD.Parameters["@fcn_progress"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_progress", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _fcn_newclinicissue;
        public String fcn_newclinicissue
        {
            get { return _fcn_newclinicissue; }
            set
            {
                _fcn_newclinicissue = value;
                if (clsCMD.Parameters.Contains("@fcn_newclinicissue"))
                { clsCMD.Parameters["@fcn_newclinicissue"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_newclinicissue", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _fcn_nextvisitplan;
        public String fcn_nextvisitplan
        {
            get { return _fcn_nextvisitplan; }
            set
            {
                _fcn_nextvisitplan = value;
                if (clsCMD.Parameters.Contains("@fcn_nextvisitplan"))
                { clsCMD.Parameters["@fcn_nextvisitplan"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_nextvisitplan", SqlDbType.NVarChar).Value = value; }
            }
        }
        private String _fcn_apper;
        public String fcn_apper
        {
            get { return _fcn_apper; }
            set
            {
                _fcn_apper = value;
                if (clsCMD.Parameters.Contains("@fcn_apper"))
                { clsCMD.Parameters["@fcn_apper"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_apper", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _fcn_speech;
        public String fcn_speech
        {
            get { return _fcn_speech; }
            set
            {
                _fcn_speech = value;
                if (clsCMD.Parameters.Contains("@fcn_speech"))
                { clsCMD.Parameters["@fcn_speech"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_speech", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _fcn_mood;
        public String fcn_mood
        {
            get { return _fcn_mood; }
            set
            {
                _fcn_mood = value;
                if (clsCMD.Parameters.Contains("@fcn_mood"))
                { clsCMD.Parameters["@fcn_mood"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_mood", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _fcn_affect;
        public String fcn_affect
        {
            get { return _fcn_affect; }
            set
            {
                _fcn_affect = value;
                if (clsCMD.Parameters.Contains("@fcn_affect"))
                { clsCMD.Parameters["@fcn_affect"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_affect", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _fcn_thoughtForm;
        public String fcn_thoughtForm
        {
            get { return _fcn_thoughtForm; }
            set
            {
                _fcn_thoughtForm = value;
                if (clsCMD.Parameters.Contains("@fcn_thoughtForm"))
                { clsCMD.Parameters["@fcn_thoughtForm"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_thoughtForm", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _fcn_thughtContent;
        public String fcn_thughtContent
        {
            get { return _fcn_thughtContent; }
            set
            {
                _fcn_thughtContent = value;
                if (clsCMD.Parameters.Contains("@fcn_thughtContent"))
                { clsCMD.Parameters["@fcn_thughtContent"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_thughtContent", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        public String fcn_whopresent
        {
            get { return _fcn_whopresent; }
            set
            {
                _fcn_whopresent = value;
                if (clsCMD.Parameters.Contains("@fcn_whopresent"))
                { clsCMD.Parameters["@fcn_whopresent"].Value = value; }
                else { clsCMD.Parameters.Add("@fcn_whopresent", SqlDbType.NVarChar).Value = value; }
            }
        }
           private String _fcn_whopresent;
        
        #endregion


        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                fcn_id = Convert.ToInt64(dr["fcn_id"]);
                fcn_sessionid = new Guid(Convert.ToString(dr["fcn_sessionid"]));
                fcn_reasonforvisit = Convert.ToString(dr["fcn_reasonforvisit"]);
                fcn_treatmentplan = Convert.ToString(dr["fcn_treatmentplan"]);
                fcn_behavior = Convert.ToString(dr["fcn_behavior"]);
                fcn_intervention = Convert.ToString(dr["fcn_intervention"]);
                fcn_response = Convert.ToString(dr["fcn_response"]);
                fcn_progress = Convert.ToString(dr["fcn_progress"]);
                fcn_newclinicissue = Convert.ToString(dr["fcn_newclinicissue"]);
                fcn_nextvisitplan = Convert.ToString(dr["fcn_nextvisitplan"]);
                fcn_apper = Convert.ToString(dr["fcn_apper"]);
                fcn_speech = Convert.ToString(dr["fcn_speech"]);
                fcn_mood = Convert.ToString(dr["fcn_mood"]);
                fcn_affect = Convert.ToString(dr["fcn_affect"]);
                fcn_thoughtForm = Convert.ToString(dr["fcn_thoughtForm"]);
                fcn_thughtContent = Convert.ToString(dr["fcn_thughtContent"]);
                fcn_whopresent = Convert.ToString(dr["fcn_whopresent"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion


        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            fcn_id = Convert.ToInt64("0");
            fcn_sessionid = new Guid();
            fcn_reasonforvisit = string.Empty;
            fcn_treatmentplan = string.Empty;
            fcn_behavior = string.Empty;
            fcn_intervention = string.Empty;
            fcn_response = string.Empty;
            fcn_progress = string.Empty;
            fcn_newclinicissue = string.Empty;
            fcn_nextvisitplan = string.Empty;
            fcn_apper = string.Empty;
            fcn_speech = string.Empty;
            fcn_mood = string.Empty;
            fcn_affect = string.Empty;
            fcn_thoughtForm = string.Empty;
            fcn_thughtContent = string.Empty;
            fcn_whopresent = string.Empty;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.fcn_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "sp_FollowupCashnoteAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean Update(Int64 fcn_id)
        {
            Boolean blnResult = false;
            this.fcn_id = fcn_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "sp_FollowupCashnoteAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }

            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 fcn_id)
        {
            Boolean blnResult = false;
            this.fcn_id = fcn_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "sp_FollowupCashnoteAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            return blnResult;
        }

        #endregion


        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "sp_FollowupCashnoteGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            return dtResult;
        }

        public List<cls_FollowupCashnote_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<cls_FollowupCashnote_PropertiesList> lstResult = new List<cls_FollowupCashnote_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].fcn_id = Convert.ToInt64(dr["fcn_id"]);
                lstResult[i].fcn_sessionid = new Guid(Convert.ToString(dr["fcn_sessionid"]));
                lstResult[i].fcn_reasonforvisit = Convert.ToString(dr["fcn_reasonforvisit"]);
                lstResult[i].fcn_treatmentplan = Convert.ToString(dr["fcn_treatmentplan"]);
                lstResult[i].fcn_behavior = Convert.ToString(dr["fcn_behavior"]);
                lstResult[i].fcn_intervention = Convert.ToString(dr["fcn_intervention"]);
                lstResult[i].fcn_response = Convert.ToString(dr["fcn_response"]);
                lstResult[i].fcn_progress = Convert.ToString(dr["fcn_progress"]);
                lstResult[i].fcn_newclinicissue = Convert.ToString(dr["fcn_newclinicissue"]);
                lstResult[i].fcn_nextvisitplan = Convert.ToString(dr["fcn_nextvisitplan"]);
                lstResult[i].fcn_apper = Convert.ToString(dr["fcn_apper"]);
                lstResult[i].fcn_speech = Convert.ToString(dr["fcn_speech"]);
                lstResult[i].fcn_mood = Convert.ToString(dr["fcn_mood"]);
                lstResult[i].fcn_affect = Convert.ToString(dr["fcn_affect"]);
                lstResult[i].fcn_thoughtForm = Convert.ToString(dr["fcn_thoughtForm"]);
                lstResult[i].fcn_thughtContent = Convert.ToString(dr["fcn_thughtContent"]);
                lstResult[i].fcn_whopresent = Convert.ToString(dr["fcn_whopresent"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 fcn_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fcn_id = fcn_id;
                clsCMD.CommandText = "sp_FollowupCashnoteGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean GetRecordBySession(Guid Sessionid)
        {
            Boolean blnResult = false;
            try
            {
                this.fcn_sessionid = Sessionid;
                clsCMD.CommandText = "sp_FollowupCashnoteGetData";
                SetGetSPFlag = "BYSESSION";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 fcn_id)
        {
            Boolean blnResult = false;
            try
            {
                this.fcn_id = fcn_id;
                clsCMD.CommandText = "sp_FollowupCashnoteGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            return blnResult;
        }

        #endregion

    }
    public class cls_FollowupCashnote_PropertiesList
    {
        public Int64 fcn_id { get; set; }
        public Guid fcn_sessionid { get; set; }
        public String fcn_reasonforvisit { get; set; }
        public String fcn_treatmentplan { get; set; }
        public String fcn_behavior { get; set; }
        public String fcn_intervention { get; set; }
        public String fcn_response { get; set; }
        public String fcn_progress { get; set; }
        public String fcn_newclinicissue { get; set; }
        public String fcn_nextvisitplan { get; set; }
        public String fcn_apper { get; set; }
        public String fcn_speech { get; set; }
        public String fcn_mood { get; set; }
        public String fcn_affect { get; set; }
        public String fcn_thoughtForm { get; set; }
        public String fcn_thughtContent { get; set; }
        public String fcn_whopresent { get; set; }
        
    }
}