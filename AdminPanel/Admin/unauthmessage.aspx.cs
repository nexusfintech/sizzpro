﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Unauthorisedaccess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (MySession.Current.MSGCODE != string.Empty)
        {
            if (MySession.Current.MSGCODE == "unauthorizepage")
            {
                unauthoriseaccess.Visible = true;
               
            }
            if (MySession.Current.MSGCODE == "notexist")
            {
                invalidpage.Visible = true;
            }
            if (MySession.Current.MSGCODE == "unauthorizerequest")
            {
                unauthoriseaccess.Visible = true;
            }
            MySession.Current.MSGCODE = string.Empty;
        }
        else
        {
            Response.Redirect("~/Admin/Home.aspx");
        }
    }
}