﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using clsDAL.Conn;

namespace clsDAL
{
    public class clsLessions
    {
        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
                clsCMD.Connection = clsCon;
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public clsLessions()
        {
            clsCMD.Parameters.Clear();
            clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsLessions()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _lsn_id; }
            set
            {
                _lsn_id = value;
                if (clsCMD.Parameters.Contains("@lsn_id"))
                { clsCMD.Parameters["@lsn_id"].Value = value; }
                else { clsCMD.Parameters.Add("@lsn_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _lsn_id;
        public Int64 lsn_id
        {
            get { return _lsn_id; }
            set
            {
                _lsn_id = value;
                if (clsCMD.Parameters.Contains("@lsn_id"))
                { clsCMD.Parameters["@lsn_id"].Value = value; }
                else { clsCMD.Parameters.Add("@lsn_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private Guid _lsn_code;
        public Guid lsn_code
        {
            get { return _lsn_code; }
            set
            {
                _lsn_code = value;
                if (clsCMD.Parameters.Contains("@lsn_code"))
                { clsCMD.Parameters["@lsn_code"].Value = value; }
                else { clsCMD.Parameters.Add("@lsn_code", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private String _lsn_name;
        public String lsn_name
        {
            get { return _lsn_name; }
            set
            {
                _lsn_name = value;
                if (clsCMD.Parameters.Contains("@lsn_name"))
                { clsCMD.Parameters["@lsn_name"].Value = value; }
                else { clsCMD.Parameters.Add("@lsn_name", SqlDbType.NVarChar, 1024).Value = value; }
            }
        }
        private String _lsn_short;
        public String lsn_short
        {
            get { return _lsn_short; }
            set
            {
                _lsn_short = value;
                if (clsCMD.Parameters.Contains("@lsn_short"))
                { clsCMD.Parameters["@lsn_short"].Value = value; }
                else { clsCMD.Parameters.Add("@lsn_short", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _lsn_description;
        public String lsn_description
        {
            get { return _lsn_description; }
            set
            {
                _lsn_description = value;
                if (clsCMD.Parameters.Contains("@lsn_description"))
                { clsCMD.Parameters["@lsn_description"].Size = value.Length; clsCMD.Parameters["@lsn_description"].Value = value; }
                else { clsCMD.Parameters.Add("@lsn_description", SqlDbType.NVarChar, value.Length).Value = value; }
            }
        }
        private Guid _lsn_UserId;
        public Guid lsn_UserId
        {
            get { return _lsn_UserId; }
            set
            {
                _lsn_UserId = value;
                if (clsCMD.Parameters.Contains("@lsn_UserId"))
                { clsCMD.Parameters["@lsn_UserId"].Value = value; }
                else { clsCMD.Parameters.Add("@lsn_UserId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }
        private Guid _lsn_OwnerId;
        public Guid lsn_OwnerId
        {
            get { return _lsn_OwnerId; }
            set
            {
                _lsn_OwnerId = value;
                if (clsCMD.Parameters.Contains("@lsn_OwnerId"))
                { clsCMD.Parameters["@lsn_OwnerId"].Value = value; }
                else { clsCMD.Parameters.Add("@lsn_OwnerId", SqlDbType.UniqueIdentifier).Value = value; }
            }
        }

        private Boolean _lsn_sts;
        public Boolean lsn_sts
        {
            get { return _lsn_sts; }
            set
            {
                _lsn_sts = value;
                if (clsCMD.Parameters.Contains("@lsn_sts"))
                { clsCMD.Parameters["@lsn_sts"].Value = value; }
                else { clsCMD.Parameters.Add("@lsn_sts", SqlDbType.Bit).Value = value; }
            }
        }

        private Int64 _lsn_version;
        public Int64 lsn_version
        {
            get { return _lsn_version; }
            set
            {
                _lsn_version = value;
                if (clsCMD.Parameters.Contains("@lsn_version"))
                { clsCMD.Parameters["@lsn_version"].Value = value; }
                else { clsCMD.Parameters.Add("@lsn_version", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                lsn_id = Convert.ToInt64(dr["lsn_id"]);
                lsn_code = new Guid(dr["lsn_code"].ToString());
                lsn_name = Convert.ToString(dr["lsn_name"]);
                lsn_short = Convert.ToString(dr["lsn_short"]);
                lsn_description = Convert.ToString(dr["lsn_description"]);
                lsn_UserId = new Guid(dr["lsn_UserId"].ToString());
                lsn_OwnerId = new Guid(dr["lsn_OwnerId"].ToString());
                lsn_version = Convert.ToInt64(dr["lsn_version"]);
                lsn_sts = Convert.ToBoolean(dr["lsn_sts"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            lsn_id = Convert.ToInt64("0");
            lsn_code = new Guid();
            lsn_name = Convert.ToString("NA");
            lsn_short = Convert.ToString("NA");
            lsn_description = Convert.ToString("NA");
            lsn_UserId = new Guid();
            lsn_OwnerId = new Guid();
            lsn_version = 0;
        }
        #endregion

        #region Declare Add/Edit/Delete Function

        public Boolean Save()
        {
            OpenConnection();
            Boolean blnResult = false;
            this.lsn_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                clsCMD.CommandText = "lessonsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 lsn_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.lsn_id = lsn_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                clsCMD.CommandText = "lessonsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 lsn_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.lsn_id = lsn_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                clsCMD.CommandText = "lessonsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean ChangeStatus(Int64 lsnid, Boolean status,Int64 ver)
        {
            OpenConnection();
            Boolean blnResult = false;
            this.lsn_id = lsnid;
            this.lsn_sts = status;
            this.lsn_version = ver;
            this.AddEditDeleteFlag = "CHANGESTS";
            try
            {
                clsCMD.CommandText = "lessonsAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "lessonsGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByMember(Guid Memberid)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "lessonsGetData";
                this.lsn_UserId = Memberid;
                SetGetSPFlag = "ALLBYMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordByUser(Guid UserId)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "lessonsGetData";
                this.lsn_UserId = UserId;
                SetGetSPFlag = "ALLBYUSER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllNotMapped()
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "lessonsGetData";
                SetGetSPFlag = "NOTMAPPED";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllNotMappedByMember(Guid Memberid)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                clsCMD.CommandText = "lessonsGetData";
                this.lsn_UserId = Memberid;
                SetGetSPFlag = "NOTMAPPEDBYMEMBER";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public DataTable GetAllRecordPKWise(Int64 intPKID)
        {
            OpenConnection();
            DataTable dtResult = new DataTable();
            try
            {
                this.lsn_id = intPKID;
                clsCMD.CommandText = "lessonsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsLessions_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsLessions_PropertiesList> lstResult = new List<clsLessions_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].lsn_id = Convert.ToInt64(dr["lsn_id"]);
                lstResult[i].lsn_name = Convert.ToString(dr["lsn_name"]);
                lstResult[i].lsn_short = Convert.ToString(dr["lsn_short"]);
                lstResult[i].lsn_description = Convert.ToString(dr["lsn_description"]);
                lstResult[i].lsn_code = new Guid(dr["lsn_code"].ToString());
                lstResult[i].lsn_OwnerId = new Guid(dr["lsn_OwnerId"].ToString());
                lstResult[i].lsn_UserId = new Guid(dr["lsn_UserId"].ToString());
                lstResult[i].lsn_sts = Convert.ToBoolean(dr["lsn_sts"]);
                lstResult[i].lsn_version = Convert.ToInt64(dr["lsn_version"]);
                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 lsn_id)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.lsn_id = lsn_id;
                clsCMD.CommandText = "lessonsGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Guid lsn_code)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.lsn_code = lsn_code;
                clsCMD.CommandText = "lessonsGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean GetRecordByCodeInProperties(Guid code)
        {
            OpenConnection();
            Boolean blnResult = false;
            try
            {
                this.lsn_code = code;
                clsCMD.CommandText = "lessonsGetData";
                SetGetSPFlag = "BYCODE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Int64 GetLessonVersion(Guid Code)
        {
            OpenConnection();
            Int64 Version = 0;
            try
            {
                this.lsn_code = Code;
                clsCMD.CommandText = "lessonsGetData";
                SetGetSPFlag = "FINDVER";
                Version = Convert.ToInt64(clsCMD.ExecuteScalar());
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                Version = -1;
            }
            CloseConnection();
            return Version;
        }
        #endregion
    }

    public class clsLessions_PropertiesList
    {
        public Int64 lsn_id { get; set; }
        public Guid lsn_code { get; set; }
        public String lsn_name { get; set; }
        public String lsn_short { get; set; }
        public String lsn_description { get; set; }
        public Guid lsn_UserId { get; set; }
        public Guid lsn_OwnerId { get; set; }
        public Boolean lsn_sts { get; set; }
        public Int64 lsn_version { get; set; }
    }

}