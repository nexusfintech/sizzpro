﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using clsDAL;
public partial class UserControl_Assessment_PhysicalSymptoms : System.Web.UI.UserControl
{
    clsInitialAssessment clsASS = new clsInitialAssessment();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();

    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    string strAddEditFlag;
    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void SetValue()
    {
        Guid objKey = new Guid(strMemberkey);
        MembershipUser mu = Membership.GetUser(objKey);
        clsUsr.GetRecordByIDInProperties(objKey);
        intUserID = clsUsr.User_Id;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            EditEntry();
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Boolean blnSave = false;
            Guid objKey = new Guid(strMemberkey);
            clsASS.ini_userid = objKey;
            if (txtcomego.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtcomego.Text.Trim();
                clsASS.ini_valueflag = txtcomego.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtPhySymOther.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtPhySymOther.Text.Trim();
                clsASS.ini_valueflag = txtPhySymOther.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtreasonforvisit.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtreasonforvisit.Text.Trim();
                clsASS.ini_valueflag = txtreasonforvisit.ClientID;
                blnSave = clsASS.Save();
            }
            if (txtsymptomsapper.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txtsymptomsapper.Text.Trim();
                clsASS.ini_valueflag = txtsymptomsapper.ClientID;
                blnSave = clsASS.Save();
            }
            if (txttimefeelPain.Text.Trim() != "")
            {
                clsASS.ini_valuetext = txttimefeelPain.Text.Trim();
                clsASS.ini_valueflag = txttimefeelPain.ClientID;
                blnSave = clsASS.Save();
            }
            ListItem li = chkconditionworse.SelectedItem;
            if (li != null)
            {
                clsASS.ini_valueflag = chkconditionworse.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(chkconditionworse.SelectedValue);
                blnSave = clsASS.Save();
            }
            ListItem li1 = chkInterfere.SelectedItem;
            if (li1 != null)
            {
                clsASS.ini_valueflag = chkInterfere.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(chkInterfere.SelectedValue);
                blnSave = clsASS.Save();
            }
            ListItem li2 = chkmovmentpain.SelectedItem;
            if (li1 != null)
            {
                clsASS.ini_valueflag = chkmovmentpain.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(chkmovmentpain.SelectedValue);
                blnSave = clsASS.Save();
            }
            ListItem li3 = chktreatmentforcondi.SelectedItem;
            if (li1 != null)
            {
                clsASS.ini_valueflag = chktreatmentforcondi.ClientID;
                clsASS.ini_valueint = Convert.ToInt64(chktreatmentforcondi.SelectedValue);
                blnSave = clsASS.Save();
            }

            clsASR.storcrd_stepid = 13;
            clsASR.stprcrd_userid = objKey;
            Boolean stprslt = clsASR.Save();
            if (stprslt == true)
            {
                Response.Redirect("~/Client/Assessments/AccidentInformation.aspx");
            }
            lblMessage.Text = "Save Successfully";
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error in save process.. of " + clsASS.ini_valueflag + " System Message : " + ex.Message;
        }
    }

    private void EditEntry()
    {
        Boolean blnEdit = false;
        Guid objKey = new Guid(strMemberkey);
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkconditionworse.ClientID);
        if (blnEdit == true)
        {
            ListItem li = chkconditionworse.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                chkconditionworse.SelectedIndex = chkconditionworse.Items.IndexOf(li);
            }
        }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkInterfere.ClientID);
        if (blnEdit == true)
        {
            ListItem li = chkInterfere.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                chkInterfere.SelectedIndex = chkInterfere.Items.IndexOf(li);
            }
        }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chkmovmentpain.ClientID);
        if (blnEdit == true)
        {
            ListItem li = chkmovmentpain.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                chkmovmentpain.SelectedIndex = chkmovmentpain.Items.IndexOf(li);
            }
        }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, chktreatmentforcondi.ClientID);
        if (blnEdit == true)
        {
            ListItem li = chktreatmentforcondi.Items.FindByValue(clsASS.ini_valueint.ToString());
            if (li != null)
            {
                chktreatmentforcondi.SelectedIndex = chktreatmentforcondi.Items.IndexOf(li);
            }
        }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtcomego.ClientID);
        if (blnEdit == true)
        { txtcomego.Text = clsASS.ini_valuetext; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtPhySymOther.ClientID);
        if (blnEdit == true)
        { txtPhySymOther.Text = clsASS.ini_valuetext; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtreasonforvisit.ClientID);
        if (blnEdit == true)
        { txtreasonforvisit.Text = clsASS.ini_valuetext; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtsymptomsapper.ClientID);
        if (blnEdit == true)
        { txtsymptomsapper.Text = clsASS.ini_valuetext; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txttimefeelPain.ClientID);
        if (blnEdit == true)
        { txttimefeelPain.Text = clsASS.ini_valuetext; }
        blnEdit = clsASS.GetRecordUseridFlagInProperties(objKey, txtTreatedDoctors.ClientID);
        if (blnEdit == true)
        { txtTreatedDoctors.Text = clsASS.ini_valuetext; }
    }
}