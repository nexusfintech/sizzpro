﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AddEditAgreement.aspx.cs" Inherits="Agreements_AddEditAgreementaspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkCreateNew" runat="server" NavigateUrl="~/Agreements/AgreementMaster.aspx"><i class="icon-retweet"></i>&nbsp;Agreements </asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Agreement Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                         <% if (User.IsInRole("administrator"))
                           { %>
                        <div class="control-group">
                            <label for="textfield" class="control-label">User</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectMember" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                <span class="help-block">Select user for create agreement</span>
                            </div>
                        </div>
                        <%} %>
                          <div class="control-group">
                            <label for="txtCode" class="control-label">Agreement Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtName" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            </div>
                        </div>
                         <div class="control-group">
                            <label for="txtCode" class="control-label">
                                Content
                            </label>
                            <div class="controls">
                                <asp:DropDownList ID="DropDownList1" CssClass="select2-me input-xlarge" runat="server">
                                    <asp:ListItem Value="">[ SMARTY TAG ]</asp:ListItem>
                                    <asp:ListItem Value="<p>{CompenyName}</p>">COMPENY NAME</asp:ListItem>
                                    <asp:ListItem Value="<p>{ClientFirst}</p>">CLIENT FIRST NAME</asp:ListItem>
                                    <asp:ListItem Value="<p>{ClientLast}</p>">CLIENT LAST NAME</asp:ListItem>
                                    <asp:ListItem Value="<p>{ClientEmail}</p>">CLIENT EMAIL</asp:ListItem>
                                    <asp:ListItem Value='<p><strong>AUTHORIZATION FOR CORRESPONDENCE</strong></p><p>I hereby authorize Lifestyle Therapy and Coaching to contact me for purposes of canceling, rescheduling, or verifying appointments,&nbsp;or for any other reason effecting my&nbsp;treatment.</p><p><strong>You May:</strong></p><p style="margin-left: 40px;">{corspondenceby}</p>'> AUTHORIZATION COROSPONDENCE</asp:ListItem>
                                    <asp:ListItem Value='<p><strong>AUTHORIZATION TO RELEASE INFORMATION (Optional)</strong></p><p>INTEGRATED CARE - In order to provide Total Health &amp; Wellness we need to communicate with your primary care physician your progress in treatment with us.</p><p>I give Lifestyle Therapy &amp; Coaching my permission to communicate with {authorisationphysian} for the purpose of improving my medical care. </p><p><strong>Address </strong>: {authorizationaddress}</p><p><strong>City </strong>:{authorizationcity}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>State </strong>:{authorizationstate}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Zip </strong>:{authorizationzip} </p><p><strong>Phone </strong>: {authorizationphn}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Fax </strong>: {authorizationfax} </p><p>This authorization will end {authorizationend} .</p><p>I acknowledge that I have been given a copy of this document.I understand my rights as a client</p>'> AUTHORIZATION TO RELEASE INFORMATION</asp:ListItem>
                                    <asp:ListItem Value='<p>As indicated earlier, Lifestyle requires you to keep a credit card on file. This will help us collect co-payments, deductibles and any unpaid balances. This will also help us collect any deductibles that may be added towards your treatment. (It&rsquo;s best to know your benefits before coming). It is also your responsibility to make sure your credit card does not expire and continues to stay updated.</p><p><br /><strong>Credit Card Type</strong>: {creditcardtype}<br /><strong>Name as it appears on Card</strong>: {nameoncreditcard}<br/><strong>Card Number:</strong> {cardnumber}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>S</strong><strong>ecurity Number</strong>:{creditcardsecnumber}<br /><strong>Expiration Date</strong>: {creditcardexpirationdate}</p>'> CREDIT CARD INFORMATION </asp:ListItem>
                                    <asp:ListItem Value='<p><strong>ATTESTATION</strong></p><p>I have read the above authorization and understand it. I agree to hold JC Enterprise Group, Inc. DBA Lifestyle Therapy &amp; Coaching | Chiropractic | Dietetic harmless for claims or damages in connection with our work together. This is a contract between myself and Lifestyle Therapy &amp; Coaching and I understand that it is also a release of potential liability.</p><p>I acknowledge I have discussed the following with my healthcare provider:</p><p style="margin-left: 40px;">1. The condition that the treatment is to address;</p><p style="margin-left: 40px;">2. The nature of the treatment;</p><p style="margin-left: 40px;">3. The risks and benefits of that treatment; and</p><p style="margin-left: 40px;">4. Any alternatives to that treatment</p><p>I have had the opportunity to ask questions and receive answers regarding the treatment.</p><p>I consent to the treatments offered or recommended to me by my healthcare provider, including osseous and soft tissue manipulation. I intend this consent to apply to all my present and future care from all professionals at Lifestyle Therapy &amp; Coaching. </p><p><strong>If there happens to be any medical emergency while in treatment at Lifestyle, if there is any physical or mental incapacity on my part I give permission for clinicians and staff to act on my behalf</strong>.</p>'> ATTESTATION </asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="controls">
                                <asp:TextBox ID="txtDescription" runat="server" class="ckeditor" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                            <asp:Button ID="cmdCancel" runat="server" CssClass="btn" Text="Cancel" OnClick="cmdCancel_Click" />
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
    <script type="text/javascript">
        $("#<%=DropDownList1.ClientID%>").change(function () {
            var val = $("#<%=DropDownList1.ClientID%>").val();
            //CKEDITOR.instances['<%=txtDescription.ClientID%>'].insertText(val);
            CKEDITOR.instances['<%=txtDescription.ClientID%>'].insertHtml(val);
        });
    </script>
</asp:Content>
