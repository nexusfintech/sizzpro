﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mycart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["cart"] != null)
        {
            List<servicecart> cart = (List<servicecart>)Session["cart"];
            rptcart.DataSource = cart;
            rptcart.DataBind();
            lbltotal.Text = "$" + calculatetotal(cart).ToString();
        }
        else
        {
            Response.Redirect("~/Checkout.aspx");
        }
    }
    protected void btn_payment_Click(object sender, EventArgs e)
    {
        Session["finalpay"] = lbltotal.Text.TrimStart('$');
        Response.Redirect("~/continuewith.aspx");
    }

    private decimal calculatetotal(List<servicecart> cart)
    {
        decimal total = 0;
        foreach (servicecart crt in cart)
        {
            decimal p = Convert.ToDecimal(crt.price.TrimStart('$'));
            total = total + p;
        }
        return total;
    }
}