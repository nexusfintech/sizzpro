﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AddEditPage.aspx.cs" Inherits="CMS_AddEditPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkCreateNew" runat="server" NavigateUrl="~/CMS/MyPages.aspx"><i class="glyphicon-projector"></i>&nbsp;My Pages</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Page Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <div class="control-group">
                            <label for="txtUrl" class="control-label">Page Url</label>
                            <div class="controls">
                                <asp:TextBox ID="txtUrl" runat="server" CssClass="input-xlarge"></asp:TextBox>
                                <span class="help-block">This will be used to redirect your page</span>
                            </div>
                        </div>
                         <div class="control-group">
                            <label for="txtUrl" class="control-label">Page Title</label>
                            <div class="controls">
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtUrl" class="control-label">Meta KeyWords</label>
                            <div class="controls">
                                <asp:TextBox ID="txtMetaKey" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            </div>
                        </div>
                         <div class="control-group">
                            <label for="txtUrl" class="control-label">Meta Description</label>
                            <div class="controls">
                                <asp:TextBox ID="txtMetaDesc" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtDescription" class="control-label">Page Content</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" class='ckeditor'></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="cmdSave_Click" />
                            <asp:Button ID="cmdCancel" runat="server" CssClass="btn" Text="Cancel" OnClick="cmdCancel_Click" />
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

