﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Globalization;
using clsDAL;

public partial class UserControl_ClientProfile_ucChildrenDetail : System.Web.UI.UserControl
{
    clsChildrenDetail clsCHD = new clsChildrenDetail();
    clsAssesmentStepRecord clsASR = new clsAssesmentStepRecord();
    clsUsers clsUsr = new clsUsers();
    Int64 intUserID;

    private static string strAddEditFlag;
    private static Int64 intEditID;

    string strMemberkey;

    public string Memberkey
    {
        get { return strMemberkey; }
        set { strMemberkey = value; }
    }

    private void BindGrid()
    {
        Guid objKey = new Guid(Memberkey);
        dtChildren.DataSource = clsCHD.GetAllRecordUserID(objKey);
        dtChildren.DataBind();
    }

    private String ValidationForm()
    {
        String strError = "";
        if (txtName.Text == "")
        { strError += "Please Enter Children Name <br/>"; }
        if (txtParName.Text == "")
        { strError += "Please Enter Parent Name <br/>"; }
        if (!clsFormat.ValidateDate(txtDOB.Text))
        { strError += "Invalid Birth Date <br/>"; }
        return strError;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        intUserID = Convert.ToInt64(Session["form_userid"]);
        if (!Page.IsPostBack)
        {
            BindGrid();
            txtDOB.Text = DateTime.Now.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            strAddEditFlag = "ADD";
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        String strResult = ValidationForm();
        if (strResult != "")
        {
            lblMessage.Text = strResult;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        DateTime dtBirthDate;
        if (!(DateTime.TryParseExact(txtDOB.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtBirthDate)))
        {
            lblMessage.Text = "Invalid Start Date format please enter in mm/dd/yyyy"; return;
        }
        clsCHD.chd_BirthDate = dtBirthDate;
        Guid objKey = new Guid(Memberkey);
        clsCHD.chd_age = txtAge.Text;
        clsCHD.chd_gender = ddlGender.SelectedItem.Text;
        clsCHD.chd_liveparent = chkLive.Checked;
        clsCHD.chd_name = txtName.Text;
        clsCHD.chd_parentname = txtParName.Text;
        clsCHD.chd_residence = txtResidence.Text;
        clsCHD.chd_userid = objKey;
        Boolean blnSave = false;
        if (strAddEditFlag == "ADD")
        { blnSave = clsCHD.Save(); }
        else { blnSave = clsCHD.Update(intEditID); }
        if (blnSave == true)
        { 
            lblMessage.Text = "Saved Successfully.";
            clsASR.storcrd_stepid = 8;
            clsASR.stprcrd_userid = objKey;
            Boolean stprslt = clsASR.Save(); 
            BindGrid();
        }
        else { lblMessage.Text = "Error in Save " + clsCHD.GetSetErrorMessage; }
    }

    protected void dtChildren_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            intEditID = Convert.ToInt64(e.CommandArgument);
            Boolean blnEdit = clsCHD.GetRecordByIDInProperties(intEditID);
            if (blnEdit == true)
            { EditEntry(); }
            else { lblMessage.Text = "Error... Edit Process not proper complete please try again."; }
        }
        if (e.CommandName == "delete")
        {
            intEditID = Convert.ToInt64(e.CommandArgument);
            Boolean blnEdit = clsCHD.DeleteByPKID(intEditID);
            if (blnEdit == true)
            { BindGrid(); lblMessage.Text = "Delete Successfully."; }
            else { lblMessage.Text = "Error... Edit Process not proper complete please try again."; }
        }
    }

    private void EditEntry()
    {
        strAddEditFlag = "EDIT";
        txtAge.Text = clsCHD.chd_age;
        txtDOB.Text = clsCHD.chd_BirthDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
        ddlGender.Text = clsCHD.chd_gender;
        chkLive.Checked = clsCHD.chd_liveparent;
        txtName.Text = clsCHD.chd_name;
        txtParName.Text = clsCHD.chd_parentname;
        txtResidence.Text = clsCHD.chd_residence;
    }
}