﻿using clsDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Agreements_AgreementServiceMapping : System.Web.UI.Page
{
    clsAgreements clsAgreement = new clsAgreements();
    clsAppointmentCourse clsCRS = new clsAppointmentCourse();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsAgreementServiceMapping clsASM = new clsAgreementServiceMapping();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (User.IsInRole("administrator"))
            {
                BindCombo();
            }
            else
            {
                bindservices();
                bindagreements();
            }
        }
    }

    private void bindservices()
    {
        DataTable dtRecords = new DataTable();
        if (User.IsInRole("administrator"))
        {
            dtRecords = clsCRS.GetAllRecordByMemberKey(new Guid(ddlSelectMember.SelectedValue));
        }
        else if (User.IsInRole("member"))
        {
            dtRecords = clsCRS.GetAllRecordByMemberKey((Guid)Membership.GetUser().ProviderUserKey);
        }
        else
        {
            dtRecords = clsCRS.GetAllRecordByStaff((Guid)Membership.GetUser().ProviderUserKey);
        }
        if (dtRecords.Rows.Count == 0)
        {
            ddlService.Items.Clear();
            ddlService.SelectedIndex = -1;
            ddlService.SelectedValue = null;
            ddlService.ClearSelection();
            ddlService.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-No Service Found->>";
            lst.Selected = true;
            ddlService.Items.Add(lst);
        }
        else
        {
            ddlService.Items.Clear();
            ddlService.SelectedIndex = -1;
            ddlService.SelectedValue = null;
            ddlService.ClearSelection();
            ddlService.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddlService.Items.Add(lst);
            ddlService.DataValueField = "crs_id";
            ddlService.DataTextField = "crs_coursename";
            ddlService.DataSource = dtRecords;
            ddlService.DataBind();
        }
    }

    private void bindagreements()
    {
        DataTable dtRecords = new DataTable();
        if (User.IsInRole("administrator"))
        {
            dtRecords = clsAgreement.GetAllRecordByUser(new Guid(ddlSelectMember.SelectedValue));
        }
        else
        {
            dtRecords = clsAgreement.GetAllRecordByUser((Guid)Membership.GetUser().ProviderUserKey);
        }
        if (dtRecords.Rows.Count == 0)
        {
            ddlAgreement.Items.Clear();
            ddlAgreement.SelectedIndex = -1;
            ddlAgreement.SelectedValue = null;
            ddlAgreement.ClearSelection();
            ddlAgreement.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-No Agreement Found->>";
            lst.Selected = true;
            ddlAgreement.Items.Add(lst);
        }
        else
        {
            ddlAgreement.Items.Clear();
            ddlAgreement.SelectedIndex = -1;
            ddlAgreement.SelectedValue = null;
            ddlAgreement.ClearSelection();
            ddlAgreement.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddlAgreement.Items.Add(lst);
            ddlAgreement.DataValueField = "agr_id";
            ddlAgreement.DataTextField = "agr_name";
            ddlAgreement.DataSource = dtRecords;
            ddlAgreement.DataBind();
        }
    }

    private void BindCombo()
    {
        ddlSelectMember.Items.Clear();
        ddlSelectMember.SelectedIndex = -1;
        ddlSelectMember.SelectedValue = null;
        ddlSelectMember.ClearSelection();
        ddlSelectMember.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddlSelectMember.Items.Add(lst);
        ddlSelectMember.DataValueField = "Userid";
        ddlSelectMember.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRole("Member");
        DataTable dtusr = clsCPM.GetAllComboByRole("User");
        dtResult.Merge(dtusr);
        ddlSelectMember.DataSource = dtResult;
        ddlSelectMember.DataBind();
    }

    private string ValidatePage()
    {
        string message = string.Empty;
        if(ddlAgreement.SelectedIndex == 0)
        {
            message = "Please select agreement to assign";
            return message;
        }
        if(ddlService.SelectedIndex==0)
        {
            message = "Please select service to assign";
            return message;
        }
        return message;
    }

    protected void ddlSelectMember_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindservices();
        bindagreements();
    }

    protected void cmdSave_Click( object sender, EventArgs e)
    {
        string vlidation = ValidatePage();
        if(vlidation != string.Empty)
        {
            lblmessage.Text = vlidation;
            return;
        }
        clsASM.asm_agreement = ddlAgreement.SelectedIndex;
        clsASM.asm_service = ddlService.SelectedIndex;
        Boolean ResltBln = clsASM.Save();
        if (ResltBln == true)
        {
            Response.Redirect("~/Agreements/AgreementMapping.aspx");
        }
        else
        {
            lblmessage.Text = clsASM.GetSetErrorMessage;
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Agreements/AgreementMaster.aspx");
    }
}