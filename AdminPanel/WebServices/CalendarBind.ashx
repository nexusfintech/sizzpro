﻿<%@ WebHandler Language="C#" Class="CalendarBind" %>

using System;
using System.Web;
using clsDAL;

public class CalendarBind : IHttpHandler 
{
    public void ProcessRequest (HttpContext context) 
    {
        context.Response.ContentType = "application/json";
        clsAppointmentMaster clsAPNT = new clsAppointmentMaster();

        Guid guUserid = new Guid(context.Request.QueryString["userid"]);
        
        clsAPNT.apt_userid = guUserid;
        System.Data.DataTable dtResult = clsAPNT.GetAllRecordUserWise();
        
        String strResult = string.Empty;
        strResult += "[";
        
        foreach (System.Data.DataRow dr in dtResult.Rows)
        {
            strResult += convertCalendarEventIntoString(dr);
        }
        
        
        if (strResult.EndsWith(","))
        {
            strResult = strResult.Substring(0, strResult.Length - 1);
        }
        strResult += "]";

        context.Response.Write(strResult);
        
    }

    private String convertCalendarEventIntoString(System.Data.DataRow dr)
    {
        String allDay = "true";
        DateTime dtStart = (DateTime)dr["apt_startdatetime"];
        DateTime dtEnd = (DateTime)dr["apt_enddatetime"];
        if (ConvertToTimestamp(dtStart).ToString().Equals(ConvertToTimestamp(dtEnd).ToString()))
        {

            if (dtStart.Hour == 0 && dtStart.Minute == 0 && dtStart.Second == 0)
            {
                allDay = "true";
            }
            else
            {
                allDay = "false";
            }
        }
        else
        {
            if (dtStart.Hour == 0 && dtStart.Minute == 0 && dtStart.Second == 0
                && dtEnd.Hour == 0 && dtEnd.Minute == 0 && dtEnd.Second == 0)
            {
                allDay = "true";
            }
            else
            {
                allDay = "false";
            }
        }
        return "{" +
                  @"""id"": """ + dr["apt_id"].ToString() + @"""," +
                  @"""title"": """ + HttpContext.Current.Server.HtmlEncode(dr["apt_title"].ToString()) + @"""," +
                  @"""start"": """ + dr["apt_startdatetime"].ToString() + @"""," +
                  @"""end"": """ + dr["apt_enddatetime"].ToString() + @"""," +
                  @"""allDay"":" + allDay + 
                  "},";
    }
    
 
    public bool IsReusable 
    {
        get {
            return false;
        }
    }
    
    private long ConvertToTimestamp(DateTime value)
    {
        long epoch = (value.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
        return epoch;
    }
}