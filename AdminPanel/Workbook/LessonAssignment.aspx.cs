﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using clsDAL;

public partial class Workbook_LessonAssignment : System.Web.UI.Page
{
    clsLessionToAssignment clsLTA = new clsLessionToAssignment();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddata();
        }
    }
    public void binddata()
    {
        if (MySession.Current.EditID != 0)
        {
            dtLSnAsn.DataSource = clsLTA.GetLessonAssignmet(MySession.Current.EditID);
            dtLSnAsn.DataBind();
        }
        else
        {
            Response.Redirect("~/Workbook/ProgrammClient.aspx");
        }
    }
    protected void lnkAns_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn=(LinkButton)sender;
        MySession.Current.EditID = Convert.ToInt64(lnkbtn.CommandArgument);
        Response.Redirect("~/Workbook/AssignmentAnswer.aspx");
    }
}