﻿using clsDAL.Conn;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace clsDAL
{
    public class clsStaticMaster
    {

        #region Class's Event and object and instance declare
        public delegate void ChangingHandler(object sender, MyEventArgs e);
        public event ChangingHandler GetRisedError;
        private SqlConnection clsCon = new SqlConnection();
        private SqlCommand clsCMD = new SqlCommand();
        #endregion

        #region Class's Method and Function Declare

        public class MyEventArgs : System.EventArgs
        {
            private string message;
            public MyEventArgs(string m)
            {
                this.message = m;
            }

            public string ErrorMessage()
            {
                return message;
            }
        }

        public clsStaticMaster()
        {
            clsCMD.Parameters.Clear();
            clsConnection clsSQLCon = new clsConnection();            
            if (!clsSQLCon.GetConnection())
            { GetSetErrorMessage = "Database Server Connection Failed...!!"; return; }
            else { this.clsCon = clsSQLCon.SqlDBCon; }
            if (clsCon.State == ConnectionState.Closed) { clsCon.Open(); }
            clsCMD.Connection = clsCon; clsCMD.CommandType = CommandType.StoredProcedure;
            SetDefaultValueToProperties();
        }

        ~clsStaticMaster()
        {

        }

        public void Dispose()
        {
            if (clsCon.State == ConnectionState.Open) { clsCon.Close(); }
            clsCMD.Parameters.Clear();
            System.GC.SuppressFinalize(this);
            this.Dispose();
        }

        private void ClearCommandParameter()
        {
            clsCMD.Parameters.Clear();
            SetDefaultValueToProperties();
        }

        #endregion

        #region Declare Class's Static Properties
        private string _strError = null;
        public string GetSetErrorMessage
        {
            get { return _strError; }
            set
            {
                _strError = value;
                if (this.GetRisedError != null)
                {
                    this.GetRisedError(this, new MyEventArgs(_strError));
                }
            }
        }
        private string _strAddEditDeleteFlag = "ADD";
        public string AddEditDeleteFlag
        {
            get { return _strAddEditDeleteFlag; }
            set
            {
                _strAddEditDeleteFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private string _strGetSPFlag = "ALL";
        public string SetGetSPFlag
        {
            get { return _strGetSPFlag; }
            set
            {
                _strGetSPFlag = value;
                if (clsCMD.Parameters.Contains("@Flag"))
                { clsCMD.Parameters["@Flag"].Value = value; }
                else { clsCMD.Parameters.Add("@Flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        public Int64 PrimeryKey
        {
            get { return _stm_id; }
            set
            {
                _stm_id = value;
                if (clsCMD.Parameters.Contains("@stm_id"))
                { clsCMD.Parameters["@stm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@stm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        #endregion

        #region Declare Class's Field Based Properties
        private Int64 _stm_id;
        public Int64 stm_id
        {
            get { return _stm_id; }
            set
            {
                _stm_id = value;
                if (clsCMD.Parameters.Contains("@stm_id"))
                { clsCMD.Parameters["@stm_id"].Value = value; }
                else { clsCMD.Parameters.Add("@stm_id", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _stm_name;
        public String stm_name
        {
            get { return _stm_name; }
            set
            {
                _stm_name = value;
                if (clsCMD.Parameters.Contains("@stm_name"))
                { clsCMD.Parameters["@stm_name"].Value = value; }
                else { clsCMD.Parameters.Add("@stm_name", SqlDbType.NVarChar, 100).Value = value; }
            }
        }
        private Int64 _stm_value;
        public Int64 stm_value
        {
            get { return _stm_value; }
            set
            {
                _stm_value = value;
                if (clsCMD.Parameters.Contains("@stm_value"))
                { clsCMD.Parameters["@stm_value"].Value = value; }
                else { clsCMD.Parameters.Add("@stm_value", SqlDbType.BigInt).Value = value; }
            }
        }
        private String _stm_group;
        public String stm_group
        {
            get { return _stm_group; }
            set
            {
                _stm_group = value;
                if (clsCMD.Parameters.Contains("@stm_group"))
                { clsCMD.Parameters["@stm_group"].Value = value; }
                else { clsCMD.Parameters.Add("@stm_group", SqlDbType.NVarChar, 50).Value = value; }
            }
        }
        private String _stm_flag;
        public String stm_flag
        {
            get { return _stm_flag; }
            set
            {
                _stm_flag = value;
                if (clsCMD.Parameters.Contains("@stm_flag"))
                { clsCMD.Parameters["@stm_flag"].Value = value; }
                else { clsCMD.Parameters.Add("@stm_flag", SqlDbType.NVarChar, 50).Value = value; }
            }
        }

        #endregion

        #region SetRowValue to Properties
        private Boolean SetValueToProperties(DataRow dr)
        {
            Boolean blnResult = false;
            try
            {
                stm_id = Convert.ToInt64(dr["stm_id"]);
                stm_name = Convert.ToString(dr["stm_name"]);
                stm_value = Convert.ToInt64(dr["stm_value"]);
                stm_group = Convert.ToString(dr["stm_group"]);
                stm_flag = Convert.ToString(dr["stm_flag"]);
                blnResult = true;
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = "Error..in row value set to properties" + Environment.NewLine + e.Message; }
            return blnResult;
        }
        #endregion

        #region Set Default Value to all field's properties
        private void SetDefaultValueToProperties()
        {
            stm_id = Convert.ToInt64("0");
            stm_name = Convert.ToString("NA");
            stm_value = Convert.ToInt64("0");
            stm_group = Convert.ToString("NA");
            stm_flag = Convert.ToString("NA");
        }
        #endregion

        #region Declare Add/Edit/Delete Function
        private void OpenConnection()
        {
            clsConnection clsSQLCon = new clsConnection();
            if (clsCon.State == ConnectionState.Closed)
            {
                if (!clsSQLCon.GetConnection())
                { GetSetErrorMessage = "Database Server Connection Failed...!!"; }
                else { this.clsCon = clsSQLCon.SqlDBCon; }
            }
        }

        private void CloseConnection()
        {
            if (clsCon.State == ConnectionState.Open)
            { clsCon.Close(); }
        }

        public Boolean Save()
        {
            Boolean blnResult = false;
            this.stm_id = 0;
            this.AddEditDeleteFlag = "ADD";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_staticmasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Entry Saved Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in new entry operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean Update(Int64 stm_id)
        {
            Boolean blnResult = false;
            this.stm_id = stm_id;
            this.AddEditDeleteFlag = "EDIT";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_staticmasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Updated Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in edit operation."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        public Boolean DeleteByPKID(Int64 stm_id)
        {
            Boolean blnResult = false;
            this.stm_id = stm_id;
            this.AddEditDeleteFlag = "DELETEBYPKID";
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_staticmasterAddEditDelete";
                int intRow = clsCMD.ExecuteNonQuery();
                if (intRow > 0)
                { blnResult = true; GetSetErrorMessage = "Record Delete Successfully."; }
                else { blnResult = false; GetSetErrorMessage = "Error in Delete Opration."; }
            }
            catch (Exception e)
            { blnResult = false; GetSetErrorMessage = e.Message; }
            CloseConnection();
            return blnResult;
        }

        #endregion

        #region Get DataTable Methods ByID,CheckUplicate etc..

        public DataTable GetAllRecord()
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                clsCMD.CommandText = "sp_staticmasterGetData";
                SetGetSPFlag = "ALL";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }

        public List<clsStaticMaster_PropertiesList> DatatableToList(DataTable dtTable)
        {
            List<clsStaticMaster_PropertiesList> lstResult = new List<clsStaticMaster_PropertiesList>();
            DataTable dtResult = dtTable;
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                lstResult[i].stm_id = Convert.ToInt64(dr["stm_id"]);
                lstResult[i].stm_name = Convert.ToString(dr["stm_name"]);
                lstResult[i].stm_value = Convert.ToInt64(dr["stm_value"]);
                lstResult[i].stm_group = Convert.ToString(dr["stm_group"]);
                lstResult[i].stm_flag = Convert.ToString(dr["stm_flag"]);

                i++;
            }
            return lstResult;
        }

        public Boolean GetRecordByIDInProperties(Int64 stm_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.stm_id = stm_id;
                clsCMD.CommandText = "sp_staticmasterGetData";
                SetGetSPFlag = "BYPKID";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = SetValueToProperties(dtResult.Rows[0]); }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public Boolean CheckDuplicateRow(Int64 stm_id)
        {
            Boolean blnResult = false;
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                this.stm_id = stm_id;
                clsCMD.CommandText = "sp_staticmasterGetData";
                SetGetSPFlag = "CHECKDUPLICATE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                DataTable dtResult = new DataTable();
                SDA.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                { blnResult = true; }
                else { blnResult = false; }
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                blnResult = false;
            }
            CloseConnection();
            return blnResult;
        }

        public DataTable GetFlagWise(string strFlag)
        {
            DataTable dtResult = new DataTable();
            try
            {
                OpenConnection();
                clsCMD.Connection = clsCon;
                stm_flag = strFlag;
                clsCMD.CommandText = "sp_staticmasterGetData";
                SetGetSPFlag = "FLAGWISE";
                SqlDataAdapter SDA = new SqlDataAdapter(clsCMD);
                SDA.Fill(dtResult);
            }
            catch (Exception e)
            {
                GetSetErrorMessage = e.Message;
                dtResult = null;
            }
            CloseConnection();
            return dtResult;
        }
        #endregion
    }
    public class clsStaticMaster_PropertiesList
    {
        public Int64 stm_id { get; set; }
        public String stm_name { get; set; }
        public Int64 stm_value { get; set; }
        public String stm_group { get; set; }
        public String stm_flag { get; set; }
    }
}