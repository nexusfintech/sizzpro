﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucFamilyofOrigin.ascx.cs" Inherits="UserControl_ClientProfile_ucFamilyofOrigin" %>
<div class="row-fluid">
    <h4>
        <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
    </h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class="icon-user"></i>
                    <asp:Label ID="lblBoxheader" runat="server"></asp:Label>
                </h3>
            </div>
            <div class="box-content nopadding">
                <div class='form-horizontal form-column form-bordered'>
                    <div class="span6">
                        <div class="control-group">
                            <label for="txtFirstName" class="control-label">First Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtFirstName" runat="server" placeholder="Firstname" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtMiddlename" class="control-label">Middle Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtMiddlename" runat="server" placeholder="Middlename" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtlastname" class="control-label">Last Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtlastname" runat="server" placeholder="Last Name" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="control-group">
                            <label for="txtAge" class="control-label">Age</label>
                            <div class="controls">
                                <asp:TextBox ID="txtAge" runat="server" placeholder="18 Years" data-rule-required="true" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="chklevewithclient" class="control-label"></label>
                            <div class="controls">
                                <div class="span12 check-line">
                                    <asp:CheckBox ID="chklevewithclient" runat="server" CssClass='icheck-me' data-skin="minimal" data-color="blue" />
                                    <label class='inline' for="chklevewithclient">Lives With Client..?</label>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtDescription" class="control-label"></label>
                            <div class="controls">
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="control-group">
                            <label for="txtDescription" class="control-label">Description</label>
                            <div class="controls">
                                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" placeholder="Description" Width="98%" class="complexify-me"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
