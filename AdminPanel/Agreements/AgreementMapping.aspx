﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AgreementMapping.aspx.cs" Inherits="Agreements_AgreementMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkReturn" runat="server" NavigateUrl="~/Agreements/AgreementMaster.aspx"><i class="icon-retweet"></i>&nbsp;Agreements </asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-color box-bordered">
        <div class="box-title">
            <h3>
                <i class="glyphicon-projector"></i>
                Agreement Service Mapping
            </h3>
        </div>
        <div class="box-content nopadding">
            <asp:Repeater ID="dtAgreementMping" runat="server">
                <HeaderTemplate>
                    <table class="table table-hover table-nomargin dataTable table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Mapped Service</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("Services ") %></td>
                        <td style="text-align: center">
                            <asp:LinkButton ID="CmdMappingRmv" runat="server" OnClick="CmdMappingRmv_Click" CssClass="btn btn-green" CommandName="MAPPING" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "asm_id") %>'><i class="icon-trash"></i> Remove Mapping</asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>
