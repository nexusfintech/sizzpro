﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PhysicalSymptoms.aspx.cs" Inherits="Client_Assessments_PhysicalSymptoms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <asp:HiddenField ID="AssesmentStep" runat="server" Value="13" />
    <div class="box">
        <div class="box-title">
            <h3>
                <i class="icon-heart"></i>
                Physical Symptoms
            </h3>
        </div>
        <div class="box-content nopadding">
            <UC:PhysicalSymptoms ID="PhySymptoms" runat="server" />
        </div>
    </div>
    <div class="row-fluid">
        <hr />
        <div class="span12">
            <asp:Button ID="cmdNext" runat="server" Text="Next Accident Information >>" CssClass="btn btn-primary" OnClick="cmdNext_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

