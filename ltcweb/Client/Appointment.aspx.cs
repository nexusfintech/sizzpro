﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Globalization;
using clsDAL;
using System.Configuration;
using System.Net;

public partial class Appointment : System.Web.UI.Page
{
    clsAppointmentCourse clsCRS = new clsAppointmentCourse();
    clsClientParentMapping clsCPM = new clsClientParentMapping();
    clsCourseMapping clsCRM = new clsCourseMapping();
    clsAppointmentMaster clsAPNT = new clsAppointmentMaster();
    clsSettings clsSTG = new clsSettings();

    clsAgreementServiceMapping clsASM = new clsAgreementServiceMapping();

    clsAgreemetFile clsAF = new clsAgreemetFile();

    clsICdetails icPathDetail = new clsICdetails();

    private string AdminKey { get; set; }
    private string UserKey { get; set; }
    private string ClientKey { get; set; }

    private void BindCombo()
    {
        ddltherapist.Items.Clear();
        ddltherapist.SelectedIndex = -1;
        ddltherapist.SelectedValue = null;
        ddltherapist.ClearSelection();
        ddltherapist.AppendDataBoundItems = true;
        ListItem lst = new ListItem();
        lst.Value = "0";
        lst.Text = "<<-Please Select->>";
        lst.Selected = true;
        ddltherapist.Items.Add(lst);
        ddltherapist.DataValueField = "UserId";
        ddltherapist.DataTextField = "UserName";
        DataTable dtResult = clsCPM.GetAllComboByRole("User");
        ddltherapist.DataSource = dtResult;
        ddltherapist.DataBind();

    }

    private Boolean ValidateDate(string strValue)
    {
        Match vldDate = Regex.Match(strValue, "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$");
        return vldDate.Success;
    }

    private string ValidatePage()
    {
        string strResult = "";
        if (ddltherapist.SelectedItem.Value == "0")
        { strResult += "Select Therapist <br/>"; return strResult; }
        if (ddlApntCourse.SelectedItem.Value == "0")
        { strResult += "Select Course/Service <br/>"; return strResult; }
        if (txtTitle.Text.Trim() == "")
        { strResult += "Please Enter Title/Subject <br/>"; return strResult; }
        if (txtDescription.Text.Trim() == "")
        { strResult += "Please Enter Description <br/>"; return strResult; }
        if (!ValidateDate(txtAppntDate.Text.Trim()))
        { strResult += "Enter proper appointment date"; return strResult; }
        else
        {
            DateTime dtCurDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime dtApntDate;
            if (!(DateTime.TryParseExact(txtAppntDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtApntDate)))
            {
                strResult += "Invalid Appointment Date format! Please enter in mm/dd/yyyy format";
                return strResult;
            }
            else
            {
                if (dtApntDate < dtCurDate)
                {
                    strResult += "Previous days appointment date is not valid..!!";
                }
            }
        }
        return strResult;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCombo();
            string uname = HttpContext.Current.User.Identity.Name.ToString();
            //chechk user logged in or not - if login check informed consent completed or not
            if (uname == "")
            {
                lblmesg.Visible = true;
                cmdAddPontment.Visible = false;
                return;
            }
            if (HttpContext.Current.User.Identity.Name.ToString() == "")
                cmdAddPontment.Visible = false;

            string strDate = DateTime.Now.ToString("MM/dd/yyyy");
            DateTime dtsysdate;
            if (DateTime.TryParseExact(strDate, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out dtsysdate))
            {
                txtAppntDate.Text = dtsysdate.ToShortDateString();
            }
        }
    }

    protected void ddltherapist_SelectedIndexChanged(object sender, EventArgs e)
    {
        Guid guUsrID = new Guid();
        if (ddltherapist.SelectedItem.Value != "0")
        {
            guUsrID = new Guid(ddltherapist.SelectedItem.Value.ToString());
        }
        DataTable dtGetData = clsCRM.GetUserWiseCombo(guUsrID);
        if (dtGetData.Rows.Count == 0)
        {
            ddlApntCourse.Items.Clear();
            ddlApntCourse.SelectedIndex = -1;
            ddlApntCourse.SelectedValue = null;
            ddlApntCourse.ClearSelection();
            ddlApntCourse.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-No Service Found->>";
            lst.Selected = true;
            ddlApntCourse.Items.Add(lst);
        }
        else
        {
            ddlApntCourse.Items.Clear();
            ddlApntCourse.SelectedIndex = -1;
            ddlApntCourse.SelectedValue = null;
            ddlApntCourse.ClearSelection();
            ddlApntCourse.AppendDataBoundItems = true;
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "<<-Please Select->>";
            lst.Selected = true;
            ddlApntCourse.Items.Add(lst);
            ddlApntCourse.DataValueField = "crs_id";
            ddlApntCourse.DataTextField = "crs_coursename";
            ddlApntCourse.DataSource = clsCRM.GetUserWiseCombo(guUsrID);
            ddlApntCourse.DataBind();
        }
    }

    protected void cmdGetAvailability_Click(object sender, EventArgs e)
    {
        string strResult = ValidatePage();
        if (strResult != "")
        { lblMessage.Text = strResult; return; }
        lblMessage.Text = "";
        lstTime.Items.Clear();
        Guid guUserid = new Guid(ddltherapist.SelectedItem.Value);
        clsAPNT.apt_userid = guUserid;
        Int64 intCRSID = Convert.ToInt64(ddlApntCourse.SelectedItem.Value);
        Boolean blnResult = clsCRS.GetRecordByIDInProperties(intCRSID);
        if (blnResult == true)
        {
            Int64 intInterval = clsCRS.crs_interval;
            //Int64 intProgTime = clsCRS.crs_minutes;
            DateTime dtCurDate;
            if (DateTime.TryParseExact(txtAppntDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtCurDate))
            {
                Boolean blnWH = clsSTG.GetRecordByFlagUserInProperties("WHSTARTTIME", guUserid);
                DateTime dtWHStart = clsSTG.stg_datetimevalue;
                clsSTG.GetRecordByFlagUserInProperties("WHENDTIME", guUserid);
                DateTime dtWHEnd = clsSTG.stg_datetimevalue;
                if (blnWH == true)
                {
                    DateTime dtstartTime = new DateTime(dtCurDate.Year, dtCurDate.Month, dtCurDate.Day, dtWHStart.Hour, dtWHStart.Minute, dtWHStart.Second);
                    DateTime dtEndTime = new DateTime(dtCurDate.Year, dtCurDate.Month, dtCurDate.Day, dtWHEnd.Hour, dtWHEnd.Minute, dtWHEnd.Second);
                    while (dtstartTime <= dtEndTime.AddMinutes( intInterval * (-1)))
                    {
                        clsAPNT.apt_apointmentdate = dtCurDate;
                        clsAPNT.apt_startdatetime = dtstartTime;
                        DateTime dtToTime = new DateTime();
                        dtToTime = dtstartTime;
                        dtToTime = dtToTime.AddMinutes(Convert.ToDouble(intInterval));
                        clsAPNT.apt_enddatetime = dtToTime;
                        DataTable dtResult = clsAPNT.GetTimeWiseAppointment();
                        if (dtResult.Rows.Count == 0)
                        {
                            DateTime dtnw = DateTime.Now;
                            if(dtstartTime > dtnw)
                            {
                                ListItem lst = new ListItem();
                                lst.Value = dtstartTime.ToString("MM/dd/yyyy hh:mm:ss tt");
                                lst.Text = dtstartTime.ToString("HH:mm:ss");
                                lst.Attributes.Add("class", "itemstwo");
                                lstTime.Items.Add(lst);
                            }
                        }
                        dtstartTime = dtstartTime.AddMinutes(5);
                    }
                }
                else { lblMessage.Text = "Working Hourse not define please contact to website administrator"; }
            }
            else { lblMessage.Text = "Invalid Appointment Date format please input in mm/dd/yyyy"; }
        }
    }

    protected void cmdAddPontment_Click(object sender, EventArgs e)
    {
        string str = this.ValidatePage();
        //user is not available then go to login page
        if (str == string.Empty)
        {
            if (HttpContext.Current.User.Identity.Name.ToString() == string.Empty)
            {
                Response.Redirect("~/Login.aspx");
            }

            AdminKey = Membership.GetUser(ConfigurationManager.AppSettings["memberuserid"].ToString()).ProviderUserKey.ToString();
            UserKey = ddltherapist.SelectedItem.Value.ToString();
            ClientKey = Membership.GetUser().ProviderUserKey.ToString();

            // set value to appoinment class
            clsAPNT.apt_clientid = new Guid(ClientKey);
            clsAPNT.apt_code = DateTime.Now.Ticks.ToString();
            clsAPNT.apt_courseid = Convert.ToInt64(ddlApntCourse.SelectedItem.Value);
            clsAPNT.apt_description = "Title : " + txtTitle.Text.Trim() + Environment.NewLine + txtDescription.Text;
            clsAPNT.apt_title = txtTitle.Text.Trim();
            clsAPNT.apt_userid =new Guid(UserKey);
            DateTime dtStartDate = DateTime.Parse(lstTime.SelectedItem.Value.ToString(), CultureInfo.InvariantCulture, DateTimeStyles.None);
            clsAPNT.apt_startdatetime = dtStartDate;
            DateTime dtEndDate = dtStartDate.AddMinutes(clsCRS.crs_interval);
            clsAPNT.apt_enddatetime = dtEndDate;

            // check for agreement of particular service 
            Boolean blnRsltAr = clsASM.GetRecordByServiceinProperties(clsAPNT.apt_courseid);
            if (blnRsltAr == true)
            {
                Boolean blnRsltFl = clsAF.CheckForParicularRecord(clsAPNT.apt_courseid, clsAPNT.apt_clientid);
                if (blnRsltFl == true)
                {
                    Session["appoinment"] = clsAPNT;
                    Response.Redirect("~/Client/AppStp2.aspx");
                }
                else
                {
                    int stscode = createpdfrequest();
                    if (stscode == 1)
                    {
                        Session["appoinment"] = clsAPNT;
                        Response.Redirect("~/Client/AppStp2.aspx");
                    }
                    else if (stscode == 2)
                    {
                        Session["msg_boxheader"] = "Error";
                        Session["msg_message"] = "Hello " + Membership.GetUser().UserName + " Sorry ..! <br/> Server is not able to save your file data to database . pleasee try again <br/> If you are continue to getting this error then contact administrator of this site .";
                        Response.Redirect("~/Messages.aspx");
                    }
                    else if (stscode == 3)
                    {
                        Session["msg_boxheader"] = "Error";
                        Session["msg_message"] = "Hello " + Membership.GetUser().UserName + "Sorry ..! <br/> Server is not able to cretae your Inform Concent File . pleasee try again <br/> If you are continue to getting this error then contact administrator of this site .";
                        Response.Redirect("~/Messages.aspx");
                    }
                    else
                    {
                        Session["msg_boxheader"] = "Error";
                        Session["msg_message"] = "Sorry ..! <br/> Some Unknown error occured please try again or contact site admin";
                        Response.Redirect("~/Messages.aspx");
                    }
                }
            }
            else
            {
                Session["appoinment"] = clsAPNT;
                Response.Redirect("~/Client/AppStp2.aspx");
            }
        }
        else
        { 
            lblMessage.Text = str.ToString(); return; 
        }

    }

    private int createpdfrequest()
    {
        //string url = ConfigurationManager.AppSettings["SizzUrl"].ToString() + "/InformConcent.aspx?mode=create&m=" + AdminKey + "&c=" + ClientKey;
        string url = ConfigurationManager.AppSettings["SizzUrl"].ToString() + "/InformConcent.aspx?mode=create&s=" + clsAPNT.apt_courseid + "&c=" + ClientKey;
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.Method = "POST";
        request.ContentLength = 0;
        var response = (HttpWebResponse)request.GetResponse();
        int statuscode = Convert.ToInt32(response.StatusCode);
        return statuscode;
    }
}