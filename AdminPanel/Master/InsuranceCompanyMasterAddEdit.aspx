﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageUM.master" AutoEventWireup="true" CodeFile="InsuranceCompanyMasterAddEdit.aspx.cs" Inherits="Master_InsuranceCompanyAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftBar" runat="Server">
    <div class="subnav">
        <div class="subnav-title">
            <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Actions</span></a>
        </div>
        <ul class="subnav-menu">
            <li>
                <asp:HyperLink ID="lnkmaster" runat="server" NavigateUrl="~/Master/InsauraceCompanyMaster.aspx"><i class="icon-retweet"></i>&nbsp;Company Master</asp:HyperLink>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <div class="alert alert-info" id="msgdiv" runat="server" visible="false">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </div>
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-edit"></i>Insurance Company Add/Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <div class="form-horizontal form-bordered">
                        <%-- <% if (User.IsInRole("administrator"))
                           { %>
                        <div class="control-group">
                            <label for="textfield" class="control-label">Creater</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSelectMember" runat="server" CssClass="select2-me input-xlarge"></asp:DropDownList>
                                <span class="help-block">Select Creater for whom Workbook Is created</span>
                            </div>
                        </div>
                        <%} %>--%>
                        <div class="control-group">
                            <label for="txtcode" class="control-label">Company Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtcompanyname" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtcode" class="control-label">Payer Id</label>
                            <div class="controls">
                                <asp:TextBox ID="txtpayorid" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtdescription" class="control-label">Address</label>
                            <div class="controls">
                                <asp:TextBox ID="txtadress" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtcode" class="control-label">City</label>
                            <div class="controls">
                                <asp:TextBox ID="txtcity" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtpos" class="control-label">State</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlState" runat="server" CssClass="select2-me input-large">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtcode" class="control-label">Pin Code</label>
                            <div class="controls">
                                <asp:TextBox ID="txtpin" runat="server" CssClass="input-medium"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtmoda" class="control-label">Phone</label>
                            <div class="controls">
                                <asp:TextBox ID="txtphone" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtmodb" class="control-label">Fax</label>
                            <div class="controls">
                                <asp:TextBox ID="txtfax" runat="server" CssClass="input-large"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtmodb" class="control-label">Insurance Type</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlInscType" runat="server" CssClass="select2-me input-large">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtmodb" class="control-label">Billing Type</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlBlngTyp" runat="server" CssClass="select2-me input-large">
                                    <asp:ListItem Value="Paper">PAPER</asp:ListItem>
                                    <asp:ListItem Value="Edi">EDI</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                         <div class="control-group">
                            <label for="txtmodb" class="control-label">Health Plan Eligibility</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlHpa" runat="server" CssClass="select2-me input-large">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtmodb" class="control-label">Default to ICD-10 on/DOS</label>
                            <div class="controls">
                                <asp:TextBox ID="txtdflticdt" runat="server" CssClass="input-large datepick"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:LinkButton ID="cmdSave" runat="server" CssClass="btn btn-primary" OnClick="cmdSave_Click"><i class="icon-ok"></i> Save</asp:LinkButton>
                            <asp:LinkButton ID="cmdCancel" runat="server" CssClass="btn" OnClick="cmdCancel_Click"><i class="icon-remove"></i> Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

