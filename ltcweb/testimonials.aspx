﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="testimonials.aspx.cs" Inherits="testimonials" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="Server">
    <div class="row-fluid">
        <div class="span12">
            <h2><i class="icon-smile"></i>&nbsp;SUCCESS STORIES</h2>
        </div>
    </div>
    <hr />
    <div class="row-fluid">
        <div class="span12">
            <p>
                <strong>There is nothing more inspiring than to hear success stories. </strong>I am writing this letter to recommend Mr. Joseph Follette's seminars and workshops. We have been fortunate to have had Mr. Follette and his team to share with our congregation on two different occasions and the results were astounding... His seminars were informative, practical, enlightening, Biblically based, captivating, and enjoyable... They were blessed; lives and relationships changed; and many persons were edified... "To God be the glory". Ricky L. Sykes, Pastor New Jerusalem Missionary Baptist Church 
                <br />
                <hr />
                <a href="http://lifestyletherapycoach.com/network/wp-content/uploads/2010/04/older-couple-beach.png"></a>
            </p>
            <a href="http://lifestyletherapycoach.com/network/wp-content/uploads/2010/04/older-couple-beach.png"><a href="http://lifestyletherapycoach.com/network/wp-content/uploads/2010/04/older-couple-beach.png">
                <img class="alignright size-medium wp-image-1298" title="older-couple-beach" src="http://sizzlinghotmarriage.com/blog/wp-content/uploads/2010/04/older-couple-beach-300x210.png" alt="Happy Couple on beach" width="143" height="111" /></a>
                <p>On April 4 my life changed forever. That was the day I found out my husband had slept with someone else. I felt like my life was over. He betrayed me. He destroyed my love. I have never known pain like that before. I believe we had a good relationship. We really never argued. We laughed and seemed to enjoy each others company. What I believed was not so. Yes, he did love me. I got too comfortable withthe thought he loved me, and I didn't have to work at our marriage any more. I felt like he wasn't going anywhere. He would nevercheat on me. Big mistake. I used to always say when I got married that if my husband ever had an affair, divorce was the only answer because I couldn't forgive that.Until you're put in the situation, you can't say what you'll never do. It's not easy as you think.I battled with my emotions and my self-esteem every day. Was I not woman enough? What could I have done to prevent it? Was she prettier than me? When this happened I was pregnant. That made matters worse.Then one day I decided to get down on my knees and ask the Lord for his help. He told me to work things out. We started getting back on track. It was still a struggle everyday. Then on June 3rd, 2000 my life changed again. I attended a "One Night Stand with Your Mate". Nobody could have made me believe that one night could have made such a difference. My eyes were opened to a new light. I feel closer to my husband than I could ever hope for or imagined. We both learned a lot about love, commitment and each other. Things became clearer. We now have a better understanding of meeting each others need and better understanding of what commitment to your marriage means. The time we spend together now is more enjoyable and meaningful. I feel more alive. I feel I have connected better spiritually, sexually, and emotionally with my husband. I have more respect for him and what he needs.After attending the seminar,  I now have a better understanding in what goes into having a loving, lasting and happy marriage. It's hard work, but now I love working at it. I have more fun working at it. I now feel secure in myself, my marriage and my husband. I am at peace with myself. I have learned no woman can seduce a happy and fulfilled married man. Without the Lord I don't think we would have made it. I am thankful he sent his angels Joe and Heidi to me to guide us and direct us to him. Thank you, I am eternally grateful.  TD </p>
                <hr />
                <p>By coming to therapy, I think it has made me a better person. Because I don't have to be so up tight about stuff anymore. And I also think it has helped because I stay out of trouble. I don't go around hitting stuff, or even trying to pick fights anymore. Because of this class, out of the numerous amount of times I had to come here, I think it really paid off because I think I have something to live for.  DW </p>
                <hr />
                <p> <a href="http://sizzlinghotmarriage.com/blog/wp-content/uploads/2010/04/JoeClaudiaFollette.png">
                    <img class="alignleft size-thumbnail wp-image-1301" style="margin-left: 10px; margin-right: 10px; border: 5px solid black;" title="JoeClaudiaFollette" src="http://sizzlinghotmarriage.com/blog/wp-content/uploads/2010/04/JoeClaudiaFollette-150x150.png" alt="Happy Couple" width="150" height="150" /></a>It is my pleasure to recommend Joseph L. Follette, Jr., M.Div., M.S. for your marriage seminars(s), I have no reservations whatever in his presentations. Mr. Follette offers a much needed series on family therapy. He conducted a Strengthening Marriage Series here at Pentecostal Lighthouse titled "Selfishness vs. Self-fulfillment in Marriage." It was a great success. It was a relief for me that night, to be freed from the responsibility of teaching my congregation, and a joy to have been taught with them by him. Mr. Follette is extremely well taught in the subject of marriage. He is a person of excellent character and integrity, and has superior "people skills." In addition to his ability and knowledge, Mr. Follette is also blessed with a lovely family; his wife is very supportive of his work. I was especially impressed by the way he used various activities in his presentation. I also appreciated the fact that he had considered in advance what we needed for the seminar; this was very helpful to my staff. His performance was extremely well. I shall be happy to provide further information you might require.  Bishop Johnny Burrell  Pentecostal Lighthouse Church
                </p>
                <hr />
                <p>
                    Mr. Joseph Follette has been providing individual and group counseling services to the students in my alternative school program. These students have been placed in this program due to their behavior and anger problems. Their ages range from thirteen to eighteen. My students are in great need of counseling. Mr. Follette meets this need very well. He is very knowledgeable and has good insight into the problems of each student. He also contacts the parents and works with the family. Mr. Follette is a valuable asset to my program.  Debbie Ezell  Bob Jones High School - Alternative School Teacher
                </p>
                <hr />
                <p>I am very grateful for the group. Thanks for making God's Word the foundation of discussion. I was very close to violating a spiritual principle last week. Last evening's discussion was really helpful for me. Keep sharing the Word and reminding us that life can be different, and satisfying God's way.  WH </p>
                <hr />
                <p><strong>Do you want to be our next success story? </strong></p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScripts" runat="Server">
</asp:Content>

